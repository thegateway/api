{% if title %}
{{ title }}

{% endif %}
{{ date }}
{% if order.seller.phone %}
Puh: {{ order.seller.phone }}
{% endif %}

Hyvä {{ order.buyer.actor.name }},
Olet pyytänyt uutta salasanaa. Saadaksesi uuden salasanan, klikkaa tästä:
{{ url_token }}

Kiitos,
    {{ order.seller.name }}

{% if order.seller.addresses %}
{{ order.seller.addresses[0].street_address }}, {{ order.seller.addresses[0].postal_code }} {{ order.seller.addresses[0].postal_area }}
{% endif %}
{% if order.seller.email %}
{{ order.seller.email }}
{% endif %}

Copyright {{ order.seller.name }} All rights reserved.
