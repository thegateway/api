import time
from datetime import datetime
from elasticsearch import Elasticsearch
from gwio.environment import Env

from elasticsearch_mapping.es_mapping_for_bare_product import es_mapping as bare_product_map
from elasticsearch_mapping.es_mapping_for_webshop_product import es_mapping as webshop_product_map

# This script is reindexing product documents from old product indexes to new product indexes, before running the script ensure Env variables are set.

es = Elasticsearch(
    hosts=Env.ELASTICSEARCH_HOST,
    timeout=200
)


def get_old_index_name(alias):
    res = list(es.indices.get(alias).keys())[0]
    return res


webshop_product_alias = f"{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}-webshop_product"
old_webshop_product_index = get_old_index_name(alias=webshop_product_alias)
new_webshop_product_index = f"{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}-webshop_product_v{datetime.now().strftime('%Y%m%d-%H.%M.%S')}"

bare_product_alias = f"{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}-bare_product"
old_bare_product_index = get_old_index_name(alias=bare_product_alias)
new_bare_product_index = f"{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}-bare_product_v{datetime.now().strftime('%Y%m%d-%H.%M.%S')}"


def create_index(index_name, index_map):
    response = es.indices.create(
        index=index_name,
        body=index_map,
    )

    print('Create index response:', response)


def create_product_indexes():
    # Create webshop product index
    create_index(
        index_name=new_webshop_product_index,
        index_map=webshop_product_map
    )

    # Create bare product index
    create_index(
        index_name=new_bare_product_index,
        index_map=bare_product_map
    )


def setup_reindex(new_index, old_index):
    reindex_query = {
        "source": {
            "index": old_index
        },
        "dest": {
            "index": new_index
        }
    }
    res = es.reindex(
        body=reindex_query,
    )
    print('response:', res)


def setup_reindex_products():
    # Setup bare product index alias
    setup_reindex(
        new_index=new_bare_product_index,
        old_index=old_bare_product_index
    )
    setup_reindex(
        new_index=new_webshop_product_index,
        old_index=old_webshop_product_index
    )


def setup_index_alias(alias, new_index, old_index):
    aliases_query = {
        "actions": [
            {"add": {"index": new_index, "alias": alias}},
            {"remove": {"index": old_index, "alias": alias}}
        ]
    }

    res = es.indices.update_aliases(aliases_query)
    # print out the response:
    print('Setup index alias response:', res)


def setup_product_indexes_aliases():
    # Setup bare product index alias
    setup_index_alias(
        alias=bare_product_alias,
        new_index=new_bare_product_index,
        old_index=old_bare_product_index
    )
    # Setup webshop product index alias
    setup_index_alias(
        alias=webshop_product_alias,
        new_index=new_webshop_product_index,
        old_index=old_webshop_product_index
    )


def delete_old_index():
    es.indices.refresh(new_bare_product_index)
    es.indices.refresh(new_webshop_product_index)
    bare_products_count = es.count(index=new_bare_product_index)['count']
    webshop_products_count = es.count(index=new_webshop_product_index)['count']
    if bare_products_count > 20000 and webshop_products_count > 20000:
        es.indices.delete(index=old_bare_product_index)
        es.indices.delete(index=old_webshop_product_index)
        print('################ SUCCESS: PRODUCT MIGRATION TO ES SUCCESSFUL!! ####################')
    else:
        # Revert bare product index alias
        setup_index_alias(
            alias=bare_product_alias,
            new_index=old_bare_product_index,
            old_index=new_bare_product_index
        )
        # Revert webshop product index alias
        setup_index_alias(
            alias=webshop_product_alias,
            new_index=old_webshop_product_index,
            old_index=new_webshop_product_index
        )
        es.indices.delete(index=new_bare_product_index)
        es.indices.delete(index=new_webshop_product_index)
        print('################ FAILED: PRODUCT MIGRATION TO ES FAILED!! ####################')


def main():
    create_product_indexes()
    setup_reindex_products()
    setup_product_indexes_aliases()
    time.sleep(30)
    delete_old_index()


if __name__ == '__main__':
    main()
