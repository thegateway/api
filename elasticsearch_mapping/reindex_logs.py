from datetime import datetime
from elasticsearch import (
    Elasticsearch,
)
from gwio.environment import Env

from elasticsearch_mapping.es_mapping_for_log_events import es_mapping as log_map

# This script is reindexing log documents from old log index to new log index, before running the script ensure Env variables are set.

es = Elasticsearch(
    hosts=Env.ELASTICSEARCH_HOST
)


def get_old_index_name(alias):
    res = list(es.indices.get(alias).keys())[0]
    return res


log_alias = f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs"
old_log_index = get_old_index_name(alias=log_alias)
new_log_index = f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs_v{datetime.now().strftime('%Y%m%d-%H.%M.%S')}"


def create_log_index():
    response = es.indices.create(
        index=new_log_index,
        body=log_map,
    )

    print('Create log index response:', response)


def reindex_logs():
    reindex_query = {
        "source": {
            "index": old_log_index
        },
        "dest": {
            "index": new_log_index
        }
    }

    response = es.reindex(
        body=reindex_query
    )
    print('Reindex logs response:', response)
    return response


def setup_log_index_alias():
    aliases_query = {
        "actions": [
            {"add": {"index": new_log_index, "alias": log_alias}},
            {"remove_index": {"index": old_log_index}}
        ]
    }

    res = es.indices.update_aliases(aliases_query)
    print('Setup log index alias response:', res)


def main():
    create_log_index()
    response = reindex_logs()
    if not response['failures']:
        setup_log_index_alias()


if __name__ == '__main__':
    main()
