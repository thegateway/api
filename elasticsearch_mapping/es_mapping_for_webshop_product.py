# coding=utf-8
# es_mapping dictionary that contains the settings and mappings schema for a new Elasticsearch webshop product index:

es_mapping = {
    "settings": {
        "analysis": {
            "analyzer": {
                "long_text_analyzer": {
                    "char_filter": ["html_strip"],
                    "tokenizer": "whitespace",
                    "filter": [
                        "lowercase",
                        "polish_stem",
                        "polish_stop",
                        "asciifolding"
                    ]
                },
                "sort_text_analyzer": {
                    "type": "custom",
                    "char_filter": ["html_strip"],
                    "tokenizer": "whitespace",
                    "filter": [
                        "lowercase",
                        "asciifolding"
                    ]
                },
            }
        },
        "number_of_shards": 3,
        "number_of_replicas": 1,
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "activated": {
                "type": "date"
            },
            "archived": {
                "type": "date"
            },
            "brief": {
                "type": "text",
                "analyzer": "sort_text_analyzer",
                "fields": {
                    "folded": {
                        "type": "text",
                        "analyzer": "long_text_analyzer"
                    }
                }
            },
            "brand_name": {
                "type": "text",
                "analyzer": "polish",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "search": {
                        "type": "search_as_you_type",
                        "analyzer": "sort_text_analyzer"
                    }
                }
            },
            "created": {
                "type": "date"
            },
            "data": {
                "type": "object",
                "dynamic": False,
                "properties": {
                    "variants": {
                        "type": "object",
                        "dynamic": False,
                        "properties": {
                            "size": {
                                "type": "text",
                                "analyzer": "long_text_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "color": {
                                "type": "text",
                                "analyzer": "long_text_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "wielkość / średnica": {
                                "type": "text",
                                "analyzer": "long_text_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "waga": {
                                "type": "text",
                                "analyzer": "long_text_analyzer",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "desc": {
                "type": "text",
                "analyzer": "sort_text_analyzer",
                "fields": {
                    "folded": {
                        "type": "text",
                        "analyzer": "long_text_analyzer"
                    }
                }
            },
            "for_sale": {
                "type": "boolean"
            },
            "guid": {
                "type": "keyword",
            },
            "images": {
                "type": "keyword",
            },
            "name": {
                "type": "text",
                "analyzer": "polish",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "search": {
                        "type": "search_as_you_type",
                        "analyzer": "sort_text_analyzer"
                    }
                }
            },
            "owner_guid": {
                "type": "keyword",
            },
            "price": {
                "properties": {
                    "currency": {
                        "type": "keyword"
                    },
                    "final": {
                        "properties": {
                            "unit": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "currency": {
                                        "type": "keyword",
                                    },
                                    "vat": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat0": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat_percent": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    }
                                }
                            },
                            "total": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "currency": {
                                        "type": "keyword",
                                    },
                                    "vat": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat0": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat_percent": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    }
                                }
                            }
                        }
                    },
                    "original": {
                        "properties": {
                            "campaign_modifier": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "desc": {
                                        "type": "text",
                                        "index": False
                                    },
                                    "fixed_modifiers": {
                                        "type": "object",
                                        "enabled": False,
                                        "dynamic": False
                                    },
                                    "method": {
                                        "type": "keyword"
                                    }
                                }
                            },
                            "unit": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "currency": {
                                        "type": "keyword",
                                    },
                                    "vat": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat0": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat_percent": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    }
                                }
                            },
                            "total": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "currency": {
                                        "type": "keyword",
                                    },
                                    "vat": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat0": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat_percent": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    }
                                }
                            }
                        }
                    },
                    "vat_class": {
                        "type": "scaled_float",
                        "scaling_factor": 100
                    }
                }
            },
            "pricing": {
                "type": "keyword",
            },
            "shop_guid": {
                "type": "keyword",
            },
            "shop_name": {
                "type": "text",
                "analyzer": "sort_text_analyzer",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "folded": {
                        "type": "text",
                        "analyzer": "long_text_analyzer"
                    }
                }
            },
            "size": {
                "properties": {
                    "width": {
                        "type": "scaled_float",
                        "scaling_factor": 100
                    },
                    "height": {
                        "type": "scaled_float",
                        "scaling_factor": 100
                    },
                    "length": {
                        "type": "scaled_float",
                        "scaling_factor": 100
                    }
                }
            },
            "sku": {
                "type": "keyword",
            },
            "suppliers": {
                "type": "keyword",
            },
            "stock_level": {
                "type": "scaled_float",
                "scaling_factor": 100
            },
            "tag_guids": {
                "type": "keyword",
            },
            "tags": {
                "properties": {
                    "type": {
                        "type": "keyword"
                    },
                    "guid": {
                        "type": "keyword"
                    },
                    "name": {
                        "type": "text",
                        "analyzer": "sort_text_analyzer",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            },
                            "folded": {
                                "type": "text",
                                "analyzer": "long_text_analyzer"
                            }
                        }
                    }
                }
            },
            "timestamps": {
                "properties": {
                    "created": {
                        "type": "date"
                    }
                }
            },
            "type": {
                "type": "keyword",
            },
            "updated": {
                "type": "date"
            },
            "vat": {
                "type": "keyword",
            },
            "weight": {
                "type": "scaled_float",
                "scaling_factor": 100
            },
        }
    },
}
