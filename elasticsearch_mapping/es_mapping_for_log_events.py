# coding=utf-8
# es_mapping dictionary that contains the settings and mappings schema for a new Elasticsearch log index:
# Objects fields are mapped based on Object.Schema. So any changes on Object.Schema's existing field need to be reflect on mappings schema as well.

es_mapping = {
    "settings": {
        "index.mapping.total_fields.limit": 2000,
        "number_of_shards": 3,
        "number_of_replicas": 1,
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "audit": {
                "type": "boolean"
            },
            "event_type": {
                "type": "keyword",
            },
            "obj": {
                "properties": {
                    "Bootstrap": {
                        "properties": {
                            "vertical": {
                                "type": "keyword",
                            }
                        }
                    },
                    "Brand": {
                        "properties": {
                            "contact": {
                                "properties": {
                                    "address": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "email": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "phone_number": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            }
                        }
                    },
                    "Location": {
                        "properties": {
                            "archived": {
                                "type": "date"
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "latitude": {
                                "type": "float"
                            },
                            "longitude": {
                                "type": "float"
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "parent": {
                                "type": "keyword",
                            },
                            "tags": {
                                "type": "keyword",
                            }
                        }
                    },
                    "Order": {
                        "properties": {
                            "available_actions": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "billing_address": {
                                "properties": {
                                    "country_code": {
                                        "type": "keyword",
                                    },
                                    "email": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "phone": {
                                        "type": "keyword",
                                    },
                                    "postal_code": {
                                        "type": "keyword",
                                    },
                                    "street_address": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    }
                                }
                            },
                            "buyer_identity_id": {
                                "type": "keyword",
                            },
                            "coupon": {
                                "properties": {
                                    "benefit": {
                                        "properties": {
                                            "amount": {
                                                "type": "scaled_float",
                                                "scaling_factor": 100
                                            },
                                            "desc": {
                                                "type": "text",
                                                "index": False
                                            },
                                            "method": {
                                                "type": "keyword",
                                            }
                                        }
                                    },
                                    "conditions": {
                                        "type": "object",
                                        "enabled": False
                                    },
                                    "guid": {
                                        "type": "keyword",
                                    },
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "used": {
                                        "type": "boolean"
                                    },
                                    "images": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "_current_state": {
                                "type": "keyword",
                            },
                            "customer": {
                                "properties": {
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "phone": {
                                        "type": "keyword",
                                    },
                                    "pin": {
                                        "type": "keyword",
                                    },
                                    "business_id": {
                                        "properties": {
                                            "id": {
                                                "type": "keyword",
                                            },
                                            "country": {
                                                "type": "keyword",
                                            }
                                        }
                                    }
                                }
                            },
                            "flags": {
                                "properties": {
                                    "backordered": {
                                        "type": "boolean"
                                    },
                                    "delivered": {
                                        "type": "boolean"
                                    },
                                    "invalid": {
                                        "type": "boolean"
                                    },
                                    "paid": {
                                        "type": "boolean"
                                    },
                                    "pickup": {
                                        "type": "boolean"
                                    },
                                    "prepayment": {
                                        "type": "boolean"
                                    },
                                    "refunded": {
                                        "type": "boolean"
                                    },
                                    "require_review": {
                                        "type": "boolean"
                                    },
                                    "require_strong_authentication": {
                                        "type": "boolean"
                                    }
                                }
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "product_lines": {
                                "type": "nested",
                                "properties": {
                                    "guid": {
                                        "type": "keyword",
                                    },
                                    "shop_guid": {
                                        "type": "keyword",
                                    },
                                    "images": {
                                        "type": "keyword",
                                    },
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "qty": {
                                        "type": "long"
                                    },
                                    "type": {
                                        "type": "keyword",
                                    },
                                    "backordered": {
                                        "type": "boolean"
                                    },
                                }
                            },
                            "packages": {
                                "properties": {
                                    "guid": {
                                        "type": "keyword",
                                    },
                                    "contents": {
                                        "properties": {
                                            "product_guid": {
                                                "type": "keyword",
                                            },
                                            "qty": {
                                                "type": "long",
                                            },
                                        }
                                    }
                                }
                            },
                            "payments": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "currency": {
                                        "type": "keyword",
                                    },
                                    "_current_state": {
                                        "type": "keyword",
                                    },
                                    "datetime": {
                                        "type": "date"
                                    },
                                    "guid": {
                                        "type": "keyword",
                                    },
                                    "payment_type": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "payment_link": {
                                "type": "keyword",
                            },
                            "payment_cancelled_link": {
                                "type": "keyword",
                            },
                            "payment_success_link": {
                                "type": "keyword",
                            },
                            "payment_method": {
                                "properties": {
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "type": {
                                        "type": "keyword",
                                    },
                                    "guid": {
                                        "type": "keyword",
                                    },
                                }
                            },
                            "payment_method_guid": {
                                "type": "keyword",
                            },
                            "product_returns": {
                                "type": "nested",
                                "properties": {
                                    "guid": {
                                        "type": "keyword",
                                    },
                                    "lines": {
                                        "type": "nested",
                                        "properties": {
                                            "product_guid": {
                                                "type": "keyword",
                                            },
                                            "shop_guid": {
                                                "type": "keyword",
                                            },
                                            "received": {
                                                "properties": {
                                                    "qty": {
                                                        "type": "long",
                                                    }
                                                }
                                            },
                                            "qty": {
                                                "type": "long",
                                            }
                                        }
                                    },
                                    "reason": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    }
                                }
                            },
                            "reference": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "shipping_address": {
                                "properties": {
                                    "country_code": {
                                        "type": "keyword",
                                    },
                                    "email": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "phone": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "postal_code": {
                                        "type": "keyword",
                                    },
                                    "street_address": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    }
                                }
                            },
                            "shop_guids": {
                                "type": "keyword",
                            },
                            "summary": {
                                "properties": {
                                    "currency": {
                                        "type": "keyword",
                                    },
                                    "price": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vat0_price": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "vats": {
                                        "properties": {
                                            "amount": {
                                                "type": "scaled_float",
                                                "scaling_factor": 100
                                            },
                                            "vat": {
                                                "type": "scaled_float",
                                                "scaling_factor": 100
                                            }
                                        }
                                    },
                                    "shops": {
                                        "properties": {
                                            "currency": {
                                                "type": "keyword",
                                            },
                                            "shop_guid": {
                                                "type": "keyword",
                                            },
                                            "price": {
                                                "type": "scaled_float",
                                                "scaling_factor": 100
                                            },
                                            "vat0_price": {
                                                "type": "scaled_float",
                                                "scaling_factor": 100
                                            },
                                            "vats": {
                                                "properties": {
                                                    "amount": {
                                                        "type": "scaled_float",
                                                        "scaling_factor": 100
                                                    },
                                                    "vat": {
                                                        "type": "scaled_float",
                                                        "scaling_factor": 100
                                                    }
                                                }
                                            },
                                        }
                                    },
                                }
                            },
                            "_transition_history": {
                                "properties": {
                                    "action": {
                                        "type": "keyword",
                                    },
                                    "timestamp": {
                                        "type": "date"
                                    },
                                    "state": {
                                        "type": "keyword",
                                    },
                                }
                            },
                        }
                    },
                    "PaymentMethod": {
                        "properties": {
                            "archived": {
                                "type": "date"
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "logo": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "type": {
                                "type": "keyword",
                            }
                        }
                    },
                    "Pricing": {
                        "properties": {
                            "guid": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "owner": {
                                "type": "keyword",
                            }
                        }
                    },
                    "Product": {
                        "properties": {
                            "activated": {
                                "type": "date"
                            },
                            "base_price_type": {
                                "type": "keyword",
                            },
                            "created": {
                                "type": "date"
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "pricing": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "owner_guid": {
                                "type": "keyword",
                            },
                            "sku": {
                                "type": "keyword",
                            },
                            "tag_guids": {
                                "type": "keyword",
                            },
                            "type": {
                                "type": "keyword",
                            },
                        }
                    },
                    "Promotion": {
                        "properties": {
                            "benefit": {
                                "properties": {
                                    "amount": {
                                        "type": "scaled_float",
                                        "scaling_factor": 100
                                    },
                                    "desc": {
                                        "type": "text",
                                        "index": False
                                    },
                                    "method": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            }
                        }
                    },
                    "Residence": {
                        "properties": {
                            "address": {
                                "properties": {
                                    "postal_code": {
                                        "type": "keyword",
                                    },
                                    "street_address": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    }
                                }
                            },
                            "archived": {
                                "type": "date"
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "house_type": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "owner_identity_id": {
                                "type": "keyword",
                            },
                            "ownership": {
                                "type": "keyword",
                            },
                            "year_built": {
                                "type": "short"
                            }
                        }
                    },
                    "Tag": {
                        "properties": {
                            "duration": {
                                "properties": {
                                    "end": {
                                        "type": "date"
                                    },
                                    "start": {
                                        "type": "date"
                                    }
                                }
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "item_count": {
                                "type": "long"
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "owner": {
                                "type": "keyword",
                            },
                            "parent_guid": {
                                "type": "keyword",
                            },
                            "product_guids": {
                                "type": "keyword",
                            },
                            "type": {
                                "type": "keyword",
                            }
                        }
                    },
                    "User": {
                        "type": "object",
                        "dynamic": False
                    },
                    "Webshop": {
                        "properties": {
                            "addresses": {
                                "properties": {
                                    "city": {
                                        "type": "keyword",
                                    },
                                    "country": {
                                        "type": "keyword",
                                    },
                                    "label": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "name": {
                                        "type": "keyword",
                                    },
                                    "street": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "zipcode": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "business_id": {
                                "type": "keyword",
                            },
                            "emails": {
                                "properties": {
                                    "email": {
                                        "type": "text",
                                        "fields": {
                                            "keyword": {
                                                "type": "keyword",
                                                "ignore_above": 256
                                            }
                                        }
                                    },
                                    "label": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "enabled": {
                                "type": "boolean"
                            },
                            "guid": {
                                "type": "keyword",
                            },
                            "locations": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "logo": {
                                "type": "keyword",
                            },
                            "name": {
                                "type": "keyword",
                            },
                            "payment_method_guids": {
                                "type": "keyword",
                            },
                            "person_in_charge": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "phones": {
                                "properties": {
                                    "label": {
                                        "type": "keyword",
                                    },
                                    "phone": {
                                        "type": "keyword",
                                    }
                                }
                            },
                            "shorthand_name": {
                                "type": "keyword",
                            },
                            "timezone": {
                                "type": "keyword",
                            }
                        }
                    },
                    "WebshopProduct": {
                        "properties": {
                            "archived": {
                                "type": "date"
                            },
                            "for_sale": {
                                "type": "boolean"
                            },
                            "pricing_guid": {
                                "type": "keyword",
                            },
                            "product_guid": {
                                "type": "keyword",
                            },
                            "stock_level": {
                                "type": "long"
                            },
                            "webshop_guid": {
                                "type": "keyword",
                            }
                        }
                    }
                }
            },
            "object_id": {
                "type": "keyword",
            },
            "object_type": {
                "type": "keyword",
            },
            "request": {
                "properties": {
                    "guid": {
                        "type": "keyword",
                    },
                    "method": {
                        "type": "keyword",
                    },
                    "remote_ips": {
                        "type": "keyword",
                    },
                    "url_path": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    },
                    "url_root": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    },
                    "user_agent": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    }
                }
            },
            "request_guid": {
                "type": "keyword",
            },
            "response": {
                "properties": {
                    "status_code": {
                        "type": "short"
                    }
                }
            },
            "subject_id": {
                "type": "keyword",
            },
            "subject_type": {
                "type": "keyword",
            },
            "timestamp": {
                "type": "date"
            }
        }
    }
}
