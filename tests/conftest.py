import time
import envs
import pytest
from .base.custom_webdriver import CustomWebDriver
from .pages.dashboard.base_page import BasePage
from .pages.dashboard.login_page import LoginPage
from .pages.sma_frontend.login_page import LoginPage as LoginPageFront
from .utilities import custom_log as cl
import logging
import os

log = cl.custom_logger(logging.DEBUG)

browser_name = envs.env('BROWSER_NAME')
dashboard_url = envs.env('DASHBOARD_URL')
headless_mode = envs.env('HEADLESS', var_type='boolean')


def get_driver(browser, url, headless, mobile=False):
    wd = CustomWebDriver(browser, url, headless, mobile)
    driver = wd.get_custom_web_driver()
    return driver


@pytest.fixture(scope='class')
def class_level_setup():
    log.info(
        f'Running class level setup, browser : {envs.env("BROWSER_NAME")}, url : {envs.env("DASHBOARD_URL")}')

    driver = get_driver(browser_name, dashboard_url, headless_mode)

    yield driver

    driver.quit()
    log.info('Running class level tearDown')


@pytest.fixture(scope=envs.env('LOGIN_SETTINGS'))
def one_time_setup():
    log.info('Running one time setup')
    driver = get_driver(browser_name, dashboard_url, headless_mode)
    lp = LoginPage(driver)
    lp.login(envs.env('EMAIL'), envs.env('PASSWORD'))
    bp = BasePage(driver)
    time.sleep(8)
    bp.handle_error_popups()
    bp.change_language()
    yield driver

    driver.quit()
    log.info('Running one time tearDown')


@pytest.fixture(scope=envs.env('LOGIN_SETTINGS'))
def one_time_setup_mobile():
    account = dict(
        email=envs.env('EMAIL'),
        password=envs.env('PASSWORD')
    )
    log.info('Running one time setup')
    driver = get_driver(browser_name, dashboard_url, headless_mode, mobile=True)
    lp = LoginPageFront(driver)
    lp.login(account)

    yield driver

    driver.quit()
    log.info('Running one time tearDown')


def project_path():
    tests_dir = f'{os.sep}tests'
    path = os.getcwd()
    while tests_dir in path:
        path, _ = os.path.split(path)
    return path
