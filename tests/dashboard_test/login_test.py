from tests.pages.dashboard.login_page import LoginPage
from faker import Faker
import pytest
import envs


@pytest.mark.usefixtures('class_level_setup')
class TestLogin:
    fake = Faker()
    ########### Delivery Method Test Data ##############
    # ============= Valid login==========================
    valid_user_id = envs.env('EMAIL')
    valid_user_pwd = envs.env('PASSWORD')

    # ============= Invalid login==========================
    invalid_user_id = fake.email()
    invalid_user_pwd = fake.password(length=12)

    ###############################################

    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, class_level_setup):
        self.lp = LoginPage(class_level_setup)

    def test_1_verify_login_page_title(self):
        assert self.lp.verify_login_page_title()

    def test_2_registration_link(self):
        assert self.lp.verify_registration_link_working()

    def test_3_verification_link(self):
        assert self.lp.verify_verification_link_working()

    def test_4_forgot_password_link(self):
        assert self.lp.verify_forgot_password_link_working()

    def test_5_failed_login(self):
        self.lp.login(self.invalid_user_id, self.invalid_user_pwd)
        assert self.lp.verify_logging_is_failed()

    def test_6_valid_login(self):
        self.lp.login(self.valid_user_id, self.valid_user_pwd)
        assert self.lp.verify_logging_is_successful()
