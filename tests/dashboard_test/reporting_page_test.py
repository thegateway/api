from tests.pages.dashboard.reporting_page import ReportingPage
import pytest


@pytest.mark.usefixtures('one_time_setup')
class TestReportPage:

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.rp = ReportingPage(one_time_setup)

    def test_1_reporting_page_title(self):
        assert self.rp.verify_reporting_page_title()

    def test_2_reporting_page_header(self):
        assert self.rp.verify_reporting_page_header()
