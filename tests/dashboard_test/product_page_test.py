from tests.pages.dashboard.products_page import ProductsPage
from tests.test_data.generate_test_data import generate_product_info, generate_category_info, generate_campaign_info
import pytest


@pytest.mark.usefixtures('one_time_setup')
class TestProductsPage:

    product_info = generate_product_info()
    edited_product_info = generate_product_info()
    stock_lvl_product_info = generate_product_info()

    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, one_time_setup):
        self.pp = ProductsPage(one_time_setup)

    def test_1_verify_product_page_title(self):
        assert self.pp.verify_products_page_title()

    def test_2_verify_product_page_header(self):
        assert self.pp.verify_product_page_header()

    def test_3_verify_product_page_create_new_product(self):
        assert self.pp.verify_product_page_new_product(self.product_info)

    def test_4_verify_product_edit_existing_product(self):
        assert self.pp.verify_product_page_edit_product(self.product_info['name'], self.edited_product_info)

    @pytest.mark.skip(reason="Campaigns and categories doesn't work at the moment")
    def test_5_verify_product_is_added_to_campaign(self):
        assert self.pp.verify_product_added_to_campaign(self.edited_product_info, generate_campaign_info())

    @pytest.mark.skip(reason="Campaigns and categories doesn't work at the moment")
    def test_6_verify_campaign_is_removed_from_product(self):
        assert self.pp.verify_campaign_is_deleted_from_product(self.edited_product_info['name']) is False

    @pytest.mark.skip(reason="Campaigns and categories doesn't work at the moment")
    def test_7_verify_product_category_is_changed(self):
        assert self.pp.verify_product_category_is_changed(generate_product_info(), generate_category_info())

    @pytest.mark.skip(reason="Campaigns and categories doesn't work at the moment")
    def test_8_verify_product_category_is_deleted(self):
        assert self.pp.verify_category_is_deleted_from_product() is False

    def test_9_verify_delete_product(self):
        assert self.pp.verify_delete_product(self.edited_product_info['name']) is False

    def test_10_verify_inventory_and_stock_lvl(self):
        assert self.pp.verify_inventory_and_stock_lvl(self.stock_lvl_product_info)

    def test_11_verify_stock_lvl_is_changed(self):
        assert self.pp.verify_inventory_and_stock_lvl_is_changed(self.stock_lvl_product_info)
