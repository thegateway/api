from tests.pages.dashboard.home_page import HomePage
import pytest


@pytest.mark.usefixtures('one_time_setup')
class TestHomePage:

    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, one_time_setup):
        self.dp = HomePage(one_time_setup)

    def test_1_verify_homePage_title(self):
        assert self.dp.verify_homePage_title()

    def test_2_homePage_header(self):
        assert self.dp.verify_homePage_header()
