from tests.pages.dashboard.settings.delivery_options_page import DeliveryOptionsPage
import pytest
from tests.test_data.generate_test_data import generate_delivery_info


@pytest.fixture(scope='class')
def delivery_options_class_setup(one_time_setup):
    dop = DeliveryOptionsPage(one_time_setup)
    dop.click_nav_menu_buttons(dop.nav_settings_delivery_options_button)


@pytest.mark.usefixtures('one_time_setup', 'delivery_options_class_setup')
class TestDeliveryOptionPage:

    delivery_info = generate_delivery_info()
    edited_delivery_info = generate_delivery_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.dop = DeliveryOptionsPage(one_time_setup)

    def test_1_delivery_option_page_title(self):
        assert self.dop.verify_delivery_options_page_title()

    def test_2_delivery_option_page_header(self):
        assert self.dop.verify_delivery_options_page_header()

    def test_3_delivery_option_create_new_option(self):
        assert self.dop.verify_add_new_delivery_option(self.delivery_info)

    def test_4_delivery_option_edit_option(self):
        assert self.dop.verify_edit_delivery_option(self.delivery_info['name'], self.edited_delivery_info)

    def test_4_delete_delivery_option(self):
        assert self.dop.verify_delete_delivery_option(self.edited_delivery_info['name']) is False

