from tests.pages.dashboard.settings.categories_page import CategoriesPage
import pytest
from tests.test_data.generate_test_data import generate_category_info


@pytest.fixture(scope='class')
def categories_class_setup(one_time_setup):
    cp = CategoriesPage(one_time_setup)
    cp.click_nav_menu_buttons(cp.nav_settings_categories_button)


@pytest.mark.usefixtures('one_time_setup', 'categories_class_setup')
class TestCategoryPage:

    category_info = generate_category_info()
    edited_category_info = generate_category_info()
    sub_category_info = generate_category_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.cp = CategoriesPage(one_time_setup)

    def test_1_verify_category_page_header(self):
        assert self.cp.verify_categories_page_header()

    def test_2_verify_category_page_title(self):
        assert self.cp.verify_categories_page_title()

    def test_3_verify_create_new_category(self):
        assert self.cp.verify_add_new_category(self.category_info)

    def test_4_verify_edit_category(self):
        assert self.cp.verify_edit_category(self.category_info['name'], self.edited_category_info)

    def test_5_verify_new_sub_category(self):
        assert self.cp.verify_new_sub_category(self.sub_category_info['name'])

    def test_6_verify_delete_sub_category(self):
        assert self.cp.verify_delete_category(self.sub_category_info['name']) is False

    def test_7_verify_delete_category(self):
        assert self.cp.verify_delete_category(self.edited_category_info['name']) is False
