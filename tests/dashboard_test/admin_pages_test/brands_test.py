from tests.pages.dashboard.settings.brands_page import BrandsPage
import pytest
from tests.test_data.generate_test_data import generate_brand_info


@pytest.fixture(scope='class')
def brands_class_setup(one_time_setup):
    bp = BrandsPage(one_time_setup)
    bp.click_nav_menu_buttons(bp.nav_settings_brands_button)


@pytest.mark.usefixtures('one_time_setup', 'brands_class_setup')
class TestBrandsPage:
    brand_info = generate_brand_info()
    edited_brand_info = generate_brand_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.bp = BrandsPage(one_time_setup)

    def test_1_verify_brands_page_header(self):
        assert self.bp.verify_brand_page_header()

    def test_2_verify_brands_page_title(self):
        assert self.bp.verify_brand_page_title()

    def test_3_brands_page_create_new_brand(self):
        assert self.bp.verify_add_new_brand(self.brand_info)

    def test_4_brands_page_edit_brands(self):
        assert self.bp.verify_edit_brand(self.edited_brand_info, self.brand_info['dialog_name'])

    #todo change brand_info to edited_brand_info when bug is fixed
    def test_5_delete_brand(self):
        assert self.bp.verify_delete_brand(self.brand_info['dialog_name']) is False
