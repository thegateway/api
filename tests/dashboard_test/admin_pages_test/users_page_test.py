from tests.pages.dashboard.settings.users_page import UsersManagementPage
import pytest


@pytest.fixture(scope='class')
def user_management_class_setup(one_time_setup):
    ump = UsersManagementPage(one_time_setup)
    ump.click_nav_menu_buttons(ump.nav_settings_users_button)


@pytest.mark.usefixtures('one_time_setup', 'user_management_class_setup')
class TestUsersManagement:
    ########### Users management Test Data ##############
    search_query = 'nahian@gmail.com'

    #####################################################

    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, one_time_setup):
        self.ump = UsersManagementPage(one_time_setup)

    def test_1_verify_users_management_page_title(self):
        assert self.ump.verify_users_management_page_title()

    def test_2_verify_users_management_page_header(self):
        assert self.ump.verify_users_management_page_header()

    # def test_3_verify_search_field_working(self):
    #     assert self.ump.verify_search_field_working(self.search_query)
