from tests.pages.dashboard.settings.retailer import RetailerPage
import pytest
from tests.test_data.generate_test_data import generate_retailer_info


@pytest.fixture(scope='class')
def retailer_class_setup(one_time_setup):
    rp = RetailerPage(one_time_setup)
    rp.click_nav_menu_buttons(rp.nav_settings_retailer_button)


@pytest.mark.skip(reason="Retailers doesn't load at the moment")
@pytest.mark.usefixtures('one_time_setup', 'retailer_class_setup')
class TestRetailerPage:

    retailer_info = generate_retailer_info()
    edited_retailer_info = generate_retailer_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.rp = RetailerPage(one_time_setup)

    def test_1_verify_retailer_page_title(self):
        assert self.rp.verify_retailer_page_title()

    def test_2_verify_retailer_header(self):
        assert self.rp.verify_retailer_page_header()

    def test_3_verify_create_new_retailer(self):
        assert self.rp.verify_add_new_retailer(self.retailer_info)

    def test_4_verify_edit_existing_retailer(self):
        assert self.rp.verify_edit_retailer(self.edited_retailer_info, self.retailer_info)

    def test_5_verify_delete_existing_retailer(self):
        assert self.rp.verify_delete_retailer(self.edited_retailer_info['name']) is False
