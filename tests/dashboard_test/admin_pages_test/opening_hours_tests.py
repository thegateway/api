from tests.pages.dashboard.settings.opening_hours import OpeningHoursPage
import pytest
from tests.test_data.generate_test_data import generate_opening_hours_info


@pytest.fixture(scope='class')
def open_hours_class_setup(one_time_setup):
    ohp = OpeningHoursPage(one_time_setup)
    ohp.click_nav_menu_buttons(ohp.nav_settings_opening_hours_button)

@pytest.mark.usefixtures('one_time_setup', 'open_hours_class_setup')
class TestOpeningHoursPage:
    opening_hours = generate_opening_hours_info()
    edited_hours = generate_opening_hours_info()
    exp_hours = generate_opening_hours_info()
    edited_exp_hours = generate_opening_hours_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.ohp = OpeningHoursPage(one_time_setup)

    def test_1_verify_opening_hours_page_header(self):
        assert self.ohp.verify_brand_page_header()

    def test_2_verify_opening_hours_page_title(self):
        assert self.ohp.verify_brand_page_title()

    def test_3_verify_opening_hours_create_new(self):
        assert self.ohp.verify_new_times_is_Added(self.opening_hours)

    def test_4_verify_opening_hours_is_modified(self):
        assert self.ohp.verify_time_is_edited(self.opening_hours, self.edited_hours)

    def test_5_verify_opening_hours_is_deleted(self):
        assert self.ohp.verify_time_is_deleted(self.edited_hours)

    @pytest.mark.skip(reason='date field needs to be fixed')
    def test_6_verify_exp_hours_is_created(self):
        assert self.ohp.verify_experimental_time_is_added(self.exp_hours)

    @pytest.mark.skip(reason='date field needs to be fixed')
    def test_7_verify_exp_hours_is_edited(self):
        assert self.ohp.verify_exp_hours_is_edited(self.exp_hours, self.edited_exp_hours)

    @pytest.mark.skip(reason='date field needs to be fixed')
    def test_8_verify_exp_hours_is_deleted(self):
        assert self.ohp.verify_exp_hours_is_deleted(self.edited_exp_hours)
