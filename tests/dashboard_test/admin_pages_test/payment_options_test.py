from tests.pages.dashboard.settings.payment_options_page import PaymentTypesPage
from tests.test_data.generate_test_data import generate_payment_info
import pytest


@pytest.fixture(scope='class')
def payment_options_class_setup(one_time_setup):
    pop = PaymentTypesPage(one_time_setup)
    pop.click_nav_menu_buttons(pop.nav_settings_payment_options_button)


@pytest.mark.usefixtures('one_time_setup', 'payment_options_class_setup')
class TestPaymentOptionPage:
    payment_info = generate_payment_info()
    edited_payment_info = generate_payment_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.pop = PaymentTypesPage(one_time_setup)

    def test_1_verify_payment_option_page_title(self):
        assert self.pop.verify_payment_types_page_title()

    def test_2_verify_payment_option_page_header(self):
        assert self.pop.verify_payment_types_page_header()

    def test_3_verify_create_new_payment_option(self):
        assert self.pop.verify_add_new_payment(self.payment_info)

    def test_4_verify_payment_option_is_added_to_retailer(self):
        assert self.pop.verify_payment_option_to_retailer(self.payment_info)

    def test_5_verify_payment_option_is_removed_from_retailer(self):
        assert self.pop.verify_delete_payment_option_retailer(self.payment_info) is False

    def test_4_verify_edit_existing_payment_option(self):
        assert self.pop.verify_edit_payment(self.payment_info['name'], self.edited_payment_info)

    def test_5_verify_delete_existing_payment_option(self):
        assert self.pop.verify_delete_payment(self.edited_payment_info['name']) is False
