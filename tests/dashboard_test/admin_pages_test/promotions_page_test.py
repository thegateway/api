from tests.pages.dashboard.settings.promotions_page import PromotionsPage
from tests.test_data.generate_test_data import generate_promotion_info
import pytest


@pytest.fixture(scope='class')
def promotion_page_class_setup(one_time_setup):
    pp = PromotionsPage(one_time_setup)
    pp.click_nav_menu_buttons(pp.nav_settings_promotions_button)


@pytest.mark.usefixtures('one_time_setup', 'promotion_page_class_setup')
class TestPromotionPage:

    promotion_info = generate_promotion_info()
    edited_promotion_info = generate_promotion_info()

    @pytest.fixture(scope="function", autouse=True)
    def class_setup(self, one_time_setup):
        self.pp = PromotionsPage(one_time_setup)

    def test_1_promotion_page_title(self):
        assert self.pp.verify_promotion_page_title()

    def test_2_promotion_page_header(self):
        assert self.pp.verify_promotion_page_header()

    def test_3_verify_new_promotion(self):
        assert self.pp.verify_new_promotion(self.promotion_info)

    def test_4_verify_edit_promotion(self):
        assert self.pp.verify_edit_promotion(self.promotion_info['name'], self.edited_promotion_info)

    def test_5_verify_delete_promotion(self):
        assert self.pp.verify_delete_promotion(self.edited_promotion_info['name']) is False
