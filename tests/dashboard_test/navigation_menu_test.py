from tests.pages.dashboard.base_page import BasePage
import pytest


@pytest.mark.usefixtures('one_time_setup')
class TestBase:

    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, one_time_setup):
        self.bp = BasePage(one_time_setup)

    def test_1_verify_nav_menu_working(self):
        assert self.bp.verify_nav_menu_working()

    def test_2_verify_admin_menu_working(self):
        assert self.bp.verify_nav_settings_menu_working()
