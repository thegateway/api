from tests.pages.dashboard.campaign_page import CampaignPage
import pytest
from tests.test_data.generate_test_data import generate_campaign_info


@pytest.fixture(scope='class')
def campaigns_class_setup(one_time_setup):
    cp = CampaignPage(one_time_setup)
    cp.click_nav_menu_buttons(cp.nav_campaigns_button)


@pytest.mark.usefixtures('one_time_setup', 'campaigns_class_setup')
class TestCampaignPage:
    campaign_info = generate_campaign_info()
    edited_campaign_info = generate_campaign_info()

    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, one_time_setup):
        self.cp = CampaignPage(one_time_setup)

    def test_1_verify_campaign_page_title(self):
        assert self.cp.verify_campaigns_page_title()

    def test_2_campaign_page_header(self):
        assert self.cp.verify_campaigns_page_header()

    def test_3_verify_create_campaign(self):
        assert self.cp.verify_add_new_campaign(self.campaign_info)

    def test_3_verify_edit_campaign(self):
        assert self.cp.verify_edit_campaign(self.campaign_info["name"], self.edited_campaign_info)

    def test_4_verify_delete_campaign(self):
        assert self.cp.verify_delete_campaign(self.edited_campaign_info['name']) is False
