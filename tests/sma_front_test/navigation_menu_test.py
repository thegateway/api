from tests.pages.sma_frontend.base_page import BasePage
import pytest


@pytest.mark.usefixtures('one_time_setup_mobile')
class TestBase:
    @pytest.fixture(scope='function', autouse=True)
    def class_setup(self, one_time_setup_mobile):
        self.bp = BasePage(one_time_setup_mobile)

    @pytest.mark.skip(reason='update needed')
    def test_1_verify_nav_menu_working(self):
        assert self.bp.verify_nav_menu_working()
