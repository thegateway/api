import logging
from random import randrange, randint

import tests.utilities.custom_log as cl
from tests.pages.dashboard.base_page import BasePage
from tests.pages.dashboard.campaign_page import CampaignPage
from tests.pages.dashboard.settings.categories_page import CategoriesPage


class ProductsPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Product Page Locators ##############
    page_title = page_header = 'Products'
    search_field = '//input[@placeholder="Search"]'
    search_result_rows = '//mat-row'
    product_category_dropdown_button = '//mat-select[@aria-label="Category"]'
    product_campaign_dropdown_button = '//mat-select[contains(div/div/span/text(),"by campaign")]'
    product_retailer_dropdown_button = '//mat-select[contains(div/div/span/text(),"by retailer")]'
    products_dropdown_option_lists = '//mat-option/span'
    product_action_button = '//button[contains(span/text(),"Action")]'
    product_action_edit_button = '//button[contains(text(),"Add to product")]'
    product_action_edit_campaign_button = '//button[contains(text(),"Campaign")]'
    product_action_edit_category_button = '//button[contains(text(),"Category")]'
    product_action_category_dropdown_button = '//div[contains(mat-select/div/div/span/text(),"Select category")]'
    product_action_remove_button = '//button[contains(text(),"Remove from product")]'
    product_add_campaign_dropdown_button = '//div[contains(mat-select/div/div/span/text(),"Select")]'
    product_confirm_campaign_button = '//button[contains(span/text(),"Add to campaign")]'
    product_confirm_category_button = '//span[contains(text(),"Add to category")]'

    def select_specific_dropdown_cell(self, name):
        return f'//span[contains(text(),"{name}")]'

    def select_vat_percentage(self, name):
        return f'//mat-option[contains(span/text(),"{name}")]'

    product_item_count_button = '//mat-select[contains(div/div/span/span/text(),"25")]'
    product_latest_products_button = '//mat-chip[contains(text(),"Latest products")]'
    products_unactivated_products_button = '//mat-chip[contains(text(),"Unactivated products")]'
    products_mass_import_button = '//a[contains(span/text(),"Mass import")]'

    products_create_new_product_button = '//a[contains(span/text(),"Create a new product")]'
    product_select_category_dropdown_list = '//mat-select[contains(div/div/span/text(),"Category")]'
    product_select_brand_dropdown_list = '//mat-select[contains(div/div/span/text(),"Brand")]'

    def specific_retail_variant_delete_button(self, name):
        return f'//div[contains(div/text(),"{name}")]/button[contains(span/mat-icon/text(),"close")]'

    def specific_retail_base_price_vat_percent(self, vat_percent):
        return f'//input[@placeholder="Retail price (VAT {vat_percent})"]'

    def select_campaign_dropdown_cell(self, name):
        return f'//span[contains(text(),"{name}")]'

    selected_vat_percent = '//mat-select[@aria-label="VAT percent"]/div/div/span/span'
    product_vat_percentage_dropdown_list = '//*[@aria-label="VAT percent"]'
    product_separate_procurement_price = '//mat-checkbox[contains(label/span/text(),"Add a separte procurement price")]'
    product_separate_price_field = '//input[@placeholder="Cost price (VAT 0%)"]'
    product_image_input_field = '//input[@name="logo" and @accept="image/*"]'
    product_image_delete_button = '//fa[@name="minus-circle"]//i'
    product_image_yes_button = '//button[contains(span/text(),"Yes")]'
    product_variant_field = '//input[@placeholder="Variant name (eg. size, color)"]'
    product_add_variant_button = '//button[contains(span/text(),"Add variant")]'
    product_name = '//input[@placeholder="Name"]'
    product_brief_desc = '//input[@placeholder="Brief description"]'
    product_desc = '//textarea[@placeholder="Description"]'
    product_sku = '//input[@placeholder="SKU"]'
    product_material = '//textarea[@placeholder="Fabric & Care"]'
    product_measurements = '//textarea[@placeholder="Fit & Sizing"]'
    product_policies = '//textarea[@placeholder="Returns & Exchanges"]'
    product_save_button = '//span[contains(text(),"Save")]'
    product_stocklvl = '//input[@placeholder="Stock level"]'

    product_inventory_button = '//a[@href="/products/inventory/list"]'
    product_edit_stock_lvl_confirm_button = '//button[span/fa[@name="check"]]'
    product_stock_lvl_field = '//input[@type="number"]'

    def get_specific_product_stock_lvl_edit_button(self, name):
        return f'//mat-row[mat-cell/div[contains(text(),"{name}")]]//mat-cell[contains(@class, "stock")]//div/a'

    def product_header(self, name):
        return f'//h2[contains(text(),"{name}")]'

    def get_specific_select_button(self, name):
        return f'//mat-row[contains(mat-cell/div0/text(),"{name}")]//mat-checkbox'

    def get_specific_cell(self, name):
        return f'//mat-cell[contains(div/text(),"{name}")]/div'

    def get_specific_category_cell_by_name_and_category(self, name, category_name):
        return f'//mat-row[mat-cell[contains(div/text(),"{name}")]]/mat-cell[contains(div/text(),"c{category_name}")]'

    def get_product_name_and_campaign(self, name):
        return f'//mat-row[mat-cell[contains(div/text(),"{name}")]]/mat-cell[4] | //mat-row[mat-cell[contains(' \
               f'div/text(),"{name}")]]/mat-cell[9]/div '

    def check_is_category_precent(self, name, category):
        return f'//mat-row[mat-cell[contains(div/text(),"{name}")]]/mat-cell[8]/div[contains(text(),"{category}")]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    def check_if_cammpaign_present(self, name, campaign):
        return f'//mat-row[mat-cell[contains(div/text(),"{name}")]]/mat-cell[9]/div[contains(text(),"{campaign}")]'

    def get_specific_stock_lvl(self, name, stock_lvl):
        return f'//mat-row[mat-cell/div[contains(text(),"{name}")]]//mat-cell[contains(@class, "stock")]//div/div[' \
               f'contains(text(),"{stock_lvl}")] '

    product_page_edit_button = '//button[contains(text(),"Edit")]'
    product_page_delete_button = '//button[contains(text(),"Delete")]'

    product_first_name_and_category = '//mat-row[1]/mat-cell[4]/div | //mat-row[1]/mat-cell[8]/div'
    product_actions_delete_category_dropdown_button = '//mat-select[contains(div/div/span/text(),"Select category")]'
    product_actions_delete_campaign_dropdown_button = '//mat-select[contains(div/div/span/text(),"Select campaign")]'
    remove_from_campaign_button = '//button[contains(span/text(),"Remove from campaign")]'
    remove_from_category_button = '//button[contains(span/text(),"Remove from category")]'
    paginator_next_page_button = '(//button[@aria-label="Next page"])[1]'
    paginator_previous_page_button = '(//button[@aria-label="Previous page"])[1]'
    paginator_counter_label = '(//div[@class="mat-paginator-range-label"])[1]'
    product_name_cell = '//app-list-table/mat-table/mat-row[1]/mat-cell[5]/div'

    ###############################################

    def submit_search_query(self, search_query):
        self.submit_input_keys(search_query, self.search_field)

    def verify_products_page_title(self):
        self.click_nav_menu_buttons(self.nav_products_button)
        return self.verify_page_title(self.page_title)

    def verify_product_page_header(self):
        return self.verify_page_header(self.page_header)

    def create_new_product(self, product_info):
        self.click_nav_menu_buttons(self.nav_products_button)
        self.wait_for_element(self.products_create_new_product_button).click()
        self.input_keys(product_info['name'], self.product_name)
        self.input_keys(product_info['brief_desc'], self.product_brief_desc)
        self.input_keys(product_info['description'], self.product_desc)
        self.input_keys(product_info['sku'], self.product_sku)
        self.input_keys(product_info['fabric'], self.product_material)
        self.input_keys(product_info['fit_sizing'], self.product_measurements)
        self.input_keys(product_info['return_exchanges'], self.product_policies)
        self.select_category()
        self.input_keys(product_info['stock'], self.product_stocklvl)
        self.setup_prices(product_info)
        self.add_image(product_info['image'])
        self.add_variant(product_info['variant_name'])
        self.wait_for_element(self.product_save_button).click()
        self.wait_for_element_is_visible(self.product_header(product_info['name']))
        self.click_nav_menu_buttons(self.nav_products_button)

    def edit_existing_product(self, product_name, edited_product_info):
        self.wait_for_element(self.get_specific_actions_button(product_name)).click()
        self.wait_for_element_is_visible(self.product_page_edit_button).click()
        self.wait_for_element_is_visible(self.product_name)
        self.input_keys(edited_product_info['name'], self.product_name)
        self.input_keys(edited_product_info['brief_desc'], self.product_brief_desc)
        self.input_keys(edited_product_info['description'], self.product_desc)
        self.input_keys(edited_product_info['sku'], self.product_sku)
        self.input_keys(edited_product_info['fabric'], self.product_material)
        self.input_keys(edited_product_info['fit_sizing'], self.product_measurements)
        self.input_keys(edited_product_info['return_exchanges'], self.product_policies)
        self.select_category()
        self.input_keys(edited_product_info['stock'], self.product_stocklvl)
        self.setup_prices(edited_product_info)
        self.add_image(edited_product_info['image2'])
        self.wait_for_element(self.product_save_button).click()
        self.wait_for_element_is_visible(self.product_header(edited_product_info['name']))
        self.click_nav_menu_buttons(self.nav_products_button)

    def delete_product(self, edited_name):
        self.wait_for_element_is_visible(self.get_specific_actions_button(edited_name)).click()
        self.wait_for_element_is_visible(self.product_page_delete_button).click()
        self.wait_for_element(self.product_image_yes_button).click()

    def add_campaign_to_product(self, product, campaign_info):
        cp = CampaignPage(self.driver)
        cp.create_new_campaign(campaign_info)
        self.click_nav_menu_buttons(self.nav_products_button)
        self.refresh_page()
        self.wait_for_element(self.get_specific_select_button(product['name'])).click()
        self.wait_for_element_is_visible(self.product_action_button).click()
        self.wait_for_element(self.product_action_edit_button).click()
        self.wait_for_element(self.product_action_edit_campaign_button).click()
        self.wait_for_element_is_visible(self.product_add_campaign_dropdown_button)
        self.wait_for_element(self.product_add_campaign_dropdown_button).click()
        random_campaign = self.get_element_list(self.products_dropdown_option_lists)
        selected_campaign = random_campaign[randrange(len(random_campaign))].text.splitlines()[0]
        self.wait_for_element(
            self.select_campaign_dropdown_cell(selected_campaign)).click()
        self.wait_for_element(self.product_confirm_campaign_button).click()
        return selected_campaign

    def create_category(self, category_info):
        cp = CategoriesPage(self.driver)
        cp.create_new_category(category_info)

    def change_category(self, product_info, category_info):
        self.create_new_product(product_info)
        self.create_category(category_info)
        self.click_nav_menu_buttons(self.nav_products_button)
        self.refresh_page()
        self.wait_for_element_is_visible(self.get_specific_select_button(product_info['name'])).click()
        self.wait_for_element(self.product_action_button).click()
        self.wait_for_element(self.product_action_edit_button).click()
        self.wait_for_element(self.product_action_edit_category_button).click()
        self.focus_element(self.product_action_category_dropdown_button)
        self.wait_for_element(self.product_action_category_dropdown_button).click()
        self.focus_element(self.select_specific_dropdown_cell(category_info['name']))
        self.wait_for_element(self.select_specific_dropdown_cell(category_info['name'])).click()
        self.wait_for_element(self.product_confirm_category_button).click()
        return category_info['name']

    def delete_campaign_from_product(self, product_name):
        selected_product_name_and_campaign = self.get_element_list(self.get_product_name_and_campaign(product_name))
        product_name = selected_product_name_and_campaign[0].text
        product_category = selected_product_name_and_campaign[1].text
        self.wait_for_element(self.get_specific_select_button(product_name)).click()
        self.wait_for_element(self.product_action_button).click()
        self.focus_element(self.product_action_remove_button)
        self.wait_for_element(self.product_action_remove_button).click()
        self.wait_for_element(self.product_action_edit_campaign_button).click()
        self.focus_element(self.product_actions_delete_campaign_dropdown_button)
        self.wait_for_element(self.product_actions_delete_campaign_dropdown_button).click()
        self.focus_element(self.select_specific_dropdown_cell(product_category))
        self.wait_for_element(self.select_specific_dropdown_cell(product_category)).click()
        self.wait_for_element(self.remove_from_campaign_button).click()
        return product_category

    def delete_category_from_product(self, product_name, product_category):
        self.wait_for_element_is_visible(self.get_specific_select_button(product_name))
        self.wait_for_element(self.get_specific_select_button(product_name)).click()
        self.wait_for_element(self.product_action_button).click()
        self.focus_element(self.product_action_remove_button)
        self.wait_for_element(self.product_action_remove_button).click()
        self.wait_for_element(self.product_action_edit_category_button).click()
        self.focus_element(self.product_actions_delete_category_dropdown_button)
        self.wait_for_element(self.product_actions_delete_category_dropdown_button).click()
        self.focus_element(self.select_specific_dropdown_cell(product_category))
        self.wait_for_element(self.select_specific_dropdown_cell(product_category)).click()
        self.wait_for_element(self.remove_from_category_button).click()

    def add_image(self, *args):
        if self.is_element_present(self.product_image_delete_button):
            self.wait_for_element(self.product_image_delete_button).click()
            self.wait_for_element(self.product_image_yes_button).click()
            self.submit_image(args, self.product_image_input_field)
        else:
            self.submit_image(args, self.product_image_input_field)

    def select_category(self):
        self.wait_for_element_is_visible(self.product_category_dropdown_button).click()
        random_category = self.get_element_list(self.products_dropdown_option_lists)
        selected_category = random_category[randrange(len(random_category))].text
        if '-' in selected_category:
            selected_category=selected_category.split('-')[1]
        self.web_scroll(self.select_specific_dropdown_cell(selected_category))
        self.focus_element(self.select_specific_dropdown_cell(selected_category))
        self.wait_for_element_is_visible(self.select_specific_dropdown_cell(selected_category)).click()

    def add_variant(self, variant):
        self.input_keys(variant, self.product_variant_field)
        self.wait_for_element(self.product_add_variant_button).click()

    def setup_prices(self, product_info):
        self.wait_for_element_is_visible(self.product_vat_percentage_dropdown_list).click()
        random_vat_percentage = self.get_element_list(self.products_dropdown_option_lists)
        self.wait_for_element_is_visible(self.select_vat_percentage(
            random_vat_percentage[randrange(len(random_vat_percentage))].text)).click()
        self.input_keys(product_info['base_price'],
                        self.specific_retail_base_price_vat_percent(self.get_text(self.selected_vat_percent)))
        if self.is_element_displayed(self.product_separate_price_field) is False:
            self.wait_for_element(self.product_separate_procurement_price).click()
        self.input_keys(product_info['separate_price'], self.product_separate_price_field)

    def verify_product_category_is_changed(self, product_info, category_info):
        self.change_category(product_info, category_info)
        self.refresh_page()
        self.wait_for_element_is_visible(self.get_specific_cell(product_info['name']))
        return self.is_element_present(self.get_specific_cell(category_info['name']))

    def verify_category_is_deleted_from_product(self):
        selected_product = self.get_element_list(self.product_first_name_and_category)
        product_name = selected_product[0].text
        product_category = selected_product[1].text
        self.delete_category_from_product(product_name, product_category)
        return self.is_element_present(
            self.get_specific_category_cell_by_name_and_category(product_name, product_category))

    def verify_delete_product(self, edited_name):
        self.click_nav_menu_buttons(self.nav_products_button)
        self.delete_product(edited_name)
        self.click_nav_menu_buttons(self.nav_products_button)
        return self.is_element_present(self.get_specific_cell(edited_name))

    def verify_product_added_to_campaign(self, product, campaign_info):
        campaign_info = self.add_campaign_to_product(product, campaign_info)
        self.wait_for_element_is_visible(self.get_specific_cell(product['name']))
        return self.is_element_present(self.check_if_cammpaign_present(product['name'], campaign_info))


    def verify_campaign_is_deleted_from_product(self, product_name):
        campaign_name = self.delete_campaign_from_product(product_name)
        self.wait_for_element_is_visible(self.get_specific_cell(product_name))
        return self.is_element_present(self.check_if_cammpaign_present(product_name, campaign_name))

    def verify_product_page_new_product(self, product_info):
        self.create_new_product(product_info)
        results = []
        self.refresh_page()
        self.wait_element_to_appear(self.get_specific_cell(product_info['base_price']))
        price = self.get_text(self.get_specific_cell(product_info['base_price']))
        parsed_price=price.split(',')[0]
        results.append(self.is_element_present(self.get_specific_cell(product_info['name'])))
        results.append(self.is_element_present(self.get_specific_cell(parsed_price)))
        return all(results)

    def verify_product_page_edit_product(self, product_name, edited_product_info):
        self.edit_existing_product(product_name, edited_product_info)
        results = []
        self.wait_element_to_appear(self.get_specific_cell(edited_product_info['base_price']))
        price = self.get_text(self.get_specific_cell(edited_product_info['base_price']))
        parsed_price = price.split(',')[0]
        results.append(self.is_element_present(self.get_specific_cell(edited_product_info['name'])))
        results.append(self.is_element_present(self.get_specific_cell(parsed_price)))
        return all(results)

    def verify_search_field_working(self, search_query=''):
        result_before_search = len(self.get_element_list(self.search_result_rows))
        self.submit_search_query(search_query)
        result_after_search = len(self.get_element_list(self.search_result_rows))

        return 0 < result_after_search < result_before_search

    def verify_paginator_working(self):
        product_name_before = self.get_text(self.product_name_cell)
        self.wait_for_element(self.paginator_next_page_button).click()
        product_name_after = self.get_text(self.product_name_cell)
        self.wait_for_element(self.paginator_previous_page_button).click()

        return product_name_before != product_name_after

    def change_stock_lvl(self, product_info):
        random_stock_lvl = randint(10,999)
        self.click_nav_menu_buttons(self.nav_products_button)
        self.wait_for_element_is_visible(self.get_specific_product_stock_lvl_edit_button(product_info['name'])).click()
        self.input_keys(random_stock_lvl, self.product_stock_lvl_field)
        self.element_click(self.product_edit_stock_lvl_confirm_button)
        self.wait_for_element_is_visible(self.product_image_yes_button).click()
        return random_stock_lvl


    def verify_inventory_and_stock_lvl(self, product_info):
        self.create_new_product(product_info)
        results=[]
        self.wait_element_to_appear(self.get_specific_cell(product_info['stock']))
        results.append(self.is_element_present(self.get_specific_stock_lvl(product_info['name'],product_info['stock'])))
        self.element_click(self.product_inventory_button)
        self.wait_element_to_appear(self.get_specific_cell(product_info['stock']))
        results.append(self.is_element_present(self.get_specific_stock_lvl(product_info['name'], product_info['stock'])))
        return all(results)

    def verify_inventory_and_stock_lvl_is_changed(self, product_info):
        changed_stock_lvl = self.change_stock_lvl(product_info)
        results = []
        self.wait_element_to_appear(self.get_specific_stock_lvl(product_info['name'], changed_stock_lvl))
        results.append(self.is_element_present(self.get_specific_stock_lvl(product_info['name'], changed_stock_lvl)))
        self.element_click(self.product_inventory_button)
        self.wait_element_to_appear(self.get_specific_cell(changed_stock_lvl))
        results.append(self.is_element_present(self.get_specific_stock_lvl(product_info['name'], changed_stock_lvl)))
        self.click_nav_menu_buttons(self.nav_products_button)
        self.delete_product(product_info['name'])
        return all(results)