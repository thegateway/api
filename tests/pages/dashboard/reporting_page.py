from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class ReportingPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########Locators#########
    page_title = page_header = "Reporting and Insights"

    def verify_reporting_page_title(self):
        self.click_nav_menu_buttons(self.nav_reporting_button)
        return self.verify_page_title(self.page_title)

    def verify_reporting_page_header(self):
        return self.verify_page_header(self.page_header)
