from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging
from random import randrange


class CampaignPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Campaign Page Locators ##############
    page_title = page_header = 'Campaigns'
    campaigns_add_new_button = '//span[contains(text(),"Create a campaign")]'
    campaigns_form_save_button = '//mat-dialog-actions[@class="mat-dialog-actions"]//button[2]'
    campaigns_form_cancel_button = '//mat-dialog-actions[@class="mat-dialog-actions"]//button[1]'
    campaigns_view_button = '//mat-table/mat-row[1]//a[1]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    def specific_campaign_header_name(self, name):
        return f'//mat-card[div/h2[contains(text(),"{name}")]]'

    campaigns_edit_button = '//button[contains(text(),"Edit")]'
    campaigns_delete_button = '//button[contains(text(),"Delete")]'
    campaigns_view_action_button = '//button[contains(text(),"View")]'

    def get_specific_product_in_campaign_view(self, name):
        return f'//mat-row[mat-cell[contains(div/text(),"{name}")]]'

    def get_specific_campaign_name_in_campaign_view(self, name):
        return f'//h2[contains(text(),"{name}")]'

    campaigns_delete_confirm_button = '//body//mat-dialog-container//button[2]'
    campaigns_delete_cancel_button = '//body//mat-dialog-container//button[1]'

    campaigns_form_name_field = '//input[@placeholder="Name"]'

    campaigns_form_start_date = '//input[@placeholder="Start"]'
    campaigns_form_end_date = '//input[@placeholder="End"]'
    campaigns_form_start_date_calender = '//div[div/input[@placeholder="Start"]]//mat-datepicker-toggle'
    campaigns_form_end_date_calender = '//div[div/input[@placeholder="End"]]//mat-datepicker-toggle'
    campaigns_form_value = '//input[@placeholder="Discount value"]'
    campaigns_operator_dropdown_button = '//mat-select[@aria-label="Operator"]'

    campaigns_add_image_checkbox = '//a[contains(text(),"Do you want to add graphics of your campign?")]/mat-checkbox'
    campaign_add_new_image_button = '//a[contains(text(),"Add image")]'
    campaign_image_type_dropdown_list_button = '//mat-select[@aria-label="Type"]'
    campaign_dropdown_options = '//mat-option'
    campaign_image_field = '//input[@name="logo"]'


    def select_campaigns_opeartor_type(self, operator_type):
        return f'//mat-option[contains(span/text(),"{operator_type}")]'

    campaigns_list_table_row = '//app-list-table/mat-table//mat-row'

    def get_specific_table_cell(self, cell_info):
        return f'//mat-cell[contains(div/text(), "{cell_info}")]'

    def get_specific_activated_cell(self, cell_info):
        return f'//mat-row[mat-cell[contains(div/text(), "{cell_info}")]]/mat-cell[2]/div'

    def get_specific_duaration_cell(self, cell_info):
        return f'//mat-row[mat-cell[contains(div/text(), "{cell_info}")]]//mat-cell[2]/div'

    #########################################################

    def click_campaigns_add_new_button(self):
        self.focus_element(self.campaigns_add_new_button)
        self.wait_for_element_is_visible(self.campaigns_add_new_button).click()

    def add_campaign_picture(self, *args):
        self.wait_for_element(self.campaigns_add_image_checkbox).click()
        self.wait_for_element(self.campaign_add_new_image_button).click()
        self.wait_for_element(self.campaign_image_type_dropdown_list_button).click()
        types = self.get_element_list(self.campaign_dropdown_options)
        selected_type = types[randrange(len(types))].text
        self.wait_for_element(self.select_campaigns_opeartor_type(selected_type)).click()
        self.submit_image(*args, self.campaign_image_field)

    def create_new_campaign(self, campaign_info):
        self.click_nav_menu_buttons(self.nav_campaigns_button)
        self.click_campaigns_add_new_button()
        self.input_keys(campaign_info['name'], self.campaigns_form_name_field)
        self.add_campaign_picture(campaign_info['image'])
        self.calendar_datepicker(campaign_info['start_date'], self.campaigns_form_start_date_calender)
        self.calendar_datepicker(campaign_info['end_date'], self.campaigns_form_end_date_calender)
        self.get_element(self.campaigns_operator_dropdown_button).click()
        self.wait_for_element(self.select_campaigns_opeartor_type(campaign_info['operator_type'])).click()
        self.input_keys(campaign_info['operator_value'], self.campaigns_form_value)
        self.focus_element(self.campaigns_form_save_button)
        self.wait_for_element(self.campaigns_form_save_button).click()
        self.wait_for_element_is_visible(self.specific_campaign_header_name(campaign_info['name']))
        self.click_nav_menu_buttons(self.nav_campaigns_button)

    def delete_campaign(self, name):
        self.wait_for_element(self.get_specific_actions_button(name)).click()
        self.wait_for_element(self.campaigns_delete_button).click()
        self.focus_element(self.campaigns_delete_confirm_button)
        self.wait_for_element(self.campaigns_delete_confirm_button).click()

    def verify_campaigns_page_title(self):
        self.wait_for_element_is_visible(self.page_title)
        return self.verify_page_title(self.page_title)

    def verify_campaigns_page_header(self):
        self.wait_for_element_is_visible(self.page_title)
        return self.verify_page_header(self.page_header)

    def edit_campaign(self, name, edited_campaign_info):
        self.wait_for_element_is_visible(self.get_specific_actions_button(name))
        self.wait_for_element(self.get_specific_actions_button(name)).click()
        self.wait_for_element_is_visible(self.campaigns_edit_button).click()
        self.input_keys(edited_campaign_info['name'], self.campaigns_form_name_field)
        self.calendar_datepicker(edited_campaign_info['start_date'], self.campaigns_form_start_date_calender)
        self.calendar_datepicker(edited_campaign_info['end_date'], self.campaigns_form_end_date_calender)
        self.get_element(self.campaigns_operator_dropdown_button).click()
        self.focus_element(self.select_campaigns_opeartor_type(edited_campaign_info['operator_type']))
        self.wait_for_element_is_visible(self.select_campaigns_opeartor_type(edited_campaign_info['operator_type'])).click()
        self.input_keys(edited_campaign_info['operator_value'], self.campaigns_form_value)
        self.focus_element(self.campaigns_form_save_button)
        self.wait_for_element(self.campaigns_form_save_button).click()
        self.wait_for_element_is_visible(self.specific_campaign_header_name(edited_campaign_info['name']))
        self.click_nav_menu_buttons(self.nav_campaigns_button)

    def verify_add_new_campaign(self, campaign_info):
        self.create_new_campaign(campaign_info)
        self.wait_for_element_is_visible(self.get_specific_duaration_cell(campaign_info['name']))
        results = []
        start_date = campaign_info['start_date']
        end_date = campaign_info['end_date']
        formatted_start_date = start_date.strftime('%d.%m.%Y')
        formatted_end_date = end_date.strftime('%d.%m.%Y')
        campaign_duration = formatted_start_date + " - " + formatted_end_date
        results.append(self.is_element_present(self.campaigns_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_cell(campaign_info['name'])))
        results.append(
            campaign_duration
            in
            self.get_text(self.get_specific_duaration_cell(campaign_info['name']))
        )
        return all(results)

    def verify_edit_campaign(self, name, edited_campaign_info):
        self.edit_campaign(name, edited_campaign_info)
        self.wait_for_element_is_visible(self.campaigns_list_table_row)
        results = []
        start_date = edited_campaign_info['start_date']
        end_date = edited_campaign_info['end_date']
        formatted_start_date = start_date.strftime('%d.%m.%Y')
        formatted_end_date = end_date.strftime('%d.%m.%Y')
        campaign_duration = formatted_start_date + " - " + formatted_end_date
        results.append(self.is_element_present(self.campaigns_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_cell(edited_campaign_info['name'])))
        results.append(
            campaign_duration
            in
            self.get_text(self.get_specific_duaration_cell(edited_campaign_info['name']))
        )

        return all(results)

    def verify_delete_campaign(self, name):
        self.delete_campaign(name)
        self.refresh_page()
        return self.is_element_present(self.get_specific_table_cell(name))
