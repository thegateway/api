from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class LoginPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Login Page Locators ##############
    page_title = ' '
    username_field = '//input[@type="email"]'
    password_field = '//input[@type="password"]'
    login_button = '//button[contains(span/text(),"Login")]'
    login_error_msg = '//div[@id="toast-container"]//div[@class="toast-message ng-star-inserted"]'
    logout_button = '//*[@id="user-logout-button"]'
    register_page_link = '//a[@href="/auth/register"]'
    register_button_span = '//button/span[contains(text(),"Register")]'
    account_verify_page_link = '//a[@href="/auth/verify"]'
    verify_button_span = '//button/span[contains(text(),"Verify" | "Zweryfikuj")]'
    forgot_password_page_link = '//a[@href="/auth/forgot-password"]'
    forgot_password_button_span = '//button/span[contains(text(),"Change" | "Zminana")]'
    page_back_button = '//div[@class="back-button"]'
    ###############################################

    def enter_username(self, username):
        self.input_keys(username, self.username_field)

    def enter_password(self, password):
        self.input_keys(password, self.password_field)

    def login(self, username='', password=''):
        self.enter_username(username)
        self.enter_password(password)
        self.wait_for_element_is_visible(self.login_button).click()

    def verify_login_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_logging_is_successful(self):
        return self.is_element_present(self.logout_button)

    def verify_logging_is_failed(self):
        return self.wait_for_element_is_visible(self.login_button)

    def verify_registration_link_working(self):
        self.wait_for_element(self.register_page_link).click()
        result = self.is_element_present(self.register_button_span)
        self.wait_for_element(self.page_back_button).click()
        return result

    def verify_verification_link_working(self):
        self.wait_for_element(self.account_verify_page_link).click()
        result = self.is_element_present(self.verify_button_span)
        self.wait_for_element(self.page_back_button).click()
        return result

    def verify_forgot_password_link_working(self):
        self.wait_for_element(self.forgot_password_page_link).click()
        result = self.is_element_present(self.forgot_password_button_span)
        self.wait_for_element(self.page_back_button).click()
        return result


