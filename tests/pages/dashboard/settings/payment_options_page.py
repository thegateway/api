from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class PaymentTypesPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Payment Types Page Locators ##############
    page_title = page_header = 'Payment options'
    payment_types_add_new_button = '//a[contains(span/text(), "Add new")]'
    payment_types_form_save_button = '//mat-dialog-actions//button[contains(span/text(), "Save")]'
    payment_types_form_cancel_button = '//mat-dialog-actions//button[contains(span/text(), "Cancel")]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    payment_types_edit_button = '//button[contains(text(),"Edit")]'
    payment_types_delete_button = '//button[contains(text(),"Delete")]'

    payment_types_delete_confirm_button = '//button[contains(span/text(), "Yes")]'
    payment_types_delete_cancel_button = '//button[contains(span/text(), "No")]'

    payment_types_form_dropdown = '//mat-select[@aria-label="Payment type"]'

    def select_payment_type(self, payment_type):
        return f'//mat-option/span[contains(text(),"{payment_type}")]'

    retailer_add_payment_option_button = '//mat-select[@aria-label="Select payment method"]'
    add_to_retailer_button = '//a[span[contains(text(),"Add to retailer")]]'

    payment_types_form_name_field = '//input[@placeholder="Name"]'
    payment_types_form_description_field = '//textarea[@placeholder="Description"]'
    payment_types_form_logo_url_field = '//input[@name="logo"]'
    payment_types_form_image_delete_button = '//div[a/fa[@name="minus-circle"]]/a'
    payment_types_form_image_delete_confirmation_button = '//button[contains(span/text(),"Yes")]'
    payment_option_row = '//mat-header-row'
    payment_types_list_table_row = '//mat-table/mat-row'

    def get_specific_table_cell(self, cell_info):
        return f'//mat-cell[contains(div/text(), "{cell_info}")]'

    def get_specific_table_image(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"")]]]//img[contains(@src,".jpg")]'

    def get_specific_retailer_cell(self, name):
        return f'//mat-card[mat-card-title[contains(text(),"Your payment methods")]]//mat-cell[contains(div/text(), "{name}")]'

    def get_specific_retailer_action_button(self, name):
        return f'//mat-card[mat-card-title[contains(text(),"Your payment methods")]]//mat-row[mat-cell[div[contains(' \
               f'text(),"{name}")]]]//button '

    #########################################################

    def click_payment_types_add_new_button(self):
        self.focus_element(self.payment_types_add_new_button)
        self.wait_for_element(self.payment_types_add_new_button).click()

    def create_new_payment(self, payment_info):
        self.click_payment_types_add_new_button()
        self.wait_for_element(self.payment_types_form_dropdown).click()
        self.wait_for_element(self.select_payment_type(payment_info['type'])).click()
        self.input_keys(payment_info['name'], self.payment_types_form_name_field)
        self.input_keys(payment_info['description'], self.payment_types_form_description_field)
        self.submit_image(payment_info['image'], self.payment_types_form_logo_url_field)
        self.focus_element(self.payment_types_form_save_button)
        self.wait_for_element(self.payment_types_form_save_button).click()

    def edit_payment(self, payment_name, edited_payment_info):
        self.wait_for_element(self.get_specific_actions_button(payment_name)).click()
        self.wait_for_element(self.payment_types_edit_button).click()
        self.wait_for_element(self.payment_types_form_dropdown).click()
        self.wait_for_element(self.select_payment_type(edited_payment_info['type'])).click()
        self.input_keys(edited_payment_info['name'], self.payment_types_form_name_field)
        self.input_keys(edited_payment_info['description'], self.payment_types_form_description_field)
        self.focus_element(self.payment_types_form_image_delete_button)
        self.wait_for_element(self.payment_types_form_image_delete_button).click()
        self.focus_element(self.payment_types_form_image_delete_confirmation_button)
        self.wait_for_element(self.payment_types_form_image_delete_confirmation_button).click()
        self.submit_image(edited_payment_info['image2'], self.payment_types_form_logo_url_field)
        self.focus_element(self.payment_types_form_save_button)
        self.wait_for_element(self.payment_types_form_save_button).click()

    def delete_payment(self, payment_name):
        self.wait_for_element(self.get_specific_actions_button(payment_name)).click()
        self.wait_for_element(self.payment_types_delete_button).click()
        self.focus_element(self.payment_types_delete_confirm_button)
        self.wait_for_element(self.payment_types_delete_confirm_button).click()

    def add_payment_option_to_retailer(self, payment_info):
        self.wait_for_element_is_visible(self.get_specific_table_cell(payment_info['name']))
        self.refresh_page()
        self.wait_for_element_is_visible(self.get_specific_table_cell(payment_info['name']))
        self.wait_for_element(self.retailer_add_payment_option_button).click()
        self.wait_for_element_is_visible(self.select_payment_type(payment_info['name'])).click()
        self.wait_for_element(self.add_to_retailer_button).click()

    def remove_payment_option_from_retailer(self, payment_info):
        self.wait_for_element_is_visible(self.get_specific_retailer_action_button(payment_info['name'])).click()
        self.wait_for_element(self.payment_types_delete_button).click()
        self.focus_element(self.payment_types_delete_confirm_button)
        self.wait_for_element(self.payment_types_delete_confirm_button).click()

    def verify_payment_types_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_payment_types_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_add_new_payment(self, payment_info):
        self.create_new_payment(payment_info)
        self.wait_element_to_appear(self.get_specific_table_cell(payment_info['name']))
        results = []
        results.append(self.is_element_present(self.payment_types_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_image(payment_info['description'])))
        results.append(self.is_element_present(self.get_specific_table_cell(payment_info['name'])))
        results.append(self.is_element_present(self.get_specific_table_cell(payment_info['description'])))

        return all(results)

    def verify_edit_payment(self, payment_name, edited_payment_info):
        self.edit_payment(payment_name, edited_payment_info)
        results = []
        self.wait_for_element_is_visible(self.get_specific_table_cell(edited_payment_info['description']))
        results.append(self.is_element_displayed(self.payment_types_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_cell(edited_payment_info['name'])))
        results.append(self.is_element_present(self.get_specific_table_cell(edited_payment_info['description'])))
        return all(results)

    def verify_delete_payment(self, payment_name):
        self.delete_payment(payment_name)
        self.click_nav_menu_buttons(self.nav_settings_payment_options_button)
        self.wait_for_element_is_visible(self.payment_option_row)
        return self.is_element_present(self.get_specific_table_cell(payment_name))

    def verify_payment_option_to_retailer(self, payment_info):
        self.add_payment_option_to_retailer(payment_info)
        results = []
        results.append(self.is_element_present(self.payment_types_list_table_row))
        results.append(self.is_element_present(self.get_specific_retailer_cell(payment_info['name'])))
        results.append(self.is_element_present(self.get_specific_retailer_cell(payment_info['description'])))
        return all(results)

    def verify_delete_payment_option_retailer(self, payment_info):
        self.remove_payment_option_from_retailer(payment_info)
        self.wait_for_element_is_visible(self.get_specific_table_cell(payment_info['name']))
        return self.is_element_present(self.get_specific_retailer_cell(payment_info['name']))
