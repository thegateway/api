from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging
from random import randrange


class CategoriesPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Categories Page Locators ##############
    page_title = page_header = 'Categories'
    categories_add_new_button = '//a[contains(span/text(), "Add new")]'
    categories_form_save_button = '//button[contains(span/text(), "Save")]'
    categories_form_cancel_button = '//button[contains(span/text(), "Cancel")]'
    categories_delete_confirm_button = '//button[contains(span/text(), "Yes")]'
    categories_delete_cancel_button = '//button[contains(span/text(), "No")]'
    parent_list = '//mat-tree/mat-tree-node[@role="group"]/div'
    parent_list_secondary = '//mat-tree/mat-tree-node'
    categories_form_name = '//input[@placeholder="Name"]'
    categories_parent_list = '//div[contains(span/text(),"Parent")]'

    categories_image_type_dropdown = '//mat-select[contains(div/div/span/text(),"Type")]'
    categories_image_input_field = '//input[@name="logo"]'
    categories_image_delete_button = '//button[span/fa/i[@class="fa fa-trash"]]'
    categories_delete_image_box = '//a[fa/i[@class="fa fa-minus-circle"]]'

    categories_add_new_image_button = '//a[fa]'
    categories_image_type_dropdown_list_button = '//mat-select[@aria-label="Type"]'
    categories_dropdown_options = '//mat-option'
    categories_image_checkbox = '//a[contains(text(),"Images")]/mat-checkbox'
    categories_image_field = '//input[@name="logo"]'

    def select_categories_type(self, operator_type):
        return f'//mat-option[contains(span/text(),"{operator_type}")]'

    def categories_select_specific_image_type(self, name):
        return f'//mat-option[contains(span/text(),"{name}")]'

    def get_specific_categories_name(self, name):
        return f'//div[contains(text(),"{name}")]'

    def get_specific_actions_button(self, name):
        return f'//mat-tree-node[div/div[contains(text(),"{name}")]]//div/button'

    categories_edit_button = '//button[contains(text(),"Edit")]'
    categories_delete_button = '//button[contains(text(),"Delete")]'

    def get_specific_categories_view_button(self, name):
        return f'//mat-tree-node[contains(div/div/text(),"{name}")]//a[1]'

    def get_specific_categories_edit_button(self, name):
        return f'//mat-tree-node[contains(div/div/text(),"{name}")]//a[2]'

    def get_specific_categories_delete_button(self, name):
        return f'//mat-tree-node[contains(div/div/text(),"{name}")]//a[3]'

    def get_specific_parent(self, name):
        return f'//mat-option[span[contains(text(),"{name}")]]'

    def get_specific_category_childs_button(self, name):
        return f'//mat-tree-node[contains(div/div/text(),"{name}")]/button'

    #########################################################

    def click_categories_add_new_button(self):
        self.focus_element(self.categories_add_new_button)
        self.wait_for_element_is_visible(self.categories_add_new_button).click()

    def click_categories_view_button(self, name):
        self.wait_for_element(self.get_specific_categories_view_button(name)).click()

    def click_categories_edit_button(self, name):
        self.wait_for_element(self.get_specific_categories_edit_button(name)).click()

    def click_categories_delete_button(self, name):
        self.wait_for_element(self.get_specific_categories_delete_button(name)).click()

    def add_category_picture(self, *args):
        self.wait_for_element(self.categories_add_new_image_button).click()
        self.wait_for_element(self.categories_image_type_dropdown_list_button).click()
        types = self.get_element_list(self.categories_dropdown_options)
        selected_type = types[randrange(len(types))].text
        self.wait_for_element(self.select_categories_type(selected_type)).click()
        self.submit_image(*args, self.categories_image_field)

    def create_new_category(self, category):
        self.click_nav_menu_buttons(self.nav_settings_categories_button)
        self.wait_for_element_is_visible(self.categories_add_new_button)
        self.click_categories_add_new_button()
        self.focus_element(self.categories_form_name)
        self.wait_for_element_is_visible(self.categories_form_name)
        self.input_keys(category['name'], self.categories_form_name)
        self.wait_for_element_is_visible(self.categories_image_checkbox).click()
        self.add_category_picture(category['image'])
        self.focus_element(self.categories_form_save_button)
        self.wait_for_element(self.categories_form_save_button).click()

    def create_new_sub_category(self, name):
        # all_parents = self.get_element_list(self.parent_list)
        all_parents = self.is_element_present(self.parent_list)
        if all_parents:
            all_parents = self.get_element_list(self.parent_list)
        else:
            all_parents = self.get_element_list(self.parent_list_secondary)
        self.click_categories_add_new_button()
        self.focus_element(self.categories_form_name)
        self.input_keys(name, self.categories_form_name)
        self.wait_for_element(self.categories_parent_list).click()
        random_parent = all_parents[randrange(len(all_parents))].text
        if ' ' in random_parent:
            random_parent= random_parent.split(' ')[0]
        self.web_scroll(self.get_specific_parent(random_parent))
        self.wait_for_element_is_visible(self.get_specific_parent(random_parent)).click()
        self.focus_element(self.categories_form_save_button)
        self.wait_for_element(self.categories_form_save_button).click()
        return random_parent

    def edit_category(self, added_name, edit_name):
        self.wait_for_element_is_visible(self.get_specific_categories_name(added_name))
        self.wait_for_element(self.get_specific_actions_button(added_name)).click()
        self.wait_for_element_is_visible(self.categories_edit_button).click()
        self.input_keys(edit_name, self.categories_form_name)
        self.focus_element(self.categories_form_save_button)
        self.wait_for_element(self.categories_form_save_button).click()

    def delete_category(self, added_name):
        self.wait_for_element_is_visible(self.get_specific_categories_name(added_name))
        self.wait_for_element(self.get_specific_actions_button(added_name)).click()
        self.wait_for_element_is_visible(self.categories_delete_button).click()
        self.focus_element(self.categories_delete_confirm_button)
        self.wait_for_element_is_visible(self.categories_delete_confirm_button).click()

    def verify_categories_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_categories_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_add_new_category(self, category):
        self.create_new_category(category)
        self.wait_element_to_appear(self.get_specific_categories_name(category['name']))
        self.wait_for_element_is_visible(self.get_specific_categories_name(category['name']))
        return self.is_element_present(self.get_specific_categories_name(category['name']))

    def verify_edit_category(self, added_name, edit_name):
        self.edit_category(added_name, edit_name['name'])
        self.wait_for_element_is_visible(self.get_specific_categories_name(edit_name['name']))
        return self.is_element_present(self.get_specific_categories_name(edit_name['name']))

    def verify_new_sub_category(self, name):
        selected_parent_category = self.create_new_sub_category(name)
        self.wait_for_element_is_visible(self.get_specific_categories_name(name))
        results = []
        results.append(self.is_element_present(self.get_specific_categories_name(name)))
        self.wait_for_element_is_visible(self.get_specific_category_childs_button(selected_parent_category)).click()
        results.append(True if self.is_element_present(self.get_specific_categories_name(name)) is False else False)
        self.wait_for_element(self.get_specific_category_childs_button(selected_parent_category)).click()
        return all(results)

    def verify_delete_sub_category(self, name):
        self.delete_category(name)
        self.refresh_page()
        return self.is_element_present(self.get_specific_categories_name(name))

    def verify_delete_category(self, added_name):
        self.delete_category(added_name)
        self.refresh_page()
        return self.is_element_present(self.get_specific_categories_name(added_name))
