from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class PricingPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Pricing Page Locators ##############
    page_title = page_header = 'Pricing'
    pricing_add_new_button = '//a//span[contains(text(), "Add new")]'
    pricing_form_save_button = '//mat-dialog-actions//button[contains(span/text(), "Save")]'
    pricing_form_cancel_button = '//mat-dialog-actions//button[contains(span/text(), "Cancel")]'
    pricing_edit_button = '//mat-table/mat-row[1]//a[1]'
    pricing_delete_button = '//mat-table/mat-row[1]//a[2]'

    def get_specific_pricing_edit_button(self, name):
        return f'//mat-row[contains(mat-cell[1]/div/text(), "{name}")]//a[1]'

    def get_specific_pricing_delete_button(self, name):
        return f'//mat-row[contains(mat-cell[1]/div/text(), "{name}")]//a[2]'

    pricing_delete_confirm_button = '//app-confirm-dialog/mat-dialog-actions/button[contains(span/text(), "Yes")]'
    pricing_delete_cancel_button = '//app-confirm-dialog/mat-dialog-actions/button[contains(span/text(), "No")]'

    pricing_form_name_field = '//input[@placeholder="Name"]'
    pricing_form_amount_field = '//input[@placeholder="Amount"]'
    pricing_form_is_global_price_checkbox = '//span[contains(text(), "Is this global price?")]'

    pricing_tab_trade_price = '//div[@class="mat-tab-label-container"]//div[contains(text(), "Tukkuhinta")]'
    pricing_tab_retail_price = '//div[@class="mat-tab-label-container"]//div[contains(text(), "Ohjehinta (ovh)")]'

    pricing_operator_dropdown_button = '//mat-select//div[@class="mat-select-value"]'

    def get_pricing_dropdown_opeartor_type(self, operator_type):
        return f'//mat-option//span[contains(text(), "{operator_type}")]'

    pricing_list_table_row = '//app-list-table/mat-table//mat-row'

    def get_specific_table_cell(self, cell_info):
        return f'//mat-cell[contains(div/text(), "{cell_info}")]'

    #########################################################

    def click_pricing_add_new_button(self):
        self.wait_for_element(self.pricing_add_new_button).click()

    def create_pricing(self, pricing_info):
        self.click_pricing_add_new_button()
        self.input_keys(pricing_info['name'], self.pricing_form_name_field)
        self.wait_for_element(self.pricing_operator_dropdown_button).click()
        self.wait_for_element(self.get_pricing_dropdown_opeartor_type(pricing_info['wholesale_operator_type'])).click()
        self.input_keys(pricing_info['wholesale_amount'], self.pricing_form_amount_field)
        self.wait_for_element(self.pricing_tab_retail_price).click()
        self.wait_for_element(self.pricing_operator_dropdown_button).click()
        self.wait_for_element(self.get_pricing_dropdown_opeartor_type(pricing_info['retail_operator_type'])).click()
        self.input_keys(pricing_info['retail_amount'], self.pricing_form_amount_field)
        self.focus_element(self.pricing_form_save_button)
        self.wait_for_element(self.pricing_form_save_button).click()

    def edit_pricing(self, name, edited_pricing_info):
        self.wait_for_element(self.get_specific_pricing_edit_button(name)).click()
        self.wait_for_element(self.pricing_operator_dropdown_button).click()
        self.wait_for_element(self.get_pricing_dropdown_opeartor_type(edited_pricing_info['wholesale_operator_type'])).click()
        self.input_keys(edited_pricing_info['wholesale_amount'], self.pricing_form_amount_field)
        self.wait_for_element(self.pricing_tab_retail_price).click()
        self.wait_for_element(self.pricing_operator_dropdown_button).click()
        self.wait_for_element(self.get_pricing_dropdown_opeartor_type(edited_pricing_info['retail_operator_type'])).click()
        self.input_keys(edited_pricing_info['retail_amount'], self.pricing_form_amount_field)
        self.focus_element(self.pricing_form_save_button)
        self.wait_for_element(self.pricing_form_save_button).click()

    def delete_pricing(self, name):
        self.wait_for_element(self.get_specific_pricing_delete_button(name)).click()
        self.focus_element(self.pricing_delete_confirm_button)
        self.wait_for_element(self.pricing_delete_confirm_button).click()

    def verify_pricing_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_pricing_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_add_new_pricing(self, pricing_info):
        self.create_pricing(pricing_info)
        results = []
        results.append(self.is_element_present(self.pricing_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_cell(pricing_info['name'])))
        results.append(self.is_element_present(self.get_specific_table_cell(pricing_info['wholesale_amount'])))
        results.append(self.is_element_present(self.get_specific_table_cell(pricing_info['retail_amount'])))

        return all(results)

    def verify_edit_pricing(self, name, edited_pricing_info):
        self.edit_pricing(name, edited_pricing_info)
        results = []
        results.append(self.is_element_present(self.get_specific_table_cell(name)))
        results.append(self.is_element_present(self.get_specific_table_cell(edited_pricing_info['wholesale_amount'])))
        results.append(self.is_element_present(self.get_specific_table_cell(edited_pricing_info['retail_amount'])))

        return all(results)

    def verify_delete_pricing(self, name):
        self.delete_pricing(name)
        return self.is_element_present(self.get_specific_table_cell(name))
