from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging

class DeliveryOptionsPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### delivery_option Page Locators ##############
    page_title = page_header = 'Delivery options'
    delivery_option_add_new_button = '//a[contains(span/text(), "Add new")]'
    delivery_option_form_save_button = '//mat-dialog-actions/button[contains(span/text(),"Save")]'
    delivery_option_form_cancel_button = '//mat-dialog-actions/button[contains(span/text(),"Cancel")]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    delivery_option_edit_button = '//button[contains(text(),"Edit")]'
    delivery_option_delete_button = '//button[contains(text(),"Delete")]'

    def get_specific_vat_percentage(self, vat_class):
        return f'//body//mat-option[contains(span/text(),"{vat_class}")]'

    delivery_option_delete_confirm_button = '//button[span/text()="Yes"]'
    delivery_option_delete_cancel_button = '//button[span/text()="No"]'
    delivery_option_dialog_container = '//mat-dialog-container'

    delivery_option_form_name_field = '//input[@placeholder="Name"]'
    delivery_option_form_description_field = '//textarea[@placeholder="Description"]'
    delivery_option_form_logo_url_field = '//input[@name="logo"]'
    delivery_option_delete_image_button = '//div[a/fa[@name="minus-circle"]]/a'
    delivery_option_form_price_field = '//div[span/label/mat-label/text()]/input'
    delivery_option_form_vat_class_field = '//*[@aria-label="VAT percent"]'
    delivery_option_add_separate_retail_price_button = '//mat-checkbox[label[contains(span/text(),"Add a separte procurement price")]]'

    def specific_retail_base_price_vat_percent(self, vat_percent):
        return f'//input[@placeholder="Retail price (VAT {vat_percent})"]'

    delivery_option_separate_cost_price_field = '//input[@placeholder="Cost price (VAT 0%)"]'

    delivery_option_separate_price_field = '//input[@placeholder="Cost price (VAT 0%)"]'

    delivery_option_dont_ask_address_button = '//mat-checkbox[label[contains(span/text(),'"Don't ask for address"')]]'
    delivery_management_list_table_row = '//app-list-table/mat-table//mat-row'

    def get_specific_table_cell(self, cell_info):
        return f'//mat-cell[contains(div/text(), "{cell_info}")]'

    def get_specific_price_cell(self, cell_info):
        return f'//mat-table/mat-row[contains(mat-cell[3]/div/text(),"{cell_info}")]/mat-cell[5]/div'

    #########################################################

    def click_delivery_option_add_new_button(self):
        self.wait_for_element_is_visible(self.delivery_option_add_new_button).click()

    def create_delivery_option(self, delivery_option):
        self.click_delivery_option_add_new_button()
        self.input_keys(delivery_option['name'], self.delivery_option_form_name_field)
        self.input_keys(delivery_option['description'], self.delivery_option_form_description_field)
        self.submit_image(delivery_option['logo_url'], self.delivery_option_form_logo_url_field)
        self.focus_element(self.delivery_option_form_vat_class_field)
        self.wait_for_element(self.delivery_option_form_vat_class_field).click()
        self.wait_for_element(self.get_specific_vat_percentage(delivery_option['vat_class'])).click()
        self.input_keys(delivery_option['price'],
                        self.specific_retail_base_price_vat_percent(delivery_option['vat_class']))
        self.wait_for_element(self.delivery_option_add_separate_retail_price_button).click()
        self.input_keys(delivery_option['retail_price'], self.delivery_option_separate_price_field)
        self.focus_element(self.delivery_option_form_save_button)
        self.wait_for_element(self.delivery_option_form_save_button).click()

    # todo when currencies are working fix this
    def edit_delivery_option(self, name, edit_delivery_option):
        self.wait_for_element(self.get_specific_actions_button(name)).click()
        self.wait_for_element(self.delivery_option_edit_button).click()
        self.input_keys(edit_delivery_option['name'], self.delivery_option_form_name_field)
        self.input_keys(edit_delivery_option['description'], self.delivery_option_form_description_field)
        self.focus_element(self.delivery_option_delete_image_button)
        self.wait_for_element(self.delivery_option_delete_image_button).click()
        self.focus_element(self.delivery_option_delete_confirm_button)
        self.wait_for_element(self.delivery_option_delete_confirm_button).click()
        self.submit_image(edit_delivery_option['logo_url2'], self.delivery_option_form_logo_url_field)
        self.input_keys(edit_delivery_option['price'], self.delivery_option_form_price_field)
        self.focus_element(self.delivery_option_form_vat_class_field)
        self.wait_for_element(self.delivery_option_form_vat_class_field).click()
        self.wait_for_element(self.get_specific_vat_percentage(edit_delivery_option['vat_class'])).click()
        if self.is_element_displayed(
                self.delivery_option_separate_cost_price_field) is False:
            self.wait_for_element(self.delivery_option_add_separate_retail_price_button).click()
        self.input_keys(edit_delivery_option['retail_price'],
                        self.delivery_option_separate_cost_price_field)
        self.focus_element(self.delivery_option_form_save_button)
        self.wait_for_element(self.delivery_option_form_save_button).click()

    def delete_delivery_option(self, name):
        self.wait_for_element(self.get_specific_actions_button(name)).click()
        self.wait_for_element(self.delivery_option_delete_button).click()
        self.focus_element(self.delivery_option_delete_confirm_button)
        self.wait_for_element(self.delivery_option_delete_confirm_button).click()

    def verify_delivery_options_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_delivery_options_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_add_new_delivery_option(self, delivery_option):
        self.create_delivery_option(delivery_option)
        self.wait_for_element_is_visible(self.get_specific_table_cell(delivery_option['name']))
        results = []
        results.append(self.is_element_present(self.delivery_management_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_cell(delivery_option['name'])))
        results.append(self.is_element_present(self.get_specific_table_cell(delivery_option['description'])))
        return all(results)

    def verify_edit_delivery_option(self, delivery_option_name, edit_delivery_option):
        self.edit_delivery_option(delivery_option_name, edit_delivery_option)
        self.wait_for_element_is_visible(self.get_specific_table_cell(edit_delivery_option['name']))
        results = []
        results.append(self.wait_for_element(self.delivery_management_list_table_row))
        results.append(self.is_element_present(self.get_specific_table_cell(edit_delivery_option['name'])))
        results.append(self.is_element_present(self.get_specific_table_cell(edit_delivery_option['description'])))
        print(results)

        return all(results)

    def verify_delete_delivery_option(self, delivery_option_name):
        self.delete_delivery_option(delivery_option_name)
        self.refresh_page()
        return self.is_element_present(self.get_specific_table_cell(delivery_option_name))
