from tests.pages.dashboard.base_page import BasePage


class PromotionsPage(BasePage):
    page_title = page_header = 'Promotions'
    promotion_page_add_new_button = '//span[contains(text(),"Create a new product...")]'
    promotion_page_name_field = '//input[@placeholder="Name"]'
    promotion_page_desc_field = '//input[@placeholder="Description"]'
    promotion_page_amount_field = '//input[@placeholder="Amount"]'
    promotion_page_image_field = '//input[@name="logo"]'
    promotion_page_save_button = '//button[span[contains(text(),"Save")]]'
    promotion_page_confirm_yes_button = '//span[contains(text(),"Yes")]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    def get_specific_name(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]'

    def get_specific_cell_information(self, name, cell_content):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//mat-cell[div[contains(text(),"{cell_content}")]]'

    promotion_page_edit_button = '//button[contains(text(),"Edit")]'
    promotion_page_delete_button = '//button[contains(text(),"Delete")]'

    def verify_promotion_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_promotion_page_header(self):
        return self.verify_page_header(self.page_header)

    # todo add else part when images are uploaded when saved
    def add_image(self, *args):
        if self.is_element_present(self.promotion_page_image_field):
            self.submit_image(args, self.promotion_page_image_field)
        else:
            pass

    def create_new_promotion(self, promotion_info):
        self.wait_for_element_is_visible(self.promotion_page_add_new_button).click()
        self.input_keys(promotion_info['name'], self.promotion_page_name_field)
        self.input_keys(promotion_info['desc'], self.promotion_page_desc_field)
        self.input_keys(promotion_info['amount'], self.promotion_page_amount_field)
        self.add_image(promotion_info['image'])
        self.wait_for_element_is_visible(self.promotion_page_save_button).click()

    def edit_promotion(self, promotion_name, edited_promotion):
        self.wait_for_element(self.get_specific_actions_button(promotion_name)).click()
        self.wait_for_element(self.promotion_page_edit_button).click()
        self.wait_for_element_is_visible(self.promotion_page_name_field)
        self.input_keys(edited_promotion['name'], self.promotion_page_name_field)
        self.input_keys(edited_promotion['desc'], self.promotion_page_desc_field)
        self.input_keys(edited_promotion['amount'], self.promotion_page_amount_field)
        self.add_image(edited_promotion['image2'])
        self.wait_for_element_is_visible(self.promotion_page_save_button).click()

    def verify_new_promotion(self, promotion_info):
        self.create_new_promotion(promotion_info)
        self.wait_for_element_is_visible(self.get_specific_name(promotion_info['name']))
        return self.is_element_present(self.get_specific_name(promotion_info['name']))

    def delete_promotion(self, promotion_name):
        self.wait_for_element(self.get_specific_actions_button(promotion_name)).click()
        self.wait_for_element(self.promotion_page_delete_button).click()
        self.wait_for_element_is_visible(self.promotion_page_confirm_yes_button).click()

    def verify_edit_promotion(self, promotion_name, edited_promotion_info):
        self.edit_promotion(promotion_name, edited_promotion_info)
        self.wait_for_element_is_visible(self.get_specific_name(edited_promotion_info['name']))
        results = []
        results.append(self.is_element_present(
            self.get_specific_cell_information(edited_promotion_info['name'], edited_promotion_info['desc'])))
        results.append(self.is_element_present(
            self.get_specific_cell_information(edited_promotion_info['name'], edited_promotion_info['amount'])))
        return all(results)

    def verify_delete_promotion(self, edited_promotion_name):
        self.delete_promotion(edited_promotion_name)
        self.click_nav_menu_buttons(self.nav_settings_promotions_button)
        return self.is_element_present(self.get_specific_name(edited_promotion_name))
