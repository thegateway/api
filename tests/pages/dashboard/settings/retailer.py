from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging
from random import randrange

class RetailerPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Create New retailer Page Locators ##############
    page_title = 'Retailers'
    page_header = 'Retailer'
    retailer_add_new_button = '//mat-card/div/a[@href="/webshop/shop"]'
    retailer_form_name = '//input[@placeholder="Name"]'
    retailer_short_name = '//input[@name="shorthand_name"]'
    retailer_form_nip = '//input[@placeholder="NIP (Tax Idenfitication Number)"]'
    retailer_form_person_in_charge = '//input[@placeholder="Person in charge"]'
    retailer_form_location_button = '//button[contains(span/text(), "Select Location")]'
    retailer_form_current_locations = '//mat-cell/div'
    retailer_form_select_button = '//mat-dialog-actions/button[contains(span/text(), "Select")]'
    retailer_show_more_button = '//div[a/fa[@name="caret-down"]]/a'

    def select_random_location_button(self, choice):
        return f'//mat-row[mat-cell[contains(div/text(),"{choice}")]]//mat-checkbox'

    retailer_form_new_email_button = '//button[contains(span/text(), "Add email")]'
    retailer_form_new_email_field = '//input[@placeholder="Email"]'
    retailer_form_new_email_desc_field = '//input[@placeholder="Description (e.g. Help desk or Returns)"]'
    retailer_form_new_phone_number_button = '//button[contains(span/text(), "Add phone number")]'
    retailer_form_new_phone_field = '//input[@placeholder="Phone number"]'
    retailer_form_new_phone_desc_field = '//input[@placeholder="Description (e.g. Main Office or Help line)"]'
    retailer_form_add_website_button = '//button[contains(span/text(), "Add website")]'
    retailer_form_new_logo = '//input[@name="logo"]'
    retialer_comission_field = '//input[@placeholder="PayU comission"]'
    retailer_form_new_url = '//input[@placeholder="URL"]'
    retailer_form_delete_image_button = '//button[span/fa/i[@class="fa fa-trash"]]'
    retailer_form_remove_saved_image = '//div[img]/div[a/fa[@name="minus-circle"]]/a'
    retailer_form_new_social_media_button = '//button[contains(span/text(), "Add social media")]'
    retailer_social_media_dropdown_list = '//div[contains(mat-select/div/div/span/text(),"Platform")]'
    retailer_social_media_options = '//mat-option'
    retailer_social_media_link = '//input[@placeholder="Link"] | //input[@placeholder="Analytics"]'
    retailer_form_stripe_field = '//input[@placeholder="Stripe comission"]'
    retailer_form_desc_field = '//textarea[@placeholder="Description"]'

    def select_specific_social_media(self, social_media):
        return f'//mat-option[contains(span/text(),"{social_media}")]'

    def get_specific_delete_button(self, name):
        return f'//div[contains(text(),"{name}")]//a/fa'

    def get_speific_edit_button(self, name):
        return f'//div[contains(text(),"{name}")]/fa'

    retailer_delete_confirm_button = '//button[contains(span/text(), "Yes")]'
    retailer_delete_cancel_button = '//button[contains(span/text(), "No")]'
    retailer_edit_delete_button = '//button[contains(span/text(), "Delete")]'
    retailer_cancel_button = '//button[contains(span/text(), "Cancel")]'
    retailer_save_button = '//mat-dialog-actions/button[contains(span/text(), "Save")]'
    retailer_saved_success_msg = '//div[contains(text(),"Saved!")]'
    retailer_save_retailer_button = '//button[contains(span/text(), "Save")]'
    retailer_error_popup = '//button[span[contains(text(),"OK")]]'

    def specific_retailer_name(self, name):
        return f'//div[contains(text(),"{name}")]'

    def specific_retailer_slider(self, name):
        return f'//mat-row[mat-cell/div[contains(text(),"{name}")]]//mat-slide-toggle/label'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    retailer_edit_button = '//button[contains(text(),"Edit")]'
    retailer_delete_button = '//button[contains(text(),"Delete")]'

    def specific_retailer_image(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//img[contains(@src,".jpg")]'

    def check_if_location_already_checked(self, name):
        return f'//mat-row[mat-cell[contains(div/text(),"{name}")]]//input[@aria-checked="false"]'

    retailer_form = '//app-shop/div/mat-card[2]'

    #########################################################

    def click_retailer_add_new_button(self):
        self.focus_element(self.retailer_add_new_button)
        self.wait_for_element(self.retailer_add_new_button).click()

    def click_retailer_delete_button(self, name):
        self.wait_for_element(self.get_specific_actions_button(name)).click()
        self.wait_for_element_is_visible(self.retailer_delete_button).click()

    def add_logo(self, *args):
        if self.is_element_present(self.retailer_form_new_logo):
            self.submit_image(args, self.retailer_form_new_logo)
        else:
            self.focus_element(self.retailer_form_remove_saved_image)
            self.wait_for_element(self.retailer_form_remove_saved_image).click()
            self.focus_element(self.retailer_delete_confirm_button)
            self.wait_for_element(self.retailer_delete_confirm_button).click()
            self.submit_image(args, self.retailer_form_new_logo)

    def add_location(self):
        self.focus_element(self.retailer_form_location_button)
        self.wait_for_element(self.retailer_form_location_button).click()
        self.wait_for_element_is_visible(self.retailer_form_current_locations)
        random_location = self.get_element_list(self.retailer_form_current_locations)
        selected_location = random_location[randrange(len(random_location))].text
        if self.is_element_present(self.check_if_location_already_checked(selected_location)):
            self.focus_element(self.select_random_location_button(selected_location))
            self.wait_for_element_is_visible(self.select_random_location_button(selected_location)).click()
        self.wait_for_element(self.retailer_form_select_button).click()

    def add_email(self, email, desc):
        self.input_keys(email, self.retailer_form_new_email_field)
        self.input_keys(desc, self.retailer_form_new_email_desc_field)
        self.wait_for_element_is_visible(self.retailer_save_button).click()

    def add_phone_number(self, number, desc):
        self.input_keys(number, self.retailer_form_new_phone_field)
        self.input_keys(desc, self.retailer_form_new_phone_desc_field)
        self.wait_for_element_is_visible(self.retailer_save_button).click()

    def create_retailer(self, retailer_info):
        self.click_nav_menu_buttons(self.nav_settings_retailer_button)
        self.click_retailer_add_new_button()
        self.input_keys(retailer_info['name'], self.retailer_form_name)
        self.input_keys(retailer_info['short_name'], self.retailer_short_name)
        self.input_keys(retailer_info['NIP'], self.retailer_form_nip)
        self.input_keys(retailer_info['payu'], self.retialer_comission_field)
        self.input_keys(retailer_info['desc'], self.retailer_form_desc_field)
        self.focus_element(self.retailer_form_person_in_charge)
        self.input_keys(retailer_info['person'], self.retailer_form_person_in_charge)
        self.add_logo(retailer_info['image'])
        self.add_location()
        self.wait_for_element(self.retailer_form_new_email_button).click()
        self.add_email(retailer_info['email'], retailer_info["email_desc"])
        self.wait_for_element(self.retailer_form_new_phone_number_button).click()
        self.add_phone_number(retailer_info['phone'], retailer_info['phone_desc'])
        self.click_element_js(self.get_element(self.retailer_show_more_button))
        self.focus_element(self.retailer_form_add_website_button)
        self.wait_for_element_is_visible(self.retailer_form_add_website_button).click()
        self.input_keys(retailer_info['url'], self.retailer_form_new_url)
        self.wait_for_element(self.retailer_save_button).click()
        self.focus_element(self.retailer_save_retailer_button)
        self.wait_for_element(self.retailer_save_retailer_button).click()

    def edit_retailer(self, edited_retailer_info, retailer_info):
        self.handle_error_popups()
        self.wait_for_element_is_visible(self.get_specific_actions_button(retailer_info['name'])).click()
        self.wait_for_element_is_visible(self.retailer_edit_button).click()
        self.wait_for_element_is_visible(self.retailer_form)
        self.input_keys(edited_retailer_info['name'], self.retailer_form_name)
        self.input_keys(edited_retailer_info['short_name'], self.retailer_short_name)
        self.input_keys(edited_retailer_info['NIP'], self.retailer_form_nip)
        self.input_keys(edited_retailer_info['payu'], self.retialer_comission_field)
        self.input_keys(edited_retailer_info['desc'], self.retailer_form_desc_field)
        self.input_keys(edited_retailer_info['person'], self.retailer_form_person_in_charge)
        self.add_logo(edited_retailer_info['image2'])
        self.add_location()
        self.wait_for_element(self.get_speific_edit_button(retailer_info['email'])).click()
        self.add_email(edited_retailer_info['email'], edited_retailer_info["email_desc"])
        self.wait_for_element(self.get_speific_edit_button(retailer_info['phone'])).click()
        self.add_phone_number(edited_retailer_info['phone'], edited_retailer_info['phone_desc'])
        self.focus_element(self.retailer_save_retailer_button)
        self.element_click(self.retailer_save_retailer_button)
        self.click_nav_menu_buttons(self.nav_settings_retailer_button)

    def delete_retailer(self, retailer_name):
        self.handle_error_popups()
        self.click_retailer_delete_button(retailer_name)
        self.focus_element(self.retailer_delete_confirm_button)
        self.wait_for_element(self.retailer_delete_confirm_button).click()

    def verify_retailer_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_retailer_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_add_new_retailer(self, retailer_info):
        self.create_retailer(retailer_info)
        results = []
        self.wait_for_element_is_visible(self.specific_retailer_name(retailer_info['name']))
        results.append(self.is_element_present(self.specific_retailer_image(retailer_info['name'])))
        results.append(self.is_element_present(self.specific_retailer_name(retailer_info['name'])))

        return all(results)

    def verify_edit_retailer(self, edited_retailer_info, retailer_info):
        self.edit_retailer(edited_retailer_info, retailer_info)
        self.click_nav_menu_buttons(self.nav_settings_retailer_button)
        results = []
        self.wait_for_element_is_visible(self.specific_retailer_name(edited_retailer_info['name']))
        results.append(self.is_element_present(self.specific_retailer_image(edited_retailer_info['name'])))
        results.append(self.is_element_present(self.specific_retailer_name(edited_retailer_info['name'])))
        return all(results)

    def verify_delete_retailer(self, retailer_name):
        self.refresh_page()
        self.delete_retailer(retailer_name)
        self.refresh_page()
        return self.is_element_present(self.specific_retailer_name(retailer_name))
