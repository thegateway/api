from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class UsersManagementPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Users management Page Locators ##############
    page_title = page_header = 'Users'
    search_field = '//input[@placeholder="Search"]'
    search_result_rows = '//mat-row'

    def get_specific_user_edit_button(self, username):
        return f'//mat-row[contains(mat-cell[4]/div/text(), "{username}")]//a[1]'

    def get_specific_user_delete_button(self, username):
        return f'//mat-row[contains(mat-cell[4]/div/text(), "{username}")]//a[2]'

    select_retailer_dropdown_button = '//mat-select[@id="mat-select-0"]'
    select_brand_dropdown_button = '//mat-select[@id="mat-select-1"]'

    def get_specific_shop_from_dropdown(self, shop_name):
        return f'//mat-option//span[contains(text(), "{shop_name}")]'

    def get_specific_brand_from_dropdown(self, brand):
        return f'//mat-option//span[contains(text(), "{brand}")]'

    dialog_box_select_button = '//mat-dialog-actions/button[2]'
    dialog_box_cancel_button = '//mat-dialog-actions/button[1]'

    ###############################################

    def submit_search_query(self, search_query):
        self.submit_input_keys(search_query, self.search_field)

    def assign_webshop_to_user(self, username, web_shop):
        self.focus_element(self.get_specific_user_edit_button(username))
        self.wait_for_element(self.get_specific_user_edit_button(username)).click()
        self.wait_for_element(self.select_retailer_dropdown_button).click()
        self.wait_for_element(self.get_specific_shop_from_dropdown(web_shop)).click()
        self.focus_element(self.dialog_box_select_button)
        self.wait_for_element(self.dialog_box_select_button).click()

    def verify_users_management_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_users_management_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_search_field_working(self, search_query=''):
        result_before_search = len(self.get_element_list(self.search_result_rows))
        self.submit_search_query(search_query)
        result_after_search = len(self.get_element_list(self.search_result_rows))

        return 0 < result_after_search < result_before_search


