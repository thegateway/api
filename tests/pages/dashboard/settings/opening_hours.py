from tests.pages.dashboard.base_page import BasePage
from random import randrange


class OpeningHoursPage(BasePage):
    page_title = page_header = 'Opening hours'
    hours_page_add_new_button = '//a[span[contains(text(),"Add new")]]'
    hours_page_add_unusual_button = '//a[span[contains(text(),"Add expe")]]'
    hours_page_day_dropdown_button = '//div[mat-select[@aria-label="Weekday"]]'
    hours_page_dropdown_options = '//mat-option'
    hours_page_opening_hours_field = '//input[@placeholder="Opens at"]'
    hours_page_closing_hours_field = '//input[@placeholder="Closes at"]'
    hours_page_save_button = '//span[contains(text(),"Save")]'
    hours_page_date_field = '//input[@placeholder="Date"]'
    hours_page_desc_field = '//input[@placeholder="Description"]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    def select_random_weekday(self, name):
        return f'//mat-option[span[contains(text(),"{name}")]]'

    def get_speficifc_cell(self, name, info):
        return f'//mat-row[mat-cell/div[contains(text(),"{name}")]]//mat-cell[div[contains(text(),"{info}")]]'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell[div[contains(text(),"{name}")]]]//button'

    hours_page_edit_button = '//button[contains(text(),"Edit")]'
    hours_page__delete_button = '//button[contains(text(),"Delete")]'
    hours_page_yes_button = '//button[span[contains(text(),"Yes")]]'

    def verify_brand_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_brand_page_header(self):
        return self.verify_page_header(self.page_header)

    def select_weekday(self):
        self.wait_for_element(self.hours_page_day_dropdown_button).click()
        weekdays = self.get_element_list(self.hours_page_dropdown_options)
        selected_weekday = weekdays[randrange(len(weekdays))].text
        self.wait_for_element(self.select_random_weekday(selected_weekday)).click()
        return selected_weekday

    def create_opening_hours(self, hours_info):
        self.wait_for_element_is_visible(self.hours_page_add_new_button).click()
        selected_day = self.select_weekday()
        self.input_keys(hours_info['start_time'], self.hours_page_opening_hours_field)
        self.input_keys(hours_info['end_time'], self.hours_page_closing_hours_field)
        self.wait_for_element(self.hours_page_save_button).click()
        return selected_day

    def create_experimental_hours(self, exp_hours):
        self.wait_for_element(self.hours_page_add_unusual_button).click()
        self.input_date(exp_hours['date'])
        self.input_keys(exp_hours['desc'], self.hours_page_desc_field)
        self.input_keys(exp_hours['start_time'], self.hours_page_opening_hours_field)
        self.input_keys(exp_hours['end_time'], self.hours_page_closing_hours_field)
        self.wait_for_element(self.hours_page_date_field).click()
        self.wait_for_element(self.hours_page_save_button).click()


    def edit_opening_hours(self, opening_hours, edited_hours):
        self.wait_for_element(self.get_specific_actions_button(opening_hours['selected_day'])).click()
        self.wait_for_element(self.hours_page_edit_button).click()
        self.input_keys(edited_hours['start_time'], self.hours_page_opening_hours_field)
        self.input_keys(edited_hours['end_time'], self.hours_page_closing_hours_field)
        self.wait_for_element(self.hours_page_save_button).click()

    def edit_exp_hours(self, exp_hours, edited_exp_hours):
        self.wait_for_element(self.get_specific_actions_button(exp_hours['desc'])).click()
        self.wait_for_element(self.hours_page_edit_button).click()
        self.input_date(edited_exp_hours['date'])
        self.input_keys(edited_exp_hours['desc'], self.hours_page_desc_field)
        self.input_keys(edited_exp_hours['start_time'], self.hours_page_opening_hours_field)
        self.input_keys(edited_exp_hours['end_time'], self.hours_page_closing_hours_field)
        self.wait_for_element(self.hours_page_date_field).click()
        self.wait_for_element(self.hours_page_save_button).click()


    def delete_opening_hours(self,edited_hours_info):
        self.wait_for_element(self.get_specific_actions_button(edited_hours_info['selected_day'])).click()
        self.wait_for_element(self.hours_page__delete_button).click()
        self.wait_for_element(self.hours_page_yes_button).click()

    def delete_exp_opening_hours(self, edited_hours_info):
        self.wait_for_element(self.get_specific_actions_button(edited_hours_info['selected_day'])).click()

    def verify_new_times_is_Added(self, hours_info):
        selected_day = self.create_opening_hours(hours_info)
        results = []
        results.append(self.is_element_present(self.get_speficifc_cell(selected_day, selected_day)))
        results.append(self.is_element_present(self.get_speficifc_cell(selected_day, hours_info['start_time'])))
        results.append(self.is_element_present(self.get_speficifc_cell(selected_day, hours_info['end_time'])))
        if all(results):
            hours_info['selected_day'] = selected_day
        return all(results)

    def verify_time_is_edited(self, opening_hours, edited_hours):
        self.edit_opening_hours(opening_hours, edited_hours)
        results = []
        results.append(self.is_element_present(self.get_speficifc_cell(opening_hours['selected_day'], opening_hours['selected_day'])))
        results.append(self.is_element_present(self.get_speficifc_cell(opening_hours['selected_day'], edited_hours['start_time'])))
        results.append(self.is_element_present(self.get_speficifc_cell(opening_hours['selected_day'], edited_hours['end_time'])))
        if all(results):
            edited_hours['selected_day'] = opening_hours['selected_day']
        return all(results)

    def verify_time_is_deleted(self, edited_hours_info):
        self.delete_opening_hours(edited_hours_info)
        return self.is_element_present(self.get_speficifc_cell(edited_hours_info['selected_day'],edited_hours_info['selected_day']))

    def verify_experimental_time_is_added(self, exp_hours):
        self.create_experimental_hours(exp_hours)
        return self.is_element_present(self.get_speficifc_cell(exp_hours['desc'], exp_hours['desc']))

    def verify_exp_hours_is_edited(self, exp_hours, edited_exp_hours):
        self.edit_exp_hours(exp_hours,edited_exp_hours)
        return self.is_element_present(self.get_speficifc_cell(edited_exp_hours['desc'], edited_exp_hours['desc']))

    def verify_exp_hours_is_deleted(self, edited_exp_hours):
        self.delete_exp_opening_hours(edited_exp_hours['desc'])
        return self.is_element_present(self.get_speficifc_cell(edited_exp_hours['desc'], edited_exp_hours['desc']))




