from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class BrandsPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Brands Page Locators ##############
    page_title = page_header = 'Brands'
    brands_add_new_button = '//a[contains(span/text(), "Add new")]'
    brand_form_save_button = '//button[contains(span/text(), "Save")]'
    brand_form_cancel_button = '//button[contains(span/text(), "Cancel")]'
    brand_delete_confirm_button = '//button[contains(span/text(), "Yes")]'
    brand_delete_cancel_button = '//button[contains(span/text(), "No")]'

    brand_dialog_name_field = '//mat-form-field[1]//input[@placeholder="Name"]'
    brand_contact_name_field = '//mat-form-field[2]//input[@placeholder="Name"]'
    brand_contact_email_field = '//input[@placeholder="E-mail"]'
    brand_contact_phone_field = '//input[@placeholder="Phone number"]'
    brand_contact_address_field = '//input[@placeholder="Address"]'
    brand_mat_table = '//mat-table'

    def get_specific_actions_button(self, name):
        return f'//mat-row[mat-cell/div[contains(text(),"{name}")]]//button'

    brand_edit_button = '//button[contains(text(),"Edit")]'
    brand_delete_button = '//button[contains(text(),"Delete")]'


    def get_specific_brand_name_field(self, name):
        return f'//mat-cell[contains(div/text(), "{name}")]/div'

    #########################################################

    def click_brands_add_new_button(self):
        self.focus_element(self.brands_add_new_button)
        self.wait_for_element(self.brands_add_new_button).click()

    def create_brand(self, brand_info):
        self.click_brands_add_new_button()
        self.input_keys(brand_info['dialog_name'], self.brand_dialog_name_field)
        self.input_keys(brand_info['contact_name'], self.brand_contact_name_field)
        self.input_keys(brand_info['contact_email'], self.brand_contact_email_field)
        self.input_keys(brand_info['contact_phone'], self.brand_contact_phone_field)
        self.input_keys(brand_info['contact_address'], self.brand_contact_address_field)
        self.wait_element_to_appear(self.brand_form_save_button)
        self.focus_element(self.brand_form_save_button)
        self.wait_for_element_is_visible(self.brand_form_save_button).click()

    def delete_brand(self, name):
        self.wait_for_element(self.get_specific_actions_button(name)).click()
        self.wait_for_element(self.brand_delete_button).click()
        self.focus_element(self.brand_delete_confirm_button)
        self.wait_for_element(self.brand_delete_confirm_button).click()

    def verify_brand_page_title(self):
        return self.verify_page_title(self.page_title)

    def verify_brand_page_header(self):
        return self.verify_page_header(self.page_header)

    def verify_add_new_brand(self, brand_info):
        self.create_brand(brand_info)
        self.refresh_page()
        self.wait_element_to_appear(self.brand_mat_table)
        return self.is_element_present(self.get_specific_brand_name_field(brand_info['dialog_name']))

    def edit_brand(self, edited_brand_info, brand_name):
        self.wait_for_element(self.get_specific_actions_button(brand_name)).click()
        self.wait_for_element(self.brand_edit_button).click()
        self.input_keys(edited_brand_info['dialog_name'], self.brand_dialog_name_field)
        self.input_keys(edited_brand_info['contact_name'], self.brand_contact_name_field)
        self.input_keys(edited_brand_info['contact_email'], self.brand_contact_email_field)
        self.input_keys(edited_brand_info['contact_phone'], self.brand_contact_phone_field)
        self.input_keys(edited_brand_info['contact_address'], self.brand_contact_address_field)
        self.focus_element(self.brand_form_save_button)
        self.wait_for_element_is_visible(self.brand_form_save_button).click()

    #todo change brand_name to edited_brand_info['dialog_name'] when bug is fixed
    def verify_edit_brand(self, edited_brand_info, brand_name):
        self.edit_brand(edited_brand_info, brand_name)
        self.refresh_page()
        self.wait_for_element_is_visible(self.brand_mat_table)
        return self.is_element_present(self.get_specific_brand_name_field(brand_name))

    def verify_delete_brand(self, name):
        self.delete_brand(name)
        self.refresh_page()
        return self.is_element_present(self.get_specific_brand_name_field(name))

