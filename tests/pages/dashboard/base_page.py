from tests.base.base_driver_methods import BaseDriver
from selenium.common.exceptions import *
import tests.utilities.custom_log as cl
import logging


class BasePage(BaseDriver):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ################ Base Page Locators ################
    logout_button = '//div[2]/button[2]'
    logout_button_text = '//button[span/fa[@name="user-circle"]]/span/div'
    title_locator = '//title'
    header_locator = '//h2'
    basepage_error_ok_button = '//mat-dialog-actions/button'
    basepage_error_popup = '//div[contains(@class,"toast-error")][1]'
    base_page_error_popup_list = '(//mat-dialog-actions/button)'
    basepage_account_button = '//div[contains(text(),"Ilari")]'
    basepage_english_language = '//div[contains(text(),"English")]'
    basepage_check_language = '//p[contains(text(),"Witaj")]'

    def error_popup_handler(self, count):
        return f'(//mat-dialog-actions/button)[{count}]'

    nav_menu_button = '//mat-toolbar/div[1]/button'
    nav_home_button = '//li[a/span/fa/i[@class="fa fa-home"]]'
    nav_orders_button = '//li[a/span/fa/i[@class="fa fa-shopping-cart"]]'
    nav_products_button = '//li[a/span/fa/i[@class="fa fa-database"]]'
    nav_reporting_button = '//li[a/span/fa/i[@class="fa fa-area-chart"]]'
    nav_campaigns_button = '//li[a/span/fa/i[@class="fa fa-bullseye"]]'
    nav_settings_button = '//li[a/span/fa/i[@class="fa fa-chevron-circle-down" or @class="fa fa-chevron-circle-up"]]'

    # ============ Nav Settings Buttons =====================
    nav_settings_system_settings_button = '//li[a/span/fa/i[@class="fa fa-sliders"]]'
    nav_settings_users_button = '//li[a/span/fa/i[@class="fa fa-users"]]'
    nav_settings_delivery_options_button = '//li[a/span/fa/i[@class="fa fa-truck"]]'
    nav_settings_payment_options_button = '//li[a/span/fa/i[@class="fa fa-credit-card"]]'
    nav_settings_retailer_button = '//li[a/span/fa/i[@class="fa fa-globe"]]'
    nav_settings_categories_button = '//li[a/span/fa/i[@class="fa fa-list"]]'
    nav_settings_brands_button = '//li[a/span/fa/i[@class="fa fa-briefcase"]]'
    nav_settings_opening_hours_button = '//li[a/span/fa/i[@class="fa fa-calendar"]]'
    nav_settings_promotions_button = '//li[a/span/fa/i[@class="fa fa-tags"]]'
    # =====================================================

    #######################################################

    def get_logout_button_text(self):
        if self.is_element_present(self.basepage_error_popup):
            self.wait_for_element(self.basepage_error_popup).click()
        self.wait_for_element_is_visible(self.logout_button)
        if '(' in self.get_text(self.logout_button_text):
            return True
        else:
            return False

    def logout(self):
        try:
            self.wait_for_element(self.logout_button).click()

        except NoSuchElementException:
            self.log.error(f'Failed to logout')
            self.log.exception()

    def verify_page_title(self, title_to_verify):
        try:
            actual_title = self.get_text(self.title_locator)
            return title_to_verify in actual_title
        except TypeError:
            self.log.error(f'Actual title: {actual_title} and expected title: {title_to_verify} does not matched')

    def verify_page_header(self, header_to_verify):
        try:
            actual_header = self.get_text(self.header_locator)
            return header_to_verify in actual_header
        except TypeError:
            self.log.error(f'Actual header: {actual_header} and expected header: {header_to_verify} does not matched')

    def handle_error_popups(self):
        if self.is_element_present(self.basepage_error_ok_button) and self.is_element_clickable(
                self.basepage_error_ok_button):
            element_list = self.get_element_list(self.base_page_error_popup_list)
            for error in range(len(element_list)):
                error_popup_index = len(element_list)
                self.wait_for_element_is_visible(self.error_popup_handler(error_popup_index)).click()
                element_list.pop(error_popup_index - 1)

    def click_nav_menu_buttons(self, element):
        if self.is_element_displayed(element):
            if self.is_element_present(self.basepage_error_ok_button):
                self.handle_error_popups()
            self.wait_for_element_is_visible(element).click()
            self.wait_for_element_is_visible(self.header_locator)

        else:
            self.handle_error_popups()
            self.wait_for_element(self.nav_settings_button).click()
            if self.is_element_displayed(element):
                self.handle_error_popups()
                self.wait_for_element(element).click()
                self.wait_for_element_is_visible(self.header_locator)

    def verify_nav_menu_working(self):
        results = []

        self.click_nav_menu_buttons(self.nav_home_button)
        results.append(self.verify_page_title('Home'))

        self.click_nav_menu_buttons(self.nav_products_button)
        results.append(self.verify_page_title('Products'))

        self.click_nav_menu_buttons(self.nav_reporting_button)
        results.append(self.verify_page_title('Reporting and Insights'))

        self.click_nav_menu_buttons(self.nav_campaigns_button)
        results.append(self.verify_page_title('Campaigns'))

        self.click_nav_menu_buttons(self.nav_settings_button)
        results.append(self.is_element_displayed(self.nav_settings_system_settings_button))

        return all(results)

    def verify_nav_settings_menu_working(self):
        results = []

        self.click_nav_menu_buttons(self.nav_settings_system_settings_button)
        results.append(self.verify_page_header('System settings'))

        self.click_nav_menu_buttons(self.nav_settings_delivery_options_button)
        results.append(self.verify_page_header('Delivery options'))

        self.click_nav_menu_buttons(self.nav_settings_payment_options_button)
        results.append(self.verify_page_header('Payment options'))

        self.click_nav_menu_buttons(self.nav_settings_retailer_button)
        results.append(self.verify_page_header("Retailer"))

        self.click_nav_menu_buttons(self.nav_settings_categories_button)
        results.append(self.verify_page_header('Categories'))

        self.click_nav_menu_buttons(self.nav_settings_brands_button)
        results.append(self.verify_page_header('Brands'))

        return all(results)

    def change_language(self):
        if self.is_element_present(self.basepage_check_language):
            self.element_click(self.basepage_account_button)
            self.wait_for_element_is_visible(self.basepage_english_language).click()
