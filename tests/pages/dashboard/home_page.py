from tests.pages.dashboard.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class HomePage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ########### Dashboard Page Locators ##############
    page_title = page_header = 'Home'

    ###############################################

    def verify_homePage_title(self):
        self.click_nav_menu_buttons(self.nav_home_button)
        return self.verify_page_title(self.page_title)

    def verify_homePage_header(self):
        self.wait_for_element_is_visible(self.page_header)
        return self.verify_page_header(self.page_header)
