from tests.pages.sma_frontend.base_page import BasePage
import tests.utilities.custom_log as cl
import logging


class LoginPage(BasePage):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    ####Locators#####
    title = "//title"
    header = '//header'
    nav_menu_button = '//ion-toolbar[ion-title[contains(text(),"Home")]]//ion-menu-button'
    login_page_homepage_login_button = '//ion-button[contains(text(),"Log in")]'
    login_page_email_field = '//input[@type="text"]'
    login_page_password_field = '//input[@type="password"]'
    login_page_forgot_password_button = '//ion-item[a[contains(text(),"Forgot password")]]'
    login_page_register_button = '//ion-item[a[contains(text(),"Register a new account")]]'
    login_page_confirm_button = '//ion-item[a[contains(text(),"Confirm account")]]'

    def login(self, account_information):
        if self.is_element_clickable(self.login_page_homepage_login_button) is False:
            self.click_nav_menu_button(self.nav_menu_button)
        self.input_keys(account_information['email'], self.login_page_email_field)
        self.input_keys(account_information['password'], self.login_page_password_field)
        self.wait_for_element_is_visible(self.login_page_homepage_login_button).click()

