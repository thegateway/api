from tests.base.base_driver_methods import BaseDriver
from selenium.common.exceptions import *
import tests.utilities.custom_log as cl
import logging


class BasePage(BaseDriver):
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver


    ####locators####

    def header_locator(self, name):
        return f'//ion-title[contains(text(),"{name}")]'

    back_button = '//ion-back-button'
    title_locator = '//title'
    nav_menu_button = '//ion-toolbar[ion-title[contains(text(),"Home")]]//ion-menu-button'
    nav_menu_profile_button = '//ion-item[contains(text(),"My profile")]'
    nav_menu_orders_button = '//ion-item[contains(text(),"My orders")]'
    nav_menu_payment_cards_button = '//ion-item[contains(text(),"My payment cards")]'
    nav_menu_my_address_button = '//ion-item[contains(text(),"My addresses")]'
    nav_menu_logout_button = '//ion-item[contains(text(),"Logout")]'

    nav_home_tab_button = '//ion-tab-button[ion-label[contains(text(),"Home")]]'
    nav_shop_tab_button = '//ion-tab-button[ion-label[contains(text(),"Shop")]]'
    nav_campaigns_tab_button = '//ion-tab-button[ion-label[contains(text(),"Campaigns")]]'
    nav_info_tab_button = '//ion-tab-button[ion-label[contains(text(),"Info")]]'
    nav_favourites_tab_button = '//ion-tab-button[ion-label[contains(text(),"Favorites")]]'


    def verify_page_title(self, title_to_verify):
        try:
            actual_title = self.get_text(self.title_locator)
            return title_to_verify in actual_title
        except TypeError:
            self.log.error(f'Actual title: {actual_title} and expected title: {title_to_verify} does not matched')


    def verify_page_header(self, header_to_verify):
        try:
            actual_header = self.get_text(self.header_locator(header_to_verify))
            return header_to_verify in actual_header
        except TypeError:
            self.log.error(f'Actual header: {actual_header} and expected header: {header_to_verify} does not matched')

    def click_nav_menu_button(self, element):
        if self.is_element_displayed(element):
            self.wait_for_element_is_visible(element).click()
        else:
            if self.is_element_clickable(self.back_button):
                self.wait_for_element_is_visible(self.back_button).click()
            self.wait_for_element_is_visible(self.nav_menu_button).click()
            if self.is_element_displayed(element):
                self.wait_for_element_is_visible(element).click()

    def click_tab_manu_button(self, element):
        if self.is_element_displayed(element) is False and self.is_element_clickable(self.nav_menu_logout_button):
            self.click_nav_menu_button(self.nav_menu_profile_button)
        else:
            self.wait_for_element_is_visible(element).click()

    def verify_nav_menu_working(self):
        results = []

        self.click_nav_menu_button(self.nav_menu_profile_button)
        results.append(self.verify_page_header("My profile"))

        self.click_nav_menu_button(self.nav_menu_orders_button)
        results.append(self.verify_page_header("My orders"))

        self.click_nav_menu_button(self.nav_menu_payment_cards_button)
        results.append(self.verify_page_header("My payment cards"))

        self.click_nav_menu_button(self.nav_menu_my_address_button)
        results.append(self.verify_page_header("My addresses"))

        self.click_tab_manu_button(self.nav_shop_tab_button)
        results.append(self.verify_page_header("Shop"))

        self.click_tab_manu_button(self.nav_campaigns_tab_button)
        results.append(self.verify_page_header("Campaigns"))

        self.click_tab_manu_button(self.nav_info_tab_button)
        results.append(self.verify_page_header("Info"))

        self.click_tab_manu_button(self.nav_favourites_tab_button)
        results.append(self.verify_page_header("Favorites"))

        return all(results)

