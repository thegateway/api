from faker import Faker
import random
import os

fake = Faker()


def project_path():
    tests_dir = f'{os.sep}tests'
    path = os.getcwd()
    while tests_dir in path:
        path, _ = os.path.split(path)
    return path


def generate_campaign_info():
    campaign_info = dict(
        name=fake.name(),
        start_date=fake.date_between(start_date="-3y", end_date="today"),
        end_date=fake.date_between(start_date="today", end_date="+3y"),
        operator_type=random.choice(['Fixed unit price', 'Percentage']),
        operator_value=random.randint(1, 30),
        image=os.path.join(project_path(), 'tests', 'test_images', 'campaign.jpg'),
        image2=os.path.join(project_path(), 'tests', 'test_images', 'peukku.jpg'),

    )
    return campaign_info


def generate_brand_info():
    brand_info = dict(
        dialog_name=fake.first_name(),
        contact_name=fake.first_name(),
        contact_email=fake.email(),
        contact_phone=fake.phone_number(),
        contact_address=fake.address()
    )
    return brand_info


def generate_category_info():
    category_info = dict(
        name=fake.name(),
        image=os.path.join(project_path(), 'tests', 'test_images', 'category.jpg'),
    )
    return category_info


def generate_delivery_info():
    delivery_info = dict(
        name=fake.name(),
        description=fake.text(max_nb_chars=10, ext_word_list=None),
        logo_url=os.path.join(project_path(), 'tests', 'test_images', 'peukku.jpg'),
        logo_url2=os.path.join(project_path(), 'tests', 'test_images', 'shake.jpg'),
        price=random.randint(100, 1000),
        vat_class=random.choice(['23%', '8%', '5%']),
        retail_price=random.randint(200, 1000)
    )
    return delivery_info


def generate_payment_info():
    payment_info = dict(
        type='PayU',
        name=fake.name(),
        description=fake.text(max_nb_chars=10, ext_word_list=None),
        image=os.path.join(project_path(), 'tests', 'test_images', 'peukku.jpg'),
        image2=os.path.join(project_path(), 'tests', 'test_images', 'shake.jpg')
    )
    return payment_info


def generate_product_info():
    product_info = dict(
        name=fake.name(),
        brief_desc=fake.text(max_nb_chars=10, ext_word_list=None),
        description=fake.text(max_nb_chars=50, ext_word_list=None),
        sku=fake.text(max_nb_chars=10, ext_word_list=None),
        fabric=fake.text(max_nb_chars=10, ext_word_list=None),
        fit_sizing=fake.text(max_nb_chars=20, ext_word_list=None),
        return_exchanges=fake.text(max_nb_chars=20, ext_word_list=None),
        base_price=random.randint(100, 1000),
        separate_price=random.randint(100, 1000),
        image=os.path.join(project_path(), 'tests', 'test_images', 'peukku.jpg'),
        image2=os.path.join(project_path(), 'tests', 'test_images', 'shake.jpg'),
        variant_name=fake.text(max_nb_chars=10, ext_word_list=None),
        stock=random.randint(10, 1000),
    )
    return product_info


def generate_retailer_info():
    retailer_info = dict(
        name=fake.name(),
        short_name=fake.name(),
        NIP=random.randint(1, 20),
        person=fake.name(),
        image=os.path.join(project_path(), 'tests', 'test_images', 'peukku.jpg'),
        image2=os.path.join(project_path(), 'tests', 'test_images', 'shake.jpg'),
        email=fake.email(),
        email_desc=fake.text(max_nb_chars=50, ext_word_list=None),
        phone=fake.msisdn(),
        phone_desc=fake.text(max_nb_chars=50, ext_word_list=None),
        url=fake.url(schemes=None),
        sm_link=fake.url(schemes=None),
        stripe=fake.text(max_nb_chars=15, ext_word_list=None),
        desc=fake.text(max_nb_chars=30, ext_word_list=None),
        payu=random.randint(1,10)
    )
    return retailer_info


def generate_promotion_info():
    promotion_info = dict(
        name=fake.name(),
        desc=fake.text(max_nb_chars=50, ext_word_list=None),
        amount=random.randint(1, 99),
        image=os.path.join(project_path(), 'tests', 'test_images', 'peukku.jpg'),
        image2=os.path.join(project_path(), 'tests', 'test_images', 'shake.jpg'),
    )
    return promotion_info


def generate_opening_hours_info():
    hours_info = dict(
        start_time=str(random.randint(8, 12)) + ":" + str(random.randint(10, 59)),
        end_time=str(random.randint(13, 23)) + ":" + str(random.randint(10, 59)),
        desc=fake.text(max_nb_chars=50, ext_word_list=None),
        date=str(random.randint(2020, 2040)) + '-' + str(random.randint(1, 12)) + '-' + str(random.randint(1, 30)),
        selected_day='',
    )

    return hours_info
