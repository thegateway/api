from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


class CustomWebDriver:

    def __init__(self, browser, base_url, headless, mobile):
        self.browser = str.lower(browser)
        self.base_url = base_url
        self.headless = headless
        self.mobile = mobile

    def get_options(self, options):
        options.add_argument("--disable-extensions")
        if self.headless:
            options.add_argument('--window-size=1200,1100');
            options.add_argument('--disable-gpu');
            options.add_argument("--start-maximized");
            options.add_argument("--allow-insecure-localhost")
            options.add_argument("--acceptInsecureCerts")
            options.add_argument("--headless")
            options.add_argument("--no-sandbox")
            options.add_argument("--disable-dev-shm-usage")
        if self.mobile and self.browser == 'chrome':
            options.add_experimental_option("mobileEmulation", {"deviceName": "Galaxy S5"})
        return options

    def get_drier(self):
        if self.browser == 'firefox':
            return webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=self.get_options(FirefoxOptions()))
        return webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=self.get_options(ChromeOptions()))

    def get_custom_web_driver(self):
        driver = self.get_drier()
        driver.implicitly_wait(5)
        if self.headless is False:
            driver.maximize_window()
        driver.get(self.base_url)
        return driver
