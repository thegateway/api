from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import *
from selenium.webdriver import ActionChains
import tests.utilities.custom_log as cl
import logging
import time


class BaseDriver:
    log = cl.custom_logger(logging.DEBUG)

    def __init__(self, driver):
        self.driver = driver

    def get_by_type(self, locator_type):
        locators = {
            'id': By.ID,
            'name': By.NAME,
            'xpath': By.XPATH,
            'css': By.CSS_SELECTOR,
            'class': By.CLASS_NAME,
            'link': By.LINK_TEXT
        }
        try:
            return locators[locator_type.lower()]
        except KeyError:
            self.log.info(f'Locator type {locator_type} not correct/supported.')
            self.log.exception()

    def refresh_page(self):
        # try:
        time.sleep(.5)
        return self.driver.refresh()
        # finally:
        #     time.sleep(5)

    def get_element(self, locator, locator_type='xpath'):
        by_type = self.get_by_type(locator_type.lower())
        element = self.driver.find_element(by_type, locator)
        if element is None:
            raise NoSuchElementException(f'Element not found with locator: {locator} and locatorType: {locator_type}')
        else:
            return element

    def get_element_list(self, locator, locator_type='xpath'):
        by_type = self.get_by_type(locator_type.lower())
        elements = self.driver.find_elements(by_type, locator)
        if len(elements) == 0:
            raise NoSuchElementException(f'Element not found with locator: {locator} and locatorType: {locator_type}')
        else:
            return elements

    def get_text(self, locator, locator_type='xpath'):
        try:
            time.sleep(.3)
            element = self.get_element(locator, locator_type)
            text = element.get_attribute('textContent')
            return text
        except NoSuchAttributeException:
            self.log.error(f' Failed to found text on element with locator {locator} and locatorType: {locator_type}')

    def get_value(self, locator, locator_type='xpath'):
        try:
            time.sleep(.3)
            element = self.get_element(locator, locator_type)
            value = element.get_attribute('value')
            return value
        except NoSuchAttributeException:
            self.log.error(f' Failed to found Value on element with locator {locator} and locatorType: {locator_type}')

    def mouse_hover(self, locator, locator_type='xpath'):
        try:
            time.sleep(.3)
            element = self.get_element(locator, locator_type)
            action = ActionChains(self.driver)
            action.move_to_element(element).perform()
        except NoSuchElementException:
            self.log.info(f'Cannot hovered on the element with locator: {locator} and locatorType: {locator_type}')

    def element_click(self, locator, locator_type='xpath'):
        try:
            time.sleep(.3)
            element = self.get_element(locator, locator_type)
            element.click()
        except ElementNotInteractableException:
            self.log.info(f'Cannot click on the element with locator: {locator} and locatorType: {locator_type}')

    def focus_element(self, locator, locator_type='xpath'):
        try:
            time.sleep(.3)
            element = self.get_element(locator, locator_type)
            element.send_keys(Keys.NULL)
        except ElementNotInteractableException:
            self.log.info(f'Cannot sent data on the element with locator: {locator} and locatorType: {locator_type}')

    def submit_image(self, data, locator, locator_type='xpath'):
        try:
            element = self.get_element(locator, locator_type)
            element.send_keys(data)
        except InvalidArgumentException:
            self.log.info(f'Cannot sent data on the element with locator: {locator} and locatorType: {locator_type}')

    def input_keys(self, data, locator, locator_type='xpath'):
        try:
            element = self.get_element(locator, locator_type)
            element.send_keys(Keys.CONTROL + 'a', Keys.BACKSPACE)
            # element.clear()
            element.send_keys(data)
        except ElementNotInteractableException:
            self.log.info(f'Cannot sent data on the element with locator: {locator} and locatorType: {locator_type}')

    def submit_input_keys(self, data, locator, locator_type='xpath'):
        try:
            element = self.get_element(locator, locator_type)
            element.send_keys(Keys.CONTROL + 'a', Keys.BACKSPACE)
            # element.clear()
            element.send_keys(data, Keys.ENTER)
        except ElementNotInteractableException:
            self.log.info(
                f'Cannot sent and submitted data on the element with locator: {locator} and locatorType: {locator_type}')

    def is_element_present(self, locator, locator_type='xpath'):
        self.driver.implicitly_wait(1)
        time.sleep(.5)
        by_type = self.get_by_type(locator_type.lower())
        try:
            element = self.driver.find_element(by_type, locator)
        except NoSuchElementException:
            return False
        if element:
            return True
        else:
            return False

    def is_element_displayed(self, locator, locator_type="xpath"):
        try:
            time.sleep(.3)
            element = self.get_element(locator, locator_type)
            return element.is_displayed()
        except NoSuchElementException:
            return False
        except ElementNotVisibleException:
            self.log.info(f'Element is not displayed with locator: {locator} and locatorType: {locator_type}')
        return False

    def is_element_clickable(self, locator, locator_type="xpath"):
        try:
            element = WebDriverWait(self.driver, 1).until(
                ec.element_to_be_clickable((locator_type, locator)))
            if element:
                return True
            else:
                return False
        except TimeoutException:
            return False

    def wait_for_element(self, locator, locator_type='xpath', timeout=10, poll_frequency=0.5):
        try:
            time.sleep(1)
            by_type = self.get_by_type(locator_type)
            wait = WebDriverWait(self.driver,
                                 timeout,
                                 poll_frequency=poll_frequency)
            element = wait.until(ec.element_to_be_clickable((by_type, locator)))
            return element

        except ElementNotInteractableException:
            self.log.info(f'Element not interactable with locator: {locator} and locatorType: {locator_type}')
        except TimeoutException:
            self.log.info(
                f'Time out exception happened for Element with locator: {locator} and locatorType: {locator_type}')

    def wait_for_element_is_visible(self, locator, locator_type='xpath', timeout=10, poll_frequency=0.5):
        try:
            time.sleep(1)
            by_type = self.get_by_type(locator_type)
            wait = WebDriverWait(self.driver,
                                 timeout,
                                 poll_frequency=poll_frequency)
            element = wait.until(ec.visibility_of_element_located((by_type, locator)))
            return element

        except ElementNotInteractableException:
            self.log.info(f'Element not interactable with locator: {locator} and locatorType: {locator_type}')
        except TimeoutException:
            self.log.info(
                f'Time out exception happened for Element with locator: {locator} and locatorType: {locator_type}')

    def web_scroll(self, locator, locator_type='xpath'):
        time.sleep(1)
        element = self.get_element(locator, locator_type)
        try:
            self.driver.execute_script("return arguments[0].scrollIntoView(true);", element)
        except NoSuchElementException:
            self.log.info(f'Element Not Found with locator: {locator} and locatorType: {locator_type}')

    def calendar_datepicker(self, date, locator):
        self.focus_element(locator)
        self.wait_for_element(locator).click()

        user_provided_day = date.day
        user_provided_month = date.month
        user_provided_year = date.year

        self.focus_element('//body//mat-datepicker-content//button[1]')
        self.wait_for_element('//body//mat-datepicker-content//button[1]').click()
        self.wait_for_element(f"//div[.={user_provided_year}]").click()
        self.focus_element(f'(//td[@role="button" and contains(div/text(),"")])[{user_provided_month}]')
        self.wait_for_element(f'(//td[@role="button" and contains(div/text(),"")])[{user_provided_month}]').click()
        self.focus_element(f'//div[./text()={user_provided_day}]')
        self.wait_for_element(f'//div[./text()={user_provided_day}]').click()

    def input_date(self, date):
        # Format Year-month-day
        self.driver.execute_script("document.getElementsByName('date')[0]" + ".valueAsDate" + f" = new Date('{date}')")

    def click_element_js(self, element):
        time.sleep(.5)
        try:
            self.driver.execute_script("arguments[0].click();", element)
        except NoSuchElementException:
            self.log.info(f'Element Not Found.')

    def wait_element_to_appear(self, locator, locator_type='xpath'):
        time.sleep(.5)
        try:
            by_type = self.get_by_type(locator_type)
            wait = WebDriverWait(self.driver, 10, poll_frequency=1,
                                 ignored_exceptions=[ElementNotVisibleException,
                                                     ElementNotSelectableException,
                                                     ElementNotInteractableException])
            element = wait.until(ec.element_to_be_clickable((by_type, locator)))
            if element:
                return True
            else:
                self.log.info(f'No such ellement with locator: {locator}')
                return False
        except TimeoutException:
            self.log.info(f'Element not found in time with locator: {locator}')
            return False
