## Production

### epharma.fi

* [changelog](production-server-changelog)

## Staging/Testing (rg.dev.epharma.fi)

### rg.dev.epharma.fi

* [initial setup](dev-test)
* [changelog](dev-changelog)

#### Instructions
* [updating database](epharma-test)

## DevOps

### backup server

Running in AWS EC2 under the reseller gateway account

* [changelog](backup-server-changelog)

## Production (The Reseller Gateway)

### "new" erp

* [initial setup](new-erp-setup)

### sportsgateway

* [initial setup](sportsgateway)
* [setup test system](sprtgw-test)

