**********
Data model
**********

.. image:: ../images/schema.png

.. note:: in the model **CHAR(32)** used to represent **UUID** 
          (or **GUID** in the actual code).

Tables
======

Identities
----------

(See :class:`dbv2.models.Ident`)

Identities table is used for mapping user logins into user identities
in the system. It provides the link between the identity established
by the provider (value) and the entity know by the system.

=========  =======  ===================================
Field      Type     Description
=========  =======  ===================================
entity_id  UUID     primary entity this identifies (FK)
provider   VARCHAR  identity provider
value      VARCHAR  providers value for the identity
=========  =======  ===================================


Entities
--------

(See :class:`dbv2.models.Entity`)

This table is used for giving an ''Entity'' a meaningful
name and the default role other entities can assume when acting
for this entity.

============  =======  ==================================
Field         Type     Description
============  =======  ==================================
id            UUID     unique identifier for the entity
name          VARCHAR  name of the entity for UI
entity_type   VARCHAR  type of entity
default_role  VARCHAR  default role for other identities
                       when operating under this entity
============  =======  ==================================

.. note:: the *default_role* can be *null* in which case an entity
          cannot be granted a role unless it is explictly 
          allowed by the *roles* table. If this is not *null* then
          **any** entity can assume the *default_role* (**in addition** 
          to the explicitly granted roles in the *roles* table).

Roles
-----

(See :class:`dbv2.models.Roles`)

This table defines the roles other entities can have when acting
under the given entity.

================  =======  ====================================
Field             Type     Description
================  =======  ====================================
entity_id         UUID     primary entity (FK)
acting_entity_id  UUID     entity that can assume the role (FK)
role              VARCHAR  role the acting entity can assume
================  =======  ====================================
