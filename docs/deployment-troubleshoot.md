# Deployment troubleshoot FAQ

## How to deploy project to AWS?

According to **kimvais**, define `certificate_arn` and `domain` to `zappa_settings.json`. `certificate_arn` can be fetch from Amazon's ACM service. To work with CloudFront, the certificate from US East has be to chosen (North Virginia). Afterwards you're ready to `zappa certify` and you're good to go.

Note: You need aws route53 permissions to run `zappa certify` so when you need to do it, get **kimvais** or **mteiste** to do it.

## botocore.errorfactory.BucketAlreadyExists: An error occurred (BucketAlreadyExists) when calling the CreateBucket operation: The requested bucket name is not available. The bucket namespace is shared by all users of the system. Please select a different name and try again.

The `s3_bucket` value seems to be `lmbda` and should be changed to something else. For development, testing and temporary deployment purposes have `gwio-scratch`. If one doesn't exist, go bother **kimvais** about it.

## botocore.errorfactory.ResourceNotFoundException: An error occurred (ResourceNotFoundException) when calling the DescribeLogStreams operation: The specified log group does not exist.

Most likely Cloudwatch does not have the log group setup for the deployment. Usually used by `<VERTICAL_NAME>_<APPLICATION_ENVIRONMENT>_audit` (the default log group name that is assumed to be used). Simply create the appropriate named one.

## Error: Warning! Status check on the deployed lambda failed. A GET request to '/' yielded a 502 response code. AttributeError: module 'gwio.SOMETHING' has no attribute '__main__'

Zappa settings `app_function` should be directed to the path that has the initialized Flask app-instance.

## Error: Warning! Status check on the deployed lambda failed. A GET request to '/' yielded a 500 response code.

A mandatory environment variable used during Flask app initialization hasn't been set. Go to AWS Lambda, search for appropriate project name and environment, then go and add your own environment variable values.

## ... Zappa deployment is live!: MY_DOMAIN (AWS_LINK), but MY_DOMAIN is not working

You should run `zappa certify`