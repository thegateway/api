# Main pages

* [specifications] (specs-home-page)
* [instructions] (instruction-home-page)
* [processes] (Processes)
* [automated processes] (automation-home-page)
* [servers](server-home-page)

# AWS setup

## Internet access from lambda services

The access from lambda functions to the internet at large is done as follows:

* the vpc (*vpc-7969751d*) for lambda has one subnet (*subnet-43ea360a) dedicated for internet access. **Never** use this subnet for the lambda services themselves
* there are three other subnets in the lambda vpc that can be used for the services themselves (*subnet-b1a583e9*, *subnet-ba9d78dd*, *subnet-010bee48*)
* there is one NAT gateway (*nat-072ed5a8e8ceaf31a*) located in *subnet-43ea360a* that is used to route requests from the lambda functions into the greater Internet.
* *subnet-43ea360a* has its own dedicated routing table (*rtb-5cc8e43b*). It only has routes to local vpc and the default route (0.0.0.0/0) via the Internet gateway (*igw-fc67ea98*).
* the other subnets share the main routing table (*rtb-8667dbe1*) which has routing information to the other vpcs and in addition the default route is defined to be the nat gateway.

## Generic structure
![](./images/aws.svg)

