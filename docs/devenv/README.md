# The Reseller Gateway software development process

## Gitlab
> **If it's not in GitLab it does not exist.**
>
>   - Old proverb from Pitäjänmäki

In practice, this means the following things

*  You should not be working on anything that does not have an GitLab issue in `Doing` column of the [group board](https://gitlab.com/groups/thegateway/-/boards])
*  Your code does not _exist_ unless it has a branch on GitLab and is pushed.

To extend this to actual rules


1. You MUST commit and push multiple times a day.
2. All branches SHOULD have a work-in-progress merge request with `WIP:`
3. All issues that are being worked on, SHOULD have been estimated and have a weight set.


### Repositories
- core
- v2018
- devops
- dashboard
- devenv
- gatewayfront-core
- gatewayfront

## Scrum

We use two week sprints, starting on Monday on even numbered weeks (as of autumn of 2018).

### Dailies

Scrum dailies are 10:30 - on remote days (Tue/Thu) and whenever someone is remote on other days they are held in [Google hangouts](http://bit.ly/2hpWssM)

### Life cycle of a task

#### 1. Backlog
This is where all issues SHOULD be created. Anyone can create these and they have no formal requirements or structure. The only guideline is to put in all _relevant_ information you have at hand at the time you create the issue.

#### 2. TODO
These issues have been reviewed by in a joint meeting called _backlog grooming_ and deemed worth doing. During that meeting the issue MUST be updated to contain enough specification for the technical people to design the approach of implementation in case of features.

#### 3. Next
Issues that have priority, these MUST be weighed in the next sprint planning so that they can be worked on during the spirnt.

#### 4. Doing
Issues someone is currently working on. These SHOULD be assigned to a person and there SHOULD be only one issue in doing per developer.

#### 5. Done
Issues completed during the sprint. They will be closed after being demoed.

### Sprint planning
A bi-weekly meeting on mondays where we play planning poker on issues chosen for the sprint.

### Demo days
End of the sprint meeting, issues in DONE are demoed and man-days by vertical information is collected.

### Retrospecives
A meeting after demo day where three questions are asked and answered:
 1. What went wrong
 2. What went well
 3. What we can do better.

## Definition of "done"

- Unit tests
- Packaged with entry points
- Issues for all TODO -items
- Deployed to production

## Code style

All code MUST be PEP 8 / pycodestyle compliant and MUST pass pyflakes without warnings.

All code SHOULD have pylint score of 10.0 with the *setup.cfg* found in repositories, the following exceptions or relaxations are made:

- Maximum line length is 159
- e, i, n, k and v are valid variable names when used as the industry standard practice is, as follows:
  - `except SomeExceptionSubclass as e:`
  - `for i, item in enumerate(collection)`
  - `n` as alias to _count of items being iterated_ when there is a single count
  - `for k, v in some_dictionary.items()`
- C0103 is disabled - so that `logger = logging.getLogger` does not need to be marked every time. Otherwise, all constants MUST be named conforming to PEP8.
- W0511 is disabled - TODO comments in code *are* fine.
- C0330 pylint and pycodestyle disagree on this but pycodestyle is right on this one.

If you are *sure* that whatever complaint pylint is giving is not actually an issue,
you can disable the warning (preferably with an explaining comment when the reason
is not trivial) with ``# pylint: disable=warning-name-given-by-lint``

Examples:

```python
# pylint: disable=too-few-public-methods
class TheRandomGuidName:
   guid = uuid.uuid4()
   name = "random"
```

Use the descriptive name when disabling, not the code (there are plenty of examples of the previous method
of using the actual warning code, you do not have to spend time to fix those, but all new ones should be
done using the descriptive name).

### DRY code is good code

Never repeat, always reuse.

**Duplicate code is *always* bad.** It also emits a pungent _code smell_ - no matter how trivial the actual duplicate lines are, e.g `return Response(foobar)` - they are a hard hint that you are something wrong. The same applies to boilerplate - if you find yourself writing a lot of boilerplate, stop to think for a moment to ponder what you might be doing wrong.

#### `elif` is a root of many evil
Two is the only special case of "many" - and

### Naming

All names should follow PEP 8 convention. For databases, table names should be named like python variables and be pluralized. Columns are just like attributes in python - and therefore id columns should be named `guid`. Python standard library names SHOULD NOT be reused as variables, therefore `guid` over `id`. Also builtins SHOULD NOT be shadowed with the exception of database attribute named `type`, but even this should be defined as `type_ = Column(...., name='type')`