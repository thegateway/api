# Updating database with production data

## Post import mandatory fixes

### RG side
1. Run the following SQL commands to fix the URLs in the database to point to the staging ERP  
    1. `UPDATE component_integration SET integration_url = REPLACE (integration_url, "http://erp.epharma.fi/", "https://erp.dev.epharma.fi/") WHERE integration_url LIKE '%erp.epharma.fi%';`  
    1. `UPDATE component_integration SET integration_url = REPLACE (integration_url, "https://erp.epharma.fi/", "https://erp.dev.epharma.fi/") WHERE integration_url LIKE '%erp.epharma.fi%';`
1. Update the `Verkko-osoite` for some store to be `shop.dev.epharma.fi`

### ERP side
1. Update the `domain` for the actor (corresponding to the one that was updated on RG side) to be `shop.dev.epharma.fi`