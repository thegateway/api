### 14.06.2018 Thursday (e-panda)
* v2018 production updated to commit 626603240657e38ab181d03475183d78bf93cd19

### 06.06.2018 Wednesday (e-panda)
* v2018 production updated to commit 557bd565094156989612a7ac48087259bcd8954c

### 31.05.2018 Wednesday (e-panda)
* v2018 production updated to commit 2a03af88a99461d8abd864d6f939a1afbb786954

### 18.05.2018 Friday (e-panda)

* v2016 production updated to commit 1780ca026df7a325a946f71fc671c36533489eda
* v2018 production updated to commit 4c6a9f5320cc2bd9fa299aeb17ca25155375d661
* v2018 production updated again to commit b9c9dacd1d0e07f61d49c3eea6e22cce2f174833

### 07.05.2018 Monday (e-panda)

* v2016 production pushed to commit bef9962c19be3d11c2bf9ae7c81c789caf388afb

### 15.01.2018 Monday (marko)

* /var/www/epharma commit 29a93da060b9b254f0a1433280260514caf1a60a
* /var/www/ecome commit f7d955a788e48c594b83177e56bc6cd58c79d7aa

### 11.01.2018 Thursday (marko)

* /var/www/epharma commit fc016882c7e6d88df2a150af55299152b65ffeaa
* /var/www/ecome commit f7d955a788e48c594b83177e56bc6cd58c79d7aa


### 11.01.2018 Thursday (e-panda)
* Ran SQL script to production DB to resolve the logo image related issue:

```
ALTER TABLE `actor`
	CHANGE COLUMN `logo_filename` `logo_filename` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_swedish_ci' AFTER `quick_code`;
	
UPDATE actor
SET logo_filename = CONCAT('https://epharma-erp-uploads.s3.amazonaws.com/original/', logo_filename)
WHERE
logo_filename IS NOT NULL;
```

* Copied logo files from production server to s3 bucket s3://epharma-erp-uploads/original/

### 10.01.2018 Wednesday (e-panda)
* Ran SQL script to production DB to resolve the image related issue:

```
SELECT REPLACE(url, 'https://epharma-product-images.s3.amazonaws.com/vnr/', 'https://epharma-erp-uploads.s3.amazonaws.com/original/')
FROM product_image
WHERE
url IS NOT NULL
AND
url LIKE 'https://epharma-product-images.s3.amazonaws.com/vnr/________________________________\___________.___';

DELETE FROM product_image
WHERE
id IN (176,1671,1635,1638,1636,1344,1634,1618,1920,2603,7414,6862,4617,7227,24075,24076,24074,24073,24072,24261,24271,24233,24272,24263,24244,24234,24240,24238,24217,24265,24220,24226,24241,24221,24214,24258,24245,24223,24228,24232,24248,24224,24227,24259,24247,24215,24211,24225,24246,24242,24229,24218,24270,24212,24239,24260,24235,24216,24231,24243,24268,24219,24256,24236,24264,24267,24255,24213,24266,24262,24269,24222,24230,22316);
```


### 09.01.2018 Tuesday (e-panda)
* Copied banners from /var/www/epharma/uploads/banners/original to s3://epharma-erp-uploads/original/
* Ran an SQL script to production DB:

```
UPDATE banner
SET filename = CONCAT('https://epharma-erp-uploads.s3.amazonaws.com/original/', filename)
WHERE
filename IS NOT NULL
```

### 13.11.2017 Monday (e-panda)
* git pull to 0ddfba1e36973874213d9204d1529fe8df7aa959
* db migrate change

### 10.10.2017 Tuesday (e-panda
* git pull to 43f892fc48d3a12d2236eee1efc093c40020e545
* db migrate change

### 18.09.2017 Monday (marko)

* /var/www/epharma commit 2c3ca53a1636f5252481d1a4a9d68fc60fc6e52a
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 15.09.2017 Friday (marko)

* /var/www/epharma commit fb7af667578362f96b420eb86c81593628d72743
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 11.09.2017 Monday (marko)

* /var/www/epharma commit 221abab1f3a7fa1ce5a5f8ad9f33e985d8b8089b
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 11.09.2017 Monday (marko)

* /var/www/epharma commit 228849b632927f8b89f0076e6b09cd420dfd8c82
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 08.09.2017 Friday (marko)

* Soft deleted product frames and products that had matching `product.vnr_code` or `product.supplier_sku_code` to the list at `s3://tgw-devops-backups/override/koodit.lst`. These products are prescription medicines for pets.

### 08.09.2017 Friday (marko)

* /var/www/epharma commit 20a9221dcc6768d950f272d05ebba19d89683c8d
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 04.09.2017 Monday (mte)
* the monthly bookkeeping was still run in the old server so access to it
  did not work from the production server even though the database showed
  it is available. Disabled the bookkeeping job in the old server and
  enabled it in the current production server, also synced the already generated
  bookkeeping reports over to the new production server.
* /var/www/epharma commit 37f52d4a41c7ab6ae78b6b3faee855bbc76fa720
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 30.08.2017 Wednesday (e-panda)

* updated paytrail url on email template
* /var/www/epharma commit 37f52d4a41c7ab6ae78b6b3faee855bbc76fa720
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb


### 29.08.2017 Tuesday (marko)

* /var/www/epharma commit 1eebe9ec6018f09bf5bec8787dc33e1893ce5536
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 28.08.2017 Monday (mte, kimvais)

* production server instance moved from ecome account to gateway account
* disabled old webshops
* /var/www/epharma commit a249cf3e9b6e46b1a3463cc0a5e65dcec5672058
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb
* cleaned up nginx config files (now all vhosts are SSL, default.conf redirects all to https://host$url/

### 23.08.2017 Wednesday (marko)

* /var/www/epharma commit 13fca29172dbfbb8705e20abd1fb005db1a01255
* /var/www/ecome commit 79248f62054dc71abd6a080475b241b0b7a2e2bb

### 21.08.2017 Monday (marko)

* /var/www/epharma commit dbb624e96f500b5a7a532f046b0b3972d02c3021
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 17.08.2017 Thursday (marko)

* /var/www/epharma commit 9702d0d370d1261ce70b95975708cc4b8ddb59a7
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 16.08.2017 Wednesday (marko)

* /var/www/epharma commit 5fe4eba5b5561d6563c31cc4db8da68cf3f38af3
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 16.08.2017 Wednesday (marko)

* /var/www/epharma commit 054c8e12fcb5be61a8b1ef182665bada4dfc9c12
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 15.08.2017 Tuesday (marko)

* /var/www/epharma commit ecc7579af8e38dbee766656d2214a4f2e1d264d2
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 14.08.2017 Monday (panda)
* /var/www/epharma commit ea4dd301235b89122df95e0728b0ff8d648b45cd

### 07.08.2017 Monday (marko)

* /var/www/epharma commit 43c259af70c0e62ecf0f8a57fa6c838b9d3c6fbc
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 07.08.2017 Monday (mte)
* changed group and permissions for /var/www/ecome from apache to wheel and added group write permissons.
* /var/www/epharma commit 8275efbc2138853b80a8723783964c8c86af8061
* /var/www/ecome commit 46d7466e2d612521b996647159c2c90755b40250

### 03.08.2017 Thursday (mte/kimvais)
* fixed bookkeeping cron job command, the job is using relative path for the location of the result files, but cron starts the job in /, now the job explicitly changes directory before actually executing the report generator
* changed cron job for bookkeeping reports to be run on 3rd instead of 10th of the month (kimvais).
* disabled LogicalDoc gateway (no files were removed, just shut down and disabled the service)
```
systemctl stop logicaldoc-gateway
systemctl disable logicaldoc-gateway
```
* created a directoy /etc/nginx/disabled.conf.d to store configuration that are no longer in use
* moved the docs.thegateway.io.conf from conf.d to disabled.conf.d
* reloaded nginx
* /var/www/epharma commit 8275efbc2138853b80a8723783964c8c86af8061
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 02.08.2017 Wednesday (marko)

Added `SAL, Paketittomat` package for all pharmacies  
```
INSERT INTO reseller_has_package (actor_id, package_id, created_at) SELECT a.id, 43, NOW() FROM actor a WHERE a.company_name LIKE "%apteekki%";
```

### 31.07.2017 Monday (marko)

* /var/www/epharma commit 0c7f8767fa9be8d630fb69541df9e989a22f3aef
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 29.06.2017 Thursday (panda)
Production ecome-database change:
```
UPDATE component_integration SET integration_url = 'http://erp.epharma.fi/product/list?categorie=431&no_filters=1' WHERE integration_url = 'http://erp.epharma.fi/product/list?categorie=3&no_filters=1'; # Allergia -> Allergia
UPDATE component_integration SET integration_url = 'http://erp.epharma.fi/product/list?categorie=126&no_filters=1' WHERE integration_url = 'http://erp.epharma.fi/product/list?categorie=4&no_filters=1'; # Apuväline -> Testit, apuvälineet ja muut
```

### 28.06.2017 Wednesday (marko)

* /var/www/epharma commit 44252099ddd628e41437c6650443d265340cefba
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 28.06.2017 Wednesday (marko)

* /var/www/epharma commit 03579df386129fca6af05619488d06967379326e
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 27.06.2017 Tuesday (mte)

* added wrapper script sync-upload for syncing the epharma upload to
  s3 (temporary measure until the rg upload works directly against s3)
* added an account for vellu for running the sync-upload
* /var/www/epharma commit a373ba89ed66575e619d34927c0d56362420f3d0
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257


### 26.06.2017 Monday (?? / mte)
* installed python35u (for the upload sync)
* fixed permissions on /var/www/epharma
* /var/www/epharma commit e3c41ff7682aa7b212d4a335b43dae5b1a8f634f
* /var/www/epharma commit 74d28b61b70dc7cfefeed86dd1cac4e149961825


### 20.06.2017 Tuesday (marko)

* /var/www/epharma commit a366d00200f9d6b782e4e731cebbe5770bf4cd47
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 19.06.2017 Monday (marko)

* /var/www/epharma commit 30f5342f94dda7910f9927e88ff4e490b4860c92
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 16.06.2017 Friday (marko)

* /var/www/epharma commit 40a73d3a4f78b477ab0ce93833288a26d3440796
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 16.06.2017 Friday (marko)

* /var/www/epharma commit 4a977a142ac3837a2d4ab1693d9bc4d1115f4e51
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 14.06.2017 Wednesday (marko)

* /var/www/epharma commit 7fb8f2b7bced68e7f30af3edc4e68356741d100d
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 09.06.2017 Friday (marko)

* /var/www/epharma commit 56dff598d63bfd938a4cae99d155ed115be0b692
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 08.06.2017 Thursday (marko)

* /var/www/epharma commit f227f6d52d9d2e48a331d347ebaed3b95cccc157
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 08.06.2017 Thursday (marko)

* /var/www/epharma commit 5adc6f7dce6eb5f1f20437e7ecf64a6541ec96a5
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 30.05.2017 Tuesday (marko)

* /var/www/epharma commit 1c99c3b742d6d586eb9a6ac82b987bf32fc11182
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 24.05.2017 Wednesday (mte)

* wrapped git so that you cannot accidentally run it as root
* /var/www/epharma commit 15c85bef022689f231a66f4da305a64c013993c9
* /var/www/ecome commit f65ed6b5050a6cca423aa4ce02f0686474cc3257

### 09.5.2017 Tuesday (Wang Han)

* https://trello.com/c/Rs8RJLD1 Tasks about "adding supports to static pages" merged to master, server updated.
* hotfix-1: https://gitlab.com/thegateway/rg/commit/9677d83c7dab757cd1bfa7dcafe76f0bb1429d85 merged to master(RG only), server updated.
* hotfix-2: https://gitlab.com/thegateway/epharma/commit/f2e8d3a9c2a6c9faab429b135ba1c37b78f48353 merged to master(ERP only), server updated.

### 17.3.2017 Friday (Marko Teiste)

* moved the the php command for fetching medical info into a script in /usr/local/bin/fetch_medicine_data. The script will automatically copy files (if any) to staging so that we can easily keep the staging up to date.

### 21.2.2017 Tuesday (Marko Teiste)

* noticed the centos crontab email job keeps spamming (still) errors to centos users email box, created trello ticket for it, cleared the mailbox
* added /usr/local/bin/synctobackup script and added to the centos users crontab
* fixed centos users crontab (e.g. PATH=$PATH:/path/to/something does not work as crontab is not shell script and *shell variable expansion* does not work

### 20.2.2017 Monday (Marko Teiste)

* created individual user accounts for wang, e-panda and marko
* added their keys to the authorized_keys
* asked them to check that the key/account work
