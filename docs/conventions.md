[[_TOC_]]

# Coding

1. Use space instead of tabs when indenting.
2. Don't mix source and test codes. Sparate sources to `gwio/` and tests to `tests/`-folders.
3. Opt for English when naming classes, variables, functions, files or anything that involves naming things. 
4. Class names should be **CamelCased**.
5. Function names should be **snake_cased**.
6. Strive for 80 character line width, 139 is the hard limit.
7. Writing tests is good. It makes people happy.

# Database

1. Table names should be **snake_cased**.
2. Use singular for table names. For instance, prefer `user` over `users`.
3. Use singular for column names.
4. Opt for **guid** over sequenced integer **id** for records.
5. SQL related: Name the many-to-many tables based by the tables involved. Like `user` and `bank` would have many-to-many table named `user_bank`.

# Version control

1. Opt for English when writing commit messages.
2. Make it clear what change does that specific commit do in your commit message.
3. Never commit directly to `master`-branch. (This is actually prevented by making `master` a protected branch.)
4. Use prefix in your branch name to help identify what the branch is about. For instance, `bugfix_my_branch`, `feature_my_branch`, `task_my_branch` or `hotfix_my_branch`. If an issue exists - it should - prefix with it.
5. When branch is ready to be put to master, make sure it is rebased on master and remove the WIP tag from the merge request for it and have someone review the changes you made.

# Kanban

1. **TODO** is a free-for-all tasks that you can move to **Ongoing** and start working on it.
2. **Doing** is to let everyone know what you're doing.
3. **Ready for merge** means that the merge request for the task has been made.
4. **Done** means not only that the changes have been merged, but also that it has been successfully deployed to demo/staging.
