# Gitlab runner

## Setup

1. added required repos using gitlab provided setup script
```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
```
1. installed the runner package
```
sudo apt-get install gitlab-ci-multi-runner
```

## Configuring the runner

1. registered the runner for apiv2 project (using url and token from the apiv2 project)
```
sudo gitlab-ci-multi-runner register
```
1. created virtualenv for the apiv2 project to use (for user gitlab-runner)
```
mkdir envs
python3.6 -m venv envs/apiv2
```
1. added aws credential for s3 tests for the gitlab-runner user
```
aws configure
```

## Configuring gitlab side of things

1. disabled shared runners
1. set the enviroment variables for email testing
1. created `make coverage-ci` target
1. configure basic pipeline


