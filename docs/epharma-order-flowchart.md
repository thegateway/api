# Flowchart depicting process flow for when customer makes an order in the Epharma system.

Created with https://www.draw.io/.  
Attached XML can be used to make changes.

[EpharmaProcessDiagram.xml](/uploads/31cbe70c45607e7a8884a3bb32be10d4/EpharmaProcessDiagram.xml)

![EpharmaProcessDiagram](/uploads/76d60703327595c96e00213d06e04f3d/EpharmaProcessDiagram.png)





