ERP: http://erp.vpkgateway.fi

 -> www/epharma

 -> db: vpk

RG: http://dashboard.vpkgateway.fi

 -> www/ecome

 -> db: ecome

Demo webstore: https://demo.vpkgateway.fi/

### 16.10.2017 (wang)

- `epharma` is on branch "vpk_production";


### 06.10.2017 (wang)

- fixed owner:group and permissions of `ecome`, so `git pull` is at least working;
- updated `ecome` and  `epharma`;