# URL:s and naming

## S3
All web content for each vertical and environment is stored in a single bucket.
The bucket should be named the FQDN of the environment, e.g. `epp-galeria.app` or `sma.dev.sovell.us`
The following prefixes and CloudFront distributions should be used. 

- `app/` - the main domain name, for the customer facing web app.
- `dashboard/` - `pro.`domainname - for Dashboard
- `uploads/` - `static.`domainname - for image etc. uploads.


## Production
Without any prefixes, the default subdomain for dashboard is `pro.domain.name` and API is at `api.domain.name`.

## Staging
Staging is sub-domain `demo.domain.name` and `pro.demo.domain.name`. API is at `api.demo.domain.name`

## Development
Development environments are at our own domains, `dev.gwapi.eu` for API:s and `dev.sovell.us` for apps.

## Examples
### SMA / g@leria

#### Production

* API: `api.epp-galeria.app`
* App: `epp-galeria.app`
* Dashboard `pro.epp-galeria.app`

#### Staging

* API: `api.demo.epp-galeria.app`
* App: `demo.epp-galeria.app`
* Dashboard `pro.demo.epp-galeria.app`

#### Development

* API: `sma.dev.gwapi.eu`
* App: `sma.dev.sovell.us`
* Dashboard `pro.sma.dev.sovell.us`

### Remppa.app

#### Production

* API: `api.remppa.app`
* App: `remppa.app`
* Dashboard `pro.remppa.app`

#### Staging

* API: `api.demo.remppa.app`
* App: `demo.remppa.app`
* Dashboard `pro.demo.remppa.app`

#### Development

* API: `remppa.dev.gwapi.eu`
* App: `remppa.dev.sovell.us`
* Dashboard `pro.remppa.dev.sovell.us`
