*********
Verticals
*********

Creating a new vertical
=======================

The minimum you need to do for a new vertical is to add the vertical
specific code (products, assessors) and deploy a staging environment

- define the product model

  - the product definition
  - product assessor

- create the test fixtures
- create deployment definitions

  - staging
  - production

Defining the product model
--------------------------

All verticals need a product model (*gwio/models/product/_vertical/<your-vertical>/product.py*)
that is adapted with the vertical name and included as part of the products.

Create the basis product definition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The most simple way to do this is just to inherit from the *BaseProduct*. A typical
starting model looks something like:

.. code-block:: python

    class ProductSchema(BaseProductSchema):
      pass

    class CreateSchema(BaseProductCreateSchema):
      pass

    class UpdateSchema(BaseProductUpdateSchema):
      pass

    class OrderLineSchema(BaseProductOrderLineSchema):
      pass

    @adapted_ddb('your-vertical')
    class Product(TagMixin, BaseProductMixIn, Model):
       class API:
           marshal = ProductSchema
           create = CreateSchema
           update = UpdateSchema
           orderline = OrderLineSchema
           assessor = None  # back-filled

           namespace = get_namespace('product')

           @classmethod
           def generate_uuid(cls, **kwargs):
               return Product.BaseProductAPI.generate_uuid(**kwargs)

       class Meta:
           table_name = 'products'
           version = 0


(The *your-product* is supposed to be a python package, so you naturally need
an __init__.py that imports the product (this is for the "vertical automation")

.. code-block:: python

    from . import product
    assert product

After you've created the product you need to "register" it by having the top level package
automatically import it, E.g in *gwio/models/products/_vertical/__init__.py* add your
package to the list of imported verticals

.. code-block:: python

    from . import xx  # your-vertical
    assert xx  # your-vertical

This concludes the basic product definition.


Creating the test fixtures
--------------------------

In order to run unittests for the new vertical you also need to create a function that
can be used to create product fixtures for the vertical. This is done by creating a function
that returns a random product. To start create a fixture file
*tests/_fixtures/_vertical/your-vertical.py* with
a function (if you used the BaseProduct) like

.. code-block:: python

    Product = use_ddb(model='Product', vertical='sma')

    @random_product_for_vertical('sma')
    def sma_product():
        params = random_base_product_without_uuid()
        uuid = Product.API.generate_uuid(**params)
        return Product(uuid, **params)

Make sure your function is imported (so that the auto-detection will work)
by *tests/_fixtures/_vertical/__init__.py*

At this point the unittests for your vertical should work.

Creating the deployment configuration
-------------------------------------

In order to deploy the new vertical you need to create *zappa_settings.json*
for your vertical (these are typically put into *deployment/your-vertical*).
The actual content of the vertical varies, see zappa documentation and
study configuration files for other verticals to figure out what to put in
there (e.g. documentation pending for this particular item).

Deploying staging environment
=============================

You can now try to deploy the initial version using zappa.

Switch to the deployment directory, create the environment then switch
to your verticals directory and try to do the initial deployment
(``zappa deploy dev``).
*Do not be alarmed* as it will most likely
fail (the lambda does not have the required environment settings defined) and
you will most likely see something like


``Error: Warning! Status check on the deployed lambda failed. A GET request to '/' yielded a 502 response code.``).

Updating the staging environment
--------------------------------

Most of these steps need to be done in the aws console, this is just a short
description of what needs to be done, for further details refer to AWS documentation.

Create cognito authentication pool
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When creating the pool at some point it will ask you to create IAM role, just
create the default role it suggests

- Create a pool

  - sign-in: email or phone, allow both
  - required standard attributes: name
  - optional custom attributes: organization_guid (string min:32, max:36, mutable)
  - password policy (9 chars, numbers letters, allow users to sign themselves)
  - MFA optional (require verification of email or phone (select both)
  - create app client (no client secret, enable sign-in API, allow read/write attributes to all)


- Add a groups
  - admin
  - shopkeeper

Now you need to take note of the pool_id and the client id as you will need them
when setting up the lambda execution environment.

Create the dynamodb tables
^^^^^^^^^^^^^^^^^^^^^^^^^^

Easiest way to do this is inside the project environment (``pipenv shell``)

.. code-block:: shell

    GW_VERTICAL_NAME=your-vertical APPLICATION_ENVIRONMENT=staging python tools/create_missing_tables.py


Create the log group for audit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create the log group *your-vertical_staging_audit*


Setting up the lambda environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The initial deployment should've setup a lambda function. You need to modify / configure
its environment to match the vertical (and to make sure the system will actually start).
So head to the console and find the lambda function and edit its configuration. (At minimum
you need to set the POOL_ID and CLIENT_ID to match the the pool you've created.

You can now update the lambda (``zappa update dev``) and if the environment has been
correctly setup everything should start working.


Certifying API gateway
^^^^^^^^^^^^^^^^^^^^^^

(**You need route53 permissions**)

As the final step, certify the API GW (``zappa certify``)


