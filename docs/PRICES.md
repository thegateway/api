# Products & pricing

## Price types

`cost` is the price the seller pays/paid for the product  
`base` is the basis for the sale price calculation
`original` is the price for the customer without any conditional modifiers *(campaign, promotion, coupon, etc.)*  
`final` is the price for the customer with some possible modifiers  


## Product differences

### Base product

- stores `base` and `cost` price
- does not contain `original` and `final` price
- only returned by administrative API endpoints for modifying products, otherwise returns `EvaluatedProduct`

### Webshop product
- never directly returned by API endpoints, but converted to `EvaluatedProduct` with `shop pricing`

### Evaluated product
- calculates `original` and `final` based on `base product` information and possible `shop pricing`
- for **admins** and **shopkeepers** it contains all the prices
- for **customers** contains only `original` and `final`


## Pricing

### General pricing
### Shop pricing
### Product pricing
