
# How to migrate the database

## Set-up

You can skip this, if you are doing this for an existing database.
1. `mysql -u root -p -h <hostname> epharma < epharma.sql`
2. `mysql -u root -p -h <hostname> epharma < anondb/drop_extra_indices.sql`
3. run anondb from devops (takes DBURI as arg 1)

## Migration

1. Set environment variable `export SQLALCHEMY_DATABASE_URI=...` or `$Env:SQLALCHEMY_DATABASE_URI=...`
2. run `python _event_migrate.py` in `dbv2/tools`
3. Drop extra tables and columns `mysql -u root -p epharma -h <hostname> < /path/to/apiv2/sql/little_bobby_tables.sql`
