# Adding new pharmacy to the RG and ERP system

## Steps

1. Add new pharmacy company as a **customer** on the RG platform. Take note of the **id** that will be generated.

2. Add new pharmacy company as an **actor** to the ERP platform **with reseller-type**. Take note of the **id** that will be generated.

3. Access the database of both platform's. **NOTE**: The manually chosen string has to be identical on both database.

3a. Access database to RG platform -> UPDATE company SET api_key=*MY_MANUALLY_CHOSEN_STRING* WHERE id=*CUSTOMER_ID_GENERATED_ON_STEP_1*.

3b. Access database to ERP platform -> UPDATE actor SET api_key=*MY_MANUALLY_CHOSEN_STRING* WHERE id=*ACTOR_ID_GENERATED_ON_STEP_2*.

4. Create new user on the RG platform. Set user group to be **Reseller** and that they belong to the company created in step 1.

5. Optional to set the user to have Finnish localization: Take note of **user id** generated on step 4. Access database to RG platform -< UPDATE `user` SET default_language_id="fi", default_locale_id="fi" WHERE id=*USER_ID_GENERATED_ON_STEP_4*.

6. Test that you can login with new user to the RG platform.

7. Forward the details.