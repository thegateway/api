# Production mysql database backup

The backup is running in [ops.gwapi.eu](backup-server-changelog) and creates a database backup from epharma and ecome production databases running in the production RDS mariadb. The backup is run twice a week (wednesday and saturday) via mysqldump and archived in S3 bucket **tgw-devops-backups/epharma-production** with the name YYYY-MM-DD.db.tgz.

Backup is a tar file and the content are the two database sql dumbs.