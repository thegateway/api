# Base image

| what   | info                              |
| ------ | --------------------------------- |
| image  | ami-CentOS Linux 6 (ami-edb9069e) |
| type   | c4.large                          |
| access | mte,marko,wang,e-panda,kimvais    |

# Initial setup

1. installed users
1. copied the authorized keys
1. enabled wheel group to use sudo without password (/etc/sudoers)
1. allow keeping the SSH_AUTH_SOCK environment variable for sudo (/etc/sudoers)
1. added admin users to wheel group (vigr -s)
1. updated the system   `yum update`
1. reboot

# Enable epel and precompiled php55 repositories
```
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm
```

# Installing necessities

## For system

1. install lsb `yum install redhat-lsb`
1. install memcached `yum install memcached`
1. install nginx `yum install nginx`

## For compiling php extensions

1. `yum install gcc`
1. `yum install libcurl-devel`
1. `yum install zlib-devel`

# Install PHP55
1. install php55 and libraries
  - `yum install php55w`
  - `yum install php55w-devel`
  - `yum install php55w-common`
  - `yum install php55w-mysqlnd` 
  - `yum install php55w-fpm`
  - `yum install php55-bcmath`
  - `yum install php55w-pecl-memcache`
  - `yum install php55w-mbstring`
  - `yum install php55w-tidy`
  - `yum install php55w-opcache`
  - `yum install php55w-pecl-apcu`
  - `yum instlal php55w-gd`

# Install pecl extensions

  - `pecl install pecl_http-1.7.6`