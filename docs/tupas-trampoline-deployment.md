## Compile Python 3.6.x

```
$ wget https://www.python.org/ftp/python/3.6.4/Python-3.6.4.tar.xz
$ tar xJvf Python-3.6.4.tar.xz
$ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev   
$ sudo apt-get install -y libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm 
$ sudo apt-get install -y libncurses5-dev  libncursesw5-dev xz-utils tk-dev
$ cd Python-3.6.4
$ ./configure --enable-optimizations
$ make -j4
$ sudo make install # goes to /usr/local...
```

## Install certbot

`$ sudo pip3.6 install certbot certbot-nginx`

## Certify
```
$ sudo certbot --nginx -d tupas.servers.gwapi.eu
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator nginx, Installer nginx
Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel): devops@gwapi.eu

-------------------------------------------------------------------------------
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server at
https://acme-v01.api.letsencrypt.org/directory
-------------------------------------------------------------------------------
(A)gree/(C)ancel: a

-------------------------------------------------------------------------------
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about EFF and
our work to encrypt the web, protect its users and defend digital rights.
-------------------------------------------------------------------------------
(Y)es/(N)o: y
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for tupas.servers.gwapi.eu
Waiting for verification...
Cleaning up challenges
Deployed Certificate to VirtualHost /etc/nginx/sites-enabled/default for tupas.servers.gwapi.eu

Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
-------------------------------------------------------------------------------
1: No redirect - Make no further changes to the webserver configuration.
2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
new sites, or if you're confident your site works on HTTPS. You can undo this
change by editing your web server's configuration.
-------------------------------------------------------------------------------
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2
Redirecting all traffic on port 80 to ssl in /etc/nginx/sites-enabled/default

-------------------------------------------------------------------------------
Congratulations! You have successfully enabled https://tupas.servers.gwapi.eu

You should test your configuration at:
https://www.ssllabs.com/ssltest/analyze.html?d=tupas.servers.gwapi.eu
-------------------------------------------------------------------------------

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/tupas.servers.gwapi.eu/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/tupas.servers.gwapi.eu/privkey.pem
   Your cert will expire on 2018-05-02. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot again
   with the "certonly" option. To non-interactively renew *all* of
   your certificates, run "certbot renew"
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

## Added HTTPS traffic as allowed in Lightsail console in AWS.

## Conf nginx

see example at https://docs.aiohttp.org/en/stable/deployment.html

This is what `/etc/nginx/sites-enabled/default` looks like right now.
```
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
                try_files $uri $uri/ =404;
        }
}

server {

        root /var/www/html;

        index index.html index.htm index.nginx-debian.html;
    server_name tupas.servers.gwapi.eu; # managed by Certbot


    location / {
      proxy_set_header Host $http_host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_redirect off;
      proxy_buffering off;
      proxy_pass http://aiohttp;
    }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/tupas.servers.gwapi.eu/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/tupas.servers.gwapi.eu/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = tupas.servers.gwapi.eu) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen 80 ;
        listen [::]:80 ;
    server_name tupas.servers.gwapi.eu;
    return 404; # managed by Certbot
}

upstream aiohttp {
  server 127.0.0.1:8080 fail_timeout=0;
}
```

## Created a venv and installed the tupas trampoline
```
$ python3.6 -m venv env
$ . env/bin/activate
$ pip install twgio_tupas_trampoline-0.0.1-py3-none-any.whl
```
## install and configure supervisor
```
$ sudo apt-get install supervisor
$ cat >> /etc/supervisor/conf.d/tupas-trampoline.conf
[program:tupas-trampoline-server]
numprocs = 1
user =nobody
autostart=true
autorestart=true
command=/home/admin/env/bin/tupas-trampoline-server
```

and `/etc/init.d/supervisor restart`


