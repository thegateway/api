## {erp,rp,shop}.dev.epharma.fi

### 10.10.2017 (mte, e-panda)

* git pulled to latest revision
* db migration ran


### 18.9.2017 (wang)

* updated /var/www/epharma to the latest master commit, discarded one local change in /protected/controllers/Frontend/CustomerApiController.php: 
  "+"        // $params['order'] = 'name';
* executed `sudo chmod 777 /var/www/epharma/protected/runtime` due to error: `exception 'CException' with message 'Application runtime path "/var/www/epharma/protected/runtime" is not valid. Please make sure it is a directory writable by the Web server process.` when trying with command `protected/yiic`

### 3.8.2017 (mte)

* In an attempt to fix php gd module copied the files installed by the package from /opt/remi/php55/root -> / (/usr/lib64/php/modules/gd.so, /etc/php.d/gd.ini)

### 2.8.2017 (wang)  

* Downloaded and installed php gd (php55-php-tidy.x86_64) module by yum
* The php gd module did not work as expected, may require adding symbolic links or copying files around. Task suspended and will ask mte first.

### 2.8.2017 (mte)

* updated the certificates
* installed python27 (required by certbot)

### 30.06.2017 (panda)

* Cloned tupas to /var/www/tupas.
* Downloaded and setup composer (https://getcomposer.org/) to /var/www/tupas. Uninstalling happens by deleting the composer.phar.

### 16.06.2017 (kimvais)

* Removed redirection to S3 bucket for uploads, `/etc/nginx/conf.d/erp.dev.epharma.fi.conf.20170616001` is the back-up of old configuration

### 14.06.2017 Wednesday (marko)

* /var/www/epharma commit 7fb8f2b7bced68e7f30af3edc4e68356741d100d
* /var/www/ecome commit 7a79cf9cf2ab35824afd9cfe8a5e9c48431e68c8

### 07.06.2017 Wednesday (mte)

* for some reason the tidy php library was not automatically loaded by php-fpm service fixed this by manually adding the extension to /etc/php.d and the shared library to /usr/lib64/php/modules
* /var/www/epharma commit c73e3b21aff46a6de8c0a570c602a9f3665915f2
* /var/www/ecome commit 7a79cf9cf2ab35824afd9cfe8a5e9c48431e68c8

### 07.06.2017 Wednesday (Wang)

* installed tidy module for PHP via: `yum install php55-php-tidy.x86_64`
* ~~add path of `tidy.so` to etc\php.ini~~ (then removed, because it is still not correctly loaded)

### 30.05.2017 Tuesday (mte)

* added gimmelogentry command
* change /var/www/epharma and ecome group to wheel and made files group writable
* /var/www/epharma commit f2e8d3a9c2a6c9faab429b135ba1c37b78f48353
* /var/www/ecome commit 7a79cf9cf2ab35824afd9cfe8a5e9c48431e68c8

### 2017-05-10 (Wang)  

* Configuration of url endpoint fix;

### 2017-04-28 

* Updated certificates using certbot. Note you need to make sure that the nginx and php-fpm services are shutdown.

## ops.dev.epharm.fi

### 30.05.2017 Tuesday (mte)

* added mysql client and libraries
* added access for marko

## 2017-05-17 mte

* created envs/apiv2-python36 for gitlab-ci-runner

## 2017-05-12 kimvais

* compiled python3.6 to /usr/local

## 2017-05-12 mte

* Added access for kimvais