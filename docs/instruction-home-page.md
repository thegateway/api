# Amazon Web Services

Instructions assume you have access to the AWS console and know how to use it for different tasks. See AWS documentation if you detailed instructions on each individual step.

## Setting up certificate for domain

1. Setup workmail for the domain
1. Create certificate request
1. Approve the certificate (or somebody in the technical mail group needs to approve it)

## Setting up workmail for domain

1. add domain to the group
1. create the required TXT and MX records in DNS (route53)
1. add the admin@domain to the technical mails group

# Changes to host

Whenever you make changes to production / testhost you should create the appropriate changlog entry in the hosts changelog in this wiki. On production and staging there is a command `gimmelogentry`(in /usr/local/bin) that gives you a cut-n-paste text to use as the base for the entry.

# Importing SAL data to the erp
apiv2 contains the code for the gwapicli scritp. The script is globally accessible in ops.gwapi.eu host as gwapicli command (the instructions assume you are running the import on this particular host).

Steps
1. set SQL_CONNECT `export SQL_CONNECT="mysql+pymysql://user:password@host:3306/epharma?charset=utf8&use_unicode=1"`
1. create a working directory for the import (it is recommended to have the date of importing as the name of directory)
   1. create a separate `output` directory within the import directory
   1. copy the production configuration file `production-pkgs.json` for the import into this directory (from apiv2/data)
   1. copy up-to-date category fix file (obtainable from vellu) here also
1. trigger production database backup
1. fetch the current sal database
`gwapicli fetch-sal --user=username --password=password sal.xml`
1. run the import
`gwapicli import-fimea-to-epharma --verbose true --package-config production-pkgs.json --file-output-dir output sal.xml`
1. after the command has completed start the image upload
`gwapicli update-images output/fetch.json`
1. finally apply category fixes
`gwapicli update-erp-product-categories-packages --verbose true --config production-pkgs.json --file-output-dir output category-fix-file`
1. deliver the newly created category fix file (`output/updated_product_categories_packages.csv`) back to vellu

# Manually triggering the backup of production mariadb (old epharma system)

- log into ops.gwapi.eu
- trigger the backup with command `sudo -ubackup /usr/local/bin/backupdb -i`

This will create a new backup of both ecome and epharma database to s3://tgw-devops-backups/epharma-production/