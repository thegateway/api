# Connecting an app to the ERP

## ERP

### Keys to the database
We need to add records to `oauth_public_keys` and `oauth_clients`. It is important that the `client_id` is matching in both records. The `oauth_clients.redirect_uri` is not important.
```
INSERT INTO `oauth_public_keys` (`client_id`, `public_key`, `private_key`, `encryption_algorithm`) 
VALUES ('sport_gateway_client_id', '{{PUBLIC_KEY_HERE}}', '{{PRIVATE_KEY_HERE}}', 'RS256');

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) 
VALUES ('sport_gateway_client_id', '', 'http://this.is.pandaaaaaaa/', NULL, NULL, NULL);
```