############################################
Identities, Entities and Roles and their use
############################################

*"We know what we are, but not what we may be"* -- William Shakespeare

*********************************
Role based access control in DBv2
*********************************

When using the DBv2 API each connection has four "variables" defined
(this actual implementation how these are passed is still open; they
can be encoded into access / identity tokens or saved in the session)

entity
   the entity we "targeting" (**must** always be a non user type). For example
   this could be the webshop the user is currently accessing, the brand
   (s)he is managing etc.

login_entity
  the identified *actual user* that has logged into the system 

acting_entity
  the user acting the role (typically this is the same as the login_entity,
  but for example in a case of a custodian shopping for a minor the login_entity
  would be that of the custodian and the acting_entity would be that of the minor)

role
  the role the *acting_enitity* has under the *entity*. There can only be one
  role active, but the role can be switched during the session.

Access control
==============
The checks are always performed using the current *role* in current *entity* and
the permission always defaults to **deny**. The check can be applied on REST api level.
Something like this [#code]_.

::

  @requires('pharmacist')
  def post(..):
      .. 

Or it can be checked explictly in the code

::

  if current_role() == 'pharmacist':
      ..

For a more relaxed check can be performed by checking if the acting entity can assume
a given role

::

   @assume('pharmacist')
   def get(..):
       ..

::

   if can_assume('pharmasist'):
       ..

Login entity
============

The login entity is set by the *identities* table e.g. after the user has been identified
via identity provider and a matching entity of the role *user* has been found, that entity
becomes the *login_entity* and *acting_entity*. Uset that does not have a matching entity
of type *user* cannot log into the system.



Selecting the initial entity and role
=====================================

Generic login
*************

After the login_entity has been determined the *roles* table is search for possible roles,
it there is **only one** defined role for the login entity then the entity of that role is
set as the current entity and the *role* is set to the defined role.

If there are multiple entity/roles (or none) then it is up to the UI to present a sensible
selection (*needs to be defined* what this actually means).

Setting entity on login
***********************

The UI can also request for specific entity based on the landing page (for example login
to specific webshop). In this case the UI provides both the identity and the entity. If
the login_identity can assume single role on for the entity (again either via *roles* table
or via *default_role*) that role is set as the current role and the requested entity as the
entity. 

If there are multiple roles the login entity can assume for the entity then it is up to the
UI to present the list of roles and have the user to select one.

.. note:: if the user cannot assume any roles for the entity then the login will be denied


Setting entity and role on login
********************************

In addition to specific entity the UI can also request the role on login
(again based on the landing page or similar mechanism). If the login entity cannot assume
that role then the login is denied, if it can but the role is defined via the
*default_role* it is added to the *roles* table.

Switching entities and roles
============================

Switching entity
****************

The entity can be switched at any point to any entity that the *acting_entity* has a role
(either explictly via the *roles* table or implicitly via the *default_role*). If the user
switches to *implicitly* defined role then that role is added to the *roles* table in order
to implemented the shop specific customer list and to make it easier to automatically set
the entity / role or subsequent logins.

It is up to the UI to provide the option for changing the entity. Entity change is
treated as a login and can fail for the same reasons, so the UI should query for
entities the acting_user can switch to.

Switching acting_entity
***********************

Login entity can switch to any acting user it has *proxy* role for in the *roles* table and
the new acting_entity has a defined role (again either explicitly via the *roles* table or
implictly via the *default_role*) within the current entity.


Switching role
**************

The UI can request a change of role for the acting_entity within the entity. The new role
**must** be available for the acting_user.

.. rubric:: Footnotes

.. [#code] all code presented here is just for mockup/reference, actual implementation 
           details are still pending.

