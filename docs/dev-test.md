# Base image

| what   | info                              |
| ------ | --------------------------------- |
| image  | Debian Jessie (ami-11c57862) |
| type   | t2.micro                          |
| access | mte,marko,wang,e-panda,kimvais    |

# Initial setup

1. installed users
1. copied the authorized keys
1. enabled sudo group to use sudo without password (/etc/sudoers)
1. allow keeping the SSH_AUTH_SOCK environment variable for sudo (/etc/sudoers)
1. added admin users to sudo group (vigr -s)
1. updated the system   `apt-get update && apt-get upgrade`
1. reboot