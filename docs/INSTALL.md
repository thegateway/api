# Install required tools

Installation requires PowerShell Core, preferably version 7.0 or higher and AWS Tools for PowerShell

```powershell
Set-PSRepository -Name PSGallery -InstallationPolicy trusted
Install-Module AWS.Tools.CloudWatch,AWS.Tools.CognitoIdentity,AWS.Tools.CognitoIdentityProvider,AWS.Tools.Common,AWS.Tools.ECS,AWS.Tools.Lambda,AWS.Tools.S3,AWS.Tools.APIGateway,AWS.Tools.CloudFront,AWS.Tools.CertificateManager,AWS.Tools.DynamoDBv2

```

# Get the most recent backend code 
 
Download the API layer artifact as Zip file from the GitLab CI manually.

*OR*

Use existing account - e.g. Galeria PropCo or something else that has automatic deployments set up or d

## Download gwio-api layer
```powershell
$VER = (Get-LMLayerVersionList -LayerName gwio-api | Select -First 1).Version 
Invoke-WebRequest (Get-LMLayerVersion -LayerName gwio-api -ProfileName thegateway -Region eu-west-1 -VersionNumber $VER).Content.Location -OutFile $HOME/layer.zip
```

## Do the same for pandas:
```powershell
$VER = 3
Invoke-WebRequest (Get-LMLayerVersion -LayerName pandas-0_25_1 -ProfileName thegateway -Region eu-west-1 -VersionNumber $VER).Content.Location -OutFile $HOME/pandas.zip
```
   
# Set up your local PowerShell 

This is for your own convenience :)

Add the AWS keys to `~/.aws/credentials` under `[<domain_name_goes_here>]`

Set environment variable AWS_PROFILE to match your and AWS_DEFAULT_REGION to whatever region you are deploying this in, probably `eu-central-1`.
```powershell
$DOMAIN="<domain_name_goes_here>"
$Env:AWS_DEFAULT_REGION="eu-central-1"
$Env:AWS_PROFILE=$DOMAIN
$AWS_REGION=$Env:AWS_DEFAULT_REGION
$AWS_PROFILE=$Env:AWS_PROFILE
``` 

Switch to correct AWS profile and region_

```powershell
Set-AWSCredential -ProfileName $DOMAIN
Set-DefaultAWSRegion $AWS_REGION
```
Set up some local variables that we'll need 
```powershell
$ACCOUNT_ID=(Get-STSCallerIdentity).Account
```
   
You can omit this, but in that case you need to pass `-ProfileName $AWS_PROFILE` and `-Region $AWS_REGION` to every single command from now on...

# Publish the layers

```powershell
Publish-LMLayerVersion -CompatibleRuntime "python3.7","python3.8" -LayerName "gwio-api" -Content_ZipFile ([system.io.file]::ReadAllBytes("$HOME/layer.zip"))
Publish-LMLayerVersion -CompatibleRuntime "python3.7","python3.8" -LayerName "pandas-0_25_1" -Content_ZipFile ([system.io.file]::ReadAllBytes("$HOME/pandas.zip"))
```

# Set up S3

Create two buckets (it's best to pass the region explicitly, because if this fails, you have to wait for 2-3 hours to be able to re-create the buckets!)

```powershell
New-S3Bucket -Region $AWS_REGION $DOMAIN -CannedACLName public
New-S3Bucket -Region $AWS_REGION private.$DOMAIN
```

# Set up Certificates for Cloudfront

Create a wildcard certificate for the domain (Remember to do this in N.Virginia! (`us-east-1`), otherwise we can't use it in CloudFront)
```powershell
New-ACMCertificate -Region us-east-1 -DomainName $DOMAIN -SubjectAlternativeName "*.$DOMAIN" -ValidationMethod DNS
$CERTIFICATE_ARN = (Get-ACMCertificateList -Region us-east-1)[0].CertificateArn
```

Get the DNS validation options e.g:

```powershell
(Get-ACMCertificatedetail -CertificateArn $CERTIFICATE_ARN -Region us-east-1).DomainValidationOptions[0].ResourceRecord
```
    
## Verify the certificate.

Add the output of the previous command to the DNS records (Route53) for the domain you are setting up.
It's easier to do this in AWS console but here's what gets you started in PowerShell (remember to pass `-ProfileName` if the domain was registered on a different account!)

```powershell
Get-R53HostedZonesByName | Where-Object {$_.Name -eq "$DOMAIN."}
```
    
# Set-up cognito 


## Set-up Cognito identity provider (User pool)

```powershell
$cognito_user_pool=New-CGIPUserPool -Region eu-central-1 -PoolName dev -UserNameAttributes "email","phone_number" -AutoVerifiedAttribute "email" -Policies_PasswordPolicy_MinimumLength 12 -PasswordPolicy_RequireLowercase $false -PasswordPolicy_RequireUppercase $false -PasswordPolicy_RequireNumber $false -PasswordPolicy_RequireSymbol $false
$cognito_client=New-CGIPUserPoolClient -UserPoolId $cognito_user_pool.Id -ClientName API -GenerateSecret 0 -Region eu-central-1 -ExplicitAuthFlow ALLOW_ADMIN_USER_PASSWORD_AUTH,ALLOW_CUSTOM_AUTH,ALLOW_REFRESH_TOKEN_AUTH,ALLOW_USER_SRP_AUTH
New-CGIPGroup -GroupName admin -Region eu-central-1 -UserPoolId $cognito_user_pool.Id
New-CGIPGroup -GroupName customer -Region eu-central-1 -UserPoolId $cognito_user_pool.Id
New-CGIPGroup -GroupName product_owner -Region eu-central-1 -UserPoolId $cognito_user_pool.Id
New-CGIPGroup -GroupName shopkeeper -Region eu-central-1 -UserPoolId $cognito_user_pool.Id
```

## Set-up Cognito Federated identities (Identity pool)

```powershell
$cognito_id_provider = New-Object Amazon.CognitoIdentity.Model.CognitoIdentityProviderInfo
$cognito_id_provider.ClientID = $cognito_client.ClientId
$cognito_id_provider.ProviderName = "cognito-idp.$REGION.amazonaws.com/$($cognito_user_pool.Id)"
$cognito_federated_identity_pool=New-CGIIdentityPool -IdentityPoolName dev -AllowUnauthenticatedIdentity $true -Region eu-central-1  -CognitoIdentityProvider $cognito_id_provider
```
# Set up configuration in SSM

## Create SNS topic for errors

```powershell
Install-AWSModule SimpleNotificationService
$ERROR_SNS_TOPIC_ARN=New-SNSTopic -Name dev-lambda-errors
```


## Create SECRET_KEY

```powershell
[Byte[]] $bytes = 1..32
$rng.getbytes($bytes)
$SECRET_KEY = [Convert]::ToBase64String($bytes)
```

## Set up KMS
   
```powershell
Install-AWSToolsModule KeyManagementService
$configuration_key_id = (New-KMSKey).KeyId
New-KMSAlias -AliasName alias/configuration_encryption_key -TargetKeyId $configuration_key_id
$configuration_key_arn=(Get-KMSKey $configuration_key_id).arn
```

## Save variables to SSM
   
```powershell
Write-SSMParameter -Name "/dev/sma/COGNITO_POOL_ID" -Value $cognito_user_pool.Id -Type string
Write-SSMParameter -Name "/dev/sma/COGNITO_CLIENT_ID" -Value $cognito_client.ClientId -Type string
Write-SSMParameter -Name "/dev/sma/COGNITO_FEDERATED_IDENTITY_POOL_ID" -Value $cognito_federated_identity_pool.IdentityPoolId -Type string
Write-SSMParameter -Name "/dev/sma/SYSTEM_SECRET_KEY" -Value $SECRET_KEY -Type securestring -KeyId $configuration_key_id
Write-SSMParameter -Name "/dev/sma/ERROR_SNS_TOPIC_ARN" -Type string -Value $ERROR_SNS_TOPIC_ARN
Write-SSMParameter -Name "/dev/sma/SWAGGER_UI_URL" -Type string -Value "https://public.the.gw/apidoc/index.html"
Write-SSMParameter -Name "/dev/sma/DEFAULT_LANGUAGE" -Type string -Value "en"
```

# Set up IAM

## Get policies

Copy policy.json from api/sample/aws_lambda_policy.json and ADD the $configuration_key_arn as "Resource" in the `VisualEditor` section that has `Action` `kms::decrypt`

```powershell
$policy_fn = Join-Path $pwd "policy.json"
$policy = [system.io.file]::ReadAllText($policy_fn)
New-IAMPolicy -ProfileName $AWS_PROFILE -PolicyDocument $policy -PolicyName 'thegw-api-lambda-permissions'
```

Copy the assumed role policy from gw and create the ZappaLambdaExecution role

```powershell
$arpd = (Get-IAMRole -RoleName ZappaLambdaExecution).AssumeRolePolicyDocument
$assumed_role_policy = [System.Web.HTTPUtility]::UrlDecode($ardp)
New-IAMRole -ProfileName $AWS_PROFILE -RoleName ZappaLambdaExecution -AssumeRolePolicyDocument $assumed_role_policy
```

## Attach the policy `AWSLambdaBasicExecutionRole` to the `ZappaLambdaExecution` role

```powershell
$lambda_exec_role = (Get-IAMPolicies | Where PolicyName -eq 'AWSLambdaBasicExecutionRole')[0].Arn
Register-IAMRolePolicy -RoleName ZappaLambdaExecution -PolicyArn $lambda_exec_role
```


## Attach OUR policy

```powershell
$policy_arn = (Get-IAMPolicyList -Scope Local|Where PolicyName -eq thegw-api-lambda-permissions)[0].arn
Register-IAMRolePolicy -RoleName ZappaLambdaExecution -PolicyArn $policy_arn
```

# Set up Zappa & deployment environment
    
0. make virtualenv, install zappa and simplejson
0. run `python zappa_settings_tool.py`
0. mkdir deploy; cp zappa_settings.json deploy; cd deploy
0. run zappa deploy (in deploy/) 

# Deploy layers

## Configure the Lambda function

We need to run the function in a VPC so that we can access ElasticSearch
```powershell
$lm_subnets=Get-EC2Subnet
$subnet_ids=$lm_subnets.SubnetId
$lm_vpc=Get-EC2Vpc
$lm_sg=Get-EC2SecurityGroup
$function_name = (Get-LMFunctionList|Select -First 1).FunctionName
$pandas_arn = (Get-LMLayerVersionList -LayerName pandas-0_25_1|Select -First 1).LayerVersionArn
$gwio_api_arn = (Get-LMLayerVersionList -LayerName gwio-api|Select -First 1).LayerVersionArn
Update-LMFunctionConfiguration -FunctionName $function_name `
 -Layer $pandas_arn,$gwio_api_arn `
 -Environment_Variables @{NAMESPACE="/dev/sma"; ERROR_SNS_TOPIC_ARN=$ERROR_SNS_TOPIC_ARN;} `
 -VpcConfig_SecurityGroupId $lm_sg.SecurityGroupId -VpcConfig_SubnetId $lm_subnets
```

## Store the invoke url 
```powershell
$invoke_url = "https://{0}.execute-api.{1}.amazonaws.com/dev/" -f (Get-AGRestApiList|Select -First 1).Id,$AWS_REGION
```

## Set up Lambda environment variables

# Test it
```powershell
iwr $invoke_url
zappa tail
```
    

# Set up CloufFront distributions

## Make public bucket public
```powershell
$public_read = [Amazon.S3.S3CannedACL]::PublicRead
Set-S3ACL -BucketName $DOMAIN -CannedACL $public_read
```
NOTE! If the bucket already has the dashboard and app uploaded, you need to log on to AWS console, and make the app/ and dashboard/ "directories" public in the UI.

## Create distributions

### Create custom error messages
We need to create custom error page for `404` so the routing works correctly in App & Dashboard

```powershell
$custom_error_response = New-Object Amazon.CloudFront.Model.CustomErrorResponse
$custom_error_response.ErrorCode=404
$custom_error_response.ResponseCode=200
$custom_error_response.ResponsePagePath="/index.html"
```

### App

```powershell
$s3_origin = New-Object Amazon.CloudFront.Model.Origin
$s3_origin.DomainName = "$DOMAIN.s3.amazonaws.com"
$s3_origin.S3OriginConfig = New-Object Amazon.CloudFront.Model.S3OriginConfig
$s3_origin.S3OriginConfig.OriginAccessIdentity = ""
$s3_origin.Id = "S3-origin-$DOMAIN-app"
$s3_origin.OriginPath = "/app"
$app_cf_distribution=New-CFDistribution `
    -DistributionConfig_Enabled $true `
    -DistributionConfig_Comment "Desktop app for $DOMAIN" `
    -Origins_Item $s3_origin `
    -Origins_Quantity 1 `
    -Logging_Enabled $true `
    -Logging_IncludeCookie $true `
    -Logging_Bucket private.$DOMAIN.s3.amazonaws.com `
    -Logging_Prefix "logs/desktop/" `
    -DistributionConfig_CallerReference Client-app `
    -DistributionConfig_DefaultRootObject index.html `
    -DefaultCacheBehavior_TargetOriginId $s3_origin.Id `
    -ForwardedValues_QueryString $true `
    -Cookies_Forward all `
    -WhitelistedNames_Quantity 0 `
    -TrustedSigners_Enabled $false `
    -TrustedSigners_Quantity 0 `
    -DefaultCacheBehavior_ViewerProtocolPolicy redirect-to-https `
    -DefaultCacheBehavior_MinTTL 360 `
    -DistributionConfig_PriceClass "PriceClass_All" `
    -CacheBehaviors_Quantity 0 `
    -Aliases_Quantity 1 `
    -Aliases_Item $DOMAIN `
    -CustomErrorResponses_Item $custom_error_response `
    -CustomErrorResponses_Quantity 1 `
    -ViewerCertificate_CloudFrontDefaultCertificate $false `
    -ViewerCertificate_ACMCertificateArn $CERTIFICATE_ARN `
    -ViewerCertificate_MinimumProtocolVersion "TLSv1.2_2018" `
    -ViewerCertificate_SSLSupportMethod "sni-only"
$app_cf_url=$app_cf_distribution.Distribution.DomainName
```
### Dashboard

We can re-use the S3 origin for APP, just change the fields that are different:
```powershell
$s3_origin.Id = "S3-origin-$DOMAIN-dashboard"
$s3_origin.OriginPath = "/dashboard"
$dashboard_cf_distribution=New-CFDistribution `
    -DistributionConfig_Enabled $true `
    -DistributionConfig_Comment "Dashboard for $DOMAIN" `
    -Origins_Item $s3_origin `
    -Origins_Quantity 1 `
    -Logging_Enabled $true `
    -Logging_IncludeCookie $true `
    -Logging_Bucket private.$DOMAIN.s3.amazonaws.com  `
    -Logging_Prefix "logs/dashboard/" `
    -DistributionConfig_CallerReference Client-dashboard `
    -DistributionConfig_DefaultRootObject index.html `
    -DefaultCacheBehavior_TargetOriginId $s3_origin.Id `
    -ForwardedValues_QueryString $true `
    -Cookies_Forward all `
    -WhitelistedNames_Quantity 0 `
    -TrustedSigners_Enabled $false `
    -TrustedSigners_Quantity 0 `
    -DefaultCacheBehavior_ViewerProtocolPolicy redirect-to-https `
    -DefaultCacheBehavior_MinTTL 360 `
    -DistributionConfig_PriceClass "PriceClass_All" `
    -CacheBehaviors_Quantity 0 `
    -Aliases_Quantity 1 `
    -Aliases_Item pro.$DOMAIN `
    -CustomErrorResponses_Item $custom_error_response `
    -CustomErrorResponses_Quantity 1 `
    -ViewerCertificate_CloudFrontDefaultCertificate $false `
    -ViewerCertificate_ACMCertificateArn $CERTIFICATE_ARN `
    -ViewerCertificate_MinimumProtocolVersion "TLSv1.2_2018" `
    -ViewerCertificate_SSLSupportMethod "sni-only"
$dashboard_cf_url=$dashboard_cf_distribution.Distribution.DomainName
```

# Build (or update) the Apps

## Desktop

1. Build gatewatfront-core by running `npm run build`.
2. Update gatewayfront-core for the app by running `yarn core:update`.
3. Upload content to S3 and invalidate Cloudfront with `make update-production`.

## iOS

1. Build gatewatfront-core by running `npm run build`.
2. Update gatewayfront-core for the app by running `yarn core:update`.
2. Run `ng build --prod`.
3. Synchronize content to iOS by running `npx cap sync ios`.
4. Run `npx cap open ios` and on XCode archive and upload content to Apple App Store.

## Android

1. Build gatewatfront-core by running `npm run build`.
2. Update gatewayfront-core for the app by running `yarn core:update`.
2. Run `ng build --prod`.
3. Synchronize content to iOS by running `npx cap sync android`.
4. Run `npx cap open android` and on Android Studio generate signed APK file and upload that to Google Play Store.

# Create DynamoDB tables

Create a new virtualenv where you install the API - or unzip the gwio-api layer and add the directory to your $Env:PYTHONPATH!

````powershell
$Env:GW_VERTICAL_NAME="sma"
$Env:GW_APPLICATION_ENVIRONMENT="dev"
$Env:AWS_DEFAULT_REGION=$AWS_REGION
$Env:AWS_PROFILE=$DOMAIN
Set-DefaultAWSRegion $AWS_REGION
Set-AWSCredential -ProfileNAme $DOMAIN
python path_to_api/backend/src/tools/create_missing_tables.py
````
...the python command WILL run for quite a while.

# Enable VPC Endpoints for services
```powershell
$default_vpc=Get-EC2Vpc
$route_table_id = (Get-EC2RouteTable).RouteTableId
New-EC2VpcEndpoint -VpcId $default_vpc.VpcId -ServiceName com.amazonaws.eu-central-1.s3 -RouteTableId $route_table_id
New-EC2VpcEndpoint -VpcId $default_vpc.VpcId -ServiceName com.amazonaws.eu-central-1.dynamodb -RouteTableId $route_table_id
New-EC2VpcEndpoint -VpcId $default_vpc.VpcId -VpcEndpointType interface -SubnetIds $subnet_ids -SecurityGroupId $lm_sg.GroupId -ServiceName com.amazonaws.eu-central-1.sns
New-EC2VpcEndpoint -VpcId $default_vpc.VpcId -VpcEndpointType interface -SubnetIds $subnet_ids -SecurityGroupId $lm_sg.GroupId -ServiceName com.amazonaws.eu-central-1.ssm
New-EC2VpcEndpoint -VpcId $default_vpc.VpcId -VpcEndpointType interface -SubnetIds $subnet_ids -SecurityGroupId $lm_sg.GroupId -ServiceName com.amazonaws.eu-central-1.secretsmanager
```
# Set up ElasticSearch cluster

## Install the necessary AWS modules
```powershell
Install-Module AWS.Tools.IdentityManagement,AWS.Tools.ElasticSearch
```
## Set up a non-default VPC and VPC peering connection.
You can omit this step if you are setting a production environment and someone is footing the 300€/month bill of "proper" EC2 that is also available in
eu-central-1 region. In that case, set `$es_instance_type` to `c5.large.elasticsearh`
```powershell
$es_instance_type="t2.small.elasticsearch"
$ES_DOMAIN_NAME="production"
$ES_REGION="eu-central-1"
$JSON = @{
Version = "2012-10-17"
Statement = @{
Effect="Allow";
Principal=@{AWS="*"}
Action="es:*"
Resource="arn:aws:es:${ES_REGION}:${ACCOUNT_ID}:domain/${ES_DOMAIN_NAME}"
}}
$es_access_policy=($JSON|ConvertTo-Json)
# Create service linked role for ES.
New-IAMServiceLinkedRole -AWSServiceName es.amazonaws.com
# Create the ES Domain
$ES_DOMAIN=New-ESDomain -DomainName $ES_DOMAIN_NAME -ElasticsearchClusterConfig_InstanceType $es_instance_type -ElasticsearchClusterConfig_InstanceCount 1 -Region $ES_REGION -EBSOptions_EBSEnabled $true -EBSOptions_VolumeSize 10 -AccessPolicy $es_access_policy -VPCOptions_SubnetIds $subnet_ids[0] -ElasticsearchVersion 7.4
```
Have a cup of coffee. It takes a while for ElasticSearch domain to be ready, check status with:
```powershell
(Get-ESDomain -DomainName $ES_DOMAIN_NAME -Region $ES_REGION).Processing
```
once it returns `False`, the ElasticSearch is ready, we just need to grant access to it.

<s>

```powershell
$ip1 = @{ IpProtocol="tcp"; FromPort="443"; ToPort="443"; IpRanges="172.31.0.0/31" }
Grant-EC2SecurityGroupIngress -GroupId $ES_DOMAIN.VPCOptions.SecurityGroupIds[0] -IpPermission @( $ip1 ) -Region $ES_REGION
```
</s>

````powershell
$es_host=(Get-ESDomain -DomainName $ES_DOMAIN_NAME).Endpoints['vpc']
Write-SSMParameter -Name "/dev/sma/ELASTICSEARCH_HOST" -Value "https://$es_host" -Type string

````
# Update Route53

Add `A` and `AAAA` record _aliases_ for `$DOMAIN` pointing to `$app_cf_url` and `pro.$DOMAIN` pointing to `$dashboard_cf_url`

To do this in powershell `Install-Module AWS.Tools.Route53` and install [https://github.com/jayers99/AWS-Powershell-101](this tool)

# Set up message templates
```powershell
Write-SSMParameter -Name "$NAMESPACE/MESSAGE_TEMPLATE_LOCATION" -Type string -Value "s3://private.$DOMAIN/message_templates/"

```

# Update SecretsManager and lambda function environment from SSM
```powershell
$layer_version = (Get-LMLayerVersionList -LayerName gwio-api|Select -First 1).Version
api\backend\src\tools\ssm_to_secrets.ps1 /dev/sma
Update-TheGwLMFunction -FunctionName $LM_FN_NAME -LayerVersion $layer_version -Namespace $NAMESPACE
```

# Give internet access to Lambda function

This is needed for the Lambda function to access some public services, such as AWS Cognito.

```powershell
$public_subnet=New-EC2Subnet -VpcId $vpc_id -CidrBlock 172.31.254.0/24
New-EC2Tag -Resource $public_subnet.SubnetId -Tag @{Key="name"; Value="public"}
$elastic_ip = New-EC2Address
$nat_gw = New-EC2NatGateway -SubnetId $public_subnet.SubnetId -AllocationId $elastic_ip.AllocationId
$public_rt = New-EC2RouteTable -VpcId $vpc_id
$private_rt = New-EC2RouteTable -VpcId $vpc_id
New-EC2Tag -Resource $public_rt.RouteTableId -Tag @{Key="name"; Value="public"}
New-EC2Tag -Resource $private_rt.RouteTableId -Tag @{Key="name"; Value="private"}
$public_rt_assoc = Register-EC2RouteTable $public_rt.RouteTableId -SubnetId $public_subnet.SubnetId
$internet_gateway = Get-EC2InternetGateway
New-EC2Route -RouteTableId $public_rt.RouteTableId -DestinationCidrBlock "0.0.0.0/0" -GatewayId $internet_gateway.InternetGatewayId
New-EC2Route -RouteTableId $private_rt.RouteTableId -DestinationCidrBlock "0.0.0.0/0" -NatGatewayId $nat_gw.NatGateway.NatGatewayId
$subnet_ids|ForEach-Object { Register-EC2RouteTable -RouteTableId $private_rt.RouteTableId -SubnetId $_ }

```
...TO BE CONTINUED!
