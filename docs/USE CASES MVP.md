# USE CASES MVP

# 0. Common

## Payment methods
 * stripe

## Customer
 * Place order
 * Get notifications of order status
 * Pay orders
   - stripe

## Shopkeeper
 * Get notifications of new orders
 * See sales reports.
 * See placed orders.
 
## Brand
 * Get notifications of new orders.
 * See placed orders
 * Ship orders

## Admin
 * Manage 'shops'
 * Manage 'brands'

# 1. VPK
## Additional payment methods
 * invoice

## User
 * Pay orders
   - Invoice
   - Bank account
   - Stripe

## Shopkeeper (VPK)
  * n/a

## Admin (Hannu)
  * Process orders
  * Bill orders
  * Accept orders

   
# 2. TLP
## Additional payment methods
 * invoice

## User
 * Pay orders
   
## Shopkeeper (Contractor)
 * Generate, send and print proposals
 * Conduct surveys
 * Place orders
 
## Brand (Dan & Dave)
 * Get notifications of new orders.
 * Modify product database
 * Bill orders
 * Accept orders

__Dan & Dave are also admins here__


# 3. Cleaner
__No additional payment methods__

## User
 * Find a cleaner
  - by location
  - by availability by time
 * Suggest a time for appointment
 * Accept cleaner proposal
 * Pay
  - by credit card
 * Give feedback
 
## Shopkeeper
 * Register as a provider
 * Edit location
 * Edit hours available
 * Accept gigs
 * Make proposals

## Admin
 * Approve providers
 * Manage providers
 * List transactions
   - by provider
   - by customer
 * Manage customers

# 4. Payment
## User
 * register
   - do tupas authorization
 * receive notifications of new invoices
 * pay invoices

## Biller
 * check if user is subscribed to service
 * send invoices (as Finvoice)

## Admin
 ???


# 5. Remppa
## Additional payment methods
 * mash
 * paytrail

## User
 * create a project
   - kitchen
   - bathroom
 * upload attachments.
 * update process/progress of a project
 * find contractors, supplies
 * define a project manager
 * create orders
  - manually (or mark bought)
  - based on shopping lists
 * pay orders
 * accept/define contractor for each phase

## Shopkeeper (Contractor)
 * receive notifications about timeline changes.
 * send proposals
 * accept proposed gigs
 * make shopping lists
 * update progress

## Brand (Supplier)
  * accept orders
  * bill orders

## Admin
 * manage end customers



# ePharma
## User
## Shopkeeper
  * Manage campaigns

## Brand
  * Manage campaigns

## Admin
  * Manage campaigns

# Elfa
## User
## Shopkeeper
## Admin
