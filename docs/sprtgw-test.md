# Setting up the sports gateway test system

The sad and convulated story so far.

## Create the database

MariaDB is running locally, so switched to root and connected to the database server and created the two databases

```
[root@ip-10-0-0-178 ~]# mysql
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 5
Server version: 10.1.22-MariaDB MariaDB Server

Copyright (c) 2000, 2016, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database erp;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> create database rg;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> quit
Bye
```

Created table descriptions by running mysql dump (with -d option, e.g. do not include data) in the staging and then moved those files over to the sportsgw host and created rg database with ecome db scheme and erp db with epharma scheme.

Next cloned the epharma and ecome_platform repos into /var/www/erp and /var/www/rg respectively, also changed the ownership of those directories to nginx:nginx (the account nginx is running as), and then as nginx user created sportsgw branches into both repos

```
[mte@ip-10-0-0-178 ~]$ sudo -s -u nginx
bash-4.1$ cd /var/www/erp
bash-4.1$ git checkout -b sportsgw
Switched to a new branch 'sportsgw'
bash-4.1$ cd ../rg
bash-4.1$ git checkout -b sportsgw
Switched to a new branch 'sportsgw'
bash-4.1$ 
```

## Setup nginx and certificates

### Make sure nginx is started by default and that firewall allows access to webservice
After initial installation the nginx should be running (service nginx status). If it is not then you need to enable it (chkconfig nginx on). Most likely the firewall will prevent access to the http and https ports:

```
[root@sportsgw bin]# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            state RELATED,ESTABLISHED 
ACCEPT     icmp --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     tcp  --  anywhere             anywhere            state NEW tcp dpt:ssh 
REJECT     all  --  anywhere             anywhere            reject-with icmp-host-prohibited 

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
REJECT     all  --  anywhere             anywhere            reject-with icmp-host-prohibited 

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
[root@sportsgw bin]#
```

### Allow HTTP(s) the traffic

Manually adding the allow rules with iptables will not survive reboot so you need modify the permanent roles (`/etc/sysconfig/iptables` and `/etc/sysconfig/ip6tables`). 

1. stop the firewall `service iptables stop`
1. add lines after the ssh (port 22) line.
```
  -A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
  -A INPUT -m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT
 ```
1. start the firewall `service iptables start`

## Verify basic configuration

Basically what we need to do is make sure everything survives reboot. So reboot and then run all the required services are running after boot and traffic to http(s) is allowed.

### Create the certificates using letsencrypt

1. install certbot
```
cd /usr/local/bin
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
```
1. stop nginx `service nginx stop`
1. run certbot `/usr/local/bin/certbot-auto certonly --standalone -d sportgw-erp.gwapi.eu -d sportgw-rg.gwapi.eu`
  (NOTE this will trigger yum to install dependencies)
1. start nginx `service nginx start`

### create dhparam.pem

1. create directory `/etc/nginx/ssl`
1. in that directory run `openssl dhparam -out dhparam.pem 4096` *NOTE* this takes a loooong time

## Setup php-fpm

At this point lets configure the php-fpm to more or less match our environment. We need to make it run as nginx:nginx (as the repo directory permissions and also use the local socket for communication. Do the changes in `/etc/php-fpm.d/www.conf`

1. change the listen from tcp port to domain-socket (`/var/run/php-fpm/.sock`)
1. change the socket permissions to nginx:nginx and 0660
1. change the process php-fpm runs as from apache:apache to nginx:nginx
1. change the onwership of /var/lib/php/* (`chown nginx:nginx /var/lib/php/*`)
1. restart the php-fpm and verify that the socket file is created with proper ownership and permissions and that the php-fpm process is running as nginx user (not as apache or something else)
```
[root@sportsgw php-fpm.d]# ls -la /var/run/php-fpm/.sock 
srw-rw---- 1 nginx nginx 0 Apr  4 05:48 /var/run/php-fpm/.sock
[root@sportsgw php-fpm.d]# ps -elf | grep -i php
5 S root      2034     1  0  80   0 - 122747 ep_pol 05:48 ?       00:00:00 php-fpm: master process (/etc/php-fpm.conf)
5 S nginx     2035  2034  0  80   0 - 122747 skb_re 05:48 ?       00:00:00 php-fpm: pool www            
5 S nginx     2036  2034  0  80   0 - 122747 skb_re 05:48 ?       00:00:00 php-fpm: pool www            
5 S nginx     2037  2034  0  80   0 - 122747 skb_re 05:48 ?       00:00:00 php-fpm: pool www            
5 S nginx     2038  2034  0  80   0 - 122747 skb_re 05:48 ?       00:00:00 php-fpm: pool www            
5 S nginx     2039  2034  0  80   0 - 122747 skb_re 05:48 ?       00:00:00 php-fpm: pool www            
0 S root      2048  1529  0  80   0 - 25828 pipe_w 05:51 pts/0    00:00:00 grep -i php
[root@sportsgw php-fpm.d]# 
```

## Database access

Created user sportgw with access to everything (for now).

## PHP config
Update `/etc/php.ini`
* `date.timezone = Europe/Helsinki`
* `short_open_tag = On`

Restart php-fpm `sudo service php-fpm restart`

## Setting up the ERP application

### Creating copies of the .dist files

```
[marko@sportsgw erp]$ sudo cp index_scripts.php.dist index_scripts.php
[marko@sportsgw erp]$ cd protected/config/
[marko@sportsgw config]$ sudo cp localParams.php.dist localParams.php
[marko@sportsgw config]$ sudo cp mail.php.dist mail.php
[marko@sportsgw config]$ sudo cp db.php.dist db.php
[marko@sportsgw config]$ sudo cp consoleComponents.php.dist consoleComponents.php
```

### Modifying the config files
```
[marko@sportsgw erp]$ sudo vi index_scripts.php
[marko@sportsgw erp]$ cd protected/config/
[marko@sportsgw config]$ sudo vi localParams.php
[marko@sportsgw config]$ sudo vi mail.php
[marko@sportsgw config]$ sudo vi db.php
[marko@sportsgw config]$ sudo vi consoleComponents.php
```

#### index_scripts.php
* YII_DEBUG -> `true`

#### localParams.php
* erpUrl -> `https://sportgw-erp.gwapi.eu/`  
* components
    * session
        * cookieParams
            * domain -> `.sportgw-erp.gwapi.eu`
* customerService
    * email -> `tilaukset@sportgw-erp.gwapi.eu`
    * phone -> `012 345 6789`
* rgUrl -> `https://sportgw-rg.gwapi.eu/`

#### mail.php
* dryRun -> `true`

#### db.php
Update with the database name and credentials

#### consoleComponents.php
* request
        *hostInfo -> `https://sportgw-erp.gwapi.eu`

### Creating admin user
**This should be done after dumping data from other sources, or alternatively take extra steps to make sure dump doesn't get rid of the actor**  
Create roles
```mysql
INSERT INTO `role` (`role_id`, `role_name`, `role_short_desc`, `role_creation_date`, `role_update_date`, `is_active`)
VALUES
	(1, 'Super Admin', 'Super Administrator Role', '2015-09-30 21:46:31', '2015-10-01 00:46:31', '1'),
	(2, 'Admin', 'Administrator Role', '2015-09-30 21:46:31', '2015-10-01 00:46:31', '1'),
	(3, 'Reseller', 'Reseller Role', '2015-09-30 21:46:31', '2015-10-01 00:46:31', '1'),
	(4, 'Customer', 'Customer Role', '2015-09-30 21:46:31', '2015-10-01 00:46:31', '1'),
	(5, 'Cashier', 'Cashier Role', '2015-09-30 21:46:31', '2015-10-01 00:46:31', '1'),
	(6, 'Picker', 'Picker Role', '2015-09-30 21:46:31', '2015-10-01 00:46:31', '1'),
	(7, 'Office Manager', 'Office Manager', '2016-08-05 07:33:40', '2016-08-05 07:33:40', '1'),
	(8, 'Office Clerk', 'Office Clerk', '2016-08-05 07:34:00', '2016-08-05 07:34:00', '1'),
	(9, 'Pharmacy Owner', 'Pharmacy Owner', '2016-05-10 07:17:50', '2016-05-10 07:17:50', '1'),
	(10, 'Pharmacy employee', 'Pharmacy employee', '2016-07-28 10:35:44', '2016-07-28 10:35:44', '1'),
	(11, 'Brand Owner', 'Brand Owner', '2016-04-28 12:20:05', '2016-04-28 12:20:05', '1'),
	(12, 'Delivery Partner', 'Delivery Partner', '2016-05-04 14:19:21', '2016-05-04 14:19:21', '1');
```
Create actor
```mysql
INSERT INTO `actor` (`id`, `company_name`, `updated_at`, `created_at`, `actor_type`)
	VALUES
(1, 'Sport Gateway', '2017-04-04 13:39:00', '2017-04-04 13:39:00', 'reseller');


```
Create user
```mysql
INSERT INTO `user` (`id`, `actor_id`, `first_name`, `last_name`, `username`, `PASSWORD`, `role_id`, `created_at`, `updated_at`)
VALUES
	(1, 1, 'Sport', 'Test-Admin', 'testadmin', '$2a$12$/33UcTt5Yodhftjnfggev.PCHBlrFBJl4T/K4T8kzOmmCAf.82khe', 2, '2017-04-04 13:39:00', '2017-04-04 13:39:00');
```

### Filling migration table with correct data
Take dump of the `tbl_migration` from the database that you dumped the structure from
```
mysqldump -h db.dev.epharma.fi -uroot -p epharma tbl_migration > staging_migration_table.sql
```
Apply the data to the new and empty database
```
mysql -usportgw -p -D erp < ~/staging_migration_table.sql
```

### Dumping some product data from ePharma

Creating mysql dumps from ePharma  
Products of specific brand owners were required to be in the Sport Gateway for demo purpose so we dumped them from ePharma database.  
`mysql` commands are used for getting the required ids for the tables where we need joins to get only the desired data. Since `mysqldump` does not support joins we have to first query for the ids separately.  
`mysqldump` commands are used to generate the actual data dumps
```
mysqldump -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -uroot -p epharma actor > amer_actors.sql --where='id IN (10105, 10106, 10107)'

mysqldump -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -uroot -p epharma brand > amer_brands.sql --where='brand_owner_id IN (10105, 10106, 10107)'

mysql -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -e "SELECT pf.id FROM product_frame pf LEFT JOIN brand b ON b.id = pf.brand_id WHERE b.brand_owner_id IN (10105, 10106, 10107)" -uroot -p epharma > amer_product_frames_ids.txt

mysqldump -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -uroot -p epharma product_frame > amer_product_frames.sql --where='id IN (1565,1566,1567,1568,1569,1570,1571,1572,1573,1574,1575,1576,1577,1578,1579,1580,1596,1609,1610,1611,1612,1613,1614,1615,1616,1617,1618,1619,1620,1621,1622,1623,1624,1597,1598,1625,1626,1627,1628,1629,1630,1631,1632,1633,1634,1635,1636,1637,1638,1639,1640,1641,1642,1643,1644,1645,1646,1647,1648,1649,1650,1651,1652,1653,1654,1655,1656,1657,1658,1659,1660,1661,1662,1663,1664,1665,1666,1667,1668,1669,1670,1671,1672,1673,1674,1675,1676,1677,1678,1679,1680,1681,1682,1683,1684,1686,1687,1688,1689,1690,1691,1692,1693,1694,1695,1696,1697,1698,1699,1700,1701,1702,1703,1704,1705,1706,1707,1708,1709,1710,1711,1712,1713,1714,1715,1716,1717,1581,1582,1583,1584,1585,1586,1587,1588,1589,1590,1591,1592,1593,1594,1595,1718,1719,1720,1721,1722,1723,1724,1725,1726,1727,1728,1729,1730,1731,1732,1733,1734,1735,1736,1737,1738,1739,1740,1741,1599,1600,1601,1602,1603,1604,1605,1606,1607,1608,697,700,702,704,706,711,712,713,714,715,716,717,718,719,720,730,731,732,734,735,737,738,739,740,741,742,743,745,747,748,750,751,753,754,755,758,759)'

mysql -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -e "SELECT p.id FROM product p LEFT JOIN product_frame pf ON pf.id = p.product_frame_id LEFT JOIN brand b ON b.id = pf.brand_id WHERE b.brand_owner_id IN (10105, 10106, 10107)" -uroot -p epharma > amer_product_ids.txt

mysqldump -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -uroot -p epharma product > amer_products.sql --where='id IN (2159,13475,13476,13477,13478,13502,13503,13479,13480,13481,13482,13483,13484,13504,13506,13505,13485,13486,13487,13488,13489,13490,13491,13492,13493,13494,13495,13496,13497,13498,13499,13500,13501,13432,13383,13384,13385,13386,13387,13388,13389,13390,13391,13392,13393,13394,13395,13396,13397,13398,13399,13400,13401,13402,13403,13404,13405,13406,13407,13408,13409,13410,13411,13412,13413,13414,13415,13416,13417,13418,13419,13420,13421,13422,13423,13424,13425,13426,13427,13428,13429,13430,13431,13433,13434,13435,13436,13437,13438,13439,13440,13441,13442,13443,13444,13445,13446,13447,13448,13449,13450,13451,13452,13453,13454,13455,13456,13457,13458,13459,13460,13461,13462,13463,13464,13465,13466,13467,13468,13469,13470,13471,13472,13473,13474,11042,11043,11044,11045,11046,11047,11048,11049,11050,11051,11052,11053,11054,11055,11056,11057,11058,11059,11060,11061,11063,11064,11065,11066,11067,11068,11069,11070,11071,11072,11073,11074,11075,11076,11077,11078,12439,13371,13372,13373,13374,13375,13376,13377,13378,13379,13380,13381,13382,964,965,968,969,971,972,974,975,978,980,982,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,1007,1009,1015,1010,1016,1011,1013,1014,1018,1019,1020,1021,1022,1023,1024,1025,1026,1028,1029,1030,1032,1033,1035,1036,1038,1039,1041,1043,1044,1045)'

mysqldump -h epharma-production.ccv0ta0fgj8j.eu-west-1.rds.amazonaws.com -uroot -p epharma product_image > amer_product_images.sql --where='product_id IN (2159,13475,13476,13477,13478,13502,13503,13479,13480,13481,13482,13483,13484,13504,13506,13505,13485,13486,13487,13488,13489,13490,13491,13492,13493,13494,13495,13496,13497,13498,13499,13500,13501,13432,13383,13384,13385,13386,13387,13388,13389,13390,13391,13392,13393,13394,13395,13396,13397,13398,13399,13400,13401,13402,13403,13404,13405,13406,13407,13408,13409,13410,13411,13412,13413,13414,13415,13416,13417,13418,13419,13420,13421,13422,13423,13424,13425,13426,13427,13428,13429,13430,13431,13433,13434,13435,13436,13437,13438,13439,13440,13441,13442,13443,13444,13445,13446,13447,13448,13449,13450,13451,13452,13453,13454,13455,13456,13457,13458,13459,13460,13461,13462,13463,13464,13465,13466,13467,13468,13469,13470,13471,13472,13473,13474,11042,11043,11044,11045,11046,11047,11048,11049,11050,11051,11052,11053,11054,11055,11056,11057,11058,11059,11060,11061,11063,11064,11065,11066,11067,11068,11069,11070,11071,11072,11073,11074,11075,11076,11077,11078,12439,13371,13372,13373,13374,13375,13376,13377,13378,13379,13380,13381,13382,964,965,968,969,971,972,974,975,978,980,982,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,1007,1009,1015,1010,1016,1011,1013,1014,1018,1019,1020,1021,1022,1023,1024,1025,1026,1028,1029,1030,1032,1033,1035,1036,1038,1039,1041,1043,1044,1045)'
```

After moving the sql dump files to the Sport Gateway server, they were applied to the database there.
```
mysql -usportgw -p -D erp < ~/amer_actors.sql 
mysql -usportgw -p -D erp < ~/amer_brands.sql 
mysql -usportgw -p -D erp < ~/amer_product_frames.sql 
mysql -usportgw -p -D erp < ~/amer_products.sql 
mysql -usportgw -p -D erp < ~/amer_product_images.sql 
```

### Adding relations to the products

Adding a category for each of the brands  
```
INSERT INTO product_link_product_category (product_frame_id, product_category_id, created_at) SELECT pf.id, 4, NOW() FROM product_frame pf WHERE pf.brand_id = 828;
INSERT INTO product_link_product_category (product_frame_id, product_category_id, created_at) SELECT pf.id, 5, NOW() FROM product_frame pf WHERE pf.brand_id = 869;
INSERT INTO product_link_product_category (product_frame_id, product_category_id, created_at) SELECT pf.id, 6, NOW() FROM product_frame pf WHERE pf.brand_id = 870;
INSERT INTO product_link_product_category (product_frame_id, product_category_id, created_at) SELECT pf.id, 7, NOW() FROM product_frame pf WHERE pf.brand_id = 871;
INSERT INTO product_link_product_category (product_frame_id, product_category_id, created_at) SELECT pf.id, 8, NOW() FROM product_frame pf WHERE pf.brand_id = 872;
```