### 15.8.2017 Tuesday (Marko Teiste)

* updated the /usr/local/src/devops to include the salfetch functionality
* updated the crontab for backup user to run the salfetch every night
* created /var/lib/sal as the working directory for sal related tasks

### 31.5.2017 Wednesday (Marko Teiste)

* installed python3.6 from source to /usr/local

### 21.2.2017 Tuesday (Marko Teiste)

* after upping the disk volume, moved the /home/epharma to /srv/epharma to make room on the system root disk
* for one reason or an other the cron was not able to run the dabase backup, log show it started but because there is no MTA all the output was rejected even though there was logger in the job itself... boring, installed MTA on the system (exim4) to see if I can get more information out of the failure that way
  * urgh the issue was the logger does not create the file so the job failed because logger could not create the log file... wonderfull, quick fix is to have the file created manually first
* changed system timezone to Europe/Helsinki
* changed the database log file, tested and scheduled db to happen bi-weekly

### 20.2.2017 Monday (Marko Teiste)

* created s3 bucket for the backups (tgw-devops-backups)
* created a policy for allowing access to that s3 bucket (access-S3-tgw-devops-backups)
* created role DevOps with EC2 policy access-S3-tgw-devops-backups assigned to it
* created devops vpc 10.2.0.0/16 to ireland (vpc-d6dce4b2)
  * added three subnets to the vpc (10.2.1.0/24, 10.2.2.0/24, 10.2.3.0/24)
* peered devops and database vpcs (vpc-d6dce4b2,  vpc-8625e6c)
* created routes for the peering in both vpcs
* added internet gateway
  * attached the internet gateway to the devops network
* created backup server ec2 instance in the devops vpc with public ipv4 address, tagged as "backup server"
* connected to the instance
* upgraded instance
  * apt-get update && apt-get upgrade
  * rebooted
* tested aws access
* created user accounts for admins (mte, kimvais)
* installed git, added admins to staff group
* clone devops repo to /usr/local/src
* create backup script for backing up the databases
* added the backup script to cron (tested from interactive and seemed to work)
* created vpc pairing between the backup vpc and epharma vpc
  * created the pairing request and accepted it
* added the routes to both vpcs
* initially the instance was created accidentally with a too small a disk, so started increase volume task
* created a non privileged user epharma to act as target for production code sync from epharma.fi