# Updating production services REVISED

1. Connect to `ops.gwapi.eu` via SSH

2. In SSH: Go to `/srv/<username>/<project_name>`

3. In SSH: Make sure the latest *master* is checked out

4. In SSH: `pipenv shell`

5. In SSH: `pipenv update --sequential` or `pipenv install --sequential`

6. In SSH: Make sure you have appropriate pip version is installed by typing `pip install pip==9.0.3`

7. In SSH: Go to deployment that has `zappa_settings.json`

8. In SSH: `less zappa_settings.json` to find the relevant names. Relevant names would be `project_name` and `remote_env`.

9. Take backup of the snapshot based on the `project_name` under zappa_settings.json to local.

10. In local: `aws lambda list-functions | grep <zappa_project_name>`. Pay attention to the correct function name.

11. In local: `aws lambda get-function --function-name=<function_name_discovered_in_previous_step>`. Copy the URL under `Code` -> `Location`

12. Go to the URL discovered in previous step and download the backup.

13. Double check the `remote_env` used by the project is up-to-dated with appropriate data.

14. Back to SSH: `zappa update <my_target>` and hope for the best.

# Enabling CORS for api gateway

If you need to enable CORS and OPTIONS with Access-Control-Access-Headers you need to

- log into the amazon console
- find the api gateway 
- in resource both for / and for {proxy+} select Action->Enable CORS and set the value
- then you **must** redeploy (bin/deploy refresh dist-service) the one you modified.