# Development environments

## Front-end

1. Install node.js 8.9.4 (using chocolatey, macports or apt-get)
2. `npm install yarn`

## Back-end

I'm assuming that you already have Python 3.6.

# Database

1. Install MariaDB:
 - Linux: `sudo apt-get install mariadb-server`
 - MacOS: `sudo port install mariadb10.2`
 - Windows (in Powershell run as Administrator) `choco install mariadb`
2. Set-Up mariadb (just google it) and start the server.
3. Do the migration if needed - see [[how to migrate legacy database]]

# Legacy API backend

1. `git clone git@gitlab.com:thegateway/apiv2.git`
2. do the usual virtualenv stuff (I strongly encourage to use `pipenv` - even though the repository itself is not yet updated to use it)
3. set environment variable `FLASK_CONFIG=development`
4. edit `src.api.settings:FlaskDevelopmentConfiguration` to have correct MariaDB credentials
5. `python -m api` starts the webserver for the API
6. test that http://localhost:5000/shops gives you a JSON.

# Shopfront Web App

1. `git clone git@gitlab.com:thegateway/shopfront.git`
2. edit `site/js/banners.json` and point the `apiUrl` for development to `http://localhost:5000`
3. `make`
4. `node server.js` starts the webserver for the shopfront.

Shopfront should now be running at http://localhost:8000 and work correctly.

# Dashboard backend

1. `git clone git@gitlab.com:thegateway/dbv2.git`
  * At time of this writing, `git checkout kimvais_order_processing` is required.
2. do the usual virtualenv stuff, `Pipfile` is already in repo, just do `pipenv install -e` and `pipenv shell`
  * Before `pipenv install`, run `make package` on `apiv2` project and run `pipenv install <my_path_to_just_created_apiv2_dist_gwio_apiv2_package>` and then run `pipenv install`
3. set environment variables `COGNITO_POOL_ID`, `COGNITO_CLIENT_ID`, `SQLALCHEMY_DATABASE_URI` and `STRIPE_API_KEY` correctly (you can use the same values as in staging for the others than the database URI, see the values from the json files in s3 bucket `gwapi-configs`) 
  * At time of this writing, the values are located under `orders.dev.epharma.fi.conf`-file.
4. `python -m orders` start the webserver for the dashboard backend.
  * If you get any ImportErrors on `gwio` when doing `python -m orders`, manually go to `gwio` installed directory and prepend `gwio` to the imports and try again. Keep repeating the process until it starts working.

# Dashboard Web App

1. `git clone git@gitlab.com:thegateway/dashboard.git`
2. `yarn`
3. edit `src/environments/environment.ts` - point `apiUrl` to `http://localhost:5000` and `ordersApiUrl` to `http://localhost:8080`
3. `yarn start` starts the webserver for dashboard.
4. Dashboard should now be running at http://localhost:4200
  * After registering, request @kimvais to activate your account and use the `tools/set_actor.py` to set your userpool up for dashboard staging usage.