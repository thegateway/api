include vars.mk

NAME = gwio-api

ENV_SRC_MAKE=. $(REPO_ROOT)/.env; $(MAKE) -C $(REPO_ROOT)/backend/src/

all: clean build copy-to-host publish

.PHONY: docs pdf-docs env

docs:
	pipenv run doit docs

pdf-docs:
	pipenv run doit docs -t pdf

build:
	docker build . -t $(NAME)-layer


copy-to-host:
	docker create -ti --name temp_$(NAME) $(NAME)-layer bash
	docker cp temp_$(NAME):/python.zip .
	docker cp temp_$(NAME):/VERSION .
	docker rm -f temp_$(NAME)

publish:
	$(CP) python.zip $(NAME)-$(shell $(CAT) VERSION).zip
	python -m awscli lambda publish-layer-version --layer-name $(NAME) --description $(shell $(CAT) VERSION) --zip-file fileb://$(NAME)-$(shell $(CAT) VERSION).zip --compatible-runtimes python3.8 python3.7


clean:
	- docker rm -f temp_$(NAME)





init: .env


.env:
	echo "export PYTHONPATH=$(REPO_ROOT)/backend/src$(PYTHONPATH_SEP)$(REPO_ROOT)/backend/forks/dynamorm">$(REPO_ROOT)/.env

api-build: env
	$(ENV_SRC_MAKE) -f Makefile.api build

api-check: env
	$(ENV_SRC_MAKE) -f Makefile.api check

api-install: env
	$(ENV_SRC_MAKE) -f Makefile.api install

api-install-local: env
	$(ENV_SRC_MAKE) -f Makefile.api install-local

api-lint: env
	$(ENV_SRC_MAKE) -f Makefile.api lint


devenv-build: env
	$(ENV_SRC_MAKE) -f Makefile.devenv

devenv-check: env
	$(ENV_SRC_MAKE) -f Makefile.devenv check

devenv-install: env
	$(ENV_SRC_MAKE) -f Makefile.devenv install

devenv-install-local: env
	$(ENV_SRC_MAKE) -f Makefile.devenv install-local

devenv-lint: env
	$(ENV_SRC_MAKE) -f Makefile.devenv lint

devenv-publish: env
	$(ENV_SRC_MAKE) -f Makefile.devenv publish


dynamorm-install: env
	. $(REPO_ROOT)/.env; $(MAKE) -C $(REPO_ROOT)/backend/forks/dynamorm/ install

dynamorm-install-local: env
	. $(REPO_ROOT)/.env; $(MAKE) -C $(REPO_ROOT)/backend/forks/dynamorm/ install-local

integration-check-chrome: integration-install
	BROWSER_NAME=chrome $(INTEGRATION_TESTS)

integration-check-firefox: integration-install
	BROWSER_NAME=firefox $(INTEGRATION_TESTS)

integration-install:
	pip install gwio-devenv --extra-index-url=https://pypi.gwapi.eu

services-build-logs_to_es: env
	$(ENV_SRC_MAKE) -f Makefile.services build-logs_to_es

services-build-notification_to_email: env
	$(ENV_SRC_MAKE) -f Makefile.services build-notification_to_email

services-build-products_to_es: env
	$(ENV_SRC_MAKE) -f Makefile.services build-products_to_es

services-build-transactions_to_qldb: env
	$(ENV_SRC_MAKE) -f Makefile.services build-transactions_to_qldb

services-check: env
	$(ENV_SRC_MAKE) -f Makefile.services check

services-deploy: env
	$(ENV_SRC_MAKE) -f Makefile.services deploy

services-lint: env
	$(ENV_SRC_MAKE) -f Makefile.services lint

services-install: env
	$(ENV_SRC_MAKE) -f Makefile.services install


utils-build: env
	$(ENV_SRC_MAKE) -f Makefile.utils build

utils-check: env
	$(ENV_SRC_MAKE) -f Makefile.utils check

utils-install: env
	$(ENV_SRC_MAKE) -f Makefile.utils install

utils-install-local: env
	$(ENV_SRC_MAKE) -f Makefile.utils install-local

utils-lint: env
	$(ENV_SRC_MAKE) -f Makefile.utils lint

env: init
	. $(REPO_ROOT)/.env
