ifndef CI_PROJECT_DIR
	REPO_ROOT=$(shell git rev-parse --show-toplevel)
else
	REPO_ROOT=$(CI_PROJECT_DIR)
endif

ifndef CI_COMMIT_REF_NAME
	GIT_BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
else
	GIT_BRANCH=$(CI_COMMIT_REF_NAME)
endif

ifeq ($(GIT_BRANCH),master)
	APP_ENV = dev
else
	APP_ENV = $(GIT_BRANCH)
endif

DEPLOYMENT_VERTICAL=$(word 2,$(subst /, ,$(NAMESPACE)))
DEPLOYMENT_NAMESPACE=/$(APP_ENV)/$(DEPLOYMENT_VERTICAL)

ifeq ($(OS),Windows_NT)
	PYPATHSH = $(SHELL) . $(REPO_ROOT)/.env;
	PWSH = powershell -NoProfile
   	RM = $(PWSH) Remove-Item -recurse -EA Ignore -Path
	CP = $(PWSH) Copy-Item
	PIP = py -m pip
	ECRLOGINCMD = $(PWSH) Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process\; Import-Module AWSPowerShell\; Invoke-Expression -Command \(Get-ECRLoginCommand -Region eu-west-1\).Command
	MKDIR = $(PWSH) New-Item -ItemType directory
	CAT = $(PWSH) Get-Content
	PYTHONPATH_SEP = ;
else
	PYPATHSH = $(SHELL) . $(REPO_ROOT)/.env;
	PWSH = pwsh -NoProfile
	PIP = pip
   	RM = rm -rf
	CP = cp
	ECRLOGINCMD = $(shell $(aws ecr get-login --no-include-email --region eu-west-1))
	MKDIR = mkdir
	CAT = cat
	PYTHONPATH_SEP = :
endif

$(info APP_ENV=$(APP_ENV))
$(info DEPLOYMENT_VERTICAL=$(DEPLOYMENT_VERTICAL))

ECR = 430384725534.dkr.ecr.eu-west-1.amazonaws.com

INTEGRATION_TESTS = HEADLESS=True LOGIN_SETTINGS=session pytest tests/
