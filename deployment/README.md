# Deployment to lambda

## Deployment parts

### Secrets
There is information that needs to be in the lambda environment variables that 
can not be stored as plain text in the repository.

~~.env -> zappa_settings.json~~
1. ~~.env file in the same directory with the zappa_settings~~
1. ~~.env file is encrypted with public key corresponding to a private key~~
1. ~~encrypted .env file is committed to git~~
1. ~~during deployment the .env is decrypted and values passed to lambda(?)~~

Not good idea because we'd need to update all the projects when some shared env variable changes.

Possibly create a tool that generates the `zappa_settings.json`?

### API versions
The API does not have a version. When there are breaking changes or a feature changes,
a new endpoint is created with the old possibly marked deprecated if necessary.

The package version will be returned in the header so that the requestee can know which 
package version responded to the request.


### Database migration
#### New plan
  - Post records on save to AWS Event Bridge that triggers possible migration to the next version of the table.
  - Migration code is in a classmethod of the model that always accepts the output of 
  `flask.json.dumps(Model.Schema().dump(object))` 
  from the model representing the previous version of the table.
  
  
##### Migration classmethod functionality  
  1. `migration_dict = flask.json.loads()`
  2. Modify the dictionary to represent the table changes
  3. `Model.Schema().load(migration_dict).save()`

#### Old plan
Run automatically during the deployment process.

Pseudo code migration logic
```python
# BASE MIGRATION FILE
class Migrate:
    tables = {
        'users': (1, 2),
        'orders': (3, 4),
    }

    @classmethod
    def migrate(cls, migration_data):
        for new_table, old_items in migration_data.items():
            for item in old_items:
                item = do_migrate(item)
                new_table.save(item)

    @classmethod
    def old_table_exists(cls, table_name):
        ...

    @classmethod
    def new_table_exists(cls, table_name):
        ...


# MIGRATION FILE #1
class Migrate1(Migrate):
    tables = {
        'users': (1, 2),
        'orders': (3, 4),
    }

    @classmethod
    def do_migrate(cls, item):
        ...


# MIGRATION FILE #2
class Migratate2(Migrate):
    tables = {
        'users': (2, 3),
    }

    @classmethod
    def do_migrate(cls, item):
        ...


# MIGRATION FILE #3
class Migratate3(Migrate):
    tables = {
        'orders': (4, 5),
    }

    @classmethod
    def do_migrate(cls, item):
        ...


# MIGRATION SCRIPT FILE
def create_table(table_name, versions):
    ...


def run_migrations():
    migration_clses = get_migrations():
    while len(migration_clses):
        for migration_cls in migration_clses:
            if not all(migration_cls.old_table_exists(x) for x in migration_cls.tables):
                continue
            if not any(migration_cls.new_table_exists(x) for x in migration_cls.tables):
                migration_data = dict()
                for table_name, versions in migration_cls.tables.items():
                    old_table, new_table = create_table(table_name, versions)
                    migration_data[new_table] = old_table.scan()['Items']
                migration_cls.migrate(migration_data)
            migration_clses.remove(migration_cls)
            break
```

### Rest of it

#### Redirecting lambdas
- lambda that returns `503 Service Unavailable` for all calls

## Flow

1. redirect host to the `503` lambda
1. run the database migration
1. update the lambda
1. revert the host redirection
