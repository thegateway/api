# Cognito Pre Token Generation deployment

Pre Token Generation is a logic on AWS Cognito where Cognito ID Token claims can be modified before encoded for usage.
This project v2018 makes use of it through zappa deployment using the function found in `gwio.operations.authentications`.

## Updating

Due to zappa version at the time of this writing `zappa==0.46.2`, it does not handle pre token generation trigger update correctly.
You'll have to manually undeploy and deploy the trigger deployment again whenever there are any
changes related to the operation of pre token generation.

**HEADS UP:** updating this trigger lambda seems to break other lambdas probably by changing the permissions of the execution role
or policy that it shares with other lambdas. The rights to access cognito are removed and need to be readded. A quick fix is to add
the missing rights for `cognito-idp:*` and  `ses:SendMail`.

## Deploying

1. Make sure zappa_settings.json has the correct pool identified with appropriate triggers.

2. `zappa deploy <ENV>`

3. Go to AWS Cognito Console and select the newly deployed lambda function in the appropriate UserPool's triggers-page.

    3.1 When updating by doing `zappa undeploy` and `zappa deploy`,
    you'll have to manually select `none` trigger, save and then re-select the deployed lambda function.

4. Verify the pre token generation works by simply authing to the pool.