# -*- coding: utf-8 -*-
import logging
import subprocess
import sys

log = logging.getLogger(__name__)


def update(loc, name):
    subprocess.run(". \"`pipenv --venv`/bin/activate\" && cd '{loc}' && zappa update '{name}'".format(name=name, loc=loc),
                   shell=True, check=True)


def main(loc, stage):
    if sys.base_prefix != sys.prefix:
        log.fatal('Cannot be run inside virtual environment')
    update(loc, stage)


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
