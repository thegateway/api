ALTER TABLE actor_opening_hours_normal DROP created_at;
ALTER TABLE actor_opening_hours_normal DROP updated_at;
ALTER TABLE actor_opening_hours_normal MODIFY start_at TIME;
ALTER TABLE actor_opening_hours_normal MODIFY end_at TIME;
ALTER TABLE actor_opening_hours_normal ADD CONSTRAINT actor_opening_hours_normal_actor_id_day_pk UNIQUE (actor_id, day);
ALTER TABLE actor_opening_hours_exceptional MODIFY start_at TIME;
ALTER TABLE actor_opening_hours_exceptional MODIFY end_at TIME;
ALTER TABLE actor_opening_hours_exceptional DROP created_at;
ALTER TABLE actor_opening_hours_exceptional DROP updated_at;
ALTER TABLE actor_opening_hours_exceptional ADD CONSTRAINT actor_opening_hours_exceptional_actor_id_date_pk UNIQUE (actor_id, date)