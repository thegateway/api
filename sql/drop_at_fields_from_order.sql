ALTER TABLE `order`
  DROP ordered_at,
  DROP picked_up_at,
  DROP shipped_at,
  DROP pharmacy_shipped_at,
  DROP accepted_at,
  DROP customer_med_advice_given_at;
