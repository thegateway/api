ALTER TABLE `order`
	DROP COLUMN `smartpost_pup_code`,
	DROP COLUMN `smartpost_address`,
	DROP COLUMN `smartpost_postal_area`,
	DROP COLUMN `smartpost_postal_code`;
