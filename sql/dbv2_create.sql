DROP DATABASE IF EXISTS v2;
CREATE DATABASE v2;
USE v2;

-- new tables created as part of the entities/roles/identities

CREATE TABLE entities (
    id CHAR(32) NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    entity_type VARCHAR(45) NOT NULL,
    default_role VARCHAR(45)
);

CREATE TABLE roles (
    entity_id CHAR(32) NOT NULL REFERENCES entities(id),
    acting_entity_id CHAR(32) NOT NULL REFERENCES entities(id),
    role VARCHAR(45) NOT NULL,
    CONSTRAINT roles_pk PRIMARY KEY (entity_id, acting_entity_id, role)
);

CREATE TABLE identities (
    entity_id CHAR(32) NOT NULL REFERENCES entities(id),
    provider VARCHAR(128) NOT NULL,
    value VARCHAR(128) NOT NULL,
    CONSTRAINT identities_pk PRIMARY KEY (entity_id, provider, value)
);

-- Tables defined by the previous schema follow

-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-09-25 08:02:05.284

-- tables
-- Table: address
CREATE TABLE address (
    id char(32) NOT NULL,
    type varchar(8) NOT NULL,
    name varchar(128) NOT NULL,
    street varchar(128) NOT NULL,
    street2 varchar(128) NULL,
    postal_code varchar(8) NOT NULL,
    city varchar(32) NOT NULL,
    country char(2) NOT NULL,
    identity_id char(32) NOT NULL,
    CONSTRAINT address_pk PRIMARY KEY (id)
);

-- Table: contact
CREATE TABLE contact (
    id char(32) NOT NULL,
    notify BOOLEAN NOT NULL,
    type varchar(8) NOT NULL,
    value varchar(128) NOT NULL,
    identity_id varchar(32) NOT NULL,
    CONSTRAINT contact_pk PRIMARY KEY (id)
);

-- Table: customer
CREATE TABLE customer (
    id char(32) NOT NULL,
    identity_id char(32) NOT NULL,
    CONSTRAINT customer_pk PRIMARY KEY (id,identity_id)
);

-- Table: identity
CREATE TABLE identity (
    id char(32) NOT NULL,
    provider varchar(128) NOT NULL,
    name varchar(128) NOT NULL,
    CONSTRAINT identity_pk PRIMARY KEY (id)
);

-- Table: order
CREATE TABLE `order` (
    id char(32) NOT NULL,
    customer_id char(32) NOT NULL,
    reseller_id char(32) NOT NULL,
    delivery_address_id char(32),
    billing_address_id char(32),
    active bool NOT NULL,
    CONSTRAINT order_pk PRIMARY KEY (id)
);

CREATE INDEX order_active_idx ON `order` (active);

-- Table: order_event
CREATE TABLE order_event (
    id char(32) NOT NULL,
    timestamp timestamp NOT NULL,
    name varchar(64) NOT NULL,
    type varchar(16) NOT NULL,
    description text NOT NULL,
    order_id char(32) NOT NULL,
    CONSTRAINT order_event_pk PRIMARY KEY (id)
);

-- Table: order_line
CREATE TABLE order_line (
    id char(32) NOT NULL,
    order_id char(32) NOT NULL,
    type varchar(16) NOT NULL COMMENT 'product, discount, vat, delivery etc.
',
    target char(32),
    price decimal(9,4) NOT NULL,
    count decimal(9,4) NOT NULL,
    supplier_id char(32) NOT NULL,
    product_id char(32) NOT NULL,
    vat decimal(3,3) NULL,
    description TEXT,
    CONSTRAINT order_line_pk PRIMARY KEY (id)
);

-- Table: price
CREATE TABLE price (
    id char(32) NOT NULL,
    type varchar(8) NOT NULL,
    price decimal(9,4) NOT NULL,
    reseller_id char(32) NULL,
    customer_id char(32) NULL,
    product_id char(32) NOT NULL,
    CONSTRAINT price_pk PRIMARY KEY (id)
);

-- Table: product
CREATE TABLE product (
    id char(32) NOT NULL,
    source varchar(20) NOT NULL COMMENT 'table name of the extra product data
',
    ref_id char(32) NOT NULL,
    name varchar(128) NOT NULL,
    slug varchar(128) NOT NULL,
    vat decimal(4,2) NOT NULL,
    active bool NOT NULL,
    CONSTRAINT product_pk PRIMARY KEY (id)
);

CREATE INDEX product_active_idx ON product (active);

-- Table: product_tags
CREATE TABLE product_tags (
    product_id char(32) NOT NULL,
    tag_id char(32) NOT NULL
);

-- Table: reseller
CREATE TABLE reseller (
    id char(32) NOT NULL,
    identity_id char(32) NOT NULL,
    CONSTRAINT reseller_pk PRIMARY KEY (id,identity_id)
);

-- Table: supplier
CREATE TABLE supplier (
    id char(32) NOT NULL,
    identity_id char(32) NOT NULL,
    CONSTRAINT supplier_pk PRIMARY KEY (id,identity_id)
);

-- Table: tag
CREATE TABLE tag (
    id char(32) NOT NULL,
    type varchar(8) NOT NULL,
    slug varchar(64) NOT NULL,
    name varchar(256) NOT NULL,
    CONSTRAINT tag_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: customer_identity (table: customer)
ALTER TABLE customer ADD CONSTRAINT customer_identity FOREIGN KEY customer_identity (identity_id)
    REFERENCES identity (id);

-- Reference: address_id (table: address)
ALTER TABLE address ADD CONSTRAINT address_identity FOREIGN KEY address_identity (identity_id)
REFERENCES identity (id);

-- Reference: contact_id (table: contact)
ALTER TABLE contact ADD CONSTRAINT contact_identity FOREIGN KEY contact_identity (identity_id)
REFERENCES identity (id);

-- Reference: order_address (table: order)
ALTER TABLE `order` ADD CONSTRAINT order_delivery_address FOREIGN KEY order_delivery_address (delivery_address_id)
    REFERENCES address (id);

-- Reference: order_address (table: order)
ALTER TABLE `order` ADD CONSTRAINT order_billing_address FOREIGN KEY order_billing_address (billing_address_id)
    REFERENCES address (id);

-- Reference: order_customer (table: order)
ALTER TABLE `order` ADD CONSTRAINT order_customer FOREIGN KEY order_customer (customer_id)
    REFERENCES customer (id);

-- Reference: order_event_order (table: order_event)
ALTER TABLE order_event ADD CONSTRAINT order_event_order FOREIGN KEY order_event_order (order_id)
    REFERENCES `order` (id);

-- Reference: order_line_product (table: order_line)
ALTER TABLE order_line ADD CONSTRAINT order_line_product FOREIGN KEY order_line_product (product_id)
    REFERENCES product (id);

-- Reference: order_line_reseller (table: order_line)
ALTER TABLE order_line ADD CONSTRAINT order_line_supplier FOREIGN KEY order_line_supplier (supplier_id)
    REFERENCES supplier (id);

-- Reference: order_reseller (table: order)
ALTER TABLE `order` ADD CONSTRAINT order_reseller FOREIGN KEY order_reseller (reseller_id)
    REFERENCES reseller (id);

-- Reference: price_product (table: price)
ALTER TABLE price ADD CONSTRAINT price_product FOREIGN KEY price_product (product_id)
    REFERENCES product (id);

-- Reference: product_tags_product (table: product_tags)
ALTER TABLE product_tags ADD CONSTRAINT product_tags_product FOREIGN KEY product_tags_product (product_id)
    REFERENCES product (id);

-- Reference: product_tags_tag (table: product_tags)
ALTER TABLE product_tags ADD CONSTRAINT product_tags_tag FOREIGN KEY product_tags_tag (tag_id)
    REFERENCES tag (id);

-- Reference: reseller_identity (table: reseller)
ALTER TABLE reseller ADD CONSTRAINT reseller_identity FOREIGN KEY reseller_identity (identity_id)
    REFERENCES identity (id);

-- Reference: supplier_identity (table: supplier)
ALTER TABLE supplier ADD CONSTRAINT supplier_identity FOREIGN KEY supplier_identity (identity_id)
    REFERENCES identity (id);

-- End of file.

