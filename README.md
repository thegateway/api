# Core
## The Gateway IO core library

For now this is just holding the code moved from the apiv2 project (mainly models and src/utils)

### Start development

You need to have
- python 3.6
- pipenv

run

  pipenv install --python 3.6 --dev

This will create the environment you need for development. After this you can switch to the
development environment using

  pipenv shell

To get the interpreter path (for use with IDE like pycharm) you can run

  pipenv --py

to get the path to the virtualenv's python.

When you do updates to this package make sure you check the dependant projects for
possible issues and upgrade them accordingly.

# v2018
## New database structure

[![pipeline status](https://gitlab.com/thegateway/dbv2/badges/master/pipeline.svg)](https://gitlab.com/thegateway/dbv2/commits/master)
[![coverage report](https://gitlab.com/thegateway/dbv2/badges/master/coverage.svg)](https://gitlab.com/thegateway/dbv2/commits/master)

## Updating dynamodb model structures

1. Update the Meta `version`.
2. Create the new changed table using `tools/create_missing_tables.py`.
Remember to set approptiate `APPLICATION_ENVIRONMENT` and `GW_VERTICAL_NAME`.
3. Migrate the data from old table to new table. Separate migration tool script may be required to be done.
4. After changes have been merged to master. Verify that Step 2 and 3 is functioning and that data has been migrated.
5. On production side, create a backup of the old data.
6. Delete old table.

## Updating MapAttributes

If you add fields to MapAttribute all the new fields **MUST** have
`null=True`. If that is not possible then you need to upgrade the table version
and adhere to the process described in "updating dynamodb model"
