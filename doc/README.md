# The Reseller Gateway software development process

## Gitlab
> **If it's not in GitLab it does not exist.**
>     
>   - Old proverb from Pitäjänmäki

In practice, this means the following things

*  You should not be working on anything that does not have an GitLab issue in `Doing` column of the [group board](https://gitlab.com/groups/thegateway/-/boards])
*  Your code does not _exist_ unless it has a branch on GitLab and is pushed.

To extend this to actual rules


1. You MUST commit and push multiple times a day.
2. All branches SHOULD have a work-in-progress merge request with `WIP:`  
3. All issues that are being worked on, SHOULD have been estimated and have a weight set.

## Scrum

### Life cycle of a task

#### 1. Backlog
This is where all issues SHOULD be created. Anyone can create these and they have no formal requirements or structure. The only guideline is to put in all _relevant_ information you have at hand at the time you create the issue.

#### 2. TODO
These issues have been reviewed by in a joint meeting called _backlog grooming_ and deemed worth doing. During that meeting the issue MUST be updated to contain enough specification for the technical people to design the approach of implementation in case of features.

#### 3. Next
Issues that have priority, these MUST be weighed in the next sprint planning so that they can be worked on during the spirnt.

#### 4. Doing
Issues someone is currently working on. These SHOULD be assigned to a person and there SHOULD be only one issue in doing per developer.

#### 5. Done
Issues completed during the sprint. They will be closed after being demoed.

### Sprint planning
### Demo days
### Retrospecives

## Code style

All code MUST be PEP 8 / pycodestyle compliant and MUST pass pyflakes without warnings. 


All code SHOULD have pylint score of 10.0 with the .pylintrc found in repositories, the following exceptions or relaxations are made:

- Maximum line length is 129
- e, i, n, k and v are valid variable names when used as the industry standard practice is, as follows:
  - `except SomeExceptionSubclass as e:`
  - `for i, item in enumerate(collection)`
  - `n` as alias to _count of items being iterated_ when there is a single count
  - `for k, v in some_dictionary.items()`
- C0103 is disabled - so that `logger = logging.getLogger` does not need to be marked every time. Otherwise, all constants MUST be named conforming to PEP8.
- W0511 is disabled - TODO comments in code *are* fine.
- C0330 pylint and pycodestyle disagree on this but pycodestyle is right on this one.

### Naming

All names should follow PEP 8 convention. For databases, table names should be named like python variables and be pluralized. Columns are just like attributes in python - and therefore id columns should be named `guid`. Python standard library names SHOULD NOT be reused as variables, therefore `guid` over `id`. Also builtins SHOULD NOT be shadowed with the exception of database attribute named `type`, but even this should be defined as `type_ = Column(...., name='type')`

### Imports

Instead of 
```
from foo import bar, spam, eggs
```
 which is prone to causing merge conflicts, 

```
from foo import (
    bar,
    eggs,
    spam,
)
```
is preferred and therefore PyCharm setting at  
`Editor -> Code Style -> Python -> Wrapping and Braces`  
should be updated as
```
"From" Import Statements  
    Align when multiline              [x]  
    New line after '('                [x]  
    Place ')' on a new line           [x]  
    Force parantheses if multiline    [x]  
    Force trailing comma if multiline [x]  
```
