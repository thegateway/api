# Set $LAMBDA_FN_NAME and $NAMESPACE first!
$pandaslayer = Get-LMLayerVersionList -LayerName pandas-0_25_1 |Select-Object -First 1
$new_lambda_layer = Publish-LMLayerVersion -CompatibleRuntime "python3.7", "python3.8" -LayerName "gwio-api" -Content_ZipFile ([system.io.file]::ReadAllBytes("$HOME/python.zip"))
$lambda_function_env=(Get-LMFunctionConfiguration -FunctionName $LAMBDA_FN_NAME).Environment.Variables
$ssm_params = Get-SSMParametersByPath -Path $NAMESPACE -WithDecryption $true|Select-Object @{ l = "Key"; e = { $_.Name.Substring($NAMESPACE.Length + 1) } }, Value
$ssm_params|ForEach-Object { $lambda_function_env.($_.Key) = $_.Value }
Update-LMFunctionConfiguration -FunctionName $LAMBDA_FN_NAME -Environment_Variables $lambda_function_env -Layer $pandaslayer.LayerVersionArn, $new_lambda_layer.LayerVersionArn


