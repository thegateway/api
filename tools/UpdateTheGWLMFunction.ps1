Import-Module AWS.Tools.Lambda
Import-Module AWS.Tools.SimpleSystemsManagement

Function Update-TheGwLMFunction
{
    <#
    .SYNOPSIS
    This Cmdlet is used to update Gateway core AWS Lambda functions

    .DESCRIPTION
    The Cmdlet will update the Lambda function environment variables based on values found in SSM, plus optionally
    change the function configuration to use either a new Gateway API lambda layer specified with -ApiLayerName and -LayerVersion or publish a new lambda layer
    from local Zip file specified with -LayerZipFile

    .PARAMETER FunctionName
    Specifies the Lambda function name - you can find this out by doing `(Get-LMFunctionList).FunctionName`

    .PARAMETER Namespace
    Specifies the Gateway NAMESPACE - e.g. /production/sma - this is used for SSM Parameter path prefix.

    .PARAMETER LayerZipFile
    Specifies the optional Zip file containing the new lambda layer to be published and used.

    .PARAMETER LayerVersion
    Specifies the layer version number that the function should be used. Optional and mutually exclusive with LayerZipFile

    .PARAMETER ApiLayerName
    Specifies the layer name that should be used for finding the layer version - defaults to "gwio-api"

    .INPUTS
    None.

    .OUTPUTS
    Output of the underlying Aws.Tools.Lambda Cmdlets

    .EXAMPLE
    PS > UpdateTheGWLMFunction -Namespace "/dev/sma" -FunctionName sma-master
    This will only update the environment

    .EXAMPLE
    PS > UpdateTheGWLMFunction -Namespace "/production/sma" -FunctionName sma-production -LayerVersion 42
    This will update the environment, and change the Lambda function to use layer version 42 of gwio-api

    .EXAMPLE
    PS > UpdateTheGWLMFunction -Namespace "/staging/sma" -FunctionName sma-staging -LayerZipFile C:\Users\gw\python.zip
    This will update the environment, publish a new Lambda layer version and switch the Lambda function to use the newly published layer.

#>
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [string]$Namespace,

        [Parameter(Mandatory = $true)]
        [string]$FunctionName,

        [Parameter(Mandatory = $false)]
        [Int32]$LayerVersion,

        [Parameter(Mandatory = $false)]
        [System.IO.FileInfo]$LayerZipFile,

        [Parameter(Mandatory = $false)]
        [string]$ApiLayerName = "gwio-api"

    )
    Begin {
        $pandaslayername = "pandas-0_25_1"
        $pandaslayer = Get-LMLayerVersionList -LayerName $pandaslayername|Select-Object -First 1
        $lambda_function_env = (Get-LMFunctionConfiguration -FunctionName $FunctionName).Environment.Variables
        $ssm_params = Get-SSMParametersByPath -Path $Namespace -WithDecryption $true|Select-Object @{
            l = "Key"; e = {
                $_.Name.Substring($Namespace.Length + 1)
            }
        }, Value
        $ssm_params|ForEach-Object {
            $lambda_function_env.($_.Key) = $_.Value
        }
    }
    Process {
        if ($LayerZipFile)
        {
            $api_layer = Publish-LMLayerVersion -CompatibleRuntime "python3.7", "python3.8" -LayerName $ApiLayerName -Content_ZipFile ([system.io.file]::ReadAllBytes($LayerZipFile))
        }
        elseif ($LayerVersion)
        {
            $api_layer = Get-LMLayerVersion -LayerName $ApiLayerName -VersionNumber $LayerVersion
        }
        else
        {
            throw "You must provide either -LayerZipFile or -LayerVersion"
        }
        Update-LMFunctionConfiguration -FunctionName $FunctionName -Environment_Variables $lambda_function_env -Layer $pandaslayer.LayerVersionArn, $api_layer.LayerVersionArn
    }
    End {
    }

}


