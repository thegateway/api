FROM lambci/lambda:build-python3.7
RUN mkdir /python
ADD ./dist/ /dist/
RUN pip install --target /python --find-links /dist/ --extra-index-url=https://pypi.gwapi.eu gwio-api
WORKDIR /python
RUN python -c "import pkg_resources; print(pkg_resources.get_distribution('gwio-api').version)" > /VERSION
RUN find . -regex '^.*\(__pycache__\|\.py[co]\)$' -delete
WORKDIR /
RUN zip -r python.zip python

