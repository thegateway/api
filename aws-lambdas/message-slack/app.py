# Taken from https://gist.github.com/hayd/234c3097f607a32f217178322bdf4e75
import json
import logging
from base64 import b64decode
from urllib.error import (
    HTTPError,
    URLError,
)
from urllib.request import (
    Request,
    urlopen,
)

import boto3
from chalice import Chalice

# value of the CiphertextBlob key in output of $ aws kms encrypt --key-id alias/<KMS key name> --plaintext "<SLACK_HOOK_URL>"
ENCRYPTED_HOOK_URL = "AQICAHiL55BN/o5WXDlTiXH8tkW9mrHcIdQ51Q+3q4MnBjpmtAG4z3JRuuw5J92wfB5a9tfMAAAApzCBpAYJKoZIhvcNAQcGoIGWMIGTAgEAMIGNBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDGMoeq4Cq+pTOQIalwIBEIBgYRJJCbFvgAOY5Gb8zbR6oL3QZPt6XRvfEGbuokOLylsRho+UtlBXXc2XdT+U3y4HmzIGFjDmoNFVuf7jBBSRcPBhesNDUBY/YaF4RZtO3+xoz9UfBoVX60LJ/th4ETlE"

HOOK_URL = "https://" + boto3.client('kms').decrypt(CiphertextBlob=b64decode(ENCRYPTED_HOOK_URL))['Plaintext'].decode(
    'utf-8')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

app = Chalice(app_name='message-slack')

app.log.setLevel(logging.DEBUG)


@app.lambda_function()
def message_slack(event, context):
    logger.info("Event: " + str(event))

    sns_msg = event['Records'][0]['Sns']
    message = f"{sns_msg['Subject']}\n\n{sns_msg['Message']}"

    env_to_channel_postfix = dict(
        dev='dev',
        development='dev',
        staging='staging',
        demo='staging',
        production='production',
    )

    channel = '#devops'
    app_env = sns_msg["MessageAttributes"]['app_env']['Value']
    try:
        channel = f"{channel}_{env_to_channel_postfix[app_env]}"
    except KeyError:
        message = f"Unknown app env `{app_env}`\n{message}"

    slack_message = dict(channel=channel, text=message, icon_emoji=':bomb:')
    try:
        slack_message['username'] = sns_msg["MessageAttributes"]['sender']['Value']
    except KeyError:
        logger.fatal(sns_msg)

    req = Request(HOOK_URL, json.dumps(slack_message).encode('utf-8'))

    try:
        logger.info("Sending message")
        response = urlopen(req)
        response.read()
        logger.info("Message posted to %s", slack_message['channel'])
    except HTTPError as e:
        logger.error("Request failed: %d %s", e.code, e.reason)
    except URLError as e:
        logger.error("Server connection failed: %s", e.reason)
