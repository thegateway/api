import envs
from gwio.devtools.utils import make_calver
from setuptools import (
    find_packages,
    setup,
)

__VERSION__ = make_calver(envs.env('CI_PROJECT_DIR', '../..'))

install_requires = [
    'boto3',
    'chalice',
]

setup(
    name='message-slack',
    version=__VERSION__,
    packages=find_packages(exclude=['tests', 'tests.*']),
    url='https://www.gwapi.eu/',
    license='',
    install_requires=install_requires,
    author='The Reseller Gateway Oy',
    author_email='dev@the.gw',
    description=''
)
