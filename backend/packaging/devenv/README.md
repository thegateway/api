# Standard development environment

Typically you use by defining the package as dev requirement in the Pipfile

# Developing in a single repo

1. clone the repo
1. run ```pipenv install --dev --sequential```
1. start working
1. the doit has the expected targets (check, lint, clean, ...), there is also
   a makefile that can be used

# Developing on multiple repositories at the same time.

Occationally you have the situation that you need to make changes to multiple
repositories at the same time and those repositories have "cross dependencies".

For example you are working on your app that will also require changes in the
gwio-core package or something similar. You can easily create a "crossrepo"
development environment manually (without using the pipenv and assuming each
repo has a proper setup.py)

*NOTE* if the repo you are working on has locked gwio-core dependency
(gwio-core==20XX.M.ZZZZZ) you should remove the lock-in as not to have
the pip install -e . complain or fail (as the -e . install of gwio-core
will have version that does not match (due to how cal_version() works).

1. create a common base directory
1. clone the repositories you are working on under this base directory
   ```
   git clone git@gitlab:thegateway/devenv
   git clone git@gitlab:thegateway/core
   git clone git@gitlab:thegateway/other-repo-you-are-working-on
   ```
1. create virtual environment in the base directory
   ```
   python3.7 -m venv .env
   ```
1. activate the virtual environment
   ```
   source .env/bin/activate
   ```
1. install the working repos using pip in each of the repos
   ```
   cd devenv
   pip install -e .
   cd ../core
   pip install -e .
   cd ../other-repo-you-are-working-on
   pip install -e .
   cd ..
   ```

*NOTE!* that you cannot use the make <target> in this environment as most of
the makefiles assume you have environment setup using pipenv. You should be
able to run the doit <task> command in each of the repos to achieve the same
goal.
