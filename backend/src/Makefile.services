include vars-services.mk

INSTALL_SERVICE = export NAMESPACE=/$(APP_ENV)/GITLAB_CI; pip install chalice; pip install --find-links $(REPO_ROOT)/dist/ --target $(REPO_ROOT)/backend/src/gwio/services
CHALICE_DEPLOY = chalice deploy --stage $(GIT_BRANCH)
MODULE_DIRS=$(foreach service, $(GWIO_SERVICES), $(REPO_ROOT)/backend/src/gwio/services/$(service))
CFG_PATH=$(REPO_ROOT)/backend/src/setup-services.cfg

SERVICES_PATH = $(REPO_ROOT)/backend/src/gwio/services

.PHONY: build

all: build check lint

build-logs_to_es:
		export REPO_ROOT="$(REPO_ROOT)"; $(PYPATHSH) python $(REPO_ROOT)/backend/src/setup-services-logs_to_es.py bdist_wheel --dist-dir $(REPO_ROOT)/dist/

build-notification_to_email:
		export REPO_ROOT="$(REPO_ROOT)"; $(PYPATHSH) python $(REPO_ROOT)/backend/src/setup-services-notification_to_email.py bdist_wheel --dist-dir $(REPO_ROOT)/dist/

build-products_to_es:
		export REPO_ROOT="$(REPO_ROOT)"; $(PYPATHSH) python $(REPO_ROOT)/backend/src/setup-services-products_to_es.py bdist_wheel --dist-dir $(REPO_ROOT)/dist/

build-transactions_to_qldb:
		export REPO_ROOT="$(REPO_ROOT)"; $(PYPATHSH) python $(REPO_ROOT)/backend/src/setup-services-transactions_to_qldb.py bdist_wheel --dist-dir $(REPO_ROOT)/dist/


deploy: deploy-logs_to_es deploy-notification_to_email deploy-products_to_es deploy-transactions_to_qldb

deploy-logs_to_es:
		$(INSTALL_SERVICE)/logs_to_es/vendor/ gwio-logs_to_es; cd $(SERVICES_PATH)/logs_to_es/; $(CHALICE_DEPLOY)
		$(PWSH) -Command "Import-Module /build/UpdateTheGWLMFunctionEnv.ps1; Update-TheGwLMFunctionEnv -Namespace $(DEPLOYMENT_NAMESPACE) -FunctionName logs-to-es-$(GIT_BRANCH)-handle_event"

deploy-notification_to_email:
		$(INSTALL_SERVICE)/notification_to_email/vendor/ gwio-email-notifications; cd $(SERVICES_PATH)/notification_to_email/; $(CHALICE_DEPLOY)
		$(PWSH) -Command "Import-Module /build/UpdateTheGWLMFunctionEnv.ps1; Update-TheGwLMFunctionEnv -Namespace $(DEPLOYMENT_NAMESPACE) -FunctionName notification_via_email-$(GIT_BRANCH)-handle_event"

deploy-products_to_es:
		$(INSTALL_SERVICE)/products_to_es/vendor/ gwio-products_to_es; cd $(SERVICES_PATH)/products_to_es/; $(CHALICE_DEPLOY)
		$(PWSH) -Command "Import-Module /build/UpdateTheGWLMFunctionEnv.ps1; Update-TheGwLMFunctionEnv -Namespace $(DEPLOYMENT_NAMESPACE) -FunctionName product-to-es-$(GIT_BRANCH)-handle_event"

deploy-transactions_to_qldb:
		$(INSTALL_SERVICE)/transactions_to_qldb/vendor/ gwio-transactions_to_qldb; cd $(SERVICES_PATH)/transactions_to_qldb/; $(CHALICE_DEPLOY)
		$(PWSH) -Command "Import-Module /build/UpdateTheGWLMFunctionEnv.ps1; Update-TheGwLMFunctionEnv -Namespace $(DEPLOYMENT_NAMESPACE) -FunctionName transactions_to_qldb-$(GIT_BRANCH)-handle_event"


check:
		pytest --verbose --cov-report=html $(foreach service, $(GWIO_SERVICES), --cov=gwio.services.$(service)) -c $(CFG_PATH)

lint:
		pycodestyle --config $(CFG_PATH) $(MODULE_DIRS)
		flake8 --config=$(CFG_PATH) $(MODULE_DIRS)
		pylint $(MODULE_DIRS) --rcfile $(CFG_PATH) --reports yes || pylint-exit $$?

install:
		pip install --find-links $(REPO_ROOT)/dist/ --extra-index-url=https://pypi.gwapi.eu gwio-logs_to_es
		pip install --find-links $(REPO_ROOT)/dist/ --extra-index-url=https://pypi.gwapi.eu gwio-email-notifications
		pip install --find-links $(REPO_ROOT)/dist/ --extra-index-url=https://pypi.gwapi.eu gwio-products_to_es
		pip install --find-links $(REPO_ROOT)/dist/ --extra-index-url=https://pypi.gwapi.eu gwio-transactions_to_qldb
