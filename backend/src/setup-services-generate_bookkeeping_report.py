import envs
from setuptools import (
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'chalice',
    'gwio-api',
    'gwio-utils',
    'pyqldb',
    'smart_open[aws]',
]

setup(
    name='gwio-generate_bookkeeping_report',
    version=__VERSION__,
    packages=['gwio', 'gwio.services', 'gwio.services.generate_bookkeeping_report'],
    url='https://www.gwapi.eu/',
    license='',
    install_requires=install_requires,
    author='The Reseller Gateway Oy',
    author_email='dev@the.gw',
    description=''
)
