import envs
from setuptools import (
    find_namespace_packages,
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'awscli',
    'docutils<0.15,>=0.12',  # Version conflict with Sphinx
    'GitPython',
    'Sphinx',
    'autopep8',
    'boto',
    'boto3',
    'botocore',
    'coverage',
    'doit',
    'envs',
    'faker',
    'fakeredis',
    'flake8>3.5.0',
    'gwio-pylint-exit',
    'ipython',
    'mkdocs',
    'moto',
    'pep8',
    'pluggy',
    'pycodestyle',
    'pyflakes',
    'pylint>2.0',
    'pytest',
    'pytest-coverage',
    'pytest-env',
    'pytest-lazy-fixture',
    'pytest-mock',
    'python-jose[cryptography]',
    'recommonmark',
    's3pypi',
    'safety',
]
packages = []
for module in envs.env('GWIO_PACKAGES').split(' '):
    packages.append(module)
    packages.append(f'{module}.*')
setup(
    name='gwio-devenv',
    version=__VERSION__,
    description='The Reseller Gateway Standard Development Environment',
    url='https://www.gwapi.eu/',
    author='The Reseller Gateway',
    author_email='devops@gwapi.eu',
    platforms=['POSIX'],
    packages=find_namespace_packages(include=packages),
    package_data={},
    include_package_data=False,
    install_requires=install_requires,
    setup_requires=[
        'GitPython',
        'envs',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'gw-dynamodbtool = gwio.devtools.dynamodb_utils:cli',
            'gw-route53tool = gwio.devtools.route53_tool:cli',
            'gw-publish-packages = gwio.devtools.utils.publish_wheel:main',
        ],
    }
)
