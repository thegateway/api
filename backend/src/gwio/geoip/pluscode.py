"""
Implementation of Open Location Code aka. Pluscode location encoding defined by Google.
"""
import re
from itertools import zip_longest

from gwio.geoip.utils import make_values

_ALPHABET = '23456789CFGHJMPQRVWX'
_VALUES = make_values(_ALPHABET)
_RE = re.compile(r'([23456789C][23456789CFGHJMPQRV][23456789CFGHJMPQRVWX]{6}\+[23456789CFGHJMPQRVWX]{2,3})')


def group_in_pairs(iterable, fillvalue=None):
    args = [iter(iterable)] * 2
    return zip_longest(*args, fillvalue=fillvalue)


def validate_and_normalize(codestr):
    try:
        pluscode = _RE.match(codestr).group(1)
    except AttributeError:
        raise ValueError(f'{codestr} does not match the regular expression {_RE}')
    if pluscode[-3] != '+':
        raise ValueError(f'{pluscode} is not a valid Open Location Code')
    return pluscode[:-3] + pluscode[-2:]


def encode(lat, lng):
    val = ''
    _lat = int((lat + 90) * 8000)
    _lng = int((lng + 180) * 8000)
    for _ in range(5):
        symbol = _lng % 20
        val = _ALPHABET[symbol] + val
        _lng //= 20
        symbol = _lat % 20
        val = _ALPHABET[symbol] + val
        _lat //= 20
    return '{}+{}'.format(val[:8], val[8:])


def _decode(pluscode):
    lat, lng = 0, 0
    error = 1 / 8000
    olc = validate_and_normalize(pluscode)
    # Truncate to 10 for now.
    for i, (_lng, _lat) in enumerate(group_in_pairs(olc[10::-1])):
        lat += 20 ** i * _VALUES[_lat]
        lng += 20 ** i * _VALUES[_lng]

    latitude = (lat / 8000) - 90
    longitude = (lng / 8000) - 180
    return (latitude, latitude + error), (longitude, longitude + error)


def decode(pluscode):
    lat, lng = _decode(pluscode)
    return sum(lat) / 2, sum(lng) / 2
