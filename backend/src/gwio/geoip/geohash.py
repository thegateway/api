"""
Utilities to convert latitude and longitude to geohash and vice versa.

Encode is a Python translation of the reference implementation that is in public domain.

Decode is simplified original Python code that implements the algorithm as described in Wikipedia.
"""

import logging

from gwio.geoip.utils import make_values

_ALPHABET = "0123456789bcdefghjkmnpqrstuvwxyz"
_VALUES = make_values(_ALPHABET)

logger = logging.getLogger(__name__)


def _encode(lat: float, lng: float, bits: int) -> int:
    """
    Converts coordinates to an integer that can be represented as a geohash.
    :param lat: latitude
    :param lng: longitude
    :param bits: number of significant bits
    :return:
    """
    min_lat, max_lat = -90, 90
    min_lng, max_lng = -180, 180
    result = 0
    for i in range(bits):
        if i % 2 == 0:
            mid = (min_lng + max_lng) / 2
            if lng < mid:
                result = result << 1
                max_lng = mid
            else:
                result = result << 1 | 1
                min_lng = mid
        else:
            mid = (min_lat + max_lat) / 2
            if lat < mid:
                result = result << 1
                max_lat = mid
            else:
                result = result << 1 | 1
                min_lat = mid
    return result


def encode(lat: float, lng: float, precision: int = 8) -> str:
    """
    Converts coordinates to a geohash
    :param lat: latitude
    :param lng: longitude
    :param precision: length of the geohash string
    :return:
    """
    bits = 5 * precision
    result = _encode(lat, lng, bits)
    bitstring = f'{result:0{bits}b}'
    ret = ''
    for i in range(0, len(bitstring), 5):
        number = int(bitstring[i:i + 5], 2)
        ret += _ALPHABET[number]
    return ret


def bracket(bounds: (float, float), high: bool) -> (float, float):
    """
    Takes the tuple `bounds`, bisects  it and returns the higher or lower bracket based on `high`
    """
    min_, max_ = bounds
    mid = (min_ + max_) / 2
    if high:
        return mid, max_
    else:
        return min_, mid


def decode(geohash: str) -> (float, float):
    """
    A simple helper function to get a single pair of lat, lng -  the midpoint of the bounding box defined by a geohash.
    :param geohash:
    :return: (latitude, longitude)
    """
    lat, lng = _decode(geohash)

    return sum(lat) / 2, sum(lng) / 2


def _decode(geohash: str) -> ((float, float), (float, float)):
    """
    Decodes a geohash into a pair of coordinates defining a bounding box.
    :return: a 2-tuple of 2-tuples defining (min_latitude, max_latitude), (min_longitude, max_longitude))
    """
    bitstring = ''
    lat = (-90, 90)
    lng = (-180, 180)
    for symbol in geohash:
        number = _VALUES[symbol]
        bitstring += f'{number:05b}'
    odd = bool(len(bitstring) % 2)
    for _bit in bitstring:
        bit = bool(int(_bit, 2))
        if odd:
            lat = bracket(lat, bit)
        else:
            lng = bracket(lng, bit)
        odd = not odd
    return lat, lng
