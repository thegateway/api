def make_values(alphabet):
    return {n: i for i, n in enumerate(alphabet)}


def check_charset(s, alphabet):
    chars = set(s)
    charset = set(alphabet)
    if not chars.issubset(charset):
        raise ValueError('"{}" contains invalid characters "{}"'.format(s, ''.join(chars - charset)))
    return True
