from functools import wraps
from typing import Union

CACHEABLE_HTTP_STATUS_CODES = {200, 203, 206, 300, 301, 410}  # Caching these makes sense by default.


def parse_age(age: Union[int, str]) -> int:
    """
    Parses short-hand time definition e.g. 5m, 8h, 3d, 2w into seconds.
    :param age:
    :return:
    """
    multipliers = dict(
        m=60,
        h=60 * 60,
        d=60 * 60 * 24,
        w=60 * 60 * 24 * 7
    )

    if isinstance(age, int):
        return age

    unit = age[-1].lower()
    return int(age[:-1]) * multipliers[unit]


def cached(age: Union[int, str], private: bool = False, allowed_extra_status_codes: set = None) -> callable:
    """
    A callable decorator for Flask views to add Cache-Control values.
    Currently supports only 'max-age', 'public', and 'private' attributes.

    usage examples:
       @cached(False) -> Sets no-cache, no-store and must-revalidate
       @cached('5m') -> caches for 5 minutes
       @cached(300) -> caches also for 5 minutes
       @cached('8h', private=True) -> allows client-side caching for 8 hours but does not cache on CloudFront.
    """
    allowed_extra_status_codes = allowed_extra_status_codes if allowed_extra_status_codes is not None else set()

    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            response = f(*args, **kwargs)
            if age and response.status_code in CACHEABLE_HTTP_STATUS_CODES | allowed_extra_status_codes:
                response.cache_control.max_age = parse_age(age)
                response.cache_control.private = private
                response.cache_control.public = not private
            else:
                response.cache_control.max_age = 0
                response.cache_control.no_cache = True
                response.cache_control.no_store = True
                response.cache_control.must_revalidate = True
            return response

        return wrapped

    return decorator
