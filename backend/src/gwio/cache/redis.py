import logging
from functools import wraps
from urllib.parse import parse_qsl

from flask import (
    request,
)

from gwio.environment import Env

logger = logging.getLogger(__name__)
TIMESCALES = dict(s=1, m=60, h=60 * 60, d=60 * 60 * 24)


class RedisBackedCache:
    disabled = False
    _redis = None

    def __init__(self, app):
        self.app = app
        self.prefix = Env.NAMESPACE
        host = Env.REDIS_HOST
        if not host:
            logger.error("REDIS_HOST is not set, disabling cache!")
            self.disabled = True
        else:
            logger.info(f"Using redis cache at {host}")
            self._redis = Env.get_redis(Env.RedisDatabases.CACHE)

    def get(self, key):
        return self._redis.get(key)

    def set(self, key, response, expiry):
        logger.debug(f"Saving {len(response)} @ '{key}' for {expiry} seconds")
        return self._redis.set(key, response, ex=expiry)

    def invalidate(self, pattern):
        if self.disabled:
            logger.debug(f"Caching disabled. Not invalidating {pattern}")
            return
        r = self._redis
        pattern.rstrip('/')
        if not pattern.endswith('*'):
            pattern += '*'

        prefix = f'{self.prefix}{pattern}'

        num_invalidations = 0
        for key in r.scan_iter(prefix):
            r.delete(key)
            num_invalidations += 1
        logger.debug(f"Invalidated {num_invalidations} cached routes under `{prefix}`")

    def __call__(self, cache_for, extra_keys):
        suffix = '_'.join((str(x)) for x in extra_keys) if extra_keys is not None else ''
        expiry = self.parse_cache_for(cache_for)

        def decorator(f):
            if self.disabled:
                logger.info("Caching requested, but cache is disabled.")
                return f

            @wraps(f)
            def wrapper(*args, **kwargs):
                qs = self.normalize_qsl(request.query_string)
                key = self.prefix + request.path + qs + suffix
                logger.debug(f"Fetching {key} from cache")
                ret = self.get(key)
                if not ret:
                    ret = f(*args, **kwargs)
                    # This is post-request stuff
                    self.set(key, ret.data, expiry)
                    return ret
                else:
                    logger.debug(f"Serving {len(ret)} bytes of cached response, auditing disabled!")
                    # Never loaded the object so can't audit it either...
                    return ret.decode('utf-8'), {'Content-Type': 'application/json'}

            return wrapper

        return decorator

    def parse_cache_for(self, cache_for):
        if isinstance(cache_for, int):
            return cache_for
        else:
            lifetime = cache_for.lower()
            exp = int(lifetime[:-1])
            timescale = TIMESCALES[lifetime[-1]]
            return exp * timescale

    def normalize_qsl(self, query_string):
        if not query_string:
            return ''
        parts = ('='.join(x) for x in parse_qsl(query_string.decode('ascii')))
        return '?' + '&'.join(x for x in parts)
