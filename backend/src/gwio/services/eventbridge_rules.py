from gwio.environment import Env

detail = {
    'environment': [
        Env.APPLICATION_ENVIRONMENT,
    ],
}

LOGS_TO_ES = {
    'detail-type': [
        'audit_log_event',
        'log_event',
    ],
    'detail': detail,
}

NOTIFICATION_TO_EMAIL = {
    'detail-type': [
        'send_email',
    ],
    'detail': detail,
}

PRODUCTS_TO_ES = {
    'detail-type': [
        'product_save_event',
        'product_delete_event',
    ],
    'detail': detail,
}

TRANSACTION_TO_QLDB = {
    'detail-type': [
        'transactions',
    ],
    'detail': detail,
}
