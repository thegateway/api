import json
import logging
from urllib import parse

from attrdict import AttrDict
from chalice import Chalice

from gwio.services.eventbridge_rules import NOTIFICATION_TO_EMAIL as EB_RULE
import jinja2
from gwio.utils.aws import report_exception
from jinja2_s3loader import S3loader

app = Chalice("notification_to_email")

app.log.setLevel(logging.DEBUG)


@app.on_cw_event(EB_RULE)
def handle_event(event):
    try:
        app.log.debug(json.dumps(event.to_dict()))
        _handle(event)
    except Exception as e:
        from gwio.utils.aws import report_exception

        report_exception(e)
        raise


def get_jinja_environment(language):
    from gwio import environment
    template_url = '/'.join((environment.Env.MESSAGE_TEMPLATE_LOCATION.rstrip('/'), language.strip('/')))
    parsed_location = parse.urlparse(template_url)
    j2 = jinja2.Environment(loader=S3loader(parsed_location.netloc, parsed_location.path.lstrip('/')))
    return j2


def get_notification_templates(notification_type, env):
    jinja_html = env.get_template(f'{notification_type}.html.jinja2')
    jinja_text = env.get_template(f'{notification_type}.txt.jinja2')
    return dict(jinja_html=jinja_html, jinja_text=jinja_text)


def get_jinja_templates(notification_type, lang, en):
    get_template_params = [
        (notification_type, get_jinja_environment(lang)),
        (notification_type, get_jinja_environment(en)),
        ('simple-order-email', get_jinja_environment(lang)),
        ('simple-order-email', get_jinja_environment(en)),
    ]
    for params in get_template_params:
        try:
            return get_notification_templates(*params)
        except Exception as e:
            report_exception(e)
    raise FileNotFoundError('Missing the `simple-order-email` templates even in English so could not send email.')


def verify(detail):
    from gwio.security import jwt
    from gwio import environment
    service_env = environment.Env.APPLICATION_ENVIRONMENT
    if service_env != detail.environment:
        raise RuntimeError(f'Service is running in `{service_env}` environment, but received an event from `{detail.environment}` environment.')
    return jwt.verify(detail)


def _handle(event):
    import boto3

    detail = AttrDict(event.detail)
    payload = verify(detail)
    notification_type = payload['type']
    subject = payload['subject']
    src, dst = payload['src'], payload['dst']

    ses = boto3.client('ses')
    app.log.info(f"Sending email to {dst}")

    templates = get_jinja_templates(notification_type, payload['language'], 'en')

    ses.send_email(
        Source=src,
        Destination=dict(ToAddresses=[dst]),
        Message=dict(
            Subject=dict(Data=subject, Charset="UTF-8"),
            Body=dict(
                Html=dict(Charset="UTF-8", Data=render(templates['jinja_html'], payload)),
                Text=dict(Charset="UTF-8", Data=render(templates['jinja_text'], payload)),
            )
        )
    )


def render(template, payload):
    return template.render(payload['context'])
