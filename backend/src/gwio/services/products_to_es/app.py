import json
import logging
import os

from chalice import Chalice

from gwio.services.eventbridge_rules import PRODUCTS_TO_ES as EB_RULE

app = Chalice(app_name='services_product-to-es')
app.log.setLevel(logging.DEBUG)


@app.on_cw_event(EB_RULE)
def handle_event(event):
    try:
        app.log.debug(json.dumps(event.to_dict()))
        _handle(event)
    except Exception as e:
        from gwio.utils.aws import report_exception

        report_exception(e)
        raise


def _handle(event):
    """
    Add the product in the event to elasticsearch.

    :param event: Event from AWS EventBridge
    :param context: AWS context
    :return:
    """
    from attrdict import AttrDict
    from gwio.environment import Env

    detail = AttrDict(event.detail)

    service_env = Env.APPLICATION_ENVIRONMENT
    if service_env != detail.environment:
        raise RuntimeError(f'Service is running in `{service_env}` environment, but received an event from `{detail.environment}` environment.')

    import datetime
    import enum

    import backoff
    import boto3
    from elasticsearch import (
        Elasticsearch,
        TransportError,
        RequestError,
    )
    from pynamodb.attributes import (
        UnicodeAttribute,
        JSONAttribute,
        TTLAttribute,
        UTCDateTimeAttribute,
    )
    from pynamodb.models import Model
    from urllib3.util import parse_url

    MAX_RETRY_TIME = os.environ.get('MAX_RETRY_TIME', 900)  # seconds
    ERROR_SNS_TOPIC_ARN = Env.ERROR_SNS_TOPIC_ARN

    class EsProduct(Model):
        class Meta:
            region = 'eu-west-1'
            table_name = f'{Env.APPLICATION_ENVIRONMENT}-es_product'
            billing_mode = 'PAY_PER_REQUEST'

        index = UnicodeAttribute(hash_key=True)
        document_id = UnicodeAttribute(range_key=True)
        data = JSONAttribute(null=True)
        updated = UTCDateTimeAttribute()
        expires = TTLAttribute()

    # TODO: Copy-paste from `gwio-api`. Should be moved to utils and shared
    def message_devops(sender: str = None, subject: str = None, message: str = None, ignore_errors: bool = False) -> bool:
        if sender is None or subject is None or message is None:
            return False
        try:
            sns = boto3.client('sns')
            response = sns.publish(TopicArn=ERROR_SNS_TOPIC_ARN, Message=message, Subject=subject,
                                   MessageAttributes=dict(sender=dict(StringValue=sender, DataType="String")))
            if response['ResponseMetadata']['HTTPStatusCode'] != 200:
                if not ignore_errors:
                    app.log.fatal('Failed to report: {status}'.format(status=response['ResponseMetadata']['HTTPStatusCode']))
                return False
        # Ok, this should not raise an exception ever as it just makes things a bit too compilicated higher
        # up, if it fails it should not be an issue (e.g. this is supposed to never raise an exception) - mte
        # pylint: disable=W0703
        except Exception as e:
            if not ignore_errors:
                app.log.fatal(repr(e))
            return False
        return True

    class EsEvents(enum.Enum):
        PRODUCT_SAVE_EVENT = 'product_save_event'
        PRODUCT_DELETE_EVENT = 'product_delete_event'

    @backoff.on_exception(
        backoff.expo,
        TransportError,
        max_time=MAX_RETRY_TIME,
    )
    def update_es_document(es: Elasticsearch, index: str, document_id: str, product: dict):
        # Check the guid hasn't just been deleted
        try:
            updated = datetime.datetime.fromtimestamp(product['updated'], tz=datetime.timezone.utc)
        except TypeError:
            # The timestamp is an ISO time
            updated = datetime.datetime.fromisoformat(product['updated'])
        try:
            es_product = EsProduct.get(index, document_id)
            if es_product.updated > updated:
                app.log.info('Not updating product %s. existing product updated "%s" and new product updated "%s"',
                             document_id, es_product.updated, product['updated'])
                return
            es_product.expires = datetime.timedelta(days=1)
            es_product.updated = updated
        except EsProduct.DoesNotExist:
            es_product = EsProduct(index=index,
                                   document_id=document_id,
                                   data=product,
                                   updated=updated,
                                   expires=datetime.timedelta(days=1))
        es_product.save()
        try:
            es.index(index=index, id=document_id, body=product)
            app.log.info('Updated %s successfully', document_id)
        except RequestError as e:
            app.log.exception('Failed to update product %s with info:\n%s', document_id, product)
            raise RuntimeError from e

    @backoff.on_exception(
        backoff.expo,
        TransportError,
        max_time=MAX_RETRY_TIME,
    )
    def delete_es_document(es: Elasticsearch, index: str, document_id: str, updated: float):
        updated = datetime.datetime.fromtimestamp(updated, tz=datetime.timezone.utc)
        try:
            es_product = EsProduct.get(index, document_id)
            if es_product.updated > updated:
                app.log.info('Not updating product %s. existing product updated "%s" and new product updated "%s"',
                             document_id, es_product.updated, updated)
                return
        except EsProduct.DoesNotExist:
            es_product = EsProduct(index=index,
                                   document_id=document_id)
        es_product.expires = datetime.timedelta(days=1)
        es_product.updated = updated
        es_product.save()
        es.delete(index=index, id=document_id)
        app.log.debug('Deleted product _id %s from index %s successfully', document_id, index)

    environment = detail.environment
    if environment != Env.APPLICATION_ENVIRONMENT:
        err_msg = f'Not updating products documents for unexpected environment {environment}. Expected {Env.APPLICATION_ENVIRONMENT}. Exiting'
        lambda_name = os.environ.get('AWS_LAMBDA_FUNCTION_NAME', None)
        message_devops(sender=lambda_name, subject=f'Unhandled environment {environment}', message=err_msg)
        app.log.debug(err_msg)
        return

    parsed = parse_url(Env.ELASTICSEARCH_HOST)
    es = Elasticsearch(hosts=[
        dict(host=parsed.host, port=parsed.port if parsed.port else 443, use_ssl=True),
    ])
    product = json.loads(detail.product)
    try:
        if EsEvents(event.detail_type) is EsEvents.PRODUCT_SAVE_EVENT:
            update_es_document(es, detail.index, detail.id, product)
        elif EsEvents(event.detail_type) is EsEvents.PRODUCT_DELETE_EVENT:
            delete_es_document(es, detail.index, detail.id, product['updated'])
    except TransportError as e:
        err_msg = f'failed to index the product "{product["guid"]}" to elasticsearch for index "{detail.index}":\n{str(e)}'
        lambda_name = os.environ.get('AWS_LAMBDA_FUNCTION_NAME', None)
        message_devops(sender=lambda_name, subject=f'Unhandled exception {environment}', message=err_msg)
        app.log.exception(err_msg)
