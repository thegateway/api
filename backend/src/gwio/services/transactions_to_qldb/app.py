import json
import logging

from chalice import Chalice

from gwio.services.eventbridge_rules import TRANSACTION_TO_QLDB as EB_RULE

app = Chalice(app_name='transactions_to_qldb')

app.log.setLevel(logging.DEBUG)


@app.on_cw_event(EB_RULE)
def handle_event(event):
    try:
        app.log.debug(json.dumps(event.to_dict()))
        _handle(event)
    except Exception as e:
        from gwio.utils.aws import report_exception

        report_exception(e)
        raise


def _handle(event):
    """
    :param event: Event from AWS EventBridge
    :return:
    """
    from attrdict import AttrDict

    from gwio.environment import Env
    from gwio.security import jwt
    from gwio.utils import qldb

    detail = AttrDict(event.detail)

    service_env = Env.APPLICATION_ENVIRONMENT
    if service_env != detail.environment:
        raise RuntimeError(f'Service is running in `{service_env}` environment, but received an event from `{detail.environment}` environment.')

    payload = jwt.verify(detail)

    for subject_guid, transactions in payload.items():
        qldb.insert_transactions(
            table=subject_guid,
            transactions=transactions,
        )
