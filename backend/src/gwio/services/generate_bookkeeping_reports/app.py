import datetime
import json
import logging

import boto3
import pytz
import smart_open
from chalice import (
    Chalice,
    Cron,
)

from gwio.environment import Env
from gwio.models.webshops import Webshop
from gwio.tools.bookkeeping import generate_sales_reports
from gwio.utils.aws import (
    check_s3_file_size,
)
from gwio.utils.transactions import PLATFORM_LEDGER_TABLE

app = Chalice(app_name='generate_bookkeeping_report')

app.log.setLevel(logging.DEBUG)


@app.schedule(Cron(0, 3, 2, '*', '?', '*'))  # 3am on the 2nd day of each month
def handle_event(event):
    try:
        app.log.debug(json.dumps(event.to_dict()))
        _handle(event)
    except Exception as e:
        from gwio.utils.aws import report_exception

        report_exception(e)
        raise


def _generate_file(*, s3_client, subject_guid, year, month, filename, timezone):
    app.log.info(f"Generating the bookkeeping report for `{subject_guid}` for {year}-{month}")
    bucket = Env.DOMAIN
    key = f'bookkeeping/{subject_guid}/{filename}'
    url = f's3://{bucket}/{key}'
    file_size = check_s3_file_size(bucket, key)
    if file_size is not None and file_size != 0:
        app.log.warning(f"The bookkeeping report for `{subject_guid}` for {year}-{month} already exists, won't modify")
        return
    if file_size is None:
        s3_client.put_object(
            Bucket=bucket,
            Key=key,
        )

    with smart_open.open(url, 'wb') as fout:

        generate_sales_reports(
            file=fout,
            subject_guid=subject_guid,
            year=year,
            month=month,
            timezone=timezone,
        )


def _handle(event):
    """
    :param event: Event from AWS EventBridge
    :return:
    """
    app.log.info(json.dumps(event.to_dict()))

    dt = datetime.datetime.now(tz=pytz.UTC).replace(day=1) - datetime.timedelta(days=1)
    month = dt.month
    year = dt.year

    s3 = boto3.client('s3')
    filename = f'Bookkeeping report {year}-{month}.xlsx'
    timezone = pytz.timezone(Env.BOOKKEEPING_TIMEZONE)

    _generate_file(
        subject_guid=PLATFORM_LEDGER_TABLE,
        year=year,
        month=month,
        s3_client=s3,
        filename=filename,
        timezone=timezone,
    )
    for webshop in Webshop.scan_all():
        _generate_file(
            subject_guid=webshop.guid,
            year=year,
            month=month,
            s3_client=s3,
            filename=filename,
            timezone=timezone,
        )
