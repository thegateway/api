import json
import logging

from chalice import Chalice

from gwio.services.eventbridge_rules import LOGS_TO_ES as EB_RULE

app = Chalice(app_name='services_logs-to-es')

app.log.setLevel(logging.DEBUG)


@app.on_cw_event(EB_RULE)
def handle_event(event):
    try:
        app.log.debug(json.dumps(event.to_dict()))
        _handle(event)
    except Exception as e:
        from gwio.utils.aws import report_exception

        report_exception(e)
        raise


def _handle(event):
    """

    :param event: Event from AWS EventBridge
    :param context: AWS context
    :return:
    """
    import uuid

    from attrdict import AttrDict
    from elasticsearch import Elasticsearch
    from urllib3.util import parse_url

    from gwio import environment
    from gwio.security import jwt

    detail = AttrDict(event.detail)

    service_env = environment.Env.APPLICATION_ENVIRONMENT
    if service_env != detail.environment:
        raise RuntimeError(f'Service is running in `{service_env}` environment, but received an event from `{detail.environment}` environment.')

    parsed = parse_url(environment.Env.ELASTICSEARCH_HOST)
    es = Elasticsearch(hosts=[
        dict(host=parsed.host, port=parsed.port if parsed.port else 443, use_ssl=True),
    ])
    _ = es.index(index=f'{detail.vertical}_{detail.environment}_logs', id=uuid.uuid4(), body=jwt.verify(detail))
