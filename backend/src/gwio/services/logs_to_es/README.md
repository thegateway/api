#Deployment
Make sure that the packages defined in `vendor/requirements.txt` have been downloaded into the `vendor` directory 
before deploying as chalice does not support defining `extra-index-url` (https://github.com/aws/chalice/issues/502)

1. navigate to the chalice project's root directory on CLI
1. install requirements `pip install -r ./vendor-requirements.txt --extra-index-url=https://pypi.gwapi.eu -t ./vendor`
1. deploy `chalice deploy --stage master|staging|deployment`
1. cleanup `rm -r ./vendor/`
    - this is to make sure that the next time you run the deployment you won't have any old packages, 
    but will indeed fetch new ones and won't deploy any packages that are no longer needed either.
