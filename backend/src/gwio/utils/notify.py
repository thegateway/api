# -*- coding: utf-8 -*-
import datetime
import logging
import urllib
from abc import (
    ABC,
)
from enum import Enum
from typing import (
    Generator,
    List,
    Optional,
)
from urllib import parse
from urllib.parse import urlparse

import boto3
import jinja2
import pyfcm
import pywebpush
from attrdict import AttrDict
from botocore.exceptions import ClientError
from flask import (
    g,
    json,
    request,
)
from jinja2_s3loader import S3loader

from gwio.environment import (
    Env,
)
from gwio.ext.index_es_document import send_to_eventbridge
from gwio.security.jwt import sign
from gwio.utils import email

EMail = email.EMail

logger = logging.getLogger(__name__)


class NotificationFailException(Exception):
    pass


def send_webpush_notification(subscription, notification, *, lang):
    """
    Send a web push notification
    """
    from py_vapid import Vapid

    now = int(datetime.datetime.now().timestamp())
    exp = now + 3600
    data = dict(
        notification=dict(
            title=notification.title(lang),
            body=notification.simple_body(lang),
            icon=notification.icon,
            vibrate=[100, 50, 100],
            data=dict(
                dateOfArrival=now,
                primaryKey=1
            )))

    subscription_info = subscription['data']

    try:
        pywebpush.webpush(
            subscription_info,
            data=json.dumps(data),
            vapid_private_key=Vapid.from_string(private_key=Env.VAPID_PRIVATE_KEY),
            vapid_claims=dict(
                sub="mailto:{}".format(Env.SYSTEM_EMAIL),
                endpoint=subscription_info['endpoint'],
                exp=exp))
    except pywebpush.WebPushException as e:
        logger.debug('Webpush failed: %s / %s', e, e.response)
        raise NotificationFailException from e


def send_fcm_push_notification(subscription, notification, *, lang):
    fcm_push_service = pyfcm.FCMNotification(api_key=Env.FCM_NOTIFICATION_API_KEY)
    fcm_push_service.notify_single_device(
        registration_id=subscription['data']['registration_key'],
        message_title=notification.title(lang),
        message_body=notification.simple_body(lang),
        message_icon=notification.icon,
    )


def send_email_notification(subscription, notification, *, lang):
    send_email_event(
        dict(
            Time=datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0),
            Source=g.aws_event_source,
            Resources=[],
            DetailType='send_email',
            Detail=sign(json.dumps(dict(
                dst=subscription['data']['email_address'],
                src=notification.source,
                type=notification.type.value,
                subject=notification.email_text(lang),
                simple_body=notification.simple_body(lang),
                context=notification.ctx_dump,
                language=lang,
            ))),
        ),
    )


def send_email_event(event):
    send_to_eventbridge(
        event,
        elasticsearch=False,
    )


def _all_subclasses(cls):
    """Utility function to recursively find all subclasses for a class"""
    return set(cls.__subclasses__()).union(
        [s for c in cls.__subclasses__() for s in _all_subclasses(c)])


# Should be Python 3.7 dataclass
# pylint: disable=too-few-public-methods
class ShortMessage:
    def __init__(self, title, body, icon):
        self.title = title
        self.body = body
        self.icon = icon


# Should be Python 3.7 dataclass
# pylint: disable=too-few-public-methods
class EmailMessage:
    # pylint: disable=too-many-arguments
    def __init__(self, from_, to_, title, html, text):
        self.source = from_
        self.destination = to_
        self.title = title
        self.html = html
        self.text = text


class Notification(ABC):
    _jinja2_env = None
    _json = None
    _html = None
    _texts = dict()
    _simple_body = None

    template_base_name = None
    type = None
    targets = tuple()

    class Type(Enum):
        FCM_PUSH = 'fcm_push'
        WEBPUSH = 'webpush'
        EMAIL = 'email'
        # SMS = 'sms'  # XXX: Not implemented yet.

    def __str__(self):
        """ This is a very simple fallback text to be used when there is no template to render with / rendering the template fails """
        return self.__class__.__name__

    def __init__(self, context: dict):
        self.ctx = context

        assert self.source
        assert self.title()
        assert self.email_text()
        assert self.email_html()
        assert self.simple_body()

    def _render_template(self, path) -> str:
        return self.jinja2_env.get_template(f'{path}/{self.template_base_name}').render(self.ctx)

    @classmethod
    def get_texts_dict(cls, lang: str) -> dict:
        s3 = boto3.resource('s3')
        s3_path = '/'.join((Env.MESSAGE_TEMPLATE_LOCATION.rstrip('/'), lang.strip('/')))
        parsed_location = parse.urlparse(s3_path)
        # pylint: disable=no-member
        path = parsed_location.path.strip('/')
        obj = s3.Object(parsed_location.netloc, f'{path}/notifications.json')
        body = obj.get()['Body'].read()
        return json.loads(body)

    @classmethod
    def get_texts(cls, lang: str) -> dict:
        try:
            notifications_in_lang = cls._texts[lang]
        except KeyError:
            try:
                notifications_in_lang = cls.get_texts_dict(lang)
                cls._texts[lang] = notifications_in_lang
            except ClientError as ex:
                if ex.response['Error']['Code'] != 'NoSuchKey':
                    raise
                # The notifications file was not found in given language
                if lang != 'en':
                    logger.warning(f'Missing notification texts completely for `{lang}` language')
                    return cls.get_texts(lang='en')
                raise NotImplementedError('Missing notifications file in English. Could not send the notification!')

        try:
            return notifications_in_lang[cls.template_base_name]
        except KeyError:
            if lang != 'en':
                # falling back to english to try to get some notification out
                logger.warning(f'Missing notification texts in `{lang}` for `{cls.template_base_name}`')
                return cls.get_texts(lang='en')
            raise

    def get_text(self, text: str, lang: str) -> str:
        return self.get_texts(lang)[text].format(**AttrDict(self.ctx))

    @property
    def ctx_dump(self):
        dump = dict()
        for key in ('order', 'user'):
            try:
                dump[key] = self.ctx[key].Schema().dump(self.ctx[key])
            except KeyError:
                pass
        try:
            shops = []
            for webshop in self.ctx['shops']:
                shops.append(webshop.Schema().dump(webshop))
            dump['shops'] = shops
        except KeyError:
            pass

        return dump

    def email_text(self, lang: str = None) -> str:
        lang = lang if lang is not None else Env.DEFAULT_LANGUAGE
        return self.get_text('text', lang=lang)

    def email_html(self, lang: str = None) -> str:
        lang = lang if lang is not None else Env.DEFAULT_LANGUAGE
        link = urllib.parse.urljoin(request.host_url, self.get_text("link", lang=lang))
        # TODO: Use jinja for template
        return f'<html><body><center><img vertical_logo><a href="{link}"{self.get_text("link_text", lang=lang)}</a></center></body></html>'

    @property
    def icon(self) -> Optional[str]:
        return self.ctx.get('icon')

    @property
    def jinja2_env(self):
        if self._jinja2_env is None:
            url = urlparse(Env.EMAIL_TEMPLATE_LOCATION)
            self._jinja2_env = jinja2.Environment(loader=S3loader(url.netloc, url.path.strip('/')), trim_blocks=True, lstrip_blocks=True)
        return self._jinja2_env

    def simple_body(self, lang: str = None) -> str:
        lang = lang if lang is not None else Env.DEFAULT_LANGUAGE
        return self.get_text('text', lang=lang)

    @property
    def source(self) -> str:
        """ The from email address """
        return self.ctx['source']

    def title(self, lang: str = None) -> str:
        lang = lang if lang is not None else Env.DEFAULT_LANGUAGE
        return self.get_text('text', lang=lang)

    @classmethod
    def emit(cls, context: dict):
        """
        The public API for sending notifications.
        :param context:
        :return:
        """
        inst = cls(context)
        for target in inst.targets:
            target.notify(inst)

    @classmethod
    def from_type(cls, notification_type: Enum) -> Generator['Notification', None, None]:
        for subclass in _all_subclasses(cls):
            if subclass.type == notification_type:
                yield subclass

    def send(self, subscriptions: List[dict], lang: str = None):
        lang = lang if lang is not None else Env.DEFAULT_LANGUAGE
        fns = dict(
            webpush=send_webpush_notification,
            fcm_push=send_fcm_push_notification,
            email=send_email_notification,
        )
        for subscription in subscriptions:
            fns[subscription['type']](subscription, self, lang=lang)


class NotificationType(Enum):
    GENERIC = 'generic'


class UserNotificationType(Enum):
    REGISTRATION_THANKS = 'registration-thanks'


# pylint: disable=abstract-method
class UserNotification(Notification, ABC):

    def __init__(self, context):
        """Superclass for single user notifications to inherit from"""
        super().__init__(context)
        # TODO: Is the context['user'] really a user object or just the UUID?
        self.targets = [context['user']]


# pylint: disable=abstract-method
class ShopNotification(Notification, ABC):

    def __init__(self, context):
        """Superclass for single shop notifications to inherit from"""
        super().__init__(context)
        try:
            self.targets = context['shops']
        except KeyError:
            self.targets = [context['shop']]


# pylint: disable=abstract-method
class PaymentNotification(Notification, ABC):

    def __init__(self, context):
        """Superclass for single payment step notifications to inherit from"""
        super().__init__(context)
        self.targets = [context['payment']]


class GenericUserNotification(UserNotification):
    type = NotificationType.GENERIC

    def __str__(self):
        return repr(self)

    def title(self, *_):
        return self.ctx['title']

    def email_text(self, *_):
        return self.ctx['body']

    def email_html(self, *_):
        return self.ctx['body']

    def simple_body(self, *_):
        return self.ctx['body']


class FromShopMixIn:
    @property
    def source(self) -> str:
        """ The from email address """
        return Env.SYSTEM_EMAIL


class FromSystemMixin:
    @property
    def source(self):
        return Env.SYSTEM_EMAIL
