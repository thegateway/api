import time

import boto3
import datetime
import logging
import pytz
import re
import simplejson as json
from amazon.ion import simpleion as ion
from decimal import Decimal
from pyqldb.driver.pooled_qldb_driver import PooledQldbDriver

from gwio.environment import Env

logger = logging.getLogger(__name__)

TIMESTAMP_COLUMN = 'ledger_datetime'
TIMESTAMP_MULTIPLIER = 1000000  # Multiplier to convert python timestamp into microseconds


def convert_object_to_ion(object):
    """
    Convert a Python object into an Ion object.

    :type py_object: object
    :param py_object: The object to convert.

    :rtype: :py:class:`amazon.ion.simple_types.IonPyValue`
    :return: The converted Ion object.
    """

    return ion.loads(ion.dumps(object))


def get_session(ledger_name):
    driver = PooledQldbDriver(ledger_name=ledger_name)
    try:
        return driver.get_session()
    except Exception as e:
        if f'The Ledger with name {ledger_name} is not found' in str(e):
            logger.info(f"Ledger `{ledger_name}` doesn't exist, creating")
            _create_ledger(ledger_name)
            return driver.get_session()
        raise


def _create_ledger(ledger_name):
    qldb_client = boto3.client('qldb')
    logger.info(f'Creating ledger `{ledger_name}`')
    result = qldb_client.create_ledger(Name=ledger_name, PermissionsMode='ALLOW_ALL')
    logger.debug(json.dumps(result, default=str))
    logger.info('Waiting for the ledger to become active')
    while True:
        result = qldb_client.describe_ledger(Name=ledger_name)
        if result.get('State') == "ACTIVE":
            logger.info(f'Ledger `{ledger_name}` is now active')
            return result
        time.sleep(10)


def insert_transactions(*, table: str, transactions: list):
    ledger_name = f'{Env.APPLICATION_ENVIRONMENT}-{Env.VERTICAL}'
    session = get_session(ledger_name)

    # Time when the data was inserted to the ledger
    ledger_datetime = int(datetime.datetime.now(tz=pytz.UTC).timestamp() * TIMESTAMP_MULTIPLIER)
    for transaction in transactions:
        transaction[TIMESTAMP_COLUMN] = ledger_datetime
        transaction['amount'] = Decimal(transaction['amount'])
        transaction['vats'] = {k: Decimal(v) for k, v in transaction['vats'].items()}

    def _insert(executor, table: str, transactions: list):
        logger.info(f'Inserting transactions in the `{table}` table...')
        statement = f'INSERT INTO "{table}" ?'
        parameters_in_ion = convert_object_to_ion(transactions)
        executor.execute_statement(statement, parameters_in_ion)
        logger.info('Transactions inserted into the table')

    try:
        session.execute_lambda(
            lambda executor: _insert(executor, table, transactions),
            lambda _: logger.info('Retrying due to OCC conflict...'),
        )
    except Exception as e:
        if re.search(f'No such .*{table}', str(e)) is not None:
            _create_table(name=table, session=session)
            session.execute_lambda(
                lambda executor: _insert(executor, table, transactions),
                lambda _: logger.info('Retrying due to OCC conflict...'),
            )
        else:
            raise


def _create_table(*, name, session):
    def _create(transaction_executor):
        logger.info(f"Creating the `{name}` table...")
        transaction_executor.execute_statement(f'CREATE TABLE "{name}"')
        transaction_executor.execute_statement(f'CREATE INDEX ON "{name}"(order_guid)')
        transaction_executor.execute_statement(f'CREATE INDEX ON "{name}"(payment_guid)')

        logger.info(f'{name} table created successfully.')

    session.execute_lambda(
        _create,
        lambda _: logger.info('Retrying due to OCC conflict...')
    )
    time.sleep(1)  # Errors seem to happen when using the table immediately after creating
