import datetime
import uuid
from decimal import Decimal
from enum import Enum
from typing import Optional

import simplejson as json
from marshmallow import (
    Schema,
    fields,
)
from marshmallow_enum import EnumField

from gwio.environment import Env
from gwio.ext.index_es_document import send_to_eventbridge
from gwio.ext.vat import Currency
from gwio.models import payment_attribute
from gwio.security import jwt
from gwio.utils.aws import (
    lambda_function_name,
    message_devops,
)

PLATFORM_LEDGER_TABLE = uuid.UUID(int=0)


def verify_number(value):
    Decimal(value)


class TransactionDescription(Enum):
    CUSTOMER_PAYMENT = "Customer's payment"
    DELIVERY_FEE = "The delivery fee"
    PAYMENT_HANDLING_COMMISSION = "Commission for payment handling"
    PLATFORM_CUT = "Platform owner's cut"


class TransactionSchema(Schema):
    customer_name = fields.String()
    order_guid = fields.UUID()
    order_reference = fields.String()
    payment_guid = fields.UUID()
    amount = fields.Decimal(as_string=True)
    vats = fields.Dict(keys=fields.Integer(), values=fields.Decimal(as_string=True))
    currency = EnumField(enum=Currency)
    description = EnumField(enum=TransactionDescription, by_value=True)
    data = fields.Dict()


class Ledger:
    Description = TransactionDescription

    def __init__(self, *, source):
        self.source = source
        self.transactions = dict()

    def add_payment(
            self, *,
            payment: 'payment_attribute.Payment',
            amount: Decimal,
            vats: dict,
            currency: Currency,
            description: TransactionDescription,
            receiver=None,
            sender=None,
            platform_cut: Optional[Decimal] = None,
            **transaction_data
    ):
        """
        Adds transaction(s) for the given information.
        If only `receiver` or `sender` is provided, the money transaction is either in or out of our system, and we'll create only one transaction.
        If both `receiver` and `sender` are provided, the money is being moved inside our system and we'll create transactions for `receiver` and `sender`.

        NOTE! Should be called before calling asking 3rd party to capture/refund/pay out/etc. the money
        to reduce chance that there is an error when sending the transactions information to EventBridge

        :param amount:
        :param vats: A dictionary where the keys are the vat percentages and the values are the vat amounts.
        :param currency:
        :param receiver: The identifier for the entity receiving the money
        :param sender: The identifier for the entity sending the money
        :param platform_cut: The cut the platform owner gets for the shop using the platform.
                             This is just a way to reduce code duplication.
        :param transaction_data:
        """
        verify_number(amount)
        if sender is None and receiver is None:
            raise ValueError('Must provide not None value for `sender` and/or `receiver`')

        for vat_category, vat_amount in vats.items():
            verify_number(vat_category)
            verify_number(vat_amount)

        transaction = dict(
            description=description,
            order_guid=payment.order.guid,
            order_reference=payment.order.reference,
            customer_name=payment.order.customer.name,
            payment_guid=payment.guid,
            amount=amount,
            vats=vats,
            currency=currency,
        )
        transaction['data'] = transaction_data
        transaction_schema = TransactionSchema()

        if receiver is not None:
            serialised_receiver_transaction = transaction_schema.dump(transaction)
            try:
                self.transactions[str(receiver)].append(serialised_receiver_transaction)
            except KeyError:
                self.transactions[str(receiver)] = [serialised_receiver_transaction]

        if sender is not None:
            transaction['amount'] = -transaction['amount']
            transaction['vats'] = {k: -v for k, v in transaction['vats'].items()}
            serialised_sender_transaction = transaction_schema.dump(transaction)
            try:
                self.transactions[str(sender)].append(serialised_sender_transaction)
            except KeyError:
                self.transactions[str(sender)] = [serialised_sender_transaction]

        if platform_cut is not None:
            # The platform owner receiving cut from the seller
            self.add_payment(
                payment=payment,
                description=Ledger.Description.PLATFORM_CUT,
                amount=platform_cut,
                vats={Env.PLATFORM_COMMISSION_VAT_PERCENTAGE: platform_cut - platform_cut / (Decimal('1') + Env.PLATFORM_COMMISSION_VAT_PERCENTAGE)},
                currency=currency,
                receiver=PLATFORM_LEDGER_TABLE,
                sender=receiver,  # The shop is the receiver of the payment when we take the cut
            )

        # To make sure the that nothing breaks during dumping or signing. Emit might still break, but at least it's much less likely.
        # This is important because `add_payment` should be called before before making calls to 3rd parties and emit after.
        # If emit would break, it would mean we'll have for example received the money, but not updated the ledgers to match, which will throw bookkeeping off.
        self._cached_detail = self.event_detail

    @property
    def event_detail(self):
        return jwt.sign(json.dumps(self.transactions))

    def emit(self, **kwargs):
        """
        Sends an event to EventBridge, containing transactions information.
        NOTE! Should be called after 3rd party confirms the money has been captured/refunded/payed out/etc. as requested.
        :param kwargs: Can be used to update the transactions to include e.g. the payment identifier.
                       If this causes an issue, the transations information will be sent without this information.
        """

        detail = self._cached_detail

        if kwargs:
            try:
                for key, value in kwargs.items():
                    for transaction in self.transactions:
                        transaction[key] = value
                detail = self._cached_detail
            except Exception:
                # Not raising, because we'll use `detail` set earlier
                message_devops(
                    sender=lambda_function_name(),
                    subject='emit kwargs caused the transactions to not serialise',
                    message='will be retrying, but this should be fixed because it causes less data to be entered into the ledger',
                    ignore_errors=True,
                )

        send_to_eventbridge(
            dict(
                Time=datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0),
                Source=self.source,
                Resources=[],
                DetailType='transactions',
                Detail=detail,
            ),
            elasticsearch=False,
        )
