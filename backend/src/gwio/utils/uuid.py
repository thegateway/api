# -*- coding: utf-8 -*-
from uuid import UUID, uuid4, uuid5

from gwio.ext.decorators import static_vars

_UUID_SEED = UUID('da7aba53-104d-f457-5a17-feeddeadbeef')


@static_vars(base=None)
def get_namespace(name=None):
    """
    Return uuid5 namespace based on common "seed uuid"

    :param name: name for the new namespace (or default if none)
    :return: namespace UUID
    """
    if get_namespace.base is None:
        get_namespace.base = uuid5(_UUID_SEED, 'The Reseller Gateway')
    if name is None:
        return get_namespace.base
    return uuid5(get_namespace.base, str(name))


@static_vars(ns=None)
def uuid_from_imeds_id(eid):
    """
    Return uuid5 of an id in imeds namespace

    :param eid: id
    :return: UUID
    """
    if uuid_from_imeds_id.ns is None:
        uuid_from_imeds_id.ns = get_namespace('imeds')
    return get_uuid(str(eid), ns=uuid_from_imeds_id.ns)


@static_vars(default_ns=None)
def get_uuid(value, *, ns=None):
    """
    Generate UUID for given value in the namespace given

    :param value: value to to generate uuid for
    :param ns: UUID of the namespace
    :return: UUID
    """
    assert value is not None
    if get_uuid.default_ns is None:
        get_uuid.default_ns = get_namespace()

    if isinstance(value, bytes):
        value = value.decode('utf-8')

    if ns is None:
        return uuid5(get_uuid.default_ns, value)
    return uuid5(ns, value)


def get_random_uuid():
    """
    Generate random UUID

    :return: UUID
    """
    return uuid4()


def uuid_over_values(*values, ns=None, ignore_errors=False):
    try:
        assert ns is not None
        return uuid5(ns, '/'.join([str(value) if value else '' for value in values]))
    except Exception as e:
        if ignore_errors:
            return uuid4()
        raise e
