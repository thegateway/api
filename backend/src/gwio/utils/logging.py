# -*- coding: utf-8 -*-
"""
Patch standard python logging to send messages to devops channel

Note! zappa has already done the basicConfig() so we cannot just simply
use the standard logger configuration functions and are therefore forced
to add our own handler to root logger.
"""
import logging

from gwio.ext.decorators import static_vars
from gwio.utils.aws import message_devops, running_in_lambda, lambda_function_name


class DevopsHandler(logging.Handler):
    def __init__(self):
        super().__init__()

    def emit(self, record):
        # noinspection PyBroadException
        # pylint: disable=W0703
        try:
            message_devops(**self.format(record), ignore_errors=True)
        except Exception:  # this should never cause the application to fail
            pass


_SLACK_ICONS = {
    logging.CRITICAL: ":bomb:",
    logging.ERROR: ":bangbang:",
    logging.WARNING: ":warning:",
    logging.INFO: ":information_source:",
    logging.DEBUG: ":bug:"
}


def _iconic_slack_message(loglevel, txt):
    """Add loglevel appropriate icon to them message"""
    return _SLACK_ICONS.get(loglevel, ':slack:') + ' ' + txt


class DevopsFormatter(logging.Formatter):
    def __init__(self):
        super().__init__()

    def format(self, record):
        lmbda = lambda_function_name()
        msg = super().format(record)
        return dict(sender=f"{record.funcName}@{lmbda}", subject=record.name, message=_iconic_slack_message(record.levelno, msg))


@static_vars(done=False, handler=None)
def configure(devops_level=logging.WARNING, lambda_only=True):
    """Setup logger to also log to slack devops channel"""
    if configure.done:  # should be called only once
        return

    if lambda_only and not running_in_lambda():
        return

    if not logging.root:
        raise RuntimeError('logging has not been configured')

    if not configure.handler:
        configure.handler = DevopsHandler()
        configure.handler.setFormatter(DevopsFormatter())

    configure.handler.setLevel(devops_level)
    logging.root.addHandler(configure.handler)
    configure.done = True


def deactivate():
    """This is mainly used by testing to deactivate the devops logger after testing it"""
    if not configure.done:
        return

    logging.root.removeHandler(configure.handler)
    configure.done = False
