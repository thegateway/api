# -*- coding: utf-8 -*-
import sys
from urllib.error import URLError
from urllib.request import urlopen

from gwio.environment import Env as env

from gwio.ext.decorators import static_vars
from .aws import lambda_function_name


@static_vars(cached_result=None)
def running_in_aws():
    """Check if we are running in AWS or not

    There are two possibilities, this is running as lambda code or running inside ec2 instance. Other uses
    are not detected.
    """
    if running_in_aws.cached_result is None:
        if lambda_function_name() != env.UNKNOWN_LAMBDA_FUNCTION:
            running_in_aws.cached_result = True
        else:
            try:
                urlopen("http://169.254.169.254/latest/meta-data/", timeout=1)
                running_in_aws.cached_result = True
            except URLError:
                running_in_aws.cached_result = False
    return running_in_aws.cached_result


def running_in_windows():
    """Simple logic to check whether or not we're running on windows

    :return: True if environment is currently being run on Windows OS, otherwise False
    """
    return sys.platform.startswith('win32')
