# -*- coding: utf-8 -*-
import base64
import hashlib
from enum import Enum
from functools import total_ordering

# pylint: disable=comparison-with-callable
import stringcase


@total_ordering
class StringEnum(Enum):
    @staticmethod
    def _is_valid_operand(other):
        return isinstance(other, (str, StringEnum))

    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        if isinstance(other, StringEnum):
            return self.value == other.value
        return self.value == other

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        if isinstance(other, StringEnum):
            return self.value < other.value
        return self.value < other


def camel_to_snake(name):
    """
    Helper function to convert CamelCase to snake_case
    :param name:
    :return:
    """
    return stringcase.snakecase(name)


def get_consistent_hashname(s):
    return base64.b32encode(hashlib.sha256(s.encode('utf-8')).digest()).decode('ascii').rstrip('=').lower()
