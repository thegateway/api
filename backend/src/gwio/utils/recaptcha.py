import json

import requests

from gwio.environment import Env

RECAPTCHA_API_URL = "https://www.google.com/recaptcha/api/siteverify"


def validate(captcha_response, action):
    """ Validating recaptcha response from google server
        Returns True captcha test passed for submitted form else returns False.
    """
    secret = Env.RECAPTCHA_SITE_SECRET
    payload = {'response': captcha_response, 'secret': secret}
    response = requests.post(RECAPTCHA_API_URL, payload)
    response_text = json.loads(response.text)
    return response_text['success'] and response_text['action'] == action
