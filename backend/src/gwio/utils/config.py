# -*- coding: utf-8 -*-
"""
Simple utility to load the configuration from settings.py
without having to include flask or werzeug.
"""
import sys


# noinspection PyTypeChecker
def _import_string(name, silent=False):
    """Import module define in the string (nicked from werzeug)"""
    name = str(name).replace(':', '.')
    try:
        try:
            __import__(name)
        except ImportError:
            if '.' not in name:
                raise
        else:
            return sys.modules[name]

        module_name, obj_name = name.rsplit('.', 1)
        try:
            # noinspection PyShadowingNames
            module = __import__(module_name, None, None, [obj_name])
        except ImportError:
            # noinspection PyShadowingNames
            module = _import_string(module_name)

        try:
            return getattr(module, obj_name)
        except AttributeError as e:
            raise ImportError(e)

    except ImportError as e:
        if not silent:
            raise ImportError(e)


def import_python(objname):
    """
    Return python object matching given string represenation
    """
    return _import_string(objname.replace(':', '.'))


def get_config(config):
    """
    An alias for import_python for use when that makes more sense for the reader of the code
    """
    return import_python(config)
