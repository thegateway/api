def make_table_name(vertical, app, name, version=None):
    if version is not None:
        return f'{vertical}-{app}-{name}.{version}'
    return f'{vertical}-{app}-{name}'
