# -*- coding: utf-8 -*-
"""
AWS related utilities
"""
import json
import logging
import traceback
import uuid
from enum import Enum
from typing import List

import boto3
import envs
import requests
from jwt import (
    decode,
    get_unverified_header,
)
from jwt.algorithms import (
    HMACAlgorithm,
    RSAAlgorithm,
)
from werkzeug.exceptions import abort

from gwio.environment import Env

logger = logging.getLogger(__name__)


def lambda_function_name():
    """Get the lambda function name"""
    return Env.AWS_LAMBDA_FUNCTION_NAME


def running_in_lambda():
    """Is the code running inside aws lambda"""
    aws_lambda_vars = ('_HANDLER', 'AWS_REGION', 'AWS_EXECUTION_ENV', 'AWS_LAMBDA_FUNCTION_NAME')
    return all(envs.env(v) for v in aws_lambda_vars)


def get_sender():
    if Env.AWS_LAMBDA_FUNCTION_NAME:
        return Env.AWS_LAMBDA_FUNCTION_NAME
    else:
        import getpass
        import socket

        return f'{getpass.getuser()}@{socket.gethostname()}'


def message_devops(sender=None, subject=None, message=None, ignore_errors=False):
    if sender is None:
        sender = get_sender()
    if subject is None:
        subject = 'NO SUBJECT'
    if message is None:
        message = 'NO MESSAGE'

    try:
        sns = boto3.client('sns')
        response = sns.publish(
            TopicArn=Env.ERROR_SNS_TOPIC_ARN,
            Message=message,
            Subject=subject[:100],  # Max length for the subject
            MessageAttributes=dict(
                sender=dict(
                    StringValue=sender,
                    DataType="String",
                ),
                app_env=dict(
                    StringValue=Env.APPLICATION_ENVIRONMENT,
                    DataType="String",
                ),
            ),
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            if not ignore_errors:
                logger.fatal('Failed to report: {status}'.format(status=response['ResponseMetadata']['HTTPStatusCode']))
            return False
    # Ok, this should not raise an exception ever as it just makes things a bit too compilicated higher
    # up, if it fails it should not be an issue (e.g. this is supposed to never raise an exception) - mte
    # pylint: disable=W0703
    except Exception as e:
        if not ignore_errors:
            logger.fatal(repr(e))
        return False
    return True


def report_exception(e, *_):
    return message_devops(
        sender=lambda_function_name(),
        subject=str(e),
        message=traceback.format_exc(),
        ignore_errors=False,
    )


class JWTokenIssuer:
    jwks = dict()

    algorithms = {
        "RS256": RSAAlgorithm,
        "HS256": HMACAlgorithm
    }

    def get_key(self, kid):
        for key in self.jwks['keys']:
            if key['kid'] == kid:
                return self.algorithms[key['alg']].from_jwk(json.dumps(key))
        raise KeyError(kid)

    def parse_jwt(self, jwt):
        """Parses and verifies the JWT provided by Amazon."""
        try:
            headers = get_unverified_header(jwt=jwt)
            key = self.get_key(headers['kid'])
            return decode(jwt,
                          key=key,
                          algorithms=[headers['alg']],
                          options={'verify_aud': False})
        except (KeyError, IndexError) as e:
            logger.exception(e)
            raise LookupError from e

    verify_token = parse_jwt


class CognitoIdpClient(JWTokenIssuer):
    """Class that will act as a wrapper client for specific Cognito user pool."""

    # pylint: disable=too-few-public-methods
    class Group(Enum):
        ADMIN = 'admin'
        CUSTOMER = 'customer'
        PRODUCT_OWNER = 'product_owner'
        SHOPKEEPER = 'shopkeeper'
        SUPERVISOR = 'supervisor'

    def __init__(self, pool_id: str, client_id: str, region: str = None):
        """Initializes the CognitoIdpClient

        :param pool_id: ID of the target Cognito Pool.
        :param client_id: ID of the target Cognito Pool's client.
        :param region: Region where the target Cognito Pool is located.
        """
        assert client_id is not None and pool_id is not None

        self._pool_id = pool_id
        self._client_id = client_id
        if region is None:
            region = pool_id.split('_', 1)[0]  # Some regions (e.g. eu-north-1) don't have Cognito IDP service.
        self._region = region
        self.issuer_name = f'https://cognito-idp.{region}.amazonaws.com/{pool_id}'
        self.client = boto3.client('cognito-idp', region_name=region)
        self._jwks = None

    @property
    def client_id(self):
        return self._client_id

    @property
    def pool_id(self):
        return self._pool_id

    @property
    def region(self):
        return self._region

    @property
    def jwks(self):
        """Returns defined jwks. If none is defined, will attempt to get
        from what was defined as url for jwks.
        """
        if self._jwks is None:
            response = requests.get(self.issuer_name + '/.well-known/jwks.json')
            if response.status_code >= 400:
                abort(response.status_code)
            self._jwks = response.json()

        return self._jwks

    _whitelist = ['MinimumLength', 'RequireUppercase', 'RequireLowercase', 'RequireNumbers', 'RequireSymbols']
    _blacklist = ['TemporaryPasswordValidityDays']

    @property
    def password_policy(self):
        password_policy = self.client.describe_user_pool(UserPoolId=self._pool_id)['UserPool']['Policies']['PasswordPolicy']
        requirements = {k: v for k, v in password_policy.items() if k in self._whitelist}
        for key in self._whitelist + self._blacklist:
            try:
                password_policy.pop(key)
            except KeyError:
                logging.info('Expected key "%s" not found in password policy')
        # Check there are no unknown (new) data in the password policy
        if password_policy:
            logging.warning('Unknown keys in password policy: %s', ', '.join(password_policy))
        return requirements

    def _admin_initiate_auth(self, auth_flow, auth_params=None):
        """Initiate authentication with desired authentication flow."""
        if auth_params is None:
            auth_params = dict()
        return self.client.admin_initiate_auth(AuthFlow=auth_flow, AuthParameters=auth_params, ClientId=self.client_id,
                                               UserPoolId=self.pool_id)

    def add_user(self, username: str, password: str, user_attributes: dict = None) -> uuid.UUID:
        """Creates a new cognito user with pre-set password silently with the given information.
        Silent creation is generally for migration process as to not trigger notification
        to end-users.
        :param username: Username
        :param password: Password to be set with.
            Fun trivia: Can also be used as temporary password and when required to change password,
                        can still be the same.
        :param user_attributes: Additional attributes that we'd like to set with.
        :return: UUID of the user.
        """
        if user_attributes is None:
            user_attributes = dict()
        attrs = list()
        for key, value in user_attributes.items():
            attrs.append({
                'Name': key,
                'Value': value
            })
            if key in ('email', 'phone_number'):
                attrs.append({
                    'Name': '{}_verified'.format(key),
                    'Value': 'true'
                })
        create_user_resp = self.client.admin_create_user(UserPoolId=self.pool_id,
                                                         Username=username,
                                                         UserAttributes=attrs,
                                                         TemporaryPassword=password,
                                                         MessageAction='SUPPRESS')
        initiate_auth_resp = self.client.admin_initiate_auth(UserPoolId=self.pool_id,
                                                             ClientId=self.client_id,
                                                             AuthFlow='ADMIN_NO_SRP_AUTH',
                                                             AuthParameters={
                                                                 'USERNAME': username,
                                                                 'PASSWORD': password
                                                             })
        if initiate_auth_resp['ChallengeName'] != 'NEW_PASSWORD_REQUIRED':
            raise RuntimeError(f'Unexpected challenge name: [{initiate_auth_resp["ChallengeName"]}]')
        self.client.admin_respond_to_auth_challenge(
            UserPoolId=self.pool_id,
            ClientId=self.client_id,
            ChallengeName='NEW_PASSWORD_REQUIRED',
            ChallengeResponses={
                'USERNAME': initiate_auth_resp['ChallengeParameters']['USER_ID_FOR_SRP'],
                'NEW_PASSWORD': password
            },
            Session=initiate_auth_resp['Session']
        )
        return uuid.UUID(create_user_resp['User']['Username'])

    def auth_with_password(self, username, password):
        """Authenticates the user using ADMIN_NO_SRP_AUTH flow."""
        return self._admin_initiate_auth('ADMIN_NO_SRP_AUTH', {'USERNAME': username, 'PASSWORD': password})

    def change_temporary_password(self, user_id, name, proposed_password, session_id):
        return self.client.admin_respond_to_auth_challenge(UserPoolId=self.pool_id,
                                                           ClientId=self.client_id,
                                                           ChallengeName='NEW_PASSWORD_REQUIRED',
                                                           ChallengeResponses={'NEW_PASSWORD': proposed_password,
                                                                               'USERNAME': user_id,
                                                                               'userAttributes.name': name},
                                                           Session=session_id)

    def auth_with_refresh_token(self, refresh_token, device_key=None):
        """Authenticates the user using REFRESH_TOKEN_AUTH flow.
        Device Key would become mandatory if the user pool is configured to remember user's devices.
        """
        auth_params = {'REFRESH_TOKEN': refresh_token}
        if device_key is not None:
            auth_params['DEVICE_KEY'] = device_key
        return self._admin_initiate_auth('REFRESH_TOKEN_AUTH', auth_params)

    def confirm_forgot_password(self, username, confirmation_code, new_password):
        """Will finalize the password change with the end-user provided confirmation code."""
        return self.client.confirm_forgot_password(
            ClientId=self.client_id,
            Username=username,
            ConfirmationCode=confirmation_code,
            Password=new_password
        )

    def forgot_password(self, username):
        """Trigger the process of changing the password due to forgetfulness."""
        return self.client.forgot_password(
            ClientId=self.client_id,
            Username=username
        )

    def list_users(self, attr_filter: str = None, pagination_token: str = None) -> dict:
        """Returns a list of users.

        Search results are sorted by the attribute named by the AttributeName string in Filter, in ascending order.
        Custom attributes are not filterable.
        To only sort by the attribute we want, simply use 'AttributeName ^=""' as the filter-rule.
        :param attr_filter: Which attribute to filter with.
        :param pagination_token: If provided, the list continues where the previous was left off.
        :return:
        """
        params = dict(UserPoolId=self.pool_id)
        if attr_filter is not None:
            params['Filter'] = attr_filter
        if pagination_token is not None:
            params['PaginationToken'] = pagination_token
        return self.client.list_users(**params)

    def add_user_to_group(self, username: uuid.UUID, group: Group):
        self.client.admin_add_user_to_group(UserPoolId=self.pool_id,
                                            Username=str(username),
                                            GroupName=group.value)

    def list_groups_for_user(self, username: str) -> List[str]:
        groups_resp = self.client.admin_list_groups_for_user(UserPoolId=self.pool_id, Username=username)
        return [group['GroupName'] for group in groups_resp['Groups']]

    def set_custom_attribute(self, attribute_name: str, value: str, username: uuid.UUID) -> dict:
        """Updates the selected user's organization_guid."""
        response = self.client.admin_update_user_attributes(
            UserPoolId=self.pool_id,
            Username=str(username),
            UserAttributes=[
                {
                    'Name': f'custom:{attribute_name}',
                    'Value': value
                },
            ]
        )
        return response

    def delete_user(self, username: str) -> None:
        self.client.admin_delete_user(UserPoolId=self.pool_id, Username=username)


class CognitoFederatedIdentitiesClient():
    def __init__(self, pool_id: str):
        """
        :param pool_id: ID of the target Cognito Federated Identity pool.
        """

        self._pool_id = pool_id
        self._client = boto3.client('cognito-identity', region_name=self.region)

    @property
    def region(self):
        return self._pool_id.split(':', 1)[0]

    def get_id(self, *, logins: dict = None) -> str:
        """
        Create or get an existing id based on the given logins.
        :param logins: Login information in format `{iss: id_token}`
        :return: The user's `Identity ID`
        """
        logger.debug('Getting Identity ID on pool %s', self._pool_id)
        if logins is None:
            logins = dict()
        response = self._client.get_id(IdentityPoolId=self._pool_id, Logins=logins)
        return response['IdentityId']

    def link_identity_id_with_login(self, *, identity_id: str, iss: str = None, id_token: str = None):
        """
        Link identity id to a login method. One identity can be linked to multiple login methods.
        :param identity_id: Identity ID of the user
        :param iss: Issuer of the token, ie. `iss` field of the token
        :param id_token: The id token in its entirety
        :raises: ResourceNotFoundException, NotAuthorizedException
        """
        params = dict(IdentityId=identity_id)
        if iss is not None and id_token is not None:
            params['Logins'] = {iss.replace('https://', ''): id_token}
        self._client.get_open_id_token(**params)

    def describe_identity(self, identity_id: str) -> dict:
        """
        Get information saved in Cognito Federated Identities about the given `Identity ID`
        :param identity_id: Identity ID of the user
        :return: dict of the user data
        """
        resp = self._client.describe_identity(IdentityId=identity_id)
        resp.pop('ResponseMetadata')
        return resp


def check_s3_file_size(bucket, key):
    """return the key's size if it exist, else None"""
    s3 = boto3.client('s3')
    response = s3.list_objects_v2(
        Bucket=bucket,
        Prefix=key,
    )
    for obj in response.get('Contents', []):
        if obj['Key'] == key:
            return obj['Size']
    return None
