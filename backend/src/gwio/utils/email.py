# -*- coding: utf-8 -*-
"""
A wrapper class to provide a simplified interface for email sending.
Will additionally setup the jinja environment.
"""
import logging
import warnings
from typing import (
    Any,
    Dict,
    Union,
)
from urllib.parse import urlparse
from urllib.request import url2pathname

import boto3
import jinja2
from botocore.exceptions import ClientError
from gwio.environment import Env as env
from jinja2.exceptions import TemplateNotFound
from jinja2_s3loader import S3loader

from gwio.ext.decorators import singleton

logger = logging.getLogger(__name__)


class EmailError(Exception):
    pass


class S3SchemeLoader(S3loader):
    """Wrapper for jinja2_s3loader."""

    def __init__(self, netloc, path):
        super().__init__(netloc, path.lstrip('/'))


# pylint: disable=W0613
# netloc is intentionally left unused so that the scheme loader can be called universally
# without breaking things.
class FileSchemeLoader(jinja2.FileSystemLoader):
    """Wrapper for jinja2 file loader."""

    # noinspection PyUnusedLocal
    def __init__(self, netloc, path):
        """netloc intentionally left as it is
        as path is enough for jinja2.FileSystemLoader.
        """
        super().__init__(url2pathname(path))


_SCHEMES = dict(s3=S3SchemeLoader, file=FileSchemeLoader)


#  TODO: THIS DOES NOT BELONG HERE
def filter_readable_delivery_method(delivery_method: str):
    """So that the delivery methods that are defined here would be
    rendered as sane readable content for the end-user."""
    defined_methods = dict(
        smartpost='Posti - SmartPOST',
        pick_up_own_mail='Posti - Economy Parcel',
        pick_up_centre='Nouto liikkeestä',
        home_delivery='Kotiinkuljetus',
        pharmacy_pick_up_locker='Nouto apteekin pakettiautomaatista',
        city_mail_express='Pikatoimitus',
    )
    return defined_methods.get(delivery_method, delivery_method)


def get_jinja_env(location, filters: dict = None):
    url = urlparse(location)
    loader = _SCHEMES[url.scheme](url.netloc, url.path)
    jinja_env = jinja2.Environment(loader=loader, trim_blocks=True, lstrip_blocks=True)
    if filters is None:
        filters = dict()
    for key, func in filters.items():
        jinja_env.filters[key] = func
    return jinja_env


# pylint: disable=R0914,R0903


@singleton
class EMail:
    """
    Wrapper for handling email sending via AWS SES
    """

    def __init__(self):
        self._jinja_env = None
        self._client = None
        # TODO: really, what is so application specific thing doing here?
        self._filters = {'readable_delivery_method': filter_readable_delivery_method}

    @property
    def client(self):
        """Property that holds the boto3 SES client."""
        if self._client is None:
            self._client = boto3.client('ses')
        return self._client

    @property
    def jinja_env(self):
        """Property that holds the jinja2 Environment setup."""
        if self._jinja_env is None:
            self._jinja_env = get_jinja_env(env.EMAIL_TEMPLATE_LOCATION, self._filters)
        return self._jinja_env

    # noinspection PyTypeChecker
    def send(self, src: str, dst: str, *, cc: list = None, bcc: list = None,
             subject: str, html: str = None, text: str = None,
             reply_to: str = None,
             charset: str = 'UTF-8') -> Union[str, None]:
        """
        Sends e-mail via SES

        :param src: From address
        :param dst: To address
        :param cc: CC addresses
        :param bcc: BCC addresses
        :param subject: E-mail subject
        :param html: HTML (formatted) email body
        :param text: plain-text email body
        :param reply_to: Reply-To address
        :param charset: character encoding to use
        :return: message ID


        :raises ValueError: too many mail recipients
        :raises AssertionError: no mail body given
        :raises EmailError: SES send operation failed
        """
        assert html or text, 'must have mail message body'

        message = dict(Subject=dict(Data=subject, Charset=charset), Body=dict())

        if text:
            text = '\r\n'.join(text.splitlines())
            message['Body']['Text'] = dict(Data=text, Charset=charset)
        if html:
            message['Body']['Html'] = dict(Data=html, Charset=charset)

        destination = dict(ToAddresses=[dst])

        num_of_recipients = 1  # To: address

        if bcc:
            destination['BccAddresses'] = bcc
            num_of_recipients += len(bcc)

        if cc:
            destination['CcAddresses'] = cc
            num_of_recipients += len(cc)

        if num_of_recipients > 50:
            raise ValueError('Too many recipients (to + cc + bcc) {n}/50'.format(n=num_of_recipients))

        try:
            response = self.client.send_email(
                Source=src,
                Destination=destination,
                Message=message,
                ReplyToAddresses=[
                    reply_to if reply_to else src,
                ]
            )

        # HOTFIX, BAD CODE (TM) follows, TODO: proper handling of errors by the caller required
        except ClientError as e:
            logger.fatal(e.response['Error']['Message'])
            return None

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            logger.fatal('HTTPStatusCode: {status}'.format(status=response['ResponseMetadata']['HTTPStatusCode']))
            return None

        return response['MessageId']

    def _render(self, template: str, context: dict = None, jinja_env: Any = None) -> Dict[str, Any]:
        """Returns the rendered templates for output.

        :param template: Name of the template we want back.
        :param context: Parameters to be passed to the template.
        :return: Dict containing the templates.
        """
        if context is None:
            context = dict()

        if jinja_env is None:
            jinja_env = self.jinja_env

        templates = dict()
        for t_type, ext in (('html', 'html'), ('text', 'txt')):
            try:
                templates[t_type] = jinja_env.get_template(
                    '{template}.{ext}'.format(template=template, ext=ext)).render(context)
            except TemplateNotFound:
                pass
        if not templates:
            raise ValueError('[{template}] does not exist'.format(template=template))

        return templates

    def send_template(self, src: str, dst: str, template: str, context: dict, *, cc: list = None, bcc: list = None,
                      subject: str, reply_to: str = None,
                      charset: str = 'UTF-8') -> str:
        warnings.warn('Please update the function to use send_templated_email()', DeprecationWarning)
        return self.send_templated_email(src=src,
                                         dst=dst,
                                         template=template,
                                         context=context,
                                         cc=cc,
                                         bcc=bcc,
                                         subject=subject,
                                         reply_to=reply_to,
                                         charset=charset)

    def send_templated_email(self, src: str, dst: str, template: str, context: dict, *, cc: list = None,
                             bcc: list = None,
                             subject: str, reply_to: str = None,
                             charset: str = 'UTF-8',
                             jinja_env: Any = None) -> str:
        """Sends an email with the given template."""
        return self.send(src=src,
                         dst=dst,
                         cc=cc,
                         bcc=bcc,
                         subject=subject,
                         reply_to=reply_to,
                         charset=charset,
                         **self._render(template, context, jinja_env))
