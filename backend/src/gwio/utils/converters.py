# -*- coding: utf-8 -*-
def to_int(s):
    """
    Convert string to integer until non digit character.

    :param s: string to convert
    :return: (int, trailing string)
    """
    v = 0
    i = 0
    for c in s:
        try:
            d = int(c)
            v = v * 10 + d
        except ValueError:
            break
        i += 1
    return v, s[i:]
