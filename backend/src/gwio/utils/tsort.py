# -*- coding: utf-8 -*-
"""
Implements basic topological sort for a graph defined as a list
of connected edges. (Slightly modified Nathan Hurst's implementation,
copied here under the conditions of LGPL.)
"""


def tsort(partials):
    """ tsort is a python implementation of the tsort(1) shell command
    partials is a list of lists of partial orders.

    tsort([['a','b','c'],['d'],['e', 'f'], ['b','c','d','e']])
    should  return ['a', 'b', 'c', 'd', 'e', 'f']
      Copyright Nathan Hurst 2001
      Licenced under the LGNU GPL.  see COPYING for details.
    """

    preds = {}
    succs = {}
    output = []
    distinct_value = (0,)
    for i in partials:
        prev = distinct_value
        for j in i:
            if j not in preds:
                preds[j] = 0

            if j not in succs:
                succs[j] = {}
            if prev is not distinct_value:
                if prev not in succs:
                    succs[prev] = {}
                if j not in succs[prev]:
                    succs[prev][j] = 1
                    preds[j] += 1

            prev = j
    starts = [x for x in map(lambda a: a[0], filter(lambda a: (a[1] == 0), preds.items()))]
    while starts:
        start = starts[0]
        starts = starts[1:]  # prune off start
        output += [start]
        for i in succs[start].keys():
            preds[i] -= 1
            if preds[i] == 0:
                starts.append(i)
        del succs[start]
    return output
