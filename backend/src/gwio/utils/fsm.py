# noinspection PyArgumentList
import datetime
import logging
from collections import namedtuple
from enum import Enum
from functools import partial
from typing import (
    Any,
    List,
    Tuple,
    Union,
)

import pytz
from transitions import (
    Machine,
)

from gwio.ext import dynamorm as gwio_dynamorm

log = logging.getLogger(__name__)

FsmTransition = namedtuple('FsmTransition', ['src', 'dst', 'on', 'status'])

# Notifications are sent when entering the state, if notification fails an FsmAction._FAIL action is triggered.
#
# Notifications are sent via the model's API.notify() method. When called it will receive parameters
# model, action and notification. Even though the notify() is called after the state change has
# happened it is conceptually working "on_enter". Any failures that must be communicated to the outside
# is up to the notify() (I would assume by adding and event to the model).
#
# In case of failure, raise appropriate exception (the engine will log it). Please, keep in mind that
# !!!!!    notify() is *NOT TO CHANGE* the model's state !!!!!
#
# You can have multiple notifications assigned to state, but there is no rollback functionality and if
# any of the notifications fail the rest are NOT called, so make sure you have notification in a
# "sensible" order. Even if the engine is requesting a notification to be sent it is actually up to
# API.notify() to make the final decision BUT if it determines that a notification is not required
# it should still normally (not raise an exception, not having to do anything even when asked to is NOT an
# error).
#
# NOTE! the notifier is expected to be a dispatcher (e.g. able to handle all notifications).
# ALSO NOTE! things like sending paytrail link and creating capture charge are considered notifications
#            from OrderHandler() point of view and it is up to the dispatcher to decide what to do.
#            (e.g. there is an action OrderNotificationType.SEND_PAYMENT_INFO which needs to take i)
# ALSO ALSO NOTE! even if transition has a notification associated with it it is up to to the notifier to decide
#                 if a notification is actually going to be sent or not. E.g. who ever is reading this code
#                 to figure out why something/or somebody was notified and you see a notification defined here
#                 you should check the notifier itself.
FsmNotification = namedtuple('FsmNotification', ['state', 'via', 'by', 'do', 'ctx'])
FsmNotificationContext = namedtuple('FsmNotificationContext', ['event', 'global_ctx', 'rule_ctx'])

# Assessments are similar to notifications and are actually run before any notifications, basically
# the engine will call models API.assess() with parameters model, action and the ctx
# NOTE! read the NOTEs for FsmNotification
FsmAssessment = namedtuple('FsmAssessment', ['state', 'via', 'by', 'do', 'ctx'])
FsmAssessmentContext = namedtuple('FsmAssessmentContext', ['event', 'global_ctx', 'rule_ctx'])


class FsmProcessingError(ValueError):
    pass


class FsmEnumMixIn(Enum):
    def __str__(self):
        return str(self.value)


class _FsmStateMixIn(FsmEnumMixIn):
    @property
    def transitions_state(self):
        return str(self.value)


class FsmState(_FsmStateMixIn):
    _ANY = '*'
    _FAILED = 'failed'


class FsmActionMixIn(FsmEnumMixIn):
    @property
    def transitions_action(self):
        return str(self.value)


class FsmAction(FsmActionMixIn):
    _ANY = '_any'
    _FAIL = '_fail'


class _FsmStatusMixIn(FsmEnumMixIn):
    @property
    def condition_checker(self):
        return f'status_{self.value}'


class FsmStatus(_FsmStatusMixIn):
    ANY = None


class FsmHandler(Machine):
    # pylint: disable=too-many-arguments
    def __init__(
            self, *,
            transitions: List[FsmTransition] = None,
            notifications: List[FsmNotification] = None,
            notification_ctx: Any = None,
            assessments: List[FsmAssessment] = None,
            assessment_ctx: Any = None,
    ):
        """Create OrderHandler, if you want to override the default logic you can do so
        by passing the transitions, notifications and assessments. Any Context passed here
        are given to the model.API.notify() and model.api.assess() in the context as
        ctx.global_ctx.

        NOTE! this is a singleton, so multiple initializations do not work!!
        """

        self._notifications = dict()
        self._assessments = dict()
        self._notifiers = None
        self._notify_handler = None

        self._notify_handler = partial(self._notify, assessments=self._assessments, notifications=self._notifications)

        super().__init__(states=self.available_transitions_states(), initial=self._initial_state.transitions_state, send_event=True)
        for t in transitions if transitions is not None else self._transitions:
            self.add_transition(t.on, t.src, t.dst, conditions=[partial(self._check_for, statuses=t.status)])
        for a in assessments if assessments is not None else self._assessments:
            self.add_assessment(a, assessment_ctx)
        for n in notifications if notifications else self._notifications:
            self.add_notification(n, notification_ctx)

        # Adding support to transition to `failed` state from any other state in case automation logic fails
        for s in list(self._state_enum):
            self.add_transition(
                action=FsmAction._FAIL,
                source=s,
                dest=FsmState._FAILED,
            )

    def is_fsm_action(self, action):
        return isinstance(action, (FsmAction, self._action_enum))

    def is_fsm_state(self, state):
        return isinstance(state, (FsmState, self._state_enum))

    @classmethod
    def available_transitions_actions(cls):
        return [x.value for x in cls._action_enum]

    @classmethod
    def available_transitions_states(cls):
        # Lint thinks `FsmState._FAILED` is a string...
        # pylint: disable=no-member
        return [x.transitions_state for x in cls._state_enum] + [FsmState._FAILED.transitions_state]

    # pylint: disable=arguments-differ
    def add_transition(
            self,
            action: Enum,
            source: Enum,
            dest: Enum,
            conditions: Union[partial, List[partial]] = None,
            **kwargs
    ):
        # Check if the call is coming from Machine() itself (*sigh*)
        # NOTE! resist the temptation to support multiple destinations and/or sources
        if isinstance(action, list):
            triggers = action
        else:
            triggers = [action]

        for trigger in triggers:
            if not (self.is_fsm_action(trigger) and self.is_fsm_state(source) and self.is_fsm_state(dest)):
                super().add_transition(trigger, source, dest, conditions=conditions, **kwargs)
                return

            if trigger == FsmAction._ANY:
                raise ValueError('_ANY action cannot be used in Transitions')

            super().add_transition(
                trigger.transitions_action, source.transitions_state, dest.transitions_state,
                conditions=conditions,
                after=self._notify_handler,
                **kwargs
            )

    @classmethod
    def _transitions_states(cls, state: Union[Enum, List[Enum]]):
        if state == FsmState._ANY:
            return cls.available_transitions_states()
        if isinstance(state, list):
            return [x.transitions_state for x in state]
        return [state.transitions_state]

    @classmethod
    def _transitions_actions(cls, action: Union[Enum, List[Enum]]):
        if action == FsmAction._ANY:
            return cls.available_transitions_actions()
        if isinstance(action, list):
            return [x.transitions_action for x in action]
        return [action.transitions_action]

    def add_notification(self, n: FsmTransition, ctx: Any):
        for src in self._transitions_states(n.via):
            for dst in self._transitions_states(n.state):
                for trigger in self._transitions_actions(n.by):
                    condition = f'{trigger}-{src}-{dst}'
                    if condition not in self._notifications:
                        self._notifications[condition] = list()
                    self._notifications[condition].append(
                        (n.do, FsmNotificationContext(event=None, global_ctx=ctx, rule_ctx=n.ctx)))

    # pylint: disable=arguments-differ
    def add_assessment(self, a: FsmAssessment, ctx: Any):
        for via in self._transitions_states(a.via):
            for state in self._transitions_states(a.state):
                for trigger in self._transitions_actions(a.by):
                    condition = f'{via}-{trigger}-{state}'
                    if condition not in self._assessments:
                        self._assessments[condition] = list()
                    self._assessments[condition].append(
                        (a.do, FsmAssessmentContext(event=None, global_ctx=ctx, rule_ctx=a.ctx)))

    def add_model(
            self,
            model: 'gwio_dynamorm.FsmModel',
            *args,
            initial: Enum = None,
            **kwargs
    ):
        # Enabling support for having model being added to fsm multiple times and only when removing it from the last one does the actual removal logic run
        # This is to support fsm A -> fsm B -> fsm A, where B has to add the model to A again to make sure it is in the fsm and can handle transitions

        # The call is coming from Machine() itself (*sigh*)
        if model is None or not isinstance(model, gwio_dynamorm.FsmModel):
            return super().add_model(model, *args,
                                     initial=initial.transitions_state if isinstance(initial, self._state_enum) else initial, **kwargs)

        # adding FsmModel, we get the initial state from the model itself if it exists
        if model.current_state is not None:
            initial = model.current_state
        elif initial is None:
            initial = self._initial

        super().add_model(
            model,
            *args,
            initial=self.deserialise_state(initial).transitions_state,
            **kwargs,
        )
        try:
            model.current_state = self.deserialise_state(model.state)
        except AttributeError as e:
            raise AttributeError('Different instance of the model was already added to the FSM beforehand') from e

        try:
            model._fsm_nesting_count += 1
        except AttributeError:
            model._fsm_nesting_count = 1

    def remove_model(self, model: 'gwio_dynamorm.FsmModel', *args, **kwargs):
        #  Check that the call is from our code and not Machine() itself (*sigh*)
        if model is not None and isinstance(model, gwio_dynamorm.FsmModel):
            model.current_state = self.deserialise_state(model.state)
            model._fsm_nesting_count -= 1
            if model._fsm_nesting_count == 0:
                delattr(model, 'state')
                super().remove_model(model, *args, **kwargs)

    @staticmethod
    def _check_for(
            event: Any,
            statuses: Union[Union[Enum, Tuple[Enum, bool]], List[Union[Enum, Tuple[Enum, bool]]]] = None,
            **kwargs
    ) -> bool:

        """Generic handler for checking the order status (separate from state)

        NOTE! event can have multiple statuses (e.g paid, shipped, etc)
        """
        if statuses == FsmStatus.ANY:
            return True
        if not isinstance(statuses, list):
            statuses = [statuses]

        # As the statuses list is an *AND* list , all must be true for the status to be true
        for condition in statuses:
            condition_requirement = True
            if isinstance(condition, tuple):
                condition, condition_requirement = condition

            if getattr(event.model, condition.condition_checker) != condition_requirement:
                return False
        return True

    # pylint: disable=unused-argument
    @staticmethod
    def _process_action(model, action: Enum):
        """Do the actual triggering (via process())

        OrderHandler().add_model() will hook this into the model and
        OrderHandler().remove_model() will unhook it.
        """
        # NOTE! before actually calling model.process() the caller must
        # update the model data and only then triggering the state change.
        # E.g. if you are here trying to figure out why for example model.process(OrderAction.PAYMENT)
        # did not trigger the transformation even though the order was fully paid, this is the reason.
        try:
            model.trigger(action.transitions_action)
        except AttributeError as e:
            if 'Model has no trigger' in e.args[0]:
                log.exception(e)
                raise FsmProcessingError(f'Unsupported action "{action.transitions_action}"')
            raise  # Lets not mask AttributeErrors that are not ours

    @staticmethod
    def _run_explicit_logic_for_state(model, dst, action, src):
        # pylint: disable=broad-except
        try:
            fn = getattr(model.ProcessLogic, dst)
            try:
                fn(model=model, action=action, source=src)
            except Exception as e:
                log.exception(e)
                # Lint thinks `FsmAction._FAIL` is a string...
                # pylint: disable=no-member
                model.trigger(FsmAction._FAIL.transitions_action)
                return False
        except AttributeError:
            pass
        return True

    @classmethod
    def _notify(
            cls,
            event_data,
            *,
            assessments=List[FsmAssessmentContext],
            notifications=List[Tuple[Enum, FsmNotificationContext]]
    ):
        """The transition after function responsible for calling the defined assessments, process logic and notifiers.

        If any errors happen in the notify, process logic or assess functions the a transition to OrderState._FAILED
        is automatically triggered. See the notes about the assessor/notifier being responsible for saving any information
        that needs to be passed to the end user.
        """
        by = event_data.event.name
        try:
            action = cls._action_enum(by)
        except ValueError:
            action = FsmAction(by)
        src = event_data.transition.source
        dst = event_data.transition.dest
        model = event_data.model

        # Adding transition to history log since this is all happening after the transition
        model._transition_history.append(dict(
            action=action,
            state=cls.deserialise_state(model.state),
            timestamp=datetime.datetime.now(tz=pytz.UTC),
        ))

        for assessment, ctx in assessments.get(f'{src}-{by}-{dst}', list()):
            model.API.assess(model=model,
                             action=action,
                             assessment=assessment,
                             ctx=FsmAssessmentContext(event=event_data.event,
                                                      global_ctx=ctx.global_ctx if ctx.global_ctx is not None else dict(),
                                                      rule_ctx=ctx.rule_ctx if ctx.rule_ctx is not None else dict()))

        # If the order model has explicit process logic defined for the state call it
        if not cls._run_explicit_logic_for_state(model, dst, action, src):
            return

        for notification, ctx in notifications.get(f'{by}-{src}-{dst}', list()):
            # pylint: disable=broad-except
            try:
                model.API.notify(model=model,
                                 action=action,
                                 notification=notification,
                                 ctx=FsmNotificationContext(event=event_data.event,
                                                            global_ctx=ctx.global_ctx if ctx.global_ctx is not None else dict(),
                                                            rule_ctx=ctx.rule_ctx if ctx.rule_ctx is not None else dict()))

            except Exception as e:
                log.exception(e)
                # Lint thinks `FsmAction._FAIL` is a string...
                # pylint: disable=no-member
                model.trigger(FsmAction._FAIL.transitions_action)
                return

    @classmethod
    def deserialise_action(cls, action):
        if isinstance(action, (FsmAction, cls._action_enum)):
            return action
        try:
            return cls._action_enum(action)
        except ValueError:
            return FsmAction(action)

    @classmethod
    def deserialise_state(cls, state):
        if isinstance(state, (FsmState, cls._state_enum)):
            return state
        try:
            return cls._state_enum(state)
        except ValueError:
            return FsmState(state)
