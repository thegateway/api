import base64

from jwt import decode


class IntegrationTokenVerifier:
    def __init__(self, key):
        self.key = base64.b64decode(key)

    def verify_token(self, jwt):
        return decode(jwt, key=self.key, algorithms="HS256", options={'verify_aud': False})
