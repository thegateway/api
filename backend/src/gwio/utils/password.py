# -*- coding: utf-8 -*-
"""
Small set of utilities for handling passwords
"""
from hmac import compare_digest as compare_hash

import bcrypt


def verify_password(pswd: str, salted_hash: str) -> bool:
    """Verify given plaintext password matches hashed password"""
    return compare_hash(bcrypt.hashpw(pswd.encode(), salted_hash.encode()).decode(), salted_hash)


def hash_password(pswd: str) -> str:
    """Create hashed password from plaintext"""
    assert pswd
    return bcrypt.hashpw(pswd.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
