# -*- coding: utf-8 -*-
"""
AES encrypt / decrypt interface for AWS KMS and local 256 bit keys
"""
import os
import base64
import boto3

from cryptography.hazmat.primitives import hashes, padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend


class _IAMKMS:
    """Borg for accessing kms"""
    _instance = None
    kms = None
    keys = None
    encrypted_keys = None

    # Not used, but they are present on the call -mte
    # pylint: disable=W0613
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self, key=None, encrypted_key=None):
        if self.kms is None:
            self.kms = boto3.client('kms')
        if self.keys is None:
            self.keys = dict()
        if self.encrypted_keys is None:
            self.encrypted_keys = dict()

        if key is None and encrypted_key is None:
            raise ValueError('either key or encrypted_key must be given')

        if key is not None and encrypted_key is not None:
            raise ValueError('key and encrypted_key are mutually exclusive')

        if key is None:
            self._init_with_encrypted_key(encrypted_key)
        else:
            self._init_with_keyid(key)

    def _init_with_encrypted_key(self, encrypted_key):
        if not isinstance(encrypted_key, bytes):
            raise TypeError('encrypted_key: expected bytes')

        if encrypted_key not in self.encrypted_keys:
            self.encrypted_keys[encrypted_key] = self._decrypt(encrypted_key)

        key = self.encrypted_keys[encrypted_key]['keyid']
        plaintext_key = self.encrypted_keys[encrypted_key]['plaintext']

        if key not in self.keys:
            self.keys[key] = dict(key=plaintext_key, encrypted_key=encrypted_key)

    def _init_with_keyid(self, key):
        if key not in self.keys:
            plaintext_key, encrypted_key = self._get_data_key(key)

            self.keys[key] = dict(key=plaintext_key, encrypted_key=encrypted_key)
            self.encrypted_keys[encrypted_key] = dict(keyid=key, plaintext=plaintext_key)

    def _get_data_key(self, keyid):
        try:
            result = self.kms.generate_data_key(KeyId=keyid, KeySpec="AES_256")
            return result['Plaintext'], result['CiphertextBlob']
        except Exception:
            raise LookupError('key "{keyid}" is not available'.format(keyid=keyid))

    def _decrypt(self, data):
        try:
            result = self.kms.decrypt(CiphertextBlob=data)
            return dict(keyid=result['KeyId'], plaintext=result['Plaintext'])
        except Exception:
            raise LookupError('key is not available')

    def get_data_key(self, key):
        """get already generated key from cache"""
        return self.keys[key]['key'], self.keys[key]['encrypted_key']

    def get_key(self, encrypted_key):
        """return key represented by the encrypted key"""
        if encrypted_key not in self.encrypted_keys:
            self.encrypted_keys[encrypted_key] = self._decrypt(encrypted_key)
        return (
            self.encrypted_keys[encrypted_key]['keyid'].split('/')[-1],
            self.encrypted_keys[encrypted_key]['plaintext']
        )

    def decache(self, keyid):
        """remove cached information related to key"""
        if keyid in self.keys:
            del self.keys[keyid]

        encrypted_keys = [x for x in self.encrypted_keys if x == keyid]
        for encrypted_key in encrypted_keys:
            del self.encrypted_keys[encrypted_key]

    def decache_all(self):
        """remove chached information related to all keys"""
        self.keys = dict()
        self.encrypted_keys = dict()


# Hide the initialization details inside __init__ otherwise basically just a dict()
# pylint: disable=R0903
class CipherKey:
    """256 bit AES key generated from a string"""

    def __init__(self, key, encoding='utf-8'):
        if isinstance(key, str):
            key = key.encode(encoding)
            shakey = hashes.Hash(hashes.SHA256(), backend=default_backend())
            shakey.update(key)
            self.key = shakey.finalize()
        else:
            if not isinstance(key, bytes) or len(key) != 32:
                raise TypeError('Expected 256 bit key')
            self.key = key


# Inherits from Cipher key purely for assertion purposes -mte
# pylint: disable=W0231,R0903
# noinspection PyMissingConstructor
class AwsCipherKey(CipherKey):
    """256 bit AES key generated by AWS KMS with the "master" key"""

    def __init__(self, keyid=None, encrypted_key=None):
        self.kms = _IAMKMS(keyid, encrypted_key)
        if encrypted_key is None:
            self.keyid = keyid
            self.key, self.encrypted_key = self.kms.get_data_key(keyid)
        else:
            self.keyid, self.key = self.kms.get_key(encrypted_key)
            self.encrypted_key = encrypted_key


class AESCipher:
    """Simple AES encrypter / decrypter interface to cryptography library"""

    def __init__(self, key):
        if not isinstance(key, CipherKey):
            raise TypeError('Unexpected key type')
        self.key = key.key

    def encrypt(self, plaintext, iv=None):
        """Encrypt plaintext and return AES/CBC ciphertext.
        Note if iv is given the ciphertext does not include the IV and you must provide the IV yourself
        when decrypting.
        """
        if iv is None:
            cipher_iv = os.urandom(16)
        else:
            if not isinstance(iv, bytes) or len(iv) != 16:
                raise TypeError('Expected 128 bit IV')
            cipher_iv = iv
        pad = padding.PKCS7(128).padder()
        padded_text = pad.update(plaintext) + pad.finalize()
        cipher = Cipher(algorithms.AES(self.key), mode=modes.CBC(cipher_iv), backend=default_backend())
        encrypter = cipher.encryptor()
        ciphertext = encrypter.update(padded_text) + encrypter.finalize()
        return cipher_iv + ciphertext if iv is None else ciphertext

    def decrypt(self, data, iv=None):
        """Decrypt AES/CBS ciphertext
        NOTE if iv is not given it is assumed that the first 128 bits of ciphertext is the IV"""
        if iv is None:
            iv = data[:16]
            ciphertext = data[16:]
        else:
            ciphertext = data

        cipher = Cipher(algorithms.AES(self.key), mode=modes.CBC(iv), backend=default_backend())
        decrypter = cipher.decryptor()
        padded_text = decrypter.update(ciphertext) + decrypter.finalize()
        unpad = padding.PKCS7(128).unpadder()
        plaintext = unpad.update(padded_text) + unpad.finalize()
        return plaintext


def decrypt_string(s, key=None):
    """Decrypt string encrypted with encrypt_string()"""

    def _decrypt(b, decrypting_key):
        cipher = AESCipher(decrypting_key)
        return cipher.decrypt(b).decode('utf-8')

    def _aws_decrypt(b, decrypting_key):
        return _decrypt(b, AwsCipherKey(encrypted_key=decrypting_key))

    part = s.split(':') if s else []
    if len(part) != 4:
        raise ValueError('malformed encrypted string')
    method = part[1]
    ciphertext = base64.b64decode(part[2].encode('utf-8'))
    encrypted_key = base64.b64decode(part[3].encode('utf-8'))
    # pylint: disable=no-else-return
    if method == 'aws':
        return _aws_decrypt(ciphertext, encrypted_key)
    elif method == 'local':
        if key is None:
            raise ValueError('parameter key not given')
        return _decrypt(ciphertext, key)

    raise LookupError('Unknown encryption')


def encrypt_string(s, key):
    """Returns encrypted string with embedded key (or :local: if not encrypted with AWS CipherKey"""
    if not isinstance(key, CipherKey):
        raise TypeError('key is not CipherKey')

    b64key = b''
    method = b'local'
    cipher = AESCipher(key)
    ciphertext = cipher.encrypt(s.encode('utf-8'))
    if isinstance(key, AwsCipherKey):
        method = b'aws'
        b64key = base64.b64encode(key.encrypted_key)
    b64ciphertext = base64.b64encode(ciphertext)
    bvalue = b':' + method + b':' + b64ciphertext + b':' + b64key
    return bvalue.decode('utf-8')


def getenv(name, default=None, key=None):
    """Decrypt encrypted environment string"""
    value = os.getenv(name, default)
    return decrypt_string(value, key=key)


def generate_env(name, value, key, prefix=None):
    """Generate bourne shell type encrypted environment setting"""
    if prefix is None:
        prefix = ''
    return "{prefix}{name}='{value}'".format(prefix=prefix, name=name, value=encrypt_string(value, key))
