import base64

import simplejson as json
from jose import jws

from gwio.environment import Env


def _serialise_dict(d: dict):
    if not isinstance(d, dict):
        raise TypeError(f'`{d}` is not a dict, but {type(d)}')
    return json.dumps(
        d,
        sort_keys=True,
        indent=2,
    )


def sign(payload: str) -> str:
    """
    Signs the payload and returns it with the JWT header and signature
    :param payload:
    :return:
    """
    payload_dict = json.loads(payload)
    signed = jws.sign(_serialise_dict(payload_dict).encode('utf-8'), Env.SYSTEM_SECRET_KEY, algorithm='HS256')
    h, _, s = signed.split('.')

    return _serialise_dict(dict(
        header=h,
        signature=s,
        payload=payload_dict,
        vertical=Env.VERTICAL,
        environment=Env.APPLICATION_ENVIRONMENT,
    ))


def verify(gwio_signed_payload: dict) -> dict:
    """
    Encodes the payload and forms the JWT from that and the given header-signature combination
    :param gwio_signed_payload: The dictionary returned by `gwio.utils.sign`
    :return: payload
    """

    def _encode(d):
        json_ = _serialise_dict(d)
        bytes_ = json_.encode('utf-8')
        return base64.urlsafe_b64encode(bytes_).decode('ascii').rstrip('=')

    token = '.'.join(
        (gwio_signed_payload['header'],
         _encode(gwio_signed_payload['payload']),
         gwio_signed_payload['signature'])
    )

    encoded_payload = jws.verify(token, Env.SYSTEM_SECRET_KEY, algorithms=['HS256'])
    return json.loads(encoded_payload)
