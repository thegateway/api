def get_categories():
    google_categories = [
        {
            'guid': '104aaf11-2aab-59df-a713-bf35560bc3d0',
            'name': 'Men',
            'google_category_id': '166'
        },
        {
            'guid': 'd22424e1-bee8-5dd8-a1d8-52c4260882f8',
            'name': 'Clothing',
            'google_category_id': '1604'
        },
        {
            'guid': '67ae9903-58dd-5509-b66f-93fe52838e1b',
            'name': 'Tshirt and T-shirts',
            'google_category_id': '212'
        },
        {
            'guid': '8e09a5a5-0bae-55ec-bb75-66c43d44ea92',
            'name': 'Shirts',
            'google_category_id': '212'
        },
        {
            'guid': '84d8667b-7f56-52d7-b0dc-cfc9dc756f84',
            'name': 'Sweaters and Sweatshirts',
            'google_category_id': '212'
        },
        {
            'guid': '208418df-6f6c-5b7c-b026-37a376adbcd1',
            'name': 'Jackets',
            'google_category_id': '5598'
        },
        {
            'guid': '078b884a-dd6e-557b-833d-c17db4ae63f1',
            'name': 'Pants and Shorts',
            'google_category_id': '204'
        },
        {
            'guid': '49fe75b7-a6f8-535b-8514-53d0064ac0b0',
            'name': 'Jeans',
            'google_category_id': '204'
        },
        {
            'guid': '2753fa93-19d0-5809-81aa-4662efec09c5',
            'name': 'Outerwear',
            'google_category_id': '203'
        },
        {
            'guid': 'a31d523d-4fdc-5946-a577-6b4dea05bef1',
            'name': 'Suits',
            'google_category_id': '1594'
        },
        {
            'guid': 'bb573e2e-9982-55e0-9523-10df81c7e82c',
            'name': 'Underwear',
            'google_category_id': '213'
        },
        {
            'guid': '05419479-afb2-5ea6-807e-8861a7495031',
            'name': 'Sportswear',
            'google_category_id': '212'
        },
        {
            'guid': 'bd6d9824-60ac-560b-ada4-bfc71bf92e37',
            'name': 'Footwear',
            'google_category_id': '187'
        },
        {
            'guid': 'b799d5dd-5ab0-5013-b7b4-1ba5f180caf7',
            'name': 'Sports Footwear',
            'google_category_id': '187'
        },
        {
            'guid': '70490c77-446f-51bb-80ea-27a23833082a',
            'name': 'Formal Shoes',
            'google_category_id': '187'
        },
        {
            'guid': '99f1fe00-9079-5ebb-893b-27462a7584fa',
            'name': 'Casual Shoes',
            'google_category_id': '187'
        },
        {
            'guid': 'bba9d175-f06d-502e-ac11-24e080687ced',
            'name': 'Sandals And Boots',
            'google_category_id': '187'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '166'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '1604'
        },
        {
            'guid': 'b137ebfd-bc19-53b8-9de6-46c7d51c3054',
            'name': 'Bags And Backpacks',
            'google_category_id': '3032'
        },
        {
            'guid': '9478e57a-6324-5a0d-8619-5964d0609b3b',
            'name': 'Jewelry',
            'google_category_id': '188'
        },
        {
            'guid': '665e37ff-e795-55ee-a317-2266da3322e0',
            'name': 'Watches',
            'google_category_id': '201'
        },
        {
            'guid': '3714db6c-0a00-523d-b6cd-5476365b6940',
            'name': 'Glasses',
            'google_category_id': '524'
        },
        {
            'guid': 'd2fb821a-7f18-5c90-90ff-9b741e071f6c',
            'name': 'Wallets And Pouch',
            'google_category_id': '2668'
        },
        {
            'guid': '1e2c2066-6df8-5955-99e6-913ae94b0e8c',
            'name': 'Belts',
            'google_category_id': '169'
        },
        {
            'guid': 'd82f02d3-680b-5d04-b2b2-a5bec8ff6a5d',
            'name': 'Scarves / hats / gloves',
            'google_category_id': '167'
        },
        {
            'guid': 'c139ffd7-8dee-5875-876f-e7055d219eb4',
            'name': 'Suit Accessories',
            'google_category_id': '167'
        },
        {
            'guid': 'd5c32e93-64dc-59bc-83bb-91885a6a5ab6',
            'name': 'Home And Garden',
            'google_category_id': '536'
        },
        {
            'guid': '02f3abc8-0b02-5d1f-9ae4-6b215357ea16',
            'name': 'Living Room',
            'google_category_id': '536'
        },
        {
            'guid': '81f170a4-c667-5940-b9eb-4745b8b12c4a',
            'name': 'Kitchen And Dining Room',
            'google_category_id': '638'
        },
        {
            'guid': '4aca41e4-5d72-5102-b75a-0e031d864051',
            'name': 'Bedroom',
            'google_category_id': '536'
        },
        {
            'guid': 'b1b0afe3-5742-5b75-9a69-01fba74b834b',
            'name': 'Bathroom',
            'google_category_id': '574'
        },
        {
            'guid': 'a9b492a9-3aa9-5b7e-acaa-6403297136d0',
            'name': "Children's Room",
            'google_category_id': '536'
        },
        {
            'guid': 'f41f6b62-88ea-572d-b3d7-e4345031c46c',
            'name': 'Furniture',
            'google_category_id': '436'
        },
        {
            'guid': '8551262f-7812-59f2-80f5-2ba9a546db36',
            'name': 'Garden',
            'google_category_id': '536'
        },
        {
            'guid': '3f27ace2-5108-53fa-a7f3-33e9515dc95d',
            'name': 'Other',
            'google_category_id': '604'
        },
        {
            'guid': '28c1a45c-1499-5f20-b591-e53eb78848dc',
            'name': 'Art. Food',
            'google_category_id': '3731'
        },
        {
            'guid': 'c01c93bc-c8ad-5ffb-b377-4ee2fdf528d6',
            'name': 'Food',
            'google_category_id': '412'
        },
        {
            'guid': 'eacb5b1c-10e4-5889-bbbf-3690027a744d',
            'name': 'Healthy Food',
            'google_category_id': '412'
        },
        {
            'guid': '972b8a5c-7b34-5428-a66e-19adb34a9c08',
            'name': 'Sport',
            'google_category_id': '988'
        },
        {
            'guid': 'd22424e1-bee8-5dd8-a1d8-52c4260882f8',
            'name': 'Clothing',
            'google_category_id': '1604'
        },
        {
            'guid': '0f046551-5c90-52e0-8ed9-3c89bbeaa6a0',
            'name': "Women's ",
            'google_category_id': '1604'
        },
        {
            'guid': '104aaf11-2aab-59df-a713-bf35560bc3d0',
            'name': 'Male',
            'google_category_id': '1604'
        },
        {
            'guid': '8c4fd0b4-cfed-57f6-97a3-85e4dfeeadbb',
            'name': 'Child',
            'google_category_id': '1604'
        },
        {
            'guid': 'bd6d9824-60ac-560b-ada4-bfc71bf92e37',
            'name': 'Footwear',
            'google_category_id': '1933'
        },
        {
            'guid': '0f046551-5c90-52e0-8ed9-3c89bbeaa6a0',
            'name': "Women's ",
            'google_category_id': '166'
        },
        {
            'guid': '104aaf11-2aab-59df-a713-bf35560bc3d0',
            'name': 'Male',
            'google_category_id': '166'
        },
        {
            'guid': '8c4fd0b4-cfed-57f6-97a3-85e4dfeeadbb',
            'name': 'Child',
            'google_category_id': '166'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '166'
        },
        {
            'guid': '1dae8b49-4aa3-5488-93b2-fb894ad5d6f6',
            'name': 'Nutrients And Supplements',
            'google_category_id': '2890'
        },
        {
            'guid': '2f110b55-3308-57eb-bfb3-9ad3f8d65bfa',
            'name': 'Equipment',
            'google_category_id': '988'
        },
        {
            'guid': '497ad890-921b-5290-9ada-69072e47aa9c',
            'name': 'Multimedia',
            'google_category_id': '222'
        },
        {
            'guid': 'bdcc7d09-4fd7-50b3-a52f-c4bc50f93b4d',
            'name': 'Rtv',
            'google_category_id': '386'
        },
        {
            'guid': 'cf302606-4d97-5695-b713-3d7df01ce138',
            'name': 'Home Appliances',
            'google_category_id': '3348'
        },
        {
            'guid': '723ca696-c420-5cad-9c94-f5dbbcf776d3',
            'name': 'Smartphones',
            'google_category_id': '270'
        },
        {
            'guid': 'b2d50386-1dd6-537e-8993-c0b15cee9b6e',
            'name': 'Laptops And Notebooks',
            'google_category_id': '328'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '2082'
        },
        {
            'guid': '3f27ace2-5108-53fa-a7f3-33e9515dc95d',
            'name': 'Other',
            'google_category_id': '2082'
        },
        {
            'guid': '5f0fd5d9-f732-5572-97b4-a988bba60ead',
            'name': 'Woman',
            'google_category_id': '166'
        },
        {
            'guid': 'd22424e1-bee8-5dd8-a1d8-52c4260882f8',
            'name': 'Clothing',
            'google_category_id': '1604'
        },
        {
            'guid': 'a235d13a-628d-58d0-b483-e52110e1a3be',
            'name': 'Dresses',
            'google_category_id': '2271'
        },
        {
            'guid': '8e09a5a5-0bae-55ec-bb75-66c43d44ea92',
            'name': 'Shirts',
            'google_category_id': '212'
        },
        {
            'guid': '2c1d1c87-eca1-59ee-8b76-9f9c1692f0f8',
            'name': 'T Shirt And Tops',
            'google_category_id': '212'
        },
        {
            'guid': '84d8667b-7f56-52d7-b0dc-cfc9dc756f84',
            'name': 'Sweaters and Sweatshirts',
            'google_category_id': '212'
        },
        {
            'guid': '2753fa93-19d0-5809-81aa-4662efec09c5',
            'name': 'Outerwear',
            'google_category_id': '203'
        },
        {
            'guid': '208418df-6f6c-5b7c-b026-37a376adbcd1',
            'name': 'Jackets',
            'google_category_id': '5598'
        },
        {
            'guid': '49fe75b7-a6f8-535b-8514-53d0064ac0b0',
            'name': 'Jeans',
            'google_category_id': '204'
        },
        {
            'guid': '078b884a-dd6e-557b-833d-c17db4ae63f1',
            'name': 'Pants and Shorts',
            'google_category_id': '204'
        },
        {
            'guid': '446ad33b-1cbf-54f1-921e-b1d858c4f1ea',
            'name': 'Skirts',
            'google_category_id': '2271'
        },
        {
            'guid': 'a3c4b226-f2fb-5bee-9e8b-f232c0ec46a0',
            'name': 'Overalls',
            'google_category_id': '7132'
        },
        {
            'guid': '5c6bf9a1-a7a6-5d38-8378-f8a62cbf2e60',
            'name': 'Underwear And Pajamas',
            'google_category_id': '213'
        },
        {
            'guid': '05419479-afb2-5ea6-807e-8861a7495031',
            'name': 'Sportswear',
            'google_category_id': '212'
        },
        {
            'guid': 'bd6d9824-60ac-560b-ada4-bfc71bf92e37',
            'name': 'Footwear',
            'google_category_id': '187'
        },
        {
            'guid': 'b799d5dd-5ab0-5013-b7b4-1ba5f180caf7',
            'name': 'Sports Footwear',
            'google_category_id': '187'
        },
        {
            'guid': '3863ee99-3338-5a15-83eb-2fad92fc3016',
            'name': 'Flat-Footed Shoes',
            'google_category_id': '187'
        },
        {
            'guid': 'ddea038d-d62c-5f56-ad2c-546d6e322d37',
            'name': 'High Heels',
            'google_category_id': '187'
        },
        {
            'guid': '1f70e2a0-ab72-5f48-a1b2-fd7d62472dd1',
            'name': 'Sandals And Flip Flops',
            'google_category_id': '187'
        },
        {
            'guid': '13c0490d-37f1-5012-a2df-72f3079eb27b',
            'name': 'Boots',
            'google_category_id': '187'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '187'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '1604'
        },
        {
            'guid': 'e599e4e6-0cfe-539a-98ce-96647007010e',
            'name': 'Purses',
            'google_category_id': '3032'
        },
        {
            'guid': '9478e57a-6324-5a0d-8619-5964d0609b3b',
            'name': 'Jewelry',
            'google_category_id': '188'
        },
        {
            'guid': '665e37ff-e795-55ee-a317-2266da3322e0',
            'name': 'Watches',
            'google_category_id': '201'
        },
        {
            'guid': '3714db6c-0a00-523d-b6cd-5476365b6940',
            'name': 'Glasses',
            'google_category_id': '524'
        },
        {
            'guid': 'd2fb821a-7f18-5c90-90ff-9b741e071f6c',
            'name': 'Wallets And Pouch',
            'google_category_id': '2668'
        },
        {
            'guid': '1e2c2066-6df8-5955-99e6-913ae94b0e8c',
            'name': 'Belts',
            'google_category_id': '169'
        },
        {
            'guid': 'f9812eb6-7adc-5d51-b10c-efe563233be2',
            'name': 'Scarves And Scarves',
            'google_category_id': '167'
        },
        {
            'guid': 'a412031a-0f0a-55b4-a102-9820a90e84c3',
            'name': 'Caps And Hats',
            'google_category_id': '167'
        },
        {
            'guid': '576ae0c1-466e-5892-a787-b49ef270ac5f',
            'name': 'Gloves',
            'google_category_id': '167'
        },
        {
            'guid': '3f27ace2-5108-53fa-a7f3-33e9515dc95d',
            'name': 'Other',
            'google_category_id': '604'
        },
        {
            'guid': 'a9d26ed8-64d4-5421-88c3-d25bc24da3d7',
            'name': 'Health And Beauty',
            'google_category_id': '469'
        },
        {
            'guid': '5014e47a-f7bb-5b68-ab02-612bab32e119',
            'name': 'Care',
            'google_category_id': '491'
        },
        {
            'guid': 'c16f2b64-c3b7-5fb0-a20c-f6b5d1648e12',
            'name': 'Body',
            'google_category_id': '474'
        },
        {
            'guid': '1d496c1e-7d67-5769-aef1-d5c2235f65f0',
            'name': 'Face',
            'google_category_id': '2619'
        },
        {
            'guid': '04b21b6a-871c-5a07-b78d-8c6b2b2867bb',
            'name': 'Hands and Feet',
            'google_category_id': '474'
        },
        {
            'guid': '9ae1036d-868a-52cc-bef9-316a4dddae68',
            'name': 'Child Care',
            'google_category_id': '474'
        },
        {
            'guid': '9824af31-b842-5d4e-bad4-2512f573aa17',
            'name': 'Makeup',
            'google_category_id': '477'
        },
        {
            'guid': 'a7417d45-84be-5f33-9173-823dc9a217f3',
            'name': 'Perfumes',
            'google_category_id': '479'
        },
        {
            'guid': 'f66fbae5-11aa-5eea-97b9-1c0c0574918e',
            'name': 'Vision Correction',
            'google_category_id': '1380'
        },
        {
            'guid': '8c4fd0b4-cfed-57f6-97a3-85e4dfeeadbb',
            'name': 'Child',
            'google_category_id': '166'
        },
        {
            'guid': 'd22424e1-bee8-5dd8-a1d8-52c4260882f8',
            'name': 'Clothing',
            'google_category_id': '1604'
        },
        {
            'guid': 'd7b132b2-b2cb-56e7-a63b-fd849f394b37',
            'name': '0-2 Years',
            'google_category_id': '1604'
        },
        {
            'guid': 'b392267b-04e4-5f01-a461-1cb5a204a872',
            'name': '2-9 Years',
            'google_category_id': '1604'
        },
        {
            'guid': '7aa46a67-3470-5c30-9c15-83522a649ec7',
            'name': '9-16 Years',
            'google_category_id': '1604'
        },
        {
            'guid': 'bd6d9824-60ac-560b-ada4-bfc71bf92e37',
            'name': 'Footwear',
            'google_category_id': '187'
        },
        {
            'guid': 'b799d5dd-5ab0-5013-b7b4-1ba5f180caf7',
            'name': 'Footwear Spor towe',
            'google_category_id': '187'
        },
        {
            'guid': '5d409a48-2e64-57a5-90be-f019861e3789',
            'name': 'Casual Footwear',
            'google_category_id': '187'
        },
        {
            'guid': 'c79893f9-8d79-5657-bf57-33ce87d846e3',
            'name': 'Summer Footwear',
            'google_category_id': '187'
        },
        {
            'guid': 'a3b4d958-75a4-56a8-9b60-838c681fb52c',
            'name': 'Winter Footwear',
            'google_category_id': '187'
        },
        {
            'guid': 'd6502ff1-6ac3-50d1-afb0-9fb473b26242',
            'name': 'Accessories',
            'google_category_id': '166'
        },
        {
            'guid': 'd0e3ad78-bf61-52c7-9659-bb02764cdb7d',
            'name': 'Toys',
            'google_category_id': '1253'
        },
        {
            'guid': '03cb5e26-c8ee-590e-afab-1cdb2bb4180e',
            'name': 'Other Articles',
            'google_category_id': '222'
        },
        {
            'guid': 'd2298145-b78c-54d7-b296-8c38de33c17e',
            'name': 'Art. Przemysłowe',
            'google_category_id': '111'
        },
        {
            'guid': '9da4d379-7b1a-5695-9dc6-479e9ea4406b',
            'name': 'Art. For Animals',
            'google_category_id': '1'
        },
        {
            'guid': 'e5e54385-4d86-5ba9-bd3b-73ab338596c8',
            'name': 'Gift items',
            'google_category_id': '2559'
        },
        {
            'guid': '26c3b3e0-7ef2-51b6-b712-b3ddb19c6896',
            'name': 'Press and Books',
            'google_category_id': '784'
        },
        {
            'guid': '95d4e01f-4f76-5b31-909c-257c257806ac',
            'name': 'Art. Stationery',
            'google_category_id': '503765'
        }
    ]
    return google_categories
