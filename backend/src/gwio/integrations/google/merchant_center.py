import os

import requests
from elasticsearch import Elasticsearch
from google.oauth2 import service_account
from google.auth.transport.requests import Request
from gwio.environment import Env
from gwio.utils.table_names import make_table_name
from gwio.integrations.google.google_categories import get_categories


class GMC:
    def __init__(self, *, elasticsearch_url, merchant_id):
        self.elastic_client = Elasticsearch(elasticsearch_url)
        self.index = make_table_name(Env.VERTICAL, Env.APPLICATION_ENVIRONMENT, 'webshop_product')
        self.merchant_id = merchant_id
        file_path = os.path.join(os.path.dirname(__file__))
        scopes = ['https://www.googleapis.com/auth/content']
        service_account_file = f'{file_path}/content-api-key.json'

        self.credentials = service_account.Credentials.from_service_account_file(
            service_account_file, scopes=scopes)
        self.credentials.refresh(Request())
        self.session = requests.Session()
        self.credentials.apply(self.session.headers)

    def run(self):
        query = dict(
            query=dict(
                bool=dict(
                    must=[
                        dict(
                            exists=dict(
                                field='for_sale'
                            )
                        ),
                        dict(
                            exists=dict(
                                field='activated'
                            )
                        )
                    ],
                    must_not=dict(
                        exists=dict(
                            field='archived'
                        )
                    )
                )
            ),
            size=100
        )
        # res = helpers.scan(elastic_client,
        #                    index=index,
        #                    query=query
        #                    )
        # loop through the generator to get all the products
        # for item in res:
        #     print(item)

        res = self.elastic_client.search(
            index=self.index,
            body=query
        )

        products = [item['_source'] for item in res['hits']['hits']]

        categories = requests.get('https://api.epp-galeria.app/bootstrap').json()['tag_hierarchy']

        def recurse_child(tag: dict) -> list:
            tags = [dict(guid=tag['guid'], name=tag['name'], google_category_id='')]
            for child in tag['children']:
                tags.extend(recurse_child(child))
            return tags

        def recurse(tags: list) -> list:
            all_tags = []
            for tag in tags:
                all_tags.extend(recurse_child(tag))
            return all_tags

        categories = recurse(categories)

        def get_category_id(item):
            google_categories = get_categories()
            filtered_categories = [guid['guid'] for guid in item if guid['type'] == 'category']
            category_id = next((category['google_category_id'] for category in google_categories if
                                category['guid'] == filtered_categories[len(filtered_categories) - 1]), None)
            return category_id

        batch_data = {
            'entries': [

            ]
        }
        batch_id = 1
        for item in products:
            product = {
                'batchId': batch_id,
                'merchantId': self.merchant_id,
                'method': 'insert',
                'product': {
                    'kind': 'content#product',
                    'id': item['sku'] if item['sku'] is not None else item['guid'],
                    'title': item['name'],
                    'description': item['brief'],
                    "offerId": item['guid'],
                    'link': f'https://galeria.online/products/item/{item["guid"]}',
                    'imageLink': item['images'][0],
                    'brand': item['brand'] if hasattr(item, 'brand') else item['shop_name'],
                    'availability': "in stock" if item['stock_level'] > 0 else "out of stock",
                    "contentLanguage": "pl",
                    "targetCountry": "PL",
                    "channel": "online",
                    "googleProductCategory": get_category_id(item['tags']),
                    'price': {
                        'value': item['price']['final']['unit']['amount'],
                        'currency': item['price']['final']['unit']['currency'],
                    },
                }
            }
            batch_data['entries'].append(product)
            batch_id += 1

        response = self.session.post('https://www.googleapis.com/content/v2.1/products/batch',
                                     json=batch_data)
        print(response.json())


if __name__ == '__main__':
    gmc = GMC(elasticsearch_url="https://vpc-gwio-dev-54ooopvrpgjn7hkjuz7lowy72y.eu-west-1.es.amazonaws.com:8443",
              merchant_id=216871179)
    gmc.run()
