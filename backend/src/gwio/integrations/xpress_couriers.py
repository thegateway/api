"""
python -m zeep http://api003.x-press.com.pl:89/Service.asmx?WSDL for API documentation.

Current operations below for reference:

CancelOrder(login: xsd:string, password: xsd:string, orderNo: xsd:string) -> CancelOrderResult: xsd:boolean
CreateOrder(login: xsd:string, password: xsd:string, Payer: ns0:Location, ServiceType: xsd:int, ServiceID: xsd:int, pickupLocation: ns0:Location,
            deliveryLocation: ns0:Location, ReadyDate: xsd:dateTime, pickupDeadline: xsd:dateTime, Parcels: ns0:ArrayOfParcel, cod: ns0:COD, ROD: xsd:boolean,
            SaturdayDelivery: xsd:boolean, InsuranceAmount: xsd:decimal, MPK: xsd:string, addidtionalInfo: xsd:string, PackageContentDescr: xsd:string,
            rebateCoupon: xsd:string, RequestLabelFormat: xsd:int, Incoterms: xsd:int, CustomsValue: xsd:decimal, CustomsCurrency: xsd:int,
            ProcessingType: xsd:string, Stacking: xsd:boolean, LoadType: xsd:int, IsADR: xsd:boolean, ADRClass: xsd:int, PackageGroup: xsd:int, NrUN: xsd:int,
            ADRWeight: xsd:decimal, Products: ns0:ArrayOfProduct) -> CreateOrderResult: ns0:Order
CreateOrderWithProducts(login: xsd:string, password: xsd:string, request: ns0:CreateOrderRequest) -> CreateOrderWithProductsResult: ns0:Order
GetAvailableServices(login: xsd:string, password: xsd:string, ServiceType: xsd:int, Payer: ns0:Location, pickupLocation: ns0:Location,
                     deliveryLocation: ns0:Location, ReadyDate: xsd:dateTime, Parcels: ns0:ArrayOfParcel, cod: ns0:COD, ROD: xsd:boolean,
                     InsuranceAmount: xsd:decimal, rebateCoupon: xsd:string) -> GetAvailableServicesResult: ns0:GetAvailableServicesResponse
GetLabel(login: xsd:string, password: xsd:string, packageNo: xsd:string, RequestLabelFormat: xsd:int) -> GetLabelResult: xsd:base64Binary
GetManifestPDF(login: xsd:string, password: xsd:string, LetterNumbers: ns0:ArrayOfString) -> GetManifestPDFResult: ns0:Manifest
GetTracking(Login: xsd:string, Password: xsd:string, LetterNo: xsd:string) -> GetTrackingResult: ns0:Tracking
"""
import datetime
import enum
import logging
from dataclasses import (
    asdict,
    dataclass,
)
from decimal import Decimal
from typing import (
    List,
    Optional,
)

import zeep
from lxml import etree
from warrant import (
    snake_to_camel,
    camel_to_snake,
)

from gwio.environment import Env

logger = logging.getLogger(__name__)


# Helper superclass for getting a dataclass as a dict with keys in PascalCase
@dataclass
class XpressAPIDataClass:
    """
    Subclasses *MUST* be named identical to the WSDL ns0 datatypes, trailing underscores will be stripped.
    """

    def asdict(self):
        return {snake_to_camel(k): v for k, v in asdict(self).items()}

    def as_ds(self, client):
        type_name = self.__class__.__name__.lstrip("_")
        ds_type = client.get_type(f'ns0:{type_name}')
        return ds_type(**self.asdict())


@dataclass(frozen=True, eq=True)
class XpressResponseDataClass:
    @classmethod
    def from_zeep_object(cls, zeep_object):
        var_names = dir(zeep_object)
        params = dict()
        for var_name in var_names:
            params[camel_to_snake(var_name)] = getattr(zeep_object, var_name)
        return cls(**params)


# Status codes
_STATUSES = {
    4: ('DELIVERED', 'DORĘCZONO DO ODBIORCY'),
    5: ('CANCELED', 'ANULOWANO ZLECENIE BEZ PODJAZDU KURIERA'),
    6: ('BRAK PRZESYŁKI', 'NO PACKAGE TO PICK UP'),
    148: ("PACZKA ZAREJESTROWANA, ALE NIE MOŻNA JEJ ODNALEŹĆ", "PACKAGE RECORDED BUT CANNOT BE FOUND"),
    5158: ("ZLECENIE ZAKOŃCZONE WYDARZENIEM WYJĄTKOWYM", "ORDER FINISHED WITH EXCEPTION"),
    5168: ("ZAKOŃCZONO PROCESOWANIE RĘCZNIE", "ORDER ENDED MANUALLY"),
    5169: ("PRZESYŁKA ZOSTAŁA ZUTYLIZOWANA", "THE PACKAGE WAS UTILISED"),
}

STATUSES = {k: dict(en=v[0], pl=v[1]) for k, v in _STATUSES.items()}
_ERRORS = {
    12001: ("Cancel possible only by phone!",
            "Anulowanie zlecenia możliwe jedynie drogą telefoniczną!"),
    12002: ("Ready date parameter must be greater or equal to current timestamp!",
            "Czas gotowości nie może być ustawiony wcześniej niż godzina i data w tej chwili!"),
    12003: ("Bad or missing pickup data!",
            "Brak danych dot. nadania lub dane niepoprawne!"),
    12004: ("Bad or missing delivery data!",
            "Brak danych dot. doręczenia lub dane niepoprawne!"),
    12005: ("Bad or missing Parcels data!",
            "Brak danych dot. paczek lub dane niepoprawne!"),
    12006: ("Bad or missing pickup country code!",
            "Brak kodu kraju nadania paczki lub dane niepoprawne!"),
    12007: ("Bad or missing delivery country code!",
            "Brak kodu kraju doręczenia paczki lub dane niepoprawne!"),
    12008: ("Max. COD amount is 10 000!",
            "Największa dopuszczalna kwota pobrania wynosi 10 000!"),
    12009: ("Max. insurance amount is 50 000!",
            "Największa dopuszczalna kwota ubezpieczenia wynosi 50 000!"),
    12010: ("Max. palette weight is 700kg!",
            "Największa dopuszczalna waga palety wynosi 700kg!"),
    12011: ("Max. package weight is 50kg!",
            "Największa dopuszczalna waga paczki wynosi 50kg!"),
    12012: ("Max. package length is 120!",
            "Największa dopuszczalna długość paczki wynosi 120!"),
    12013: ("Oversize!",
            "Przekroczone maksymalne dopuszczalne wymiary!"),
    12014: ("Overweight!",
            "Przekroczona maksymalna dopuszczalna waga!"),
    12015: ("Bad service type specified!",
            "Wybrano niepoprawny typ usługi!"),
    12016: ("Missing COD bank account!",
            "Brak konta do zwrotu pobrania!"),
    12017: ("Auth failed!",
            "Niepoprawne dane logowania!"),
    12018: ("Account locked!",
            "Konto zablokowane!"),
    12019: ("Service unavailable!",
            "Usługa niedostępna!"),
    12020: ("Bad payer!",
            "Niepoprawny płatnik!"),
    12021: ("Service unavailable on specified readydate (should be between: (8:00 and 14:00)",
            "Usługa niedostępna dla wybranego czasu gotowości możliwe w godzinach 8:00-14:00)"),
    12022: ("Service unavailable on specified readydate (should be between 8:00 and 17:00)",
            "Usługa niedostępna dla wybranego czasu gotowości (możliwe w godzinach 8:00-17:00)"),
    12023: ("Service unavailable on specified readydate (should be between 8:00 and 15:00)",
            "Usługa niedostępna dla wybranego czasu gotowości (możliwe w godzinach 8:00-15:00)"),
    12024: ("Service unavailable on specified readydate (should be between 8:00 and 15:00)",
            "Usługa niedostępna dla wybranego czasu gotowości (możliwe w godzinach 8:00-15:00)"),
    12025: ("Cant check service availabilty",
            "Nie można zweryfikować dostępności usługi"),
    12026: ("The selected service is not available!",
            "Wybrana usługa jest niedostępna!"),
    12027: ("Pickup address is incorrect!",
            "Niepoprawny adres nadania!"),
    12028: ("Delivery address is incorrect!",
            "Niepoprawny adres doręczenia!"),
    12029: ("Bad country code!",
            "Niepoprawny kod kraju!"),
}
ERRORS = {k: dict(en=v[0], pl=v[1]) for k, v in _ERRORS.items()}

# Special field names that do are not pascalcase

FIELD_NAME_MAPPING = {
    "cod": "cod",
    "cost_centre": "MPK",
    "login": "login",
    "password": "password",
    "delivery_location": "deliveryLocation",
    "pickup_location": "pickupLocation",
    "pickup_deadline": "pickupDeadline",
    "additional_info": "additionalInfo",
    "rebate_coupon": "rebateCoupon",
    "return_of_documents": "ROD",
    "order_id": "orderNo",
    "service_id": "ServiceID",
}


class XpressAPIException(Exception):

    def __init__(self, error_code):
        try:
            message = ERRORS['en'][error_code]
        except KeyError:
            message = f"Unknown error, code {error_code}"
        super().__init__(message)


# Magic integer values from PDF


class ServiceType(enum.IntEnum):
    LOCAL = 1
    DOMESTIC = 2
    INTERNATIONAL = 23
    SUBURBAN = 25
    ECOMMERCE = 5027


class ServiceId(enum.IntEnum):
    CROSSBORDER_EU = 6520
    CROSSBORDER_NON_EU = 6521
    DOMESTIC_NA_KM = K_NA_KM = 92
    DOMESTIC_PS = K_PS = 89
    DOMESTIC_SXP = K_SXP = 91
    DOMESTIC_XP = K_XP = 91
    DOMESTIC_PALLET = PALETA = 5116
    DOMESTIC_N_08_18 = PKP_N_08_18 = 106
    DOMESTIC_N_18_08 = PKP_N_18_08 = 107
    DOMESTIC_NO_08_18 = PKP_NO_08_18 = 109
    DOMESTIC_NO_18_08 = PKP_NO_18_08 = 110
    DOMESTIC_O_08_18 = PKP_O_08_18 = 111
    DOMESTIC_O_18_08 = PKP_O_18_08 = 112
    EU_ROAD = Z_EU_PS_DROGOWY = 93
    EU_LOT = Z_EU_PS_LOT = 99
    EU_SXP = Z_EU_SXP = 100
    EU_XP = Z_EU_XP = 101
    EU_SKYSAVER = Z_EU_SKYSAVER = 5141
    LOCAL_DUTY = L_DYZUR = 84
    LOCAL_N_PREMIUM = L_N_PREM = 6262
    LOCAL_N_PS = L_N_PS = 6260
    LOCAL_N_ZP = L_N_XP = 6261
    LOCAL_PALLET = L_PALETA = 7000
    LOCAL_PREMIUM = L_PREM = 83
    LOCAL_PS = L_PS = 81
    LOCAL_DAILY = L_SERWIS_DZIENNY = 83
    LOCAL_HOLIDAY = L_SERWIS_SWIAECZNY = 5131
    LOCAL_XPRESS = L_XP = 82
    NON_EU_ROAD = Z_NON_EU_PS_DROGOWY = 102
    NON_EU_LOT = Z_NON_EU_PS_LOT = 103
    NON_EU_SXP = Z_NON_EU_SXP = 104
    NON_EU_XP = Z_NON_EU_XP = 105
    NON_EU_SKYSAVER = Z_NON_EU_SKYSAVER = 5142
    SUBURBAN_DAILY = W_DUZYR = 97
    SUBURBAN_PALLET = W_PALETA = 7001
    SUBURBAN_PRM = W_PRM = 98
    SUBURBAN_XP = W_XP = 96
    XPRESS_PLUS = 6118
    XPRESS_REGIONAL = XPRESS_VOIVODESHIP = XPRESS_WOJEW = 6119
    XPRESS_CITY = 6117
    XPRESS_LOCAL_PALLET = PALETA_LOKALNA = 6122
    ECOMMERCE_NA_KM = _K_NA_KM = 6120
    SAME_DAY_10_14 = 6201
    SAME_DAY_14_17 = 6202
    SAME_DAY_18_22 = 6203
    NEXT_DAY_10_14 = 5127
    NEXT_DAY_14_17 = 5128
    NEXT_DAY_18_22 = 5129


class LabelFormat(enum.IntEnum):
    A4 = 0
    _10X15 = 1
    _10X10 = 2
    ZPL = 3
    EPL = 4


class IncoTermsType(enum.IntEnum):
    EXW = 1
    FCA = 2
    FAS = 3
    FOB = 4
    CFR = 5
    CIF = 6
    CPT = 7
    CIP = 8
    DAS = 9
    DES = 10
    DEQ = 11
    DDU = 12
    DDP = 13
    DAP = 14
    DAT = 15


class CustomsCurrency(enum.IntEnum):
    PLN = 1
    EUR = 19
    USD = 23
    GBP = 5023
    UAH = 5025
    CZK = 5024


class ProcessingType(enum.IntEnum):
    STANDARD = 0
    E_COMMERCE_WITH_PICKUP_AND_WITH_CUSTOMS_CLEARANCE = 1
    E_COMMERCE_WITHOUT_PICKUP_AND_WITH_CUSTOMS_CLEARANCE = 2
    E_COMMERCE_WITH_PICKUP_AND_WITHOUT_CUSTOMS_CLEARANCE = 3
    E_COMMERCE_WITHOUT_PICKUP_AND_WITHOUT_CUSTOMS_CLEARANCE = 4


# Data classes
@dataclass(frozen=True, eq=True)
class Service(XpressResponseDataClass):
    id: int
    max_delivery_time: datetime.datetime
    max_pickup_time: datetime.datetime
    name: str
    price_with_tax: Decimal
    price_without_tax: Decimal
    tax: Decimal
    price_elements: Optional[object] = None  # TODO: What is this?


@dataclass(frozen=True, eq=True)
class OrderResponse(XpressResponseDataClass):
    no: str
    letter_no: str
    mime_data: bytes
    order_price: Decimal
    ready_date: datetime.datetime
    price_elements: Optional[object] = None  # TODO: What is this?


@dataclass
class Location(XpressAPIDataClass):
    name: str
    address: str
    city: str
    post_code: str
    country_code: str
    is_private_person: bool = None
    client_no: str = None
    client_tax_id: str = None
    e_mail: str = None
    info: str = None
    person: str = None
    contact: str = None


@dataclass
class COD(XpressAPIDataClass):
    amount: Decimal
    ret_account_no: Optional[str] = None


@dataclass
class Price(XpressAPIDataClass):
    price_item: str
    item_price: Decimal


@dataclass
class Product(XpressAPIDataClass):
    """Xpresscouriers Product"""
    PackageNo: str
    Amount: int
    UnitPrice: Decimal
    Currency: str
    SourceCountryCode: str
    Description: str
    LocalDescription: str
    ExportPurpose: str
    CustomsCode: str
    NetWeight: Decimal
    GrossWeight: Decimal
    MeasureUnit: str
    LocalMeasureUnit: str
    ProductURL: str
    ProductSKU: str
    Reference1: str
    Content: str
    DocsNonDocs: object  # TODO: What is ns0:DocsNonDocsType
    LvHv: object  # TODO: What is ns0:LvHvType
    InvoiceNumber: str

    def asdict(self) -> dict:
        return asdict(self)

    def as_ds(self, client):
        product = client.get_type('ns0:Product')
        return product(**self.asdict())


@dataclass
class Parcel(XpressAPIDataClass):
    """
    Parcel dimensions in centimeters and and kilograms
    """

    class Type(enum.Enum):
        ENVELOPE = "Envelope"
        PACKAGE = "Package"
        PALLET = "Pallet"

    type: Type
    weight: Decimal
    length: Decimal
    heigth: Decimal
    width: Decimal
    products: Optional[List[Product]] = None

    def asdict(self):
        return {
            'Type': self.type.value,
            'Weight': self.weight,
            'D': self.length,
            'W': self.heigth,
            'S': self.width,
            'Products': self.products,
        }

    def as_ds(self, client):
        parcel = client.get_type('ns0:Parcel')
        return parcel(**self.asdict())


class LogisticsAPI:
    def __init__(self, url=None, debug=False):
        plugins = list()
        self.debug = debug
        if url is None:
            wsdl_url = Env.XPRESS_COURIERS_API_URL
        else:
            wsdl_url = url
        if self.debug:
            from zeep.plugins import HistoryPlugin
            self.history = HistoryPlugin()
            plugins.append(self.history)
        else:
            self.history = None

        self.client = zeep.Client(wsdl_url, plugins=plugins)
        self.factory = self.client.type_factory('ns0')
        self._username = Env.XPRESS_COURIERS_API_USERNAME
        self._password = Env.XPRESS_COURIERS_API_PASSWORD

    def _translate_arguments(self, kwargs):
        """
        Converts keys to PascalCase and calls as_ds() on values.
        """
        ret = dict()
        for k, v in kwargs.items():
            if v is None:
                continue
            try:
                key = FIELD_NAME_MAPPING[k]
            except KeyError:
                key = snake_to_camel(k)
            try:  # XpressAPIDataClass
                value = v.as_ds(self.client)
            except AttributeError:
                try:  # enum.IntEnum
                    value = v.value
                except AttributeError:
                    value = v
            ret[key] = value
        return ret

    def _call_api(self, op_name, **kwargs):
        """
        Converts arguments and calls the actual zeep function.
        """
        api_function = self.client.service[op_name]
        api_args = self._translate_arguments(kwargs)
        try:
            return api_function(login=self._username, password=self._password, **api_args)
        except zeep.exceptions.Fault:
            if self.debug:
                for data in (self.history.last_received, self.history.last_sent):
                    text = etree.tostring(data['envelope'], pretty_print=True)
                    logger.error(text)
            raise
        except Exception as e:
            print(e)
            raise

    def cancel_order(self, order_no):
        return self._call_api('CancelOrder', order_no=order_no)

    def _make_product_array(self, items):
        return self.factory.ArrayOfProduct(Product=[self.factory.Product(**p.asdict()) for p in items])

    def create_order(
            self, *,
            payer: Location,
            service_type: ServiceType,
            service_id: ServiceId,
            pickup_location: Location,
            delivery_location: Location,
            parcels: List[Parcel],
            ready_date: datetime.datetime = None,
            pickup_deadline: datetime.datetime = None,
            cash_on_delivery: COD = None,
            return_of_documents: bool = False,
            saturday_delivery: bool = None,
            insurance_amount: Decimal = None,
            cost_centre: str = None,
            info: str = None,
            description: str = None,
            coupon: str = None,
            label_format: LabelFormat = LabelFormat.A4,
            incoterms: IncoTermsType = None,
            value: Decimal = None,
            currency: CustomsCurrency = None,
            stacking: bool = None,
            processing_type: ProcessingType = None,
            is_adr: bool = None,
            package_group: int = None,
            un_number: int = None,
            products: List[Product] = None) -> OrderResponse:
        """
        :param payer: Location object containing information about the payer
        :param service_type: ServiceType object
        :param service_id: Id of the service required
        :param pickup_location: Location object containing information about the pick up location
        :param delivery_location: Location object containing information about the pick up location
        :param parcels: Table of Parcel objects containing information about the shipment content
        :param ready_date: Date indicating when the package is ready to be picked up.
                           If the value won't be set, it'll be automatically set to next available pickup moment (only API003)
        :param pickup_deadline: Pickup required to happen not later than
        :param cash_on_delivery: COD object containing information about COD payment
        :param return_of_documents: If true ROD will be processed
        :param saturday_delivery: If true support delivery on Saturday. Saturday delivery is not available for services in the API002/API003.
        :param insurance_amount: Amount the order is insured for TODO: Is it really so?
        :param cost_centre: TODO: Whose cost center?
        :param info: Additional information. Shipment attentions TODO: For who?
        :param description: Package content description TODO: For who?
        :param coupon: TODO: Reference?
        :param label_format: LabelFormat object describing the format of the shipping label
        :param incoterms: TODO: ?
        :param value: Customs value
        :param currency: Customs value currency as CustomsCurrency object
        :param stacking: True if the order is stackable pallet TODO: order?
        :param is_adr: Is the order ADR
        :param package_group: TODO: ?
        :param un_number: TODO: ?
        :param products: List of products in the order
        :param processing_type: TODO: ?
        :return: `OrderResponse`
        """
        if products is None:
            products = []

        response = self._call_api(
            'CreateOrder',
            payer=self.factory.Location(**payer.asdict()),
            service_type=service_type,
            service_id=service_id.value,
            pickup_location=self.factory.Location(**pickup_location.asdict()),
            delivery_location=self.factory.Location(**delivery_location.asdict()),
            ready_date=ready_date,
            pickup_deadline=pickup_deadline,
            parcels=self._make_parcel_array(parcels),
            cod=cash_on_delivery,
            return_of_documents=return_of_documents,
            saturday_delivery=saturday_delivery,
            insurance_amount=insurance_amount,
            cost_centre=cost_centre,
            additional_info=info,
            package_content_desrc=description,
            rebate_coupon=coupon,
            request_label_format=label_format.value,
            incoterms=incoterms,
            customs_value=value,
            customs_currency=currency,
            stacking=stacking,
            processing_type=processing_type,
            is_adr=is_adr,
            package_group=package_group,
            nr_un=un_number,
            products=self._make_product_array(products),
        )
        return OrderResponse.from_zeep_object(response)

    def _make_parcel_array(self, items):
        return self.factory.ArrayOfParcel(Parcel=[self.factory.Parcel(**p.asdict()) for p in items])

    def get_available_services(
            self, *,
            service_type: ServiceType,
            payer: Location,
            pickup_location: Location,
            delivery_location: Location,
            parcels: List[Parcel],
            cash_on_delivery: COD,
            ready_date: datetime.datetime = None,
            insurance_amount: Decimal = None,
            coupon: str = None):
        response = self._call_api(
            'GetAvailableServices',
            service_type=service_type,
            payer=payer,
            pickup_location=pickup_location,
            delivery_location=delivery_location,
            parcels=self._make_parcel_array(parcels),
            ready_date=ready_date,
            insurance_amount=insurance_amount,
            coupon=coupon,
            cod=cash_on_delivery,
            return_of_documents=False,
        )
        try:
            services = dict(available_services=[],
                            next_working_day_services=[])
            if response.AvailableServices is not None:
                services['available_services'] = [Service.from_zeep_object(service) for service in response.AvailableServices.Service]
            if response.NextWorkingDayServices is not None:
                services['next_working_day_services'] = [Service.from_zeep_object(service) for service in response.NextWorkingDayServices.Service]
        except AttributeError:
            # Sometimes the response does not have AvailableServices and NextWorkingDayServices ¯\_(ツ)_/¯
            services = [Service.from_zeep_object(service) for service in response]

        return services

    def get_tracking(self, order_id: str):
        return self._call_api("GetTracking", letter_no=order_id)

    def get_manifest(self, *, label_id: str):
        return self._call_api("GetManifestPDF", letter_numbers=label_id)

    def get_label(self, label_id: str, label_format: LabelFormat = LabelFormat.A4):
        return self._call_api("GetLabel", package_no=label_id, request_label_format=label_format)
