# coding=utf-8
"""Contains wrapper for needed Cognito commands."""
import logging
import uuid

from warrant import cognito_to_dict

import gwio.user
from gwio.utils.strings import camel_to_snake

CoreUser = gwio.user.User

logger = logging.getLogger(__name__)


# pylint: disable=R0903, R0902
# There is no need for more public methods.
# This many instance variables are to store the expected information from Cognito.
class User:
    """User-object based on Cognito data."""

    @classmethod
    def from_cognito(cls, user_dict):
        """Creates User-object from Cognito Identity JWT."""
        attr_map = dict(
            organization_guid='custom:organization_guid',
            created_at='user_create_date',
            updated_at='user_last_modified_date',
            status='user_status',
            id='sub'
        )
        user_attributes = cognito_to_dict(user_dict['Attributes'])
        for field in ('Enabled', 'UserCreateDate', 'UserLastModifiedDate', 'UserStatus', 'Username'):
            user_attributes[camel_to_snake(field)] = user_dict[field]
        for new_key, old_key in attr_map.items():
            user_attributes[new_key] = user_attributes.pop(old_key, None)
        return cls(**user_attributes)

    # noinspection PyShadowingBuiltins
    # pylint: disable=W0622
    # "id" intentionally redefined from built-in "id".
    def __init__(self, *,
                 organization_guid=None,
                 email=None,
                 email_verified=None,
                 enabled=None,
                 name=None,
                 phone_number=None,
                 phone_number_verified=None,
                 id=None,
                 created_at=None,
                 updated_at=None,
                 status=None,
                 username=None,
                 **_):
        try:
            self.organization_guid = uuid.UUID(organization_guid)
        except TypeError:
            logger.debug('organization_guid {} for user {} <{}> is invalid'.format(organization_guid, name, email))
            self.organization_guid = None
        self.email = email
        self.email_verified = email_verified
        self.enabled = enabled
        self.name = name
        self.phone_number = phone_number
        self.phone_number_verified = phone_number_verified
        self.id = id
        self.created_at = created_at
        self.updated_at = updated_at
        self.status = status
        self.username = username
        try:
            self.identity_id = CoreUser(uuid.UUID(self.id)).identity_id
        except AttributeError:
            logger.warning('Non-uuid user id: %s', self.id)
            self.identity_id = None
