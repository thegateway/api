from gwio.core.decorators.api.authenticate import authorise
from .api import (
    AuditObject,
    DefaultSchema,
    endpoint,
)
from .http import Method

DELETE = Method.DELETE
GET = Method.GET
POST = Method.POST
PUT = Method.PUT

__all__ = [
    'AuditObject',
    'authorise',
    'DefaultSchema',
    'DELETE',
    'endpoint',
    'GET',
    'POST',
    'PUT',
]
