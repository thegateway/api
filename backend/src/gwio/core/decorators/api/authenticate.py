from werkzeug.exceptions import Forbidden


def authorise(*roles, guid_name):
    """
    Helper function for concisely defining endpoint authorization for list of roles when needing to pass a guid to the auth functions

    :param roles:
        Iterable of authorization functions (rbac.customer, rbac.customer, etc.)
    :param guid_name:
        The name the guid, that should be given to the authorization functions, is found with in the kwargs
    :return:
    """

    doc = f"""Allowed roles: {', '.join(x.__name__ for x in roles)}"""

    def auth_fn(**kwargs):
        if not any(rbac(kwargs[guid_name]) for rbac in roles):
            raise Forbidden

    auth_fn.__doc__ = doc

    return auth_fn
