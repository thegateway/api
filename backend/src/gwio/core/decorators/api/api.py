import inspect
import logging
from enum import Enum
from functools import wraps
from typing import (
    Callable,
    Iterable,
    Tuple,
    Union,
)
from uuid import UUID

import stringcase
from attrdict import AttrDict
from dynamorm.indexes import Index
from flask import g
from flask_apispec import (
    marshal_with,
    use_kwargs,
)
from werkzeug.exceptions import (
    Conflict,
    Forbidden,
    NotFound,
    Unauthorized,
)

import gwio.user
from gwio.core import (
    cw_logging,
    rbac,
)
from gwio.core.decorators.api.http import Method

User = gwio.user.User
LogEventType = cw_logging.LogEventType
AuditRequestsUtil = cw_logging.AuditRequestsUtil

logger = logging.getLogger(__name__)


class DefaultSchema(Enum):
    AUTOMATIC = 'Get the schema from provided class'


class AuditObject(Enum):
    SELF = 'self'  # Will use `g.user` as the object of the request
    UPLOAD = 'uploads'


def _pop_attributes(*, keys_to_remove: Iterable, values: dict) -> dict:
    """Helper for removing the consumed values from the input values"""
    popped = dict()
    for k in keys_to_remove:
        try:
            popped[k] = values.pop(k)
        except KeyError:
            pass
    return popped


def _find_arguments_of_func(*, func, klass):
    """
    Helper to figure out what paremeters should be given to the function.

    :param func:
        The target function
    :param klass:
        The object of the request
    :return:
        Set of argument names that the provided function accepts. If function takes any kwargs, then the `klass` attributes will be included.
    """
    fullargsspec = inspect.getfullargspec(func)
    varkws = []
    if fullargsspec.varkw:
        # If function takes in just anything, the assumtion is that it can and should handle all of the request object's attributes
        # Getting Dynamorm model's attributes
        varkws = list(klass.Schema._declared_fields.keys())
    return set(fullargsspec.args + fullargsspec.kwonlyargs + varkws)


def _audit_setup(
        klass,
        event_type: LogEventType,
        extra_kwargs: dict,
        **_
) -> Callable:
    """
    The decorator for setting up audit log event information before even checking the cache for response

    :param klass:
        The resource that is the target of the audit
    :param event_type:
        The audit event type
    :param extra_kwargs:
        Hard coded values that will be treated like they were request values. Used in querying and updating the target.
    """

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            kwargs.update(extra_kwargs)

            if not g.log_events_util.audit_disabled and klass:
                if event_type:
                    g.log_events_util.audit_event.event_type = event_type

                g.log_events_util.audit_event.object_type = klass.name if isinstance(klass, AuditObject) else klass.__name__

            return func(*args, **kwargs)

        return wrapper

    return decorator


def _audit_uncached(
        klass,
        self_target: bool,
        **_
) -> Callable:
    """
    The decorator for filling audit log event with more information when the request wasn't handled by cache

    :param klass:
        The resource that is the target of the audit
    :param self_target:
        Tells if the audit target is the requester
    """

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not g.log_events_util.audit_disabled and klass:
                if self_target:
                    g.log_events_util.audit_object = g.user

                # Create and list reguests (for example) do not have guid
                try:
                    try:
                        g.log_events_util.audit_event.object_id = kwargs[klass.Table.identifier]
                    except AttributeError:
                        g.log_events_util.audit_event.object_id = kwargs['guid']
                except KeyError:
                    pass

            return func(*args, **kwargs)

        return wrapper

    return decorator


def _check_authorisation(
        authorisation: Union[None, Iterable, Callable],
        self_target: bool,
        snakecase_cls_name: str = None,
        **_
) -> Callable:
    """
    The decorator for checking the requester is sufficiently authenticated and authorized for the request

    :param authorisation:
        An iterable of `rbac` roles, a callable that raises an exception, or None (for no authentication required).
    :param self_target:
        Tells if the audit target is the requester
    :param snakecase_cls_name:
        The target resource name in snakecase
    """

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            if authorisation is not None:
                if g.user is None:
                    raise Unauthorized  # Maybe some other error?

                if callable(authorisation):
                    auth_kwargs = kwargs.copy()
                    if self_target:
                        auth_kwargs[User.__name__.lower()] = g.user
                    if 'guid' in auth_kwargs:
                        # This is just for the sake readability of the authorization function.
                        # It's clearer to define `webshop_guid` as an argument than just `guid`
                        auth_kwargs[f'{snakecase_cls_name}_guid'] = auth_kwargs.pop('guid')
                    authorisation(**auth_kwargs)
                else:
                    assert authorisation, 'Use `rbac.ALL` to define public endpoint instead of empty iterable'
                    if not any(f() for f in authorisation):
                        raise Forbidden
            return func(*args, **kwargs)

        return wrapper

    return decorator


def _create(*, klass, index, automodify, input_values, **_):
    """
    Initialises a new instance of the target model.

    :param klass:
        The target model.
    :param index:
        Function to use to initialise klass instead of directly initialising
    :param automodify:
        Whether to initialise the model with the input values or not.
    :param input_values:
        The input values deserialised from  the request url and body.
    :return:
        Initialised target model
    """

    if automodify:
        if index:
            func_arguments = _find_arguments_of_func(func=index, klass=klass)
            return index(**_pop_attributes(keys_to_remove=func_arguments, values=input_values))
        # Getting Dynamorm model's attributes
        attributes = klass.Schema._declared_fields.keys()

        return klass(**_pop_attributes(keys_to_remove=attributes, values=input_values))
    return klass()


def _read(
        *,
        klass,
        self_target: bool,
        index: Tuple[callable, Index],
        query: bool,
        catch_not_found: bool, query_params: dict,
        input_values: dict,
        **_
):
    """
    Loads the requested resource.

    :param klass:
        The target model
    :param self_target:
        Tells if the target is the requester
    :param index:
        A function, method, or Index to be used to query the object(s)
    :param query:
        Used to define that the default object fetching should be done with `klass.query()` instead of `klass.get()` to get a list instead of an object.
        Does not do anything if `index` is used.
    :param catch_not_found:
        Catch object not found and return None
    :param query_params:
        Used along the `input_values` in querying for the object
    :param input_values:
        The input values deserialised from  the request url and body.
    :return:
        Instance of the target model
    """
    if self_target:
        return g.user

    # User is a special case since it's not a resource stored in the database
    if klass is User:
        try:
            try:
                user = User.by_identity_id(input_values.pop('identity_guid'))
            except KeyError:
                user = User.by_identity_id(input_values.pop('identity_id'))
            if user is None and not catch_not_found:
                raise NotFound
            return user
        except KeyError as e:
            raise Conflict('Either `identity_guid` or `identity_id` required') from e

    target = index if index is not None else klass.Table

    # Get the names of the primary key field(s) (partition key [+ range key])
    if inspect.ismethod(target):
        key_word_args = _find_arguments_of_func(func=target.__func__, klass=klass)
        primary_key = [x for x in key_word_args if x != 'klass']
    else:
        primary_key = [target.hash_key]
        try:
            if target.range_key is not None:
                primary_key.append(target.range_key)
        except AttributeError:
            # No range key
            pass

    # Make sure the primary key components are not in request data AND `query_params`
    assert not (set(input_values.keys()) & set(query_params.keys()) & set(
        primary_key)), 'primary key from multiple sources'
    get_primary_keys = set(primary_key) - set(query_params.keys())
    get_params = {x: input_values.pop(x) for x in get_primary_keys if x in input_values}
    get_params.update(query_params)

    get_params = {k: v for k, v in get_params.items() if v is not None}

    # Values with key that contain double underscore in middle of it are assumed to being Dynamorm query parameters
    for k, v in dict(**input_values).items():
        try:
            if k.index('__') > 0:
                get_params[k] = input_values.pop(k)
        except ValueError:
            pass

    try:
        # Trying to first call the target since that'd be the simplest case
        obj = target(**get_params)
    except TypeError:
        # Target wasn't a callable, so it must be queried
        try:
            # Dynamorm (doesn't support UUIDs)
            obj = index.query(**{k: str(v) if isinstance(v, UUID) else v for k, v in get_params.items()})
        except TypeError:
            # No range key so no need for any comparison weirdness
            obj = index.query(**get_params)
        except AttributeError:
            # Target wasn't queryable, so it must have just been dynamorm Table, which means klass must be used
            if query:
                obj = klass.query(**get_params)
            else:
                try:
                    obj = klass.get(**get_params)
                except klass.DoesNotExist:
                    obj = None
    except klass.DoesNotExist:
        obj = None

    # TODO: Remove after dynamodb has been refactored to raise NotFound
    if obj is None and not catch_not_found:
        if catch_not_found:
            # Adding back the parameters that were popped from `input_values` so they don't just vanish
            for k, v in get_params.items():
                input_values[k] = v
        else:
            raise NotFound

    return obj


def _update(*, automodify: bool, input_values: dict, **params):
    """
    Loads the target object and modifies it if necessary.

    :param automodify:
        Tells whether to modify the object or leave that for the endpoint function.
    :param input_values:
        The input values deserialised from  the request url and body.
    :param params:
        The rest of the params from the endpoint decorator
    :return:
        The updated (if necessary) target object
    """
    # pylint: disable=missing-kwoa
    obj = _read(
        automodify=automodify,
        input_values=input_values,
        **params,
    )

    if automodify and obj is not None:
        # Getting Dynamorm model's attributes
        attributes = obj.Schema._declared_fields.keys()
        for k, v in _pop_attributes(keys_to_remove=attributes, values=input_values).items():
            setattr(obj, k, v)
    return obj


def _delete(**params):
    """
    Always catches not found because not finding record that is going to be deleted is not a problem
    :return:
        Target object or None
    """
    params['catch_not_found'] = True
    return _read(**params)


# The object fetching (and possible modifying) logic based on the request event type
_load_object_logic_funcs = {
    LogEventType.CREATE: _create,
    LogEventType.READ: _read,
    LogEventType.LIST: _read,
    LogEventType.UPDATE: _update,
    LogEventType.DELETE: _delete,
    LogEventType.LOGIN: _read,
    LogEventType.INVITE: _read,
}


def _load_object(
        *,
        klass,
        autoload: bool,
        event_type: LogEventType,
        snakecase_cls_name=None,
        **params) -> Callable:
    """
    Loads and modifies (depending on request event type and endpoint definition) the target object
    and adds it to the key word arguments given to the endpoint function.

    :param klass:
        The target model
    :param autoload:
        Whether to load the instance of the target model or leave that for the endpoint function
    :param event_type:
        The request event type
    :param snakecase_cls_name:
        Snakecase name of the target model
    :param params:
        The rest of the params from the endpoint decorator
    :return:
        The function wrapper
    """

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            # if params.klass is not None and not getattr(params.marshal_schema, 'many', False):
            if klass is not None and event_type is not None:
                if autoload:
                    obj = _load_object_logic_funcs[event_type](
                        klass=klass,
                        input_values=kwargs,
                        **params,
                    )
                    # TODO: How to audit listings?
                    if isinstance(obj, User):
                        g.log_events_util.audit_event.object_id = obj.guid
                    # `obj` might be iterable if the target is a list
                    elif not isinstance(obj, Iterable):
                        g.log_events_util.audit_object = obj

                    if obj is None or isinstance(obj, klass):
                        kwargs[snakecase_cls_name] = obj
                    else:
                        table_name = klass.Table.name

                        # Get the resource name from the table name because that way we'll have grammatically correct name for the variable containing plural
                        key = table_name.split('-')[-1].split('.')[0]
                        kwargs[key] = obj

            return func(*args, **kwargs)

        return wrapper

    return decorator


def endpoint(
        app, klass, *,
        input_schema=DefaultSchema.AUTOMATIC,
        output_schema=DefaultSchema.AUTOMATIC,
        event_type: LogEventType = None,
        route: str = None,
        method: Method = None,
        automodify: bool = True,
        authorisation: Union[None, Iterable, Callable] = (rbac.admin,),
        autoload: bool = True,
        index=None,
        query: bool = False,
        extra_kwargs: dict = None,
        query_params: dict = None,
        description: str = None,
        catch_not_found: bool = False,
        cache_for=None,
        extra_keys=None,
) -> Callable:
    """
    Uses the guid from the kwargs (which should come from the URL) to get a klass object (GET/PUT/DELETE) or initialises a new klass object (POST)

    :param app:
        Flask application
    :param klass:
        A class extending PynamoDb/Dynamorm or AuditObject if request object not a model instance representing a database record.
        Used to define the log stream,
             to target the query of the object if `autoload=True`, and
             as the name of the returned object (unless a list, in which case (the resource part of) the table name is used as the variable name.
        If AuditObject, then object is loaded only if the value is `AuditObject.SELF`
    :param input_schema:
        The Marshmallow schema that is used to load the request parameters.
        No input if `None`.
        Defaults to `None` on `GET` and `DELETE` requests.
    :param output_schema:
        The marshmallow schema used to dump the return value from the endpoint.
        No dump if `None`.
        Defaults to `None` on `DELETE` request.
    :param event_type:
        The audit log event type. Defaults to one that matches the HTTP method of the request.
    :param route:
        The route that the endpoint function can be reached with.
    :param method:
        The HTTP method the endpoint function can be reached with. Only required if `route` is given.
    :param automodify:
        Defines that the object should be modified by the decorator.
        Removes the arguments that are used to update the object from the request parameters so that they are not returned to the endpoint function.
        Has no effect if `autoload=False`
    :param authorisation:
        An iterable of `rbac` roles, a callable that raises an exception, or None (for no authentication required).
        If callable, then the callable must have a docstring so that it can be appended to the endpoint description
        to inform the user of the authorization requirements the endpoint has.
    :param autoload:
        Defines that the object should be loaded and returned.
        If object is loaded, the arguments used in the query will be removed from the request parameters so they won't be returned to the endpoint function.
    :param index:
        A function, method, or Index to be used to query the object(s)
    :param query:
        Used to define that the default object fetching should be done with `klass.query()` instead of `klass.get()` to get a list instead of an object.
        Does not do anything if `index` is used.
    :param extra_kwargs:
        Added to the request parameters so they are available when querying the object,
        but will be returned if not removed by any of the decorators #TODO: Can maybe be removed?
    :param query_params:
        Used along the request parameters in querying for the object, but not added to the request parameters
    :param description:
        Can be used to override the endpoint description from the function docstring
    :param catch_not_found:
        Catch object not found and return None
    :return:
    """

    def decorator(func: Callable) -> Callable:
        if klass is None:
            assert route, 'Can not disable audit for endpoint without defining route. If a view method being called, the logic here needs to be updated.'
            AuditRequestsUtil.endpoints_without_audit.append(route)

        if method is None:
            try:
                # `method` wasn't Enum so assumption is that the endpoint function is a method view...
                http_method = Method(func.__name__)
            except ValueError:
                # not a method view. Possibly missing the method
                raise ValueError('Cannot parse HTTP method from parameters or method name')
        else:
            http_method = method

        self_target = klass == AuditObject.SELF
        params = AttrDict(
            klass=User if self_target else klass,
            event_type=event_type if event_type is not None else AuditRequestsUtil.default_audit_event_type_from_crud[
                http_method.name],
            autoload=autoload,
            authorisation=authorisation,
            index=index,
            query=query,
            automodify=automodify,
            extra_kwargs=extra_kwargs if extra_kwargs is not None else dict(),
            self_target=self_target,
            query_params=query_params if query_params is not None else dict(),
            catch_not_found=catch_not_found,
        )

        try:
            params.snakecase_cls_name = params.klass.value if isinstance(params.klass,
                                                                         AuditObject) else stringcase.snakecase(
                params.klass.__name__)
        except AttributeError:
            pass

        # Define the input schema to use
        if input_schema is not None:
            if input_schema is DefaultSchema.AUTOMATIC:
                if http_method.name in ('GET', 'DELETE') or not params.klass:
                    pass
                else:
                    params.input_schema = getattr(params.klass.Schemas, http_method.value.capitalize())
                    # If using the default schema and request is for update, then none of the top level fields are required.
                    # We can not use `partial=True` here because nested schema's required attributes must still be obeyed.
                    if http_method.name in 'PUT':
                        params.input_schema = params.input_schema(partial=[x for x in params.input_schema._declared_fields.keys()])
            else:
                params.input_schema = input_schema

        # Define the output schema to use
        if output_schema is not None:
            if output_schema is DefaultSchema.AUTOMATIC:
                # By default DELETE has no response body so no schema is needed
                if http_method.name != 'DELETE' and params.klass:
                    try:
                        # The new schema structure for models
                        params.marshal_schema = params.klass.Schemas.Get
                    except AttributeError:
                        # The old schema structure
                        # TODO: should be removed once no model uses follows this anymore
                        params.marshal_schema = params.klass.API.marshal
            else:
                params.marshal_schema = output_schema

        if not isinstance(params.klass, AuditObject):
            # Wrap the endpoint function with all the required decorators
            wrapped = _load_object(**params)(func)
        else:
            wrapped = func

        try:
            i_schema = params.input_schema
            wrapped = use_kwargs(i_schema)(wrapped)
        except AttributeError:
            pass

        wrapped = _check_authorisation(**params)(wrapped)
        wrapped = _audit_uncached(**params)(wrapped)
        # `marshal_with` must be wrapped AFTER `use_kwargs` or otherwise `use_kwargs` behaves strange and the request parameters will only be available
        # in the root level function (the API endpoint), instead of the `_load_object` decorator
        try:
            o_schema = params.marshal_schema
            wrapped = marshal_with(o_schema)(wrapped)
        except AttributeError:
            pass
        if cache_for:
            logger.info(f"Enabling caching for {route}, cached results will NOT be audited!")
            wrapped = app.redis_cache(cache_for, extra_keys)(wrapped)

        wrapped = _audit_setup(**params)(wrapped)

        wrapped.__spec__ = params
        params.method = http_method
        if wrapped not in app.doc_endpoints and route is not None:
            app.add_url_rule(route, None, wrapped, methods=[http_method.name], strict_slashes=False)
            app.doc_endpoints[wrapped] = route

        return wrapped

    return decorator
