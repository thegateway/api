import copy
import datetime
import logging
import urllib.parse
from decimal import Decimal
from enum import Enum
from json import JSONDecodeError
from typing import (
    Optional,
    Dict,
)
from uuid import UUID

import requests
import simplejson
from requests import Response

from gwio.environment import Env
from gwio.ext.vat import Currency
from gwio.utils.strings import camel_to_snake

log = logging.getLogger(__name__)


class PayuException(Exception):
    pass


class SubmerchantDoesNotExist(PayuException):
    pass


class PayuOrderType(Enum):
    NORMAL = 'normal'
    MARKETPLACE = 'marketplace'


class PayuOrderStatus(Enum):
    PENDING = 'PENDING'
    WAITING_FOR_CONFIRMATION = 'WAITING_FOR_CONFIRMATION'
    COMPLETED = 'COMPLETED'
    CANCELED = 'CANCELED'


class Payu:
    class Vars:
        def __getattr__(self, attr_name):
            if attr_name in ['PAYU_HOST', 'PAYU_CLIENT_ID', 'PAYU_CLIENT_SECRET', 'PAYU_NOTIFY_URL', 'PAYU_POS_ID', 'PAYU_SHOP_ID']:
                obj_attr_name = f'_{attr_name}'
                if not hasattr(self, obj_attr_name):
                    setattr(self, obj_attr_name, getattr(Env, attr_name))
                return getattr(self, obj_attr_name)
            raise AttributeError(f'{self.__class__.__name__} has no attribute {attr_name}')

        @property
        def PAYU_AUTHORIZE_URL(self):
            return urllib.parse.urljoin(self.PAYU_HOST, '/pl/standard/user/oauth/authorize')

        @property
        def PAYU_ORDERS_URL(self):
            return urllib.parse.urljoin(self.PAYU_HOST, '/api/v2_1/orders/')

        @property
        def PAYU_PAYMENT_METHODS_URL(self):
            return urllib.parse.urljoin(self.PAYU_HOST, '/api/v2_1/paymethods/')

        @property
        def PAYU_SUBMERCHANT_STATUS_URL(self):
            return urllib.parse.urljoin(self.PAYU_HOST, '/api/v2_1/customers/ext/')

        @property
        def PAYU_PAYOUTS_URL(self):
            return urllib.parse.urljoin(self.PAYU_HOST, '/api/v2_1/payouts')

        @property
        def PAYU_SUBMERCHANT_BALANCE_URL(self):
            return urllib.parse.urljoin(self.PAYU_HOST, '/api/v2_1/customers/ext/')

    _VARS = Vars()

    _authorization_token = dict()

    @classmethod
    def _get_authorization_token(cls, credential_type: str = None) -> str:
        if credential_type is None:
            credential_type = 'client_credentials'  # TODO: Other option is 'trusted_merchant' which is needed for token payments
        """Get the authorization token if we don't have one already or if the one we have is expired"""
        assert cls._VARS.PAYU_CLIENT_ID is not None
        assert cls._VARS.PAYU_CLIENT_SECRET is not None
        if (not cls._authorization_token) or cls._authorization_token['expires'] < datetime.datetime.now(tz=datetime.timezone.utc):
            payload = dict(grant_type=credential_type,
                           client_id=cls._VARS.PAYU_CLIENT_ID,
                           client_secret=cls._VARS.PAYU_CLIENT_SECRET)
            response = requests.post(cls._VARS.PAYU_AUTHORIZE_URL, data=payload)
            if not response.ok:
                raise PayuException(f'Failed PayU authentication: {response.status_code} / {response.json()}')
            try:
                resp_data = response.json()
            except (JSONDecodeError, simplejson.JSONDecodeError):
                log.error(cls._response_content(response))
                raise
            # Remove a few seconds to the expiration time to play it safe
            resp_data['expires'] = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(seconds=(resp_data['expires_in'] + 10))
            cls._authorization_token = resp_data
        return cls._authorization_token['access_token']

    @classmethod
    def _get(cls, url: str, *, headers: dict = None, **requests_kwargs) -> Response:
        if headers is None:
            headers = dict()
        params = dict(url=url,
                      headers={'Authorization': f'Bearer {cls._get_authorization_token()}',
                               **headers})
        return requests.get(**params, **requests_kwargs)

    @classmethod
    def _post(cls, url: str, *, body: dict = None, headers: dict = None, **requests_kwargs) -> Response:
        if headers is None:
            headers = dict()
        if body is None:
            body = dict()
        params = dict(url=url,
                      json=body,
                      headers={'Authorization': f'Bearer {cls._get_authorization_token()}',
                               'Content-Type': 'application/json',
                               **headers})
        return requests.post(**params, **requests_kwargs)

    @classmethod
    def _put(cls, url: str, *, body: dict = None, headers: dict = None, **requests_kwargs) -> Response:
        if headers is None:
            headers = dict()
        if body is None:
            body = dict()
        params = dict(url=url,
                      json=body,
                      headers={'Authorization': f'Bearer {cls._get_authorization_token()}',
                               'Content-Type': 'application/json',
                               **headers})
        return requests.put(**params, **requests_kwargs)

    @classmethod
    def _delete(cls, url: str, *, headers: dict = None, **requests_kwargs) -> Response:
        if headers is None:
            headers = dict()
        params = dict(url=url,
                      headers={'Authorization': f'Bearer {cls._get_authorization_token()}',
                               **headers})
        return requests.delete(**params, **requests_kwargs)

    @classmethod
    def _response_content(cls, resp) -> dict:
        try:
            return resp.json()
        except (JSONDecodeError, simplejson.JSONDecodeError):
            return resp.text

    @classmethod
    def get_payment_methods(cls, lang_abbrv: str = None) -> dict:
        """
        Get the available PayU payment methods.
        :param lang_abbrv: 2 letter iso code for payment methods' description languages
        :return: `dict` of the HTTP response body
        :raise: `PayuException` if retrieving the payment methods fails
        """
        if lang_abbrv is None:
            lang_abbrv = 'pl'
        resp = cls._get(f'{cls._VARS.PAYU_PAYMENT_METHODS_URL}?lang={lang_abbrv}')
        if not resp.ok:
            raise PayuException(f'Failed to get payment methods: {resp.status_code} / {cls._response_content(resp)}')
        return resp.json()

    @classmethod
    def get_register_form_url(cls, webshop_guid: UUID, language: Optional[str] = None) -> str:
        """
        Get the url for registering the submerchant account for the given webshop
        :param webshop_guid: The guid of the webshop to create the submerchant account for
        :param language: Language code for the submerchant account registration form. Defaults to _pl_
        :return: str of the url to the registration form for the webshop
        """
        if language is None:
            language = 'pl'
        boarding_path = f'boarding/#/form?lang={language}&nsf=false&partnerId={Env.PAYU_MARKETPLACE_ID}&marketplaceExtCustomerId={webshop_guid}'
        return urllib.parse.urljoin(cls._VARS.PAYU_HOST, boarding_path)


class PayuSubmerchant(Payu):
    @classmethod
    def get(cls, submerchant_id) -> 'PayuSubmerchant':
        return cls(submerchant_id=submerchant_id)

    @staticmethod
    def hundreds_int_to_decimal(num: int) -> Decimal:
        return Decimal(num) / Decimal(100)

    def __init__(self, submerchant_id):
        super().__init__()
        self._submerchant_id = submerchant_id

    def _get(self, *args, **kwargs):
        resp = super()._get(*args, **kwargs)
        if resp.status_code == 404:
            raise SubmerchantDoesNotExist(f'Submerchant {self._submerchant_id} does not exist')
        return resp

    def _post(self, *args, **kwargs):
        resp = super()._post(*args, **kwargs)
        if resp.status_code == 404:
            raise SubmerchantDoesNotExist(f'Submerchant {self._submerchant_id} does not exist')
        return resp

    def status(self, currency_code: Optional[str] = None) -> dict:
        """
        Get the status of the PayU submerchant registration
        :param currency_code: Optional currency code for the request. Defaults to _PLN_. Required by the API
        :return: The response JSON dict
        """
        if currency_code is None:
            currency_code = 'PLN'
        try:
            resp = self._get(urllib.parse.urljoin(self._VARS.PAYU_SUBMERCHANT_STATUS_URL, f'{self._submerchant_id}/status') + f'?currencyCode={currency_code}')
            if not resp.ok:
                raise PayuException(f'Failed to get status for submerchant {self._submerchant_id}: {resp.status_code} / {self._response_content(resp)}')
        except SubmerchantDoesNotExist:
            # No submerchant yet created for the webshop so not verified
            return dict(created=False,
                        verified=False,
                        name=None,
                        tax_id=None,
                        regon=None)
        response = resp.json()
        response['created'] = True
        response['verified'] = True if response.pop('customerVerificationStatus') == 'Verified' else False
        return response

    def payout(self, *,
               currency: str = None,
               amount: Decimal = None,
               ext_customer_id: str = None,
               description: str = None,
               payout_id: UUID = None,
               customer_name: str = None) -> dict:
        """
        Do a payout from the submerchant's PayU account
        :param currency: ISO-4217 Currency code for the amount. Required if amount is set
        :param amount: Amount in decimal to pay out. Can be omitted or None for full payout
        :param ext_customer_id: External submerchant identifier. Required for marketplace
        :param description: Description for the payout. Not required
        :param payout_id: UUID for the payout. Not required
        :param customer_name: Name of the payout recipient (person?)
        :return: `dict` with fields `full_payout`: `boolean` indicating if the whole balance was paid out,
                 `payment_id` PayU payout id,
                 `amount` to indicate the amount if the balance was not paid out in full and missing if a full payout was done,
                 `currency` to indicate the currency if the balance was not paid out in full and missing if a full payout was done
        :raises: SubmerchantDoesNotExist if the submerchant does not exist
        """
        payload = dict(shopId=self._VARS.PAYU_SHOP_ID,
                       payout=dict(),
                       account=dict(extCustomerId=str(self._submerchant_id)))
        if amount is not None:
            payload['payout']['amount'] = int(amount * 100)
            if currency is None:
                raise PayuException('Currency is required when not doing full payouts')
        if currency is not None:
            payload['payout']['currencyCode'] = currency
        if ext_customer_id is not None:
            payload['account']['extCustomerId'] = ext_customer_id
        if description is not None:
            payload['payout']['description'] = description
        if payout_id is not None:
            payload['payout']['extPayoutId'] = str(payout_id)
        if customer_name is not None:
            payload['customerAddress'] = dict(name=customer_name)

        resp = self._post(self._VARS.PAYU_PAYOUTS_URL, body=payload)
        if not resp.ok:
            raise PayuException(f'Failed to do payout for submerchant {self._submerchant_id}: {resp.status_code} / {self._response_content(resp)}')
        payout_amount = f'payout of {amount} {currency}' if amount else 'full payout'
        log.debug(f'PayU {payout_amount} for submerchant {self._submerchant_id} successfully done: %s', resp.json())
        response = dict(payout_id=resp.json()['payout']['payoutId'],
                        full_payout=amount is None)
        if amount is not None:
            response['amount'] = amount
            response['currency'] = currency
        return response

    def operation_history(self):
        raise NotImplementedError('PayU operation history not implemented')

    def balance(self, currency_code: str = None) -> Dict[str, Decimal]:
        """
        Get the balance available for the submerchant
        :param currency_code: 3 letter currency code
        :return: `dict` with keys _amount_available_ for balance that is not blocked and _total_amount_ for sum of blocked and non blocked amount
        :raises: SubmerchantDoesNotExist if the submerchant does not exist
        """
        if currency_code is None:
            currency_code = 'PLN'
        url = urllib.parse.urljoin(self._VARS.PAYU_SUBMERCHANT_BALANCE_URL, f'{self._submerchant_id}/balances?currencyCode={currency_code}')
        resp = self._get(url)
        if not resp.ok:
            raise PayuException(f'Failed to get balance for submerchant {self._submerchant_id}: {resp.status_code} / {self._response_content(resp)}')
        log.debug(f'PayU submerchant {self._submerchant_id} successfully retrieved: %s', resp.json())
        balance = resp.json()['balance']
        return dict(available_amount=self.hundreds_int_to_decimal(balance['availableAmount']),
                    total_amount=self.hundreds_int_to_decimal(balance['totalAmount']),
                    amount_available_for_payout=self.hundreds_int_to_decimal(balance['availableAmount']))


class PayuOrder(Payu):
    def __init__(self, order_data: dict, redirect_location: str = None, type: PayuOrderType = None):
        super().__init__()
        self.order_data = copy.deepcopy(order_data)
        self.redirect_location = None
        self.order_id = None
        self.order_create_date = None
        self.notify_url = None
        self.customer_ip = None
        self.merchant_pos_id = None
        self.description = None
        self.currency_code = None
        self.total_amount = None
        self.status = None
        self.products = None
        self.shopping_carts = None
        self.status_code = None
        self.status_desc = None
        self.ext_order_id = None
        if redirect_location is not None:
            self.type = type
            self.redirect_location = redirect_location
            self.order_id = self.order_data['orderId']
            self.ext_order_id = self.order_data['extOrderId']
            self.status_code = self.order_data['status']['statusCode']
        elif 'shoppingCarts' in self.order_data['orders'][0]:
            # Fields in the response
            self.type = PayuOrderType.MARKETPLACE
            order_fields = dict(orders=["orderId", "orderCreateDate", "notifyUrl", "customerIp", "merchantPosId", "description", "currencyCode", "totalAmount",
                                        "status", "shoppingCarts", 'extOrderId'],
                                status=["statusCode", "statusDesc"])
            for key, values in order_fields.items():
                for field_name in values:
                    d = self.order_data[key] if isinstance(self.order_data[key], dict) else self.order_data[key][0]
                    setattr(self, camel_to_snake(field_name), d[field_name])
        else:
            # Fields in the response
            self.type = PayuOrderType.NORMAL
            order_fields = dict(orders=["orderId", "orderCreateDate", "notifyUrl", "customerIp", "merchantPosId", "description", "currencyCode", "totalAmount",
                                        "status", "products", 'extOrderId'],
                                status=["statusCode", "statusDesc"])
            for key, values in order_fields.items():
                for field_name in values:
                    d = self.order_data[key] if isinstance(self.order_data[key], dict) else self.order_data[key][0]
                    setattr(self, camel_to_snake(field_name), d[field_name])

    @classmethod
    def _from_dict(cls, response: dict, redirect_location: str = None, type: PayuOrderType = None) -> 'PayuOrder':
        order = cls(response, redirect_location=redirect_location, type=type)
        return order

    @classmethod
    def get(cls, order_id: str) -> 'PayuOrder':
        """
        Get the PayU Order with the given PayU Order ID
        :param order_id: Order ID of the PayU Order
        :return: The corresponding `PayuOrder`
        :raise: `PayuException` if the order cannot be retrieved
        """
        resp = cls._get(urllib.parse.urljoin(cls._VARS.PAYU_ORDERS_URL, order_id))
        if not resp.ok:
            raise PayuException(f'Failed to get order {order_id}: {resp.status_code} / {cls._response_content(resp)}')
        log.debug('PayU order successfully retrieved: %s', resp.json())
        return cls._from_dict(resp.json())

    @classmethod
    def _create_submerchants_order(cls, *,
                                   card_token: Optional[str],
                                   amounts: Dict[UUID, Dict[str, int]],
                                   total_amount: int,
                                   currency: Currency,
                                   buyer_id: str,
                                   customer_ip: str,
                                   continue_url: str,
                                   description: str,
                                   notify_url: str,
                                   order_guid: UUID,
                                   pos_id: str) -> 'PayuOrder':
        for values in amounts.values():
            for value in values.values():
                assert isinstance(value, int)
        payload = dict(
            buyer=dict(
                extCustomerId=buyer_id,
            ),
            notifyUrl=notify_url,
            merchantPosId=pos_id,
            description=description,
            currencyCode=currency.abbr,
            totalAmount=total_amount,
            extOrderId=str(order_guid),
            shoppingCarts=[],
        )

        for shop_guid, submerchant_amount in amounts.items():
            payload['shoppingCarts'].append(
                dict(
                    extCustomerId=str(shop_guid),
                    products=[dict(name='Products',
                                   unitPrice=submerchant_amount['amount'],
                                   quantity=1)],
                    **submerchant_amount,  # amount and fee are in submerchant amount
                ),
            )

        if card_token is not None:
            # TODO: payMethods requires `trusted_merchant` grant
            payload['payMethods'] = dict(payMethod=dict(type="CARD_TOKEN", value=card_token))
        if customer_ip is not None:
            payload['customerIp'] = customer_ip
        if continue_url is not None:
            payload['continueUrl'] = continue_url

        log.info('Creating order: %s', payload)
        resp = cls._post(f'{cls._VARS.PAYU_ORDERS_URL}', body=payload, allow_redirects=False)
        if not resp.ok:
            raise PayuException(f'Failed to create order: {resp.status_code} / {cls._response_content(resp)}')

        log.debug('PayU order successfully created: %s', resp.json())
        if resp.is_redirect:
            return cls._from_dict(resp.json(), redirect_location=resp.headers['location'], type=PayuOrderType.MARKETPLACE)
        else:
            return cls._from_dict(resp.json())

    @classmethod
    def _create_order(cls, *,
                      card_token: Optional[str],
                      amount: int,
                      currency: Currency,
                      customer_ip: str,
                      continue_url: str,
                      description: str,
                      notify_url: str,
                      order_guid: UUID,
                      pos_id: str) -> 'PayuOrder':
        assert isinstance(amount, int)
        payload = dict(
            notifyUrl=notify_url,
            merchantPosId=pos_id,
            description=description,
            currencyCode=currency.abbr,
            totalAmount=str(amount),
            extOrderId=str(order_guid),
            products=[
                dict(
                    name="Products",
                    unitPrice=str(amount),
                    quantity="1"
                ),
            ])

        if card_token is not None:
            # TODO: payMethods requires `trusted_merchant` grant
            payload['payMethods'] = dict(payMethod=dict(type="CARD_TOKEN", value=card_token))
        if customer_ip is not None:
            payload['customerIp'] = customer_ip
        if continue_url is not None:
            payload['continueUrl'] = continue_url

        log.info('Creating order: %s', payload)
        resp = cls._post(f'{cls._VARS.PAYU_ORDERS_URL}', body=payload, allow_redirects=False)
        if not resp.ok:
            raise PayuException(f'Failed to create order: {resp.status_code} / {cls._response_content(resp)}')

        log.debug('PayU order successfully created: %s', resp.json())
        if resp.is_redirect:
            return cls._from_dict(resp.json(), redirect_location=resp.headers['location'], type=PayuOrderType.NORMAL)
        else:
            return cls._from_dict(resp.json())

    @classmethod
    def create(cls, *,
               card_token: str = None,
               customer_ip: str = None,
               continue_url: str,
               amount: int,
               currency: Currency,
               description: str,
               order_guid: UUID) -> 'PayuOrder':
        """
        Create a new PayU Order.
        :param card_token: Token for the card to be used for the order. Can be `None` if not using a pre tokenized card
        :param customer_ip: The ip address where the order is placed from
        :param continue_url: The url to continue from PayU payment page
        :param amount: Total amount of the order in the smallest increments as int
        :param currency: Currency ISO code
        :param description: Shop's name in documentation TODO: Really?
        :param order_guid: Corresponding `Order`'s guid
        :return: A new `PayuOrder`
        :raise: `PayuException` if creating the order fails
        """
        return cls._create_order(
            card_token=card_token,
            customer_ip=customer_ip,
            continue_url=continue_url,
            amount=amount,
            currency=currency,
            description=description,
            notify_url=cls._VARS.PAYU_NOTIFY_URL,
            pos_id=cls._VARS.PAYU_POS_ID,
            order_guid=order_guid
        )

    @classmethod
    def create_submerchants(cls, *,
                            card_token: str = None,
                            customer_ip: str = None,
                            buyer_id: str,
                            continue_url: str,
                            amounts: Dict[UUID, Dict[str, int]],
                            total_amount: int,
                            currency: Currency,
                            description: str,
                            order_guid: UUID) -> 'PayuOrder':
        """
        :param card_token: Token for the card to be used for the order. Can be `None` if not using a pre tokenized card
        :param customer_ip: The ip address where the order is placed from
        :param buyer_id: External identifier of the buyer
        :param continue_url: The url to continue from PayU payment page
        :param amounts: dict of keys with webshop uuids and values of dict(amount=int, fee=int)
        :param total_amount: Total amount to be charged
        :param currency: Currency ISO code
        :param description: Shop's name in documentation TODO: Really?
        :param order_guid: Corresponding `Order`'s guid
        :return: A new `PayuOrder`
        :raise: `PayuException` if creating the order fails
        """
        return cls._create_submerchants_order(
            card_token=card_token,
            buyer_id=buyer_id,
            amounts=amounts,
            total_amount=total_amount,
            currency=currency,
            notify_url=cls._VARS.PAYU_NOTIFY_URL,
            customer_ip=customer_ip,
            continue_url=continue_url,
            description=description,
            pos_id=cls._VARS.PAYU_POS_ID,
            order_guid=order_guid,
        )

    def _update(self, **kwargs) -> Response:
        return self._put(urllib.parse.urljoin(self._VARS.PAYU_ORDERS_URL, self.order_id), body=kwargs)

    def collect_payment(self) -> dict:
        """
        Finalize (collect/capture) the payment.
        :return: `dict` of the collect response HTTP body
        :raise: `PayuException` if the capture fails
        """
        resp = self._update(orderId=self.order_id, orderStatus='COMPLETED')
        if not resp.ok:
            raise PayuException(f'Failed to collect order {self.order_id}: {resp.status_code} / {self._response_content(resp)}')
        try:
            if PayuOrderStatus[resp.json()['order']['status']] is not PayuOrderStatus.COMPLETED:
                raise PayuException("The PayU payment update's response's status is not completed as expected")
        except (JSONDecodeError, KeyError) as e:
            log.exception('Failed to parse payment update response as json due to %s: %s', str(e), self._response_content(resp))
            raise PayuException('Failed to parse payment update response as json due to %s: %s', str(e), self._response_content(resp))
        log.debug('PayU order successfully collected: %s', resp.json())
        return resp.json()

    def refund(self, description: str, refund_guid: UUID = None, ext_submerchant_id: str = None, amount: int = None) -> dict:
        """
        Refund the PayU Order's payment to the customer fully or partially
        :param description: Description for the refund
        :param refund_guid: An external identifier for the refund. Required for Marketplace orders unless amount is omitted or None (full refund)
        :param ext_submerchant_id: The external submerchant id from whom the refund should be taken from
        :param amount: Amount to refund. Can be omitted or `None` for full refund
        :return: `dict` of the refund response HTTP body
        :raise: `PayuException` if the refund fails
        """
        refund = dict()
        if refund_guid is None and self.type is PayuOrderType.MARKETPLACE:
            raise ValueError('Refund id required for marketplace orders')
        elif refund_guid is not None:
            refund['extRefundId'] = str(refund_guid)
        if ext_submerchant_id is not None:
            refund['extCustomerId'] = str(ext_submerchant_id)
        if description is not None:
            refund['description'] = description
        if amount is not None:
            refund['amount'] = amount
        payload = dict(refund=refund, order_id=self.order_id)
        resp = self._post(urllib.parse.urljoin(self._VARS.PAYU_ORDERS_URL, f'{self.order_id}/refunds'), body=payload)
        if not resp.ok:
            raise PayuException(f'Failed to refund order {self.order_id}: {resp.status_code} / {self._response_content(resp)}')
        log.debug('PayU order successfully refunded: %s', resp.json())
        return resp.json()

    def cancel(self) -> dict:
        """
        Cancel the PayU Order. Fully refunds the ordera automatically.
        :return: `dict` of the cancel response HTTP body
        :raise: `PayuException` if the cancellation fails
        """
        resp = self._delete(urllib.parse.urljoin(self._VARS.PAYU_ORDERS_URL, self.order_id))
        if not resp.ok:
            raise PayuException(f'Failed to cancel order {self.order_id}: {resp.status_code} / {self._response_content(resp)}')
        log.debug('PayU order successfully cancelled: %s', resp.json())
        return resp.json()
