# -*- coding: utf-8 -*-
import random
import uuid
from typing import Iterable
from urllib.parse import urlparse

from faker import Faker
from flask import json
from flask.testing import FlaskClient
from pytest_lazyfixture import LazyFixture
from werkzeug.exceptions import BadRequest

from gwio.environment import Env
from gwio.ext.vat import Currency

fake = Faker('fi_FI')

THE_ONE_CURRENCY = random.choice(list(Currency))


def image_url():
    while True:
        url = fake.image_url()  # pylint: disable=no-member
        if bool(urlparse(url).netloc):
            return url


def n_times(f, n):
    return [f() for _ in range(n)]


_valid_as_role_get_values = ('as_role', 'guid', 'identity_id')


def as_roles(pytest, *, get: Iterable = ('as_role',), include: list = None, skip: list = None, random_anonymous_identifiers=False):
    assert all(x in _valid_as_role_get_values for x in get), 'Invalid value requested'

    if not skip:
        skip = []
    role_fixtures = dict(
        admin=dict(
            as_role='as_admin',
            guid='admin_guid',
            identity_id='admin_identity_id',
        ),
        shopkeeper=dict(
            as_role='as_shopkeeper',
            guid='shopkeeper_guid',
            identity_id='shopkeeper_identity_id',
        ),
        customer=dict(
            as_role='as_customer',
            guid='customer_guid',
            identity_id='customer_identity_id',
        ),
        brand_owner=dict(
            as_role='as_brand_owner',
            guid='brand_owner_guid',
            identity_id='brand_owner_identity_id',
        ),
        anonymous=dict(
            as_role='as_anonymous',
            guid=uuid.uuid4() if random_anonymous_identifiers else None,
            identity_id=f'{Env.AWS_REGION}:{uuid.uuid4()}' if random_anonymous_identifiers else None,
        ),
    )

    def wrap_lazy_fix(x):
        return [pytest.lazy_fixture(y) if isinstance(y, str) else y for y in x]

    parametrize_data = []
    for key, fixtures in role_fixtures.items():
        # Check if the role is targeted based on `include` and `skip`
        if (not include or key in include) and key not in skip:
            # Only get the fixtures that are of interest
            target_fixtures = [fix_value for fix_name, fix_value in fixtures.items() if fix_name in get]
            parametrize_data.append(wrap_lazy_fix(target_fixtures))

    return pytest.mark.parametrize(get, parametrize_data, ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))


class FlaskTestClient(FlaskClient):
    def __method(self, method, *args, token_payload, return_headers=False, **kw):
        original_headers = kw.pop('headers', token_payload.copy() if token_payload is not None else None)
        headers = self.set_auth_header(token_payload, original_headers)
        headers['X-Amazon-Request-ID'] = str(uuid.uuid4())
        # self.environ_base['lambda.context'] = 'arn:partition:service:region:account-id:resource-type/resource-id'
        response = getattr(super(), method)(
            *args,
            headers=headers,
            **kw
        )
        if return_headers:
            return response, headers
        return response

    def _get(self, *args, **kw):
        return self.__method('get', *args, **kw)

    def _post(self, *args, **kw):
        return self.__method('post', *args, **kw)

    def _put(self, *args, **kw):
        return self.__method('put', *args, **kw)

    def _delete(self, *args, **kw):
        return self.__method('delete', *args, **kw)

    def _options(self, *args, **kw):
        return self.__method('options', *args, **kw)

    def _head(self, *args, **kw):
        return self.__method('head', *args, **kw)

    def set_auth_header(self, token_payload, headers):
        if token_payload is None:
            return headers
        headers['Authorization'] = 'Bearer {}'.format(self.application.get_auth_token(token_payload))
        return headers

    def open(self, *args, **kwargs):
        if 'json' in kwargs:
            kwargs['data'] = json.dumps(kwargs.pop('json'))
            kwargs['content_type'] = 'application/json'
        ret = super().open(*args, **kwargs)
        if ret.content_type == 'application/json':
            try:
                if kwargs['method'] != 'HEAD' and getattr(ret, 'json') is None:
                    ret.json = json.loads(ret.data.decode('utf-8'))
            except BadRequest:
                pass
        return ret


def endpoints_from_app(app):
    for rule in (x for x in app.application.url_map.iter_rules() if x.rule not in app.application.endpoints_without_audit):
        url = rule.rule
        for method in [x.lower() for x in rule.methods]:
            yield method, url


def assert_error_message(response, expected):
    errors = json.loads(response.json['message'].replace("'", '"'))

    def _assert_expected(error, expected, path=''):
        if isinstance(expected, dict):
            for key, expected_error in expected.items():
                path = f'{path}.{key}'
                _assert_expected(error[str(key)], expected_error, path)
        else:
            assert expected in error, f'{path} was `{error}` instead of expected `{expected}`'

    _assert_expected(errors, expected)


def request_redirected_uri(request_method, *request_args, **request_kwargs):
    # `return_headers=False` to make sure we don't get tuple here
    response = request_method(*request_args, **{**request_kwargs, **dict(return_headers=False)})

    assert 308 == response.status_code, response.json
    return request_method(response.location, **request_kwargs)
