# -*- coding: utf-8 -*-
"""
Random guids that are used over the test session to represent the webshop, customer and admin

These are random and vary from session to session (but remain the same over single test run
naturally)
"""
from uuid import uuid4

import pytest


@pytest.fixture(scope='session')
def random_identity_id():
    def f():
        return f'eu-west-1:{uuid4()}'

    return f


# TODO: Change to returning `webshop.guid` once core and v2018 are merged
@pytest.fixture(scope='session')
def webshop_guid():
    return uuid4()


@pytest.fixture(scope='session')
def delivery_method_webshop_guid():
    return uuid4()


@pytest.fixture(scope='session')
def customer_guid():
    return uuid4()


@pytest.fixture(scope='session')
def customer_identity_guid():
    return uuid4()


@pytest.fixture(scope='session')
def customer_identity_id(customer_identity_guid):
    return f'eu-west-1:{customer_identity_guid}'


@pytest.fixture(scope='session')
def shopkeeper_guid():
    return uuid4()


@pytest.fixture(scope='session')
def shopkeeper_identity_id():
    return f'eu-west-1:{uuid4()}'


@pytest.fixture(scope='session')
def admin_guid():
    return uuid4()


@pytest.fixture(scope='session')
def admin_identity_id():
    return f'eu-west-1:{uuid4()}'


@pytest.fixture(scope='session')
def location_guid():
    return uuid4()


@pytest.fixture(scope='session')
def brand_guid():
    return uuid4()


@pytest.fixture(scope='session')
def brand_owner_guid():
    return uuid4()


@pytest.fixture(scope='session')
def brand_owner_identity_id():
    return f'eu-west-1:{uuid4()}'
