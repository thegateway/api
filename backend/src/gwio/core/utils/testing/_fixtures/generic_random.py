import pytest

from gwio.core.utils.testing.helpers import fake


@pytest.fixture(scope='session')
def faker():
    return fake
