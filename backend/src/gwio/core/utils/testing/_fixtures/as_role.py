import random
from uuid import UUID

import pytest

from gwio.models import user

UserData = user.UserData

ISS_PYTEST = 'http://localhost/pytest'


# pylint: disable=protected-access
class AuthenticatedTestClient:
    """
    This supercedes AuthenticatedTest
    """
    app = None
    token_payload = {}

    def __init__(self, app, token):
        self.app = app
        self.token_payload = token

    def _request(self, method, *args, **kwargs):
        kwargs = {'token_payload': self.token_payload, **kwargs}
        return getattr(self.app, f'_{method}')(*args, **kwargs)

    def get(self, *args, **kwargs):
        return self._request('get', *args, **kwargs)

    def post(self, *args, **kwargs):
        return self._request('post', *args, **kwargs)

    def put(self, *args, **kwargs):
        return self._request('put', *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self._request('delete', *args, **kwargs)

    def options(self, *args, **kwargs):
        return self._request('options', *args, **kwargs)

    def head(self, *args, **kwargs):
        return self._request('head', *args, **kwargs)


@pytest.fixture(scope='class')
def create_auth_client(app, faker):
    def fn(**kwargs):
        return AuthenticatedTestClient(app, {
            'iss': ISS_PYTEST,
            'name': faker.name(),
            **kwargs
        })

    return fn


@pytest.fixture(scope='class')
def as_customer(create_auth_client, customer_guid, customer_identity_id, faker):
    user_data = UserData(guid=customer_guid, identity_id=customer_identity_id)
    user_data.save()

    yield create_auth_client(**{
        'cognito:username': str(customer_guid),
        'cognito:groups': ['customer'],
        'pin': faker.ssn(),
        'email': faker.email(),
    })

    user_data.delete()


@pytest.fixture(scope='class')
def as_admin(create_auth_client, admin_identity_id, admin_guid, faker):
    user_data = UserData(guid=admin_guid, identity_id=admin_identity_id)
    user_data.save()

    yield create_auth_client(**{
        'cognito:groups': ['admin'],
        'cognito:username': str(admin_guid),
        'email': faker.email(),
    })

    user_data.delete()


@pytest.fixture(scope='class')
def as_shopkeeper(create_auth_client, shopkeeper_guid, shopkeeper_identity_id, webshop_guid, faker, as_any_shopkeeper):
    user_data = UserData(guid=shopkeeper_guid, identity_id=shopkeeper_identity_id)
    user_data.save()

    yield as_any_shopkeeper(
        shopkeeper_guid=shopkeeper_guid,
        shopkeeper_identity_id=shopkeeper_identity_id,
        webshop_guid=webshop_guid
    )

    user_data.delete()


@pytest.fixture(scope='class')
def as_any_shopkeeper(create_auth_client, faker):
    def fn(shopkeeper_guid, shopkeeper_identity_id, webshop_guid):
        user_data = UserData(guid=shopkeeper_guid, identity_id=shopkeeper_identity_id)
        user_data.save()

        return create_auth_client(**{
            "custom:organization_guid": str(webshop_guid),
            'cognito:groups': ['shopkeeper'],
            'cognito:username': str(shopkeeper_guid),
            'email': faker.email(),
        })

    return fn


@pytest.fixture(scope='class')
def as_brand_owner(create_auth_client, brand_guid, brand_owner_guid, brand_owner_identity_id, faker):
    user_data = UserData(guid=brand_owner_guid, identity_id=brand_owner_identity_id)
    user_data.save()

    yield create_auth_client(**{
        'custom:organization_guid': str(brand_guid),
        'cognito:groups': ['product_owner'],
        'cognito:username': str(brand_owner_guid),
        'email': faker.email(),
    })

    user_data.delete()


@pytest.fixture(scope='class')
def as_anonymous(app):
    yield AuthenticatedTestClient(app, {})


@pytest.fixture(scope='class')
def create_brand_owner_client(create_auth_client, brand_owner_guid, brand_guid, faker):
    def client(organization_guid: UUID = None):
        guid = str(organization_guid if organization_guid is not None else brand_guid)

        return create_auth_client(**{
            "custom:organization_guid": guid,
            'cognito:groups': ['product_owner'],
            'cognito:username': str(brand_owner_guid),
            'email': faker.email(),
        })

    return client


@pytest.fixture(scope='class')
def as_random_role(as_customer, as_admin, as_shopkeeper, as_brand_owner, as_anonymous):
    return random.choice((
        ('as_customer', as_customer),
        ('as_admin', as_admin),
        ('as_shopkeeper', as_shopkeeper),
        ('as_brand_owner', as_brand_owner),
        ('as_anonymous', as_anonymous),
    ))
