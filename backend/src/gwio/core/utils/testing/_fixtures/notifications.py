import random

import pytest
import pywebpush

from gwio.utils import notify
from gwio.utils.notify import Notification
from gwio.core.utils.testing.helpers import fake


class FakeFcmNotification:
    notified = False
    exception = None
    notifications = list()

    def __init__(self, **_):
        pass

    @classmethod
    def notify_single_device(cls, **kwargs):
        cls.notified = True
        cls.notifications.append(kwargs)
        if cls.exception:
            raise cls.exception  # pylint: disable=raising-bad-type

    @classmethod
    def reset_mock(cls):
        cls.notified = False
        cls.exception = None
        cls.notification = list()


@pytest.fixture(scope='function')
def mock_fcm(mocker):
    mock_fcm = FakeFcmNotification
    mocker.patch('pyfcm.FCMNotification', new=mock_fcm)
    yield mock_fcm


class FakeWebpush:
    def __init__(self):
        self.webpush_called = False

    def webpush(self, *args, **kwargs):
        self.webpush_called = True


@pytest.fixture(scope='function')
def mock_webpush(mocker):
    mock_webpush = FakeWebpush()
    mocker.patch.object(pywebpush, 'webpush', mock_webpush.webpush)
    yield mock_webpush


class MockedEMail:
    eb_event = {}

    @classmethod
    def mocked_send(cls, event):
        cls.eb_event = event


@pytest.fixture(scope='function')
def mock_email(mocker):
    mocked_email = MockedEMail()
    mocker.patch.object(notify, 'send_email_event', mocked_email.mocked_send)
    yield mocked_email


def random_notification_data(notification_type):
    # fake use confuses lint
    # pylint:disable=no-member
    datas = {
        Notification.Type.WEBPUSH.value: lambda: dict(endpoint=fake.uri(),
                                                      expirationTime=fake.unix_time(),
                                                      keys=dict(p256dh=fake.sha256(), auth=fake.sha256())),
        Notification.Type.FCM_PUSH.value: lambda: dict(registration_key=fake.sha256())
    }
    try:
        return datas[notification_type]()
    except KeyError:
        return dict()


@pytest.fixture(scope='session')
def random_notification_subscription_payload():
    def subscription_payload(notification_type=None, **data):
        if notification_type is None:
            notification_type = random.choice(list(Notification.Type)).value
        return dict(
            notification_types=dict(
                type=notification_type,
                data={**random_notification_data(notification_type), **data}))

    return subscription_payload
