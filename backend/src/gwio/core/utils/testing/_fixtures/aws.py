import copy
import io
import json
import logging
import random
import uuid
import zipfile
from unittest import mock

import boto3
import elasticsearch_dsl
import pytest
from attrdict import AttrDict
from dynamorm import (
    GlobalIndex,
)
from elasticsearch import NotFoundError
from moto import (
    mock_cognitoidentity,
    mock_cognitoidp,
    mock_dynamodb2,
    mock_events,
    mock_logs,
    mock_s3,
    mock_ses,
    mock_ssm,
    mock_secretsmanager,
    mock_lambda,
)

from gwio.environment import Env
from gwio.ext.decorators import singleton
from gwio.ext.dynamorm import (
    _vertical_models,
    all_models,
)
from gwio.ext.index_es_document import EsEvent

logger = logging.getLogger(__name__)
AttrDict.to_dict = lambda self: self


# pylint: disable=redefined-builtin, no-self-use
@singleton
class FakeElasticSearch:
    """Extremely simplified elastic search mock up it is basically able to do simple index() and
    the basic match query.
    """

    def __init__(self):
        self.values = dict()
        self.query = dict()

    def clear(self):
        self.values = dict()
        self.query = dict()

    def create_index(self, index: str):
        self.values[index] = dict()

    def index(self, index: str = None, id: uuid = None, body: str = None) -> None:
        assert index is not None
        assert id is not None
        assert body is not None

        if index not in self.values:
            self.values[index] = dict()

        self.values[index][id] = dict(body=body)

    # pylint: disable=unused-argument
    def delete(self, index: str = None, id: uuid = None) -> None:
        try:
            del self.values[index][id]
        except Exception:  # pylint: disable=broad-except
            pass

    def _create_entry(self, index: str, id: str) -> dict:
        return dict(_index=index, _id=id, _score=random.randrange(0.0, 10.0),
                    _source=copy.deepcopy(self.values[index][id]['body']))

    def _matches(self, entry: str, query: dict) -> bool:
        for key, value in query.items():
            if value in entry['body'].get(key, list()):
                return True
        return False

    def get(self, index: str = None, id: uuid = None) -> dict:
        try:
            return self.values[index][id]
        except KeyError as e:
            if id in str(e):
                raise NotFoundError(f'item {id} not found')
            raise NotFoundError(f'index {index} not found') from e

    def search(self, **kwargs) -> dict:
        hits = list()
        indexes = kwargs['index']
        # NOTE! this mockup only supports one simple type of query that checks for partial match in any data field
        # on the top level of the data
        try:
            query = kwargs['body']['query']['match']
        except KeyError:
            query = kwargs['body']['query']['bool']['must'][0]['match']
            # TODO: Add filters
        if indexes == '_all':
            indexes = self.values.keys()
        else:
            indexes = [indexes]
        for index in indexes:
            for id in self.values[index].keys():
                if self._matches(self.values[index][id], query):
                    hits.append(self._create_entry(index, id))
        return dict(_scroll_id=str(uuid.uuid4()), took=random.randint(1, 10), timed_out=False,
                    _shards=dict(total=1, successful=1, skipped=0, failed=0),
                    hits=dict(total=dict(value=len(hits)), max_score=random.randrange(1.0, 10.0), hits=hits))

    def scroll(self, **kwargs) -> dict:
        # Complete fake, e.g. returns empty results for every token
        return dict(_scroll_id=kwargs['scroll_id'], hits=dict(total=0, hits=list()))


def update_es_document(es, index: str, document_id: str, product: str):
    product = AttrDict(json.loads(product))
    try:
        existing = es.get(index=index, id=product.guid)
    except NotFoundError:
        # Entry does not yet exist
        existing = None

    if existing is None or existing['updated'] < product.updated:
        es.index(index=index, id=document_id, body=product)

    # Not updating product: es updated > product updated"',
    return


def delete_es_document(es, index: str, document_id: str):
    es.delete(index=index, id=document_id)


def _document_to_es(event: dict):
    event = AttrDict(event)
    details = AttrDict(json.loads(event.Detail))
    if EsEvent(event.DetailType) is EsEvent.PRODUCT_SAVE_EVENT:
        update_es_document(FakeElasticSearch(), details.index, details.id, details.product)
    elif EsEvent(event.DetailType) is EsEvent.PRODUCT_DELETE_EVENT:
        delete_es_document(FakeElasticSearch(), details.index, details.id)
    else:
        assert False, 'Unhandled es event type'


def _execute_search(dsl: elasticsearch_dsl.Search) -> dict:
    from gwio.ext.eshelpers import es

    assert 1 == len(dsl._index)
    params = dict(index=dsl._index[0], body=dsl.to_dict())
    return es().search(**params)


def start_services():
    logs = mock_logs()
    logs.start()

    events = mock_events()
    events.start()

    ses = mock_ses()
    ses.start()

    dynamo = mock_dynamodb2()
    dynamo.start()

    pre_fix = f'{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}'
    for model in all_models:
        # Making sure all names are prefixed correctly
        # Table name
        t_name = model.Table.name
        assert t_name.startswith(pre_fix) or any(t_name.startswith(f'{x}-{Env.APPLICATION_ENVIRONMENT}') for x in _vertical_models), t_name
        # Index names
        for index in model.__dict__.values():
            try:
                if not issubclass(index, GlobalIndex):
                    continue
            except TypeError:
                continue
            assert index.name.startswith(pre_fix) or any(index.name.startswith(f'{x}-{Env.APPLICATION_ENVIRONMENT}') for x in _vertical_models), index.name

        model.create_table(wait=True)

    es_eventbridge = mock.patch('gwio.ext.index_es_document.send_to_eventbridge', wraps=_document_to_es)
    es_eventbridge.start()
    fake_es = mock.patch('gwio.ext.eshelpers.es', wraps=FakeElasticSearch)
    fake_es.start()
    mock_execute_es_search = mock.patch('gwio.ext.eshelpers._execute_search', wraps=_execute_search)
    mock_execute_es_search.start()

    s3 = mock_s3()
    s3.start()

    aws_lambda = mock_lambda()
    aws_lambda.start()

    secretsmanager = mock_secretsmanager()
    secretsmanager.start()

    zip_output = io.BytesIO()
    zip_file = zipfile.ZipFile(zip_output, "w", zipfile.ZIP_DEFLATED)
    zip_file.writestr("lambda_function.py", """
    def lambda_handler(event, context):
        return event
    """)
    zip_file.close()
    zip_output.seek(0)

    secrets_client = boto3.client('secretsmanager')
    secrets = dict(
        FCM_NOTIFICATION_API_KEY='fcm_notification_api_key',
        PAYU_CLIENT_ID='payu_client_id',
        PAYU_CLIENT_SECRET='payu_client_secret',
        PAYU_SHOP_ID='payu_shop_id',
        PAYU_MARKETPLACE_ID='payu_marketplace_id',
        PAYU_POS_ID='payu_pos_id',
        RECAPTCHA_SITE_SECRET='recaptcha_site_secret',
        STRIPE_API_KEY=None,
        VAPID_PRIVATE_KEY='vapid_private_key',
        XPRESS_COURIERS_CLIENT_NUMBER='xpress_couriers_client_number',
        XPRESS_COURIERS_API_USERNAME='xpress_couriers_api_username',
        XPRESS_COURIERS_API_PASSWORD='xpress_couriers_api_password',
    )
    secrets_client.create_secret(
        Name=Env.NAMESPACE,
        SecretString=json.dumps(secrets),
    )

    return dict(
        dynamo=dynamo,
        events=events,
        ses=ses,
        logs=logs,
        es=fake_es,
        es_eventbridge=es_eventbridge,
        s3=s3,
        secretsmanager=secretsmanager,
        ssm=start_ssm_mock(),
        cognitoidentity=start_cognitoidentity(),
        cognitoidp=start_cognitoidp(),
    )


def start_ssm_mock():
    ssm = mock_ssm()
    ssm.start()
    return ssm


def start_cognitoidentity():
    cognitoidentity = mock_cognitoidentity()
    cognitoidentity.start()
    return cognitoidentity


def start_cognitoidp():
    cognitoidp = mock_cognitoidp()
    cognitoidp.start()
    return cognitoidp


class MockedEventsStore():
    _events = list()

    # pylint:disable=no-self-argument
    def mocked_publish(audit_requests_util):
        MockedEventsStore._events.append(audit_requests_util.marshal())


def start_clients():
    logs_client = boto3.client('logs')

    region = Env.AWS_REGION
    s3_client = boto3.client('s3', region_name=region)

    cognito_password_policy = mock.patch('gwio.utils.aws.CognitoIdpClient.password_policy',
                                         new_callable=lambda *args, **kwargs: dict(MinimumLength=8,
                                                                                   RequireUppercase=True,
                                                                                   RequireLowercase=True,
                                                                                   RequireNumbers=True,
                                                                                   RequireSymbols=False))
    cognito_password_policy.start()

    mock_event_publish = mock.patch('gwio.core.cw_logging.Event.emit', new=MockedEventsStore.mocked_publish)
    mock_event_publish.start()

    return dict(
        logs=logs_client,
        s3=s3_client,
        cognito_password_policy=cognito_password_policy,
    )


def start_aws_mock():
    return dict(
        services=start_services(),
        clients=start_clients(),
    )


mocks = start_aws_mock()


@pytest.fixture(scope='session')
def mock_aws():
    yield mocks
