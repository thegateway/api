import uuid
from datetime import datetime

import pytest
import pytz
from attrdict import AttrDict
from flask import json

import gwio.user
from gwio.core import cw_logging
from gwio.core.utils.testing._fixtures.aws import MockedEventsStore
from gwio.core.utils.testing.helpers import request_redirected_uri

User = gwio.user.User
Event = cw_logging.Event
LogEventType = cw_logging.LogEventType
AuditRequestsUtil = cw_logging.AuditRequestsUtil


@pytest.fixture(scope='session')
def audit_request(mock_aws):
    def fn(*, request_method, url, check_new_event=True, **request_kwargs):
        MockedEventsStore._events = []
        try:
            r, headers = request_method(url, **request_kwargs, return_headers=True)
            if r.status_code == 301:
                r, headers = request_redirected_uri(request_method, url, **request_kwargs, return_headers=True)
        except NotImplementedError:
            # TODO: Remove once user delete does has moto support
            if not url.startswith('/register'):
                raise
            return None, None, None
        new_events = MockedEventsStore._events

        if not check_new_event:
            return r, None, headers

        assert 1 == len(new_events)
        audit_log = AttrDict(**Event.audit_schema.load(json.loads(new_events[0]['Detail'])['payload']))

        return r, audit_log, headers

    return fn


def _validate_audit_log(*, r, audit_log, method, url, start, identity_id, as_role, headers):
    if audit_log is None:
        return

    assert audit_log.audit is True

    def _assert(value, expected):
        """Helps get sensible information about the reason for failure since PyCharm messes up due to assert failure not happening in the test function"""
        assert value == expected, f'{type(value).__name__}({value}) == {type(expected).__name__}({expected})'

    assert audit_log.timestamp >= start
    # TODO: Remove the microsend part after marshmallow has the fix to microseconds breaking deserialation of DateTime applied to a released version
    #       https://github.com/marshmallow-code/marshmallow/issues/627#issuecomment-508372565
    assert audit_log.timestamp <= datetime.now(pytz.timezone('UTC')).replace(microsecond=0)

    assert isinstance(audit_log.event_type, LogEventType)

    _assert(audit_log.response.status_code, r.status_code)
    if not as_role.token_payload:
        assert audit_log.subject_id is None
        assert audit_log.subject_type is None
    else:
        _assert(audit_log.subject_type, User.__name__)
        _assert(audit_log.subject_id, identity_id)

    assert audit_log.object_type is not None

    # The object guid might be None if the request wasn't successful
    #  and will be None if the response is a list
    if r.status_code >= 400 or r.status_code in (204, 308) or isinstance(r.json, list) or all(x in r.json for x in ('next', 'total')):
        assert hasattr(audit_log, 'object_id')
        assert hasattr(audit_log, 'object')
    else:
        # The bootstrap get is a very special little endpoint that might return stuff even if bootstrap record doesn't exist
        if not (method == 'get' and url == '/bootstrap/'):
            assert audit_log.object_id is not None
            assert audit_log.object and isinstance(audit_log.object, dict)

    request_id = uuid.UUID(headers['X-Amazon-Request-ID'])
    assert audit_log.request_guid == request_id
    assert audit_log.request.guid == request_id


@pytest.fixture(scope='session')
def check_endpoint_audits(audit_request):
    def __check_endpoint_audits(*, as_role, identity_id, method, url):
        # TODO: Remove the microsend part after marshmallow has the fix to microseconds breaking deserialation of DateTime applied to a released version
        #       https://github.com/marshmallow-code/marshmallow/issues/627#issuecomment-508372565
        start = datetime.now(pytz.timezone('UTC')).replace(microsecond=0)

        while '<' in url:
            url = f"{url[:url.find('<')]}{uuid.uuid4()}{url[url.find('>') + 1:]}"

        # TODO: Redo the sent json better later. Just a solution to some POST endpoints returning `No Content` response if not sending any (valid) fields.
        r, audit_log, headers = audit_request(
            request_method=getattr(as_role, method),
            url=url,
            # We want to test these methods to make sure they work even though they are not audited, to confirm auditing doesn't break them
            check_new_event=False if method.upper() in ('OPTIONS', 'HEAD') else True,
            json=dict(name='test'),
        )

        _validate_audit_log(
            r=r,
            audit_log=audit_log,
            method=method,
            url=url,
            start=start,
            identity_id=identity_id,
            as_role=as_role,
            headers=headers,
        )

    return __check_endpoint_audits
