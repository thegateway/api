# -*- coding: utf-8 -*-
"""
Utilities for XML handling, mostly related to xml importers (Fimea, SAL)
"""
from . import registry as _registry
from . import base as _base


def new_xml_scheme(xml_scheme=None):
    """
    Add new xml scheme and return the decorator class to use be used for defining the scheme

    :param xml_scheme: name of the new xml scheme
    :return: tuple of decorators
    """
    _registry.init_registry(xml_scheme)
    return _base.SchemeDecorator(xml_scheme)
