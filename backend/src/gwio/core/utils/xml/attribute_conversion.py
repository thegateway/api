# -*- coding: utf-8 -*-
"""
Commonly use attribute type conversions
"""
from decimal import Decimal, ROUND_UP

from gwio.utils.converters import to_int


def _ltk_boolean(x):
    """
    Convert "ltkcode" into Boolean
    :param x: character (0,1,9)
    :return: Boolean or None
    """
    return {'0': False, '1': True}.get(x)


def _cents_to_euro(x):
    return (Decimal(x) / Decimal('100')).quantize(Decimal('0.01'), rounding=ROUND_UP)


def _firstvalue(x):
    return x.split(';')[0]


def _zeropadded(v, pad=2):
    i, s = to_int(v)
    return str(i).rjust(pad, '0') + s


def _as_is(x):
    return x


_converters = {
    'ltk_boolean': _ltk_boolean,
    'cents_to_euro': _cents_to_euro,
    'int': int,
    'Decimal': Decimal,
    'zeropadded': _zeropadded,
    'firstvalue': _firstvalue,
    None: _as_is
}


def get_converter(method: str) -> callable:
    """
    Return the function to handle named conversion.
    """
    return _converters[method]
