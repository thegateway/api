# -*- coding: utf-8 -*-
"""
Class based xml to dict converter
"""
from functools import partial

from .attribute_conversion import get_converter
from .registry import register_entity, register_element
from .actions import fimea_uuid, add_foreign_key


def _stripped_text(node):
    if node.text is None:
        return u''
    s = node.text
    if isinstance(s, bytes):
        s = s.decode('utf-8')
    return s.strip()


def _stripped_attr(node, key):
    s = node.attrib[key]
    if isinstance(s, bytes):
        s = s.decode('utf-8')
    return s.strip()


_DEFAULT_ATTRIB_VALUES = {
    '__tag__': None,
    '__key__': None,
    '__uuid__': None,
    '__text__': None,
    '__storage__': None,
    '__value__': None,
    '__converter__': None,
    '__attribs__': None,
    '__converters__': None,
    '__values__': None,
    '__foreign_key__': None,
    '__relations__': None,
    '__creates__': None,

    '_element': None,
    '_xml_scheme': None,
    '_xml_tag': None,
    '_container': False,
    '_detached': False,
    '_sequence': False,
    '_referenced': False,
    '_relation': False,
    '_triggers': None,
    '_ignored': False
}


class _BaseAttributes(type):
    # pylint: disable=bad-mcs-classmethod-argument
    def __new__(mcs, name, bases, dct):
        new_dct = dct.copy()
        for key in set(_DEFAULT_ATTRIB_VALUES) - set(new_dct):
            new_dct[key] = _DEFAULT_ATTRIB_VALUES[key]
        cls = super(_BaseAttributes, mcs).__new__(mcs, name, bases, new_dct)
        return cls


# Slightly overly tricky code in here, some of the attributes are generated
# dynamically when the class is instantiated, code checkers do not detect
# this, so warnings related to these are disabled. -mte
# pylint: disable=E1101,R0902,W0212
# noinspection PyUnresolvedReferences, PyProtectedMember
class ConverterElement(metaclass=_BaseAttributes):
    """
    Root class for all the conversion elements and entities
    """
    _storage_name = None

    def __init__(self, converter=None, proxy_for=None, node=None):
        assert converter is not None
        assert node is not None
        self._parser = converter
        self._proxied = None
        self._proxy_for = proxy_for
        self._values = {}
        self._pushed_container = None
        self._node = node
        self._closed = False

        if not self._referenced and self.__storage__ is not None:
            self._storage_name = self.__storage__.__name__

    def _finish(self):
        self._closed = True
        self._node = None
        self._parser.add_seen(self.__class__.__name__)  # for unittesting

    def _generate_uuid(self, keys=None, exclude=None):
        # Generate uuid using values stored in the ConverterElement()
        assert exclude is not None
        if not keys:
            keys = self._values.keys()

        value = self._storage_name
        for key in sorted(keys):
            if key not in exclude:
                key_value = self._values.get(key)
                if key_value is not None:
                    try:
                        value += '-' + self._values.get(key)
                    except TypeError:
                        value += '-' + str(self._values.get(key))

        return fimea_uuid(value)

    def _save_text(self, node):
        if self.__text__ is None:
            return

        storage = self._parser.get_top_element(self._storage_name)
        default = self.__value__
        convertfn = get_converter(self.__converter__)
        value = _stripped_text(node)
        if value is None or value == '':
            value = default
        else:
            value = convertfn(value)
        storage._values[self.__text__] = value

    def _save_attribs(self, node):
        if self.__attribs__ is None:
            return

        storage = self._parser.get_top_element(self._storage_name)
        convert = self.__converters__
        default = self.__values__
        attribs = self.__attribs__
        if convert is None:
            convert = {}
        if default is None:
            default = {}
        for key in node.attrib:
            convertfn = get_converter(convert.get(key))
            default_value = default.get(key, None)
            value = _stripped_attr(node, key)
            if value is None or value == '':
                value = default_value
            else:
                value = convertfn(value)
            if attribs:
                save_as = attribs.get(key, None)
            else:
                save_as = key

            storage._values[save_as] = value

    @classmethod
    def _add_trigger(cls, related):
        if cls._triggers is None:
            cls._triggers = []
        if related not in cls._triggers:
            cls._triggers.append(related)

    @classmethod
    def _triggered(cls, converter=None, node=None):
        assert cls.__relations__
        assert node is not None
        assert converter is not None

        name = cls.__name__
        element = converter.get_top_element(name)
        if element is None or element._closed:
            self = cls(converter=converter, node=node)
            self._process_trigger(cls.__relations__)

    def _process_trigger(self, elements):
        related = []
        for cls in elements:
            element = self._parser.get_top_element(cls._storage_name)
            if element is None or element._closed:
                return
            related.append(element)

        self._parser.push_element(self._storage_name, self)
        for foreign in related:
            self._parser.add_action(partial(add_foreign_key, self=self, foreign=foreign))
            self._parser.add_action(self._create_uuid)

        if self._triggers is not None:
            self._fire_triggers(converter=self._parser, node=self._node)

        self._finish()

    @classmethod
    def _fire_triggers(cls, converter=None, node=None):
        assert cls._triggers is not None
        assert node is not None
        assert converter is not None

        for related in cls._triggers:
            related._triggered(converter=converter, node=node)

    def process(self, node):
        """
        Process associated ElementTree node when it is encountered
        """
        assert node is not None
        assert not self._relation, "xml elements cannot be relations"

        if self._referenced:
            pass

        if self._container and not self._proxy_for:
            self._pushed_container = self
            self._parser.push_container(self._pushed_container)

        if self._container:
            self._parser.push_element(self._storage_name, self)

        if self.__creates__ is not None:
            self._proxied = self.__creates__(converter=self._parser, proxy_for=self, node=self._node)
            self._proxied.process(node)

        if node is not None and not self._ignored:
            self._save_text(node)
            self._save_attribs(node)

        if self._triggers is not None:
            self._fire_triggers(converter=self._parser, node=self._node)

    def _create_uuid(self):
        if self.__class__.__name__ == 'LaakepakkausKorvausluokka':
            pass
        if self.__key__ is not None:
            self._values[self.__key__] = self._generate_uuid(self.__uuid__, exclude=[self.__key__])
        if self.__class__.__name__ == 'LaakepakkausKorvausluokka':
            pass

    def finalize(self):
        """
        Finalize processing the element after all its sub nodes have been processed
        """
        assert not self._relation, "xml elements cannot be relations"

        if self._referenced:
            pass

        if self._proxied is not None:
            self._proxied.finalize()

        if self.__uuid__ is not None:
            self._create_uuid()

        if self._pushed_container:
            self._parser.pop_container(self._pushed_container)

        # If this was not the first time we generated this element, remove it from element stack
        # (relations are always kept as they are triggered by having the new element in the relation
        # if there is a key conflict that will not be detected by the parser
        if self.__key__ is not None:
            element = self._parser.get_or_add_index(self._storage_name, self._values[self.__key__], self)
            if self != element:
                self._parser.pop_element(self._storage_name, self)

        if self._referenced:
            parent = self._parser.get_top_element(self.__storage__._storage_name)
            if parent is not None:
                add_foreign_key(self=parent, foreign=self)

        if self.__foreign_key__ is not None:
            foreign = self._parser.get_top_element(self.__foreign_key__._storage_name)
            self._parser.add_action(partial(add_foreign_key, self=self, foreign=foreign))
        elif self._container and not self._detached:
            parent = self._parser.get_container()
            if parent is not None:
                add_foreign_key(self=parent, foreign=self)

        self._finish()

    def values(self):
        """
        Get values stored in the element
        """
        return self._values

    def get_reference(self):
        """
        Get a unique (typically) uuid reference to the element
        """
        assert self.__key__ is not None

        if self._proxy_for is not None:
            ref = '{0}_{1}'.format(self._proxy_for._xml_tag.lower(), self.__key__)
        else:
            ref = '{0}_{1}'.format(self._storage_name.lower(), self.__key__)
        return ref, self._values[self.__key__]


class DatabaseElement(ConverterElement):
    """
    Base class for all DatabaseElements to store derived information.
    Not directly instantiated by the converter.

    An instance of DatabaseElement() is only created either
    by trigger or by proxy element.
    """
    _xml_element = False


class XmlElement(ConverterElement):
    """
    Base class for all XML elements instantiated by the converter
    for handling of each individual XML entry.
    """
    _xml_element = True


# noinspection PyProtectedMember
class SchemeDecorator:
    """
    Decorator class for defining the XML element structure as python class structure
    the xml hierarchy is mapped as class inheritance
    """

    def __init__(self, xml_scheme):
        self._xml_scheme = xml_scheme

    def _inheritance_chain(self, cls):  # NOTE! assuming single inheritance
        if cls.__name__ == 'XmlElement':
            return []
        return [cls._xml_tag] + self._inheritance_chain(cls.__bases__[0])

    def _register_entity(self, cls):
        cls._element = cls.__name__
        if not cls._xml_element:
            register_entity(self._xml_scheme, cls.__name__, cls)
            cls._xml_tag = None
            return cls

        try:
            cls._xml_tag = cls.__tag__
        except AttributeError:
            cls._xml_tag = None

        if cls._xml_tag is None:
            cls._xml_tag = cls.__name__

        path = reversed(self._inheritance_chain(cls))
        if path:
            register_element(self._xml_scheme, cls.__name__, path, cls)

        return cls

    def container(self, cls):
        """
        Decorator to mark element as a container (will automatically generate a reference
        into itself to the parent element)
        """
        cls._container = True
        cls._storage_name = cls.__name__

        return self._register_entity(cls)

    def detached_container(self, cls):
        """
        Decorator to mark element as a container without no direct link to parent element
        """
        cls._detached = True
        return self.container(cls)

    def sequence(self, cls):
        """
        Decorator to mark element as a sequence (container with 0 to n sub elements)
        """
        cls._container = True
        cls._sequence = True
        cls._storage_name = cls.__name__
        return self._register_entity(cls)

    def structure(self, cls):
        """
        Decorator to mark element as a node in xml hierarchy
        """
        return self._register_entity(cls)

    def normalized(self, cls):
        """
        Decorator to mark element as "normalized", same as structure, this is syntactic sugar
        for defining xml nodes that create elements that are not directly mapped from xml
        """
        return self._register_entity(cls)

    def referenced(self, cls):
        """
        Decorator to mark element that stores a reference to element outside python class
        inheritance chain
        """
        assert cls.__storage__ is not None, "referenced elements must define __storage__"
        cls._referenced = True
        cls._container = True
        cls._storage_name = cls.__name__
        cls._detached = True
        return self._register_entity(cls)

    def value(self, cls):
        """
        Decorator to mark element as value node, the values extracted are stored to the
        "closest" parent container.
        """
        assert cls.__text__ is not None or cls.__attribs__ is not None, "value elements need to store something"
        return self._register_entity(cls)

    def ignored(self, cls):
        """
        Decorator to mark element completely ignored
        """
        cls._ignored = True
        return self._register_entity(cls)

    def relation(self, cls):
        """
        Decorator to create new element when any of the related elements (which must be some of the container
        types) change.
        """
        assert cls.__relations__ is not None, "Must define __relations__"

        cls._container = True
        cls._relation = True
        cls._storage_name = cls.__name__

        for related in cls.__relations__:
            related._add_trigger(cls)

        return self._register_entity(cls)
