# -*- coding: utf-8 -*-
"""
Constructor, finalizer and action request decorators that define
how the ConverterElement() actually convert the xml.ElementTree nodes.
"""
from gwio.utils.uuid import get_namespace, get_uuid
from gwio.ext.decorators import static_vars


@static_vars(namespace=None)
def fimea_uuid(value=None):
    """
    Create UUID in predetermined FIMEA namespace

    :param value: str, value to get UUID from
    :return: UUID
    """
    assert value is not None
    if fimea_uuid.namespace is None:
        fimea_uuid.namespace = get_namespace('FIMEA Laaketietokanta')
    return get_uuid(value, ns=fimea_uuid.namespace)


# This called as if it were an instance method -mte
# pylint: disable=W0212
def add_foreign_key(self=None, foreign=None):
    """
    Add a reference to foreign into self
    """
    assert foreign is not None
    assert self is not None
    key, value = foreign.get_reference()
    self._values[key] = value
    # noinspection PyProtectedMember
    self._parser.add_edge(foreign._storage_name, self._storage_name)
