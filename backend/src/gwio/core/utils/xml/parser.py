# -*- coding: utf-8 -*-
"""
Parse XML input into ElementTree, but remove namespace from Element tag.
"""
from xml.etree.ElementTree import ElementTree

from io import StringIO
from defusedxml.ElementTree import iterparse


# noinspection PyClassHasNoInit
class Parse:
    """
    Convert Fimea medical database xml into python xml element tree.

    To make processing easier for the converter, removes namespace
    from element tags
    """

    @staticmethod
    def _parse(instream):
        root = None
        namespaces = []
        for event, element in iterparse(instream,
                                        events=('start', 'start-ns')):
            if event == 'start' and root is None:
                root = element
            elif event == 'start-ns':
                ns = '{{{0}}}'.format(element[1])
                namespaces.append(ns)
        return root, namespaces

    @staticmethod
    def _strip_namespaces(element, namespaces):
        for ns in namespaces:
            element.tag = element.tag.replace(ns, '')
        for child in element:
            Parse._strip_namespaces(child, namespaces)
        return element

    @staticmethod
    def _parse_and_remove_namespaces(source):
        return Parse._strip_namespaces(*Parse._parse(source))

    @classmethod
    def from_stream(cls, stream):
        """
        Parse xml from stream

        :param stream: IOStream
        :return: xml.ElementTree
        """
        return ElementTree(Parse._parse_and_remove_namespaces(stream))

    @classmethod
    def from_file(cls, filename):
        """
        Parse xml from file

        :param filename: filename
        :return: xml.ElementTree
        """
        return ElementTree(Parse._parse_and_remove_namespaces(filename))

    @classmethod
    def from_string(cls, s):
        """
        Parse xml from string
        :param s: str
        :return: xml.ElementTree
        """
        return ElementTree(Parse._parse_and_remove_namespaces(StringIO(s)))
