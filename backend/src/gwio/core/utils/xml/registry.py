# -*- coding: utf-8 -*-
"""
Element and entity registration. Difference between the element and entity is that
elements are something we run into while parsing the xml node tree and entities
are simply triggered by actions/relations in elements
"""

# The registry is implemented as module global -mte
# pylint: disable=W0603
_registry = {}


# pylint: disable=C0111

# noinspection PyMissingOrEmptyDocstring
def init_registry(xml_scheme):
    global _registry

    if xml_scheme not in _registry:
        _registry[xml_scheme] = {'entities': {}, 'paths': {}}


# noinspection PyMissingOrEmptyDocstring
def register_entity(xml_scheme, name, cls):
    global _registry

    _registry[xml_scheme]['entities'][name] = cls


# noinspection PyMissingOrEmptyDocstring
def register_element(xml_scheme, name, path, cls):
    global _registry

    register_entity(xml_scheme, name, cls)
    _registry[xml_scheme]['paths']['.'.join(path)] = cls


# noinspection PyMissingOrEmptyDocstring
def all_registered_entities(xml_scheme):
    global _registry

    return _registry[xml_scheme]['entities'].keys()


# noinspection PyMissingOrEmptyDocstring
def get_path_entity(xml_scheme, path):
    global _registry

    return _registry[xml_scheme]['paths'][path]
