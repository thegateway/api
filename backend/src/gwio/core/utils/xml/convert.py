# -*- coding: utf-8 -*-
"""
Convert parsed (ElementTree) fimea.fi  medical info tree into dictionary
objects that can then be further processed and saved into the database

NOTE! This will only return the base objects, relations need to be
separately built.
"""
from __future__ import print_function

from gwio.utils.tsort import tsort
from .registry import get_path_entity


# number of instance variables is ok -mte
# pylint: disable=R0902
class Converter:
    """
    Implements the actual converter that converts xlm.ElementTree into
    FimeaElement() and DatabaseElement() structure that can then be used
    for further processing / importing.
    """

    def __init__(self, xml_scheme=None):
        assert xml_scheme is not None

        self._path = []
        self._containers = []
        self._elements = {}
        self._indexed_elements = {}
        self._edges = []
        self._actions = []
        self._seen = {}

        self.xml_scheme = xml_scheme

    def _reset(self):
        self._path = []
        self._containers = []
        self._elements = {}
        self._indexed_elements = {}
        self._edges = []
        self._actions = []
        self._seen = {}

    def _get_stacks(self):  # used only by unit tests
        return self._elements.keys()

    def _get_values(self, key=None):  # used only by unit tests
        assert key is not None
        return [x.values() for x in self._elements[key]]

    def _all_values(self):
        values = {}
        for stack in self._elements:
            values[stack] = [x.values() for x in self._elements[stack]]
        return values

    def _convert_elements(self, node):
        self._path.append(node)
        element = get_path_entity(self.xml_scheme, '.'.join([x.tag for x in self._path]))(converter=self, node=node)

        element.process(node)

        for child in node:
            self._convert_elements(child)

        element.finalize()
        del self._path[-1]

    def _run_actions(self):
        for action in self._actions:
            action()
        self._actions = []

    def add_action(self, fn: callable) -> None:
        """
        Add a function that will be executed after the whole xml tree has been processed
        """
        self._actions.append(fn)

    def add_edge(self, a: str, b: str) -> None:
        """
        Add a relation from element a to b as graph edge. The final
        graph is used to determine the order in which to load the parsed
        data into the database.
        """
        if [a, b] not in self._edges:
            self._edges.append([a, b])

    def remove_edge(self, edge: str) -> None:
        """
        Remove a relation (used by postprocessing)
        """
        # noinspection PyUnresolvedReferences
        self._edges = [[x[0], x[1]] for x in self._edges if edge not in [x[0], x[1]]]

    def add_seen(self, element: str) -> None:
        """
        Add xml node / element to list of seen elements (see elements_seen())
        """
        self._seen[element] = self._seen.get(element, 0) + 1

    def elements_seen(self):
        """
        Used by unittests to check that all defined xml elements have been encountered in the test data.
        """
        return self._seen.keys()

    def get_import_order(self):
        """
        Get the order in which the parsed data needs to be loaded into
        the database so that reference integrity is maintained.

        :return: list of str, ordered list of data element type names
        """
        return tsort(self._edges)

    def convert(self, etree):
        """
        Run the conversion and then creates python nested
        dictionary from the values

        :param etree: xml.ElementTree
        :returns: dict, the xml content in python dictionary
        """
        self._reset()
        self._convert_elements(etree.getroot())
        self._run_actions()
        return self._all_values()

    def get_or_add_index(self, stack, key, element):
        """
        Add element to element index
        """
        if stack not in self._indexed_elements:
            self._indexed_elements[stack] = {}
        if key not in self._indexed_elements[stack]:
            self._indexed_elements[stack][key] = element
        return self._indexed_elements[stack][key]

    def get_container(self):
        """
        Get the topmost (latest created/pushed) container
        """
        try:
            return self._containers[-1]
        except IndexError:
            return None

    def push_container(self, container):
        """
        Save the container as the top of the container stack
        """
        self._containers.append(container)

    def pop_container(self, container=None):
        """
        Return (and remove from stack) the topmost container. Giving the container
        can be used as safe-guard for only removing "known" topmost container.
        """
        if container is not None:  # pragma: no branch
            assert self._containers[-1] == container
        return self._containers.pop()

    def push_element(self, stack, element):
        """
        Push the element as topmost element in named stack
        """
        if stack not in self._elements:
            self._elements[stack] = []
        self._elements[stack].append(element)

    def pop_element(self, stack, element=None):
        """
        Return (and remove) topmost element from named stack
        """
        if element is not None:  # pragma: no branch
            assert self._elements[stack][-1] == element
        self._elements[stack].pop()

    def get_top_element(self, stack):
        """
        Return (but do not remove) topmost element from named stack
        """
        try:
            return self._elements[stack][-1]
        except KeyError:
            return None
