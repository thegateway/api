# coding=utf-8
"""Flask views for user authentication related endpoints."""
import importlib
import logging
import random

from botocore.exceptions import (
    ClientError,
    ParamValidationError,
)
from flask import (
    current_app,
    g,
    json,
    make_response,
    request,
)
# noinspection PyPackageRequirements
from jose import JOSEError
# noinspection PyPackageRequirements
from jose.jws import get_unverified_claims
from jwt import ExpiredSignatureError
from warrant import (
    Cognito,
    snake_to_camel,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    Unauthorized,
)

from gwio import user as core_user
from gwio.utils import uuid
from gwio.utils.aws import (
    CognitoFederatedIdentitiesClient,
    CognitoIdpClient,
)
from gwio.utils.integration import IntegrationTokenVerifier

logger = logging.getLogger(__name__)
COGNITO_ERROR_LOGGERS = {'NotAuthorizedException': logger.info,
                         'UserNotConfirmedException': logger.info,
                         'UserNotFoundException': logger.info,
                         'PasswordResetRequiredException': logger.info}


def add_to_customer_group(user: core_user.User):
    if CognitoIdpClient.Group.CUSTOMER.value not in user.groups:
        current_app.customer_pool.add_user_to_group(user.guid, CognitoIdpClient.Group.CUSTOMER)


def replace_existing_idp_guid_references(user_guid: str, identity_id: str):
    logger.info('Migrating user guids to identity ids')

    user_guid_fields = [('gwio.remppa.models.project', 'Project', ['people', 'identity_id']),
                        ('gwio.remppa.models.user_task', 'UserTask', 'identity_id'),
                        ('gwio.remppa.models.user_project', 'UserProject', 'identity_id'),
                        ('gwio.models.orders', 'Order', 'buyer_identity_id'),
                        ('gwio.models.residence', 'Residence', 'owner_identity_id')]
    for module_name, table_name, field_name in user_guid_fields:
        try:
            # Some of the tables having user guids are vertical specific. Skip those for other verticals
            module = importlib.import_module(module_name)
        except ModuleNotFoundError:
            continue

        table = getattr(module, table_name)
        logger.info('Migrating table %s', table_name)

        try:
            for row in table.scan():
                field_name_copy = field_name
                if isinstance(field_name_copy, list):
                    field_name_copy = field_name.copy()
                    field = row
                    while len(field_name_copy) > 1:
                        field = getattr(field, field_name_copy.pop(0))
                    if isinstance(field, list):
                        for entry in list(field):
                            field = getattr(entry, field_name_copy[0])
                            if field == user_guid:
                                logger.info('Replacing user guid %s with identity id %s in table %s', str(user_guid),
                                            identity_id, table_name)
                                setattr(entry, field_name_copy[0], identity_id)
                                row.save()
                else:
                    field = getattr(row, field_name_copy)
                    if field == user_guid:
                        logger.info('Replacing user guid %s with identity id %s in table %s', str(user_guid), identity_id,
                                    table_name)
                        setattr(row, field_name_copy, identity_id)
                        row.save()
        except Exception as e:
            if 'Requested resource not found' in str(e):
                continue
            raise


def set_identity_id(client: CognitoFederatedIdentitiesClient, iss: str, id_token: str):
    g.user.set_identity_id(client, iss, id_token)
    # Replace the existing references to the cognito identity provider user id
    replace_existing_idp_guid_references(g.user.guid, g.user.identity_id)


def change_temporary_password(pool, user_id, name, proposed_password, session_id):
    try:
        pool.change_temporary_password(user_id, name, proposed_password, session_id)
    except ClientError as e:
        COGNITO_ERROR_LOGGERS.get(e.response['Error']['Code'], logger.exception)(e)
        raise Unauthorized() from e
    except ParamValidationError as e:
        logger.info(e)
        raise Unauthorized() from e
    return make_response('', 204)


def create_user_without_password(pool, **user_attrs):
    chars = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjklmnpqrstuvwxyz123456789!.+?:-_'
    temp_passwd = ''.join(random.sample(chars, 5) + random.sample(chars, 5))
    try:
        u = Cognito(pool.pool_id, pool.client_id)
        try:
            username = user_attrs['phone_number']
        except KeyError:
            username = user_attrs['email']
        result = u.admin_create_user(username, temp_passwd, **user_attrs)
        # TODO: Create audit event for billing purposes!
        current_app.customer_pool.add_user_to_group(result['User']['Username'], CognitoIdpClient.Group.CUSTOMER)
        return dict(user_id=result['User']['Username'], username=username)
    except (AttributeError, KeyError, ParamValidationError) as e:
        logger.error(e)
        raise BadRequest(e)
    except ClientError as e:
        logger.fatal(e)
        raise Conflict(e)


def _make_token_response(resp):
    ret = dict()
    for token in ('access_token', 'refresh_token', 'id_token'):
        ret[token] = resp['AuthenticationResult'].get(snake_to_camel(token))
    g.identity = verify_token(ret['id_token'])
    setup_cognito_user()
    ret['identity_id'] = g.user.identity_id
    return ret


def setup_cognito_user():
    def _as_uuid(value):
        if value is None or isinstance(value, uuid.UUID):
            return value
        return uuid.UUID(str(value))

    groups = g.identity.get('cognito:groups', list())
    organization_guid = g.identity.get('custom:organization_guid', None)

    for claim in ('cognito:username', 'username', 'sub'):
        try:
            guid = _as_uuid(g.identity[claim])
        except (KeyError, ValueError, TypeError):
            pass
        else:
            g.user = core_user.User(
                guid=guid,
                organization_guid=_as_uuid(organization_guid),
                groups=groups,
                email=g.identity.get('email'),
                name=g.identity.get('name'),
                phone=g.identity.get('phone_number'),
                pin=g.identity.get('pin'),
            )
            logger.debug("Authorized as cognito user [%s]", g.user.guid)
            g.log_events_util.audit_event.subject = g.user
            break
    else:
        g.user = None


def setup_integration_shopkeeper(shop_guid: uuid.UUID):
    g.user = core_user.IntegrationUser(
        guid=shop_guid,
        organization_guid=shop_guid,
        groups=['shopkeeper']
    )
    g.log_events_util.audit_event.subject = g.user
    logger.debug("Authorized as  user for organization ID [%s]", g.user.organization_guid)


def verify_token(token):
    claim = json.loads(get_unverified_claims(token).decode('utf-8'))
    issuer = claim['iss']
    verifier = get_verifier(issuer)
    identity = verifier.verify_token(token)
    return identity


def get_verifier(issuer: str):
    try:
        verifier = current_app.token_issuers[issuer]
        return verifier
    except KeyError:
        if issuer.startswith("shop:"):
            from gwio.models.webshops import Webshop
            shop_guid = uuid.UUID(issuer[5:])
            shop = Webshop.get(guid=shop_guid)
            key = shop._private_data.secret_key
            return IntegrationTokenVerifier(key)
        else:
            raise KeyError


def authenticate():
    # pylint: disable=C0111
    g.user = None
    try:
        authorization_header = request.headers['Authorization']
        bearer_header, token = authorization_header.split(' ', 1)
        if bearer_header != 'Bearer':
            return
        g.identity = verify_token(token)
    except (JOSEError, LookupError, ExpiredSignatureError) as e:
        logger.info(e)
        return
    except Exception as e:
        logger.exception(e)
        return

    issuer = g.identity['iss']
    if issuer in current_app.token_issuers:
        setup_cognito_user()
    elif issuer.startswith('shop:'):
        setup_integration_shopkeeper(uuid.UUID(issuer[5:]))
