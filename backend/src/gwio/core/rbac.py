# coding=utf-8
"""Contains functions for rbac-like implementation."""
from functools import wraps
from typing import (
    Callable,
    Union,
)
from uuid import UUID

from flask import g
from gwio.environment import Env


def when_authenticated(fn: Callable[[Union[UUID, None]], bool]) -> Callable[[Union[UUID, None]], bool]:
    # need to use *args and **kwargs as the name of the argument changes
    # TODO: change the semantics for the rbac.X() so that the parameter is always guid ?
    @wraps(fn)
    def _wrapped(*args, **kwargs) -> bool:
        if getattr(g, 'user', None) is None:
            return False
        return fn(*args, **kwargs)

    return _wrapped


@when_authenticated
def admin(*_, **__) -> bool:
    from gwio.utils import aws

    return aws.CognitoIdpClient.Group.ADMIN.value in g.user.groups


def check_authorization(group, attr_name, guid):
    if group not in g.user.groups:
        return False
    return guid is None or getattr(g.user, attr_name) == guid


@when_authenticated
def shopkeeper(shop_guid: UUID = None, **_) -> bool:
    # TODO for enabling admin and check that the shop_id matches the shop_id in url
    from gwio.utils import aws

    return check_authorization(aws.CognitoIdpClient.Group.SHOPKEEPER.value, 'organization_guid', shop_guid)


@when_authenticated
def customer(customer_guid: str = None, identity_id: Union[str, UUID] = None, **_) -> bool:
    from gwio.utils import aws

    if identity_id is not None:
        if isinstance(identity_id, UUID):
            identity_id = f'{Env.AWS_REGION}:{identity_id}'
        return check_authorization(aws.CognitoIdpClient.Group.CUSTOMER.value, 'identity_id', identity_id)
    return check_authorization(aws.CognitoIdpClient.Group.CUSTOMER.value, 'guid', customer_guid)


@when_authenticated
def product_owner(brand_guid: UUID = None, **_) -> bool:
    from gwio.utils import aws

    return check_authorization(aws.CognitoIdpClient.Group.PRODUCT_OWNER.value, 'organization_guid', brand_guid)


ALL = (product_owner, customer, shopkeeper, admin)


def _any():
    return True


ANY = _any
