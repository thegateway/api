import base64
from urllib.parse import (
    urlencode,
    urljoin,
    urlparse,
)
from uuid import UUID

from flask import (
    current_app,
    g,
)
from gwio.environment import Env
from marshmallow import (
    ValidationError,
    fields,
    post_dump,
)
from werkzeug.exceptions import NotFound

from gwio import user as gwio_user
from gwio.core import rbac
from gwio.core.api.app import app
from gwio.core.decorators.api import (
    GET,
    POST,
    PUT,
)
from gwio.core.cw_logging import LogEventType
from gwio.utils.email import EMail
from gwio.ext.marshmallow import GwioSchema
from gwio.models import user


def _admin_or_self(user_data_guid, **_):
    """Requires admin credentials or must request own data"""
    if not (rbac.admin() or g.user.guid == user_data_guid):
        raise NotFound


def get_user_data(user_guid: UUID):
    try:
        user_data = user.UserData.get(guid=user_guid)
    except user.UserData.DoesNotExist:
        user_data = user.UserData(guid=user_guid)

    return user_data


@app.register_endpoint(
    user.UserData,
    route='/users/<uuid:guid>/data',
    method=GET,
    autoload=False,
    output_schema=user.UserData.Schemas.Get,
    authorisation=_admin_or_self,
)
def user_data_get(guid):
    """
    summary: Get user data
    description: User data is used to store extra non-starndard attributes for a user.
    tags:
        - User
    """
    user_data = get_user_data(guid)
    g.log_events_util.audit_object = user_data
    return user_data


@app.register_endpoint(
    user.UserData,
    route='/users/<uuid:guid>/data',
    method=PUT,
    autoload=False,
    output_schema=user.UserData.Schemas.Get,
    input_schema=user.UserData.Schemas.Put,
    authorisation=_admin_or_self,
)
def user_data_put(guid, **values):
    """
    summary: Update user data
    description: User data is used to store extra non-starndard attributes for a user.
    tags:
        - User
    """
    user_data = get_user_data(guid)
    g.log_events_util.audit_object = user_data

    for attr_name, value in values.items():
        setattr(user_data, attr_name, value)
    user_data.save()

    return user_data


class InviteSchema(GwioSchema):
    """
    name: Invitation
    description: Contact information for the user to be invited.
    """
    person_email = fields.String(attribute='email')
    person_phone_number = fields.String(attribute='phone_number')
    register_url = fields.Url(required=True)

    @post_dump
    def check_person(self, values, **kwargs):
        filter_keys = ['email', 'phone_number']
        person_fields = [key for key in filter_keys if key in values.keys()]
        if not person_fields:
            raise ValidationError(f'At least one of {", ".join(filter_keys)}')
        return values


class InviteResponse(GwioSchema):
    identity_id = fields.String()


@app.register_endpoint(
    gwio_user.User,
    route='/users/invite',
    method=POST,
    event_type=LogEventType.INVITE,
    output_schema=InviteResponse,
    input_schema=InviteSchema,
    authorisation=rbac.ALL)
def invite_user(register_url, **user_data):
    """Invite user to service"""
    identity = current_app.identity_pool.get_id()
    identity_id = base64.urlsafe_b64encode(identity.encode('utf-8'))
    register_url_for_identity = register_url + ('&' if urlparse(register_url).query else '?') + urlencode(dict(identityId=identity_id))
    service_url = urljoin(register_url, '/')
    # TODO: Send a link to the registration page with the identity id to the user
    EMail().send_templated_email(
        src=Env.SYSTEM_EMAIL,
        dst=user_data['email'],
        template='invitation',
        context=dict(service_name=service_url, service_url=service_url, url_token=register_url_for_identity),
        cc=None,
        bcc=None,
        subject="Invitation",
        reply_to=None,
        charset='UTF-8',
        jinja_env=None
    )
    return dict(identity_id=identity)
