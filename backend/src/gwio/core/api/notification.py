from flask import make_response
from marshmallow import (
    ValidationError,
    fields,
    post_load,
)
from werkzeug.exceptions import (
    Conflict,
    Unauthorized,
)

from gwio.core import rbac
from gwio.core.api.app import app
from gwio.core.decorators.api import (
    AuditObject,
    GET,
    POST,
)
from gwio.utils.notify import Notification
from gwio.core.cw_logging import LogEventType
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)


class FcmNotification(GwioSchema):
    registration_key = fields.String(required=True)
    device_name = fields.String()


class WebpushNotificationKeys(GwioSchema):
    p256dh = fields.String()
    auth = fields.String()


class WebpushNotification(GwioSchema):
    endpoint = fields.Url(required=True)
    expirationTime = fields.Number(allow_none=True)
    keys = fields.Nested(WebpushNotificationKeys)


_TYPE_DATA_CHECK = {Notification.Type.FCM_PUSH: FcmNotification,
                    Notification.Type.WEBPUSH: WebpushNotification}


class NotificationSubscribeSchema(GwioSchema):
    type = GwioEnumField(enum=Notification.Type, by_value=True, required=True, attribute='notification_type')
    data = fields.Dict(required=True)


class NotificationTypes(GwioSchema):
    """
    name: notification types
    description: list of availabe notification types.
    """
    notification_types = fields.List(fields.Nested(NotificationSubscribeSchema), many=True, required=True)

    # pylint: disable=no-self-use

    @post_load
    def check_type_for_data(self, types, **kwargs):
        try:
            for data in [t for t in types['notification_types']]:
                try:
                    _TYPE_DATA_CHECK[data['notification_type']]().load(data['data'])
                except ValidationError as e:
                    raise Conflict(f'Notification subscription data does not match expected: {e.messages}') from e
        except KeyError:
            # No check needed for the type
            pass
        return types


def check_customer_access(*, user, **_):
    """Requires admin or customer role"""
    if not (rbac.admin() or rbac.customer(user.guid)):
        raise Unauthorized


@app.register_endpoint(
    AuditObject.SELF,
    route='/notifications/types',
    method=GET,
    output_schema=NotificationSubscribeSchema(many=True),
    authorisation=check_customer_access,
)
def get_notification_subscriptions(user):
    """Get all notification subscription types"""
    try:
        subscriptions = user.notifications['types']
    except KeyError:
        return []
    return [dict(notification_type=Notification.Type(n['type']), data=n['data']) for n in subscriptions]


@app.register_endpoint(
    AuditObject.SELF,
    route='/notifications/types',
    method=POST,
    event_type=LogEventType.UPDATE,
    output_schema=None,
    input_schema=NotificationTypes,
    authorisation=check_customer_access,
    description=f"""Add notification subscription types.

Supported types: {', '.join([notification_type.value for notification_type in list(Notification.Type)])}""")
def subscribe_with_notification_type(user, notification_types):
    user.add_notification_types(
        map(lambda x: dict(type=x['notification_type'].value, data=x['data']), notification_types))
    return make_response('', 204)
