# -*- coding: utf-8 -*-
"""
Core Flask App.
"""
import getpass
import logging
import os
import re
import socket
from collections import defaultdict
from typing import (
    Callable,
    Union,
)

import yaml
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from envs import env
from flask import (
    Flask,
    g,
    request,
)
from flask_apispec import (
    MethodResource,
)
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from gwio.environment import Env
from marshmallow import ValidationError
from webargs.flaskparser import parser
from werkzeug.exceptions import Conflict
from yaml.scanner import ScannerError

from gwio.cache.redis import RedisBackedCache
from gwio.core import rbac
from gwio.core.authentication import authenticate
from gwio.core.decorators.api import endpoint
from gwio.utils.aws import (
    CognitoFederatedIdentitiesClient,
    CognitoIdpClient,
    running_in_lambda,
)
from gwio.core.cw_logging import AuditRequestsUtil
from gwio.utils.logging import configure as configure_devops_logging
from gwio.utils.strings import camel_to_snake

logger = logging.getLogger(__name__)

PKG_NAME = "gwio-api"

try:
    import pkg_resources

    __version__ = pkg_resources.get_distribution(PKG_NAME).version
except Exception:
    __version__ = 'UNKNOWN'


def generate_auth_doc(authorisation, method_spec):
    security_schemes = {
        rbac.admin: "admin_auth",
        rbac.customer: "customer_auth",
        rbac.product_owner: 'product_owner_auth',
        rbac.shopkeeper: "shopkeeper_auth"
    }
    if callable(authorisation):
        try:
            method_spec['security'] = [security_schemes.get(authorisation, 'custom_auth')]
        except KeyError:
            logger.fatal(f'{authorisation} does not have a security scheme set')
    else:
        method_spec['security'] = [security_schemes[k] for k in authorisation]


@parser.error_handler
def handle_input_schema_error(error: Union[ValidationError, ValueError], *_) -> Conflict:
    """
    Error handler for webargs parser so that client gets meaningful error messages in case when Schema input
     validation fails
    """
    raise Conflict(error.messages) from error


def fix_url_parts(url):
    return re.sub(r'<(?:(?P<type>[a-z_]+):)?(?P<name>[a-z_]+)>', r'{\2}', url)


class App(Flask):
    # pylint: disable=C0111
    common_config = {
        'APISPEC_WEBARGS_PARSER': parser,
    }

    endpoints_without_audit = AuditRequestsUtil.endpoints_without_audit

    register_endpoint = endpoint

    def __init__(self, import_name, *, conf, root_path=None):
        """
        Flask app wrapper to handle setup and contain FlaskApiSpec and Marshmallow within the app namespace.
        """
        self.endpoints_without_audit.extend([
            '/swagger/',
            '/swagger-ui/',
            '/flask-apispec/',
            '/static/<path:filename>',
            '/flask-apispec/static/<path:filename>',
        ])

        super().__init__(import_name, root_path=root_path)
        self.config.update(self.common_config)
        self.config.update(conf)
        # It is easier just store all the things to be documented into a dictionary and create the APISpec when the
        # swagger.json is requested.
        self.documentation = defaultdict(lambda: defaultdict(dict))
        self.endpoints_for_lazy_document_generation = set()
        self.doc_endpoints = dict()
        self.documentation['version'] = __version__
        self.redis_cache = RedisBackedCache(self)

        self.ma = Marshmallow(self)
        self.token_issuers = dict()
        self.setup_login()

        self.before_request(self._audit_setup)
        self.after_request(self._audit_finalize)

        CORS(self)
        # Let's not have boto spamming the log
        logging.getLogger('boto').setLevel(logging.ERROR)
        logging.getLogger('botocore').setLevel(logging.ERROR)
        logging.getLogger('boto3').setLevel(logging.ERROR)

        # Additional logging configuration (only effective if we are running in lambda)
        configure_devops_logging(devops_level=env('DEVOPS_LOG_LEVEL', 'WARNING'))

    @staticmethod
    def _audit_setup():
        # Initialise logging for the request
        g.aws_request_id = request.headers.get('X-Amazon-Request-ID')
        g.log_events_util = AuditRequestsUtil()
        if request.url_rule and request.url_rule.rule in App.endpoints_without_audit:
            g.log_events_util._audit_disabled = True

    @staticmethod
    def _audit_finalize(response):
        g.log_events_util.audit_event.response = response
        g.log_events_util.save()
        return response

    def setup_login(self):
        self.customer_pool = CognitoIdpClient(pool_id=Env.COGNITO_POOL_ID,
                                              client_id=Env.COGNITO_CLIENT_ID)
        self.identity_pool = CognitoFederatedIdentitiesClient(Env.COGNITO_FEDERATED_IDENTITY_POOL_ID)
        self.token_issuers[self.customer_pool.issuer_name] = self.customer_pool

    # pylint: disable=too-many-locals
    def register_resource(self, resource_class: MethodResource) -> MethodResource:
        """
        Decorator for Method resources, adds URL rules and registers to APISpec.
        :param resource_class: class to be decorated
        """
        _basepath = getattr(resource_class, '_basepath', None)
        if _basepath is not None:
            slug = _basepath[1:] if _basepath.startswith('/') else _basepath
        else:
            name = resource_class.__name__
            if name.endswith('API'):
                name = name[:-3]
            slug = self.slugify(name)
        try:
            # noinspection PyUnresolvedReferences
            objnames = resource_class.objname if isinstance(resource_class.objname, list) else [resource_class.objname]
        except AttributeError:
            objnames = ['{name}_id'.format(name=slug[:-1])]
        path_names = [slug]
        for s in objnames:
            if s.startswith('/'):
                path_names.append(s)
        endpoint = '_'.join(path_names)
        view_func = resource_class.as_view(endpoint)
        rud_methods = [method.upper() for method in ('put', 'get', 'delete') if hasattr(resource_class, method)]
        names = [name[1:] if name.startswith('/') else '<{}>'.format(name) for name in objnames]
        ids = '/'.join(names)
        rule = f'/{slug}/{ids}/'
        self.add_url_rule(rule, view_func=view_func,
                          methods=rud_methods, strict_slashes=False)
        for meth_ in rud_methods:
            view = getattr(resource_class, meth_.lower())
            self.doc_endpoints[view] = rule
        post_ids = '/'.join(names[:-1])
        post_slug = f'/{slug}/{post_ids}/' if post_ids else f'/{slug}/'
        try:
            view = getattr(resource_class, 'post')
            self.add_url_rule(post_slug, view_func=view_func, methods=['POST'], strict_slashes=False)
            self.doc_endpoints[view] = rule
        except AttributeError:
            pass

        return resource_class

    def parse_plain_dosctring(self, docstring, method_spec):
        try:
            summary, *descs = docstring.strip().splitlines()
            descr_ = '\n'.join(descs)
        except AttributeError:
            summary = ''
            descr_ = ''
        method_spec['summary'] = summary
        method_spec['description'] = descr_

    def generate_endpoint_documentation(
            self,
            route,
            view):
        authorisation = view.__spec__.get('authorisation')
        input_schema = view.__spec__.get('input_schema')
        output_schema = view.__spec__.get('marshal_schema')
        http_method = view.__spec__.method
        function_docstring = view.__doc__
        method_spec = dict()
        try:
            method_spec['tags'] = [view.__spec__.klass.__name__]
        except AttributeError:
            method_spec['tags'] = ['Documentation']
        try:
            doc_ = yaml.load(function_docstring)
            if isinstance(doc_, dict):
                method_spec.update(doc_)
            else:
                self.parse_plain_dosctring(function_docstring, method_spec)

        except (TypeError, KeyError, AttributeError, ScannerError):
            self.parse_plain_dosctring(function_docstring, method_spec)

        if 'security_schemes' not in method_spec and authorisation:
            generate_auth_doc(authorisation, method_spec)

        if route is not None:
            url = fix_url_parts(route)
            if output_schema:
                method_spec['responses'] = {
                    "200": {
                        'content': {
                            "application/json": {
                                "schema": output_schema
                            }
                        }
                    }
                }
            if input_schema:
                try:
                    parsed_doc = yaml.load(input_schema.__doc__)
                    description = parsed_doc['description']
                except AttributeError as e:  # "'NoneType' has no attribute 'read'"
                    raise RuntimeError(f'{input_schema} has no docstring') from e
                except TypeError:  # "string idices must be integers"
                    logger.error(f'{input_schema} docstring is malformed, most likely a bare string not a yaml mapping')
                    logger.debug(input_schema.__doc__)
                    description = input_schema.__doc__
                if http_method.name in ('POST', 'PUT'):
                    method_spec['requestBody'] = {
                        "description": description,
                        "content": {
                            "application/json": {
                                "schema": input_schema
                            }
                        },
                        "required": True
                    }
                else:
                    # TODO: Fill in query params
                    pass

            self.documentation[url][http_method.value] = method_spec

    @staticmethod
    def path_params(*args: str) -> Callable[[MethodResource], MethodResource]:
        def _set_id(resource_class: MethodResource) -> MethodResource:
            resource_class.objname = list(args)
            return resource_class

        return _set_id

    # pylint: disable=protected-access
    @staticmethod
    def base_path(arg: str) -> Callable[[MethodResource], MethodResource]:
        def _set_base(resource_class: MethodResource) -> MethodResource:
            resource_class._basepath = arg
            return resource_class

        return _set_base

    @staticmethod
    def slugify(name: str) -> str:
        """
        Converts CamelCase name to snake_case so that URL:s are more conventional
        """
        return camel_to_snake(name)


# noinspection SpellCheckingInspection
DEFAULT_JWKS = {
    "keys": [
        {"alg": "RS256", "e": "AQAB", "kid": "ZHnx604rL22rwNDT3uoKBj2YmwP/mvCWzu5klXvavWc=", "kty": "RSA",
         "n": "zN_GiKi4-_1iPEp_zVTplr4xThgjfnF5FK4tAa0mXogEYeVf-2kDuuvvrjkAdrsiX8WK7ej9SqlDfdzS3ki6yyen66zyFKc_Oni49Xph"
              "3D59o4N34ThGkQrLNIFk6GCVmp510ujiNtsMy55Jg3ImtJP0b0imAe4A0MdXTN-LXsASejuCZNXGD4G0zz4llzc11Xp90xIYJ9gnOJqm"
              "w6pGr36dcK6YClFkvHcbGY6_huLJ3I4QL5Ws6qUGtT02AuCcZVoR4KC1MQ_T0QauG53z9WzQ15vM160Ojo44CfY14p3HUnLJFQS7J2nP"
              "PeWQAIFYh5n4vTdme7G1wOHKLy-GUQ",
         "use": "sig"},
        {"alg": "RS256", "e": "AQAB", "kid": "bkCmRcsSKMfj0y5iIs0JmeMap5gO2dfVN43+Vuax57E=", "kty": "RSA",
         "n": "j0GE51_X4Vdhe86AmJ7Az5RisTLHyovPZKYFetc-P0D-yyKTjQ1mZwtAZHDDZcUWSrLxANWIk-wevBZluxPyJEXPGYQWWQcmxQN7ZmNN"
              "GCs8HuChwoqn_qLBlH-ZZnTEA1sF7elVo6eVUtM471eYQMOnwiZm073ekHw9V6Ac5akAfYaS-HtGEoUtDlIfFufqss174BhjLqrTtwxG"
              "CdPPBzSFseIzXduRxTobBSa0EWHp02u1H4m_9sBqnOtg_8I3R2EFaW4S_HzmiuYAt8tyOQqWMS4jE6_QJ_nwwNHJJvfDFnVdDY1UAdgp"
              "t_YLC1Q1N-4hdra23xH77YY-fq7EzQ",
         "use": "sig"}]}

# noinspection SpellCheckingInspection
CONFIG = {
    'APISPEC_SPEC': APISpec(
        title="The Reseller Gateway API",
        version=__version__,
        openapi_version='2.0',
        plugins=(
            MarshmallowPlugin(),
        ),
    ),
    'APISPEC_WEBARGS_PARSER': parser,
    'AWS_ZONE': 'eu-west-1',
    'COGNITO_ADMIN_CLIENT_ID': env('COGNITO_ADMIN_CLIENT_ID'),
    'COGNITO_ADMIN_POOL_ID': env('COGNITO_ADMIN_POOL_ID'),
    'COGNITO_CLIENT_ID': env('COGNITO_CLIENT_ID'),
    'COGNITO_POOL_ID': env('COGNITO_POOL_ID'),
    'JWKS': env('COGNITO_JWKS', DEFAULT_JWKS, var_type='dict'),
    'LEGACY_API_HOST': env('LEGACY_API_HOST'),
    'PARTNER_TOKEN_ISSUERS': env("PARTNER_TOKEN_ISSUERS", ["api-test.mehilainen.fi"], var_type="list"),
    'SECRET_KEY': env('SECRET_KEY', os.urandom(32)),
    'SWAGGER_VALIDATOR_URL': env('SWAGGER_VALIDATOR_URL')
}
CONFIG['JWKS']['keys'].extend(env('LEGACY_JWKS', dict(), var_type='dict').get('keys', list()))

app = App(__name__, conf=CONFIG)


@app.before_request
def _authenticate():
    # Must authenticate on all requests for logging
    authenticate()


@app.before_request
def _local_testing():
    try:
        g.aws_event_source = request.environ['lambda.context'].invoked_function_arn
    except KeyError:
        if running_in_lambda():
            raise
        g.aws_event_source = f'{getpass.getuser()}@{socket.gethostname()}'


@app.after_request
def add_version_header(response):
    response.headers['X-Gateway-API-Version'] = "{} {}/{}".format(
        __version__,
        Env.VERTICAL,
        Env.APPLICATION_ENVIRONMENT)

    return response
