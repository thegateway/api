# coding=utf-8
"""Flask views for user authentication related endpoints."""
import logging

import boto3
from botocore.exceptions import (
    ClientError,
    ParamValidationError,
)
from flask import (
    current_app,
    g,
    jsonify,
    make_response,
    request,
)
# noinspection PyPackageRequirements
from jwt import ExpiredSignatureError
from marshmallow import (
    ValidationError,
    fields,
    post_load,
    validate,
)
from warrant import Cognito
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    Unauthorized,
)

from gwio import user as core_user
from gwio.core import rbac
from gwio.core.api.app import app
from gwio.core.authentication import (
    COGNITO_ERROR_LOGGERS,
    _make_token_response,
    add_to_customer_group,
    create_user_without_password,
    set_identity_id,
    verify_token,
)
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import (
    AuditObject,
    DELETE,
    GET,
    POST,
)
from gwio.environment import Env
from gwio.ext.marshmallow import GwioSchema
from gwio.models.user import (
    IdentityStatus,
    UserData,
)
from gwio.user import User
from gwio.utils import uuid
from gwio.utils.aws import CognitoIdpClient
from gwio.utils.notify import (
    UserNotificationType,
)

logger = logging.getLogger(__name__)


def downcase(values, keys):
    for key in keys:
        try:
            values[key] = values[key].lower()
        except AttributeError:
            pass


class TokenSchema(GwioSchema):
    """Marshmallow Schema for Token response"""
    access_token = fields.Str()
    refresh_token = fields.Str()
    id_token = fields.Str()
    identity_id = fields.Str()


class LoginSchema(GwioSchema):
    """
    name: login
    description: username and password"""
    username = fields.Str(required=True)
    password = fields.Str(required=True)

    @post_load
    def strip_username(self, values, **kwargs):
        values['username'] = values['username'].strip()
        return values

    @post_load
    def downcase(self, values, **kwargs):
        downcase(values, ['username'])
        return values


@app.register_endpoint(
    core_user.User,
    route='/login',
    method=POST,
    event_type=LogEventType.LOGIN,
    autoload=False,
    output_schema=TokenSchema,
    input_schema=LoginSchema,
    description="Cognito pool backed username/password login for customers.",
    authorisation=None,
)
def login(username, password):
    """
    summary: Login
    description: Authenticate user
    """
    pool = current_app.customer_pool
    try:
        response = pool.auth_with_password(username, password)
    except ClientError as e:
        COGNITO_ERROR_LOGGERS.get(e.response['Error']['Code'], logger.exception)(e)
        raise Unauthorized() from e
    except ParamValidationError as e:
        logger.info(e)
        raise Unauthorized() from e
    try:
        resp = _make_token_response(response)
        logger.debug(resp)
        if g.user.identity_status is not IdentityStatus.LINKED:
            set_identity_id(current_app.identity_pool, g.identity['iss'], resp['id_token'])
            # Identity id is set in _make_token_response normally but for the first login it's generated after writing the response
            # Fill in the identity id
            resp['identity_id'] = g.user.identity_id
    except KeyError as e:
        logger.error('Failed to create response token')
        raise Unauthorized from e

    add_to_customer_group(g.user)
    return resp


class SocialRegisterSchema(GwioSchema):
    """
    name: Social login registration
    description: Identity token
    """
    identity_token = fields.Str(required=True)


class SocialLoginResponseSchema(GwioSchema):
    """name: Social login registration response
    description: Identity id
    """
    identity_id = fields.Str()


@app.register_endpoint(
    core_user.User,
    route='/social_login',
    method=POST,
    autoload=False,
    output_schema=SocialLoginResponseSchema,
    input_schema=SocialRegisterSchema,
    authorisation=None,
)
def register_social_login(identity_token):
    """Pass the social login token to initialize the user data"""
    try:
        parsed_token = verify_token(identity_token)
    except ExpiredSignatureError:
        raise Unauthorized('Token expired')
    sub = uuid.UUID(parsed_token['sub'])
    username = parsed_token['cognito:username']
    user = core_user.User(guid=sub)
    if user.identity_id is None:
        user.set_identity_id(client=current_app.identity_pool,
                             iss=parsed_token['iss'],
                             id_token=identity_token)
        current_app.customer_pool.add_user_to_group(username, CognitoIdpClient.Group.CUSTOMER)
    return dict(identity_id=user.identity_id)


class RefreshTokenAuthSchema(GwioSchema):
    """
    name: refresh token
    description: device key and refresh token used to acquire a new authorization token.
    """
    refresh_token = fields.String(required=True)
    device_key = fields.String(default=None, required=False)


@app.register_endpoint(
    core_user.User,
    route='/refresh_token',
    method=POST,
    event_type=LogEventType.LOGIN,
    autoload=False,
    output_schema=TokenSchema,
    input_schema=RefreshTokenAuthSchema,
    authorisation=None,
)
def refresh_token_auth(refresh_token, device_key=None):
    """Cognito pool refresh token authentication"""
    pool = current_app.customer_pool
    try:
        response = pool.auth_with_refresh_token(refresh_token, device_key)
    except ClientError as e:
        COGNITO_ERROR_LOGGERS.get(e.response['Error']['Code'], logger.exception)(e)
        raise Unauthorized() from e
    except ParamValidationError as e:
        logger.info(e)
        raise Unauthorized() from e
    resp = _make_token_response(response)
    add_to_customer_group(g.user)
    return resp


class UserIdSchema(GwioSchema):
    """Marshmallow Schema for User identification output"""
    user_id = fields.UUID()
    username = fields.String()


class UserSchema(GwioSchema):
    """
    name: user information
    description: user's information to be updated"""
    password = fields.Str()
    phone_number = fields.Str()
    email = fields.Email()
    name = fields.Str()
    identity_id = fields.Str()

    @post_load
    def downcase(self, values, **kwargs):
        try:
            downcase(values, ['email'])
        except KeyError:
            # email is not required
            pass
        return values


@app.register_endpoint(
    core_user.User,
    route='/register',
    method=POST,
    autoload=False,
    output_schema=UserIdSchema,
    input_schema=UserSchema,
    authorisation=None,
)
def register(**kwargs):
    """
    summary: User registration.
    description: Register a new user.
    """
    pool = current_app.customer_pool
    pool_id = pool.pool_id
    client_id = pool.client_id
    phone_number = kwargs.get('phone_number')
    email = kwargs.get('email')
    password = kwargs.get('password')
    username = email if email else phone_number
    try:
        u = Cognito(pool_id, client_id, user_pool_region=Env.COGNITO_USER_POOL_REGION)
        attrs = {k: v for k, v in kwargs.items() if k in ('phone_number', 'email', 'name') and v}
        u.add_base_attributes(**attrs)
        result = u.register(username, password)
        user_id = result['UserSub']
        current_app.customer_pool.add_user_to_group(user_id, CognitoIdpClient.Group.CUSTOMER)

        try:
            identity_id = kwargs['identity_id']
        except KeyError:
            identity_id = current_app.identity_pool.get_id()
        user_data = UserData(guid=uuid.UUID(user_id), identity_id=identity_id)
        user_data.save()

        user = User(user_data.guid, data=user_data)
        user.API.notify(model=user, notification=UserNotificationType.REGISTRATION_THANKS)

        return dict(user_id=user_id, username=username)
    except (AttributeError, KeyError, ParamValidationError) as e:
        logger.error(e)
        raise BadRequest(e)
    except ClientError as e:
        if 'UsernameExistsException' in str(e):
            logger.info('User "%s" already exists', email)
        elif 'InvalidPasswordException' in str(e):
            logger.debug(e)
        else:
            logger.fatal(e)
        raise Conflict(e)


class RegisterWithoutPassword(GwioSchema):
    """
    name: registration info
    description: user's email or phone number to be used to sign up / invite a new user.
    """
    phone_number = fields.Str()
    email = fields.Str()

    @post_load
    def check_either(self, values, **kwargs):
        if len(values) == 0:
            raise ValidationError(f'One of {", ".join(self.declared_fields.keys())} is required')
        return values

    @post_load
    def downcase(self, values, **kwargs):
        try:
            downcase(values, ['email'])
        except KeyError:
            # email is not required
            pass
        return values


@app.register_endpoint(
    core_user.User,
    route='/register_without_password',
    method=POST,
    autoload=False,
    output_schema=UserIdSchema,
    input_schema=RegisterWithoutPassword,
    authorisation=None,
)
def register_without_password(**user_attrs):
    """User registration without password"""
    pool = current_app.customer_pool
    return create_user_without_password(pool, **user_attrs)


@app.register_endpoint(
    AuditObject.SELF,
    route='/register',
    method=DELETE,
    authorisation=rbac.ALL,
)
def delete_self(user):
    """
    summary: Delete a user.
    description: Allows a logged in user to delete themselves.
    """
    # TODO: add tests
    client = boto3.client('cognito-idp', region=Env.COGNITO_USER_POOL_REGION)
    client.delete_user(AccessToken=request.headers['Authorization'][len('Bearer '):])
    return make_response('', 204)


class ConfirmationSchema(GwioSchema):
    """
    name: confirmation code schema
    description: confirmation code for username (phone number or email)
    """
    username = fields.Str(required=True)
    confirmation_code = fields.String(validate=validate.Regexp('^[0-9]{6}$'), required=True)

    def downcase(self, values):
        downcase(values, ['username'])
        return values


@app.register_endpoint(
    core_user.User,
    route='/confirm_user',
    method=POST,
    event_type=LogEventType.UPDATE,
    autoload=False,
    output_schema=None,
    input_schema=ConfirmationSchema,
    authorisation=None,
)
def confirm_user(**kwargs):
    """
    summary: Sign-up confirmation
    description: Confirm user registration (email / phone)
    """
    pool = current_app.customer_pool
    pool_id = pool.pool_id
    client_id = pool.client_id
    try:
        u = Cognito(pool_id, client_id, username=(kwargs['username']), user_pool_region=Env.COGNITO_USER_POOL_REGION)
        u.confirm_sign_up(kwargs['confirmation_code'])
        # TODO: Create identity here
    except (KeyError, AttributeError, ValidationError) as e:
        logger.error(e)
        raise BadRequest(e)
    except ClientError as e:
        logger.fatal(e)
        raise Conflict(e)


class PasswordResetInputSchema(GwioSchema):
    """
    name: password reset data
    description: data to set a new password
    """
    username = fields.String(required=True)
    confirmation_code = fields.String(required=True)
    new_password = fields.String(required=True)

    def downcase(self, values):
        downcase(values, ['username'])
        return values


@app.register_endpoint(
    core_user.User,
    route='/forgot_password',
    method=POST,
    event_type=LogEventType.UPDATE,
    autoload=False,
    output_schema=None,
    input_schema=PasswordResetInputSchema(exclude=('confirmation_code', 'new_password')),
    authorisation=None,
)
def forgot_password(**kwargs):
    """To trigger end-user's password process."""
    pool = current_app.customer_pool
    try:
        pool.forgot_password(**kwargs)
    except ClientError as error:
        raise BadRequest(error.response['Error']['Message']) from error
    return make_response('', 204)


@app.register_endpoint(
    core_user.User,
    route='/reset_password',
    method=POST,
    event_type=LogEventType.UPDATE,
    autoload=False,
    output_schema=None,
    input_schema=PasswordResetInputSchema(),
    authorisation=None,
)
def reset_password(**kwargs):
    """To finalize the end-user's password reset."""
    pool = current_app.customer_pool
    try:
        pool.confirm_forgot_password(**kwargs)
    except ClientError:
        # Don't send any error so that this cannot be used to figure out whether a email or phone number is registered.
        pass
    return make_response('', 204)


@app.register_endpoint(
    core_user.User,
    route='/whoami',
    method=GET,
    event_type=LogEventType.READ,
    autoload=False,
    output_schema=None,
    authorisation=rbac.ANY
)
def who_am_i():
    pool = current_app.customer_pool
    username = g.identity['cognito:username']
    c = boto3.client("cognito-idp", region=Env.COGNITO_USER_POOL_REGION)
    resp = c.admin_get_user(UserPoolId=pool.pool_id, Username=username)
    return jsonify({d['Name']: d['Value'] for d in resp['UserAttributes']})
