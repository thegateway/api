import uuid

from gwio.ext.eshelpers import (
    BARE_PRODUCT_ES_INDEX,
    WEBSHOP_PRODUCT_ES_INDEX,
)
from gwio.ext.index_es_document import (
    EsEvent,
    log,
    update_es_document,
)
from gwio.models.tags import Tag


def _get_product(product_guid):
    from gwio.models.products import Product

    try:
        product = Product.by_guid(product_guid)
        return product
    except Product.DoesNotExist:
        log.warning('Added product or webshop product for product that does not exist %s', product_guid)
        return None


def _set_brand_name(product):
    product['brand_name'] = None
    if product.get('tags'):
        for tag in product['tags']:
            if tag['type'] == Tag.Type.BRAND:
                product['brand_name'] = tag['name']
                break


def update_webshop_product_es(product, webshop_product):
    from gwio.models.webshop_product import (
        WebshopProduct,
        ProductElasticsearchDocumentSchema,
    )

    evaluated = WebshopProduct.API.Assessor.evaluate(
        product=product,
        webshop_product=webshop_product,
        schema=product.Schemas.Elasticsearch())
    _set_brand_name(evaluated)
    body = ProductElasticsearchDocumentSchema().dump(evaluated)
    update_es_document(index=WEBSHOP_PRODUCT_ES_INDEX,
                       event_type=EsEvent.PRODUCT_SAVE_EVENT,
                       document_id=generate_es_id(product.guid, webshop_product.webshop_guid),
                       body=body)


def update_webshop_products_es(*, product_guid=None, product=None, webshop_products=None):
    from gwio.models.webshop_product import WebshopProduct

    if product is None:
        product = _get_product(product_guid)
    if product is not None and product.type in product.API.searchable_types:
        if webshop_products is None:
            webshop_products = WebshopProduct.query(product_guid=product.guid)

        for webshop_product in webshop_products:
            update_webshop_product_es(product, webshop_product)


def update_product_es(*, product_guid=None, product=None, update_webshop_products=True):
    from gwio.models.webshop_product import WebshopProduct

    if product is None:
        product = _get_product(product_guid)
    if product is not None and product.type in product.API.searchable_types:
        evaluated = product.API.Assessor.evaluate(product=product)
        webshop_products = list(WebshopProduct.query(product_guid=product.guid))
        evaluated['suppliers'] = [wsp.webshop_guid for wsp in webshop_products]
        _set_brand_name(evaluated)
        dumped = product.API.Assessor.EvaluatedProduct.Schemas.Elasticsearch().dump(evaluated)
        update_es_document(index=BARE_PRODUCT_ES_INDEX,
                           event_type=EsEvent.PRODUCT_SAVE_EVENT,
                           document_id=generate_es_id(product.guid),
                           body=dumped)

        if update_webshop_products:
            update_webshop_products_es(
                product=product,
                webshop_products=webshop_products,
            )


def delete_webshop_product(*, product_guid, webshop_product):
    update_es_document(index=WEBSHOP_PRODUCT_ES_INDEX,
                       event_type=EsEvent.PRODUCT_DELETE_EVENT,
                       document_id=generate_es_id(product_guid, webshop_product.webshop_guid))


def delete_webshop_products_es(*, product_guid):
    from gwio.models.webshop_product import WebshopProduct

    for webshop_product in WebshopProduct.query(product_guid=product_guid):
        delete_webshop_product(product_guid=product_guid, webshop_product=webshop_product)


def delete_product_es(*, product=None, product_guid: uuid.UUID = None):
    if product_guid is None:
        if product is None:
            raise ValueError('Either product or product_guid is required')
        product_guid = product.guid
    update_es_document(index=BARE_PRODUCT_ES_INDEX, event_type=EsEvent.PRODUCT_DELETE_EVENT, document_id=generate_es_id(product_guid))
    delete_webshop_products_es(product_guid=product_guid)


def generate_es_id(product_guid: uuid.UUID, webshop_guid: uuid.UUID = None):
    if webshop_guid is None:
        return f'/products/{product_guid}'
    else:
        return f'/webshops/{webshop_guid}/products/{product_guid}'
