# coding=utf-8
import logging
import pprint
import re
import uuid
from datetime import (
    datetime,
    timezone,
)
from enum import Enum
from typing import Any

import boto3
import simplejson
from attrdict import AttrDict
from flask import (
    g,
    json,
    request as flask_request,
)
from marshmallow import (
    fields,
    post_dump,
    post_load,
    pre_dump,
)

import gwio.user
from gwio.ext.index_es_document import send_to_eventbridge
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.security.jwt import (
    _serialise_dict,
    sign,
)

logger = logging.getLogger(__name__)


class LogEventType(Enum):
    CREATE = 'create'
    READ = 'read'
    LIST = 'list'
    UPDATE = 'update'
    DELETE = 'delete'

    INVITE = 'invite'
    LOGIN = 'login'

    ORDER_PLACE = 'order_place'

    PAYMENT_RESERVE = 'payment_reserve'
    PAYMENT_FINALIZE = 'payment_finalize'
    PAYMENT_REFUND = 'payment_refund'

    ACCEPT_TOS = 'accept_tos'


class ResponseSchema(GwioSchema):
    status_code = fields.Int()
    data = fields.String(allow_none=True)  # DELETE requests for example don't respond with any content

    @pre_dump
    def dump_data(self, response, **kwargs):
        response['data'] = json.dumps(response['data'])
        return response

    @post_load
    def load_data(self, response, **kwargs):
        response['data'] = json.loads(response['data'])
        return response


class RequestSchema(GwioSchema):
    guid = fields.UUID(required=True)  # AWS request id
    remote_ips = fields.String(required=True)
    user_agent = fields.String(required=True)
    method = fields.String(required=True)
    url_root = fields.String(required=True)
    url_path = fields.String(required=True)


class EventSchema(GwioSchema):
    class Meta:
        json_module = simplejson

    request_guid = fields.UUID(required=True)
    request = fields.Nested(RequestSchema, required=True)
    timestamp = fields.DateTime(required=True)

    event_type = GwioEnumField(enum=LogEventType, by_value=True, required=True)

    subject_id = fields.String(allow_none=True, default=None)
    subject_type = fields.String(allow_none=True, default=None)

    object_id = fields.String(allow_none=True)
    object_type = fields.String(allow_none=True)
    obj = fields.Dict(allow_none=True, attribute='object', default=dict)

    data = fields.Dict(allow_none=True)

    audit = fields.Boolean(required=True)

    @post_dump
    def convert_object(self, data, **kwargs):
        # This put object inside obj field to avoid conflict in elasticsearch, regarding fields value.
        if data['obj'] is not None:
            data['obj'] = {data['object_type']: data['obj']}
        else:
            data['obj'] = dict()
        return data


class AuditEventSchema(EventSchema):
    response = fields.Nested(ResponseSchema)


class _EventBridgeDetailType(Enum):
    AUDIT_LOG = 'audit_log_event'
    LOG = 'log_event'


# pylint: disable=R0902
class Event:
    schema = EventSchema()
    audit_schema = AuditEventSchema()
    token = None

    class API:
        Type = LogEventType

    # pylint: disable=R0913
    def __init__(
            self, *,
            event_type: LogEventType = None,
            obj: Any = None,
            data: dict = None,
            subject: Any = None,
            event_id: uuid.UUID = None,
            timestamp: datetime = None,
            audit: bool = False,
    ):
        """
        Log event class used by AuditRequestsUtil

        :param event_type:
            The intended action. In most cases reflects the HTTP method.
        :param obj:
            The target resource of the request, should be a subclass of DynaModel
        :param data:
            Some extra data to log when the object dump is not enough.
        :param subject:
            The requester. `g.user` in almost all the cases.
        :param event_id:
            The identifier for the audit event.
        :param timestamp:
            The UTC time for when request was received
        :param audit:
            Is this an audit event or just some other log
        """

        # TODO: Remove the `request` from arguments and mock whatever needs to be mocked by the tests
        # Catching invalid log events in when running tests
        if not audit:
            assert obj
            assert subject
            assert event_type

        try:
            self.request = AttrDict(
                remote_ips=flask_request.headers.get('X-Forwarded-For', '').split(', '),
                user_agent=flask_request.headers.get('User-Agent'),
                guid=flask_request.headers.get('X-Amazon-Request-ID'),
                method=flask_request.method,
                url_root=flask_request.url_root,
                url_path=flask_request.path,
            )
        except (AttributeError, RuntimeError):
            self.request = None

        self.id = event_id if event_id is not None else uuid.uuid4()
        self.timestamp = timestamp if timestamp is not None else datetime.now(timezone.utc)
        if event_type is None:
            try:
                event_type = AuditRequestsUtil.default_audit_event_type_from_crud[self.request.method]
            except KeyError:
                pass
        self.event_type = event_type
        self.subject = subject
        self.object = obj

        self.data = data

        self.audit = audit

    def __str__(self):
        try:
            event = self.marshal()
            event['Detail'] = json.loads(event['Detail'])
            return f'Event:\n{pprint.pformat(event)}'
        except Exception as e:
            logger.exception(e)
            return f'Event with failing __str__: {json.dumps(self.__dict__, default=str)}'

    @property
    def request_guid(self):
        return self.request.guid

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, res):
        if self.audit:
            list_body_replacement = 'Response is list and body was redacted due to size limit issues'
            if self.event_type == LogEventType.LIST:
                response_data = list_body_replacement
            else:
                # This is just an extra measure to make sure we don't try to send list response to EventBridge,
                # because it'd almost always make the request exceed size limits.
                # If response includes `next` and `total` the response is paginated list
                if isinstance(res.json, list) or (isinstance(res.json, dict) and all(x in res.json.keys() for x in ('next', 'total'))):
                    logger.warning(f'`{self.request.method} {self.request.url_path}` is returning a list, but the event type is `{self.event_type.name}`')
                    response_data = list_body_replacement
                else:
                    response_data = res.get_json()

            self._response = AttrDict(
                status_code=res.status_code,
                data=response_data,
            )

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value):
        self._subject = value

    @property
    def subject_id(self):
        try:
            return self.subject.identity_id
        except AttributeError:
            if self.subject is None:
                return None
            return self.subject.guid

    @property
    def subject_type(self):
        if self.subject is None:
            return None
        return self.subject.__class__.__name__

    @property
    def object(self):
        return self._object

    @object.setter
    def object(self, value):
        try:
            obj_dict = value.as_event_context()
            self._object = obj_dict['obj']
            self.object_id = obj_dict['object_id']
            self.object_type = obj_dict['object_type']
        except AttributeError:
            self._object = None
            self.object_id = None
            self.object_type = type(self).__name__

    @staticmethod
    def _calc_entry_size(entry):
        """
        Calculating EventBridge entry size, so we could try to avoid sending an entry that is too large
        https://docs.aws.amazon.com/eventbridge/latest/userguide/calculate-putevents-entry-size.html
        """

        def _byte_count(value):
            if value is None:
                return 0
            return len(value.encode('utf-8'))

        size = 0

        try:
            if entry['Time'] is not None:
                # AWS: If the Time parameter is specified, it is measured as 14 bytes.
                size += 14
        except KeyError:
            pass

        # AWS: The Source and DetailType parameters are measured as the number of bytes for their UTF-8 encoded forms.
        # AWS: If the Detail parameter is specified, it is measured as the number of bytes for its UTF-8 encoded form.
        for x in ('Source', 'DetailType', 'Detail'):
            try:
                size += _byte_count(entry[x])
            except KeyError:
                pass

        try:
            if entry['Resources'] is not None:
                # AWS: If the Resources parameter is specified, each entry is measured as the number of bytes for their UTF-8 encoded forms.
                for resource in entry['Resources']:
                    size += _byte_count(resource)
        except KeyError:
            pass

        return size

    def emit(self):
        # No need to audit OPTIONS and HEAD requests
        if self.request.method in ('OPTIONS', 'HEAD'):
            return
        logger.info('Sending an event to EventBridge')
        logger.debug(self)

        entry = self.marshal()
        entry_size = self._calc_entry_size(entry)
        if entry_size > 256 * 1000:  # 256KB limit / entry
            # TODO: trying to fix the issue quicly, needs better solution later
            logger.info('The event was too large even by itself, so redacting response data.')
            detail = json.loads(entry['Detail'])
            detail['payload']['response']['data'] = 'Redacted due to size'
            entry['Detail'] = sign(_serialise_dict(detail['payload']))
        send_to_eventbridge([entry])

    def marshal(self):
        """Forms data to be sent to CloudWatch"""
        User = gwio.user.User
        # The subject can be None only in case the requester is not authenticated
        # TODO: Fix to work outside request context (without g)
        assert ((self.subject_id is None and g.user is None)
                or self.subject_type == User.__name__  # User object  # noqa: W504,W503
                or isinstance(self.subject_id, uuid.UUID)  # User/webshop guid  # noqa: W504,W503
                or re.match(r'\w{2}-\w{4,9}-[0-9]:[A-Fa-f0-9\-]+', str(self.subject_id))  # noqa: W504,W503
                # Identity id of non logged in user supporting moto  # noqa: W504,W503
                ), 'Log event subject missing'

        # Audit logs might lack object type if request is one that doesn't match an endpoint and therefore there is no known object for the request
        assert (self.audit and self.response.status_code in (404, 405)) or self.object_type, 'Log event must have object type'

        # TODO: Remove after marshmallow has the fix to microseconds breaking deserialation of DateTime applied to a released version
        #       https://github.com/marshmallow-code/marshmallow/issues/627#issuecomment-508372565
        self.timestamp = self.timestamp.replace(microsecond=0)

        if self.audit:
            schema = self.audit_schema
        else:
            schema = self.schema

        # The commented out lines can not be used until AWS Lambda python environment updates to use up-to-date boto.
        # Currently events will go to the default EventBridge event bus, which has the rule to reroute these to the correct lambda function
        return dict(
            Time=self.timestamp,
            # Source=f'API {self.request["url_root"]} {self.request["url_path"]}',
            Source=g.aws_event_source,
            Resources=[],
            DetailType=_EventBridgeDetailType.AUDIT_LOG.value if self.audit else _EventBridgeDetailType.LOG.value,
            Detail=sign(schema.dumps(self)),
            # EventBusName=AuditRequestsUtil.event_bus,
        )


# TODO: Refactor this AuditRequestsUtil away. https://trello.com/c/ceVxbJPm
class AuditRequestsUtil:
    endpoints_without_audit = []

    default_audit_event_type_from_crud = dict(
        POST=LogEventType.CREATE,
        GET=LogEventType.READ,
        PUT=LogEventType.UPDATE,
        DELETE=LogEventType.DELETE,
    )

    def __init__(self):
        self.client = boto3.client('events')

        self._audit_disabled = False

        self.audit_event = Event(audit=True)

    @property
    def audit_disabled(self):
        return self._audit_disabled

    @property
    def audit_object(self):
        return self.audit_event.get('object', None)

    @audit_object.setter
    def audit_object(self, value):
        # Just to make sure we don't accidentally write code where we're initialising the request audit event twice
        assert not self.audit_event.object
        self.audit_event.object = value

    def save(self):
        if not self.audit_disabled:
            self.audit_event.emit()
