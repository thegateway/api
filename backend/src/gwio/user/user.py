import logging
from collections import defaultdict
from dataclasses import dataclass
from datetime import date
from typing import (
    Iterable,
    Union,
)
from warnings import warn

from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.core import rbac
from gwio.ext.dynamorm import FsmModel
from gwio.ext.marshmallow import GwioSchema
from gwio.models import user
from gwio.utils import uuid
from gwio.utils.notify import (
    Notification,
    NotificationFailException,
    UserNotificationType,
)

logger = logging.getLogger(__name__)


class NonLoggedInUser:
    class Schemas:
        class BaseSchema(GwioSchema):
            email = fields.String()
            name = fields.String()
            phone = fields.String()
            identity_id = fields.String()

    Schema = Schemas.BaseSchema

    def __init__(self, identity_id, email: str = None):
        self.identity_id = identity_id
        self.email = email

    def notify(self, notification: Notification):
        try:
            subscription = dict(type='email',
                                data=dict(email_address=self.email))
            notification.send([subscription])
        except NotificationFailException as e:
            logger.warning(e)


class User:
    # pylint: disable=R0903

    guid = None
    organization_guid = None
    groups = None
    email = None
    name = None
    phone = None
    pin = None
    _data = None

    class Schemas:
        class BaseSchema(GwioSchema):
            guid = fields.UUID()
            organization_guid = fields.UUID()
            email = fields.String()
            groups = fields.String()
            name = fields.String()
            phone = fields.String()
            pin = fields.String()

    # This is needed by the endpoint decorator in some cases
    Schema = Schemas.BaseSchema

    class API:
        @classmethod
        def notify(cls, *, model: 'User', notification: UserNotificationType, **_):
            """
            Send notifications to relevant parties ABOUT the user
            """
            FsmModel.API._notify(
                notification=notification,
                context=dict(
                    user=model,
                ),
            )

    # pylint: disable=too-many-arguments
    def __init__(self, guid, organization_guid=None, groups=None, email=None, name=None, phone=None, pin=None, data: user.UserData = None):
        self.guid = guid
        self.organization_guid = organization_guid
        self.groups = frozenset(groups) if groups is not None else frozenset()
        self.email = email
        self.name = name
        self.phone = phone
        self.pin = pin
        assert isinstance(data, (user.UserData, type(None)))
        self._data = data

    @classmethod
    def by_identity_id(cls, identity_id: str, id_if_user_missing: bool = False) -> Union['User', NonLoggedInUser]:
        """
        Get user object by identity id
        :param identity_id: Identity ID of the wanted user
        :param id_if_user_missing: If the user is not found return the ID in `AttrDict(identity_id=identity_id)`
                                   instead of raising error. Useful for non logged in users
        :return: `User` object or `AttrDict(identity_id=identity_id)`
        :raises: `werkzeug.exceptions.NotFound`
        """
        try:
            userdata = user.UserData.get_by_identity_id(identity_id)
            user_ = cls(userdata.guid, data=userdata)
            return user_
        except NotFound:
            if id_if_user_missing:
                return NonLoggedInUser(identity_id=identity_id)
            raise

    @property
    def data(self):
        if self._data is None:
            try:
                self._data = user.UserData.get(guid=self.guid)
            except user.UserData.DoesNotExist:
                self._data = user.UserData(guid=self.guid)
        return self._data

    @property
    def is_admin(self):
        warn('is_admin is deprecated, should use rbac.admin instead')
        return 'admin' in self.groups

    @property
    def is_shopkeeper(self):
        warn('is_shopkeeper is deprecated, should use rbac.shopkeeper instead')
        return 'shopkeeper' in self.groups

    @property
    def is_product_owner(self):
        warn('is_product_owner is deprecated, should use rbac.product_owner instead')
        return 'product_owner' in self.groups

    @property
    def is_customer(self):
        warn('is_customer is deprecated, should use rbac.customer instead')
        return not any((self.is_shopkeeper, self.is_admin, self.is_product_owner))

    @property
    def shop_uuid(self):
        """Shop_uuid is defined for shopkeepers and regular customers e.g. admins and owners are excluded"""
        if rbac.customer() or rbac.shopkeeper():
            return self.organization_guid
        return None

    @property
    def shop_id(self):
        warn('shop_id is deprecated, should use organization_guid instead')
        return self.organization_guid

    def add_notification_topic(self, topics: Iterable[dict]):
        notifications = defaultdict(list, self.notifications)
        existing_topics = notifications['topics']
        for topic in topics:
            if topic not in existing_topics:
                existing_topics.append(topic)
                self.notifications = dict(notifications)
            else:
                logger.info('User %s already subscribed to topic "%s"', self.guid, topic)

    def add_notification_types(self, types: Iterable[dict]):
        notifications = defaultdict(list, self.notifications)
        existing_types = notifications['types']
        for notification_type in types:
            if notification_type not in existing_types:
                if Notification.Type(notification_type['type']) == Notification.Type.EMAIL:
                    if not self.email:
                        raise Conflict('No email address to subscribe with')
                    notification_type['data'] = dict(email_address=self.email)
                existing_types.append(notification_type)
                self.notifications = dict(notifications)
            else:
                logger.info('User %s already subscribed using "%s: %s"', self.guid, notification_type['type'],
                            notification_type['data'])

    @property
    def notifications(self):
        return self.data.notifications

    @notifications.setter
    def notifications(self, data):
        self.data.notifications = data
        self.data.save()

    @property
    def business_id(self):
        return self.data.business_id

    @business_id.setter
    def business_id(self, business_id: dict):
        self.data.business_id = business_id
        self.data.save()

    @property
    def coupons(self):
        return self.data.coupons

    def get_coupon(self, coupon_guid: uuid.UUID):
        for coupon in self.coupons:
            if coupon.guid == coupon_guid:
                return coupon
        raise ValueError('No coupon exists with given guid')

    @property
    def stripe_id(self):
        return self.data.stripe_id

    @stripe_id.setter
    def stripe_id(self, data):
        self.data.stripe_id = data
        self.data.save()

    @property
    def birthday(self):
        return self.data.birthday

    @birthday.setter
    def birthday(self, birthday: date):
        self.data.birthday = birthday
        self.data.save()

    @property
    def language(self):
        return self.data.language

    @language.setter
    def language(self, language: data):
        self.data.language = language
        self.data.save()

    @property
    def tax_number(self):
        return self.data.tax_number

    @tax_number.setter
    def tax_number(self, tax_number: str):
        self.data.tax_number = tax_number
        self.data.save()

    @property
    def identity_id(self):
        return self.data.identity_id

    @property
    def identity_status(self):
        return self.data.identity_status

    def set_identity_id(self, client, iss, id_token: str):
        self.data.set_identity_id(client, iss, id_token)
        self.data.save()

    # noinspection PyAttributeOutsideInit
    def notify(self, notification: Notification) -> None:
        """
        Delivers a notification to user using all the means the user wishes to receive notifications in.

        This function is both context and delivery method agnostic on purpose.
        """
        working_notifications = list()
        for subscription in self.notifications.get('types', []):
            try:
                # pylint: disable=E1102
                notification.send([subscription], lang=self.language)
                working_notifications.append(subscription)
            # pylint: disable=broad-except
            except NotificationFailException as e:
                logger.warning(e)
        # Only save the notification subscription information for the working notification types
        notifications = self.notifications
        notifications['types'] = working_notifications
        self.notifications = notifications


@dataclass
class IntegrationUser:
    guid: uuid.UUID
    organization_guid: uuid.UUID
    groups: list
    data = None
