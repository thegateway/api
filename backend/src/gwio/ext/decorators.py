# -*- coding: utf-8 -*-


def static_vars(**kwargs):
    """
    Decorator for adding static variable to a function

    :param kwargs: variables and their initializers
    :return: decorated function
    """

    def _decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return _decorate


# pylint: disable=R0903
class _SingletonWrapper:
    def __init__(self, cls):
        self.__wrapped__ = cls
        self._instance = None

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = self.__wrapped__(*args, **kwargs)
        return self._instance


def singleton(cls):
    return _SingletonWrapper(cls)
