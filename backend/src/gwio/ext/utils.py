# -*- coding: utf-8 -*-
"""
Magic helpers
"""


def all_subclasses_of(cls):
    """Utility function to recursively find all subclasses for a class"""
    return set(cls.__subclasses__()).union([s for c in cls.__subclasses__() for s in all_subclasses_of(c)])
