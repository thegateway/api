# -*- coding: utf-8 -*-
"""
Helper functions for the elasticsearch query
"""
import logging
from typing import (
    Generator,
)

import elasticsearch_dsl
from elasticsearch import (
    ConnectionError,
    Elasticsearch,
    NotFoundError,
    RequestError,
)
from elasticsearch_dsl import (
    Search,
    connections,
)
from flask import request
from gwio.environment import Env
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    InternalServerError,
    ServiceUnavailable,
)

from gwio.utils.table_names import make_table_name
from gwio.ext.decorators import static_vars
from gwio.ext.elasticsearch import es_hosts
from gwio.ext.marshmallow import GwioSchema

log = logging.getLogger(__name__)

MAX_ES_SEARCH_SIZE = 1000


@static_vars(es=None)
def es():
    """Delay connection to elastic search until needed

    This is basically here to make testing and mocking easier as well as not trying to establish connection
    to Elasticsearch until actually needed
    """
    if es.es is None:
        es.es = Elasticsearch(hosts=es_hosts())
    return es.es


def next_url(base, token):
    return '{origin}{base}/{token}'.format(origin=request.host_url.rstrip('/'), base=base, token=token)


def do_es_search(es_fn: callable) -> dict:
    # pylint: disable=broad-except
    try:
        result = es_fn()
    except ConnectionError:
        raise ServiceUnavailable
    except RequestError:
        raise
    except Exception as e:
        log.exception(e)
        return dict(total=0, products=[])

    return result


# Makes mocking simpler
def _execute_search(dsl: elasticsearch_dsl.Search) -> dict:
    return dsl.execute()


def _add_sort_keys(hits: dict) -> Generator[dict, None, None]:
    for hit in hits['hits']:
        try:
            doc = hit['_source'].to_dict()
        except AttributeError:
            doc = hit['_source']
        try:
            # Add the sort key of the document to the response if one exists
            doc['sort'] = list(hit['sort'])
        except KeyError:
            pass
        yield doc


def es_search(*, index: str, dsl: Search = None) -> dict:
    """
    Do an Elasticsearch query with the given dsl
    :param index: The name of the index to query
    :param dsl: The query body
    :return: Tuple with total hits and a list of the found documents
    """
    if dsl is None:
        raise Conflict('dsl is required')
    # pylint: disable=broad-except

    # Give the Elasticsearch DSL an existing connection to use
    try:
        connections.get_connection()
    except KeyError:
        connections.add_connection('default', es())

    try:
        size = dsl.to_dict().get('size')
        if size is not None and size > MAX_ES_SEARCH_SIZE:
            dsl.update_from_dict(dict(size=MAX_ES_SEARCH_SIZE))
        dsl = dsl.index(index)
        result = _execute_search(dsl)
        hits = result['hits']
        # Add this "track_total_hits": true to get accurate count.
        # Check https://www.elastic.co/guide/en/elasticsearch/reference/master/search-request-body.html#request-body-search-track-total-hits
        response = dict(total=hits['total']['value'], products=list(_add_sort_keys(hits)))
        try:
            response['aggregations'] = result['aggregations'].to_dict()
        except KeyError:
            pass
        return response
    except KeyError:  # database is empty
        pass
    except ConnectionError as e:
        log.exception('Failed to execute search due to timeout: %s', e)
        raise
    except NotFoundError as e:
        log.exception(e)
        raise InternalServerError('ES index missing') from e
    except RequestError as e:
        log.exception(e)
        reason = 'es error'
        if e.status_code == 400:
            reason = str(e)
        raise BadRequest(reason) from e
    except Exception as e:
        log.exception('Elasticsearch search failed: %s', str(e))
        raise InternalServerError from e
    return dict(total=0, products=list(), aggregations=dict())


def es_index(*, index, **kwargs):
    ":raises elasticsearch.exceptions.TransportError 429 `es_rejected_execution_exception`"
    return es().index(index=index, **kwargs)


def es_delete(*, index, **kwargs):
    return es().delete(index=index, **kwargs)


BARE_PRODUCT_ES_INDEX = make_table_name(Env.VERTICAL, Env.APPLICATION_ENVIRONMENT, 'bare_product')
WEBSHOP_PRODUCT_ES_INDEX = make_table_name(Env.VERTICAL, Env.APPLICATION_ENVIRONMENT, 'webshop_product')


class QueryDDLSchema(GwioSchema):
    """
    name: DSL query
    description: an ElasticSearch DSL query
    """
    dsl = fields.Dict(required=True)
