# -*- coding: utf-8 -*-
"""
Utilities related to elastic search
"""
from gwio.environment import Env
from urllib3.util import parse_url


def es_hosts():
    es_host = Env.ELASTICSEARCH_HOST
    if es_host is None:
        return None

    parsed = parse_url(es_host)

    if parsed.scheme == 'https':
        return [dict(host=parsed.host, port=parsed.port if parsed.port else 443, use_ssl=True)]
    return [dict(host=parsed.host, port=parsed.port if parsed.port else 80)]
