import enum
import logging
from datetime import (
    datetime,
    timezone,
)
from typing import Union

import boto3
import simplejson as json

from gwio.environment import Env
from gwio.ext.eshelpers import (
    es_delete,
    es_index,
)

log = logging.getLogger(__name__)


class EsEvent(enum.Enum):
    PRODUCT_SAVE_EVENT = 'product_save_event'
    PRODUCT_DELETE_EVENT = 'product_delete_event'


def _es_product(entry):
    details = json.loads(entry['Detail'])
    params = dict(index=details['index'],
                  id=details['id'])
    if EsEvent(entry['DetailType']) is EsEvent.PRODUCT_SAVE_EVENT:
        params['body'] = details['product']
        es_index(**params)
    elif EsEvent(entry['DetailType']) is EsEvent.PRODUCT_DELETE_EVENT:
        es_delete(**params)


def _es_log_event(entry):
    details = json.loads(entry['Detail'])
    params = dict(index=f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs",
                  id=details['payload']["request_guid"])
    params['body'] = details['payload']
    es_index(**params)


def update_es_synchronously(entries):
    log.debug('Parsing entry of type %s: %s', type(entries), entries)
    fns = {
        "product_save_event": _es_product,
        "product_delete_event": _es_product,
        "audit_log_event": _es_log_event,
        "log_event": _es_log_event
    }

    for entry in entries:
        fns[entry["DetailType"]](entry)


def send_to_eventbridge(entries: Union[dict, list], elasticsearch=True):
    if not isinstance(entries, list):
        entries = [entries]
    if elasticsearch and Env.SYNCHRONOUS_ES_INDEXING:
        update_es_synchronously(entries)
    else:
        boto3.client('events').put_events(Entries=entries)


def update_es_document(index: str, event_type: EsEvent, document_id: str, body: dict = None):
    """Create an event for EventBridge to deliver to the service updating es"""
    event_bus_name = f'{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}'
    if body is None:
        body = dict()
    body['updated'] = datetime.now(timezone.utc).timestamp()
    entry = dict(
        Detail=json.dumps(dict(
            environment=Env.APPLICATION_ENVIRONMENT,  # Environment is used for routing
            vertical=Env.VERTICAL,  # Vertical the receiving lambda will use
            index=index,
            id=document_id,
            product=json.dumps(body),
        )),
        DetailType=event_type.value,
        # EventBusName=event_bus_name,
        Resources=[],
        Source=event_bus_name,
        Time=datetime.now(timezone.utc)
    )
    send_to_eventbridge(entry)
