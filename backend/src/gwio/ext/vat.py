# -*- coding: utf-8 -*-
"""
Support classes for handling prices with currency and vat

"""
import logging
import re
from decimal import (
    Decimal,
    InvalidOperation,
)
from enum import Enum
from typing import (
    Optional,
    Tuple,
    TypeVar,
    Union,
)

from gwio.environment import Env
from marshmallow import (
    fields,
)

from gwio.ext import (
    dynamorm as dynamorm_ext,
    marshmallow as marshmallow_ext,
)
from gwio.ext.decorators import static_vars

log = logging.getLogger(__name__)

Number = TypeVar('Number', int, float, Decimal)


class Currency(Enum):
    """Supported currencies"""
    EUR = '€'
    USD = '$'
    GBP = '£'
    PLN = 'zł'

    def __str__(self):
        return str(self.value)

    @property
    def abbr(self):
        """Currency abbreviation"""
        return self.name


# Pattern for getting currency and amount from string
_CURRENCIES = f"({'|'.join(c.name for c in Currency)})"
_PRE_VALUE_POST = r'\s*(?P<value>-?\d+[.]\d+|\d+)\s*(?P<currency>' + _CURRENCIES + r')\s*(?P<vat>\S*)'
_PRICE_PATTERN = re.compile(_PRE_VALUE_POST, re.UNICODE)


def _map_currency(currency: Optional[Union[Currency, str]]) -> Optional[Currency]:
    """Helper function to map string representation (symbol or abbreviation) of a currency to the Enum type"""
    if currency is None:
        return None

    if isinstance(currency, Currency):
        return currency

    try:
        return Currency[currency.lower()]
    except KeyError:
        pass

    try:
        return Currency[currency]
    except KeyError:
        try:
            return Currency(currency)
        except ValueError:
            raise ValueError(f'Unknown currency "{currency}"')


@static_vars(pattern=None)
def _parse_price_string(s: str) -> Tuple[Decimal, Currency]:
    try:
        parsed = _PRICE_PATTERN.match(s)
        try:
            currency = parsed['currency'] if parsed['currency'] else None
        except TypeError:
            currency = None
        try:
            parsed_price = Decimal(parsed['value'])
        except TypeError:
            parsed_price = Decimal(s.strip())
        parsed_currency = _map_currency(currency)
        return parsed_price, parsed_currency
    except Exception:
        raise ValueError(f'"{s}" is not a recognizable price')


class Price:
    """
    Represents a price currency but without VAT

    NOTE! currently no currency conversions are supported
    ALSO NOTE! Price(10.0) is assumed to be equal to VatPrice(10.0, vat=0)
    """
    currency = None
    value = None

    def __init__(self, value: Union[Number, 'Price', 'VatPrice', str],
                 currency: Optional[Union[str, Currency]] = None):
        if isinstance(value, VatPrice):
            value = value.without_vat

        if isinstance(value, self.__class__):
            self.value, self.currency = value.value, value.currency
        elif isinstance(value, str):
            self.value, self.currency = _parse_price_string(value)
        else:
            self.value = Decimal(value)

        currency = _map_currency(currency)

        if self.currency is None:
            self.currency = currency if currency is not None else _map_currency(Env.DEFAULT_CURRENCY)
            return

        if currency is not None and self.currency != currency:
            raise RuntimeError(
                f'Currency conversion {self.currency.abbr}/{currency.abbr} not supported')

    @property
    def amount(self) -> Decimal:
        return round(self.value, 2)

    def __str__(self):
        return f"{round(self.value, 2)} {str(self.currency.name)}"

    def __repr__(self):
        return f"<Price({self.value} {self.currency.abbr})>"

    def __eq__(self, other: Union['VatPrice', 'Price', Number]) -> bool:
        return round(self.value, 2) == round(Price(other, currency=self.currency).value, 2)

    def __ne__(self, other: Union['VatPrice', 'Price', Number]) -> bool:
        return round(self.value, 2) != round(Price(other, currency=self.currency).value, 2)

    def __lt__(self, other: Union['VatPrice', 'Price', Number]) -> bool:
        return round(self.value, 2) < round(Price(other, currency=self.currency).value, 2)

    def __gt__(self, other: Union['VatPrice', 'Price', Number]) -> bool:
        return round(self.value, 2) > round(Price(other, currency=self.currency).value, 2)

    def __le__(self, other: Union['VatPrice', 'Price', Number]) -> bool:
        return round(self.value, 2) <= round(Price(other, currency=self.currency).value, 2)

    def __ge__(self, other: Union['VatPrice', 'Price', Number]) -> bool:
        return round(self.value, 2) >= round(Price(other, currency=self.currency).value, 2)

    def __add__(self, other: Union['VatPrice', 'Price', Number]) -> 'Price':
        return Price(self.value + Price(other, currency=self.currency).value, currency=self.currency)

    def __sub__(self, other: Union['VatPrice', 'Price', Number]) -> 'Price':
        return Price(self.value - Price(other, currency=self.currency).value, currency=self.currency)

    def __mul__(self, other: Number) -> 'Price':
        return Price(self.value * Decimal(other), currency=self.currency)

    def __truediv__(self, other: Number) -> 'Price':
        return Price(self.value / Decimal(other), currency=self.currency)

    def __matmul__(self, other: 'Vat') -> 'VatPrice':
        return VatPrice.from_price_and_vat(self, vat=0) @ other


class Vat:
    """Represents VAT category"""

    category = None
    scaler = None

    def __init__(self, category: Optional[Union['Vat', Number, str]] = None):
        if category is None:
            self.category = Decimal(0)
        elif isinstance(category, self.__class__):
            self.category = category.category
        elif isinstance(category, str) and category.lower().startswith('vat'):
            self.category = Decimal(category[3:])
        else:
            try:
                self.category = Decimal(category)
            except InvalidOperation:
                self.scaler = Decimal(1.0)

        if self.category is not None:
            self.scaler = (Decimal(100) + self.category) / Decimal(100)

    def vat(self, price: Price) -> Price:
        if self.scaler is None:
            raise ValueError('Unknown VAT')
        return price * self.scaler

    def vat0(self, price: Price) -> Price:
        if self.scaler is None:
            raise ValueError('Unknown VAT')
        return price / self.scaler

    @property
    def percentage(self) -> int:
        try:
            return int(round(self.category, 0))
        except TypeError:
            raise ValueError('Unknown VAT')

    def __str__(self):
        try:
            return f"VAT{self.percentage}"
        except ValueError:
            return "ERROR"

    def __repr__(self):
        try:
            return f"<Vat({self.percentage})>"
        except ValueError:
            return "<Vat(ERR)>"

    def __eq__(self, other: 'Vat') -> bool:
        return self.percentage == other.percentage

    def __ne__(self, other: 'Vat') -> bool:
        return self.percentage != other.percentage

    def __hash__(self):
        return hash(self.category)


class VatPriceSchema(marshmallow_ext.GwioSchema):
    amount = fields.Decimal(required=True, allow_none=False)
    currency = marshmallow_ext.GwioEnumField(enum=Currency, by_value=True, required=True, allow_none=False)
    vat_percent = fields.Integer(allow_none=False, missing=0)


class BaseVatPriceSchema(VatPriceSchema):
    def _serialize(self, data, **kwargs):
        return str(data)

    def _deserialize(self, data, **kwargs):
        # Deserializing from the serialized value...
        if isinstance(data, str):
            return VatPrice.from_price_and_vat(data)
        # or from a dictionary matching to the schema
        return super()._deserialize(data, **kwargs)


class VatPrice(dynamorm_ext.NestedSchemaModel):
    """Represents a price with a vat classification

    NOTE! currently no currency conversion are supported,
    this can be added later if needed for now trying to
    do operations (add, sub) with different currencies
    will raise an exception
    """
    Schema = BaseVatPriceSchema

    class Schemas:
        class Get(VatPriceSchema):
            vat0 = fields.Decimal(required=True, allow_none=False, dump_only=True)
            vat = fields.Decimal(required=True, allow_none=False, dump_only=True)

        class Post(VatPriceSchema):
            vat_percent = fields.Integer(required=True, allow_none=False)

    @classmethod
    def from_price_and_vat(cls, value: Union['VatPrice', Number, Price, str] = None, *,
                           vat: Optional[Union[Vat, Number, str]] = None):
        """
        The old init
        Left around to make refactoring easier
        """
        currency = None
        if isinstance(value, VatPrice):
            if vat is not None:
                value = value @ Vat(vat)
            value, vat = value.value, value.vat_model
            return cls(amount=value.amount, currency=value.currency, vat_percent=vat.percentage)

        if isinstance(value, Price):
            value, vat = value, Vat(0) if vat is None else Vat(vat)
            return cls(amount=value.amount, currency=value.currency, vat_percent=vat.percentage)

        if isinstance(value, str):
            parsed = _PRICE_PATTERN.match(value)
            (value, parsed_vat, currency) = [parsed[k] if parsed[k] else None for k in ['value', 'vat', 'currency']]
            if vat is not None and parsed_vat is not None:
                raise ValueError(f'Both vat and value including vat cannot be passed: {parsed_vat} {vat}')
            vat = parsed_vat if parsed_vat else vat

        value, vat = Price(value, currency), Vat(vat)
        return cls(amount=value.amount, currency=value.currency, vat_percent=vat.percentage)

    @property
    def without_vat(self) -> Price:
        """Price without vat"""
        return self.vat_model.vat0(self.value)

    @property
    def included_vat(self) -> Price:
        """Vat included in the VatPrice"""
        return self.value - self.vat_model.vat0(self.value)

    @property
    def price(self) -> Price:
        return Price(self.amount, self.currency)

    @property
    def vat_model(self) -> Vat:
        return Vat(self.vat_percent)

    @property
    def vat0(self) -> Decimal:
        return self.without_vat.amount

    @property
    def vat(self) -> Decimal:
        return self.included_vat.amount

    @property
    def value(self) -> Price:
        # TODO: REMOVE
        return self.price

    def __str__(self) -> str:
        return f'{str(self.value)} {self.vat_model}'

    def __repr__(self) -> str:
        return f'<VatPrice({repr(self.value)},{repr(self.vat_model)})>'

    def __eq__(self, other: 'VatPrice') -> bool:
        return self.without_vat == VatPrice.from_price_and_vat(other, vat=self.vat_percent).without_vat

    def __ne__(self, other: 'VatPrice') -> bool:
        return self.without_vat != VatPrice.from_price_and_vat(other, vat=self.vat_percent).without_vat

    def __lt__(self, other: 'VatPrice') -> bool:
        return self.without_vat < VatPrice.from_price_and_vat(other, vat=self.vat_percent).without_vat

    def __gt__(self, other: 'VatPrice') -> bool:
        return self.without_vat > VatPrice.from_price_and_vat(other, vat=self.vat_percent).without_vat

    def __le__(self, other: 'VatPrice') -> bool:
        return self.without_vat <= VatPrice.from_price_and_vat(other, vat=self.vat_percent).without_vat

    def __ge__(self, other: 'VatPrice') -> bool:
        return self.without_vat >= VatPrice.from_price_and_vat(other, vat=self.vat_percent).without_vat

    def __add__(self, other: 'VatPrice') -> 'VatPrice':
        if not isinstance(other, VatPrice) or self.vat_model != other.vat_model:
            raise TypeError(f"VAT categories don't match ({str(self.vat_model)}/{str(other.vat_model)})")
        return VatPrice.from_price_and_vat(self.price + other.price, vat=self.vat_model)

    def __sub__(self, other: 'VatPrice') -> 'VatPrice':
        if not isinstance(other, VatPrice) or self.vat_model != other.vat_model:
            raise TypeError(f"VAT categories don't match ({str(self.vat_model)}/{str(other.vat_model)})")
        return VatPrice.from_price_and_vat(self.price - other.price, vat=self.vat_model)

    def __mul__(self, other: Number) -> 'VatPrice':
        return VatPrice.from_price_and_vat(self.value * Decimal(other), vat=self.vat_model)

    def __truediv__(self, other: Number) -> 'VatPrice':
        return VatPrice.from_price_and_vat(self.value / Decimal(other), vat=self.vat_model)

    def __matmul__(self, other: Vat) -> 'VatPrice':
        return VatPrice.from_price_and_vat(other.vat(self.without_vat), vat=other)


class PriceField(fields.Field):
    def _validate(self, value):
        if value is None:
            return None
        if isinstance(value, Price):
            return value
        self.fail('validator_failed')

    def _serialize(self, value, attr, obj, **kwargs):
        try:
            return str(value)
        except AttributeError:
            return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return Price(value)
        except (ValueError, TypeError):
            super()._deserialize(value, attr, data, **kwargs)


class VatField(fields.Field):
    def _validate(self, value):
        if value is None:
            return None
        if isinstance(value, Vat):
            return value
        self.fail('validator_failed')

    def _serialize(self, value, attr, obj, **kwargs):
        try:
            return str(value)
        except AttributeError:
            return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return Vat(value)
        except (ValueError, TypeError):
            super()._deserialize(value, attr, data, **kwargs)
