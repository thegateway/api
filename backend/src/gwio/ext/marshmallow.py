import datetime
import re
import simplejson
from enum import Enum

import pycountry
import pycountry.db

from marshmallow import (
    Schema,
    fields,
)
from marshmallow_enum import EnumField


# pylint: disable=R1710

class ValidationErrorMessage(Enum):
    DOES_NOT_EXIST = 'Does not exist.'


class GwioSchema(Schema):
    class Meta:
        render_module = simplejson

    _default_error_messages = {
        **Schema._default_error_messages,
        **{'does_not_exist': ValidationErrorMessage.DOES_NOT_EXIST.value},
    }


class HexColor(fields.String):
    m = re.compile(r'^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$')

    # pylint: disable=inconsistent-return-statements
    def _validate(self, value):
        if value is None:
            return None
        if isinstance(value, str) and self.m.match(value):
            return value
        self.fail('validator_failed')


class HourMinuteField(fields.Field):
    def _serialize(self, value, attr, obj, **kwargs):
        try:
            return value.strftime('%H:%M')
        except AttributeError:
            return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, datetime.time):
            return value
        try:
            return datetime.datetime.strptime(value, '%H:%M').time()
        except (ValueError, TypeError):
            super()._deserialize(value, attr, data, **kwargs)


class ISODateField(fields.Field):
    # pylint: disable=inconsistent-return-statements
    def _validate(self, value):
        if value is None:
            return None
        if isinstance(value, datetime.date):
            return value
        self.fail('validator_failed')

    def _serialize(self, value, attr, obj, **kwargs):
        try:
            return value.strftime('%Y-%m-%d')
        except AttributeError:
            return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, datetime.date):
            return value
        try:
            parts = value.split('-')
            # *sigh* no fromisoformat in pre 3.7 python
            return datetime.date(int(parts[0]), int(parts[1]), int(parts[2]))
        except (IndexError, ValueError, TypeError):
            super()._deserialize(value, attr, data, **kwargs)


class GWIODateTime(fields.DateTime):
    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, datetime.datetime):
            return value
        return super()._deserialize(value, attr, data, **kwargs)

    def _serialize(self, value, attr, obj, **kwargs):
        if isinstance(value, str):
            return value
        return super()._serialize(value, attr, obj, **kwargs)


class GwioEnumField(EnumField):
    def __init__(self, *args, by_value=True, **kwargs):
        super().__init__(*args, by_value=by_value, **kwargs)

    def _serialize(self, value, attr, obj):
        if isinstance(value, str):
            return value
        return super()._serialize(value, attr, obj)

    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, self.enum):
            return value
        return super()._deserialize(value, attr, data, **kwargs)


class GwioCurrencyField(fields.Field):
    def _deserialize(self, value, attr, data, **kwargs):
        currency = pycountry.currencies.get(alpha_3=value)
        if currency is None:
            self.fail(f'{value} is not valid ISO 4217 currency')
        return currency

    def _serialize(self, value, attr, obj, **kwargs):
        if isinstance(value, str):
            currency = pycountry.currencies.get(alpha_3=value)
            if currency is None:
                self.fail(f'{value} is not valid ISO 4217 currency')
            return value
        # `Currency` is dynamically created...
        elif isinstance(value, pycountry.db.Currency):  # pylint: disable=no-member
            return value.alpha_3
        self.fail(f'unhandled type {type(value)}: {value} for currency')
