import copy
import json
import random
import time
import traceback
import uuid
import warnings
from enum import Enum
from functools import partial
from logging import getLogger
from typing import (
    Any,
    Callable,
    Dict,
    Generator,
    Optional,
    Type,
    Union,
)

import stringcase
from dynamorm import (
    DynaModel,
    GlobalIndex,
)
from dynamorm.signals import (
    post_init,
    pre_init,
)
from marshmallow import (
    Schema,
    ValidationError,
    fields,
)
from marshmallow_enum import EnumField

import gwio
from gwio.environment import Env as env
from gwio.ext import marshmallow as marshmallow_ext
from gwio.utils import (
    fsm,
    notify,
)
from gwio.utils.table_names import make_table_name

all_models = list()
_vertical_models = dict()

_MAX_ATTEMPTS = 25
# These should cover all other exceptions that Amazon states are ok to retry except "UnrecognizedClientException"
_OK_TO_RETRY_MESSAGES = ('LimitExceeded', 'ThroughputExceeded', 'Throttling')
_OK_TO_RETRY_ERRORS = ('InternalServerError', 'ServiceUnavailable')

log = getLogger(__name__)


def _using_jitter_retry(fn, *args, **kwargs):
    """
    call dynamorm function and retry the operation in case we get
    an exception because the actual provisioned throughput limit
    was exceeded (we can get this even in the case where the table
    has been configured to automatically scale up)
    """
    attempt = 1
    while True:
        try:
            return fn(*args, **kwargs)
        # pylint: disable=broad-except
        except Exception as e:
            exception_str = str(type(e))
            # TODO: dynamorm might have, maybe, someone should really look into this
            # pynamodb does not have a separate exception for this so we need to resort to
            # checking the actual message of the exception.
            no_message_retry = all(x not in getattr(e, 'msg', '') for x in _OK_TO_RETRY_MESSAGES)
            no_error_retry = all(x not in exception_str for x in _OK_TO_RETRY_ERRORS)
            if (no_message_retry and no_error_retry) or attempt >= _MAX_ATTEMPTS:
                log.error('Giving up retrying DynamoDB operation after %d attempts', attempt)
                raise
        time.sleep(min(1000, random.randint(1, 25 * attempt ** 2)) / 1000)
        attempt += 1


def hotfixed_index_query(inst, query_kwargs=None, **kwargs):
    """
    Fixes querying with Python data types that dynamorm does not support
    """
    for k, v in kwargs.items():
        if isinstance(v, uuid.UUID):
            kwargs[k] = str(v)
        elif isinstance(v, Enum):
            kwargs[k] = v.value
    return super(inst.__class__, inst).query(query_kwargs, **kwargs)


def adapted_dynamodel(arg: Union[DynaModel, GlobalIndex, str]) -> DynaModel:
    """
    Decorator for setting DynamORM model Meta host, region etc and table_name.
    :param arg:
    :return:
    """

    # pylint: disable=too-few-public-methods
    class Wrapper:
        def __new__(cls, dyna_cls, vertical=None):
            if issubclass(dyna_cls, GlobalIndex):
                base_name = dyna_cls.name
                try:
                    version = dyna_cls.version
                except AttributeError:
                    version = None
            else:
                base_name = dyna_cls.Table.name
                all_models.append(dyna_cls)
                try:
                    version = dyna_cls.Table.version
                except AttributeError:
                    version = None

            appenv = env.APPLICATION_ENVIRONMENT
            vertical = vertical if vertical is not None else env.VERTICAL
            name = make_table_name(vertical, appenv, base_name, version)
            try:
                setattr(dyna_cls.Table, 'name', name)
            except AttributeError:  # index
                setattr(dyna_cls, 'name', name)
                dyna_cls.query = hotfixed_index_query

            return dyna_cls

    # noinspection PyTypeChecker
    def register_adapted_ddb(cls: Union[DynaModel, GlobalIndex]) -> Union[DynaModel, GlobalIndex]:
        name = cls.__name__
        if arg not in _vertical_models:
            _vertical_models[arg] = dict()
        if name in _vertical_models[arg]:
            raise RuntimeError(f'Redefinition of "{name}" for "{arg}"')
        model = Wrapper(cls, vertical=arg)
        _vertical_models[arg][name] = model
        return model

    if callable(arg):
        return Wrapper(arg)

    return register_adapted_ddb


def use_ddb(*, model: str, vertical: str = None) -> DynaModel:
    """Returns the vertical specific model"""
    vertical = vertical if vertical is not None else env.VERTICAL
    try:
        return _vertical_models[vertical][model]
    except KeyError:
        raise ValueError(f'Model "{model}" not available for "{vertical}"')


def extend_schema_with_modified_required(*,
                                         src: Any,
                                         target_schema_name: str,
                                         fields: Dict[str, Optional[bool]]) -> Type[Schema]:
    """
    Modify an object looking like a `Schema` class and make the returned new class extend Schema.
    Does not support changing the `required` property of a nested field.

    :param src: The source class that looks like a Schema class but does not inherit from it
    :param target_schema_name: Target class name
    :param fields: Dict of names of the fields to inherit into the target schema as keys and boolean indicating whether or not the field is required
                   in the inherited schema. None inherits the `required` value from the source schema
    :return:
    """
    fields_in_src = {field_name: getattr(src, field_name) for field_name in dir(src) if not field_name.startswith('__')}

    target_fields = dict()
    for field_name, required in fields.items():
        field = copy.deepcopy(fields_in_src[field_name])
        if required is not None:
            field.required = required
        target_fields[field_name] = field

    return type(target_schema_name, (Schema,), target_fields)


def model_pre_init(*_, instance, **__):
    instance._nested_models = dict()
    # Setting `missing` for fields so that dynamorm doesn't raise marshmallow validation error during deserialization
    for field_name, field in instance.Schema._declared_fields.items():
        if isinstance(field, GwioStructureFieldMixIn):
            field._parent_model = instance
            field.missing = field.new_struct


def model_post_init(*_, instance, **__):
    def _get(self, *, field_name):
        field = self.Schema._declared_fields[field_name]
        try:
            value = self._nested_models[field_name]
        except KeyError:
            value = None
        if isinstance(field, GwioStructureFieldMixIn) and value is None:
            value = field.new_struct()
            setattr(self, field_name, value)
        if value is not None:
            value._parent_model = self
        return value

    def _set(self, value, *, field_name):
        field = self.Schema._declared_fields[field_name]

        if isinstance(field, GwioModelField) and value is not None:
            # TODO: Remove `issubclass` when `VatPrice` / `VatPriceModel` have been refactored to make more sense
            if not (isinstance(value, field._model) or issubclass(field._model, value.__class__)):
                value = field._model(**value)
            value._parent_model = self
        if isinstance(field, GwioStructureFieldMixIn):
            if not isinstance(value, field._structure):
                value = field.new_struct(value)
            value._parent_model = self

        self._nested_models[field_name] = value

    for field_name, field in instance.Schema._declared_fields.items():
        if isinstance(field, GwioNestingFieldMixIn):
            # Setting the current attribute value back through the property so that it gets stored correctly and is not lost
            attr_value = getattr(instance, field_name)
            setattr(instance.__class__, field_name, property(partial(_get, field_name=field_name), partial(_set, field_name=field_name)))
            setattr(instance, field_name, attr_value)


class _ModelMixIn:
    """
    All the models (dynamodb models and their submodels) extend from this

    Adds
      - ability to compare the model to UUID
      ... #TODO: continue this
    """

    def __hash__(self):
        try:
            identifier = getattr(self, self.Table.identifier)
        except AttributeError:
            identifier = self.guid
        return hash(identifier)

    def __eq__(self, other):
        return isinstance(other, (self.__class__, uuid.UUID)) and hash(self) == hash(other)


def _fix_zero_length_strings(data_struct, schema):
    for field_name, field in schema().declared_fields.items():
        value = getattr(data_struct, field_name)
        if value is None:
            continue
        if isinstance(field, fields.String) and not value:
            setattr(data_struct, field_name, None)
        elif isinstance(field, GwioListField):
            for item in value:
                _fix_zero_length_strings(item, field.model_cls.Schema)
        elif isinstance(field, GwioDictField):
            for item in value.values():
                _fix_zero_length_strings(item, field.model_cls.Schema)
        elif isinstance(field, GwioModelField):
            _fix_zero_length_strings(value, field.model_cls.Schema)


class HotfixMixIn(_ModelMixIn):
    """
    All the DynamoDb models should extend from this.

    Adds
      - ability to compare the model to UUID (from _ModelMixIn)
      ... #TODO: continue this
    """

    class DoesNotExist(Exception):
        """Error for when the requested model does not exist"""

    def _run_hooks(self, category, op):
        if getattr(self, '_hooks', None) is None:
            return

        for fn, fail_safe in self._hooks[category][op]:
            try:
                fn(self)
            # pylint: disable=broad-except
            except Exception as e:
                log.exception(e)
                if not fail_safe:
                    raise

    def _process_kwargs(self, kwargs):
        api = getattr(self, 'API', None)
        if api is None:
            return kwargs
        fn = getattr(self.API, 'process_kwargs', None)
        return kwargs if fn is None else fn(kwargs)

    @classmethod
    def get(cls, **kwargs):
        ret = super().get(**kwargs)
        if ret is None:
            raise cls.DoesNotExist(f'{cls.__name__} does not exist')
        return ret

    # pylint: disable=arguments-differ
    def save(self, *args, **kwargs):
        # Setting up missing values in case we have some old logic that didn't handle it already correctly
        self._fix_values()

        self._run_op('save', *args, **kwargs)

    def update(self, *args, **kwargs):
        # Setting up missing values in case we have some old logic that didn't handle it already correctly
        self._fix_values()

        self._run_hooks('pre', 'update')
        _using_jitter_retry(getattr(super(), 'save'), *args, **kwargs)
        self._run_hooks('post', 'update')

    # pylint: disable=arguments-differ
    def delete(self, *args, **kwargs):
        # Workaround for https://github.com/NerdWalletOSS/dynamorm/issues/67
        orig_values = {}
        for key_name in (self.Table.hash_key, self.Table.range_key):
            if key_name is not None:
                key_value = getattr(self, key_name)
                orig_values[key_name] = key_value
                setattr(self, key_name, str(key_value))
        self._run_op('delete', *args, **kwargs)

        for key, value in orig_values.items():
            setattr(self, key, value)

    # Matching pynamodb
    @classmethod
    def exists(cls):
        # pylint: disable=no-member
        return cls.Table.exists

    # Matching pynamodb
    @classmethod
    def create_table(cls, **kwargs):
        # pylint: disable=no-member
        return cls.Table.create(**kwargs)

    # Matching pynamodb
    @classmethod
    def delete_table(cls, **kwargs):
        # pylint: disable=no-member
        return cls.Table.delete(**kwargs)

    @classmethod
    def register_hook(cls, *, hook: str, allow_fail: bool = True) -> Callable:
        def _register(fn: Callable, category: str = None, op: str = None, fail_safe: bool = False) -> Callable:
            if getattr(cls, '_hooks', None) is None:
                cls._hooks = {category: {op: list() for op in ['save', 'delete', 'update']} for category in ['pre', 'post']}
            cls._hooks[category][op].append((fn, fail_safe))
            return fn

        category, op = hook.split('-')
        return partial(_register, category=category, op=op, fail_safe=allow_fail)

    def _run_op(self, op, *args, **kwargs):
        self._run_hooks('pre', op)
        _using_jitter_retry(getattr(super(), op), *args, **kwargs)
        self._run_hooks('post', op)

    @staticmethod
    def _update_object(obj, **kwargs):
        """The default update function in case the model's API does not implement one"""
        for attr in kwargs:
            setattr(obj, attr, kwargs[attr])

    def refresh(self):
        kwargs = {
            self.Table.hash_key: getattr(self, self.Table.hash_key)
        }
        # DynamORM of course sets range key to None if it doesn't exist...
        if self.Table.range_key is not None:
            kwargs[self.Table.range_key] = getattr(self, self.Table.range_key)

        # Copy from DynaModel.get
        kwargs = self._normalize_keys_in_kwargs(kwargs)
        raw = self.Table.get(**kwargs)

        # Yeah, horrible, but simplest way to implement refresh
        self.__init__(**raw)

    def modify(self, **kwargs):
        # Setting up missing values in case we have some old logic that didn't handle it already correctly
        self._fix_values()

        self._run_hooks('pre', 'update')
        # To allow the derived classes to have their own update implemented in class.API
        # pylint: disable=E1101
        fn = getattr(self.API, 'update_object', self._update_object)
        fn(self, **self._process_kwargs(kwargs))
        super().save()
        self._run_hooks('post', 'update')

    def _fix_values_init(self):
        """
        Meant to handle setting up values that have been added to the schema and might be missing from the database.
        The model itself must have a call to this in it's init (as shown below) because we can not have init here,
        since the model itself must extend from DynaModel, due to how dynamorm works.

            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self._fix_values_init()
        """
        # Only running the given function if being called from within __init__ that was called by DynamORM
        stack = traceback.extract_stack()
        # TODO: Refactor how we define our models so we can get rid of this mess (https://trello.com/c/qkt0mdNG)
        if stack[-2].name == '__init__' and [x for x in stack if 'dynamorm/dynamorm/model.py' in x.filename]:
            if self._fix_values():
                self.save()

    def _fix_values(self):
        # Fix zero length strings not being supported
        _fix_zero_length_strings(self, self.Schema)

        any_value_set = False
        try:
            for fn in self._fix_values_funcs:
                if fn(self) is True:
                    any_value_set = True
        except AttributeError:
            pass
        return any_value_set

    @classmethod
    def scan_all(cls) -> Generator[Any, None, None]:
        """
        Yields instances of `cls`
        """
        iterator = cls.scan()
        yield from iterator
        while iterator.last is not None:
            iterator.again()
            yield from iterator

    def as_event_context(self):
        res = dict()
        if self is not isinstance(self, gwio.user.User):
            try:
                schema = self.Schemas.LogEvent()
            except AttributeError:
                schema = self.Schema()
            res['obj'] = schema.dump(self)
        if hasattr(self, 'guid'):
            res['object_id'] = self.guid
        else:
            try:
                res['object_id'] = getattr(self, self.Table.identifier)
            except TypeError:
                res['object_id'] = f'{getattr(self, self.Table.identifier[0])}_{getattr(self, self.Table.identifier[1])}'
        res['object_type'] = type(self).__name__
        return res


def _assess(
        cls, *,
        model: Union['FsmModel'],
        action: Enum,
        assessment: Enum,
        ctx: Any = None
):
    """Directs the call to the correct assessment logic"""
    try:
        function = cls._assessor_functions[assessment]
    except (AttributeError, KeyError):  # No assessors at all or just missing this specific one
        warnings.warn(f'`{assessment.name}` assessment not implemented to `{cls.__name__}.API.assess`.')
        return
    function(model=model, action=action, ctx=ctx)


class FsmModel(HotfixMixIn):
    """Use this for models that supposed to be handled by gwio.logic handlers"""

    class API:
        assess = classmethod(_assess)

        @staticmethod
        def _notify(*, notification, context):
            """Handle notifications triggered by state changes for the class instance"""
            for notification_handler in notify.Notification.from_type(notification):
                notification_handler.emit(context)

    @property
    def current_state(self):
        state = self.state if hasattr(self, 'state') else self._current_state  # pylint: disable=no-member

        enum_cls = self.Schema._declared_fields['_current_state'].enum  # pylint: disable=no-member
        if not isinstance(state, (fsm.FsmState, enum_cls)):
            try:
                state = enum_cls(state)
            except ValueError:
                state = fsm.FsmState(state)

        return state

    def process(self, action: Enum):
        self._fsm_handler.add_model(self)  # pylint: disable=no-member
        try:
            self._fsm_handler._process_action(self, action)  # pylint: disable=no-member
        finally:
            self._fsm_handler.remove_model(self)  # pylint: disable=no-member

    @current_state.setter
    def current_state(self, value):
        # pylint: disable=no-member
        if not isinstance(value, (fsm.FsmState, self.Schema._declared_fields['_current_state'].enum)):
            raise ValueError('State is not a valid enum')
        self._current_state = value


class FsmEnumField(EnumField):
    def __init__(self, *, fsm_enum, **kwargs):
        kwargs['by_value'] = True
        super().__init__(**kwargs)
        self.fsm_enum = fsm_enum

    def _serialize(self, value, *args, **kwargs):
        try:
            return super()._serialize(value, *args, **kwargs)
        except ValueError:
            if not isinstance(value, self.fsm_enum):
                value = self.fsm_enum(value)
            return value.value
        except Exception as e:
            raise e

    def _deserialize(self, value, *args, **kwargs):
        try:
            return super()._deserialize(value, *args, **kwargs)
        except ValidationError as e:
            try:
                if isinstance(value, self.fsm_enum):
                    return value
                return self.fsm_enum(value)
            except ValueError:
                raise e


class ModelNestingMixIn:
    @property
    def model_cls(self):
        if type(self._model).__name__ == 'function':
            # `model` was a callable that is supposed to return the nested model class
            return self._model()
        return self._model

    @model_cls.setter
    def model_cls(self, value):
        assert_check = type(value).__name__ == 'function' or issubclass(value, NestedSchemaModel)
        assert assert_check, f'The `model` must be subclass of {NestedSchemaModel.__name__} or a callable that return one'
        self._model = value


class GwioDict(ModelNestingMixIn, dict):
    def __init__(self, value=None, *, keys, values):
        if value is None:
            value = dict()
        self._keys = keys
        self._model = values
        self._parent_model = None  # This is set when the `GwioDict` is assigned to a class extending from `HotfixMixIn`
        deserialized_values = dict()
        for k, v in value.items():
            if not isinstance(k, keys):
                k = self._init_key(k)
            if not isinstance(v, values):
                v = self._init_value(v, dict_init=True)
            deserialized_values[k] = v
        super().__init__(deserialized_values)

    def _init_key(self, value):
        if not isinstance(value, self._keys):
            try:
                value = str(value)
            except AttributeError:
                pass
            value = self._keys(value)
        return value

    def _init_value(self, value, dict_init=False):
        assert self._parent_model or dict_init  # Not available when initialising the list
        if not isinstance(value, self.model_cls):
            try:
                value = value.Schemas.Nested().dump(value)
            except AttributeError:
                pass
            value = self.model_cls(**value)
        value._parent_model = self._parent_model
        return value

    def __setitem__(self, key, value):
        assert self._parent_model
        k = self._init_key(key)
        item = self._init_value(value)
        super().__setitem__(k, item)

    def __getitem__(self, index):
        assert self._parent_model
        item = super().__getitem__(index)
        try:
            item._parent_model = self._parent_model
        except AttributeError:  # Slicing the list, so the item is a list
            for v in item:
                v._parent_model = self._parent_model
        return item


class GwioList(ModelNestingMixIn, list):
    def __init__(self, value=None, *, model):
        if value is None:
            value = []
        self.model_cls = model
        self._parent_model = None  # This is set when the `GwioList` is assigned to a class extending from `HotfixMixIn`
        deserialized_values = []
        for v in value:
            if not isinstance(v, self.model_cls):
                v = self._init_element(v, list_init=True)
            deserialized_values.append(v)
        super().__init__(deserialized_values)

    def __setitem__(self, index, value):
        assert self._parent_model
        item = self._init_element(value)
        super().__setitem__(index, item)

    def __getitem__(self, index):
        assert self._parent_model
        item = super().__getitem__(index)
        try:
            item._parent_model = self._parent_model
        except AttributeError:  # Slicing the list, so the item is a list
            for v in item:
                v._parent_model = self._parent_model
        return item

    def __iter__(self):
        for v in super().__iter__():
            v._parent_model = self._parent_model
            yield v

    def _init_element(self, value, list_init=False):
        assert self._parent_model or list_init  # Not available when initialising the list
        if not isinstance(value, self.model_cls):
            try:
                value = value.Schemas.Nested().dump(value)
            except AttributeError:
                pass
            value = self.model_cls(**value)
        value._parent_model = self._parent_model
        return value

    def append(self, other):
        super().append(self._init_element(other))


class GwioNestingFieldMixIn(ModelNestingMixIn):
    pass


class GwioStructureFieldMixIn(GwioNestingFieldMixIn):

    def _deserialize(self, value: Union[list, dict, GwioList, GwioDict], *args, **kwargs):
        deserialised_value = self.new_struct(value)
        deserialised_value._parent_model = self._parent_model
        return deserialised_value

    def new_struct(self, value=None):
        if value is not None:
            return self._structure(value, **self._struct_init_values)
        return self._structure(**self._struct_init_values)


class NestedSchemaModel(_ModelMixIn):
    """
    All the submodels nested within the Dynamodb models should extend from this.

    Adds
      - ability to compare the model to UUID (from _ModelMixIn)
      ... #TODO: continue this
    """

    def __init__(self, *, _single_value=None, _parent_model=None, **kwagrs):
        model_pre_init(instance=self)
        schema = self.Schema()
        try:
            del kwagrs['_nested_models']
        except KeyError:
            pass
        # This is to support VatPrice, which is a model that serializes into a string
        if _single_value:
            data = schema.load(_single_value)
        else:
            data = schema.load(kwagrs)
        # TODO: Tidy this up when refactoring `VatPrice` / `VatPriceModel`
        for k in schema._declared_fields.keys():
            try:
                value = data[k]
            except TypeError:
                value = getattr(data, k)
            except KeyError:
                value = None
            try:
                setattr(self, k, value)
            except AttributeError:
                pass
        model_post_init(instance=self)

    def __getattr__(self, item):
        try:
            # Check if `item` matches to the parent models class name so that we can assume parent model was requested
            parent_model = super().__getattribute__('_parent_model')
            if item == stringcase.snakecase(parent_model.__class__.__name__):
                return parent_model
        except AttributeError:
            pass
        # Otherwise must be trying to access one of the atrributes defined in the Schema
        if item in self.Schema._declared_fields.keys():
            return None
        raise AttributeError(f'`{self.__class__.__name__}` has no attribute `{item}`')

    def __getitem__(self, item):
        if item not in self.Schema._declared_fields.keys():
            raise KeyError(f'{self.__class__.__name__} has no key ´{item}´')
        return getattr(self, item)

    def __iter__(self):
        # TODO: missing properties
        return (x for x in self.__dict__ if x not in ('_nested_models', '_parent_model'))


class GwioListField(GwioStructureFieldMixIn, fields.Nested):
    """
    Parent model must extend from HotfixMixIn so that `_parent_model` gets set
    """
    _structure = GwioList

    def __init__(self, model, *args, schema=None, **kwargs):
        self._struct_init_values = dict(model=model)
        self.model_cls = model
        self._parent_model = None  # This will be set by the model that contains the GwioList
        kwargs['many'] = True
        # This is mostly irrelevant since if None is set as the value, GwioList will be initialised in it's place anyway.
        # The reason this is needed, is that during model initialisation an error will be thrown if None is passed if this is not "allowed" to be None
        kwargs['allow_none'] = True

        if not schema:
            schema = model.Schema
        super().__init__(schema, *args, **kwargs)


class GwioDictField(GwioStructureFieldMixIn, fields.Dict):
    _structure = GwioDict

    def __init__(self, *, keys_type, keys_field, values: Union[Type[NestedSchemaModel], Callable], schema_name=None, **kwargs):
        self._struct_init_values = dict(keys=keys_type, values=values)
        self._keys_type = keys_type
        self._keys_field = keys_field
        self._model = values
        self._parent_model = None  # This will be set by the model that contains the GwioDict

        # This is mostly irrelevant since if None is set as the value, GwioDict will be initialised in it's place anyway.
        # The reason this is needed, is that during model initialisation an error will be thrown if None is passed if this is not "allowed" to be None
        kwargs['allow_none'] = True

        try:
            schema = values.Schema
        except AttributeError:
            # This is only intended to be used when `model` is a function that returns the model class
            schema = schema_name
        super().__init__(keys=keys_field, values=fields.Nested(schema), **kwargs)


class GwioModelField(GwioNestingFieldMixIn, fields.Nested):
    # TODO: rename `load`
    def __init__(self, model, *args, load=False, **kwargs):
        assert issubclass(model, NestedSchemaModel), model
        self.model_cls = model
        kwargs['many'] = False
        if load is True:
            kwargs['missing'] = model

        super().__init__(model.Schema, *args, **kwargs)

    def _deserialize(self, value: dict, field_name: str, *args, **kwargs):
        # TODO: Remove `issubclass` when `VatPrice` / `VatPriceModel` have been refactored to make more sense
        if isinstance(value, self.model_cls) or issubclass(self.model_cls, value.__class__):
            return value

        try:
            del value['_parent_model']
        except (KeyError, TypeError):
            pass

        try:
            return self.model_cls(**value)
        except TypeError:
            return self.model_cls(_single_value=value)


class TransitionEvent(NestedSchemaModel):
    class Schema(marshmallow_ext.GwioSchema):
        timestamp = marshmallow_ext.GWIODateTime()


class JSONField(fields.Field):
    """Field that serializes to a JSON string and deserialises to python dict"""

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None
        if isinstance(value, str):
            # Is a string, so converting to python dict to make sure value is valid JSON string
            value = json.loads(value)
        return json.dumps(value)

    def _deserialize(self, value, attr, data, **kwargs):
        if not isinstance(value, str):
            # Not a string, so converting to json string to make sure value can successfully be converted to JSON string
            value = json.dumps(value)

        return json.loads(value)


pre_init.connect(model_pre_init)
post_init.connect(model_post_init)
