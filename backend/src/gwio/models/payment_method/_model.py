# -*- encoding: utf-8 -*-
"""Payment method model."""
import uuid
from decimal import Decimal

from dynamorm import DynaModel
from marshmallow import (
    Schema,
    fields,
    post_load,
)

from gwio.utils.uuid import get_namespace
from gwio.ext.dynamorm import (
    HotfixMixIn,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.logic.payments.types import PaymentTypes
from gwio.models import payment_attribute
from gwio.models.residence import GWIODateTime


# pylint: disable=too-few-public-methods
@adapted_dynamodel
class PaymentMethod(HotfixMixIn, DynaModel):
    class Table:
        name = 'payment_methods'
        hash_key = 'guid'

    class Schemas:
        class BaseSchema():
            guid = fields.UUID(missing=uuid.uuid4)
            name = fields.String(required=True)
            logo = fields.Url(allow_none=True)
            description = fields.String(allow_none=True)
            data = fields.Dict(allow_none=True)
            type = GwioEnumField(enum=PaymentTypes, by_value=True)
            archived = GWIODateTime(allow_none=True)

        class Get(BaseSchema, Schema):
            pass

        class Post(Get):
            """
            name: payment method update
            description: the payment method to be created.
            """

            class Meta:
                exclude = ['guid', 'archived']

        class Put(Get):
            """
            name: payment method update
            description: the payment method to be updated.
            """

            class Meta:
                exclude = ['guid', 'archived']

        class GetInput(GwioSchema):
            """
            name: query
            description: query parameters for the payment method listing.
            """
            archived = fields.Boolean(allow_none=True)

            @post_load
            def archived_conversion(self, data, **_):
                """Converts the boolean input value to the query argument"""
                if not data.pop('archived', False):
                    data['archived__not_exists'] = True
                return data

    Schema = Schemas.BaseSchema

    class API:
        Type = PaymentTypes

        namespace = get_namespace('payment_method')

    def new_payment_from_order(self, order):
        payment = self.new_payment(Decimal(order.total_price), order.summary.currency)
        order.payments.append(payment)
        return payment

    def new_payment(self, amount: Decimal, currency: str):
        return payment_attribute.Payment(
            payment_type=self.type,
            amount=amount,
            currency=currency,
            _data={self.type.value: dict()}
        )
