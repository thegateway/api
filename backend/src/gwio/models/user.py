import base64
import logging
import uuid
from datetime import (
    datetime,
)
from enum import Enum
from typing import (
    Union,
)
from urllib.parse import urlparse

import pycountry
from boto.cognito.identity.exceptions import (
    NotAuthorizedException,
    ResourceNotFoundException,
)
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import (
    fields,
    validate,
)
from werkzeug.exceptions import (
    NotFound,
    Unauthorized,
)

from gwio.environment import Env
from gwio.ext.dynamorm import (
    GwioListField,
    GwioModelField,
    HotfixMixIn,
    JSONField,
    NestedSchemaModel,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
    ISODateField,
)
from gwio.logic.users import notifications
from gwio.models.coupons.promotions import Coupon
from gwio.utils import aws

CognitoFederatedIdentitiesClient = aws.CognitoFederatedIdentitiesClient

logger = logging.getLogger(__name__)
COUNTRY_CODES = tuple(i.alpha_2 for i in pycountry.languages if hasattr(i, 'alpha_2'))

assert uuid  # pylint does not detect the use for whatever reason
# It is important to import order notifications to make sure `Order.notify()` will function correctly
assert notifications


# pylint: disable=too-few-public-methods
class SystemUser:
    id = uuid.UUID(int=0)
    uuid = uuid.UUID(int=0)


class BusinessIdSchema(GwioSchema):
    country = fields.String()
    id = fields.String()


class BusinessId(NestedSchemaModel):
    Schema = BusinessIdSchema


class UserDataSchema(GwioSchema):
    """
    name: user data
    description: user's personal data that are connected to a real person, not to a login identity
    """
    birthday = fields.Date()
    tax_number = fields.String()
    coupons = GwioListField(Coupon)
    business_id = fields.Nested(BusinessIdSchema, allow_none=True)
    language = fields.String()


class UserDataPutSchema(UserDataSchema):
    """
    name: user data
    description: user's personal data that are connected to a real person, not to a login identity
    """

    class Meta:
        exclude = ('coupons',)


class IdentityStatus(Enum):
    REGISTERED = 'registered'
    LINKED = 'linked'


# Lint gets very very confused by dynamorm init
# pylint: disable=access-member-before-definition
# pylint: disable=no-value-for-parameter
# pylint: disable=no-member
@adapted_dynamodel
class UserData(HotfixMixIn, DynaModel):
    class Table:
        name = 'users'
        version = 2
        hash_key = 'guid'

    @adapted_dynamodel
    class IdentityIdIndex(GlobalIndex):
        name = 'user-identity_id-index'
        hash_key = 'identity_id'
        projection = ProjectAll()

    class Schemas:
        class BaseSchema:
            guid = fields.UUID()
            # TODO suggestion to rename `types` -> `destinations`
            # Schema: { "topics": [{"name": "order_updates"}, {"name": "shop_advertising", "shop_guid": <uuid>}]
            #           "types": [{"type": "fcm_push", "data": {"registration_key": "<fcm_registration_key>"}},
            #                     {"type": "webpush", "data": {<webpush data>}},
            #                     {"type": "email", "data": {"email_address": <email>}} ]}
            notifications = JSONField(allow_none=False, missing=dict())  # TODO suggestion to rename `subscriptions`
            coupons = GwioListField(Coupon)
            pin = fields.String(allow_none=True)
            stripe_id = fields.String(allow_none=True)
            birthday = ISODateField(allow_none=True)
            tax_number = fields.String(allow_none=True)
            business_id = GwioModelField(BusinessId, allow_none=True)

            groups = fields.List(fields.String(), missing=list)
            identity_id = fields.String(allow_none=True)
            identity_status = GwioEnumField(enum=IdentityStatus, missing=IdentityStatus.REGISTERED)
            language = fields.String(validate=validate.OneOf(COUNTRY_CODES))

        Get = UserDataSchema
        Put = UserDataPutSchema

    Schema = Schemas.BaseSchema

    def set_identity_id(self, client: CognitoFederatedIdentitiesClient, iss: str, id_token: str):
        """
        Link user's Cognito Federated Identities identity to the given login
        :param client A `CognitoFederatedIdentitiesClient`
        :param iss: Token issuer
        :param id_token: The logged in user's id token
        """
        if self.identity_status is IdentityStatus.LINKED:
            logger.debug('Identity ID already linked')
            return

        if self.identity_id is None:
            # An old user who has no identity id yet
            url = urlparse(iss)
            iss = url.netloc + url.path
            logger.debug('Creating Identity ID for existing user: %s / %s', iss, id_token)

            login = {iss: id_token}
            self.identity_id = client.get_id(logins=login)
        else:
            # An invited user who does not have a login linked to their identity id
            # or a social login user
            logger.debug('Linking Identity ID for existing user')
            try:
                client.link_identity_id_with_login(identity_id=self.identity_id, iss=iss, id_token=id_token)
                self.identity_status = IdentityStatus.LINKED
            except ResourceNotFoundException as e:
                logger.error('User with identity "%s" not found', self.identity_id)
                raise Unauthorized from e
            except NotAuthorizedException as e:
                logger.error('Invalid token')
                raise Unauthorized from e

    @classmethod
    def get_by_identity_id(cls, identity_id: Union[str, uuid.UUID]):
        try:
            identity_id = uuid.UUID(identity_id)
        except (AttributeError, ValueError):  # AttributeError if is already UUID, ValueError if not uuid in str format
            pass
        if isinstance(identity_id, uuid.UUID):
            identity_id = f'{Env.AWS_REGION}:{identity_id}'
        try:
            return next(cls.IdentityIdIndex.query(identity_id=identity_id))
        except StopIteration:
            raise NotFound


# pylint: disable=too-few-public-methods
@adapted_dynamodel
class UserTupasData(HotfixMixIn, DynaModel):
    class Table:
        name = 'user_tupas'
        version = 1
        hash_key = 'guid'

    class Schemas:
        class BaseSchema:
            guid = fields.UUID()
            stamp = fields.String()
            b02k_timestmp = fields.String(allow_none=True)
            b02k_idnbr = fields.String(allow_none=True)

    Schema = Schemas.BaseSchema

    @property
    def is_authed(self):
        if self.b02k_idnbr:
            return True
        return False

    def _generate_string_token(self) -> str:
        return base64.b32encode(self.guid.bytes)[:6].decode('ascii')  # pylint: disable=no-member

    def generate_new_stamp(self):
        self.stamp = '{datetime}{token}'.format(datetime=datetime.now().strftime('%Y%m%d%H%M%S'),
                                                token=self._generate_string_token())
