# -*- coding: utf-8 -*-
"""
WebshopProduct indicating which shops are selling products using which pricing schema
"""
import datetime
import logging
from decimal import Decimal
from uuid import UUID

import marshmallow
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import (
    ValidationError,
    fields,
    post_load,
    pre_dump,
    validates_schema,
)

from gwio.core.index_es_document import (
    delete_webshop_product,
    update_product_es,
)
from gwio.ext.dynamorm import (
    HotfixMixIn,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.pricing import (
    Pricing,
    PricingError,
)
from gwio.models.products import Product
from gwio.models.products.base import TimestampsSchema
from gwio.models.residence import GWIODateTime

log = logging.getLogger(__name__)


class _WebshopProductBaseSchema:
    product_guid = fields.UUID(required=True)
    webshop_guid = fields.UUID(required=True)
    pricing_guid = fields.UUID(allow_none=True)
    stock_level = fields.Decimal(required=False, missing=Decimal('0'))
    for_sale = fields.Boolean(allow_none=True)
    archived = GWIODateTime(allow_none=True)


class _GetWebshopProductSchema(_WebshopProductBaseSchema, GwioSchema):
    pass


# pylint: disable=too-few-public-methods
class ProductElasticsearchDocumentSchema(Product.API.Assessor.EvaluatedProduct.Schemas.Elasticsearch):
    # New values
    shop_guid = fields.UUID()
    shop_name = fields.Str()
    suppliers = fields.List(fields.UUID())


# pylint: disable=too-few-public-methods
class ProductElasticsearchSchema(Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet):
    # Override DateTimes as they are strings, not datetimes
    class Timestamps(TimestampsSchema):
        created = fields.Str()

    # Override DateTimes as they are strings, not datetimes
    activated = fields.Str()
    created = fields.Str()
    timestamps = fields.Nested(Timestamps)

    # New values
    archived = fields.Str()
    shop_guid = fields.UUID()
    shop_name = fields.Str()
    suppliers = fields.List(fields.UUID())
    brand_name = fields.Str()

    # Sort key for Elasticsearch
    sort = fields.Raw()


class ProductSearchResponseSchema(GwioSchema):
    total = fields.Int()
    products = fields.Nested(ProductElasticsearchSchema(), many=True)
    aggregations = fields.Dict()


# pylint: disable=too-few-public-methods
class ProductElasticsearchCustomerSchema(Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet):
    class Meta:
        exclude = ('pricing', 'activated', 'stock_level')

    # Override DateTimes as they are strings, not datetimes
    class Timestamps(TimestampsSchema):
        created = fields.Str()

    # Override DateTimes as they are strings, not datetimes
    archived = fields.Str()
    created = fields.Str()
    timestamps = fields.Nested(Timestamps)

    # Replace stock level with boolean indicator
    in_stock = fields.Bool()

    # New values
    shop_guid = fields.UUID()
    shop_name = fields.Str()
    suppliers = fields.List(fields.UUID())
    brand_name = fields.Str()

    # Sort key for Elasticsearch
    sort = fields.Raw()

    @pre_dump
    def stock_level_to_bool(self, values, **kwargs):
        try:
            values['in_stock'] = values.pop('stock_level') > 0
        except KeyError:
            values['in_stock'] = False
            log.error('Stock level not saved for webshop (%s) product %s', values['shop_guid'], values['guid'])
        return values


class ProductSearchCustomerResponseSchema(GwioSchema):
    total = fields.Int()
    products = fields.Nested(ProductElasticsearchCustomerSchema(), many=True)
    aggregations = fields.Dict()


class _WebshopProductPostSchema(GwioSchema):
    """
    name: a product being sold
    description: a product that is for sale in a given shop with (optional) custom pricing.
    """

    class _CreateWebshopProductSchema(_GetWebshopProductSchema):
        class Meta:
            exclude = ['webshop_guid']

        archived = fields.Bool()
        product_guid = fields.UUID(required=False)
        product = fields.Nested(Product.Schemas.Post)
        tag_guids = fields.List(fields.UUID())

        @post_load
        def archived_to_timestamp(self, values: dict, **kwargs) -> dict:
            if values.get('archived', None):
                values['archived'] = datetime.datetime.now(tz=datetime.timezone.utc)
            else:
                try:
                    del values['archived']
                except KeyError:
                    pass
            return values

        @validates_schema
        def product_or_guid(self, data, **kwargs):
            errors = dict()
            if 'product' in data and 'product_guid' in data:
                error = 'Can not provide both `product` and `product_guid`'
                errors['product'] = [error]
                errors['product_guid'] = [error]
            elif 'product' not in data and 'product_guid' not in data:
                error = 'Must provide either `product` or `product_guid`'
                errors['product'] = [error]
                errors['product_guid'] = [error]
            if errors:
                raise ValidationError(errors)

    products = fields.Nested(
        _CreateWebshopProductSchema,
        many=True,
        allow_none=False,
        required=True,
    )


class _WebshopProductPutSchema(_GetWebshopProductSchema):
    """
    name: product for sale
    description: product for sale in shop to be updated.
    """

    class Meta:
        exclude = ('product_guid', 'webshop_guid')

    archived = fields.Bool()

    @post_load
    def archived_to_none(self, values: dict, **kwargs) -> dict:
        try:
            if not values['archived']:
                values['archived'] = None
        except KeyError:
            pass
        return values


# pylint: disable=too-few-public-methods
@adapted_dynamodel
class WebshopProduct(HotfixMixIn, DynaModel):
    class Table:
        name = 'webshop-products'
        hash_key = 'product_guid'
        range_key = 'webshop_guid'
        identifier = ('product_guid', 'webshop_guid')

    class Schemas:
        Get = _GetWebshopProductSchema
        Post = _WebshopProductPostSchema
        Put = _WebshopProductPutSchema

    Schema = _WebshopProductBaseSchema

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._product = None

    class API:
        namespace = Product.API.namespace

        # pylint: disable=no-member
        class Assessor:
            @classmethod
            def evaluate(cls, *,
                         product: Product = None,
                         webshop_product: 'WebshopProduct' = None,
                         webshop_guid: UUID = None,
                         product_guid: UUID = None,
                         qty: Decimal = None,
                         schema: marshmallow.Schema = None) -> Product.API.Assessor.EvaluatedProduct:
                from gwio.models.webshops import Webshop

                if webshop_guid is not None and webshop_product is not None and webshop_guid != webshop_product.webshop_guid:
                    raise ValueError('`webshop_product.webshop_guid` and `webshop_guid` both provided, but they do not match')
                try:
                    webshop_guid = webshop_product.webshop_guid
                except AttributeError:
                    if webshop_guid is None:
                        raise ValueError('`webshop_product` or `webshop_guid` must be provided')

                try:
                    product = product if product is not None else Product.by_guid(product_guid)
                    wsp = webshop_product if webshop_product is not None else WebshopProduct.get(product_guid=product.guid, webshop_guid=webshop_guid)
                except (Product.DoesNotExist, WebshopProduct.DoesNotExist):
                    raise PricingError(f'Product {product_guid} not available')

                try:
                    if wsp.pricing_guid is None:
                        pricing = Pricing.get(owner=webshop_guid, guid=Pricing.API.generate_guid(name=product.type, owner=webshop_guid))
                    else:
                        pricing = Pricing.by_guid(wsp.pricing_guid)
                except Pricing.DoesNotExist:
                    pricing = None
                product = Product.API.Assessor.evaluate(product, qty=qty, pricing=pricing, schema=schema)
                product.shop_guid = webshop_guid
                product.stock_level = wsp.stock_level
                product.archived = wsp.archived
                product.for_sale = wsp.for_sale
                try:
                    product.shop_name = Webshop.get(guid=webshop_guid).name
                except Webshop.DoesNotExist:
                    log.warning('Indexing webshop product with a nonexistent webshop %s', webshop_guid)
                    product.shop_name = None
                return product

    # Indexes
    @adapted_dynamodel
    class WebshopIndex(GlobalIndex):
        name = 'products-by-webshop-index'
        hash_key = 'webshop_guid'
        projection = ProjectAll()

    @property
    def product(self):
        if not hasattr(self, '_product') or not self._product:
            self._product = Product.by_guid(self.product_guid)
        return self._product


@WebshopProduct.register_hook(hook='post-save', allow_fail=False)
@WebshopProduct.register_hook(hook='post-update', allow_fail=False)
def add_product_to_elastic_search(webshop_product):
    # Adding webshop product affects the `suppliers` of the product
    update_product_es(product_guid=webshop_product.product_guid)


@WebshopProduct.register_hook(hook='post-delete', allow_fail=False)
def delete_product_from_elastic_search(webshop_product):
    delete_webshop_product(product_guid=webshop_product.product_guid, webshop_product=webshop_product)
    # Deleting webshop product affects the `suppliers` of the product
    update_product_es(product_guid=webshop_product.product_guid, update_webshop_products=False)
