import uuid

from dynamorm import DynaModel
from marshmallow import (
    Schema,
    fields,
    post_load,
)

from gwio import geoip
from gwio.ext.dynamorm import (
    HotfixMixIn,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.residence import GWIODateTime


@adapted_dynamodel
class Location(HotfixMixIn, DynaModel):
    class Table:
        name = 'locations'
        hash_key = 'guid'
        version = 0

    class Schemas:
        class BaseSchema:
            guid = fields.UUID(missing=uuid.uuid4)
            name = fields.Str(required=True)
            description = fields.Str()
            parent = fields.UUID(allow_none=True)
            tags = fields.List(fields.UUID(), allow_none=True)
            latitude = fields.Decimal(required=True)
            longitude = fields.Decimal(required=True)
            archived = GWIODateTime(allow_none=True)

        class Get(BaseSchema, Schema):
            geohash = fields.Str()
            olc = fields.Str()

        class Put(BaseSchema, Schema):
            """
            name: location
            description: the location to update.
            """

            class Meta:
                exclude = ('guid', 'archived')

        Post = Put

        class ListQuery(GwioSchema):
            """
            name: a location query
            description: query parameters for location listing
            """
            archived = fields.Boolean()

            @post_load
            def archived_conversion(self, data, **kwargs):
                """Converts the boolean input value to the query argument"""
                value = not data.pop('archived', False)
                if value:
                    data['archived__not_exists'] = value
                return data

    Schema = Schemas.BaseSchema

    @property
    def geohash(self):
        return geoip.geohash.encode(self.latitude, self.longitude)

    @geohash.setter
    def geohash(self, geohash_):
        self.latitude, self.longitude = geoip.geohash.decode(geohash_)

    @property
    def olc(self):
        return geoip.pluscode.encode(self.latitude, self.longitude)

    @olc.setter
    def olc(self, pluscode):
        self.latitude, self.longitude = geoip.pluscode.decode(pluscode)
