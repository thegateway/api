# -*- coding: utf-8 -*-
"""
Tags and ProductTags
"""
import logging
import uuid
from abc import (
    ABC,
    abstractmethod,
)
from datetime import (
    datetime,
    timezone,
)
from enum import Enum
from typing import (
    Generator,
    Union,
)
from uuid import UUID

import amazon.ion.simpleion as ion
import pytz
from marshmallow import (
    ValidationError,
    fields,
    post_load,
    pre_load,
)

from gwio.environment import Env
from gwio.ext import SYSTEM_GUID
from gwio.ext.dynamorm import (
    GwioListField,
    GwioModelField,
    NestedSchemaModel,
)
from gwio.ext.marshmallow import (
    GWIODateTime,
    GwioEnumField,
    GwioSchema,
)
from gwio.models import (
    modifiers,
    products,
)
from gwio.utils.uuid import (
    get_namespace,
    uuid_over_values,
)

log = logging.getLogger(__name__)

_UUID_NAMESPACE = get_namespace('tags')


class TagType(Enum):
    BRAND = 'brand'
    CAMPAIGN = 'campaign'
    CATEGORY = 'category'
    LOCATION = 'location'
    VARIANT = 'variant'  # E.g. same shirt, different size / color.
    SAME = 'same'  # For exact same product (sold by multiple Webshops)
    SIMILAR = 'similar'  # For comparable products, e.g. different brands


class ImageType(Enum):
    MOBILE_BANNER = 'mobile_banner'
    DESKTOP_BANNER = 'desktop_banner'
    LOGO = 'logo'


def _get_product(product_tag):
    from gwio.models.products import Product

    try:
        return Product.by_guid(product_tag.product_guid)
    except Product.DoesNotExist:
        log.warning('Added product tag for product that does not exist %s', product_tag.product_guid)
        return None


class DurationSchema(GwioSchema):
    start = GWIODateTime()
    end = GWIODateTime()


class Duration(NestedSchemaModel):
    Schema = DurationSchema


class TagImageSchema(GwioSchema):
    url = fields.String(required=True, allow_none=False)
    type = GwioEnumField(enum=ImageType, by_value=True, required=True, allow_none=False)


class TagImage(NestedSchemaModel):
    Schema = TagImageSchema


class RedisIndex(ABC):
    @classmethod
    @abstractmethod
    def get_key(cls):
        raise NotImplementedError(f'{cls.__name__} needs to implement `get_key` staticmethod')

    @classmethod
    def query(cls, **kwargs) -> Generator['Tag', None, None]:
        """
        Used to query the index. Gets the tag guids containing set from redis with the key from `cls.get_key`.
        Then gets all the actual tag records from redis and converts them to Tag instances.
        :param kwargs: Passed to the index's `get_key`
        :return:  Generator of Tags
        """
        index_key = cls.get_key(**kwargs)
        # TODO: Refactor this to use `redis.sscan` and do `redis.mget` for each page
        redis_keys = [x.decode('utf8') for x in Tag.API.redis.sscan_iter(index_key)]  # sscan > smembers, because it doesn't block the server as much
        for i, redis_item in enumerate(Tag.API.redis.mget(redis_keys)):
            if redis_item is None:
                # There can be a timing issue that causes this to happen if the item is removed accessing the set and the actual tag record
                # Deleting the item from the set just in case though, as it won't break even if the item wouldn't anymore exist in the set
                Tag.API.redis.srem(index_key, redis_keys[i])
                continue
            yield Tag(**Tag.Schema().loads(ion.loads(redis_item)))


# Lint gets very very confused by dynamorm init
# pylint: disable=access-member-before-definition
class Tag(NestedSchemaModel):  # TODO: Should be separated into a different class to extend from (which would share a lot of code with NestedSchemaModel)
    """This is your basic tag definition.

    This does allow for hierarchical tags in case those are needed (for example for creating
    product categories)

    The `data` attribute can be used to describe the campaign effect:
    {'pricing_data': {'operator': 'MULT', 'value': 0.8}})
    """
    # TODO: Remove, they are in API
    Type = TagType
    ImageType = ImageType

    class DoesNotExist(Exception):
        pass

    class Table:
        name = 'tags'
        hash_key = 'owner'
        range_key = 'guid'

    class Schemas:
        class _BaseSchema(GwioSchema):
            guid = fields.UUID()
            owner = fields.UUID()
            name = fields.String(required=True)
            data = GwioModelField(modifiers.Modifier, allow_none=True)
            duration = GwioModelField(Duration, allow_none=True)
            type = GwioEnumField(enum=TagType, by_value=True, missing=TagType.CATEGORY)
            images = GwioListField(TagImage)
            item_count = fields.Integer(required=False, allow_none=True)  # Deprecated
            product_guids = fields.List(fields.UUID(), missing=list)

            @pre_load
            def unify_name_case(self, values, **_):
                # This is to prevent situation where there is 2 instances of the same tag with different case
                values['name'] = Tag.API.unify_name(values['name'])
                return values

        class Get(_BaseSchema):
            """
            name: tag
            description: tag information
            """
            active = fields.Boolean()
            # TODO: Remove `subtags` once frontend is not using these anymore
            subtags = fields.List(fields.UUID(), missing=list, default=list)
            item_count = fields.Integer(attribute='num_items')  # Deprecated

        # pylint: disable=too-few-public-methods
        class Post(Get):
            """
            name: tag
            description: the tag to be created.
            """

            class Meta:
                # TODO: Remove `subtags` once frontend is not using these anymore
                exclude = ['active', 'guid', 'owner', 'subtags', 'item_count']

            data = fields.Nested(modifiers.UpdateModifierSchema, allow_none=True)
            duration = fields.Nested(DurationSchema, allow_none=True)

            @post_load
            def _validate_duration(self, data, **kwargs):
                tag_type = data.get('type')
                if tag_type is not None and tag_type == TagType.CAMPAIGN:
                    errors = {}
                    if data.get('duration') is None:
                        errors['duration'] = ['Duration field is required for campaign tags.']
                    if data.get('data') is None:
                        errors['data'] = ['Data field is required for campaign tags.']
                    if errors:
                        raise ValidationError(errors)
                return data

        class Put(Post):
            """
            name: tag
            description: the tag to be updated.
            """

        # Used for tags nested inside other tables like with products
        class Nested(Get):
            # Old field in database that we want to get rid of
            product_guids = fields.List(fields.UUID(), missing=list, load_only=True)

        class LogEvent(_BaseSchema):
            class Meta:
                exclude = ('product_guids',)

    Schema = Schemas._BaseSchema

    class API:
        redis = Env.get_redis(Env.RedisDatabases.TAGS)  # TODO: Cache these process-wide, probably in Env
        namespace = _UUID_NAMESPACE
        Type = TagType
        ImageType = ImageType

        @staticmethod
        def unify_name(name):
            return ' '.join([f'{x[0].upper()}{x[1:]}' for x in name.lower().split(' ') if len(x) > 0])

        @classmethod
        def generate_guid(cls, *, name: str, owner: UUID) -> UUID:
            ns = owner if owner is not None else SYSTEM_GUID
            return uuid_over_values(name, owner, ns=ns)

    # Indexes
    class GuidIndex:
        @staticmethod
        def query(*, guid: uuid.UUID):
            redis_res = Tag.API.redis.get(f'{Tag.redis_key_prefix()}_{guid}')
            if redis_res is None:
                return []
            return [Tag(**Tag.Schema().loads(ion.loads(redis_res)))]

    class OwnerGuidIndex(RedisIndex):
        @staticmethod
        def get_key(tag: 'Tag' = None, *, owner: UUID = None, guid: UUID = None):
            try:
                guid = tag.guid
                owner = tag.owner
            except AttributeError:  # tag is None
                if not isinstance(guid, UUID):
                    guid = UUID(guid)
                if not isinstance(owner, UUID):
                    owner = UUID(owner)
            return f'by_owner_guid_{owner}_{guid}'

    class OwnerIndex(RedisIndex):
        @staticmethod
        def get_key(tag: 'Tag' = None, *, owner: UUID = None):
            try:
                owner = tag.owner
            except AttributeError:  # tag is None
                if not isinstance(owner, UUID):
                    owner = UUID(owner)
            return f'by_owner_{owner}'

    class OwnerTypeIndex(RedisIndex):
        # TODO: Replace these with something else that the endpoint decorator can use
        hash_key = 'owner'
        range_key = 'guid'

        @staticmethod
        def get_key(tag: 'Tag' = None, *, owner: UUID = None, type: TagType = None):
            if tag is None and type is None:
                # We don't have to worry about this when this is called with `tag` as that is only the case when saving/deleting a tag
                # and in that case we will get the OwnerIndex key anyway when iterating all the indexes
                return Tag.OwnerIndex.get_key(owner=owner)
            try:
                owner = tag.owner
                type = tag.type
            except AttributeError:  # tag is None
                if not isinstance(owner, UUID):
                    owner = UUID(owner)
                if not isinstance(type, TagType):
                    type = TagType(type)
            return f'by_owner_type_{owner}_{type.value}'

    class TypeIndex(RedisIndex):
        @staticmethod
        def get_key(tag: 'Tag' = None, *, type: TagType = None):
            try:
                type = tag.type
            except AttributeError:  # tag is None
                pass
            if not isinstance(type, TagType):
                type = TagType(type)
            return f'by_type_{type.value}'

    def __init__(self, *, guid=None, owner=None, name="", **attributes):
        """
        Basically the table is partitioned using owner with each tag as the
        range_key. To allow quickly accessing the information via guid only
        a separate index is created for tag guid and to determine the optional
        class hierarchy a second index is used to represent the relations.
        """
        owner = owner if owner is not None else SYSTEM_GUID
        guid = attributes.get('guid', self.API.generate_guid(name=name, owner=owner) if guid is None else guid)
        assert owner
        super().__init__(guid=guid, owner=owner, name=name, **attributes)

    @classmethod
    def by_guid(cls, guid: Union[UUID, str]) -> 'Tag':
        """Get the Tag by id"""
        if not isinstance(guid, UUID):
            guid = UUID(guid)
        for item in cls.GuidIndex.query(guid=guid):
            return item
        raise cls.DoesNotExist

    # pylint: disable=arguments-differ
    def save(self, **kwargs):
        """Just make sure we have valid data before saving"""
        assert self.name
        if not self.owner:
            self.owner = SYSTEM_GUID
        if not self.guid:
            self.guid = self.API.generate_guid(name=self.name, owner=self.owner)

        # Changed data might have changed which indexes the tag belongs to, so safer to just remove from all and add back to the ones it belongs to
        self._remove_from_indexes()

        self.API.redis.set(self.redis_key, self.redis_data)
        for index in self.indexes:
            self.API.redis.sadd(index.get_key(self), self.redis_key)

    @property
    def indexes(self):
        return RedisIndex.__subclasses__()

    @classmethod
    def get(cls, *, owner, guid):
        for item in cls.OwnerGuidIndex.query(owner=owner, guid=guid):
            return item
        raise cls.DoesNotExist

    def delete(self):
        for product_guid in list(self.product_guids):
            try:
                product = products.Product.by_guid(product_guid)
                product.remove_tag(self)
                product.save()
            except products.Product.DoesNotExist:
                # Don't have to care about `product_guids` attribute since we're deleting the record
                pass
        self._remove_from_indexes()
        self.API.redis.delete(self.redis_key)

    def _remove_from_indexes(self):
        # Iterate over all the index keys
        for key in self.API.redis.scan_iter(match='by_*'):
            self.API.redis.srem(key, self.redis_key)

    @classmethod
    def scan(cls):
        for redis_key in cls.API.redis.scan_iter(f'{cls.redis_key_prefix()}_*'):
            redis_item = cls.API.redis.get(redis_key.decode('utf8'))
            yield cls(**cls.Schema().loads(ion.loads(redis_item)))

    def add_product(self, product: 'products.Product'):
        """Add a product to this tag"""
        assert self.guid
        assert product.guid
        self.remove_duplicate_products()
        product.remove_duplicate_tags()

        if product.guid not in self.product_guids:
            self.product_guids.append(product.guid)
        if self.guid not in product.tags:
            product.tags.append(self)

    def remove_product(self, product: 'products.Product'):
        """Remove a product from this tag"""
        assert self.guid
        assert product.guid
        self.remove_duplicate_products()
        product.remove_duplicate_tags()

        try:
            self.product_guids.remove(product.guid)
        except ValueError:
            pass  # wasn't in the list to begin with

        # `self` could potentially be a different instance of the same tag, so `self.tags.remove(self)` wouldn't work
        for pt in product.tags:
            if pt.guid == self.guid:
                product.tags.remove(pt)
                break

    def remove_duplicate_products(self):
        # TODO: Remove once `product_guids` is a set
        self.product_guids = list(set(self.product_guids))

    @property
    def active(self) -> bool:
        if self.duration is not None:
            start = self.duration.start.replace(tzinfo=pytz.UTC)
            end = self.duration.end.replace(tzinfo=pytz.UTC)
            if start <= datetime.now(timezone.utc) <= end:
                return True
        return False

    @property
    def num_items(self) -> int:
        return len(self.product_guids)

    @classmethod
    def redis_key_prefix(cls):
        return cls.__name__.lower()

    @property
    def redis_key(self):
        return f'{self.redis_key_prefix()}_{self.guid}'

    @property
    def redis_data(self):
        return ion.dumps(self.Schema().dumps(self))

    @classmethod
    def query(cls, owner: uuid.UUID = SYSTEM_GUID):
        return cls.OwnerIndex.query(owner=owner)

    def update(self):
        self.save()

    def refresh(self):
        raw = self.API.redis.get(self.redis_key)
        # Yeah, horrible, but simplest way to implement refresh
        self.__init__(**self.Schema().loads(ion.loads(raw)))
