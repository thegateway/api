# coding=utf-8
"""
Model for webshop.
"""
import base64
import datetime
import logging
import os
import uuid
from enum import Enum
from typing import (
    Dict,
    Generator,
    List,
    Union,
)

import envs
import marshmallow.validate
import pytz
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import (
    fields,
    post_dump,
    post_load,
    pre_dump,
    pre_load,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    Unauthorized,
)

from gwio.core import rbac
from gwio.environment import Env
from gwio.ext import SYSTEM_GUID
from gwio.ext.dynamorm import (
    GwioListField,
    GwioModelField,
    HotfixMixIn,
    NestedSchemaModel,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
    HexColor,
    HourMinuteField,
    ISODateField,
)
from gwio.models import (
    locations,
    payment_method,
    products,
    tags,
    webshop_product,
)
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.utils.notify import Notification, NotificationFailException, logger
from gwio.utils.uuid import (
    get_namespace,
)

Location = locations.Location
PaymentMethod = payment_method.PaymentMethod
Product = products.Product
BaseProduct = products.base.BaseProduct
Tag = tags.Tag
WebshopProduct = webshop_product.WebshopProduct

log = logging.getLogger(__name__)

TIMEZONE = Env.TIMEZONE


# noinspection SpellCheckingInspection
class SupportedSocialMedia(Enum):
    FACEBOOK = 'facebook'
    GOOGLE = 'google'
    INSTAGRAM = 'instagram'
    LINKEDIN = 'linkedin'
    TWITTER = 'twitter'


class WebshopAddressSchema(GwioSchema):
    label = fields.String(allow_none=True)
    name = fields.String(required=True, allow_none=False)
    street = fields.String(required=True, allow_none=False)
    city = fields.String(required=True, allow_none=False)
    zipcode = fields.String(required=True, allow_none=False)
    country = fields.String(required=True, allow_none=False)


class _WebshopAlertSchema(GwioSchema):
    text = fields.String(required=True, allow_none=False)
    color = HexColor(required=True, allow_none=False)
    link = fields.Url(allow_none=True)


class WebshopAlert(NestedSchemaModel):
    Schema = _WebshopAlertSchema

    class Schemas:
        Get = _WebshopAlertSchema


class _WebshopBannerSchema(GwioSchema):
    title = fields.String(allow_none=True)
    image = fields.Url(required=True, allow_none=False)
    link = fields.Url(allow_none=True)
    text = fields.String(allow_none=True)
    type = fields.String(required=True, allow_none=False)


class WebshopBanner(NestedSchemaModel):
    Schema = _WebshopBannerSchema


class _WebshopBannersSchema(GwioSchema):
    main = GwioListField(WebshopBanner)
    side = GwioListField(WebshopBanner)
    top = GwioListField(WebshopBanner)


class _EmailBaseSchema(GwioSchema):
    label = fields.String(required=True, allow_none=False)
    email = fields.Email(required=True, allow_none=False)


class Email(NestedSchemaModel):
    Schema = _EmailBaseSchema

    class Schemas:
        class Get(_EmailBaseSchema):
            pass


class _WebshopOpeningHoursExceptionSchema(GwioSchema):
    date = ISODateField(required=True)
    end_at = HourMinuteField(required=False, allow_none=True)
    start_at = HourMinuteField(required=False, allow_none=True)
    desc = fields.String(allow_none=True)


class OpeningHoursException(NestedSchemaModel):
    Schema = _WebshopOpeningHoursExceptionSchema

    class Schemas:
        Get = _WebshopOpeningHoursExceptionSchema


class _WebshopNormalOpeningHoursSchema(GwioSchema):
    day = fields.Int(required=True)
    start_at = HourMinuteField(required=True)
    end_at = HourMinuteField(required=True)


class OpeningHoursNormal(NestedSchemaModel):
    Schema = _WebshopNormalOpeningHoursSchema


class _WebshopCurrentOpeningHoursSchema(_WebshopNormalOpeningHoursSchema):
    exceptional = fields.String()


class _PhoneBaseSchema(GwioSchema):
    label = fields.String(required=True, allow_none=False)
    phone = fields.String(required=True, allow_none=False)  # TODO: rename to `number` as this is already item in `phones` list


class Phone(NestedSchemaModel):
    Schema = _PhoneBaseSchema

    class Schemas:
        Get = _PhoneBaseSchema


class _BaseSocialMediaSchema(GwioSchema):
    label = GwioEnumField(enum=SupportedSocialMedia, by_value=True)
    link = fields.Url(allow_none=True)
    analytics = fields.String(allow_none=True)

    @pre_dump
    def validate_link_analytics(self, social_media, **_):
        if not social_media['link'] and not social_media['analytics']:
            raise ValueError("Attributes 'link' and 'analytics' cannot be both None")
        return social_media


class SocialMedia(NestedSchemaModel):
    Schema = _BaseSocialMediaSchema

    class Schemas:
        Get = _BaseSocialMediaSchema


class _WebshopPublicDataSchema(GwioSchema):
    stripe = fields.String(allow_none=True)
    domain = fields.String(allow_none=True)


class PublicData(NestedSchemaModel):
    Schema = _WebshopPublicDataSchema

    class Schemas:
        Get = _WebshopPublicDataSchema


class _WebshopPrivateDataPaytrailSchema(GwioSchema):
    hash = fields.String(required=True, allow_none=False)
    id = fields.String(required=True, allow_none=False)


class _WebshopPaytrailData(NestedSchemaModel):
    Schema = _WebshopPrivateDataPaytrailSchema

    class Schemas:
        Get = _WebshopPrivateDataPaytrailSchema


class _WebshopPrivateDataStripeSchema(GwioSchema):
    account_id = fields.String(required=False, allow_none=True)  # Account id does not exist before webshop starts Stripe integration
    commission_percentage = fields.Decimal(required=True, allow_none=False, validate=marshmallow.validate.Range(0, 100))


class _WebshopStripeData(NestedSchemaModel):
    Schema = _WebshopPrivateDataStripeSchema

    class Schemas:
        Get = _WebshopPrivateDataStripeSchema


class _WebshopPrivateDataPayuSchema(GwioSchema):
    commission_percentage = fields.Decimal(required=True, allow_none=False, validate=marshmallow.validate.Range(0, 100))


class _WebshopPayuData(NestedSchemaModel):
    Schema = _WebshopPrivateDataPayuSchema

    class Schemas:
        Get = _WebshopPrivateDataPayuSchema


class _WebshopPrivateDataXpressCouriersSchame(GwioSchema):
    client_id = fields.String()


class _WebshopXpressCouriersData(NestedSchemaModel):
    Schema = _WebshopPrivateDataXpressCouriersSchame

    class Schemas:
        Get = _WebshopPrivateDataXpressCouriersSchame
        Put = _WebshopPrivateDataXpressCouriersSchame


class _PrivateDataSchema(GwioSchema):
    paytrail = GwioModelField(_WebshopPaytrailData, allow_none=True)
    stripe = GwioModelField(_WebshopStripeData, allow_none=True)
    payu = GwioModelField(_WebshopPayuData, allow_none=True)
    xpress_couriers = GwioModelField(_WebshopXpressCouriersData, allow_none=True)
    email = fields.Email(allow_none=True)
    secret_key = fields.String(missing=lambda: base64.b64encode(os.urandom(32)).decode())


class PrivateData(NestedSchemaModel):
    Schema = _PrivateDataSchema

    class Schemas:
        Get = _PrivateDataSchema


class _BaseStaticPageSchema(GwioSchema):
    guid = fields.UUID(allow_none=False, missing=uuid.uuid4)
    title = fields.String(required=True, allow_none=False)
    link = fields.Url(required=True, allow_none=False)
    inline = fields.Bool(required=True, allow_none=False)


class StaticPage(NestedSchemaModel):
    Schema = _BaseStaticPageSchema

    class Schemas:
        Get = _BaseStaticPageSchema

        class Put(_BaseStaticPageSchema):
            guid = fields.UUID(allow_none=False)


class OpeningHoursSchema(GwioSchema):
    timezone = fields.String()
    opening_hours = fields.Nested(_WebshopCurrentOpeningHoursSchema, many=True)
    opening_hours_normal = fields.Nested(_WebshopNormalOpeningHoursSchema, many=True)
    opening_hours_exceptions = fields.Nested(_WebshopOpeningHoursExceptionSchema, many=True,
                                             unknown=marshmallow.EXCLUDE)


class PromotionsSchema(GwioSchema):
    local = fields.Nested(Tag.Schemas.Get, many=True)
    general = fields.Nested(Tag.Schemas.Get, many=True)


# pylint: disable=too-few-public-methods
class OpeningHoursUpdateSchema(OpeningHoursSchema):
    """
    name: opening hours
    description: the opening hours to be updated.
    """

    class Meta:
        exclude = ['opening_hours']


class _RequiresActionSchema(GwioSchema):
    url = fields.String()
    service = fields.String()
    description = fields.String()


class RequiresActionSchema(NestedSchemaModel):
    Schema = _RequiresActionSchema

    class Schemas:
        Get = _RequiresActionSchema


class WebshopAddress(NestedSchemaModel):
    Schema = WebshopAddressSchema


class WebshopBanners(NestedSchemaModel):
    Schema = _WebshopBannersSchema


class _WebshopBaseSchema:
    guid = fields.UUID(missing=uuid.uuid4)
    name = fields.String(required=True, allow_none=False)
    logo = fields.Url(allow_none=True)
    addresses = GwioListField(WebshopAddress)
    phones = GwioListField(Phone)
    locations = fields.List(fields.UUID(), missing=list)
    alert = GwioModelField(WebshopAlert, allow_none=True)
    banners = GwioModelField(WebshopBanners)
    brand_color = HexColor()
    emails = GwioListField(Email)
    enabled = fields.Boolean(default=False)
    timezone = fields.String(default=TIMEZONE)
    person_in_charge = fields.String()
    social_media = GwioListField(SocialMedia)
    public_data = GwioModelField(PublicData)
    business_id = fields.String(required=True)
    websites = fields.List(fields.Url())
    description = fields.String(allow_none=True)
    tags = fields.List(fields.UUID(), allow_none=True)
    shorthand_name = fields.String(allow_none=True)
    opening_hours_normal = GwioListField(OpeningHoursNormal)
    opening_hours_exceptions = GwioListField(OpeningHoursException)
    payment_method_guids = fields.List(fields.UUID(), missing=list)
    static_pages = GwioListField(StaticPage)
    _private_data = GwioModelField(PrivateData, allow_none=True, load=True)


class _GetWebshop(_WebshopBaseSchema, GwioSchema):
    class Meta:
        exlude = [
            'opening_hours_normal',
            'opening_hours_exceptions',
            'payment_method_guids',
            'static_pages',
        ]

    phone = fields.String()
    phones = fields.Nested(Phone.Schemas.Get, many=True)
    delivery_methods = fields.Nested(BaseProduct.Schemas.DeliveryMethodGet, many=True)
    email = fields.String()
    emails = fields.Nested(Email.Schemas.Get, many=True)
    opening_hours = fields.Nested(OpeningHoursSchema)
    payment_methods = fields.Nested(PaymentMethod.Schemas.Get, many=True)
    promotions = fields.Nested(PromotionsSchema, allow_none=True)

    alert = fields.Nested(WebshopAlert.Schemas.Get, allow_none=True)
    public_data = fields.Nested(PublicData.Schemas.Get)
    social_media = fields.Nested(SocialMedia.Schemas.Get, many=True)
    opening_hours_exceptions = fields.Nested(OpeningHoursException.Schemas.Get, many=True, missing=list)
    static_pages = fields.Nested(StaticPage.Schemas.Get, many=True, missing=list)
    requires_action = fields.Nested(RequiresActionSchema.Schemas.Get, many=True, missing=list)

    @post_dump
    def remove_private_data(self, data, **kwargs):
        try:
            _ = data['_private_data']
        except KeyError:
            # No point in checking for admin if `private_data` does not even exist
            return data

        private_data = data.pop('_private_data', None)
        if rbac.admin():
            data['private_data'] = private_data
        return data

    @post_dump
    def remove_requires_action(self, data, **kwargs):
        try:
            _ = data['requires_action']
        except KeyError:
            # No point in checking for rights if `requires_action` does not even exist
            return data

        if not (rbac.admin() or rbac.shopkeeper(uuid.UUID(data['guid']))):
            del data['requires_action']

        return data


# pylint: disable=too-few-public-methods
class _PostWebshop(_GetWebshop):
    """
    name: shop
    description: the shop to be created.
    """

    class Meta:
        exclude = ['delivery_methods', 'email', 'guid', 'opening_hours', 'payment_methods', 'promotions', 'phone', 'requires_action']

    person_in_charge = fields.String(required=True)

    @pre_load
    def remove_private_data(self, data, **kwargs):
        try:
            _ = data['_private_data']
        except KeyError:
            # No point in checking for admin if `private_data` does not even exist
            return data

        if not rbac.admin():
            del data['_private_data']
        return data

    @pre_load
    def check_business_id(self, data, **_):
        try:
            # pylint: disable=no-value-for-parameter
            if list(Webshop.BusinessIdIndex.scan(business_id=data['business_id'])):
                raise Conflict(description='Webshop business_id already exists')
        except KeyError:
            pass
        return data

    @post_load
    def check_location_and_tag_guids(self, values, **_):
        try:
            if values['locations']:
                for l_guid in values['locations']:
                    try:
                        Location.get(guid=l_guid)
                    except Location.DoesNotExist:
                        raise BadRequest('Location guid is not valid')
        except KeyError:
            pass
        try:
            if values['tags'] is not None:
                for guid in values['tags']:
                    Tag.by_guid(guid)
        except KeyError:
            pass
        except Tag.DoesNotExist:
            raise BadRequest(description='Unknown tag guid')
        return values

    @post_load
    def check_shorthand_name(self, values, **_):
        try:
            shorthand_name = values['shorthand_name']
        except KeyError:
            # shorthand_name not included
            return values
        if shorthand_name is None:
            return values

        LEN_SHORTHAND_NAME = 12
        illegal_statement_description_characters = ['<', '>', '\\', '\'', '"', '*']
        for c in illegal_statement_description_characters:
            if c in shorthand_name:
                raise BadRequest(description=f'`shorthand_name` cannot include {", ".join(illegal_statement_description_characters)}')
        if len(values['shorthand_name']) > LEN_SHORTHAND_NAME:
            raise BadRequest(description=f'`shorthand_name` cannot be longer than {LEN_SHORTHAND_NAME} characters')
        return values


# pylint: disable=too-few-public-methods
class _PutWebshop(_PostWebshop):
    """
    name: shop
    description: a shop to be updated.
    """
    name = fields.String(allow_none=False)
    person_in_charge = fields.String()

    class Meta(_PostWebshop.Meta):
        exclude = _PostWebshop.Meta.exclude


# pylint: disable=too-many-instance-attributes
@adapted_dynamodel
class Webshop(HotfixMixIn, DynaModel):
    class Table:
        name = 'webshops'
        hash_key = 'guid'
        version = 5

    class Schemas:
        Base = _WebshopBaseSchema
        Get = _GetWebshop
        Post = _PostWebshop
        Put = _PutWebshop

        class Nested(Base, GwioSchema):
            class Meta:
                exclude = [x for x in dir(_WebshopBaseSchema) if not x.startswith('__') and x not in ('guid', 'name', 'business_id', 'phone', 'email', 'logo')]

            # To override missing, we want to avoid any chance of random guid popping up when webshop data is nested inside another model
            guid = fields.UUID(required=True)

            # because it's easier to extend from Base than Get, and then just add these...
            email = fields.String()
            phone = fields.String()

    Schema = Schemas.Base

    class API:
        namespace = get_namespace('shops')

        class OpeningHours:
            marshal = OpeningHoursSchema
            update = OpeningHoursUpdateSchema

        PaymentMethod = PaymentMethod
        StaticPage = StaticPage

    # Indexes
    @adapted_dynamodel
    class BusinessIdIndex(GlobalIndex):
        name = 'business-id-index'
        hash_key = 'business_id'
        projection = ProjectAll()

    @property
    def promotions(self) -> dict:
        # XXX: Dirty tricks, but it's 1 AM and I have a headache.
        if envs.env('ENABLE_HORRIBLY_INEFFICIENT_TAG_QUERY'):
            return dict(
                local=[tag for tag in Tag.query(owner=self.guid) if tag.active],  # pylint: disable=no-member
                general=[tag for tag in Tag.query(owner=SYSTEM_GUID) if tag.active]
            )
        else:
            return dict()

    @property
    def email(self) -> Union[str, None]:
        """Should return the shop's for public use email."""
        try:
            # pylint: disable=no-member
            return next(iter(self.emails)).email
        except StopIteration:
            return None

    @property
    def phone(self) -> Union[str, None]:
        """Should return the shop's for public use phone number."""
        try:
            # pylint: disable=no-member
            return next(iter(self.phones)).phone
        except StopIteration:
            return None

    @property
    def requires_action(self):
        # pylint: disable=no-member  # pylint gets confused about dynamorm
        if not (rbac.admin() or rbac.shopkeeper(self.guid)):
            return None
        try:
            if self._private_data.stripe.account_id is not None:
                return None
        except AttributeError:
            pass
        return [dict(url=f'/webshops/{self.guid}/payment_methods/stripe/account_link',
                     service='stripe',
                     description='Stripe account onboarding')]

    def __get_opening_hours(self) -> dict:
        # Note the days start from 0 (monday)
        tz = pytz.timezone(self.timezone if self.timezone is not None else TIMEZONE)
        now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC).astimezone(tz=tz)
        current_date = datetime.date(now.year, now.month, now.day)
        hours = list()
        for i in range(7):
            hours.append(dict(day=i, start_at=None, end_at=None, exceptional=None))
        if self.opening_hours_normal is not None:
            for normal in self.opening_hours_normal:
                day = normal['day']
                hours[day]['start_at'] = normal['start_at']
                hours[day]['end_at'] = normal['end_at']

        if self.opening_hours_exceptions is not None:
            for exception in self.opening_hours_exceptions:
                from_now = exception['date'] - current_date
                # Only the exceptions that in the coming week
                if 0 <= from_now.days < 7:
                    day = (current_date.weekday() + from_now.days) % 7
                    try:
                        hours[day]['start_at'] = exception['start_at']
                        hours[day]['end_at'] = exception['end_at']
                    except KeyError:
                        # As agreed with Markus, start_at and end_at being None/Null is equivalent of day being closed.
                        hours[day]['start_at'] = None
                        hours[day]['end_at'] = None
                    try:
                        description = exception.desc + " "
                    except TypeError:
                        description = ""
                    hours[day]['exceptional'] = f'{description}({exception.date.strftime("%d.%m")})'

        return dict(timezone=self.timezone,
                    opening_hours=hours,
                    opening_hours_normal=self.opening_hours_normal,
                    opening_hours_exceptions=self.opening_hours_exceptions)

    # pylint: disable=no-member
    def __set_opening_hours(self, value: dict):
        self.opening_hours_normal = value.get('opening_hours_normal', list())
        self.opening_hours_exceptions = value.get('opening_hours_exceptions', list())
        self.timezone = value.get('timezone', TIMEZONE)

    opening_hours = property(__get_opening_hours, __set_opening_hours)

    def __get_payment_methods(self) -> List[PaymentMethod]:
        methods = list()
        for item in self.payment_method_guids:
            try:
                methods.append(PaymentMethod.get(guid=item))
            except PaymentMethod.DoesNotExist:
                pass
        return methods

    def __set_payment_methods(self, value: list):
        self.payment_method_guids = value

    payment_methods = property(__get_payment_methods, __set_payment_methods)

    def __get_private_data(self) -> Union[Dict, None]:
        if rbac.admin() or rbac.shopkeeper(self.guid):
            return self._private_data
        return None

    def __set_private_data(self, value: Dict):
        if rbac.admin():
            # Allow only admin to update:
            # * stripe commission or account id
            # * payu commission
            # * xpress couriers client id
            self._private_data = value
            return
        elif rbac.shopkeeper(self.guid):
            value['stripe'] = self._private_data.stripe
            value['payu'] = self._private_data.payu
            value['xpress_couriers'] = self._private_data.xpress_couriers
            self._private_data = value
            return
        raise Unauthorized

    private_data = property(__get_private_data, __set_private_data)

    def __get_delivery_methods(self) -> Generator[Product, None, None]:
        # Check if delivery methods are even available for this vertical
        return Product.TypeIndex.query(type=PRODUCT_TYPE_DELIVERY_METHOD, activated__exists=True)

    def __set_delivery_methods(self, value: list):
        # Check if delivery methods are even available for this vertical
        if not hasattr(Product, 'TypeIndex'):
            return

        # Remove the old methods
        for method in self.delivery_methods:
            # pylint: disable=broad-except
            try:
                shop_method = WebshopProduct.get(product_guid=method.guid, webshop_guid=self.guid)
                shop_method.delete()
            except Exception as e:
                log.exception(e)

        # Add new methods
        for method in value:
            method_guid = method['method_guid']
            pricing_guid = method.get('pricing_guid')  # default pricing has no guid
            try:
                product = Product.by_guid(method_guid)
                if product.type != PRODUCT_TYPE_DELIVERY_METHOD:
                    raise Product.DoesNotExist
                shop_method = WebshopProduct(product_guid=method_guid, webshop_guid=self.guid, pricing_guid=pricing_guid)
                shop_method.save()
            except Product.DoesNotExist:
                log.warning('Trying to add unknown delivery method %s', str(method_guid))

    delivery_methods = property(__get_delivery_methods, __set_delivery_methods)

    def notify(self, notification: Notification) -> None:
        """
        Delivers a notification to webshop's users using all the means the users wish to receive notifications in.

        This function is both context and delivery method agnostic on purpose.
        """
        try:
            notification.send([dict(type='email', data=dict(email_address=self.email))])
        except NotificationFailException as e:
            logger.error(e)
        # TODO: Implement this to do the same work as `gwio.user.user.User.notify` does.
