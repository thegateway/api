"""
Brand model definition.
"""
import uuid

from dynamorm import DynaModel
from marshmallow import (
    fields,
)

from gwio.ext.dynamorm import (
    GwioModelField,
    HotfixMixIn,
    NestedSchemaModel,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.tags import Tag
from gwio.utils.uuid import get_namespace


class ContactSchema(GwioSchema):
    name = fields.String()
    address = fields.String()
    phone_number = fields.String()
    email = fields.String()


class BrandContact(NestedSchemaModel):
    Schema = ContactSchema


@adapted_dynamodel
class Brand(HotfixMixIn, DynaModel):
    class Table:
        name = 'brands'
        hash_key = 'guid'

    class API:
        namespace = get_namespace('brand')

        @classmethod
        def generate_uuid(cls, *, ns: uuid.UUID, name: str) -> uuid.UUID:
            return uuid.uuid5(ns, name)

    class Schemas:
        class BaseSchema:
            guid = fields.UUID()
            name = fields.String(required=True)
            contact = GwioModelField(BrandContact, allow_none=True)
            data = fields.Dict(allow_none=True)

        # pylint: disable=too-few-public-methods
        class Get(GwioSchema, BaseSchema):
            """
            name: brand
            description: a brand
            """
            tags = fields.Nested(Tag.Schemas.Get(exclude=['subtags']), many=True)

        # pylint: disable=too-few-public-methods
        class Post(Get):
            """
            name: brand
            description: the brand to be created
            """

            class Meta:
                exclude = ['guid', 'tags']

        # pylint: disable=too-few-public-methods
        class Put(Get):
            """
            name: brand
            description: the brand to be updated
            """

            class Meta:
                # Name can not be modified because guid is calculated from the name
                exclude = ['guid', 'name', 'tags']

    Schema = Schemas.BaseSchema

    def __init__(self, **attributes):
        hash_key = attributes.get(Brand.Table.hash_key)

        try:
            guid = Brand.API.generate_uuid(name=attributes['name'], ns=Brand.API.namespace)
        except KeyError:
            guid = None

        assert not hash_key or hash_key in (guid, str(guid))
        attributes[Brand.Table.hash_key] = guid

        super().__init__(**attributes)

    @property
    def tags(self):
        return list(Tag.query(owner=self.guid))
