import datetime
import uuid
from enum import Enum

from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import (
    Schema,
    fields,
    validate,
)

from gwio.environment import Env
from gwio.ext.dynamorm import (
    GwioListField,
    GwioModelField,
    HotfixMixIn,
    NestedSchemaModel,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GWIODateTime,
    GwioEnumField,
    GwioSchema,
)


class OwnershipType(Enum):
    """
    These are in Finnish because they are based on local legislation and the types will vary in different
    jurisdictions.
    """
    KIINTEISTO = 'kiinteisto'
    ASOY = 'asoy'
    ASUMISOIKEUS = 'asumisoikeus'
    VUOKRA = 'vuokra'


class HouseType(Enum):
    APARTMENT = 'apartment'  # Kerrostalo
    TOWNHOUSE = 'townhouse'  # Rivitalo
    DUPLEX = 'duplex'  # Paritalo
    DETACHED = 'detached'  # Omakoti- tai erillistalo


class ResidenceAddressSchema(GwioSchema):
    street_address = fields.String(required=True)
    postal_code = fields.String(required=True)


class FileSchema(GwioSchema):
    name = fields.Str(required=True)
    url = fields.Url(required=True)


class ResidenceAddress(NestedSchemaModel):
    Schema = ResidenceAddressSchema


class ResidenceFile(NestedSchemaModel):
    Schema = FileSchema


@adapted_dynamodel
class Residence(HotfixMixIn, DynaModel):
    # pylint: disable=too-few-public-methods
    class Table:
        name = 'residences'
        hash_key = 'guid'
        version = 1

    @adapted_dynamodel
    class ByOwner(GlobalIndex):
        name = 'residence-owner-index'
        hash_key = 'owner_identity_id'
        projection = ProjectAll()

    @classmethod
    def get_by_owner(cls, owner_identity_guid, archived=False):
        identity_id = f'{Env.AWS_REGION}:{str(owner_identity_guid)}'
        return cls.ByOwner.query(owner_identity_id=identity_id, archived__not_exists=archived)  # pylint: disable=no-value-for-parameter

    class Schemas:
        class BaseSchema:
            # pylint: disable=unnecessary-lambda
            guid = fields.UUID(missing=lambda: uuid.uuid4())
            name = fields.Str(allow_none=True)
            ownership = GwioEnumField(enum=OwnershipType, by_value=True, required=True)
            year_built = fields.Integer(required=True, validate=validate.Range(max=datetime.datetime.utcnow().year))
            owner_identity_id = fields.Str(required=True)
            house_type = GwioEnumField(enum=HouseType, by_value=True, required=True)
            address = GwioModelField(ResidenceAddress, required=True)
            files = GwioListField(ResidenceFile)
            archived = GWIODateTime(allow_none=True)
            data = fields.Dict(default=dict)

        class Get(Schema, BaseSchema):
            pass

        class Post(Get):
            """
            name: residence
            description: the residence to be created.
            """

            class Meta:
                exclude = ('guid', 'archived')

        class Put(Get):
            """
            name: residence
            description: the residence to be updated.
            """

            class Meta:
                exclude = ('guid', 'owner_identity_id', 'archived')

        class ListResidences(GwioSchema):
            """
            name: query
            description: query parameters for the residences to list.
            """
            include_archived = fields.Boolean()

    Schema = Schemas.BaseSchema

    class API:
        OwnershipType = OwnershipType
        HouseType = HouseType
