# -*- coding: utf-8 -*-
"""
Implement pricing
"""
import uuid
from decimal import Decimal
from enum import Enum
from typing import Union
from uuid import UUID

from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import (
    Schema,
    fields,
)

from gwio.utils.uuid import get_uuid
from gwio.ext.dynamorm import (
    GwioModelField,
    HotfixMixIn,
    NestedSchemaModel,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.ext.vat import (
    Price,
    VatPrice,
)


class PricingError(LookupError):
    pass


class PricingOperation(Enum):
    ADD = 'add'
    SUB = 'sub'
    MULT = 'mult'
    SET = 'set'


class _BasePricingMethodSchema(GwioSchema):
    op = GwioEnumField(enum=PricingOperation, by_value=True)
    value = fields.Decimal()


class PricingMethod(NestedSchemaModel):
    Schema = _BasePricingMethodSchema

    def _mult(self, price: Price) -> Price:
        return price * self.value

    def _add(self, price: Price) -> Price:
        return price + self.value

    def _set(self, price: Price) -> Price:
        return Price(self.value, currency=price.currency)

    def _sub(self, price: Price) -> Price:
        return price - self.value

    _ops = {
        PricingOperation.MULT: _mult,
        PricingOperation.ADD: _add,
        PricingOperation.SET: _set,
        PricingOperation.SUB: _sub,
    }

    def apply(self, price: Price):
        return self._ops[self.op](self, price)

    def __init__(self, **kwargs):
        # TODO: find out why the values are sometimes passed as strings into this init
        if 'op' in kwargs:
            kwargs['op'] = PricingOperation(kwargs['op'])
        if 'value' in kwargs:
            kwargs['value'] = Decimal(kwargs['value'])
        super().__init__(**kwargs)


#
#     class SubSchema(GwioSchema):
#         op = EnumField(enum=PricingOperation, by_value=True)
#         value = fields.Decimal()


class PriceType(Enum):
    COST = 'cost'
    RETAIL = 'retail'

    def __str__(self):
        return str(self.value)


class PricingMethods(NestedSchemaModel):
    """
    There must be an attribute for each price type
    name of the attribute must match the value of associated PriceType
    """

    class Schema(GwioSchema):
        cost = GwioModelField(PricingMethod, allow_none=True)
        retail = GwioModelField(PricingMethod, allow_none=True)

    class Schemas:
        class Get(Schema):
            cost = fields.Nested(PricingMethod.Schema, allow_none=True)
            retail = fields.Nested(PricingMethod.Schema, allow_none=True)


class _BaseSchema:
    owner = fields.UUID()
    guid = fields.UUID()
    name = fields.String(required=True)
    methods = GwioModelField(PricingMethods, allow_none=True)


class _PricingGetSchema(_BaseSchema, Schema):
    methods = fields.Nested(PricingMethods.Schemas.Get, allow_none=True)


class _PricingPostSchema(_PricingGetSchema):
    """
    name: payment method
    description: the pricing model to be created.
    """

    class Meta:
        exclude = ['owner', 'guid']


class _PricingPutSchema(_PricingPostSchema):
    """
    name: payment method
    description: the pricing model to be updated.
    """

    class Meta(_PricingPostSchema.Meta):
        exclude = _PricingPostSchema.Meta.exclude + ['name']


@adapted_dynamodel
class Pricing(HotfixMixIn, DynaModel):
    class Table:
        # TODO: Should be `pricings`
        name = 'pricing'
        version = 1

        hash_key = 'owner'
        range_key = 'guid'

    class Schemas:
        Get = _PricingGetSchema
        Post = _PricingPostSchema
        Put = _PricingPutSchema

    Schema = _BaseSchema

    class API:
        @staticmethod
        def generate_guid(*, name, owner, **_):
            assert name is not None
            assert owner is not None
            return get_uuid(name, ns=owner)

        @staticmethod
        def update_object(obj, *, methods=None, **_):
            """API is only allowed to update the methods"""
            cost = methods.get('cost')
            if cost is not None and not isinstance(cost, PricingMethod):
                cost = PricingMethod(**cost)
            retail = methods.get('retail')
            if retail is not None and not isinstance(retail, PricingMethod):
                retail = PricingMethod(**retail)
            obj.methods = PricingMethods(cost=cost, retail=retail)

    # pylint: disable=too-few-public-methods
    @adapted_dynamodel
    class GuidIndex(GlobalIndex):
        name = 'pricing-index'
        hash_key = 'guid'
        projection = ProjectAll()

    def __init__(self, owner=None, guid=None, name=None, methods=None, **kwargs):
        """To make it possible to initialize methods via dict()"""

        def _force_guid(guid_):
            if isinstance(guid_, uuid.UUID) or guid_ is None:
                return guid_
            elif isinstance(guid_, str):
                return uuid.UUID(guid_)
            else:
                raise ValueError(f"invalid type {type(guid_)} for guid")

        # When loading the record, guids are strings before calling the super's init
        guid = _force_guid(guid)
        owner = _force_guid(owner)

        generated_guid = Pricing.API.generate_guid(owner=owner, name=name, **kwargs)
        assert guid is None or guid == generated_guid
        guid = generated_guid

        super().__init__(owner=owner, guid=guid, name=name, **kwargs)
        if isinstance(methods, dict):
            self.API.update_object(self, methods=methods)

    @classmethod
    def by_guid(cls, guid: UUID) -> 'Pricing':
        """Get the first item"""
        # pylint: disable=no-value-for-parameter
        for item in cls.GuidIndex.query(guid=guid):
            return item
        raise cls.DoesNotExist

    def apply(self, price: Union[VatPrice, Price], price_type: PriceType) -> Union[VatPrice, Price, None]:
        """Apply pricing to given price, price_type defines the method to be used"""
        if price is None:
            return None

        # All pricing calculations are done in VAT0, but for VatPrice() the result should be VatPrice()
        # in the same VAT category as the original price.
        # Note for ints, floats and Decimals the returned value is of type Price()
        value = getattr(self.methods, str(price_type)).apply(Price(price))
        return value if not isinstance(price, VatPrice) else value @ price.vat_model
