# -*- coding: utf-8 -*-
"""
Make sure all verticals get imported
"""
from . import imeds, tradelinkpro, vpk, sma, remppa

assert imeds  # pyflakes issue, this way it gets "usage"
assert tradelinkpro
assert vpk
assert sma
assert remppa
