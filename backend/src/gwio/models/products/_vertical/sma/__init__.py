# -*- coding: utf-8 -*-
"""
Import VPK Product
"""
from . import product

# pyflakes issue, this will prevent the complaint of import unused
assert product
