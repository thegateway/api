# -*- coding: utf-8 -*-
"""
SMA product model (WIP)
"""
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)

from gwio.ext.dynamorm import adapted_dynamodel
from gwio.models.products.base import BaseProduct


# pylint: disable=too-few-public-methods
@adapted_dynamodel('sma')
class Product(BaseProduct, DynaModel):
    # Attributes must be defined here because dynamorm somehow only reads attributes this class directly has...

    class Table:
        name = BaseProduct.Table.name
        hash_key = BaseProduct.Table.hash_key
        range_key = BaseProduct.Table.range_key
        version = 2

    Schema = BaseProduct.Schema

    @adapted_dynamodel('sma')
    class GuidIndex(GlobalIndex):
        name = 'product-guid-index'
        hash_key = 'guid'
        projection = ProjectAll()

    @adapted_dynamodel('sma')
    class TypeIndex(GlobalIndex):
        name = 'product-types-index'
        hash_key = 'type'
        range_key = 'guid'
        projection = ProjectAll()
