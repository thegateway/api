# -*- coding: utf-8 -*-
"""
TradelinkPro product model
"""
import logging
from decimal import Decimal
from enum import Enum

import marshmallow
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import (
    fields,
    post_dump,
)

from gwio.ext.dynamorm import (
    GwioModelField,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.ext.vat import VatPrice
from gwio.models.pricing import Pricing
from gwio.models.products.base import (
    BaseProduct,
    _BaseProductGetSchema,
    _BaseProductPostSchema,
    _BaseProductPutSchema,
)
from gwio.utils.uuid import get_namespace

log = logging.getLogger(__name__)


class Category(Enum):
    EXISTING = 'existing'
    LED = 'led'


class Version(Enum):
    STANDARD = 'standard'
    EMERGENCY = 'emergency'


class _TlpProductBaseSchema(BaseProduct.Schemas._BaseSchema):
    """Schema for representing the product information"""

    # Common attributes
    category = GwioEnumField(enum=Category, by_value=True, allow_none=True)
    size = fields.String(allow_none=True)
    technology = fields.String(allow_none=True)
    version = GwioEnumField(enum=Version, allow_none=True)
    watts = fields.Decimal(allow_none=True)
    light_type = fields.String(allow_none=True)

    # Fields specific for product category 'existing'
    adapter = fields.String(allow_none=True)
    replacement = fields.String(allow_none=True)
    replacement_guid = fields.UUID(allow_none=True)

    # Fields specific for product category 'led'
    code = fields.String(allow_none=True)
    colour_temperature = fields.String(allow_none=True)
    motion_sensor = fields.String(allow_none=True)

    # Override `base_price` to make it optional for existing lighting products
    base_price = GwioModelField(VatPrice, allow_none=True, unknown=marshmallow.EXCLUDE)

    # pylint: disable=no-self-use
    @post_dump
    def remove_nones(self, dump, **kwargs):
        none_fields = dict()
        try:
            if dump['category'] == Category.LED.value:
                none_fields = ['adapter', 'replacement', 'replacement_guid']
            elif dump['category'] == Category.EXISTING.value:
                none_fields = ['code', 'colour_temperature', 'motion_sensor']
            for field in none_fields:
                dump.pop(field, None)
        except KeyError:
            # Delivery methods will be dumped using this schema too and will not have these fields
            pass
        return dump


class _TlpProductGet(_TlpProductBaseSchema, _BaseProductGetSchema, GwioSchema):
    # TODO: this is not an attribute or property for the product, so it's always None? Is this needed?
    brand_owner = fields.UUID(allow_none=True)


# pylint: disable=too-few-public-methods, too-many-ancestors
class _TlpProductPost(_TlpProductGet, _BaseProductPostSchema):
    # Override `base_price` to make it not required for existing lighting products
    base_price = fields.Nested(VatPrice.Schema, unknown=marshmallow.EXCLUDE)


# pylint: disable=too-few-public-methods
class _TlpProductPut(_TlpProductPost, _BaseProductPutSchema):
    class Meta(_BaseProductPutSchema.Meta):
        exclude = list(set(_BaseProductPutSchema.Meta.exclude + ['code']))


class _TlpEvaluatedProductSchema(_TlpProductGet, BaseProduct.API.Assessor.EvaluatedProduct.Schemas.WebshopGet):
    pass


# pylint: disable=too-few-public-methods
@adapted_dynamodel('tradelinkpro')
class Product(BaseProduct, DynaModel):
    """
    Generic product model for dynamo DB
    """

    class Table(BaseProduct.Table):
        name = BaseProduct.Table.name
        hash_key = BaseProduct.Table.hash_key
        range_key = BaseProduct.Table.range_key
        version = 3

    class Schemas:
        _BaseSchema = _TlpProductBaseSchema
        Get = _TlpProductGet
        Post = _TlpProductPost
        Put = _TlpProductPut

        class Elasticsearch(_TlpProductGet):
            # pylint: disable=too-few-public-methods
            class Meta:
                exclude = ['base_price', 'base_price_type']

    Schema = Schemas._BaseSchema

    class API(BaseProduct.API):
        namespace = get_namespace('product')

        class Assessor(BaseProduct.API.Assessor):
            # pylint: disable=unused-argument
            @classmethod
            def evaluate(cls,
                         product,
                         *,
                         qty: Decimal = None,
                         pricing: Pricing = None,
                         schema: marshmallow.Schema = None,
                         evaluate_price: bool = True) -> 'EvaluatedProduct':
                if product.category is Category.EXISTING:
                    return super().evaluate(product, qty=qty, pricing=pricing, schema=Product.Schemas.WebshopGet(), evaluate_price=False)
                return super().evaluate(product, qty=qty, pricing=pricing, schema=Product.Schemas.WebshopGet())

            class EvaluatedProduct(BaseProduct.API.Assessor.EvaluatedProduct):
                class Schemas(BaseProduct.API.Assessor.EvaluatedProduct.Schemas):
                    WebshopGet = _TlpEvaluatedProductSchema

    @adapted_dynamodel('tradelinkpro')
    class TypeIndex(GlobalIndex):
        name = 'product-types-index'
        hash_key = 'type'
        range_key = 'guid'
        projection = ProjectAll()
