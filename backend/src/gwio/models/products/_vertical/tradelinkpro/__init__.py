# -*- coding: utf-8 -*-
"""
Import TradelinkPro Product
"""
from . import product

# pyflakes issue, this will prevent the complaint of import unused
assert product
