# -*- coding: utf-8 -*-
"""
imeds product definition
"""

from decimal import Decimal
from typing import (
    Any,
    Union,
)

import marshmallow
from dynamorm import (
    GlobalIndex,
    ProjectAll,
    ProjectKeys,
    DynaModel,
)
from marshmallow import (
    fields,
)

from gwio.utils.uuid import get_namespace
from gwio.ext.dynamorm import adapted_dynamodel
from gwio.ext.marshmallow import ISODateField
from gwio.ext.vat import (
    Price,
    Vat,
    VatPrice,
)
from gwio.models.pricing import (
    PriceType,
    Pricing,
    PricingError,
)
from gwio.models.products import base


class ImedsProduct:
    code = fields.String(default='00000')
    substitution_group = fields.String(default='00000')
    valid_until = ISODateField(allow_none=True)


# pylint: disable=too-few-public-methods
@adapted_dynamodel('imeds')
class Product(base.BaseProduct, DynaModel):
    class Schemas:
        class _BaseSchema(ImedsProduct, base.BaseProduct.Schemas._BaseSchema):
            code = fields.String(default='00000')
            substitution_group = fields.String(default='00000')
            valid_until = ISODateField(allow_none=True)

        class Get(ImedsProduct, base.BaseProduct.Schemas.Get):
            pass

        class Post(ImedsProduct, base.BaseProduct.Schemas.Post):
            pass

        class Put(ImedsProduct, base.BaseProduct.Schemas.Put):
            class Meta:
                exclude = list(set(base.BaseProduct.Schemas.Put.Meta.exclude + ['code']))

    Schema = Schemas._BaseSchema

    class API(base.BaseProduct.API):
        namespace = get_namespace('product')

        searchable_types = ('default', 'medicine', 'prescription_medicine')

        # TODO: XXX: This is *IGNORED* due to it not (possibly - there was an attempt) being compatible with the current base product
        class IgnoredAssessor(base.BaseProduct.API.Assessor):
            medicine_pricing = {
                # Valtioneuvoston asetus lääketaksasta 17.10.2013/713
                #
                # 3 § Reseptilääkkeen hinta
                # Myytäessä apteekista itsehoitolääkkeitä tulee niiden vähittäismyyntihintana käyttää
                # seuraavan laskentakaavan mukaan määräytyvää hintaa:
                #
                # Ostohinta, euroa  Vähittäismyyntihinta
                # 0 – 9,25          1,45 x ostohinta
                # 9,26 – 46,25      1,35 x ostohinta + 0,92 €
                # 46,26 – 100,9     1,25 x ostohinta + 5,54 €
                # 100,92 – 420,47   1,15 x ostohinta + 15,63 €
                # yli 420,47        1,1 x ostohinta + 36,65 €
                'prescription_medicine': [
                    (Decimal('420.48'), Decimal('1.1'), Decimal('36.65')),
                    (Decimal('100.92'), Decimal('1.15'), Decimal('15.63')),
                    (Decimal('46.26'), Decimal('1.25'), Decimal('5.54')),
                    (Decimal('9.26'), Decimal('1.35'), Decimal('0.92')),
                    (Decimal(0), Decimal('1.45'), Decimal('0')),
                ],
                # 4 § Itsehoitolääkkeen hinta
                # Myytäessä apteekista itsehoitolääkkeitä tulee niiden vähittäismyyntihintana
                # käyttää seuraavan laskentakaavan mukaan määräytyvää hintaa:
                #
                # Ostohinta, euroa  Vähittäismyyntihinta
                # 0 – 9,25          1,5 x ostohinta + 0,50 €
                # 9,26 – 46,25      1,4 x ostohinta + 1,43 €
                # 46,26 – 100,91    1,3 x ostohinta + 6,05 €
                # 100,92 – 420,47   1,2 x ostohinta + 16,15 €
                # yli 420,47        1,125 x ostohinta +  47,68 €
                'medicine': [
                    (Decimal('420.48'), Decimal('1.125'), Decimal('47.68')),
                    (Decimal('100.92'), Decimal('1.2'), Decimal('16.15')),
                    (Decimal('46.26'), Decimal('1.3'), Decimal('6.05')),
                    (Decimal('9.26'), Decimal('1.4'), Decimal('1.43')),
                    (Decimal(0), Decimal('1.5'), Decimal('.5')),
                ],
            }

            class MedicinePricing:
                def __init__(self, levels):
                    self.levels = levels

                def apply(self, price: Union[VatPrice, Price], price_type: PriceType) -> Price:
                    assert price_type  # ignored, but silence linter
                    amount = (price @ Vat(0)).without_vat
                    for cutoff, multiplier, fee in self.levels:
                        if cutoff <= amount.amount:
                            return amount * multiplier + fee

                    raise PricingError('Invalid medicine pricing')  # Negative base price

            @classmethod
            def evaluate(cls,
                         product,
                         *,
                         qty: Decimal = None,
                         pricing: Union[Pricing, MedicinePricing] = None,
                         schema: marshmallow.Schema = None,
                         evaluate_price: bool = False) -> Any:
                qty = Decimal(qty) if qty is not None else Decimal(1)
                try:
                    pricing = cls.MedicinePricing(cls.medicine_pricing[product.type])
                except KeyError:
                    pass
                return super().evaluate(product, qty=qty, pricing=pricing)

    class Table:
        name = base.BaseProduct.Table.name
        hash_key = base.BaseProduct.Table.hash_key
        range_key = base.BaseProduct.Table.range_key
        version = 2

    @adapted_dynamodel('imeds')
    class TypeIndex(GlobalIndex):
        name = 'product-types-index'
        hash_key = 'type'
        range_key = 'guid'
        projection = ProjectAll()

    # pylint: disable=too-few-public-methods
    @adapted_dynamodel('imeds')
    class SubstitutionIndex(GlobalIndex):
        name = 'substitution-index'
        hash_key = 'substitution_group'
        range_key = 'guid'
        projection = ProjectKeys()
