# -*- coding: utf-8 -*-
"""
Remppa product model
"""
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)

from gwio.ext.dynamorm import adapted_dynamodel
from gwio.models.products.base import BaseProduct


# pylint: disable=too-few-public-methods
@adapted_dynamodel('remppa')
class Product(BaseProduct, DynaModel):
    class Table(BaseProduct.Table):
        name = BaseProduct.Table.name
        hash_key = BaseProduct.Table.hash_key
        range_key = BaseProduct.Table.range_key
        version = 3

    Schema = BaseProduct.Schema

    class API(BaseProduct.API):
        searchable_types = ('default', 'service')

    @adapted_dynamodel('remppa')
    class TypeIndex(GlobalIndex):
        name = 'product-types-index'
        hash_key = 'type'
        range_key = 'guid'
        projection = ProjectAll()

    @adapted_dynamodel('remppa')
    class GuidIndex(GlobalIndex):
        name = 'product-guid-index'
        hash_key = 'guid'
        projection = ProjectAll()
