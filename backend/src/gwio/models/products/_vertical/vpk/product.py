# -*- coding: utf-8 -*-
"""
VPK product model
"""
import logging

from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from marshmallow import fields

from gwio.utils.uuid import get_namespace
from gwio.ext.dynamorm import adapted_dynamodel
from gwio.ext.marshmallow import GwioSchema
from gwio.models.products.base import (
    BaseProduct,
    _BaseProductGetSchema,
    _BaseProductPostSchema,
    _BaseProductPutSchema,
)

log = logging.getLogger(__name__)


class _VpkProductBaseSchema(BaseProduct.Schemas._BaseSchema):
    """Schema for representing the product information"""

    code = fields.String(allow_none=True)
    subtitle = fields.String(allow_none=True)
    ean_code = fields.String(allow_none=True)
    width = fields.Integer(allow_none=True)
    length = fields.Integer(allow_none=True)
    height = fields.Integer(allow_none=True)
    weight = fields.Decimal(allow_none=True)
    volume = fields.Decimal(allow_none=True)
    cn_code = fields.String(allow_none=True)


class _VpkProductGetSchema(_VpkProductBaseSchema, _BaseProductGetSchema, GwioSchema):
    class Meta:
        # TODO: I was just trying to avoid a change here during refactoring pynamodb away, but this doesn't make any sense
        exclude = ['code']


# pylint: disable=too-few-public-methods
class _VpkProductPostSchema(_BaseProductPostSchema):
    class Meta:
        exclude = ('guid',)


# pylint: disable=too-few-public-methods
class _VpkProductPutSchema(_BaseProductPutSchema):
    class Meta:
        exclude = ('name', 'guid')

    # Let's not require new name on update
    name = fields.String()


# pylint: disable=too-few-public-methods
class OrderLineSchema(_VpkProductGetSchema):
    class Meta:
        # TODO: Base price should be hidden from the end user - ie. removed from here
        exclude = ('cn_code', 'desc', 'height', 'image', 'length', 'subtitle', 'tags', 'guid', 'volume', 'weight', 'width')

    product_guid = fields.UUID(attribute="guid")
    quantity = fields.Decimal(attribute='qty')


# pylint: disable=too-few-public-methods, too-many-ancestors
class ProductLineSchema(OrderLineSchema):
    class Meta:
        exclude = OrderLineSchema.Meta.exclude + ('product_guid',)

    guid = fields.UUID(attribute="guid")
    quantity = fields.Decimal()


@adapted_dynamodel('vpk')
class Product(BaseProduct, DynaModel):
    """
    Generic product model for dynamo DB
    """

    class Table(BaseProduct.Table):
        name = BaseProduct.Table.name
        hash_key = BaseProduct.Table.hash_key
        range_key = BaseProduct.Table.range_key
        version = 2

    class Schemas:
        _BaseSchema = _VpkProductBaseSchema
        Get = _VpkProductGetSchema
        Post = _VpkProductPostSchema
        Put = _VpkProductPutSchema

    Schema = Schemas._BaseSchema

    class API(BaseProduct.API):
        orderline = OrderLineSchema
        product_line = ProductLineSchema
        assessor = None  # back-filled

        namespace = get_namespace('product')

    @adapted_dynamodel('vpk')
    class TypeIndex(GlobalIndex):
        name = 'product-types-index'
        hash_key = 'type'
        range_key = 'guid'
        projection = ProjectAll()
