# -*- coding: utf-8 -*-
"""
Implements vertical specific product models
"""
import logging

from gwio.ext.dynamorm import use_ddb
from . import _vertical

assert _vertical

log = logging.getLogger(__name__)

# Setup the Models for the current vertical
try:
    Product = use_ddb(model='Product')
except ImportError:
    log.critical('Importing Product failed')
    Product = None
except (KeyError, ValueError):
    Product = None
