# coding=utf-8
"""
BaseProduct which all vertical specific products should inherit

Special products APIs (like delivery methods and fees) actually
use this model even though they import the vertical specific product
"""
import datetime
import logging
import uuid
from decimal import Decimal
from functools import (
    partial,
    reduce,
)
from typing import (
    List,
    Optional,
)
from uuid import UUID

import marshmallow
import pytz
from attrdict import AttrDict
from marshmallow import (
    fields,
    post_load,
    pre_dump,
    pre_load,
)

from gwio.core.index_es_document import (
    delete_product_es,
    update_product_es,
)
from gwio.environment import Env
from gwio.ext import SYSTEM_GUID
from gwio.ext.dynamorm import (
    GwioListField,
    GwioModelField,
    HotfixMixIn,
    JSONField,
    NestedSchemaModel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.ext.vat import (
    Currency,
    Price,
    VatField,
    VatPrice,
)
from gwio.models import (
    modifiers,
    tags,
)
from gwio.models.pricing import (
    PriceType,
    Pricing,
)
from gwio.models.residence import GWIODateTime
from gwio.utils.uuid import (
    get_namespace,
)

log = logging.getLogger(__name__)

PRODUCT_TYPE_DEFAULT = 'default'
PRODUCT_TYPE_DELIVERY_METHOD = 'delivery_method'


def normalise_product_data_variants_fields_name(product: dict):
    try:
        product['data']['variants'] = {k.lower().strip(): v for k, v in product['data']['variants'].items()}
    except (KeyError, TypeError):
        pass
    return product


class TimestampsSchema(GwioSchema):
    created = GWIODateTime(missing=partial(datetime.datetime.now, tz=pytz.UTC))


class ProductTag(NestedSchemaModel):
    class Schema(tags.Tag.Schemas.Nested):
        @pre_load
        def remove_deprecated_field(self, values, **_):
            # TODO: Remove this after products have been updated
            try:
                del values['parent_guid']
            except KeyError:
                pass
            return values

    class Schemas:
        Get = tags.Tag.Schemas.Nested

    @property
    def tag(self):
        return tags.Tag.by_guid(self.guid)


class SizeSchema(NestedSchemaModel):
    class Schema(GwioSchema):
        width = fields.Decimal()
        height = fields.Decimal()
        length = fields.Decimal()


class Timestamps(NestedSchemaModel):
    Schema = TimestampsSchema


class _BaseProductBaseSchema:
    guid = fields.UUID(allow_none=False, missing=uuid.uuid4)
    sku = fields.String(required=False, allow_none=True)
    name = fields.String(required=True, allow_none=False)
    type = fields.String(missing='default')
    brief = fields.String(allow_none=True)
    desc = fields.String(allow_none=True)
    images = fields.List(fields.Url(), allow_none=True)
    data = JSONField(allow_none=True)
    pricing = fields.UUID(allow_none=True)
    vat = VatField(required=True, allow_none=False)
    activated = GWIODateTime(allow_none=True)
    owner_guid = fields.UUID(missing=SYSTEM_GUID)
    created = GWIODateTime(missing=partial(datetime.datetime.now, tz=pytz.UTC))
    timestamps = GwioModelField(Timestamps, load=True)
    # Base price indicates either the recommended retail price or the purchase price of the product.
    # The selling price will be evaluated based on the base_price
    base_price_type = GwioEnumField(enum=PriceType, by_value=True, required=True, allow_none=False)
    # This is for GUI purposes to make it easier to format and handle
    # e.g. the actual field _base_price is split into three separate
    # fields and the _base_price field is not returned at all
    # unknown=EXCLUDE needed since Nested schemas don't inherit parent schemas' unknown.
    # Used for schema.load in BaseProduct.API.Assessor.EvaluatedProduct.evaluate
    base_price = GwioModelField(VatPrice, required=True, allow_none=False, unknown=marshmallow.EXCLUDE)
    cost_price = GwioModelField(VatPrice, unknown=marshmallow.EXCLUDE)
    size = GwioModelField(SizeSchema, allow_none=True)
    weight = fields.Decimal(allow_none=True)

    tags = GwioListField(ProductTag)
    tag_guids = fields.List(fields.UUID(), missing=list)  # deprecated

    # Need to normalise data.variants fields, as those use in ES for searching products.
    # And in ES field name is case sensitive, so 'size' and 'Size' correspond two different fields.
    @post_load()
    def normalise_product_data_variants_in_load(self, product, **kwargs):
        return normalise_product_data_variants_fields_name(product)

    @pre_dump()
    def normalise_product_data_variants_in_dump(self, product, **kwargs):
        return normalise_product_data_variants_fields_name(product)


class _BaseProductGetSchema(_BaseProductBaseSchema):
    tags = fields.Nested(tags.Tag.Schemas.Nested, many=True)

    data = fields.Dict(allow_none=True)

    base_price = fields.Nested(VatPrice.Schemas.Get, required=True, allow_none=False, unknown=marshmallow.EXCLUDE)
    cost_price = fields.Nested(VatPrice.Schemas.Get, unknown=marshmallow.EXCLUDE)


class _ActivatedInputMixIn:
    activated = fields.Boolean(required=False, allow_none=True)

    @post_load
    def convert_activated_datetime(self, values, **kwargs):
        try:
            values['activated'] = datetime.datetime.now(tz=pytz.UTC) if values['activated'] else None
        except KeyError:
            pass

        return values


class _BaseProductPostSchema(_ActivatedInputMixIn, _BaseProductGetSchema):
    # pylint: disable=too-few-public-methods
    class Meta:
        exclude = ['created', 'guid', 'owner_guid', 'tags', 'timestamps']

    base_price = fields.Nested(VatPrice.Schemas.Post, required=True, allow_none=False, unknown=marshmallow.EXCLUDE)
    cost_price = fields.Nested(VatPrice.Schemas.Post, required=True, allow_none=False, unknown=marshmallow.EXCLUDE)
    # TODO: Later this should be removed and make cost_price required in Baseschema, once we don't have products that are missing cost price anymore


class _BaseProductPutSchema(_BaseProductPostSchema):
    # pylint: disable=too-few-public-methods
    class Meta(_BaseProductPostSchema.Meta):
        exclude = list(set(_BaseProductPostSchema.Meta.exclude + ['type']))


class _BasePriceUnitTotalSchema(GwioSchema):
    total = GwioModelField(VatPrice, required=True, allow_none=False)
    unit = GwioModelField(VatPrice, required=True, allow_none=False)


class _GetPriceUnitTotalSchema(GwioSchema):
    total = fields.Nested(VatPrice.Schemas.Get, required=True, allow_none=False)
    unit = fields.Nested(VatPrice.Schemas.Get, required=True, allow_none=False)


class _OriginalSchema(_BasePriceUnitTotalSchema):
    modifiers = GwioListField(modifiers.ModifierLine)


class OriginalPrice(NestedSchemaModel):
    class Schemas:
        class Get(_GetPriceUnitTotalSchema, _OriginalSchema):
            modifiers = fields.Nested(modifiers.ModifierLine.Schemas.Get, many=True)

    Schema = _OriginalSchema


class FinalPrice(NestedSchemaModel):
    class Schemas:
        class Get(_GetPriceUnitTotalSchema):
            pass

    Schema = _BasePriceUnitTotalSchema


class BasePriceSchema(GwioSchema):
    original = GwioModelField(OriginalPrice, load=True)
    final = GwioModelField(FinalPrice, load=True)
    currency = GwioEnumField(enum=Currency, by_value=True, required=True, allow_none=False)
    vat_class = fields.Decimal(required=True, allow_none=False)


class PriceModel(NestedSchemaModel):
    class Schemas:
        class Get(BasePriceSchema):
            original = fields.Nested(OriginalPrice.Schemas.Get, allow_none=False)
            final = fields.Nested(FinalPrice.Schemas.Get, allow_none=False)
            base = fields.Nested(_GetPriceUnitTotalSchema, required=True, allow_none=False, dump_only=True)
            cost = fields.Nested(_GetPriceUnitTotalSchema, required=True, allow_none=False, dump_only=True)

    Schema = BasePriceSchema


class _EvaluatedBaseProductSchema(_BaseProductBaseSchema):
    # pylint: disable=too-few-public-methods
    class Meta:
        exclude = ['base_price', 'cost_price']  # Included in `price.base`

    class EpPriceSchema(PriceModel.Schemas.Get):
        class Meta:
            dump_only = ['base.total', 'original.total', 'final.total', 'cost.total']
            exclude = ['base.total', 'original.total', 'final.total', 'cost.total']  # This is just to fix test that uses the schema.load()

        class EpOriginalSchema(_GetPriceUnitTotalSchema):
            campaign_modifier = fields.Nested(modifiers.GetModifierSchema, many=False, required=True, allow_none=True)

        original = fields.Nested(EpOriginalSchema, required=True, allow_none=False)

    price = fields.Nested(EpPriceSchema)
    updated = fields.String()
    suppliers = fields.List(fields.UUID())
    # The following item only for EvaluatedProducts
    stock_level = fields.Decimal(required=False)
    archived = GWIODateTime(allow_none=True)
    for_sale = fields.Boolean(allow_none=True)

    tags = fields.Nested(tags.Tag.Schemas.Get, many=True)


class _EvaluatedGetProductSchema(_EvaluatedBaseProductSchema):
    data = fields.Dict(allow_none=True)


class _EvaluatedGetProductSchemaForCustomer(_EvaluatedGetProductSchema):
    # pylint: disable=too-few-public-methods
    class Meta:
        # TODO: Should `archived` be added here? Should that be shown to the end customers? They can GET archived products by guid
        exclude = ['base_price', 'cost_price', 'base_price_type']  # Prices included in `price.base`

    price = fields.Nested(_EvaluatedBaseProductSchema.EpPriceSchema, exclude=('base', 'cost'))


class _EvaluatedGetProductSchemaForElasticsearch(_EvaluatedGetProductSchema):
    tags = fields.Nested(tags.Tag.Schemas.Get, many=True, only=['guid', 'type', 'name'])
    brand_name = fields.Str()


class _ProductLineBaseSchema(GwioSchema):
    """ Order product line schema """

    shop_guid = fields.UUID()
    guid = fields.UUID()
    type = fields.String()
    name = fields.String()
    images = fields.List(fields.Url(), missing=list)
    qty = fields.Decimal()
    backordered = fields.Boolean(allow_none=True, missing=False)
    size = GwioModelField(SizeSchema, allow_none=True)
    weight = fields.Decimal(allow_none=True)

    price = GwioModelField(PriceModel, load=True)


class _ProductLineGetSchema(_ProductLineBaseSchema):
    """ Order product line schema """

    class Meta:
        exclude = ['backordered']

    price = fields.Nested(PriceModel.Schemas.Get)


# pylint: disable=too-few-public-methods
class BaseProduct(HotfixMixIn):
    """
    Defines the base product for all verticals to use

    NOTE! due to the way we are handling vertical specific products, all indexes must be defined on the final Model inheriting from this
    """

    class Table:
        name = 'products'
        hash_key = 'owner_guid'
        range_key = 'guid'

    class Schemas:
        _BaseSchema = _BaseProductBaseSchema

        # The scemas are defined here like this because vertical specific products extend from the `_BaseProduct*Schema` instead of `BaseProduct.Schemas.*`
        # This is to avoid extending from two `Schema` classes
        class Get(_BaseProductGetSchema, GwioSchema):
            pass

        class Post(_BaseProductPostSchema, GwioSchema):
            """
            name: product
            description: the product to be created.
            """

        class Put(_BaseProductPutSchema, GwioSchema):
            """
            name: product
            description: the product being updated.
            """

        class Elasticsearch(_BaseProductGetSchema, GwioSchema):
            # pylint: disable=too-few-public-methods
            class Meta:
                exclude = ['base_price', 'base_price_type', 'cost_price']

            shop_guid = fields.UUID(default=None, allow_none=True)
            shop_name = fields.Str(default=None, allow_none=True)

        class DeliveryMethodGet(_BaseProductGetSchema, GwioSchema):
            class Meta:
                exclude = ['tags']

    Schema = Schemas._BaseSchema

    class API:
        namespace = get_namespace('product')

        PriceType = PriceType

        # The elastic search hook uses this to determine if the product
        # is to be included in the elastic search data set
        searchable_types = (PRODUCT_TYPE_DEFAULT,)

        # The tag products API uses this to check that the products are allowed to be added to campaigns
        valid_campaign_types = (PRODUCT_TYPE_DEFAULT, PRODUCT_TYPE_DELIVERY_METHOD)

        @staticmethod
        def process_kwargs(kwargs):
            # Handle the UI trickery (e.g. splitting of base_price into three fields)
            # Basically if we do not have base_price in the arguments assume this data
            # is coming from the UI. If we have the base_price and the three properties
            # the base_price is valid and ignore the properties
            split_fields = ['base_price_amount', 'base_price_currency', 'base_price_vat']
            if 'base_price' not in kwargs and any([x in kwargs for x in split_fields]):
                kwargs['base_price'] = VatPrice.from_price_and_vat(
                    Price(
                        kwargs.pop('base_price_amount', Decimal(0)),
                        currency=kwargs.pop('base_price_currency', Env.DEFAULT_CURRENCY)
                    ),
                    vat=kwargs.pop('base_price_vat', 0),
                )
            else:
                _ = [kwargs.pop(x, None) for x in split_fields]  # noqa: F841
            return kwargs

        class Assessor:
            # pylint: disable=too-many-ancestors,too-many-instance-attributes
            class EvaluatedProduct(AttrDict):
                class Schemas:
                    class WebshopGet(_EvaluatedGetProductSchema, GwioSchema):
                        pass

                    class CustomerGet(_EvaluatedGetProductSchemaForCustomer, GwioSchema):
                        pass

                    class Elasticsearch(_EvaluatedGetProductSchemaForElasticsearch, GwioSchema):
                        pass

                class ProductLine(NestedSchemaModel):
                    class Schemas:
                        BaseSchema = _ProductLineBaseSchema
                        Get = _ProductLineGetSchema

                    Schema = Schemas.BaseSchema

                    @classmethod
                    def mapped(cls, ep):
                        d = dict()
                        # pylint: disable=protected-access
                        for attr_name in cls.Schemas.BaseSchema._declared_fields.keys():
                            if attr_name == 'shop_guid':
                                value = ep.get('owner_guid')
                            else:
                                value = ep.get(attr_name)
                            if attr_name.endswith('guid') and value is not None:
                                if not isinstance(value, UUID):
                                    value = UUID(value)
                            if attr_name == 'vat_class' and value is not None:
                                if not isinstance(value, Decimal):
                                    value = Decimal(value)
                            d[attr_name] = value
                        d.get('price', dict()).pop('base', None)  # Base price is not saved for product lines
                        d.get('price', dict()).pop('cost', None)  # Cost price is not saved for product lines
                        # Product lines have list of modifiers instead of the campaign modifier
                        d['price']['original'].pop('campaign_modifier', None)
                        return cls(**d)

                @property
                def campaign_modifier(self) -> Optional[AttrDict]:
                    def better_tag(tag1, tag2):
                        # pylint: disable=no-member
                        return tag1 if tag1.data.calc_fixed_modifiers(self).unit < tag2.data.calc_fixed_modifiers(self).unit else tag2

                    try:
                        best_tag = reduce(better_tag, self.product.campaigns())
                        return best_tag.data.dump_with_fixed_modifiers(self)
                    except TypeError:
                        return None

            @classmethod
            def evaluate(cls,
                         product,
                         *,
                         qty: Decimal = None,
                         pricing: Pricing = None,
                         schema: marshmallow.Schema = None,
                         evaluate_price: bool = True) -> 'EvaluatedProduct':
                if qty is None:
                    qty = Decimal(1)

                try:
                    ep = cls.EvaluatedProduct(schema.load(schema.dump(product), unknown=marshmallow.EXCLUDE),
                                              qty=Decimal(qty), product=product)
                    ep.price = dict(base=dict(), original=dict(), final=dict())
                    if not evaluate_price:
                        return ep
                except AttributeError:
                    schema = BaseProduct.Schemas.Get(exclude=('tags.data',))
                    dumped_product = schema.dump(product)
                    loaded_product = schema.load(dumped_product, unknown=marshmallow.EXCLUDE)
                    ep = cls.EvaluatedProduct(loaded_product, qty=qty, product=product)

                try:
                    if pricing is None:
                        pricing = Pricing.get(
                            owner=SYSTEM_GUID,
                            guid=Pricing.API.generate_guid(name=product.type, owner=SYSTEM_GUID),
                        )
                except Pricing.DoesNotExist:
                    log.warning('No pricing defined for "%s"', product.type)
                    pricing = None
                except Exception as e:  # pylint: disable=broad-except
                    log.exception(e)
                    pricing = None

                # If we have no pricing defined, the unit price is just the base price in the vat of the product
                if pricing is None:
                    unit_price = product.base_price @ product.vat
                else:
                    unit_price = pricing.apply(product.base_price, product.base_price_type) @ product.vat

                total_price = unit_price * qty

                # pylint: disable=attribute-defined-outside-init
                ep.price = AttrDict(
                    base=AttrDict(
                        unit=product.base_price,
                        total=product.base_price * qty),
                    cost=AttrDict(
                        unit=product.cost_price,
                        total=product.cost_price * qty),
                    original=AttrDict(
                        unit=unit_price,
                        total=total_price),
                    final=AttrDict(  # Final is set here so that it exists when `campaign_modifier` property is called as it needs final price
                        unit=unit_price,
                        total=total_price),
                    vat_class=product.vat.category,
                    currency=total_price.currency)
                # Must set to ep before final price is updated, otherwise the fixed modifiers are calculated from final that they were already applied to
                campaign_modifier = ep.campaign_modifier
                ep['price']['original']['campaign_modifier'] = campaign_modifier
                try:
                    fixed_modifiers = campaign_modifier.fixed_modifiers
                    ep['price']['final']['unit'] += fixed_modifiers.unit
                    ep['price']['final']['total'] += fixed_modifiers.total
                except AttributeError:
                    pass

                ep.qty = Decimal(qty)
                return ep

    # Possible tags assigned to this product

    # UI has easier time if the base price is broken into separate fields

    @property
    def campaign_modifier(self) -> Optional[AttrDict]:
        return self.API.Assessor.evaluate(self).campaign_modifier

    def sync_es(self):
        add_product_to_elastic_search(self)

    @classmethod
    def by_guid(cls, guid: uuid.UUID) -> 'BaseProduct':
        try:
            # pylint: disable=no-member  # Should be implemented in the vertical product
            return next(cls.GuidIndex.query(guid=guid))
        except StopIteration:
            raise cls.DoesNotExist('Product does not exist')
        except AttributeError:
            raise Exception(f'{cls.__name__} has not implemented guid `index`')

    @classmethod
    def by_organization_product(cls, organization_guid, guid) -> 'BaseProduct':
        return cls.get(owner_guid=organization_guid, guid=guid)

    def campaigns(self, *, only_active: bool = True) -> List[tags.Tag]:
        campaigns = []
        # pylint: disable=no-value-for-parameter
        for tag in self.tags:
            if only_active and not tag.active:
                continue
            if tag.type == tags.Tag.API.Type.CAMPAIGN:
                campaigns.append(tag)

        return campaigns

    def add_tag(self, tag: 'tags.Tag'):
        """Add a product to this tag"""
        assert self.guid
        assert tag.guid
        self.remove_duplicate_tags()
        tag.remove_duplicate_products()

        if tag.guid not in self.tags:
            self.tags.append(tag)
        if self.guid not in tag.product_guids:
            tag.product_guids.append(self.guid)

    def remove_tag(self, tag: 'tags.Tag'):
        """Remove a product from this tag"""
        assert self.guid
        assert tag.guid
        self.remove_duplicate_tags()
        tag.remove_duplicate_products()

        try:
            for x in self.tags:
                if x.guid == tag.guid:
                    self.tags.remove(x)
        except ValueError:
            pass
        try:
            tag.product_guids.remove(self.guid)
        except ValueError:
            pass

    def remove_duplicate_tags(self):
        # TODO: Remove once `tags` is a set
        self.tags = list(set(self.tags))

    def delete(self):
        for x in list(self.tags):
            try:
                tag = tags.Tag.by_guid(x.guid)
                tag.remove_product(self)
                tag.save()
            except tags.Tag.DoesNotExist:
                # Don't have to care about `tags` attribute since we're deleting the record
                pass
        super().delete()


@BaseProduct.register_hook(hook='post-save', allow_fail=False)
@BaseProduct.register_hook(hook='post-update', allow_fail=False)
def add_product_to_elastic_search(product):
    update_product_es(product=product)


@BaseProduct.register_hook(hook='post-delete', allow_fail=False)
def delete_product_from_elastic_search(product):
    delete_product_es(product=product)
