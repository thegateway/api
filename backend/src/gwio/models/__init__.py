# coding=utf-8
"""
Models used by API
"""
from . import (
    bootstrap,
    brands,
    coupons,
    locations,
    orders,
    payment_method,
    pricing,
    products,
    residence,
    tags,
    user,
    webshop_product,
    webshops,
)

__all__ = [
    'brands',
    'bootstrap',
    'coupons',
    'locations',
    'orders',
    'payment_method',
    'products',
    'pricing',
    'residence',
    'webshops',
    'tags',
    'user',
    'webshop_product',
]
