from dynamorm import DynaModel
from marshmallow import (
    Schema,
    fields,
    ValidationError,
    pre_load,
)

from gwio import environment
from gwio.ext import dynamorm as ext_dynamorm
from gwio.ext.dynamorm import (
    GwioListField,
    HotfixMixIn,
    NestedSchemaModel,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models import webshops
from gwio.models.user import COUNTRY_CODES


def normalise_display_name_key(children):
    if children:
        for item in children:
            try:
                item['display_name'] = {k.lower().strip(): v for k, v in item['display_name'].items()}
            except (KeyError, AttributeError):
                pass
            if item['children']:
                normalise_display_name_key(item['children'])


def validate_language_code(ln_code):
    if ln_code not in COUNTRY_CODES:
        raise ValidationError(f'Provided language code ({ln_code}) is not valid')


class _BootstrapTagBaseSchema(GwioSchema):
    guid = fields.UUID()
    name = fields.String(allow_none=True)
    display_name = fields.Dict(keys=fields.Str(validate=validate_language_code), values=fields.Str(), allow_none=True)
    children = GwioListField(lambda: BootstrapTag, schema='_BootstrapTagBaseSchema')
    hidden = fields.Bool(missing=False, default=False)


class _BootstrapTagUpdateSchema(_BootstrapTagBaseSchema):
    class Meta:
        exclude = ['name']

    children = GwioListField(lambda: BootstrapTag, schema='_BootstrapTagUpdateSchema')


class BootstrapTag(NestedSchemaModel):
    Schema = _BootstrapTagBaseSchema

    class Schemas:
        Get = _BootstrapTagBaseSchema
        Put = _BootstrapTagUpdateSchema


@ext_dynamorm.adapted_dynamodel
class Bootstrap(HotfixMixIn, DynaModel):
    # pylint: disable=too-few-public-methods
    class Table:
        name = 'bootstrap'
        version = 1
        hash_key = 'vertical'

        # This is used by the auditing logic since the table doesn't have `guid`
        identifier = 'vertical'

    class Schemas:
        class BaseSchema:
            vertical = fields.Str(missing=environment.Env.VERTICAL)
            data = fields.Dict(default=dict, missing=dict, allow_none=True)
            tag_hierarchy = GwioListField(BootstrapTag)

        class Get(Schema, BaseSchema):
            shops = fields.Nested(webshops.Webshop.Schemas.Get, many=True)

        class Put(Schema, BaseSchema):
            """
            name: bootstrap data
            description: bootstrap data to be updated.
            """

            class Meta:
                exclude = ('vertical',)

            tag_hierarchy = fields.Nested(BootstrapTag.Schemas.Put, many=True)

            # pylint: disable=no-self-use
            @pre_load
            def normalise_display_name(self, values, **kwargs):
                normalise_display_name_key(values.get('tag_hierarchy'))
                return values

    Schema = Schemas.BaseSchema
