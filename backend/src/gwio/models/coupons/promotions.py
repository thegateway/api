import uuid

from dynamorm import DynaModel
from marshmallow import fields

from gwio.ext import dynamorm as ext_dynamorm
from gwio.ext.dynamorm import (
    GwioListField,
    GwioModelField,
    HotfixMixIn,
    NestedSchemaModel,
)
from gwio.ext.marshmallow import (
    GwioSchema,
)
from gwio.models import (
    modifiers,
)
from gwio.models.coupons import conditions


class _BaseCouponSchema(GwioSchema):
    guid = fields.UUID(missing=uuid.uuid4)
    name = fields.String(allow_none=True)
    images = fields.List(fields.String(), missing=list)
    benefit = GwioModelField(modifiers.Modifier)
    conditions = GwioListField(conditions.CouponCondition)
    used = fields.Boolean(allow_none=False, missing=False)


class Coupon(NestedSchemaModel):
    Schema = _BaseCouponSchema

    class Schemas:
        class Get(_BaseCouponSchema):
            """
            name: coupon
            description: a coupon
            """

    def check_conditions(self, order):
        """Checks all the conditions of the coupon against the order and returns True if coupon can be applied to the order"""
        for condition in self.conditions:
            if not condition.check(order):
                return False

        return True


class Benefit(NestedSchemaModel):
    Schema = modifiers.Modifier.Schema


class _PromotionBaseSchema:
    guid = fields.UUID(missing=uuid.uuid4)
    name = fields.String(allow_none=True)
    images = fields.List(fields.URL(), missing=list)
    benefit = GwioModelField(Benefit)
    conditions = GwioListField(conditions.CouponCondition)


@ext_dynamorm.adapted_dynamodel
class Promotion(HotfixMixIn, DynaModel):
    class Table:
        name = 'promotions'
        hash_key = 'guid'

    class Schemas:
        class Get(_PromotionBaseSchema, GwioSchema):
            """
            name: promotion
            description: a promotion.
            """

        class Post(Get):
            """
            name: promotion
            description: the promotion to be created.
            """

            class Meta:
                exclude = ('guid',)

        class Put(Post):
            """
            name: promotion
            description: the promotion to be updated.
            """

    Schema = _PromotionBaseSchema

    def check_order_validity(self, order):
        return True

    def generate_coupon_from_order(self, user):
        coupon_data = dict(
            benefit=self.benefit,
            conditions=self.conditions,
        )
        coupon = Coupon(**Coupon.Schema().dump(coupon_data))
        user.coupons.append(coupon)
        user.data.save()
        return coupon
