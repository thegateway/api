from enum import Enum

from gwio.ext.dynamorm import NestedSchemaModel
from gwio.ext.marshmallow import GwioSchema


class Type(Enum):
    NO_CONDITION = 'no_condition'


class CouponConditionSchema(GwioSchema):
    pass


class CouponCondition(NestedSchemaModel):
    Schema = CouponConditionSchema

    def check(self, order):
        """Should import dynamically the conditions logic from the `.conditions` based on the type of the condition"""
        raise NotImplementedError
