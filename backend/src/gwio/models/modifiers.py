# -*- encoding: utf-8 -*-
"""
Modifier / Campaing "model"

Product specific campaigns and order product line modifiers are basically the same.
"""
from decimal import Decimal
from enum import Enum

import marshmallow
from attrdict import AttrDict
from marshmallow import (
    fields,
)

from gwio.ext import vat
from gwio.ext.dynamorm import (
    GwioModelField,
    NestedSchemaModel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.ext.vat import VatPrice


class ModifierMethodType(Enum):
    FIXED_UNIT = 'fixed unit'
    FIXED_TOTAL = 'fixed total'
    PERCENTAGE = 'percentage'
    # These are not saved, but get converted to one of the methods above
    OVERRIDE_UNIT = 'override unit'
    OVERRIDE_TOTAL = 'override total'


_method_conversions = {
    ModifierMethodType.OVERRIDE_UNIT: ModifierMethodType.FIXED_UNIT,
    ModifierMethodType.OVERRIDE_TOTAL: ModifierMethodType.FIXED_TOTAL,
}


class ModifierLineSource(Enum):
    MANUAL = 'manual'
    CAMPAIGN = 'campaign'
    COUPON = 'coupon'


class _BaseModifierSchema(GwioSchema):
    desc = fields.String(required=True, allow_none=False)
    method = GwioEnumField(enum=ModifierMethodType, by_value=True, required=True)
    amount = fields.Decimal(required=True, as_string=True)


class UpdateModifierSchema(_BaseModifierSchema):
    """ Used for validating input on order price modifier update """


class _FixedModifierSchema(GwioSchema):
    total = fields.Nested(VatPrice.Schemas.Get)
    unit = fields.Nested(VatPrice.Schemas.Get)


class GetModifierSchema(UpdateModifierSchema):
    """
    name: modifier
    description: a modifier
    """
    fixed_modifiers = fields.Nested(_FixedModifierSchema, default=dict)


class ModifierLineUpdateSchema(GwioSchema):
    """
    name: update order line price modifiers
    description: update order product line price modifiers
    """
    modifiers = fields.Nested(UpdateModifierSchema, many=True)


class FixedModifiers(NestedSchemaModel):
    class Schema(GwioSchema):
        unit = GwioModelField(VatPrice)
        total = GwioModelField(VatPrice)

    class Schemas:
        Get = _FixedModifierSchema


class _BaseModifierLineSchema(GetModifierSchema):
    source = GwioEnumField(enum=ModifierLineSource, by_value=True)
    fixed_modifiers = GwioModelField(FixedModifiers)


# Lint gets very very confused by dynamorm init
# pylint: disable=access-member-before-definition
class Modifier(NestedSchemaModel):
    """ ModifierLineMap extends from this because the modifier values are copied directly to the modifier line """

    Schema = _BaseModifierSchema

    class Schemas:
        Get = GetModifierSchema
        Put = UpdateModifierSchema

    def calc_fixed_modifiers(self, evaluated_product):
        """ Calculates and sets the fixed modifiers of ModifierLineMap based on the non-fixed modifiers of ModifierMap"""

        def _calc_fixed_unit(modifier, ep):
            unit_modifier = vat.VatPrice.from_price_and_vat(vat.Price(modifier.amount, currency=ep.price.currency), vat=vat.Vat(ep.price.vat_class))
            return unit_modifier, unit_modifier * ep.qty

        def _calc_fixed_total(modifier, ep):
            total_modifier = vat.VatPrice.from_price_and_vat(vat.Price(modifier.amount, currency=ep.price.currency), vat=vat.Vat(ep.price.vat_class))
            return total_modifier / ep.qty, total_modifier

        def _calc_percent(modifier, ep):
            total_modifier = ep.price.final.total * (modifier.amount / Decimal(100))
            return total_modifier / ep.qty, total_modifier

        def _calc_override_unit(modifier, ep):
            price = vat.Price(modifier.amount, currency=ep.price.currency)
            unit_modifier = vat.VatPrice.from_price_and_vat(price, vat=vat.Vat(ep.price.vat_class)) - ep.price.final.unit
            self.amount = unit_modifier.amount
            return unit_modifier, unit_modifier * ep.qty

        def _calc_override_total(modifier, ep):
            price = vat.Price(modifier.amount, currency=ep.price.currency)
            total_modifier = vat.VatPrice.from_price_and_vat(price, vat=vat.Vat(ep.price.vat_class)) - ep.price.final.total
            self.amount = total_modifier.amount
            return total_modifier / ep.qty, total_modifier

        calc_fixed = {
            ModifierMethodType.FIXED_UNIT: _calc_fixed_unit,
            ModifierMethodType.FIXED_TOTAL: _calc_fixed_total,
            ModifierMethodType.PERCENTAGE: _calc_percent,
            ModifierMethodType.OVERRIDE_UNIT: _calc_override_unit,
            ModifierMethodType.OVERRIDE_TOTAL: _calc_override_total,
        }

        unit_price, total_price = calc_fixed[self.method](self, evaluated_product)
        try:
            self.method = _method_conversions[self.method]
        except KeyError:
            pass

        return AttrDict(
            unit=unit_price,
            total=total_price,
        )

    def dump_with_fixed_modifiers(self, evaluated_product):
        """

        :param evaluated_product: Product.API.Assessor.EvaluatedProduct
        :return:
        """
        schema = UpdateModifierSchema(unknown=marshmallow.EXCLUDE)
        return AttrDict(
            **schema.load(schema.dump(self)),
            fixed_modifiers=self.calc_fixed_modifiers(evaluated_product),
        )


class ModifierLine(Modifier):
    """ Map for order product line price modifiers """

    Schema = _BaseModifierLineSchema

    class Schemas:
        class Get(_BaseModifierLineSchema):
            """
            name: modifier line
            description: modifier line information
            """
            fixed_modifiers = fields.Nested(FixedModifiers.Schemas.Get)

    def calc_fixed_modifiers(self, evaluated_product):
        self.fixed_modifiers = super().calc_fixed_modifiers(evaluated_product)
        return self.fixed_modifiers
