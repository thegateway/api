import datetime

from gwio.ext.marshmallow import GwioSchema

try:
    import pandas as pd
except ModuleNotFoundError:
    pass

from marshmallow import (
    fields,
    pre_dump,
    validates,
    ValidationError,
)
from marshmallow.validate import Range

from gwio.models.residence import GWIODateTime


# This function takes a dataframe which index should be timestamp and calculate daily/weekly/monthly/yearly stat. Return a dataframe with all those stat
# take 4 other parameters 'agg_dict' (dict to pass in pandas agg() function), 'time_frame' (e.g 'daily', 'monthly' etc.), start_date and end_date.
def get_time_stat(df, agg_dict, time_frame, start_date, end_date):
    time_frame = time_frame.upper()[0]
    # This will generate stat upto dataframe's latest date not calender latest date (e.g. datetime.now())
    time_stat = df.resample(time_frame).agg(agg_dict)
    time_stat.index = time_stat.index.date
    if time_frame in 'DW':
        new_index = pd.date_range(start=start_date, end=end_date, freq=time_frame)
        # this will create a datetime index with latest calender dates and fill the dataframe with values base on previous dataframe dates.
        time_stat = time_stat.reindex(new_index, fill_value=0)
    time_stat = time_stat.round(2)
    # Convert time_stat dataframe index from datetime object to string, as timestamp is not json serializable
    time_stat.index = time_stat.index.astype(str)
    return time_stat


def modify_es_aggs_response(buckets: list):
    return {item['key_as_string']: item['doc_count'] for item in buckets}


class Statistics:
    class Schemas:
        class BasicStatistics(GwioSchema):
            """
            name: basic-statistics
            description: Statistic data represent basic statistics.
            """
            total_users_count = fields.Int(default=0)
            total_products_count = fields.Int(default=0)
            total_tags_count = fields.Int(default=0)
            total_categories_count = fields.Int(default=0)

        class OrderStatSchema(GwioSchema):
            """
            name: order-statistics
            description: Statistic data represent order statistics.
            """
            since = GWIODateTime(default=None)
            total_sale = fields.Decimal(default=0)
            order_count = fields.Int(default=0)
            delivery_count = fields.Dict(default=dict)
            payment_count = fields.Dict(default=dict)
            daily_stat = fields.Dict(default=dict)
            last_seven_days = fields.Dict(default=dict)
            weekly_stat = fields.Dict(default=dict)
            monthly_stat = fields.Dict(default=dict)
            yearly_stat = fields.Dict(default=dict)
            delivery_method_stat = fields.Dict(default=dict)
            payment_method_stat = fields.Dict(default=dict)

            @pre_dump
            def process_order_stat(self, data, **kwargs):
                """
                This function take a dataframe and extract necessary statistics data and eventually convert those data into a dict for serialisation
                """
                order_stat = dict()
                order_df = data['df']
                if order_df.empty:
                    return order_stat
                try:
                    # Calculating all the stats
                    order_stat['order_count'] = int(order_df['object_id'].count())
                    order_stat['since'] = data['start_date']
                    order_stat['delivery_count'] = order_df['delivery_method_name'].value_counts().to_dict()
                    order_stat['payment_count'] = order_df['payment_method_type'].value_counts().to_dict()
                    order_stat['total_sale'] = order_df['sale'].sum().round(2)

                    # Keeping only those columns, which needs to calculate time series stats
                    time_stat_df = order_df[
                        ['object_id', 'sale', 'timestamp', 'delivery_method_name', 'payment_method_type']
                    ].rename(
                        columns={"object_id": "order_count"}
                    )

                    # Set timestamp column as index
                    time_stat_df.set_index('timestamp', inplace=True)

                    # Create new columns base on delivery_method_name and payment_method_type's unique values.
                    # New columns will help to create time series stat for delivery_methods and payment_type
                    delivery_methods = time_stat_df['delivery_method_name'].unique().tolist()
                    payment_methods = time_stat_df['payment_method_type'].unique().tolist()
                    delivery_method_agg_dict = dict()
                    payment_method_agg_dict = dict()
                    for delivery_method in delivery_methods:
                        time_stat_df[delivery_method] = pd.np.where(time_stat_df['delivery_method_name'] == delivery_method, 1, 0)
                        delivery_method_agg_dict[delivery_method] = 'sum'

                    for payment_method in payment_methods:
                        time_stat_df[payment_method] = pd.np.where(time_stat_df['payment_method_type'] == payment_method, 1, 0)
                        payment_method_agg_dict[payment_method] = 'sum'

                    delivery_method_stat = dict()
                    payment_method_stat = dict()

                    # Here dict keys are column name and values are mathematical function to use for aggregating that column data. Need for 'get_time_stat()'
                    order_agg_dict = {'order_count': 'count', 'sale': 'sum'}
                    for timeframe in ('daily', 'weekly', 'monthly', 'yearly'):
                        order_stat[f'{timeframe}_stat'] = get_time_stat(
                            df=time_stat_df,
                            agg_dict=order_agg_dict,
                            time_frame=timeframe,
                            start_date=data['start_date'],
                            end_date=data['end_date']
                        ).T.to_dict()
                        delivery_method_stat[f'delivery_method_{timeframe}_stat'] = get_time_stat(
                            df=time_stat_df,
                            agg_dict=delivery_method_agg_dict,
                            time_frame=timeframe,
                            start_date=data['start_date'],
                            end_date=data['end_date']
                        ).T.to_dict()
                        payment_method_stat[f'payment_method_{timeframe}_stat'] = get_time_stat(
                            df=time_stat_df,
                            agg_dict=payment_method_agg_dict,
                            time_frame=timeframe,
                            start_date=data['start_date'],
                            end_date=data['end_date']
                        ).T.to_dict()
                    order_stat['last_seven_days'] = get_time_stat(
                        df=time_stat_df,
                        agg_dict=order_agg_dict,
                        time_frame='D',
                        start_date=data['start_date'],
                        end_date=data['end_date']
                    ).tail(7).T.to_dict()
                    order_stat['delivery_method_stat'] = delivery_method_stat
                    order_stat['payment_method_stat'] = payment_method_stat
                except (AttributeError, KeyError, TypeError):
                    # AttributeError, KeyError happen, when trying to access empty dataframe's column value
                    # And TypeError happen when trying to extract time series value against a empty dataframe
                    pass
                return order_stat

        class FilterParams(GwioSchema):
            """
            name: Filter order statistics
            description: query parameters for filtering order statistics by delivery method and payment method.
            """
            delivery_method_guid = fields.UUID(allow_none=True)
            payment_method_guid = fields.UUID(allow_none=True)
            start_date = fields.Date(format='%Y-%m-%d', allow_none=True)
            end_date = fields.Date(format='%Y-%m-%d', allow_none=True)

            @validates('start_date')
            def min_date(self, value):
                """'value' is the date parsed from start_date by marshmallow"""
                min_searcheble_date = datetime.date(2019, 6, 1)
                if value < min_searcheble_date:
                    raise ValidationError("Cannot search statistics beyond 2019-06-01!")

            @validates('end_date')
            def max_date(self, value):
                """'value' is the date parsed from start_date by marshmallow"""
                today = datetime.date.today()
                if value > today:
                    raise ValidationError("Cannot search future statistics!")

        class TopProductsSchema(GwioSchema):
            """
            name: Top sold products
            description: Statistic data represent top sold products.
            """
            top_sold_products = fields.List(fields.Dict(), default=list)

            @pre_dump
            def process_top_products(self, data, **kwargs):
                """
                This function take a dataframe and extract necessary statistics data and eventually convert those data into a dict for serialisation
                """
                top_sold_products = []
                top_products_df = data
                if top_products_df.empty:
                    return top_sold_products
                try:
                    # Rename column to convert dataframe to a presentable list with dictionaries
                    top_products_df = top_products_df.rename(columns={"index": "product_guid", "product_guids": "count"})
                    top_sold_products = top_products_df.sort_values(by=['count', 'product_guid'], ascending=False).to_dict('records')
                except (AttributeError, KeyError):
                    # AttributeError, KeyError happen, when trying to access empty dataframe's column value
                    pass
                return dict(top_sold_products=top_sold_products)

        class TopProductsQuery(FilterParams, GwioSchema):
            """
            name: Top sold products query
            description: query parameters for listing top sold products.
            """

            class Meta:
                exlude = ['delivery_method_guid', 'payment_method_guid']

            count = fields.Int(allow_none=True, validate=Range(min=1, error="Value must be greater than 0"))

        class ProductStatisticsResultSize(GwioSchema):
            """
            name: Result size for product statistics
            description: query parameters for product statistics result size, How many webshop's product stats should return
            """

            size = fields.Int(missing=100, validate=Range(min=1, error="Value must be greater than 0"))

        class OrderedProductStatSchema(GwioSchema):
            """
            name: Ordered product statistics
            description: Statistic data represent product statistics.
            """
            daily_sold = fields.Dict(default=dict)
            sold_in_last_seven_days = fields.Dict(default=dict)
            weekly_sold = fields.Dict(default=dict)
            monthly_sold = fields.Dict(default=dict)
            yearly_sold = fields.Dict(default=dict)

            @pre_dump
            def process_ordered_product_stat(self, data, **kwargs):
                """
                This function take a dataframe and extract necessary statistics data and eventually convert those data into a dict for serialisation
                """
                product_stat = dict()
                product_df = data['df']
                if product_df.empty:
                    return product_df
                try:
                    product_df = product_df.rename(columns={"product_guids": "product_sold"})
                    agg_dict = {'product_sold': 'count'}
                    for timeframe in ('daily', 'weekly', 'monthly', 'yearly'):
                        product_stat[f'{timeframe}_sold'] = get_time_stat(
                            df=product_df,
                            agg_dict=agg_dict,
                            time_frame=timeframe,
                            start_date=data['start_date'],
                            end_date=data['end_date']
                        ).T.to_dict()
                    product_stat['sold_in_last_seven_days'] = get_time_stat(
                        df=product_df,
                        agg_dict=agg_dict,
                        time_frame='D',
                        start_date=data['start_date'],
                        end_date=data['end_date']
                    ).tail(7).T.to_dict()
                except (AttributeError, KeyError, TypeError):
                    # AttributeError, KeyError happen, when trying to access empty dataframe's column value
                    # And TypeError happen when trying to extract time series value against a empty dataframe
                    pass

                return product_stat

        class ProductStatSchema(GwioSchema):
            """
            name: product statistics
            description: Statistic data represent product statistics.
            """
            total_products_count = fields.Int(default=0)
            active_products_count = fields.Int(default=0)
            inactive_products_count = fields.Int(default=0)
            for_sale_products_count = fields.Int(default=0)
            webshops_product_stats = fields.List(fields.Dict(), default=list)

            @pre_dump
            def process_product_stat(self, data, **kwargs):
                """
                This function take a ES aggregated dict and extract necessary statistics data
                """
                product_stat = dict()
                try:
                    # Calculating all the stats
                    product_stat['total_products_count'] = data['product_count']['value']
                    product_stat['active_products_count'] = data['active_product_count']['value']
                    product_stat['inactive_products_count'] = data['product_count']['value'] - data['active_product_count']['value']
                    product_stat['for_sale_products_count'] = data['for_sale_product_count']['value']
                except (AttributeError, KeyError):
                    pass
                try:
                    product_stat['webshops_product_stats'] = [
                        dict(
                            webshop_guid=item['key'],
                            total_products_count=item['product_count']['value'],
                            active_products_count=item['active_product_count']['value'],
                            inactive_products_count=item['product_count']['value'] - item['active_product_count']['value'],
                            for_sale_products_count=item['for_sale_product_count']['value'],
                        ) for item in data['webshops_product_stats']['buckets']
                    ]
                except KeyError:
                    pass

                return product_stat

        class WebshopProductStatSchema(ProductStatSchema, GwioSchema):
            """
            name: webshop's product statistics
            description: Statistic data represent webshop's product statistics.
            """

            class Meta:
                exclude = ['webshops_product_stats']

        class ProductTimeStatSchema(GwioSchema):
            """
            name: Added product statistics over time
            description: Statistic data represent added product statistics.
            """
            daily_added_products = fields.Dict(default=dict)
            weekly_added_products = fields.Dict(default=dict)
            monthly_added_products = fields.Dict(default=dict)
            yearly_added_products = fields.Dict(default=dict)

            @pre_dump
            def process_product_time_series_stat(self, data, **kwargs):
                """
                This function modify elasticsearch time aggregation response and represent them better way.
                """
                product_stat = dict()
                try:
                    product_stat['daily_added_products'] = modify_es_aggs_response(data['daily_added_products']['buckets'])
                    product_stat['weekly_added_products'] = modify_es_aggs_response(data['weekly_added_products']['buckets'])
                    product_stat['monthly_added_products'] = modify_es_aggs_response(data['monthly_added_products']['buckets'])
                    product_stat['yearly_added_products'] = modify_es_aggs_response(data['yearly_added_products']['buckets'])
                except (AttributeError, KeyError):
                    pass
                return product_stat
