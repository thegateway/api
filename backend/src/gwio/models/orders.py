# -*- encoding: utf-8 -*-
"""
Order model
"""
import base64
import logging
import random
import uuid
from collections import defaultdict
from decimal import Decimal
from functools import reduce
from typing import (
    Optional,
)

from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from flask import g
from marshmallow import (
    EXCLUDE,
    ValidationError,
    fields,
    post_dump,
    post_load,
    pre_dump,
    pre_load,
)
from marshmallow.fields import Field
from werkzeug.exceptions import (
    Conflict,
    NotFound,
    InternalServerError,
)

import gwio.user
from gwio.core.cw_logging import LogEventType, Event
from gwio.environment import Env
from gwio.ext.dynamorm import (
    FsmEnumField,
    FsmModel,
    GwioListField,
    GwioModelField,
    NestedSchemaModel,
    TransitionEvent,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.ext.vat import (
    Currency,
)
from gwio.logic.orders import notifications as order_notifications
from gwio.logic.deliveries.xpress_couriers import (
    create_xpresscouriers_orders,
    XpressCouriersDeliveryMethod,
)
from gwio.logic.orders.handler import OrderHandler
from gwio.logic.orders.types import (
    OrderAction,
    OrderAssessment,
    OrderNotificationType as OrdN,
    OrderState,
)
from gwio.logic.packages import notifications as package_notifications
from gwio.logic.packages.handler import (
    PackageAction,
    PackageHandler,
    PackageNotificationType,
    PackageState,
)
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.logic.returns import notifications as return_notifications
from gwio.logic.returns.handler import (
    ReturnDeliveryAction,
    ReturnDeliveryHandler,
    ReturnDeliveryState,
)
from gwio.models import payment_attribute
from gwio.models.coupons.promotions import Coupon
from gwio.models.payment_method import PaymentMethod
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.user import BusinessId
from gwio.models.webshops import Webshop
from gwio.utils.fsm import (
    FsmAction,
    FsmState,
)
from .modifiers import (
    ModifierLineSource,
    ModifierLineUpdateSchema,
)

# It is important to import order notifications to make sure `Order.notify()` will function correctly
assert order_notifications
assert package_notifications
assert return_notifications

log = logging.getLogger(__name__)


def _generate_reference() -> str:
    return base64.b32encode(random.randint(0, 1 << (8 * 3)).to_bytes(3, 'little')).rstrip(b'=').decode('ascii')


class OrderProductLineInputSchema(GwioSchema):
    guid = fields.UUID(required=True)
    shop_guid = fields.UUID(required=True)
    qty = fields.Decimal(required=True)


class ContactInformationSchema(GwioSchema):
    email = fields.String()
    name = fields.String(required=True)
    phone = fields.String()
    postal_code = fields.String()
    street_address = fields.String()
    country_code = fields.String()


class ContactInformationWithoutLoginSchema(ContactInformationSchema):
    email = fields.String(required=True, allow_none=False)


class AddressesSchema(GwioSchema):
    billing = fields.Nested(ContactInformationSchema)
    shipping = fields.Nested(ContactInformationSchema)


class PaymentSchema(GwioSchema):
    type = fields.String()
    state = fields.String()
    date = fields.DateTime()
    amount = fields.Decimal()
    last4 = fields.String()
    intent_id = fields.String()
    client_secret = fields.String()
    redirect_url = fields.String()


class TimestampsSchema(GwioSchema):
    ordered = fields.DateTime()
    paid = fields.DateTime()


class OrderProductsInputSchema(GwioSchema):
    """Schema for products input on create/update"""
    products = fields.List(fields.Nested(OrderProductLineInputSchema), many=True, required=False)


class VatModel(NestedSchemaModel):
    class Schema(GwioSchema):
        vat = fields.Decimal()
        amount = fields.Decimal()


class Summary(NestedSchemaModel):
    class Schema(GwioSchema):
        currency = GwioEnumField(enum=Currency, by_value=True, allow_none=True)
        price = fields.Decimal()
        vat0_price = fields.Decimal()
        vats = GwioListField(VatModel)


class ShopSummary(NestedSchemaModel):
    class Schema(Summary.Schema):
        shop_guid = fields.UUID()


class RootSummary(NestedSchemaModel):
    class Schema(Summary.Schema):
        delivery_fee = GwioModelField(Summary)
        shops = GwioListField(ShopSummary)


class _BaseFlagsSchema(GwioSchema):
    prepayment = fields.Boolean(missing=True, default=True)
    require_review = fields.Boolean(missing=False)
    require_strong_authentication = fields.Boolean(missing=False)


class GetFlagsSchema(_BaseFlagsSchema):
    # Order statuses
    delivered = fields.Boolean()
    invalid = fields.Boolean()
    paid = fields.Boolean()
    require_phone = fields.Boolean()


# Just making FlagsSchema copy TimestampsSchema fields as booleans so that FlagsSchema does not need to be updated when TimestampsSchema changes
# pylint: disable=protected-access, no-member
for _field in TimestampsSchema._declared_fields:  # '_field' is used to make sure there won't be `redefined-outer-name` lint warnings later
    GetFlagsSchema._declared_fields[_field] = fields.Boolean()


class _OrderCustomerBaseSchema(GwioSchema):
    name = fields.String(default=None)
    phone = fields.String(default=None)
    # TODO: populate from token on CREATE, UPDATE on CREATED, and PLACE. Remember to test the pin is only returned
    pin = fields.String(allow_none=True)
    business_id = GwioModelField(BusinessId)


class Customer(NestedSchemaModel):
    Schema = _OrderCustomerBaseSchema


class Address(NestedSchemaModel):
    class Schema(GwioSchema):
        name = fields.String()
        phone = fields.String(allow_none=True)
        email = fields.String(allow_none=True)
        street_address = fields.String(allow_none=True)
        postal_code = fields.String(allow_none=True)
        country_code = fields.String(allow_none=True)


class Flags(NestedSchemaModel):
    Schema = _BaseFlagsSchema

    @property
    def delivered(self):
        return self.order.status_delivered

    @property
    def require_phone(self):
        return self.require_review


class Data(NestedSchemaModel):
    class Schema(GwioSchema):
        class StripeData(NestedSchemaModel):
            class Schema(GwioSchema):
                card_id = fields.String()
                intent_id = fields.String()
                client_secret = fields.String()

        class PaytrailData(NestedSchemaModel):
            class Schema(GwioSchema):
                class Urls(NestedSchemaModel):
                    class Schema(GwioSchema):
                        success = fields.String()
                        cancel = fields.String()

                urls = GwioModelField(Urls)

        class PayuData(NestedSchemaModel):
            class Schema(GwioSchema):
                card_token = fields.String()
                continue_url = fields.String()

        stripe = GwioModelField(StripeData, load=True)
        paytrail = GwioModelField(PaytrailData, load=True)
        payu = GwioModelField(PayuData, load=True)


class PackageTransitionEvent(TransitionEvent):
    class Schema(TransitionEvent.Schema):
        action = FsmEnumField(enum=PackageAction, fsm_enum=FsmAction)
        state = FsmEnumField(enum=PackageState, fsm_enum=FsmState)


class PackageContent(NestedSchemaModel):
    class Schema(GwioSchema):
        product_guid = fields.UUID(allow_none=False)
        qty = fields.Decimal(allow_none=False)


class XpressCouriersPackageDataSchema(GwioSchema):
    order_no = fields.String()


class XpressCouriersPackageData(NestedSchemaModel):
    Schema = XpressCouriersPackageDataSchema

    class Schemas:
        Get = XpressCouriersPackageDataSchema


class PackageDeliveryMethodDataSchema(GwioSchema):
    xpress_couriers = GwioModelField(XpressCouriersPackageData, allow_none=True)


class PackageDeliveryMethodData(NestedSchemaModel):
    Schema = PackageDeliveryMethodDataSchema

    class Schemas:
        Get = PackageDeliveryMethodDataSchema


class Package(FsmModel, NestedSchemaModel):
    class Schema(GwioSchema):
        guid = fields.UUID(missing=uuid.uuid4)
        contents = GwioListField(PackageContent)
        delivery_method_data = GwioModelField(PackageDeliveryMethodData, allow_none=True)

        # FSM related attributes
        _current_state = FsmEnumField(enum=PackageState, fsm_enum=FsmState, missing=PackageState.NOT_STARTED)
        _transition_history = GwioListField(PackageTransitionEvent)

    # FSM related attributes
    _fsm_handler = PackageHandler()

    class API(FsmModel.API):
        @classmethod
        def notify(cls, *, model: 'Package', notification: PackageNotificationType, **_):
            """Handle notifications triggered by state changes for the package"""
            try:
                user = gwio.user.User.by_identity_id(model.order.buyer_identity_id)
            except NotFound:
                from gwio.user.user import NonLoggedInUser

                user = NonLoggedInUser(identity_id=model.order.buyer_identity_id,
                                       email=model.order.billing_address.email)
            context = dict(
                package=model,
                user=user,
                shops=[Webshop.get(guid=shop_guid) for shop_guid in model.order.shop_guids],
            )
            cls._notify(notification=notification, context=context)

    @property
    def status_delivered(self) -> bool:
        """Order has been shipped/picked up"""
        return True if self.timestamps.delivered else False

    @property
    def timestamps(self):
        class Timestamps():
            order = None

            def __init__(self, order):
                self.order = order

            def _find_transition_timestamp(self, *conditions):
                previous_event = None
                for event in self.order._transition_history:  # pylint: disable=protected-access
                    if all(x(previous_event, event) for x in conditions):
                        return event.timestamp
                    previous_event = event
                return None

            @property
            def packed(self):
                return self._find_transition_timestamp(
                    lambda prev_event, event: prev_event and prev_event.state == PackageState.WAITING_PACKING,
                    lambda prev_event, event: event.state == PackageState.WAITING_DELIVERY,
                )

            @property
            def delivered(self):
                return self._find_transition_timestamp(lambda prev_event, event: event.state == PackageState.DELIVERED)

        return Timestamps(self)

    class ProcessLogic:
        @staticmethod
        def delivered(*, model: 'Package', **_):
            model.order.process(OrderAction.COMPLETE)


class _BaseReceivedReturnSchema(GwioSchema):
    qty = fields.Decimal(required=True)


class ReceivedReturn(NestedSchemaModel):
    Schema = _BaseReceivedReturnSchema

    class Schemas:
        class Get(_BaseReceivedReturnSchema):
            """
            name: received return
            description: information for a received product return
            """

        class Post(Get):
            """
            name: received return
            description: information for newly received return product
            """


class _BaseProductReturnLineSchema(GwioSchema):
    product_guid = fields.UUID(required=True)
    shop_guid = fields.UUID(required=True)
    qty = fields.Decimal(required=True)
    received = GwioListField(ReceivedReturn)


class ProductReturnLine(NestedSchemaModel):
    Schema = _BaseProductReturnLineSchema

    class Schemas:
        class Get(_BaseProductReturnLineSchema):
            """
            name: product return line
            description: information for a product return
            """
            received = fields.Nested(ReceivedReturn.Schemas.Get, many=True)

    @property
    def fully_returned(self):
        returned_qty = 0
        for received_line in self.received:
            returned_qty += received_line.qty
        return returned_qty >= self.qty


class ProductReturnTransitionEvent(TransitionEvent):
    class Schema(TransitionEvent.Schema):
        action = FsmEnumField(enum=ReturnDeliveryAction, fsm_enum=FsmAction)
        state = FsmEnumField(enum=ReturnDeliveryState, fsm_enum=FsmState)


class _BaseProductReturnSchema(GwioSchema):
    guid = fields.UUID(missing=uuid.uuid4)
    lines = GwioListField(ProductReturnLine)
    reason = fields.String(missing=None)

    # FSM related attributes
    _current_state = FsmEnumField(enum=ReturnDeliveryState, fsm_enum=FsmState, missing=ReturnDeliveryState.NOT_STARTED)
    _transition_history = GwioListField(ProductReturnTransitionEvent)


class ProductReturn(FsmModel, NestedSchemaModel):
    Schema = _BaseProductReturnSchema

    class Schemas:
        class Get(_BaseProductReturnSchema):
            """
            name: product return
            description: product return information
            """
            lines = fields.Nested(ProductReturnLine.Schemas.Get, many=True, required=True)

        class Post(Get):
            """
            name: product return
            description: return request information
            """

            class Meta:
                exclude = ['guid']

    # FSM related attributes
    _fsm_handler = ReturnDeliveryHandler()

    class API(FsmModel.API):
        @classmethod
        def notify(cls, *, model: 'ProductReturn', notification: OrdN, **_):
            """Handle notifications triggered by state changes for the ProductReturn"""
            try:
                user = gwio.user.User.by_identity_id(model.order.buyer_identity_id)
            except NotFound:
                from gwio.user.user import NonLoggedInUser

                user = NonLoggedInUser(identity_id=model.order.buyer_identity_id,
                                       email=model.order.billing_address.email)
            context = dict(
                return_delivery=model,
                user=user,
                shops=[Webshop.get(guid=return_line.shop_guid) for return_line in model.lines],
            )
            cls._notify(notification=notification, context=context)

    @property
    def status_fully_returned(self):
        for return_line in self.lines:
            if not return_line.fully_returned:
                return False
        return True


class OrderTransitionEvent(TransitionEvent):
    class Schema(TransitionEvent.Schema):
        action = FsmEnumField(enum=OrderAction, fsm_enum=FsmAction)
        state = FsmEnumField(enum=OrderState, fsm_enum=FsmState)


class OrderWebshop(NestedSchemaModel):
    class Schemas:
        Base = Webshop.Schemas.Nested
        Get = Base

    Schema = Schemas.Base


class _BaseOrderPaymentMethodSchema(GwioSchema, PaymentMethod.Schemas.BaseSchema):
    guid = fields.UUID(required=True)  # To override missing, we want to avoid any chance of random guid popping up here


class OrderPaymentMethod(NestedSchemaModel):
    Schema = _BaseOrderPaymentMethodSchema

    class Schemas:
        class Get(_BaseOrderPaymentMethodSchema):
            # Stripe specific
            card = fields.String()

            @pre_dump
            def load_values_for_backwards_compatibility(self, values, **_):
                try:
                    values.card = values.order.data.stripe.card_id
                except AttributeError:
                    pass
                return values

        class Post(Get):
            class Meta:
                # guid is replaced by `instance`: PaymentMethod in @post_load
                exclude = [x for x in _BaseOrderPaymentMethodSchema._declared_fields.keys() if x not in ('guid',)]

            class PaytrailUrls(GwioSchema):
                success = fields.String(required=True)
                cancel = fields.String(required=True)

            # Paytrail specific
            urls = fields.Nested(PaytrailUrls, load_only=True)
            # PayU specific
            continue_url = fields.String()

            # pylint: disable=no-self-use
            @post_load
            def get_payment_method(self, values, **_):
                try:
                    values['instance'] = PaymentMethod.get(guid=values['guid'])
                    values.pop('guid')
                except PaymentMethod.DoesNotExist:
                    raise Conflict('Invalid payment method guid.')

                if values['instance'].type == PaymentTypes.STRIPE:
                    try:
                        values['card']
                    except KeyError:
                        raise ValidationError(Field.default_error_messages['required'], field_name='card')

                return values

        Put = Post

    @property
    def order(self):
        return self._parent_model

    @property
    def payment_method(self):
        return PaymentMethod.get(guid=self.guid)


class _OrderBaseSchema:
    class Meta(GwioSchema.Meta):
        unknown = EXCLUDE

    # NOTE! when defining attributes, make sure you do not use names that are defined / used by transitions Machine()
    guid = fields.UUID(missing=uuid.uuid4)
    customer = GwioModelField(Customer)
    # shop_guid = fields.UUID()
    shop_guids = fields.List(fields.UUID())
    shops = GwioListField(OrderWebshop)
    buyer_identity_id = fields.Str()
    product_lines = GwioListField(Product.API.Assessor.EvaluatedProduct.ProductLine)
    product_returns = GwioListField(ProductReturn)

    data = GwioModelField(Data, load=True)
    summary = GwioModelField(RootSummary, load=True)
    reference = fields.String(missing=_generate_reference)
    packages = GwioListField(Package)
    payments = GwioListField(payment_attribute.Payment)
    payment_link = fields.String(allow_none=True)
    payment_success_link = fields.String(allow_none=True)
    payment_cancelled_link = fields.String(allow_none=True)
    description = fields.String(allow_none=True)
    payment_method_guid = fields.UUID(allow_none=True)
    payment_method = GwioModelField(OrderPaymentMethod, allow_none=True)
    shipping_address = GwioModelField(Address, allow_none=True)
    billing_address = GwioModelField(Address, allow_none=True)
    flags = GwioModelField(Flags, load=True)
    coupon = GwioModelField(Coupon, allow_none=True)

    # Statement descriptor is shown on the buyer's statement. Stripe allows maximum of ASCII 22 characters.
    # https://stripe.com/docs/api/python#create_charge-statement_descriptor
    statement_descriptor = fields.String(allow_none=True)

    # FSM related attributes
    _current_state = FsmEnumField(enum=OrderState, fsm_enum=FsmState, by_value=True, missing=OrderState.START)
    _transition_history = GwioListField(OrderTransitionEvent)


class CustomerSchema(_OrderCustomerBaseSchema, GwioSchema):
    class Meta:
        exclude = ['pin']


class PackageSchema(GwioSchema):
    guid = fields.UUID()
    state = fields.String(attribute='current_state')


class OrderSchema(GwioSchema):
    """Schema for representing the order information"""

    addresses = fields.Nested(AddressesSchema)
    buyer_identity_id = fields.Str()
    customer = fields.Nested(CustomerSchema)
    guid = fields.UUID()
    lines = fields.Nested(Product.API.Assessor.EvaluatedProduct.ProductLine.Schemas.Get, attribute='product_lines', many=True)
    summary = fields.Nested(RootSummary.Schema)

    packages = fields.Nested(PackageSchema, many=True)
    product_returns = fields.Nested(ProductReturn.Schemas.Get, many=True)
    payment = fields.Nested(PaymentSchema, attribute='payment_data')
    reference = fields.String()
    shops = fields.Nested(OrderWebshop.Schemas.Get, many=True)
    source = fields.String()
    state = fields.String(attribute='current_state')
    timestamps = fields.Nested(TimestampsSchema)

    payment_method = fields.Nested(OrderPaymentMethod.Schemas.Get)
    flags = fields.Nested(GetFlagsSchema)
    coupon = fields.Nested(Coupon.Schemas.Get, allow_none=True)

    @pre_dump
    # pylint:disable=no-self-use
    def pre_dump_data_population(self, order, **_):
        try:
            order.payment_method.card = order.data.stripe.card_id
        except (AttributeError, IndexError):
            pass
        try:
            order.payment_method.continue_url = order.data.payu.continue_url
        except (AttributeError, IndexError):
            pass
        # Add statuses to flags
        for attr_name in (x for x in dir(order) if x.startswith('status_') and x != 'status_need_review'):
            if not hasattr(order.flags, attr_name[7:]):
                setattr(order.flags, attr_name[7:], getattr(order, attr_name))

        for attr_name in dir(order.timestamps):
            if isinstance(getattr(order.timestamps.__class__, attr_name), property) and not hasattr(order.flags, attr_name):
                setattr(order.flags, attr_name, bool(getattr(order.timestamps, attr_name)))

        return order

    @post_dump
    # pylint:disable=no-self-use
    def available_actions(self, dump, **kwargs):
        # Payment is triggered by backend automatically when a payment is received
        dump['available_actions'] = [x for x in OrderHandler().get_triggers(dump['state']) if not x.startswith(('to_', '_')) and x != 'payment']
        return dump


class OrderShopkeeperSchema(OrderSchema):
    class CustomerShopkeeperSchema(CustomerSchema):
        pin = fields.String()

    handler = fields.String()
    customer = fields.Nested(CustomerShopkeeperSchema)


class OrderCustomerSchema(OrderSchema):
    pass


class OrderCreateSchema(GwioSchema):
    """
    name: order
    description: the order to be created.
    """
    customer_name = fields.String(required=True)
    products = fields.List(fields.Nested(OrderProductLineInputSchema), many=True, required=True)
    payment_method = fields.Nested(OrderPaymentMethod.Schemas.Post)


class OrderCreateWithoutLoginSchema(OrderCreateSchema):
    """
    name: order
    description: base schema for updating order and creating order without logging in
    """
    shipping_address = fields.Nested(ContactInformationWithoutLoginSchema)
    billing_address = fields.Nested(ContactInformationWithoutLoginSchema)
    delivery_method = fields.UUID()
    customer = fields.Nested(CustomerSchema)

    @post_load
    # pylint: disable=no-self-use
    def load_delivery_method(self, values, **kwargs):
        try:
            values['delivery_method'] = Product.by_guid(values['delivery_method'])
            if values['delivery_method'].type != PRODUCT_TYPE_DELIVERY_METHOD:
                raise Conflict('Delivery method does not exist')
        except KeyError:
            pass
        except Product.DoesNotExist:
            raise Conflict('Delivery method does not exist')
        return values


class OrderUpdateSchema(OrderCreateWithoutLoginSchema):
    """
    name: order
    description: the order to be updated.
    """

    class Meta:
        exclude = ('customer_name',)

    products = fields.List(fields.Nested(OrderProductLineInputSchema), many=True)
    payment_method = fields.Nested(OrderPaymentMethod.Schemas.Put)
    coupon = fields.UUID()

    @pre_load
    # pylint: disable=no-self-use
    def combine_products_by_guid(self, data, **kwargs):
        try:
            product_qtys = dict()
            for x in data['products']:
                product_qtys[x['guid']] = dict(qty=product_qtys.get(x['guid'], dict()).get('qty', 0) + x['qty'], shop_guid=x['shop_guid'])
            data['products'] = [dict(guid=guid, **vals) for guid, vals in product_qtys.items()]
        except KeyError:
            pass
        return data

    @post_load
    # pylint: disable=no-self-use
    def load_coupon(self, values, **kwargs):
        try:
            values['coupon'] = g.user.get_coupon(values['coupon'])
        except KeyError:
            pass
        except ValueError:
            raise Conflict('Coupon does not exist')
        return values


def _fill_shop_summary(line):
    shop_summary = ShopSummary(shop_guid=line.shop_guid, currency=None, price=Decimal(0), vat0_price=Decimal(0))
    if shop_summary.currency is None:
        shop_summary.currency = line.price.original.total.price.currency
    shop_summary.vats.append(dict(vat=line.price.vat_class, amount=line.price.final.total.included_vat.amount))
    shop_summary.vat0_price += line.price.final.total.without_vat.amount
    shop_summary.price += line.price.final.total.amount
    return shop_summary


def _generate_shop_summary(shop_summaries):
    def _combine_shops(coll, el):
        try:
            combined_shop = next(filter(lambda p: p.shop_guid == el.shop_guid, coll))
            combined_shop.price += el.price
            combined_shop.vat0_price += el.vat0_price
            combined_shop.vats.extend(el.vats)
        except StopIteration:
            coll.append(el)
        return coll

    shop_summaries = list(reduce(_combine_shops, shop_summaries, []))

    def _sumount(coll, el):
        coll[el.vat] += el.amount
        return coll

    for shop_summary in shop_summaries:
        shop_vats = reduce(_sumount, shop_summary.vats, defaultdict(lambda: 0))
        shop_summary.vats = [VatModel(vat=vat, amount=amount) for vat, amount in shop_vats.items()]
    return shop_summaries


def _apply_coupons(line, model, product):
    try:
        if model.coupon.check_conditions(model):
            line.price.original.modifiers.append(
                dict(
                    source=ModifierLineSource.COUPON,
                    **model.coupon.benefit.dump_with_fixed_modifiers(product.API.Assessor.evaluate(product)),
                )
            )
    except AttributeError:
        # AttributeError happens when there is no coupon
        pass


def _apply_campaign(line, product):
    try:
        line.price.original.modifiers.append(
            dict(source=ModifierLineSource.CAMPAIGN, **product.campaign_modifier))
    except TypeError:
        pass


def _calculate_prices(line):
    price_attrs = ('unit', 'total')
    # Resetting the final prices back to original before the modifiers will be applied
    for price_attr in price_attrs:
        setattr(line.price.final, price_attr, getattr(line.price.original, price_attr))

    for price_modifier in line.price.original.modifiers:
        price_modifier.calc_fixed_modifiers(line)

        # Applying modifiers to line final prices
        for price_attr in price_attrs:
            setattr(line.price.final, price_attr,
                    getattr(line.price.final, price_attr) + getattr(price_modifier.fixed_modifiers,
                                                                    price_attr))


def _check_currency(model, line, summary):
    if summary.currency is None:
        summary.currency = line.price.original.total.price.currency
    elif summary.currency is not line.price.original.total.price.currency:
        log.exception('Currencies for order %s do not match', str(model.guid))
        raise TypeError(f'Currencies for order {model.guid} do not match')


def _assess_prices(*, model: 'Order', **_):
    """
    Handle assessments requests triggered by state change for the order. Assessor
    is responsible for creating the summary field and updating possible status
    flags (in imeds case to check if the order has prescription medicines and if
    it does then flag the order as requiring review).
    """
    summary = RootSummary(
        currency=None,
        price=Decimal(0),
        vat0_price=Decimal(0),
        delivery_fee=Summary(
            currency=None,
            price=Decimal(0),
            vat0_price=Decimal(0),
            vats=list(),
        ),
    )

    shop_summaries = []

    vats = dict()
    # TODO: This should propably be removed so that this assessment can only change the order to require review, not change it from True ot False
    model.require_review = False

    for line in model.product_lines:
        # Resetting applied campaign discounts
        # TODO: This should be done only before the order is placed
        line.price.original.modifiers = [x for x in line.price.original.modifiers if
                                         x.source not in (ModifierLineSource.CAMPAIGN, ModifierLineSource.COUPON)]

        if line.type == 'prescription_medicine':
            model.flags.require_review = True
            model.flags.require_strong_authentication = True
        # NOTE! this assumes that the all prices are in the same currency
        _check_currency(model, line, summary)
        if vats.get(line.price.original.unit.vat_percent) is None:
            vats[line.price.vat_class] = Decimal(0)

        product = Product.by_guid(line.guid)
        _apply_campaign(line, product)
        _apply_coupons(line, model, product)

        _calculate_prices(line)

        # Applying line final prices to order totals in summary
        summary.vat0_price += line.price.final.total.without_vat.amount
        summary.price += line.price.final.total.amount
        vats[line.price.vat_class] += line.price.final.total.included_vat.amount

        # If the line is delivery fee, it's not part of the shop's summary because the money goes to the platform owner
        if line.type == PRODUCT_TYPE_DELIVERY_METHOD:
            if summary.delivery_fee.currency is not None:
                raise NotImplementedError('No support for orders with multiple delivery methods')
            summary.delivery_fee.currency = line.price.currency
            summary.delivery_fee.vat0_price = line.price.final.total.without_vat.amount
            summary.delivery_fee.price = line.price.final.total.amount
            summary.delivery_fee.vats.append(VatModel(
                amount=line.price.final.total.included_vat.amount,
                vat=line.price.vat_class,
            ))
        else:
            shop_summaries.append(_fill_shop_summary(line))

    summary.vats = [VatModel(vat=key, amount=vats[key]) for key in vats]
    model.summary = summary

    model.summary.shops = _generate_shop_summary(shop_summaries)


def _log_placement(*, model: 'Order', **_):
    try:
        Event(
            event_type=LogEventType.ORDER_PLACE,
            subject=gwio.user.User.by_identity_id(model.buyer_identity_id, id_if_user_missing=True),
            obj=model,
            data=model.Schema().dumps(model, use_decimal=True)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the order place information for order %s: %s', model.guid, str(e))
        raise InternalServerError('Failed to place the order')


_assessors = {
    OrderAssessment.PRICES: _assess_prices,
    OrderAssessment.LOG_PLACEMENT: _log_placement,
}


def _setup_missing_payment_method(order):
    if order.payment_method is None and order.payment_method_guid is not None:
        order.payment_method = PaymentMethod.get(guid=order.payment_method_guid).to_dict()
        return True
    return False


def _setup_missing_shops(order):
    shops_guids = set([x.guid for x in order.shops])
    product_lines_shop_guids = set([x.shop_guid for x in order.product_lines])

    modified = False
    remove_guids = shops_guids - product_lines_shop_guids
    if remove_guids:
        order.shops = [x for x in order.shops if x.guid not in remove_guids]
        modified = True

    for add_guid in product_lines_shop_guids - shops_guids:
        try:
            order.shops.append(Webshop.get(guid=add_guid))
            modified = True
        except Webshop.DoesNotExist:
            # No need to report warning if the "shop" is the platform owner
            if add_guid != uuid.UUID(int=0):
                log.warning('Order has products from nonexistent shop %s', str(add_guid))

    return modified


@adapted_dynamodel
class Order(FsmModel, DynaModel):
    _fix_values_funcs = [
        _setup_missing_payment_method,
        _setup_missing_shops,
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._fix_values_init()

    class Table:
        name = 'orders'
        version = 3
        hash_key = 'guid'

    class Schemas:
        CustomerGet = OrderCustomerSchema
        ShopkeeperGet = OrderShopkeeperSchema
        Post = OrderCreateSchema
        Put = OrderUpdateSchema
        PostWithoutLogin = OrderCreateWithoutLoginSchema

    Schema = _OrderBaseSchema

    # noinspection PyBroadException
    class API(FsmModel.API):
        update_modifiers = ModifierLineUpdateSchema

        generate_reference = _generate_reference

        _assessor_functions = _assessors

        @classmethod
        def notify(cls, *, model: 'Order', notification: OrdN, **_):
            """Handle notifications triggered by state changes for the order"""
            # model.buyer_identity_id should already an identity_id, except for the users who haven't logged in since update
            try:
                user = gwio.user.User.by_identity_id(model.buyer_identity_id)
            except NotFound:
                from gwio.user.user import NonLoggedInUser

                user = NonLoggedInUser(identity_id=model.buyer_identity_id,
                                       email=model.billing_address.email)
            context = dict(
                order=model,
                user=user,
                shops=[Webshop.get(guid=shop_guid) for shop_guid in model.shop_guids],
            )
            cls._notify(notification=notification, context=context)

    # pylint: disable=no-member,unused-argument
    class ProcessLogic:
        @staticmethod
        def processing(*, model: 'Order', **_):
            """
                The payment fsm is triggered to capture if
                    the order is `prepayment` and there are no payments yet
                    or
                    order has packages that have all been delivered
                otherwise if the order has no packages, a package is created with all the products in the order
                and the package fsm is triggered.
                """
            if model.status_prepayment and not model.payments:
                payment = model.payment_method.payment_method.new_payment_from_order(order=model)
                payment.create_charge()

            # If multiple packages exist, each one of them will trigger this code if there are still other undelivered packages
            elif not model.packages:
                try:
                    # Try to create Xpress Couriers' delivery
                    pm = PaymentMethod.get(guid=model.payment_method_guid)
                    cash_on_delivery = pm.type is PaymentTypes.COD
                    xpc_dm = XpressCouriersDeliveryMethod.from_order(model, cash_on_delivery)
                    xpc_orders = create_xpresscouriers_orders(model, xpc_dm)
                    for xpc_order in xpc_orders:
                        package = Package(
                            contents=[dict(product_guid=x.guid, qty=x.qty) for x in model.product_lines],
                            delivery_method_data=dict(
                                xpress_couriers=dict(
                                    order_no=xpc_order.no
                                )),
                        )
                        model.packages.append(package)
                        package.process(PackageAction.START)

                        if cash_on_delivery:
                            payment = model.payment_method.payment_method.new_payment(amount=xpc_order.order_price, currency=model.summary.currency)
                            model.payments.append(payment)
                            payment.create_charge()
                except XpressCouriersDeliveryMethod.XpressCouriersInitDataException:
                    # Not Xpress Couriers delivery method
                    package = Package(
                        contents=[dict(product_guid=x.guid, qty=x.qty) for x in model.product_lines],
                        delivery_method_data=dict()
                    )
                    model.packages.append(package)
                    package.process(PackageAction.START)

    # FSM related attributes
    _fsm_handler = OrderHandler()

    # Properties
    _buyer = None
    _shops = []
    _payment_method = None

    @adapted_dynamodel
    class CustomerIndex(GlobalIndex):
        name = 'order-customer-index'
        hash_key = 'buyer_identity_id'
        range_key = 'guid'
        projection = ProjectAll()

    # pylint: disable=too-few-public-methods
    @adapted_dynamodel
    class StateIndex(GlobalIndex):
        name = 'orders-state-index'
        hash_key = '_current_state'
        # range_key = 'shop_guid'
        projection = ProjectAll()

    def __repr__(self):
        return f'<Model guid={self.guid}>'

    @classmethod
    def get_by_shop(cls, shop_guid, guid):
        # pylint: disable=no-value-for-parameter
        order = cls.get(guid=str(guid))
        if shop_guid not in order.shop_guids:
            raise cls.DoesNotExist
        return order

    @classmethod
    def get_by_customer(cls, identity_guid, guid):
        identity_id = str(identity_guid)
        if len(identity_id.split(':')) == 1:
            identity_id = f'{Env.AWS_REGION}:{identity_id}'
        # pylint: disable=no-value-for-parameter
        for order in cls.CustomerIndex.query(buyer_identity_id=identity_id, guid=str(guid)):
            return order
        raise cls.DoesNotExist

    @classmethod
    def get_by_webshop_guid(cls, webshop_guid: uuid.UUID):
        res = cls.scan(shop_guids__contains=str(webshop_guid))
        res.recursive()
        res = list(res)
        return res

    @property
    def delivery_method_guid(self):
        try:
            delivery_method_lines = [x for x in self.product_lines if x.type == PRODUCT_TYPE_DELIVERY_METHOD]
            if len(delivery_method_lines) > 1:
                raise LookupError('Order has multiple delivery methods')
            return delivery_method_lines[0].guid
        except IndexError:
            return None

    @property
    def addresses(self):
        addresses = {}
        for address_type in ('billing', 'shipping'):
            address = getattr(self, f'{address_type}_address')
            if not address:
                continue
            addresses[address_type] = address
        return addresses

    @property
    def buyer(self):
        if self._buyer is None or self._buyer.identity_id != self.buyer_identity_id:
            # Using `g.user` if possible, so that the same instance is modified
            if g.user and g.user.identity_id == self.buyer_identity_id:
                self._buyer = g.user
            else:
                self._buyer = gwio.user.User.by_identity_id(self.buyer_identity_id, id_if_user_missing=True)
        return self._buyer

    @property
    def payment_data(self):
        try:
            payment = self.payments[0]
            type_specific_data = payment.client_data
            return {
                'type': payment.payment_type,
                'last4': payment.last4,
                'date': payment.datetime,
                'amount': payment.amount,
                'state': payment.current_state,
                **type_specific_data,
            }
        except IndexError:
            return dict()
        except Exception as e:
            # This is here because marshmallow apparently eats and silences exceptions
            log.error(e)
            raise

    @property
    def timestamps(self):
        class Timestamps():
            order = None

            def __init__(self, order):
                self.order = order

            def _find_transition_timestamp(self, *conditions):
                previous_event = None
                for event in self.order._transition_history:  # pylint: disable=protected-access
                    if all(x(previous_event, event) for x in conditions):
                        return event.timestamp
                    previous_event = event
                return None

            @property
            def ordered(self):
                return self._find_transition_timestamp(lambda prev_event, event: event.action == OrderAction.PLACE)

        return Timestamps(self)

    # *Properties* needed by OrderHandling() logic for checking models status for FsmTransition()
    # NOTE try not to use the current_state as the sole determining factor for the status
    #      as these are mainly used by the OrderHandler() to validate transitions between states
    @property
    def status_paid(self) -> bool:
        """Order has been paid in full"""
        paid = Decimal(sum(x.amount for x in self.payments if x.status_paid))
        return paid >= self.total_price

    @property
    def total_price(self) -> Decimal:
        try:
            return self.summary.price if self.summary.price is not None else Decimal('0')
        except AttributeError:
            return Decimal('0')

    @property
    def total_currency_code(self) -> Optional[str]:
        try:
            return self.summary.currency.abbr
        except AttributeError:
            return None

    @property
    def reviewed(self) -> bool:
        return str(OrderState.WAITING_REVIEW) in self._transition_history

    @property
    def status_delivered(self):
        if not self.packages:
            return False
        for package in self.packages:
            if not package.status_delivered:
                return False
        return True

    @property
    def status_need_review(self) -> bool:
        """Order needs a review"""
        return self.flags.require_review

    @property
    def status_prepayment(self) -> bool:
        """Order must be prepaid"""
        return self.flags.prepayment

    @property
    def status_invalid(self) -> bool:
        """The order is invalid (for whatever reason)"""
        # This is the exception to the rule e.g. if the order is not in created state
        # it cannot be invalid as it should have not gone further if it was
        if self.current_state != OrderState.CREATED:
            return False
        # Now for the START and CREATED states we must actually check the order itself
        # TODO: Implement checks for example for "can not determine price for some product"
        # TODO: If product line price type is error, order is invalid
        invalid_checks = [
            (lambda order: not order.payment_method_guid, 'Payment method missing'),
            (lambda order: not order.customer.name, 'Customer name missing'),
            (lambda order: order.flags.require_phone and not order.customer.phone, 'Phone number missing'),
            (lambda order: not order.delivery_method_guid, 'Delivery method missing'),
            (lambda order: not [x for x in order.product_lines if x.type != PRODUCT_TYPE_DELIVERY_METHOD], 'Order has no products'),
            (lambda order: order.flags.require_strong_authentication and not order.customer.pin, 'Customer SSN missing'),
        ]

        try:
            for (check, error_msg) in invalid_checks:
                if check(self):
                    return True
        except AttributeError:  # Some required attribute is None so sub attribute can not be accessed
            return True
        return False

    def call_payment_operation(self, *, payment_guid, fn):
        try:
            payment = next(x for x in self.payments if x.guid == payment_guid)
        except StopIteration:
            raise NotFound(f'Payment {payment_guid} does not exist')
        fn(payment=payment)
        return self
