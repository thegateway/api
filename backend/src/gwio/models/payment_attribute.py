import logging
import uuid
from decimal import Decimal

import stripe
from attrdict import AttrDict
from marshmallow import (
    fields,
)
from memoization import cached
from stripe.error import InvalidRequestError
from werkzeug.exceptions import NotFound

import gwio
import gwio.user
from gwio.environment import Env
from gwio.ext.dynamorm import (
    FsmEnumField,
    FsmModel,
    GwioListField,
    GwioModelField,
    JSONField,
    NestedSchemaModel,
    TransitionEvent,
)
from gwio.ext.marshmallow import (
    GWIODateTime,
    GwioEnumField,
    GwioSchema,
)
from gwio.ext.vat import Currency
from gwio.logic.orders.types import (
    OrderAction,
    OrderNotificationType,
)
from gwio.logic.payments import (
    paytrail,
    payu,
    stripe as gw_stripe,
    cod,
)
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentHandler,
    PaymentNotificationType,
    PaymentState,
)
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.models import webshops
from gwio.models.coupons import promotions
from gwio.utils.aws import report_exception
from gwio.utils.fsm import (
    FsmAction,
    FsmState,
)

log = logging.getLogger(__name__)


# pylint: disable=too-few-public-methods
class PaymentPropertyMixin():
    @property
    def last4(self):
        return None

    @property
    def client_data(self):
        return dict()


# Lint gets very very confused by dynamorm init
# pylint: disable=access-member-before-definition
class StripeData(NestedSchemaModel):
    class Schema(GwioSchema):
        charge_capture = fields.Dict(allow_none=True)  # Only available after a successful capture
        charge = fields.Dict(allow_none=True)  # Only available after a successful charge
        charge_id = fields.String(allow_none=True)  # Only available after a successful charge
        refund = fields.String(allow_none=True)

        intent_id = fields.String(allow_none=True)  # Only available after a successful charge
        client_secret = fields.String(allow_none=True)  # Only available after a successful charge
        sca_success = fields.Dict(allow_none=True)  # Only available after a successful sca

        transfers = fields.List(fields.Dict(), allow_none=True)  # Only available after successful capture and available funds

        _last4 = fields.String(allow_none=True)  # Only available after a successful charge

    class API:
        REQUIRES_ACTION_STATUS = ['requires_source_action', 'requires_action']

    @property
    def last4(self):
        log.debug('charge_capture: %s', self.charge_capture)
        log.debug('charge: %s', self.charge)
        try:
            # Old timey way
            return self.charge_capture.get('source', {}).get('last4')  # pylint: disable=no-member
        except AttributeError:
            # PaymentIntent way
            if self._last4 is None:
                payment_method_id = None
                try:
                    payment_method_id = self.charge['payment_method']
                    self._last4 = stripe.PaymentMethod.retrieve(payment_method_id, api_key=Env.STRIPE_API_KEY)['card']['last4']
                except TypeError:
                    # Return None for last4 since a charge/capture has not been saved yet
                    pass
                except InvalidRequestError as e:
                    log.info('Could not get stripe payment method %s: %s', payment_method_id, str(e))
            return self._last4

    @property
    def client_data(self):
        return dict(intent_id=self.intent_id, client_secret=self.client_secret)

    @property
    def status_refunded(self):
        return bool(self.refund)

    @property
    def status_sca_required(self):
        if self.charge['status'] in self.API.REQUIRES_ACTION_STATUS:
            return True
        return False

    @property
    def status_sca_successful(self):
        if self.sca_success:
            return True
        return False


class PaytrailData(NestedSchemaModel):
    class Schema(GwioSchema):
        notify = fields.String(allow_none=True)

    @property
    def status_sca_required(self):
        return False


class PayuData(NestedSchemaModel):
    class Schema(GwioSchema):
        order_id = fields.String(allow_none=True)  # If the order creation fails this will be set by DynamOrm to None
        payu_order_data = JSONField(allow_none=True)  # If the order creation fails this will be set by DynamOrm to None
        redirect_url = fields.String(allow_none=True)  # If the order creation fails this will be set by DynamOrm to None
        completed_data = JSONField(allow_none=True)
        waiting_for_confirmation_data = JSONField(allow_none=True)
        refund_data = JSONField(allow_none=True)

    class API:
        SCA_COMPLETED_STATUS = 'COMPLETED'
        SCA_STATUSES = ['WARNING_CONTINUE_3DS', 'WARNING_CONTINUE_CVV']

    @property
    def client_data(self):
        return dict(redirect_url=self.redirect_url)

    @property
    def last4(self):
        try:
            self.completed_data['payMethods']['payMethod']['card']['number'][-4:]
        except (TypeError, AttributeError, KeyError):
            return ''

    @property
    def status_refunded(self):
        if self.refund_data:
            return True
        return False

    @property
    def status_sca_required(self):
        try:
            status = self.payu_order_data['status']
            if status['statusCode'] in self.API.SCA_STATUSES and status['severity'] == 'WARNING':
                return True
        except (KeyError, AttributeError, TypeError) as e:
            log.exception('Failed to check if SCA is required: %s', e)
            raise
        return False

    @property
    def status_sca_successful(self):
        try:
            if self.completed_data['order']['status'] == self.API.SCA_COMPLETED_STATUS:
                return True
        except (KeyError, AttributeError, TypeError):
            pass
        return False


class CodData(PaymentPropertyMixin, NestedSchemaModel):
    class Schema(GwioSchema):
        paid = fields.DateTime(allow_none=True)  # Only available after successful capture and available funds

    @property
    def status_sca_required(self):
        return False


class PaymentData(NestedSchemaModel):
    class Schema(GwioSchema):
        stripe = GwioModelField(StripeData, allow_none=True)
        paytrail = GwioModelField(PaytrailData, allow_none=True)
        payu = GwioModelField(PayuData, allow_none=True)
        cod = GwioModelField(CodData, allow_none=True)


class PaymentTransitionEvent(TransitionEvent):
    class Schema(TransitionEvent.Schema):
        action = FsmEnumField(enum=PaymentAction, fsm_enum=FsmAction)
        state = FsmEnumField(enum=PaymentState, fsm_enum=FsmState)


class _RefundShopAmounts(GwioSchema):
    shop_guid = fields.UUID(required=True)
    amount = fields.Decimal(required=True)


class RefundShopSchema(NestedSchemaModel):
    Schema = _RefundShopAmounts

    class Schemas:
        Post = _RefundShopAmounts


class PaymentSchema(GwioSchema):
    guid = fields.UUID(missing=uuid.uuid4)
    payment_type = GwioEnumField(PaymentTypes, by_value=True)
    datetime = GWIODateTime(allow_none=True)
    amount = fields.Decimal(allow_none=True)
    currency = GwioEnumField(Currency, by_value=True, allow_none=False)
    webshop_guid = fields.UUID(allow_none=True)
    _data = GwioModelField(PaymentData, load=True)

    # FSM related fields
    _current_state = FsmEnumField(enum=PaymentState, fsm_enum=FsmState, missing=PaymentState.NOT_STARTED)
    _transition_history = GwioListField(PaymentTransitionEvent)


class Payment(FsmModel, NestedSchemaModel):
    Schema = PaymentSchema

    _data_attrs = {
        PaymentTypes.STRIPE: 'stripe',
        PaymentTypes.PAYTRAIL: 'paytrail',
        PaymentTypes.PAYU: 'payu',
        PaymentTypes.COD: 'cod',
    }

    # FSM related attributes
    _fsm_handler = PaymentHandler()

    class API(FsmModel.API):
        PAYMENT_METHODS = {
            PaymentTypes.STRIPE: gw_stripe,
            PaymentTypes.PAYTRAIL: paytrail,
            PaymentTypes.PAYU: payu,
            PaymentTypes.COD: cod,
            # PaymentTypes.INVOICE: invoice,
        }

        Type = PaymentTypes

        @classmethod
        def notify(cls, *, model: 'Payment', notification: PaymentNotificationType, **_):
            """Handle notifications triggered by state changes for the payment"""
            try:
                user = gwio.user.User.by_identity_id(model.order.buyer_identity_id)
            except NotFound:
                from gwio.user.user import NonLoggedInUser

                user = NonLoggedInUser(identity_id=model.order.buyer_identity_id,
                                       email=model.order.billing_address.email)
            context = dict(
                payment=model,
                user=user,
                shop=[webshops.Webshop.get(guid=shop_guid) for shop_guid in model.order.shop_guids],
            )
            cls._notify(notification=notification, context=context)

    @property
    def data(self):
        data = getattr(self._data, self._data_attrs[self.payment_type])
        if data is None:
            data = dict()
            self.data = data
        return data

    @data.setter
    def data(self, value):
        setattr(self._data, self._data_attrs[self.payment_type], value)

    @property
    def last4(self):
        return self.data.last4

    @property
    def client_data(self):
        return self.data.client_data

    @property
    def status_manual_capture(self):
        "This is used when the shop captures the payment out of band and separately marks the payment explicitly paid"
        log.warning('Manual capture payment method not implemented yet')
        return False

    @property
    def status_paid(self) -> bool:
        """Payment has been captured/collected"""

        def _paytrail_paid(payment):
            if payment.data.notify:
                return True
            return False

        def _stripe_paid(payment):
            if payment.data.charge_capture:
                return True
            return False

        def _payu_paid(payment):
            # TODO: Probably not like this in the end
            try:
                if payment.data.completed_data['order']['status'] == 'COMPLETED':
                    return True
            except (TypeError, KeyError):
                pass
            return False

        def _cod_paid(payment):
            return payment.data.paid is not None

        _fns = {
            PaymentTypes.PAYTRAIL: _paytrail_paid,
            PaymentTypes.STRIPE: _stripe_paid,
            PaymentTypes.PAYU: _payu_paid,
            PaymentTypes.COD: _cod_paid,
        }
        # If current state if `COMPLETED` the payment is definitely already paid
        if self.current_state == PaymentState.COMPLETED:
            return True
        # Otherwise need to check payment method specific logic
        try:
            return _fns[self.payment_type](self)
        except KeyError:
            # TODO: The except should be removed once support for all the existing payment types has been implemented
            log.error('No `status_paid` support for payment type `%s`', str(self.payment_type))
            return False
        except AttributeError as e:
            log.exception(e)
            return False

    @property
    def status_refunded(self):
        return self.data.status_refunded

    @property
    def status_sca_required(self):
        return self.data.status_sca_required

    @property
    def status_sca_successful(self):
        return self.data.status_sca_successful

    @property
    def timestamps(self):
        class Timestamps():
            order = None

            def __init__(self, order):
                self.order = order

            def _find_transition_timestamp(self, *conditions):
                previous_event = None
                for event in self.order._transition_history:  # pylint: disable=protected-access
                    if all(x(previous_event, event) for x in conditions):
                        return event.timestamp
                    previous_event = event
                return None

            @property
            def paid(self):
                return self._find_transition_timestamp(lambda prev_event, event: event.state == PaymentState.COMPLETED)

        return Timestamps(self)

    class ProcessLogic:
        @staticmethod
        def charged(*, model: 'Payment', **_):
            model.process(PaymentAction.CAPTURE)

        @staticmethod
        def waiting_payment(*, model: 'Payment', **_):
            try:
                model.API.PAYMENT_METHODS[model.payment_type].finalize_payment(payment=model)
            except Exception as e:
                log.exception(e)

        @staticmethod
        def completed(*, model: 'Payment', action: PaymentAction, **_):
            # Generate coupons if necessary
            order = model.order
            user = order.buyer
            # Only for logged in users
            if isinstance(user, gwio.user.User):
                for active_promotion in promotions.Promotion.scan():
                    if active_promotion.check_order_validity(order):
                        coupon = active_promotion.generate_coupon_from_order(user)

                        order.API.notify(
                            action=action,
                            model=order,
                            notification=OrderNotificationType.NEW_COUPON,
                            ctx=dict(
                                coupon=coupon,
                            ),
                        )

            # Try to move order to completed
            model.order.process(OrderAction.COMPLETE)

        @staticmethod
        def waiting_refund(*, model: 'Payment', **_):
            model.API.PAYMENT_METHODS[model.payment_type].refund(refund_payment=model)

    @property
    def order(self):
        return self._parent_model

    def create_charge(self):
        self.API.PAYMENT_METHODS[self.payment_type].create_charge(payment=self)

    def new_refund(self, amount: Decimal, webshop_guid: uuid.UUID = None) -> None:
        """
        Create a new refund payment
        :param amount: Amount to refund
        :param webshop_guid: Webshop from which to retrieve the refund amount
        """
        refund_payment = Payment(
            amount=amount * -1,
            payment_type=self.payment_type,
            currency=self.currency,
            webshop_guid=webshop_guid,
        )
        # Can not set during init because we have bit of our own magic in the way
        if self.payment_type == PaymentTypes.STRIPE:
            refund_payment.data = dict(intent_id=self.data.intent_id)
        elif self.payment_type == PaymentTypes.PAYU:
            refund_payment.data = dict(order_id=self.data.order_id)
        self.order.payments.append(refund_payment)
        refund_payment.process(PaymentAction.REFUND)

    @property
    def money_distribution(self) -> AttrDict:
        """
        Calculates and returns all the different ways the money is being distributed (platform owner's cut, payment handler's cut, delivery fee, etc.)
        """

        @cached(ttl=5)
        def _get_webshop(guid: uuid.UUID) -> webshops.Webshop:
            """
            Get (possibly cached) `Webshop` object
            :param guid: UUID of the webshop to retrieve
            :return: The `Webshop` object
            """
            return webshops.Webshop.get(guid=str(guid))

        try:
            payment_commission_by_shop = self.API.PAYMENT_METHODS[self.payment_type].get_platform_payment_commission(self)

            distributions_by_shop = AttrDict()
            for shop_summary in self.order.summary.shops:
                shop_distributions = AttrDict(
                    total=shop_summary.price,
                    currency=shop_summary.currency,
                    vats={x.vat: x.amount for x in shop_summary.vats}
                )

                # Dividing the payment handler commission between shops
                shop_distributions['payment_handling'] = payment_commission_by_shop[shop_summary.shop_guid]
                # TODO: What to do when roundings cause payment_handler_commission != sum(x['payment_handler] for _, x in shops_amounts)

                try:
                    webshop = _get_webshop(shop_summary.shop_guid)
                except webshops.Webshop.DoesNotExist:
                    log.warning('Cannot create transfer for shop %s in order %s', str(shop_summary.shop_guid), str(self.order.guid))
                    continue

                webshop_payment_type_private_data = getattr(webshop._private_data, self.payment_type.value)
                try:
                    platform_commission_percent = Decimal(webshop_payment_type_private_data.commission_percentage) / 100
                except AttributeError:
                    raise ValueError(f'Webshop `{webshop.guid}` does not have platform commission percent set for payment type `{self.payment_type.value}`')

                if not (0 <= platform_commission_percent < 1):
                    log.error('Platform commission %d. Expected 0 <= platform commission < 1', platform_commission_percent)
                    raise ValueError('Bad value for platform commission')

                # Calculated with the smallest denominator
                platform_commission = shop_distributions['total'] * platform_commission_percent

                shop_distributions['platform_fee'] = platform_commission
                shop_distributions['shop_profit'] = shop_distributions['total'] - platform_commission - shop_distributions['payment_handling']
                shop_distributions['webshop_object'] = webshop

                distributions_by_shop[shop_summary.shop_guid] = shop_distributions

            payment_handler_commission, payment_handler_currency = self.API.PAYMENT_METHODS[self.payment_type].get_payment_platform_commission(self)
            try:
                delivery_fee = AttrDict(
                    amount=self.order.summary.delivery_fee.price,
                    vats={x.vat: x.amount for x in self.order.summary.delivery_fee.vats},
                    currency=self.order.summary.delivery_fee.currency,
                )
            except AttributeError:
                delivery_fee = None

            return AttrDict(
                payment_handler=payment_handler_commission,
                payment_handler_currency=payment_handler_currency,
                shops=distributions_by_shop,
                platform=AttrDict(
                    delivery_fee=delivery_fee,
                ),
            )
        except Exception as e:
            # To find out what's causing the error in lambda
            report_exception(e)
            raise e
