from enum import Enum

from gwio.ext.decorators import singleton
from gwio.utils import fsm
from gwio.utils.fsm import (
    FsmAction,
    FsmActionMixIn,
    FsmAssessment,
    FsmEnumMixIn,
    FsmNotification,
    FsmState,
    FsmStatus,
    FsmTransition,
    _FsmStateMixIn,
    _FsmStatusMixIn,
)


class PackageAction(FsmActionMixIn):
    START = 'start'
    PACK = 'pack'
    CONFIRM = 'confirm'


class PackageState(_FsmStateMixIn):
    NOT_STARTED = 'not_started'
    WAITING_PACKING = 'waiting_packing'
    WAITING_DELIVERY = 'waiting_delivery'
    DELIVERED = 'delivered'


class PackageStatus(_FsmStatusMixIn):
    pass


_transitions = [
    FsmTransition(src=PackageState.NOT_STARTED, on=PackageAction.START, dst=PackageState.WAITING_PACKING, status=FsmStatus.ANY),

    FsmTransition(src=PackageState.WAITING_PACKING, on=PackageAction.PACK, dst=PackageState.WAITING_DELIVERY, status=FsmStatus.ANY),

    FsmTransition(src=PackageState.WAITING_DELIVERY, on=PackageAction.CONFIRM, dst=PackageState.DELIVERED, status=FsmStatus.ANY),
]


class PackageAssessment(FsmEnumMixIn):
    PACKAGING = 'shipping'


_assessments = [
    FsmAssessment(state=PackageState.WAITING_PACKING, via=FsmState._ANY, by=FsmAction._ANY, do=PackageAssessment.PACKAGING, ctx=None),  # Update packaging list
]


class PackageNotificationType(Enum):
    NEW_DELIVERY = 'new-delivery'
    ORDER_READY_FOR_PICKUP = 'ready-for-pickup'


_notifications = [
    FsmNotification(
        state=PackageState.WAITING_PACKING,
        via=FsmState._ANY,
        by=FsmAction._ANY,
        do=PackageNotificationType.NEW_DELIVERY,
        ctx=dict(fail_safe=True),
    ),
    # FsmNotification(
    #     state=PackageState.WAITING_DELIVERY,
    #     via=FsmState._ANY,
    #     by=FsmAction._ANY,
    #     do=PackageNotificationType.ORDER_READY_FOR_PICKUP,
    #     ctx=dict(fail_safe=True),
    # ),
]


@singleton
class PackageHandler(fsm.FsmHandler):
    _initial_state = PackageState.NOT_STARTED
    _action_enum = PackageAction
    _state_enum = PackageState
    _status_enum = PackageStatus

    def __init__(self):
        super().__init__(
            transitions=_transitions,
            assessments=_assessments,
            notifications=_notifications,
        )
