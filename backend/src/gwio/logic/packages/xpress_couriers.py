import enum
from dataclasses import dataclass

from gwio.models.products.base import PRODUCT_TYPE_DEFAULT


@dataclass
class Shipment:
    class ShippingMethod(enum.IntEnum):
        DIRECT_TO_CUSTOMER = 1
        VIA_CENTRAL_HUB = 2  # Warsaw
        VIA_LOCAL_HUB = 3

    method: ShippingMethod
    products: list


def by_weight(p):
    return p['weight']


def shipping_process(order):
    products = sorted((product for product in order['product_lines'] if product['type'] == PRODUCT_TYPE_DEFAULT), key=by_weight, reverse=True)
    number_of_malls = len(set(shop['location_guid'] for shop in order['summary']['shops']))
    shipping_methods = []
    single_shop_ordered_products = []
    multiple_shops_ordered_products = []
    multiple_malls_ordered_products = []

    if len(products) == 1:
        return [Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, products)]
    while products:
        product = products.pop(0)
        if product['weight'] > 50:
            shipping_methods.append(Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [product]))
        elif product['weight'] + sum(p['weight'] for p in products) > 50:
            shipping_methods.append(Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [product]))
        elif product['price']['final']['total'] > 1000:
            shipping_methods.append(Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [product]))
        elif any(v > 120 for v in product['size'].values()):
            shipping_methods.append(Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [product]))
        elif len(order['shop_guids']) > 1:
            if number_of_malls == 1:
                multiple_shops_ordered_products.append(product)
            else:
                multiple_malls_ordered_products.append(product)
        else:
            single_shop_ordered_products.append(product)

    if single_shop_ordered_products:
        shipping_methods.append(Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, single_shop_ordered_products))
    if multiple_shops_ordered_products:
        shipping_methods.append(Shipment(Shipment.ShippingMethod.VIA_LOCAL_HUB, multiple_shops_ordered_products))
    if multiple_malls_ordered_products:
        shipping_methods.append(Shipment(Shipment.ShippingMethod.VIA_CENTRAL_HUB, multiple_malls_ordered_products))

    return shipping_methods
