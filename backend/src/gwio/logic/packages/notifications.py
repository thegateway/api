from gwio.utils.notify import (
    FromShopMixIn,
    UserNotification,
)
from gwio.logic.packages.handler import PackageNotificationType


class OrderReadyForPickupUserNotification(FromShopMixIn, UserNotification):
    type = PackageNotificationType.ORDER_READY_FOR_PICKUP
    template_base_name = 'order_ready_for_pickup_user'
