# -*- coding: utf-8 -*-


from gwio.logic.orders.types import OrderNotificationType
from gwio.utils.notify import (
    FromShopMixIn,
    ShopNotification,
    UserNotification,
    FromSystemMixin,
)


class OrderReceivedUserNotification(FromShopMixIn, UserNotification):
    type = OrderNotificationType.ORDER_RECEIVED
    template_base_name = 'order_received_user'


class OrderPaidUserNotification(FromShopMixIn, UserNotification):
    type = OrderNotificationType.ORDER_PAID
    template_base_name = 'order_paid_user'


class OrderCancelledUserNotification(FromShopMixIn, UserNotification):
    type = OrderNotificationType.ORDER_CANCELLED
    template_base_name = 'order_cancelled_user'


class OrderReceivedShopNotification(FromSystemMixin, ShopNotification):
    type = OrderNotificationType.ORDER_RECEIVED
    template_base_name = 'order_received_shop'


class OrderPaidShopNotification(FromSystemMixin, ShopNotification):
    type = OrderNotificationType.ORDER_PAID
    template_base_name = 'order_paid_shop'


class OrderCancelledShopNotification(FromSystemMixin, ShopNotification):
    type = OrderNotificationType.ORDER_CANCELLED
    template_base_name = 'order_cancelled_shop'


class NewCouponNotification(FromShopMixIn, UserNotification):
    type = OrderNotificationType.NEW_COUPON
    template_base_name = 'new_coupon_user'
