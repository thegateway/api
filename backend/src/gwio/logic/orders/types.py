# -*- encoding: utf-8 -*-
"""
Configuration for the OrderProcessing

This basically holds the OrderStates to avoid circular import (as it is need both by the
Order(Model) and OrderHandling(Machine))
"""
from enum import Enum

# To avoid typos and give linter a chance to catch errors let's just define everything via enums
from gwio.utils.fsm import (
    FsmActionMixIn,
    FsmEnumMixIn,
    _FsmStateMixIn,
    _FsmStatusMixIn,
)


class OrderState(_FsmStateMixIn):
    ARCHIVED = 'archived'
    CANCELLED = 'cancelled'
    CREATED = 'created'
    PROCESSING = 'processing'
    START = 'start'
    COMPLETED = 'completed'


class OrderStatus(_FsmStatusMixIn):
    DELIVERED = 'delivered'  # Customer has received items or they have at least been shipped out
    INVALID = 'invalid'
    NEED_REVIEW = 'need_review'  # Order needs review
    PAID = 'paid'
    PREPAYMENT = 'prepayment'  # Order must be paid before further processing


class OrderNotificationType(Enum):
    ORDER_CANCELLED = 'order-cancelled'
    ORDER_PAID = 'order-paid'
    ORDER_RECEIVED = 'order-received'
    NEW_COUPON = 'new-coupon'

    def __str__(self):
        return str(self.value)


class OrderAssessment(FsmEnumMixIn):
    PRICES = 'prices'
    BILLING = 'billing'
    COMMISSIONS = 'commissions'
    LOG_PLACEMENT = 'log-placement'


class OrderAction(FsmActionMixIn):
    """Possible order actions"""
    ARCHIVE = 'archive'
    CANCEL = 'cancel'
    COMPLETE = 'complete'
    CREATE = 'create'
    PLACE = 'place'
    UPDATE = 'update'
