# -*- encoding: utf-8 -*-
import logging

from gwio.ext.decorators import singleton
from gwio.utils import fsm
from gwio.utils.fsm import (
    FsmAction,
    FsmAssessment,
    FsmNotification,
    FsmState,
    FsmStatus,
    FsmTransition,
)
from .types import (
    OrderAction,
    OrderAssessment,
    OrderNotificationType,
    OrderState,
    OrderStatus,
)

# from transitions.extensions import GraphMachine as Machine

log = logging.getLogger(__name__)

# pylint: disable=protected-access
_transitions = [
    FsmTransition(src=OrderState.START, on=OrderAction.CREATE, dst=OrderState.CREATED, status=FsmStatus.ANY),

    FsmTransition(src=OrderState.CREATED, on=OrderAction.UPDATE, dst=OrderState.CREATED, status=FsmStatus.ANY),
    FsmTransition(src=OrderState.CREATED, on=OrderAction.ARCHIVE, dst=OrderState.ARCHIVED, status=FsmStatus.ANY),
    # TODO: Test what happens
    FsmTransition(src=OrderState.CREATED, on=OrderAction.PLACE, dst=OrderState.CREATED, status=OrderStatus.INVALID),
    FsmTransition(src=OrderState.CREATED, on=OrderAction.PLACE, dst=OrderState.PROCESSING, status=FsmStatus.ANY),

    FsmTransition(src=OrderState.PROCESSING, on=OrderAction.COMPLETE, dst=OrderState.COMPLETED, status=[OrderStatus.PAID, OrderStatus.DELIVERED]),
    FsmTransition(src=OrderState.PROCESSING, on=OrderAction.COMPLETE, dst=OrderState.PROCESSING, status=FsmStatus.ANY),

    FsmTransition(src=OrderState.COMPLETED, on=OrderAction.ARCHIVE, dst=OrderState.ARCHIVED, status=FsmStatus.ANY),
    FsmTransition(src=OrderState.COMPLETED, on=OrderAction.CANCEL, dst=OrderState.CANCELLED, status=FsmStatus.ANY),

]

# pylint: disable=protected-access
_notifications = [
    FsmNotification(
        state=OrderState.PROCESSING,
        via=OrderState.CREATED,
        by=FsmAction._ANY,
        do=OrderNotificationType.ORDER_RECEIVED,
        ctx=None,
    ),
    FsmNotification(state=OrderState.CANCELLED, via=FsmState._ANY, by=FsmAction._ANY, do=OrderNotificationType.ORDER_CANCELLED, ctx=dict(fail_safe=True)),

    # DO NOT PLAY WITH FIRE. NO NOTIFICATIONS FOR OrderState.FAILED STATE.
]

# pylint: disable=protected-access
_assessments = [
    # User is creating and modifying the order
    FsmAssessment(state=OrderState.CREATED, via=FsmState._ANY, by=[OrderAction.CREATE, OrderAction.UPDATE], do=OrderAssessment.PRICES, ctx=None),
    # Seller has possibly modified the order
    # FsmAssessment(state=OrderState.WAITING_REVIEW, via=FsmState._ANY, by=OrderAction.UPDATE, do=OrderAssessment.PRICES, ctx=None),
    # Do the commission calculations
    FsmAssessment(state=OrderState.ARCHIVED, via=FsmState._ANY, by=FsmAction._ANY, do=OrderAssessment.COMMISSIONS, ctx=None),
    # This the most reliable way to create order placement event log, without doing it multiple times
    FsmAssessment(state=OrderState.PROCESSING, via=OrderState.CREATED, by=OrderAction.PLACE, do=OrderAssessment.LOG_PLACEMENT, ctx=None),
]


# noinspection PyArgumentList
@singleton
class OrderHandler(fsm.FsmHandler):
    _initial_state = OrderState.START
    _action_enum = OrderAction
    _state_enum = OrderState
    _status_enum = OrderStatus

    def __init__(self):
        super().__init__(
            transitions=_transitions,
            notifications=_notifications,
            assessments=_assessments,
        )
