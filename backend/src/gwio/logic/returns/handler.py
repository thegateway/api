from gwio.ext.decorators import singleton
from gwio.logic.returns.notifications import ReturnDeliveryNotificationType
from gwio.utils import fsm
from gwio.utils.fsm import (
    FsmActionMixIn,
    FsmEnumMixIn,
    FsmNotification,
    FsmStatus,
    FsmTransition,
    _FsmStateMixIn,
    _FsmStatusMixIn,
)


class ReturnDeliveryAction(FsmActionMixIn):
    START = 'start'
    CONFIRM_RETURN = 'confirm_return'


class ReturnDeliveryState(_FsmStateMixIn):
    NOT_STARTED = 'not_started'
    WAITING_RETURN = 'waiting_return'
    PARTIALLY_RECEIVED = 'partially_received'
    RECEIVED = 'received'


class ReturnDeliveryStatus(_FsmStatusMixIn):
    FULLY_RETURNED = 'fully_returned'


_transitions = [
    FsmTransition(
        src=ReturnDeliveryState.NOT_STARTED,
        on=ReturnDeliveryAction.START,
        dst=ReturnDeliveryState.WAITING_RETURN,
        status=FsmStatus.ANY
    ),

    FsmTransition(
        src=ReturnDeliveryState.WAITING_RETURN,
        on=ReturnDeliveryAction.CONFIRM_RETURN,
        dst=ReturnDeliveryState.RECEIVED,
        status=ReturnDeliveryStatus.FULLY_RETURNED,
    ),
    FsmTransition(
        src=ReturnDeliveryState.WAITING_RETURN,
        on=ReturnDeliveryAction.CONFIRM_RETURN,
        dst=ReturnDeliveryState.PARTIALLY_RECEIVED,
        status=FsmStatus.ANY,
    ),

    FsmTransition(
        src=ReturnDeliveryState.PARTIALLY_RECEIVED,
        on=ReturnDeliveryAction.CONFIRM_RETURN,
        dst=ReturnDeliveryState.RECEIVED,
        status=ReturnDeliveryStatus.FULLY_RETURNED,
    ),
    FsmTransition(
        src=ReturnDeliveryState.PARTIALLY_RECEIVED,
        on=ReturnDeliveryAction.CONFIRM_RETURN,
        dst=ReturnDeliveryState.PARTIALLY_RECEIVED,
        status=FsmStatus.ANY,
    ),

    FsmTransition(
        src=ReturnDeliveryState.RECEIVED,
        on=ReturnDeliveryAction.CONFIRM_RETURN,
        dst=ReturnDeliveryState.RECEIVED,
        status=FsmStatus.ANY,
    ),
]


class ReturnDeliveryAssessment(FsmEnumMixIn):
    pass


_assessments = []

_notifications = [
    FsmNotification(
        state=ReturnDeliveryState.RECEIVED,
        via=[ReturnDeliveryState.WAITING_RETURN, ReturnDeliveryState.PARTIALLY_RECEIVED],
        by=ReturnDeliveryAction.CONFIRM_RETURN,
        do=ReturnDeliveryNotificationType.PRODUCTS_RECEIVED,
        ctx=None,
    ),
]


@singleton
class ReturnDeliveryHandler(fsm.FsmHandler):
    _initial_state = ReturnDeliveryState.NOT_STARTED
    _action_enum = ReturnDeliveryAction
    _state_enum = ReturnDeliveryState
    _status_enum = ReturnDeliveryStatus

    def __init__(self):
        super().__init__(
            transitions=_transitions,
            assessments=_assessments,
            notifications=_notifications,
        )
