from enum import Enum

from gwio.utils.notify import (
    FromShopMixIn,
    UserNotification,
)


class ReturnDeliveryNotificationType(Enum):
    PRODUCTS_RECEIVED = 'products-received'


class ReturnDeliveryReceivedUserNotification(FromShopMixIn, UserNotification):
    type = ReturnDeliveryNotificationType.PRODUCTS_RECEIVED
    template_base_name = 'return_delivery_received_user'
