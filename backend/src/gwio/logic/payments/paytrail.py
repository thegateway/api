# -*- coding: utf-8 -*-
"""Class operation to make use of external service that provides
Paytrail service.
"""
from hashlib import sha256

from gwio.logic.payments.handler import (
    PaymentAction,
)


def create_charge(**_):
    pass


def finalize_payment(*, payment):
    payment.process(PaymentAction.PAY)


def cancel_charge(**_):
    pass


def send_paytrail_notification(context: dict):
    order = context['order']
    if order.reviewed:
        # TODO: Send the payment link to the customer
        pass


# pylint: disable=R0902
class Paytrail:
    """A simple class to help setup and handle Paytrail Payment E2-related logic."""

    # pylint: disable=R0913, R0914
    # These are setup so that all necessary information can be set during class
    # initialization rather than have properties set one by one.
    def __init__(
            self,
            *,
            merchant_secret,
            merchant_id=None,
            url_success=None,
            url_cancel=None,
            order_number=None,
            amount=None,
            url_notify=None,
            locale=None,
            msg_ui_merchant_panel=None
    ):
        # Seller specific properties
        self.merchant_secret = merchant_secret
        self.merchant_id = merchant_id
        self.currency = 'EUR'
        self.url_success = url_success
        self.url_cancel = url_cancel
        self.order_number = order_number
        self.amount = amount
        self.params_in = ''
        self.params_out = 'ORDER_NUMBER,PAYMENT_ID,AMOUNT,TIMESTAMP,STATUS'
        self.alg = '1'  # '1' is 'SHA-256', used for authcode hashing.
        self.url_notify = url_notify
        self.locale = locale
        self.msg_ui_merchant_panel = msg_ui_merchant_panel

    def generate_payment_form_data_set(self):
        """Will generate a payment form data set to be used as form data for Paytrail payments.
        At the time of this writing, the payment form data goes by Paytrail's version E2.
        :return: Dictionary
        """
        # TODO: MSG_SETTLEMENT_PAYER could be added and filled with Order.statement_description to populate customer's payment statement
        data_set = dict(
            MERCHANT_ID=self.merchant_id,
            CURRENCY=self.currency,
            URL_SUCCESS=self.url_success,
            URL_CANCEL=self.url_cancel,
            ORDER_NUMBER=self.order_number,
            AMOUNT=self.amount,
            PARAMS_OUT=self.params_out,
            ALG=self.alg,
            URL_NOTIFY=self.url_notify,
            LOCALE=self.locale,
            MSG_UI_MERCHANT_PANEL=self.msg_ui_merchant_panel,
        )
        data_set['PARAMS_IN'] = ('MERCHANT_ID,CURRENCY,URL_SUCCESS,URL_CANCEL,ORDER_NUMBER,AMOUNT,PARAMS_IN,PARAMS_OUT,'
                                 'ALG,URL_NOTIFY,LOCALE,MSG_UI_MERCHANT_PANEL')

        data_set['AUTHCODE'] = self.generate_authcode(data_set=data_set)
        return data_set

    # pylint: disable=R0913
    # These arguments are needed to help validate the return response from Paytrail.
    def validate_return_authcode(self, order_number=None, payment_id=None, amount=None, timestamp=None, status=None,
                                 return_authcode=None):
        """Validates if the return authcode is valid with the provided arguments"""
        concatenated_string = '|'.join((order_number, payment_id, amount, timestamp, status, self.merchant_secret))
        return sha256(concatenated_string.encode('utf-8')).hexdigest().upper() == return_authcode

    def generate_authcode(self, data_set):
        """Will generate authcode according to Payment E2 specification. Null or None must be assigned as
        empty string if they are to be included in the hashing logic.
        """
        concatenated_string = self.merchant_secret
        for key in data_set['PARAMS_IN'].split(','):
            v = data_set[key]
            concatenated_string += '|{}'.format(v if v is not None else '')
        return sha256(concatenated_string.encode('utf-8')).hexdigest().upper()
