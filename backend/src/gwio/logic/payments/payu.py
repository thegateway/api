import json
import logging
import uuid
from datetime import (
    datetime,
    timezone,
)
from decimal import Decimal
from typing import Tuple

import flask
import pycountry
from flask import (
    g,
    request,
)

from gwio import user
from gwio.core import cw_logging
from gwio.core.cw_logging import Event
from gwio.core.utils.payu.payu import (
    Payu,
    PayuException,
    PayuOrder,
    PayuSubmerchant,
)
from gwio.environment import Env
from gwio.ext.vat import Currency
from gwio.logic.payments.handler import PaymentAction
from gwio.utils.transactions import (
    Ledger,
    PLATFORM_LEDGER_TABLE,
)

LogEventType = cw_logging.LogEventType
log = logging.getLogger(__name__)


def refund(*, refund_payment):
    payu_order = PayuOrder.get(refund_payment.data.order_id)
    # The refund payment has negative amount, payu expects positive refund amount in smallest increments
    refund_amount = -int(refund_payment.amount * 100)
    params = dict(refund_guid=refund_payment.guid,
                  ext_submerchant_id=refund_payment.webshop_guid,
                  description=f'Refund {refund_payment.order.reference}',
                  amount=refund_amount)
    # So... Payu does not really support submerchants (except predefined hardcoded ones) so testing anything submerchant related is not really possible
    # in the sandbox
    ledger = _prepare_refund_transactions()  # pylint: disable=assignment-from-no-return
    if Env.APPLICATION_ENVIRONMENT == 'production':
        refund_data = payu_order.refund(**params)
    else:
        del params['ext_submerchant_id']
        del params['refund_guid']
        refund_data = payu_order.refund(**params)
    ledger.emit()
    refund_payment.data.refund_data = refund_data
    refund_payment.order.save()
    log.info(f'Refunded PayU order {refund_payment.data.order_id} for order {refund_payment.order.guid} successfully')

    try:
        Event(
            event_type=LogEventType.PAYMENT_REFUND,
            subject=user.User.by_identity_id(refund_payment.order.buyer_identity_id, id_if_user_missing=True),
            obj=refund_payment.order,
            data=json.dumps(refund_data)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the PAYMENT_REFUND refund information "%s" for order %s: %s',
                      payu_order.order_id, refund_payment.order.guid, str(e))
        raise


def cancel_charge(*, payment):
    payu_order = PayuOrder.get(payment.data.order_id)
    refund_data = payu_order.refund(amount=int(payment.amount * 100), description=f'Canceling charge for order {payment.order.guid}')
    payment.data.refund_data = refund_data
    payment.order.save()
    log.info(f'Refunded PayU order {payment.data.order_id} for order {payment.order.guid} successfully')

    try:
        Event(
            event_type=LogEventType.PAYMENT_CANCEL,
            subject=user.User.by_identity_id(payment.order.buyer_identity_id, id_if_user_missing=True),
            obj=payment.order,
            data=json.dumps(refund_data)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the payment cancel information "%s" for order %s: %s',
                      payu_order.order_id, payment.order.guid, str(e))
        raise


def _submerchant_amounts(payment) -> dict:
    """
    Get platform commissions for all the shops in PayU input format
    :param payment: The payment to divide between submerchants
    :return: dict with `shop_guid` as the key and values `amount` and `fee` with corresponding values in cents
    """
    amounts = dict()
    for shop_guid, distributions in payment.money_distribution.shops.items():
        amounts[shop_guid] = dict(
            amount=int(distributions['total'] * 100),
            fee=int(distributions['platform_fee'] * 100),
        )
    return amounts


def create_charge(*, payment):
    """This will not create charge but instead creates a PayU Order that has a redirect url to the payment page"""
    try:
        customer_ip = request.environ['X-Forwarded-For']
    except KeyError:
        log.info('Could not get `X-Forwarded-For` from: %s', request.environ)
        customer_ip = ''
    if isinstance(customer_ip, list):
        try:
            customer_ip = customer_ip[0]
        except IndexError:
            log.warning('X-Forwarded-For is an empty list')
            log.info('Env: %s', request.environ)
            customer_ip = ''
    continue_url = payment.order.data.payu.continue_url or flask.request.host_url
    log.info("Customer IP: %s", customer_ip)

    params = dict(
        amounts=_submerchant_amounts(payment),
        total_amount=int(payment.order.summary.price * 100),
        buyer_id=payment.order.buyer_identity_id,
        currency=payment.currency,
        customer_ip='127.0.0.1',
        description=f'Order {payment.order.reference}',
        order_guid=payment.order.guid,
        continue_url=continue_url,
    )
    card_token = payment.order.data.payu.card_token
    if card_token is not None:  # TODO: card_token?
        params['card_token'] = card_token

    try:
        # So... Payu does not really support submerchants (except predefined hardcoded ones) so testing anything submerchant related is not really possible
        # in the sandbox
        if Env.APPLICATION_ENVIRONMENT == 'production':
            payu_order = PayuOrder.create_submerchants(**params)
        else:
            del params['buyer_id']
            del params['amounts']
            params['amount'] = params.pop('total_amount')
            # pylint: disable=unexpected-keyword-arg
            # pylint: disable=missing-kwoa
            payu_order = PayuOrder.create(**params)
    except PayuException as e:
        log.exception('Failed to create PayU order: %s', e)
        raise
    payment.amount = payment.amount  # Cannot be saved from PayU response as if SCA is required the response does not contain amount
    payment.datetime = datetime.now(timezone.utc)
    payment.data.payu_order_data = payu_order.order_data
    payment.data.order_id = payu_order.order_id
    payment.data.redirect_url = payu_order.redirect_location
    payment.order.save()
    log.info(f'Created PayU order {payment.data.order_id} for order {payment.order.guid} successfully')

    try:
        Event(
            event_type=LogEventType.PAYMENT_RESERVE,
            subject=user.User.by_identity_id(payment.order.buyer_identity_id, id_if_user_missing=True),
            obj=payment.order,
            data=json.dumps(payu_order.order_data)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the payment intialize information "%s" for order %s: %s',
                      payu_order.order_id, payment.order.guid, str(e))
        raise


def finalize_payment(*, payment):
    """Non express accounts do not support manual capture"""


def do_finalize_payment(*, payment, **kwargs):
    ledger = _prepare_transactions(payment)
    ledger.emit()
    try:
        Event(
            event_type=LogEventType.PAYMENT_FINALIZE,
            subject=user.User.by_identity_id(payment.order.buyer_identity_id, id_if_user_missing=True),
            obj=payment.order,
            data=json.dumps(kwargs)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the payment finalize information "%s" for order %s: %s',
                      kwargs['order']['orderId'], payment.order.guid, str(e))
        return '', 200

    payment.data.completed_data = kwargs
    payment.process(PaymentAction.PAY)


def register_form_link(webshop_guid, language: str = None) -> str:
    """
    Get a link to the PayU signup form for a webshop
    :param webshop_guid: UUID of the webshop whose signup link should be created
    :param language: Language code for the form
    :return: str URL to the signup form
    """
    return Payu.get_register_form_url(webshop_guid, language)


def get_merchant_status(webshop_guid: uuid) -> dict:
    return PayuSubmerchant.get(webshop_guid).status()


def get_merchant_balance(webshop_guid: uuid) -> dict:
    submerchant = PayuSubmerchant.get(webshop_guid)
    return submerchant.balance()


def merchant_payout(webshop_guid: uuid, amount: Decimal, currency: 'pycountry.db.Currency', description: str = None, customer_name: str = None) -> dict:
    submerchant = PayuSubmerchant.get(webshop_guid)
    params = dict(amount=amount,
                  currency=currency.alpha_3,
                  payout_id=uuid.uuid4())
    if description is not None:
        params['description'] = description
    if customer_name is not None:
        params['customer_name'] = customer_name
    payout_result = submerchant.payout(**params)
    balance = submerchant.balance()
    return dict(balance=balance,
                payout=payout_result)


def _prepare_transactions(payment):
    ledger = Ledger(source=g.aws_event_source)

    distributions = payment.money_distribution

    # Platform owner receiving the delivery fee
    if distributions.platform.delivery_fee:
        ledger.add_payment(
            payment=payment,
            description=ledger.Description.DELIVERY_FEE,
            amount=distributions.platform.delivery_fee.amount,
            vats=distributions.platform.delivery_fee.vats,
            currency=distributions.platform.delivery_fee.currency,
            receiver=PLATFORM_LEDGER_TABLE,
        )

    # TODO: PAYU INVOICES THESE LATER, SO SHOULD WE REALLY ADD THEM TO LEDGER AT THIS POINT?
    # ledger.add_payment(
    #    order_guid = payment.order.guid,
    #    payment_guid = payment.guid,
    #    amount=distributions.payment_handler,
    #    vats={Env.PAYU_COMMISSION_VAT_PERCENTAGE: distributions.payment_handler * Env.PAYU_COMMISSION_VAT_PERCENTAGE / 100},
    #    currency=distributions.payment_handler_currency,
    #    sender=PLATFORM_LEDGER_TABLE,
    # )

    for shop_guid, shop_distributions in distributions.shops.items():
        # The shop receiving the money from the customer
        ledger.add_payment(
            payment=payment,
            description=Ledger.Description.CUSTOMER_PAYMENT,
            amount=shop_distributions['total'],
            vats=shop_distributions['vats'],
            currency=shop_distributions['currency'],
            receiver=shop_guid,
            platform_cut=shop_distributions['platform_fee'],
        )
        # Reducing the payment handling commission from the shop
        ledger.add_payment(
            payment=payment,
            description=Ledger.Description.PAYMENT_HANDLING_COMMISSION,
            amount=shop_distributions['payment_handling'],
            vats={Env.PLATFORM_COMMISSION_VAT_PERCENTAGE: shop_distributions['payment_handling'] * Env.PLATFORM_COMMISSION_VAT_PERCENTAGE / 100},
            currency=shop_distributions['currency'],
            receiver=PLATFORM_LEDGER_TABLE,
            sender=shop_guid,
        )
    return ledger


def _prepare_refund_transactions():
    ledger = Ledger(source=g.aws_event_source)
    # TODO: Return platform cut?
    # TODO: Can not return PayU cut, but we still need to return the money to the customer. How to mark this in ledger? Which VAT?
    # TODO: Return delivery fee?
    # TODO: Must only return relevant amounts to the relevant shops
    assert ledger  # For lint
    raise NotImplementedError('Need to implement ledger entries')


def get_payment_platform_commission(payment) -> Tuple[Decimal, Currency]:
    return payment.amount * Decimal(Env.PAYU_COMMISSION_PERCENTAGE) / 100, payment.currency


def get_platform_payment_commission(payment) -> dict:
    """
    Calculates the payment handling fees the platform owner charges from the shops
    :param payment: Payment object
    :return: The payment handling fee per shop
    """
    by_shop = dict()
    platform_extra_commission = Decimal(Env.PLATFORM_PAYU_PAYMENT_PROCESSING_COMMISSION) / 100
    for shop_summary in payment.order.summary.shops:
        by_shop[shop_summary.shop_guid] = shop_summary.price * platform_extra_commission
    return by_shop
