from enum import Enum

from gwio.ext.decorators import singleton
from gwio.utils import fsm
from gwio.utils.fsm import (
    FsmActionMixIn,
    FsmStatus,
    FsmTransition,
    _FsmStateMixIn,
    _FsmStatusMixIn,
)


class PaymentAction(FsmActionMixIn):
    CAPTURE = 'capture'
    CHARGE = 'charge'
    PAY = 'pay'
    REFUND = 'refund'


class PaymentState(_FsmStateMixIn):
    CHARGED = 'charged'
    COMPLETED = 'completed'
    NOT_STARTED = 'not_started'
    REFUNDED = 'refunded'
    WAITING_PAYMENT = 'waiting_payment'
    WAITING_REFUND = 'waiting_refund'
    WAITING_SCA = 'waiting_sca'


class PaymentStatus(_FsmStatusMixIn):
    MANUAL_CAPTURE = 'manual_capture'
    PAID = 'paid'
    REFUNDED = 'refunded'
    SCA_REQUIRED = 'sca_required'
    SCA_SUCCESSFUl = 'sca_successful'


_transitions = [
    FsmTransition(src=PaymentState.NOT_STARTED, on=PaymentAction.CHARGE, dst=PaymentState.WAITING_PAYMENT, status=PaymentStatus.MANUAL_CAPTURE),
    FsmTransition(src=PaymentState.NOT_STARTED, on=PaymentAction.CHARGE, dst=PaymentState.WAITING_SCA, status=PaymentStatus.SCA_REQUIRED),
    FsmTransition(src=PaymentState.NOT_STARTED, on=PaymentAction.CHARGE, dst=PaymentState.CHARGED, status=FsmStatus.ANY),
    FsmTransition(src=PaymentState.NOT_STARTED, on=PaymentAction.PAY, dst=PaymentState.COMPLETED, status=PaymentStatus.PAID),
    FsmTransition(src=PaymentState.NOT_STARTED, on=PaymentAction.REFUND, dst=PaymentState.WAITING_REFUND, status=FsmStatus.ANY),

    FsmTransition(src=PaymentState.WAITING_SCA, on=PaymentAction.CHARGE, dst=PaymentState.CHARGED, status=PaymentStatus.SCA_SUCCESSFUl),

    FsmTransition(src=PaymentState.CHARGED, on=PaymentAction.CAPTURE, dst=PaymentState.WAITING_PAYMENT, status=FsmStatus.ANY),

    FsmTransition(src=PaymentState.WAITING_PAYMENT, on=PaymentAction.PAY, dst=PaymentState.COMPLETED, status=PaymentStatus.PAID),

    FsmTransition(src=PaymentState.WAITING_REFUND, on=PaymentAction.REFUND, dst=PaymentState.COMPLETED, status=PaymentStatus.REFUNDED),
]


class PaymentNotificationType(Enum):
    pass


_notifications = []


@singleton
class PaymentHandler(fsm.FsmHandler):
    _initial_state = PaymentState.NOT_STARTED
    _action_enum = PaymentAction
    _state_enum = PaymentState
    _status_enum = PaymentStatus

    def __init__(self):
        super().__init__(
            transitions=_transitions,
            notifications=_notifications,
        )
