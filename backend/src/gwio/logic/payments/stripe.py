import json
import logging
import time
import uuid
from datetime import (
    datetime,
    timezone,
)
from decimal import Decimal
from typing import (
    Any,
    List,
    Tuple,
)

import stripe
from flask import g
from gwio.core.cw_logging import Event
from stripe.error import (
    CardError,
    InvalidRequestError,
    StripeError,
)

from gwio import user
from gwio.core import cw_logging
from gwio.environment import Env
from gwio.ext.vat import Currency
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentState,
)
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.models import (
    payment_attribute,
    webshops,
)
from gwio.utils.transactions import (
    Ledger,
    PLATFORM_LEDGER_TABLE,
)

LogEventType = cw_logging.LogEventType

log = logging.getLogger(__name__)


def get_payment_from_order(order) -> 'payment_attribute.Payment':
    payments = order.payments
    if len(payments) > 1:
        raise NotImplementedError('Multiple payments per order not supported')

    if not payments:
        raise LookupError(f'No payment found for order {order.guid}')

    payment = payments[0]
    if payment.payment_type != PaymentTypes.STRIPE:
        raise ValueError(f'A payment with type {payment.type} was found for order {order.guid} but attempting to find a stripe payment')

    return payment


def refund(*, refund_payment):
    intent_id = refund_payment.data.intent_id
    # The refund payment has negative amount, stripe expects positive refund amount in smallest increments
    refund_amount = int(refund_payment.amount * -1 * 100)
    try:
        # ledger = _prepare_refund_transactions()  NOT IMPLEMENTED
        refund = stripe.Refund.create(api_key=Env.STRIPE_API_KEY, payment_intent=intent_id, amount=refund_amount)
        # ledger.emit()

        try:
            Event(
                event_type=LogEventType.PAYMENT_REFUND,
                subject=user.User.by_identity_id(refund_payment.order.buyer_identity_id),
                obj=refund_payment.order,
                data=json.dumps(refund)
            ).emit()
        except Exception as e:
            log.exception('Failed to log the payment refund information "%s" for order %s: %s', refund.id, refund_payment.order.guid, str(e))

        refund_payment.data.refund = json.dumps(refund)
        refund_payment.process(PaymentAction.REFUND)
        refund_payment.order.save()
    except StripeError as err:
        log.error('Cannot refund stripe intent %s for order %s: %s', intent_id, refund_payment.order.guid, str(err))
        raise


def cancel_charge(*, payment):
    intent_id = payment.data.intent_id
    try:
        # Stripe refund will by default refund the whole charge
        refund = stripe.Refund.create(api_key=Env.STRIPE_API_KEY, payment_intent=intent_id)

        try:
            Event(
                event_type=LogEventType.PAYMENT_REFUND,
                subject=user.User.by_identity_id(payment.order.buyer_identity_id),
                obj=payment.order,
                data=json.dumps(refund)
            ).emit()
        except Exception as e:
            log.exception('Failed to log the payment refund information "%s" for order %s: %s', refund['charge'], payment.order.guid, str(e))

        payment.data.refund = json.dumps(refund)
        payment.process(PaymentAction.REFUND)
        payment.order.save()
    except StripeError as err:
        log.error('Cannot refund stripe intent %s for order %s: %s', intent_id, payment.order.guid, str(err))
        raise


def create_charge(*, payment):
    # pylint does not seem to understand Webshop.get returns a Webshop object
    # pylint: disable=no-member
    card_id = payment.order.data.stripe.card_id
    customer = user.User.by_identity_id(payment.order.buyer_identity_id)
    try:
        intent = _create_stripe_intent(
            amount=int(payment.amount * 100),
            currency=payment.currency.abbr,
            user=customer,
            order_reference=payment.order.reference,
            order_guid=payment.order.guid,
            payment_method_id=card_id,
        )
    except CardError:
        # TODO: handle `stripe.error.CardError: Request req_VfVGGzCI0bNDFQ: Your card was declined.`
        raise
    try:
        payment.data._last4 = stripe.PaymentMethod.retrieve(payment.order.data.stripe.card_id, api_key=Env.STRIPE_API_KEY)['card']['last4']
    except (InvalidRequestError, KeyError) as e:
        log.warning('Could not get stripe payment method last4: %s', str(e))

    try:
        Event(
            event_type=LogEventType.PAYMENT_RESERVE,
            subject=user.User.by_identity_id(payment.order.buyer_identity_id),
            obj=payment.order,
            data=json.dumps(intent)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the payment initialize information "%s" for order %s: %s', intent['id'], payment.order.guid, str(e))

    payment.data.intent_id = intent['id']
    payment.data.client_secret = intent['client_secret']
    payment.data.charge = intent
    if intent.status == 'requires_capture':
        payment.amount = Decimal(intent['amount']) / 100
        payment.datetime = datetime.now(timezone.utc)
    elif intent.status not in payment.data.API.REQUIRES_ACTION_STATUS:
        error_msg = f'Unexpected stripe payment intent status: {intent.status}'
        log.error(error_msg)
        raise StripeError(error_msg)
    payment.process(PaymentAction.CHARGE)
    payment.order.save()


# New stripe intents
def _create_stripe_intent(amount: int,
                          currency: str,
                          user: 'user.User',
                          order_reference: str,
                          order_guid: uuid.UUID,
                          payment_method_id: str):
    metadata = dict(user_identity_id=user.identity_id, order_guid=str(order_guid))
    intent = stripe.PaymentIntent.create(api_key=Env.STRIPE_API_KEY,
                                         amount=amount,
                                         currency=currency,
                                         customer=user.stripe_id,
                                         capture_method='manual',
                                         confirm=True,
                                         metadata=metadata,
                                         payment_method_types=['card'],
                                         payment_method=payment_method_id,
                                         statement_descriptor_suffix=order_reference,
                                         transfer_group=order_guid)
    return intent


def finalize_payment(*, payment: Any):
    if payment.current_state == PaymentState.WAITING_PAYMENT:
        _confirm_payment_intent(payment=payment)
    elif payment.current_state == PaymentState.WAITING_SCA:
        # The payment requires secure customer authentication that will be done after frontend gets the order back with `stripe client secret`.
        # Payment status can stay in WAITING_SCA for now as it will be upated to CHARGED when a webhook request with `amount_capturable_updated` type arrives.
        # The Order will proceed to WAITING_PAYMENT state since there is no money captured yet
        # After updating the payment status to charged in the stripe webhook handling this function will be called again and the payment will be captured.
        pass
    else:
        log.error('Payment for order %s has not been charged. Cannot capture', payment.order.guid)
        raise ValueError('Payment has not been charged. Cannot capture')


def _finalize_successful_payment(*, payment: 'payment_attribute.Payment', intent: dict):
    # Handle post-payment fulfillment
    payment.datetime = datetime.now()
    payment.amount = Decimal(intent['amount']) / 100
    payment.data.charge_capture = intent

    payment.process(PaymentAction.PAY)

    try:
        Event(
            event_type=LogEventType.PAYMENT_FINALIZE,
            subject=user.User.by_identity_id(payment.order.buyer_identity_id),
            obj=payment.order,
            data=json.dumps(intent)
        ).emit()
    except Exception as e:
        log.exception('Failed to log the payment finalize information "%s" for order %s: %s', intent['id'], payment.order.guid, str(e))

    payment.order.save()


def _confirm_payment_intent(*, payment: 'payment_attribute.Payment'):
    payment_intent_id = payment.data.intent_id
    try:
        stripe.api_key = Env.STRIPE_API_KEY
        order_amount = int(payment.order.total_price * 100)
        intent = stripe.PaymentIntent.retrieve(payment_intent_id)
        if intent.get('status') != 'requires_capture':
            log.error('Cannot capture payment for order %s due to stripe intent status being "%s", not "requires_capture"',
                      payment.order.guid, intent.get('status', ''))
            raise StripeError('Cannot capture payment due to stripe intent status')
        ledger = _prepare_transaction_to_platform_owner(payment)
        intent = stripe.PaymentIntent.capture(payment_intent_id, amount_to_capture=order_amount)
    except CardError as e:
        # Raise to user?
        log.warning('Stripe card error: %s', e)
        raise
    except StripeError as e:
        log.warning('Stripe error: %s', e)
        raise

    # (mostly) Copy-paste from stripe documentation
    if intent.status in payment.data.API.REQUIRES_ACTION_STATUS and intent.next_action.type == 'use_stripe_sdk':
        payment.status = PaymentState.WAITING_SCA
        payment.datetime = datetime.now()
        payment.amount = Decimal(intent['amount']) / 100
        payment.data.charge_capture = intent
        payment.order.save()
    elif intent.status == 'succeeded':
        ledger.emit()  # Transaction that gateway captured the payment
        # Transfers are apparently triggered rith as they are created so do capture before transferring funds to shops
        # Optionally `source_transaction` could be used but that makes error handling more difficult
        transfers = _create_stripe_transfers_for_payment_intent(payment, intent.transfer_group)
        payment.data.transfers = transfers
        _finalize_successful_payment(payment=payment, intent=intent)
    else:
        raise StripeError(f'Invalid PaymentIntent status {intent.status} for order{payment.order.guid}')


def _create_stripe_transfers_for_payment_intent(payment: Any, transfer_group: str) -> List[stripe.Transfer]:
    """
    Create Stripe Transfers for all involved webshops to transfer funds from main Stripe account to connected (webshop) accounts

    :param payment: Payment being paid
    :param transfer_group: The related stripe charge's `transfer_group` identifier
    :return: List of the `Transfer` object
    """

    transfers = []
    for shop_guid, shop_distributions in payment.money_distribution.shops.items():
        if all(x == 0 for x in shop_distributions.values()):
            continue

        ledger = _prepare_ledger_transactions_to_shop(
            payment=payment,
            amount=Decimal(shop_distributions['total']),
            platform_commission=shop_distributions['platform_fee'],
            stripe_commission=shop_distributions['payment_handling'],
            currency=payment.order.summary.currency,
            shop_guid=shop_guid,
            vats=shop_distributions['vats'],
        )
        try:
            transfer = stripe.Transfer.create(
                api_key=Env.STRIPE_API_KEY,
                amount=int(shop_distributions['shop_profit'] * 100),
                currency=shop_distributions['currency'].abbr,
                destination=shop_distributions['webshop_object']._private_data.stripe.account_id,
                transfer_group=transfer_group,
                metadata=dict(
                    order_guid=str(payment.order.guid),
                    webshop_guid=str(shop_guid),
                )
            )
            ledger.emit()
            transfers.append(transfer)
        except InvalidRequestError as e:
            log.warning('Could not create stripe transfer for order %s for shop %s (%s): %s',
                        payment.order.reference, shop_distributions['webshop_object'].name, shop_guid, str(e))

    return transfers


def create_custom_account(*, capabilities: List[str] = None, country: str, email: str = None, collect_now: bool = True, **kwargs) -> stripe.Account:
    """Create a Stripe Connected account
    :param capabilities: List of the new account's requested capabilities
    :param country: The country in which the account holder resides, or in which the business is legally established
    :param email: E-mail of the account holder. For identification purposes. Stripe will not send anything to this address
    :param collect_now: True (default) to collect all required information from the customer right away or False to fill the information later
    """
    if capabilities is None:
        capabilities = ['transfers']
    if email is not None:
        kwargs['email'] = email
    return stripe.Account.create(api_key=Env.STRIPE_API_KEY,
                                 country=country,
                                 requested_capabilities=capabilities,
                                 type='custom',
                                 **kwargs)


def custom_account_link(*, account_id: str, success_url: str, failure_url: str, link_type: str):
    """
    Create a link to verify/update the Stripe account information
    :param account_id: Account id for the Stripe Custom account to modify
    :param success_url: Redirect url where to forward the user after successfully returning from the account link
    :param failure_url: Redirect url where to forward the user if the account link filling does not end successfully
    :param link_type: Type of the link to fetch [custom_account_verification, custom_account_update]
    :return:
    """
    response = stripe.AccountLink.create(api_key=Env.STRIPE_API_KEY,
                                         account=account_id,
                                         success_url=success_url,
                                         failure_url=failure_url,
                                         type=link_type)
    return response.url


def accept_tos(*, account_id: str, ip: str, user_agent: str, user: 'user.User', webshop: 'webshops.Webshop'):
    """
    Mark the Stripe Custom account to have accepted the Terms of Service for Stripe
    :param account_id: Account id for the Stripe Custom account for which to mark the TOS accepted
    :param ip: IP address for the browser where the accept request is coming from
    :param user_agent: User agent for the browser where the accept request is coming from
    :param user: GWIO User object of the user doing the accept request
    :param webshop: Webshop whose stripe account is accepting the TOS
    :return:
    """
    result = stripe.Account.modify(api_key=Env.STRIPE_API_KEY,
                                   id=account_id,
                                   tos_acceptance=dict(date=int(time.time()),
                                                       ip=ip,
                                                       user_agent=user_agent))

    Event(
        event_type=LogEventType.ACCEPT_TOS,
        subject=user,
        obj=webshop,
        data=json.dumps(result)
    ).emit()


def add_external_account(*, account_id: str, external_account_token: str):
    """
    Add new external account for the given Stripe account for payouts
    :param account_id: Account for which the connection will be added
    :param external_account_token: Token for the external account
    """
    stripe.Account.create_external_account(api_key=Env.STRIPE_API_KEY,
                                           id=account_id,
                                           external_account=external_account_token)


def requires_action(*, account_id: str) -> dict:
    """
    Get the actions required to enable/continue payouts for the given Stripe account
    :param account_id: The id of the Stripe account
    :return: dict of the requirements as described in https://stripe.com/docs/api/accounts/object#account_object-requirements
    """
    account_info = stripe.Account.retrieve(api_key=Env.STRIPE_API_KEY, id=account_id)
    # requirements has the following keys (https://stripe.com/docs/api/accounts/object#account_object-requirements):
    # current_deadline, currently_due, disabled_reason, eventually_due, past_due, pending_verification
    return account_info.requirements


def _prepare_transaction_to_platform_owner(payment):
    ledger = Ledger(source=g.aws_event_source)
    ledger.add_payment(
        payment=payment,
        description=Ledger.Description.CUSTOMER_PAYMENT,
        amount=payment.order.summary.price,
        vats={x.vat: x.amount for x in payment.order.summary.vats},
        currency=payment.order.summary.currency,
        receiver=PLATFORM_LEDGER_TABLE,
    )
    # Reducing the Stripe commission from the shop
    stripe_commission, stripe_commission_currency = get_payment_platform_commission(payment)  # pylint: disable=assignment-from-no-return
    ledger.add_payment(
        payment=payment,
        description=Ledger.Description.PAYMENT_HANDLING_COMMISSION,
        amount=stripe_commission,
        vats={Env.STRIPE_COMMISSION_VAT_PERCENTAGE: stripe_commission * Env.STRIPE_COMMISSION_VAT_PERCENTAGE / 100},
        currency=stripe_commission_currency,
        sender=PLATFORM_LEDGER_TABLE,
    )
    return ledger


def _prepare_ledger_transactions_to_shop(*, payment, amount, vats, currency, shop_guid, platform_commission, stripe_commission):
    ledger = Ledger(source=g.aws_event_source)
    # The shop receiving the money from us
    ledger.add_payment(
        payment=payment,
        description=Ledger.Description.CUSTOMER_PAYMENT,
        amount=amount,
        vats=vats,
        currency=currency,
        receiver=shop_guid,
        sender=PLATFORM_LEDGER_TABLE,
    )
    # Collecting our cut from the shop
    ledger.add_payment(
        payment=payment,
        description=Ledger.Description.PLATFORM_CUT,
        amount=platform_commission,
        vats={Env.PLATFORM_COMMISSION_VAT_PERCENTAGE: platform_commission * Env.PLATFORM_COMMISSION_VAT_PERCENTAGE / 100},
        currency=currency,
        receiver=PLATFORM_LEDGER_TABLE,
        sender=shop_guid,
    )
    # Collecting back the Stripe commission from the shop, since Stripe collected it before paying the money to us
    ledger.add_payment(
        payment=payment,
        description=Ledger.Description.PAYMENT_HANDLING_COMMISSION,
        amount=stripe_commission,
        vats={Env.STRIPE_COMMISSION_VAT_PERCENTAGE: stripe_commission * Env.STRIPE_COMMISSION_VAT_PERCENTAGE / 100},
        currency=currency,
        receiver=PLATFORM_LEDGER_TABLE,
        sender=shop_guid,
    )
    return ledger


def _prepare_refund_transactions():
    ledger = Ledger(source=g.aws_event_source)
    assert ledger  # For lint
    raise NotImplementedError('Need to implement ledger entries')


def get_payment_platform_commission(payment) -> Tuple[Decimal, Currency]:
    assert payment  # for lint
    raise NotImplementedError('Stripe is not relevant at the moment')
    # res = stripe.BalanceTransaction.retrieve(payment.data.charge_capture.charges.data[0].balance_transaction)
    # return Decimal(res['fee'] / 100),


def get_platform_payment_commission(payment):
    assert payment  # For lint
    raise NotImplementedError('Stripe is not relevant at the moment')
