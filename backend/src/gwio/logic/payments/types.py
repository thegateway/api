"""Types for payment related functionality."""

from enum import Enum


class PaymentTypes(Enum):
    STRIPE = 'stripe'
    PAYTRAIL = 'paytrail'
    PAYU = 'payu'
    COD = 'cod'
    # INVOICE = 'invoice'
