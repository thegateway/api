
def refund(*, refund_payment) -> None:
    raise NotImplementedError


def cancel_charge(*, payment) -> None:
    raise NotImplementedError


def create_charge(*, payment) -> None:
    payment.order.save()


def finalize_payment(*, payment) -> None:
    pass
