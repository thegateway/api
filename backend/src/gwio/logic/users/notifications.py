from gwio.utils.notify import (
    FromSystemMixin,
    UserNotification,
    UserNotificationType,
)


class UserRegistrationNotification(FromSystemMixin, UserNotification):
    type = UserNotificationType.REGISTRATION_THANKS
    template_base_name = 'user_registration_thanks'
