import logging
from dataclasses import dataclass
from decimal import Decimal

import datetime
import envs
import pycountry
import pytz
import requests
from envs import EnvsValueException
from typing import (
    List,
    Optional,
)

import gwio.integrations.xpress_couriers
from gwio.environment import Env
from gwio.integrations.xpress_couriers import (
    Location as XpresscouriersLocation,
    ServiceType,
    COD,
    Parcel,
    OrderResponse,
    ServiceId,
)
from gwio.models.locations import Location
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.webshops import Webshop

log = logging.getLogger(__name__)


class XpressCouriersException(Exception):
    pass


class XpressCouriersLocationException(XpressCouriersException):
    pass


def parse_location_from_description(location: str) -> dict:
    # Zgrupowania AK "Kampinos", 00-001 Warszawa, Polska
    address, postal_code_city, country = location.rsplit(',', 2)
    postal_code, city = postal_code_city.strip().split(' ', 1)
    country = country.strip()
    city = city.strip()
    try:
        # Works only for english country names
        country_code = pycountry.countries.get(name=country).alpha_2
    except AttributeError:
        try:
            country_code = pycountry.countries.get(alpha_2=country).alpha_2
        except AttributeError:
            try:
                # Handle polish country names
                country_code = {
                    'Polska': 'PL',
                    'Finlandia': 'FI',
                }[country]
            except KeyError:
                raise ValueError(f'Unexpected country {country}')
    return dict(
        address=address,
        post_code=postal_code,
        city=city,
        country_code=country_code,
    )


def location_from_webshop(webshop: Webshop) -> XpresscouriersLocation:
    try:
        location = Location.get(guid=webshop.locations[0])  # TODO: Which location?
    except IndexError:
        raise ValueError(f'No location set for shop {webshop.name}')
    return XpresscouriersLocation(
        name=webshop.name,
        is_private_person=False,
        client_no=webshop._private_data.xpress_couriers.client_id,
        client_tax_id=webshop.business_id,
        e_mail=webshop.email,
        **parse_location_from_description(location.description)
    )


def city_from_postal_code(country_code: str, postal_code: str) -> str:
    GOOGLE_GEOCODE_URL = f'https://maps.googleapis.com/maps/api/geocode/json?&key={Env.GOOGLE_MAPS_API_KEY}&components='
    response = requests.get(f'{GOOGLE_GEOCODE_URL}country:{country_code}|postal_code:={postal_code}')
    city_component = None
    for result in response.json()['results']:
        for component in result['address_components']:
            if 'locality' in component['types']:
                city_component = component
                break
    if city_component:
        return city_component['long_name']
    else:
        raise ValueError(f'No matching city found for postal code {postal_code}')


def get_available_delivery_methods(delivery_location: Location, product_list: List[dict], cash_on_delivery: bool, service_type: ServiceType):
    # 89 (K PS) – 24 hours domestic service
    # 81 (L NPS) – 24 hours local service in cities
    #   * Szczecin <-> Szczecin
    #   * Kraków <-> Kraków
    #   * Katowice <-> Katowice
    #   * Wroclaw <-> Wrocław
    #   * Poznań<->Poznań
    #   * Łódź<->Łódź
    # ID 6260 (L N PS) – local 24 hours service Warsaw-Warsaw
    client = gwio.integrations.xpress_couriers.LogisticsAPI(Env.XPRESS_COURIERS_API_URL, debug=True)
    # TODO: How to figure correct size? The dimensions do not have to be (even close to) exact
    small_parcel = Parcel(
        type=Parcel.Type.PACKAGE,
        weight=Decimal("0.2"),
        length=Decimal("40"),
        heigth=Decimal("30"),
        width=Decimal("20"),
    )
    services = {}
    shop_guids = set([p['shop_guid'] for p in product_list])
    for shop_guid in shop_guids:
        if not cash_on_delivery:
            cod = COD(amount=Decimal(0), ret_account_no='')
        else:
            cod = COD(amount=sum(p['amount'] for p in product_list if p['shop_guid'] == shop_guid), ret_account_no='')
        # No consolidation yet.
        # All products are delivered directly from shop to customer.
        # Maybe: Xpress Couriers' "algorithms" should be run here to check if shop's products should be split to multiple orders? Or just when creating order
        shop = Webshop.get(guid=shop_guid)
        webshop_location = location_from_webshop(shop)
        if Env.APPLICATION_ENVIRONMENT == 'production':
            payer_location = webshop_location
        else:
            # Hackety hack.. Xpress couriers supports only preconfigured locations in testing...
            try:
                location_data = envs.env('XPC_PAYER_TEST_LOCATION', var_type='dict')
            except EnvsValueException as e:
                log.exception('Could not load test xpc payer location data from environment `XPC_PAYER_TEST_LOCATION`: %s', str(e))
                raise
            payer_location = XpresscouriersLocation(**location_data)

        response = client.get_available_services(
            service_type=service_type,
            payer=payer_location,  # This should be the shop in reality
            pickup_location=webshop_location,
            delivery_location=delivery_location,
            parcels=[small_parcel],
            cash_on_delivery=cod,
            ready_date=datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(hours=4),  # Not required according to documentation
            insurance_amount=Decimal(0),  # TODO: What's supposed to go here?
        )
        # Append list of available delivery methods
        try:
            # the response might be a dict of `AvailableServices` and `NextWorkingDayAvailableServices`...
            services[shop_guid] = response['available_services']
        except TypeError:
            # ... or a list of services
            services[shop_guid] = response

    return services


@dataclass
class XpressCouriersDeliveryMethod:
    service_type: ServiceType
    service_id: ServiceId
    cash_on_delivery: bool
    delivery_fee: Optional[Decimal] = None

    class XpressCouriersInitDataException(ValueError):
        pass

    @classmethod
    def from_order(cls, order, cash_on_delivery, delivery_method: Product = None) -> 'XpressCouriersDeliveryMethod':
        dm = next(p for p in order.product_lines if p.type == PRODUCT_TYPE_DELIVERY_METHOD)
        if delivery_method is None:
            delivery_method = Product.get(owner_guid=dm.shop_guid, guid=dm.guid)
        try:
            params = dict(
                service_type=ServiceType(delivery_method.data['xpress_couriers']['service_type']),
                service_id=ServiceId(delivery_method.data['xpress_couriers']['service_id']),
                cash_on_delivery=cash_on_delivery,
                delivery_fee=dm.price.final
            )
        except KeyError:
            raise cls.XpressCouriersInitDataException('Not xpress couriers delivery method') from KeyError
        return cls(**params)


def xpress_couriers_location_from_order(order):
    location = XpresscouriersLocation(
        name=order.shipping_address.name,
        address=order.shipping_address.street_address,
        city=city_from_postal_code(order.shipping_address.country_code, order.shipping_address.postal_code),
        post_code=order.shipping_address.postal_code,
        country_code=order.shipping_address.country_code,
        is_private_person=order.customer.business_id is None,
    )
    return location


def parcel_from_order(order):
    small_parcel = Parcel(
        type=Parcel.Type.PACKAGE,
        weight=Decimal("0.2"),
        length=Decimal("40"),
        heigth=Decimal("30"),
        width=Decimal("20"),
    )
    return small_parcel


def create_xpresscouriers_orders(order, xpc_dm: XpressCouriersDeliveryMethod) -> List[OrderResponse]:
    try:
        delivery_location = xpress_couriers_location_from_order(order)
    except ValueError as e:
        raise XpressCouriersLocationException from e
    parcel = parcel_from_order(order)

    client = gwio.integrations.xpress_couriers.LogisticsAPI(Env.XPRESS_COURIERS_API_URL, debug=True)
    shop_guids = set([p.shop_guid for p in order.product_lines if p.type != PRODUCT_TYPE_DELIVERY_METHOD])
    xpc_orders = []
    for shop_guid in shop_guids:
        shop = Webshop.get(guid=shop_guid)
        webshop_location = location_from_webshop(shop)

        if Env.APPLICATION_ENVIRONMENT == 'production':
            payer_location = webshop_location
        else:
            # Hackety hack.. Xpress couriers supports only preconfigured locations in testing...
            try:
                location_data = envs.env('XPC_PAYER_TEST_LOCATION', var_type='dict')
            except EnvsValueException as e:
                log.exception('Could not load test xpc payer location data from environment `XPC_PAYER_TEST_LOCATION`: %s', str(e))
                raise
            payer_location = XpresscouriersLocation(**location_data)

        params = dict(
            payer=payer_location,
            service_type=xpc_dm.service_type,
            service_id=xpc_dm.service_id,
            pickup_location=webshop_location,
            delivery_location=delivery_location,
            ready_date=datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(hours=4),  # Not required according to documentation
            pickup_deadline=datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(days=1, hours=4),
            saturday_delivery=False,
            parcels=[parcel],
            insurance_amount=Decimal(0),
        )

        if xpc_dm.cash_on_delivery:
            shop_summary = next(summary for summary in order.summary.shops if summary.shop_guid == shop_guid)
            # TODO: The delivery methods should be chosen per shop
            # Here we just use the same delivery method price for each XPC order
            amount = Decimal(shop_summary.price).quantize(Decimal('0.01')) + xpc_dm.delivery_fee.unit.amount
            params['cash_on_delivery'] = COD(amount=amount, ret_account_no='')
        else:
            params['cash_on_delivery'] = COD(amount=Decimal(0), ret_account_no='')
        xpc_order = client.create_order(**params)
        xpc_orders.append(xpc_order)

    return xpc_orders
