import boto3
import click

client = boto3.client('route53')


@click.group()
def cli():
    """
    a CLI for route53.
    """
    pass


def get_zone(zone_name):
    if not zone_name.endswith('.'):
        zone_name += '.'
    zones = client.list_hosted_zones()['HostedZones']
    for zone in zones:
        if zone['Name'] == zone_name:
            return zone


@click.argument('zone_name')
@cli.command()
def download_zonefile(zone_name):
    """
    Creates a BIND -like zonefile from a zone.
    """
    with open(f'{zone_name}.txt', 'w') as output:
        for record in generate_records(zone_name):
            output.write(record)


def generate_records(zone_name):
    """
    Helper function to generate a zonefile like entries from ResourceRecordSets
    :param zone_name:
    :return:
    """
    zone_id = get_zone(zone_name)['Id']
    resources = client.list_resource_record_sets(HostedZoneId=zone_id)['ResourceRecordSets']
    max_rec_len = max(*(len(x['Name']) for x in resources))
    for resource in resources:
        name = resource['Name']
        for record in resource.get('ResourceRecords', tuple()):
            yield f"{name:{max_rec_len}}\t{resource['TTL']:8}\t{resource['Type']:8}\t{record['Value']}\n"


if __name__ == '__main__':
    cli()
