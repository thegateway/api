import time
import logging

import boto3
import click

client = boto3.client('dynamodb')
logger = logging.getLogger(__name__)


@click.group()
@click.option('-i', '--interactive', is_flag=True)
@click.option('-f', '--filter')
@click.pass_context
def cli(ctx, interactive, filter):
    """
    A tool to perform operations on DynamoDB tables.
    """
    ctx.ensure_object(dict)
    ctx.obj['INTERACTIVE'] = interactive
    ctx.obj['FILTER'] = filter


@cli.command()
@click.pass_context
def bill_by_request(ctx):
    """
    Set table's billing mode to PAY_PER_REQUEST
    """
    for table in get_tables(ctx.obj['INTERACTIVE'], ctx.obj['FILTER']):
        try:
            billing_mode = table['BillingModeSummary']['BillingMode']
        except KeyError:
            billing_mode = 'PROVISIONED'
        if billing_mode != 'PAY_PER_REQUEST':
            table_name = table['TableName']
            logger.info(f'Setting {table_name} to PAY_PER_REQUEST billing mode')
            while True:
                try:
                    logger.info(f'Updating billing mode on table {table_name}')
                    client.update_table(TableName=table_name, BillingMode='PAY_PER_REQUEST')
                    break
                except Exception as e:
                    if 'LimitExceededException' in str(type(e)):
                        # Wait for a moment before attempting again
                        logger.warn('Limit exceeded. Trying again in a moment')
                        time.sleep(30)
                    else:
                        raise


@cli.command()
@click.pass_context
def delete_temp_tables(ctx):
    """
    Delete tables with name starting with '__TEMP'
    """
    prompt = ctx.obj['INTERACTIVE']
    for table in get_tables(False, ctx.obj['FILTER']):
        table_name = table['TableName']
        if table_name.startswith('__TEMP') and prompt and click.confirm(f'{table_name}'):
            logger.info(f'Deleting table {table_name}')
            client.delete_table(TableName=table_name)


@cli.command()
@click.pass_context
def delete_tables(ctx):
    """
    Delete all tables. Always interactive.
    """
    for table in get_tables(True, ctx.obj['FILTER']):
        table_name = table['TableName']
        if click.prompt('Enter the table name to delete it:') == table_name:
            client.delete_table(TableName=table_name)


@cli.command()
@click.pass_context
def list_tables(ctx):
    """
    List all tables.
    """
    for table in get_tables(False, ctx.obj['FILTER']):
        table_name = table['TableName']
        print(table_name)


@cli.command()
@click.pass_context
def enable_backups(ctx):
    """
    Enable point-in-time backups for tables with 'production' in their name.
    """
    for table in get_tables(ctx.obj['INTERACTIVE'], ctx.obj['FILTER']):
        table_name = table['TableName']
        if 'production' in table_name:
            try:
                enabled = table['PointInTimeRecoverySpecification']['PointInTimeRecoveryEnabled']
                if enabled:
                    continue
            except KeyError:
                pass
            logger.info(f'Enabling point-in-time backups for {table_name}')
            response = client.update_continuous_backups(
                TableName=table_name,
                PointInTimeRecoverySpecification={
                    'PointInTimeRecoveryEnabled': True
                }
            )
            try:
                status = response['ContinuousBackupsDescription']['ContinuousBackupsStatus']
                if status != 'ENABLED':
                    raise KeyError
            except KeyError:
                logger.fatal(f'Failed with: {response}')


def get_tables(prompt, filter=None):
    """
    A helper function to get table descriptions.
    """
    tables_info = client.list_tables()
    while True:
        for table_name in (t for t in tables_info.get('TableNames', list()) if filter is None or filter in t):
            table = client.describe_table(TableName=table_name)
            if not prompt or click.confirm(f'{table_name}'):
                yield table['Table']
        exclusive_start = tables_info.get('LastEvaluatedTableName')
        if exclusive_start is None:
            break
        tables_info = client.list_tables(ExclusiveStartTableName=exclusive_start)


if __name__ == '__main__':
    # pylint: disable=E1120, E1123
    cli(obj={})
