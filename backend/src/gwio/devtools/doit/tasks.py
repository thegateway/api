# -*- coding: utf-8 -*-
from .helpers import (check_gwio_upgrade, checklist, checkmodule,
                      message_devops, rm_all_dirs, rm_glob, rm_tree_glob, run_lint,
                      run_sphinx_apidoc, shell_python)


__all__ = ['task_build', 'task_build_wheel', 'task_clean_all', 'task_clean_build', 'task_clean_dist',
           'task_clean_pyc', 'task_pycodestyle', 'task_pyflake8', 'task_pyflakes', 'task_lint', 'task_safety', 'task_wheel',
           'task_check', 'task_coverage', 'task_mrproper', 'task_pylint', 'task_message', 'task_upgwio',
           'task_docs', 'task_nocoverage', 'task_nocoverage_check', 'DOIT_CONFIG']

DOIT_CONFIG = dict(default_tasks=['wheel'])


def task_pycodestyle():
    return dict(actions=[shell_python('-m', 'pycodestyle', '--config', 'setup.cfg', *checklist())], verbosity=2)


def task_pyflake8():
    return dict(actions=[shell_python('-m', 'flake8', *checklist())], verbosity=2)


def task_pyflakes():
    return task_pyflake8()


def task_lint():
    return dict(actions=[(run_lint, [checkmodule()])], verbosity=2, task_dep=['pycodestyle', 'pyflake8'])


def task_pylint():
    return dict(actions=None, task_dep=['lint'])


def task_clean_build():
    return dict(actions=[(rm_tree_glob, ['build/*'])])


def task_clean_dist():
    return dict(actions=[(rm_glob, ['dist/*'])])


def task_clean_pyc():
    return dict(actions=[(rm_all_dirs, ['.', '__pycache__'])])


def task_build_wheel():
    return dict(actions=[shell_python('setup.py', 'bdist_wheel')])


def task_wheel():
    return dict(actions=None, task_dep=['lint', 'clean_build', 'clean_dist', 'build_wheel'])


def task_build():
    return dict(actions=None, task_dep=['wheel'])


def task_safety():
    return dict(actions=['safety check --bare'], verbosity=2)


def task_nocoverage():
    args = ['-m', 'pytest', '-v']
    return dict(actions=[shell_python(*args)], verbosity=2)


def task_coverage():
    args = ['-m', 'pytest', '-v', '--cov-report=html', '--cov=gwio']
    return dict(actions=[shell_python(*args)], verbosity=2)


def task_check():
    return dict(actions=None, task_dep=['clean_pyc', 'safety', 'coverage'])


def task_nocoverage_check():
    return dict(actions=None, task_dep=['clean_pyc', 'safety', 'nocoverage'])


def task_clean_all():
    return dict(actions=None, task_dep=['clean_pyc', 'clean_build', 'clean_dist'])


def task_mrproper():
    return dict(actions=None, task_dep=['clean_all'])


def task_message():
    return dict(actions=[(message_devops,)],
                params=[dict(name='sender', short='u', long='user', default='Random Dude'),
                        dict(name='subject', short='s', long='subject', default='Unknown'),
                        dict(name='message', short='m', long='message', default='Kukkakaali')],
                verbosity=2)


def task_upgwio():
    return dict(actions=[(check_gwio_upgrade,)],
                params=[dict(name='pkg', short='p', long='package', default='core')],
                verbosity=2)


def task_docs():
    return dict(actions=[(run_sphinx_apidoc,)],
                params=[dict(name='docdir', short='d', long='docdir', default='docs'),
                        dict(name='builddir', short='b', long='builddir', default='build'),
                        dict(name='sourcedir', short='s', long='sourcedir', default='gwio'),
                        dict(name='doctype', short='t', long='type', default='html')],
                verbosity=2)
