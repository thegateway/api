import sys

import boto3
import glob
import logging
import os
import re
import s3pypi.__main__ as s3pypi
import shutil
from envs import env
from git import Repo
from pylint.lint import Run
from subprocess import CalledProcessError, check_output, run
from typing import Callable, List, Union

logger = logging.getLogger(__name__)
SNS_TOPIC_ARN = "arn:aws:sns:eu-west-1:430384725534:lambda_flask_errors"


def checkmodule() -> Union[str, None]:
    if os.path.exists('gwio'):
        return 'gwio'
    print('Warning: could not detect "gwio" package.')
    return None


def checklist() -> List[str]:
    """Return list of files and directories to check"""
    pyfiles = glob.glob('*.py')
    pyfiles.append(checkmodule())
    return pyfiles


def run_lint(src: str) -> bool:
    """
    Run pylint over the source code, this is intended to be used in doit task of lint

    This can be used as an action in dodo.py with (run_lint, ['gwio'])
    NOTE! to see the output you should set the verbosity level of the action to 2

    :param src: root directory of the source
    :return:  True if there were no errors detected
    """
    results = Run([src, '--rcfile', 'setup.cfg', '-r', 'yes'], do_exit=False)
    return results.linter.stats['error'] == 0


def message_devops(sender: str = None, subject: str = None, message: str = None) -> bool:
    """
    Send a message to devops channel in slack

    This can be sued as an action in dodo.py with
    (message_devops, [], dict(sender='sender', subject='subject', message='the message))

    :param sender: sender of the message
    :param subject: subject of the message
    :param message: the message itself
    :return: status if the sending succeeded or not
    """
    if sender is None or subject is None or message is None:
        logger.error('Incomplete arguments')
        return False
    try:
        sns = boto3.client('sns')
        response = sns.publish(TopicArn=env('SNS_TOPIC_ARN', SNS_TOPIC_ARN, var_type='string'),
                               Message=message, Subject=subject,
                               MessageAttributes=dict(sender=dict(StringValue=sender, DataType="String")))
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            logger.fatal('Failed to report: {status}'.format(
                status=response['ResponseMetadata']['HTTPStatusCode']))
            return False
    # pylint: disable=W0703
    except Exception as e:
        logger.fatal(repr(e))
        return False
    return True


def rm_tree_glob(pattern):
    for path in glob.glob(pattern):
        shutil.rmtree(path)


def rm_glob(pattern):
    for path in glob.glob(pattern):
        os.remove(path)


def rm_all_dirs(base, dirname):
    caches = list()
    for path, _, _ in os.walk(base):
        if path.endswith(dirname):
            caches.append(path)
    for path in reversed(sorted(caches)):
        shutil.rmtree(path)


def shell_python(*args):
    """Run python using some common flags and the provided arguments.

    This is mainly for not having to redo the -W flags for every single command. Currently we are ignoring
    DeprecationWarnings -> the virtualenv is using deprecated flag for reading
    ImportWarnings -> sphinxcontrib missing __init__(I think the issue is actually with setuptools)
    """
    return ' '.join([sys.executable] + ['-Wonce', '-Wignore::DeprecationWarning', '-Wignore::ImportWarning'] + list(args))


def _publish(pkgname):
    pkg = pkgname.replace('-', '_')
    dist = re.compile(r'.*{pkg}-(?P<version>.*)-(py2\.)?py3-.*'.format(pkg=pkg))
    version = None
    for file in glob.glob('./dist/*.whl'):
        m = dist.match(file)
        if not m:
            continue
        version = m.group('version')
    if version is None:
        raise RuntimeError('Could not determine version for {pkg}'.format(pkg=pkg))
    repo = Repo()
    loghash = repo.head.object.hexsha
    logmsg = repo.head.object.message
    s3pypi.create_and_upload_package(s3pypi.parse_args(['--bucket', 'pypi.gwapi.eu']))
    message_devops(sender='pypi.gwapi.eu', subject='new package',
                   message=('New "{pkgname}" package available\n'
                            'version: {version}\n'
                            'commit: {loghash})\n'
                            '{logmsg}\n').format(version=version, pkgname=pkgname, loghash=loghash, logmsg=logmsg))


def check_gwio_upgrade(pkg):
    def get_version(package):
        try:
            out = check_output([sys.executable, '-m', 'pip', 'list'])
            for pkginfo in out.decode().splitlines():
                if pkginfo.startswith(package):
                    return [x for x in pkginfo.split(' ') if x][-1]
            raise ValueError

        except CalledProcessError as e:
            print('pip freeze failed ({err})'.format(err=e.returncode))
            raise ValueError

    def install(package, version=None):
        try:
            if version is None:
                print('Trying to upgrade "{package}"'.format(package=package))
                run([sys.executable, '-m', 'pip', 'install', '--extra-index-url', 'https://pypi.gwapi.eu/',
                     '--upgrade', '--no-deps', package])
            else:
                print('Trying to install "{package} {version}"'.format(
                    package=package, version=version))
                run([sys.executable, '-m', 'pip', 'install', '--extra-index-url', 'https://pypi.gwapi.eu/',
                     '{pkg}=={version}'.format(pkg=package, version=version)])
            return get_version(package)
        except CalledProcessError as e:
            print('pip install failed ({err})'.format(err=e.returncode))
            raise ValueError

    def successfull_check():
        print('Running check')
        try:
            run([sys.executable, '-m', 'pytest'])
            return True
        except CalledProcessError:
            return False

    gwio_pkg = 'gwio-{pkg}'.format(pkg=pkg)

    try:
        current_version = get_version(gwio_pkg)
        print('"{gwio_pkg}" current version: {current_version}'.format(gwio_pkg=gwio_pkg,
                                                                       current_version=current_version))
    except ValueError:
        print('Cannot determine version of "{gwio_pkg}"'.format(gwio_pkg=gwio_pkg))
        return False
    try:
        new_version = install(gwio_pkg)
        if new_version == current_version:
            print("No upgrade available")
            return False
    except ValueError:
        print('Cannot upgrade "{gwio_pkg}"'.format(gwio_pkg=gwio_pkg))
        return False

    if not successfull_check():
        print("Tests failed, reverting back to version {version}".format(version=current_version))
        version = None
        try:
            version = install(gwio_pkg, current_version)
        except ValueError:
            pass
        if version != current_version:
            print("Failed to recover version, your environment is most likely botched. Sorry")
        else:
            print("Restored original version")
        return False

    print('Upgraded "{pkg}" to {version}'.format(pkg=gwio_pkg, version=new_version))
    print('You should modify setup.py to match.')

    return True


def run_sphinx_apidoc(docdir=None, sourcedir=None, builddir=None, doctype=None):
    def change_to_dir(d: str) -> Callable:
        """Create callable that changes to the target directory (to be used in doc pipeline)"""

        def _chdir():
            os.chdir(d)

        return _chdir

    def run_latexmk():
        """Find the tex file in the current directory and run the standard latexmk on it"""
        try:
            texfile = glob.glob('*.tex')[0]
        except IndexError:
            raise RuntimeError('Could not locate main TeX file for processing')
        run(['latexmk', '-pdf', '-dvi-', '-ps-', texfile])
        try:
            pdf_file = glob.glob('*.pdf')[0]
            print(f'The PDF document is: "{os.getcwd()}/{pdf_file}"')
        except IndexError:
            raise RuntimeError('Failed to create PDF file')

    apidocs = '{d}/apidocs'.format(d=docdir)
    shutil.rmtree(apidocs, ignore_errors=True)
    os.makedirs(apidocs, exist_ok=True)
    staticdir = '{d}/_static'.format(d=docdir)
    shutil.rmtree(staticdir, ignore_errors=True)
    os.makedirs(staticdir, exist_ok=True)
    doctreedir = '{d}/doctrees'.format(d=builddir)
    shutil.rmtree(doctreedir, ignore_errors=True)
    os.makedirs(doctreedir, exist_ok=True)
    targetdir = '{d}/{t}'.format(d=builddir, t=doctype)
    shutil.rmtree(targetdir, ignore_errors=True)
    os.makedirs(apidocs, exist_ok=True)
    out = ''

    _doc_pipeline = {
        "html": [['sphinx-apidoc', '-o', apidocs, sourcedir],
                 ['sphinx-build', '-b', 'html', '-d', doctreedir, docdir, targetdir]],
        "pdf": [['sphinx-apidoc', '-o', apidocs, sourcedir],
                ['sphinx-build', '-b', 'latex', '-d', doctreedir, docdir, targetdir],
                change_to_dir(targetdir),
                run_latexmk]
    }

    try:
        for cmd in _doc_pipeline[doctype]:
            if callable(cmd):
                cmd()
            else:
                run(cmd)
    except KeyError:
        print(f'FAILED: Unsupported document type "{doctype}"')
        return False
    except CalledProcessError as e:
        print(f'FAILED: ({e.returncode})\n{out}')
        return False
    except Exception as e:  # pylint: disable=W0703
        print(f'FAILED: {e!r}')
        return False
    return True


# pylint: disable=E1101
# noinspection PyUnresolvedReferences
def publish():
    return dict(actions=[(_publish, [publish.__pkg_name__])], task_dep=['wheel', 'safety'])


def publisher(name):
    setattr(publish, '__pkg_name__', name)
    return publish
