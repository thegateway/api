import os

import click
from s3pypi.package import Package
from s3pypi.storage import S3Storage

default_wheel_dir = 'dist'
default_bucket = 'pypi.gwapi.eu'


@click.command()
@click.option('--wheelhouse_dir', '-w', default=default_wheel_dir)
@click.option('--bucket', '-b', default=default_bucket)
def main(wheelhouse_dir, bucket):
    click.echo('Publishing wheels')
    click.echo(f'Wheelhouse directory: {wheelhouse_dir}')
    click.echo(f'Bucket: {bucket}')
    for filename in os.listdir(wheelhouse_dir):
        if filename.endswith('.whl'):
            click.echo(filename)
            package_name, package_version, _ = filename.split('-', 2)
            package_name_with_version = f'{package_name}-{package_version}'
            package = Package(package_name_with_version, [filename])
            storage = S3Storage(bucket)

            index = storage.get_index(package)
            if any(p.version == package.version for p in index.packages):
                click.echo(f'Package `{package_name_with_version}` already exists, skipping')
                continue
            index.add_package(package)
            storage.put_package(package)
            storage.put_index(package, index)
            click.echo(f'Uploaded `{package_name_with_version}`')


if __name__ == '__main__':
    main(default_wheel_dir, default_bucket)
