# -*- coding: utf-8 -*-


import datetime


def make_calver(path='.', pre_release=False):
    """
    Construct a sensible calendar version based on last Git commit.
    """

    # These are no module level imports so that this can be imported during setup.py
    import envs
    import git

    head = git.Repo(path).head
    n = datetime.datetime.fromtimestamp(head.object.committed_date)
    try:
        branch_name = head.ref.name
    except TypeError:
        branch_name = envs.env('CI_COMMIT_REF_NAME')
    d = n.replace(hour=0, day=1, minute=0, second=0)
    release = int((n - d).total_seconds() / 60)
    package_name = f'{n.year}.{n.month}.{release}'
    if pre_release:
        package_name = f'{package_name}{branch_name}'
    return package_name
