"""
All environment variables *MUST* be defined here.
"""
import enum
import getpass
import json
import logging
import uuid
from dataclasses import dataclass
from decimal import Decimal
from itertools import zip_longest
from typing import Any

import boto3
import envs
from redis import Redis

logger = logging.getLogger(__name__)


class Store(enum.Enum):
    ENVIRONMENT = enum.auto()
    SECRETS_MANAGER = enum.auto()


_NO_DEFAULT = 'no_default'


@dataclass(eq=True, frozen=True)
class Setting:
    name: str
    store: Store
    var_type: type = str
    default: Any = _NO_DEFAULT


class _Env:
    """
    Class containing all runtime environment variables. os.environ and envs.env MUST NOT be used elsewhere.

    Project should subclass this and add / override attributes.
    """
    FINNISH_PERSON_IDENTITY_CODE_NS = uuid.UUID('6aa7b826-b21d-5aed-8a22-5f3cd74ffb71'),

    SETTINGS = {
        Setting(name='ADMIN_GROUP_NAME', store=Store.ENVIRONMENT),
        Setting(name='AWS_LAMBDA_FUNCTION_NAME', store=Store.ENVIRONMENT, default=None),
        Setting(name='AWS_REGION', store=Store.ENVIRONMENT),
        Setting(name='COGNITO_CLIENT_ID', store=Store.ENVIRONMENT),
        Setting(name='COGNITO_FEDERATED_IDENTITY_POOL_ID', store=Store.ENVIRONMENT),
        Setting(name='COGNITO_POOL_ID', store=Store.ENVIRONMENT),
        Setting(name='DEFAULT_CURRENCY', store=Store.ENVIRONMENT),
        Setting(name='DEFAULT_LANGUAGE', store=Store.ENVIRONMENT),
        Setting(name='DEFAULT_VAT', store=Store.ENVIRONMENT),
        Setting(name='DYNAMODB_HOST', store=Store.ENVIRONMENT, default=None),
        Setting(name='ELASTICSEARCH_HOST', store=Store.ENVIRONMENT),
        Setting(name='ERROR_SNS_TOPIC_ARN', store=Store.ENVIRONMENT),
        Setting(name='FCM_NOTIFICATION_API_KEY', store=Store.SECRETS_MANAGER),
        Setting(name='GOOGLE_MAPS_API_KEY', store=Store.SECRETS_MANAGER),
        Setting(name='MESSAGE_TEMPLATE_LOCATION', store=Store.ENVIRONMENT),
        Setting(name='PAYU_CLIENT_ID', store=Store.SECRETS_MANAGER),
        Setting(name='PAYU_CLIENT_SECRET', store=Store.SECRETS_MANAGER),
        Setting(name='PAYU_COMMISSION_PERCENTAGE', store=Store.ENVIRONMENT, var_type=Decimal),
        Setting(name='PAYU_COMMISSION_VAT_PERCENTAGE', store=Store.ENVIRONMENT, var_type=Decimal),
        Setting(name='PAYU_HOST', store=Store.ENVIRONMENT),
        Setting(name='PAYU_MARKETPLACE_ID', store=Store.SECRETS_MANAGER),
        Setting(name='PAYU_NOTIFY_URL', store=Store.ENVIRONMENT),
        Setting(name='PAYU_POS_ID', store=Store.SECRETS_MANAGER),
        Setting(name='PAYU_SECOND_KEY', store=Store.SECRETS_MANAGER),
        Setting(name='PAYU_SHOP_ID', store=Store.SECRETS_MANAGER),
        Setting(name='PLATFORM_COMMISSION_VAT_PERCENTAGE', store=Store.ENVIRONMENT, var_type=Decimal),
        Setting(name='PLATFORM_PAYU_PAYMENT_PROCESSING_COMMISSION', store=Store.ENVIRONMENT, var_type=Decimal),
        Setting(name='RECAPTCHA_SITE_SECRET', store=Store.SECRETS_MANAGER),
        Setting(name='REDIS_HOST', store=Store.ENVIRONMENT, default=None),
        Setting(name='STRIPE_API_KEY', store=Store.SECRETS_MANAGER),
        Setting(name='STRIPE_COMMISSION_VAT_PERCENTAGE', store=Store.ENVIRONMENT, var_type=Decimal),
        Setting(name='STRIPE_COUNTRY_CODE', store=Store.ENVIRONMENT),
        Setting(name='STRIPE_ENDPOINT_SECRET', store=Store.SECRETS_MANAGER),
        Setting(name='SUPPORT_EMAIL', store=Store.ENVIRONMENT),
        Setting(name='SWAGGER_UI_URL', store=Store.ENVIRONMENT),
        Setting(name='SYNCHRONOUS_ES_INDEXING', store=Store.ENVIRONMENT, default=False, var_type=bool),
        Setting(name='SYSTEM_EMAIL', store=Store.ENVIRONMENT),
        Setting(name='SYSTEM_SECRET_KEY', store=Store.SECRETS_MANAGER),
        Setting(name='TIMEZONE', store=Store.ENVIRONMENT, default='UTC'),
        Setting(name='UPLOAD_BUCKET', store=Store.ENVIRONMENT),
        Setting(name='UPLOAD_LOCATION', store=Store.ENVIRONMENT),
        Setting(name='VAPID_PRIVATE_KEY', store=Store.SECRETS_MANAGER),
        Setting(name='XPRESS_COURIERS_API_PASSWORD', store=Store.SECRETS_MANAGER),
        Setting(name='XPRESS_COURIERS_API_URL', store=Store.ENVIRONMENT),
        Setting(name='XPRESS_COURIERS_API_USERNAME', store=Store.SECRETS_MANAGER),
        Setting(name='XPRESS_COURIERS_CLIENT_NUMBER', store=Store.SECRETS_MANAGER),
    }

    class RedisDatabases(enum.IntEnum):
        CACHE = 0
        TAGS = 1

    def __new__(cls):
        namespace = envs.env("NAMESPACE")
        try:
            cls.APPLICATION_ENVIRONMENT, cls.VERTICAL = namespace.strip('/').split('/')
        except (AttributeError, ValueError):
            logger.fatal(f"Invalid NAMESPACE: {namespace}, falling back to GW_VERTICAL_NAME and APPLICATION_ENVIRONMENT")
            cls.VERTICAL = envs.env('GW_VERTICAL_NAME', '__TEMP__{}'.format(getpass.getuser()))
            cls.APPLICATION_ENVIRONMENT = envs.env('APPLICATION_ENVIRONMENT', getpass.getuser())
        cls._store = None
        return super().__new__(cls)

    def __getattr__(self, item):
        try:
            setting = next(s for s in self.SETTINGS if s.name == item)
        except StopIteration:
            raise AttributeError(f"{item} is not a known parameter")

        if setting.store is Store.ENVIRONMENT:
            # Load environment only variables directly from environment
            s = envs.env(setting.name, setting.default)
            if s is _NO_DEFAULT:
                raise AttributeError(f'Environment variable {item} not set')
            return setting.var_type(s) if s is not None else None

        try:
            try:
                val = self._store[item]
            except TypeError:
                # Not cached yet
                self._load()
                val = self._store[item]
        except KeyError as e:
            # The given variable is not set
            if setting.default and setting.default is not _NO_DEFAULT:
                val = setting.default
            else:
                raise AttributeError(f'"{item}" variable is not set') from e
        return val

    def _load(self):
        self._store = dict()

        # Load settings from Secrets Manager
        secret_settings = [s for s in self.SETTINGS if s.store is Store.SECRETS_MANAGER]
        for setting in secret_settings.copy():
            value = envs.env(setting.name)
            if value is None:
                value = getattr(self, setting.name, None)  # getattr for tests
            if value:
                self._store_var(setting.name, value, setting.var_type)
                secret_settings = [s for s in secret_settings if s.name != setting.name]
        # Only load the keys not set as Environment variables from Secrets Manager
        for key, value in self._secret_load_vars():
            try:
                # Check if the setting has already been loaded from env
                setting = next(s for s in secret_settings if s.name == key)
            except StopIteration:
                # The setting has already been loaded from env. Don't overwrite it
                pass
            else:
                # Not loaded from env. Save
                self._store_var(key, value, setting.var_type)
                secret_settings.remove(setting)
        if secret_settings:
            logger.warning(f'Missing value(s) for Secrets Manager {[s.name for s in secret_settings]}')

    def _secret_load_vars(self):
        # Will get the AWSCURRENT version by default
        secrets_client = boto3.client('secretsmanager')
        try:
            secrets_response = secrets_client.get_secret_value(SecretId=self.NAMESPACE)
        except Exception as e:
            if 'ResourceNotFoundException' in str(e):
                raise KeyError('Secrets Manager variables are not set') from e
            else:
                raise e
        secrets = json.loads(secrets_response['SecretString'])
        return ((k, v) for k, v in secrets.items())

    @staticmethod
    def _grouper(iterable, n, fillvalue=None):
        "Collect data into fixed-length chunks or blocks"
        # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
        args = [iter(iterable)] * n
        return zip_longest(*args, fillvalue=fillvalue)

    def _store_var(self, name, value, var_type):
        try:
            value = var_type(value)
        except KeyError:
            pass
        logger.debug('Storing environment variable "%s"', name)
        self._store[name] = value

    def get(self, *_, **__):
        raise RuntimeError("Only attribute access available. Define defaults in {}".format(self.__class__.__name__))

    __getitem__ = get

    @property
    def NAMESPACE(self):
        return f'/{self.APPLICATION_ENVIRONMENT}/{self.VERTICAL}'

    @property
    def COGNITO_USER_POOL_REGION(self):
        # Some regions (e.g. eu-north-1) don't have Cognito IDP service, so can't use AWS_REGION for cognito
        return self.COGNITO_POOL_ID.split('_', 1)[0]

    def get_redis(self, db: RedisDatabases = RedisDatabases.CACHE) -> Redis:
        return Redis(self.REDIS_HOST, db=db.value)
