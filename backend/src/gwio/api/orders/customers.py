import logging
from functools import partial
from typing import (
    Iterable,
    Tuple,
)

from flask import (
    current_app,
    g,
)
from werkzeug.exceptions import (
    Conflict,
    Forbidden,
    NotFound,
)

from gwio import (
    models,
)
from gwio.api.flask_app import app
from gwio.api.orders.base import (
    api_place_logic,
    api_update_logic,
    basic_endpoint_logic,
)
from gwio.core import rbac
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import (
    GET,
    POST,
    PUT,
)
from gwio.environment import Env
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.logic.returns.handler import ReturnDeliveryAction
from gwio.models import user
from gwio.models.orders import ProductReturn
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.webshop_product import WebshopProduct
from gwio.operations.orders import create_order

UserData = user.UserData
Order = models.orders.Order
Webshop = models.webshops.Webshop

GWIO_SHOP_HEADER_NAME = 'X-TheGw-Shop-GUID'

log = logging.getLogger(__name__)

no_guid_route = '/customers/orders/'
base_route = '/customers/<uuid:identity_guid>/orders/'


def _customer(identity_guid, **_):
    """Requires customer credentials"""
    if not rbac.customer(identity_id=identity_guid):
        raise Forbidden


@app.register_endpoint(
    Order,
    route=base_route,
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=Order.Schemas.CustomerGet(many=True),
    authorisation=_customer,
    autoload=False,
)
def customers_orders_list(identity_guid) -> Iterable[Order]:
    """
    Lists all customer's orders
    """
    identity_id = f'{Env.AWS_REGION}:{identity_guid}'
    # pylint: disable=no-value-for-parameter
    return Order.CustomerIndex.query(buyer_identity_id=identity_id)


@app.register_endpoint(
    Order,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/',
    method=GET,
    index=Order.get_by_customer,
    output_schema=Order.Schemas.CustomerGet,
    authorisation=_customer)
def customers_orders_single(order) -> Order:
    """Get an order by uuid"""
    return basic_endpoint_logic(order=order, func=lambda order: True)


@app.register_endpoint(
    Order,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/anonymous',
    method=GET,
    index=Order.get_by_customer,
    output_schema=Order.Schemas.CustomerGet,
    authorisation=None)
def customers_orders_single_anonymous(order) -> Order:
    """Get an order by uuid"""
    try:
        UserData.get_by_identity_id(order.buyer_identity_id)
    except NotFound:
        return basic_endpoint_logic(order=order, func=lambda order: True)
    else:
        raise NotFound


@app.register_endpoint(
    Order,
    autoload=False,
    route=f'{base_route}',
    method=POST,
    output_schema=Order.Schemas.ShopkeeperGet,  # TODO: Should be different for customer and shopkeeper
    authorisation=(rbac.customer, rbac.shopkeeper, rbac.admin),
)
def webshops_orders_create(customer_name, products, identity_guid) -> Tuple[Order, int]:
    """
    Creates order and triggers CREATE action on it
    """
    if rbac.shopkeeper():
        customer = UserData.get_by_identity_id(identity_guid)
        if customer is None:
            raise NotFound(f'User {identity_guid} not found')
    elif rbac.customer(identity_id=identity_guid):
        customer = g.user
    else:
        raise NotFound(f'No user {identity_guid} found')

    order = create_order(
        customer_name=customer_name,
        customer_pin=g.user.pin,
        products=products,
        customer_identity_id=customer.identity_id)

    g.log_events_util.audit_object = order
    order.save()
    return order, 201


@app.register_endpoint(
    Order,
    autoload=False,
    route=no_guid_route,
    method=POST,
    input_schema=Order.Schemas.PostWithoutLogin,
    output_schema=Order.Schemas.CustomerGet,
    authorisation=None,
)
def anonymous_create_order(*, customer_name, products, **kwargs) -> Tuple[Order, int]:
    """
    Creates order and triggers CREATE action followed by PLACE action on it
    """
    identity_id = current_app.identity_pool.get_id()

    # Create
    order = create_order(
        customer_name=customer_name,
        products=products,
        customer_identity_id=identity_id)

    g.log_events_util.audit_object = order

    # Update
    order = basic_endpoint_logic(order=order, func=partial(api_update_logic, **kwargs))

    # Place
    order = basic_endpoint_logic(order=order, func=partial(api_place_logic))

    try:
        for product_line in order.product_lines:
            if product_line.type != PRODUCT_TYPE_DELIVERY_METHOD:
                webshop_product = WebshopProduct.get(product_guid=product_line.guid, webshop_guid=product_line.shop_guid)
                webshop_product.stock_level -= product_line.qty
                webshop_product.save()
    except Exception as e:
        # It makes no sense (at least currently as the stock tracking feature is under developed) to fail order place if this fails
        log.exception(e)

    return order, 201


@app.register_endpoint(
    Order,
    index=Order.get_by_customer,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/',
    method=PUT,
    output_schema=Order.Schemas.CustomerGet,
    automodify=False,
    authorisation=_customer,
)
def customers_orders_update(order, **kwargs) -> Order:
    """Triggers UPDATE action on the order"""
    res = basic_endpoint_logic(order=order, func=partial(api_update_logic, customer_pin=g.user.pin, **kwargs))
    return res


@app.register_endpoint(
    Order,
    index=Order.get_by_customer,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_place',
    method=PUT,
    output_schema=Order.Schemas.CustomerGet,
    authorisation=_customer,
)
def customers_orders_place(order) -> Order:
    """Triggers PLACE action on the order"""
    order = basic_endpoint_logic(order=order, func=partial(api_place_logic, customer_pin=g.user.pin))

    try:
        for product_line in order.product_lines:
            if product_line.type != PRODUCT_TYPE_DELIVERY_METHOD:
                webshop_product = WebshopProduct.get(product_guid=product_line.guid, webshop_guid=product_line.shop_guid)
                webshop_product.stock_level -= product_line.qty
                webshop_product.save()
    except Exception as e:
        # It makes no sense (at least currently as the stock tracking feature is under developed) to fail order place if this fails
        log.exception(e)

    return order


@app.register_endpoint(
    Order,
    index=Order.get_by_customer,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_archive',
    method=PUT,
    output_schema=Order.Schemas.CustomerGet,
    authorisation=_customer,
)
def customers_orders_archive(order) -> Order:
    """Triggers ARCHIVE action on the order in `created` state"""
    if order.current_state != OrderState.CREATED:
        raise Conflict(f'Order is not in "{OrderState.CREATED.value}" state, can not archive')

    return basic_endpoint_logic(
        order=order,
        func=lambda order: order.process(OrderAction.ARCHIVE),
    )


@app.register_endpoint(
    Order,
    index=Order.get_by_customer,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/product_return',
    method=PUT,
    input_schema=ProductReturn.Schemas.Post,
    automodify=False,
    output_schema=Order.Schemas.CustomerGet,
    authorisation=_customer,
)
def new_product_return(*, order, **inputs) -> Order:
    """
    name: request refund
    description: requests refund for the given products in the order
    """
    order.product_returns.append(inputs)
    product_return = order.product_returns[-1]
    product_return.process(ReturnDeliveryAction.START)
    order.save()
    return order
