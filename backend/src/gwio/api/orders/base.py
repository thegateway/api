from typing import Callable

from flask import g
from werkzeug.exceptions import (
    Conflict,
)

from gwio.core import rbac
from gwio.utils.fsm import FsmState
from gwio.logic.orders.types import (
    OrderAction,
)
from gwio.models.modifiers import ModifierLineSource
from gwio.models.orders import Order
from gwio.operations.orders import update_order


def _shopkeeper_order_access(order):
    return rbac.shopkeeper() and rbac.shopkeeper(order.shop_uuid)


def _customer_order_access(order):
    return rbac.customer() and order.buyer_uuid == g.user.identity_id


def basic_endpoint_logic(*, order: Order, func: Callable) -> Order:
    """Fetches the order and does all the checks common between all the action endpoints"""

    start_state = order.current_state
    func(order)
    order.save()

    # Must save user data to save possible changes to the coupons
    try:
        g.user.data.save()
    except AttributeError:
        # No user for anonymous orders
        pass

    if start_state != order.current_state and FsmState._FAILED == order.current_state:
        raise Conflict('Order processing failed.')

    return order


def api_update_logic(order: Order, **kwargs):
    update_order(order, **kwargs)
    order.process(OrderAction.UPDATE)


def api_place_logic(order: Order, *, customer_pin: str = None):
    order.customer.pin = customer_pin
    if order.status_invalid:
        raise Conflict('Order is invalid for placement.')
    order.process(OrderAction.PLACE)


def api_update_price_modifiers_logic(order: Order, product_guid, modifiers):
    product_line = next(x for x in order.product_lines if x.guid == product_guid)
    product_line.price.original.modifiers = [dict(**x, source=ModifierLineSource.MANUAL) for x in modifiers]
    order.process(OrderAction.UPDATE)
