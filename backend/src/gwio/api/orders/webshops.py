import datetime
import logging
from datetime import timezone
from functools import partial
from typing import (
    Iterable,
)

from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.api.orders.base import (
    api_place_logic,
    api_update_logic,
    api_update_price_modifiers_logic,
    basic_endpoint_logic,
)
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
    PUT,
    authorise,
)
from gwio.core.cw_logging import LogEventType
from gwio.ext.marshmallow import (
    GwioSchema,
    GWIODateTime,
)
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.logic.packages.handler import (
    PackageAction,
)
from gwio.logic.payments.handler import PaymentAction
from gwio.models.orders import Order

log = logging.getLogger(__name__)

base_route = '/webshops/<uuid:shop_guid>/orders/'

_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='shop_guid')


@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/',
    method=GET,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_single(order) -> Order:
    """
    Get an order by uuid
    """
    return basic_endpoint_logic(order=order, func=lambda order: True)


class FilterOrdersInStateSchema(GwioSchema):
    """
    name: order state query
    description: query parameters to filter orders by state and date.
    """
    after = GWIODateTime(missing=None)
    state = fields.String(missing=None)


@app.register_endpoint(
    Order,
    route=base_route,
    method=GET,
    event_type=LogEventType.LIST,
    autoload=False,
    output_schema=Order.Schemas.ShopkeeperGet(many=True),
    input_schema=FilterOrdersInStateSchema,
    authorisation=_admin_or_shopkeeper,
    description=('Lists all orders optionally filtered by query parameters "state" and "after" (time the state has been entered as UTC ISO time). '
                 'Possible states are '
                 f'{", ".join([state.value for state in list(OrderState) if not state.value.startswith("_") and not state.value.startswith("*")])}.')
)
def shops_orders_list(shop_guid, state, after) -> Iterable[Order]:
    """
    Lists all orders
    """
    if state is None:
        if after is not None:
            raise Conflict("`state` is required if `after` is given")
        # pylint: disable=no-value-for-parameter
        return Order.get_by_webshop_guid(webshop_guid=shop_guid)

    try:
        state = OrderState(state)
    except ValueError:
        raise NotFound(f'Unknown state {state}')

    # pylint: disable=no-value-for-parameter
    orders = Order.StateIndex.query(_current_state=state.value, shop_guids__contains=str(shop_guid))
    if after is not None:
        after = after.replace(tzinfo=timezone.utc)
        filtered_orders = []
        for order in orders:
            # pylint: disable=protected-access
            if any(transition for transition in order._transition_history if
                   transition.state == state and transition.timestamp > after):
                filtered_orders.append(order)
        return filtered_orders
    return orders


# noinspection PyUnresolvedReferences
@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/',
    method=PUT,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_update(order, products) -> Order:
    """
    Triggers UPDATE action on the order
    """
    return basic_endpoint_logic(order=order,
                                func=partial(api_update_logic, products=products))


@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_accept',
    method=PUT,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_accept(order) -> Order:
    """
    Triggers ACCEPT action on the order
    """
    return basic_endpoint_logic(order=order,
                                func=lambda order: order.process(OrderAction.ACCEPT))


# noinspection PyUnresolvedReferences
@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_place',
    method=PUT,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_place(order) -> Order:
    """
    Triggers PLACE action on the order
    """
    return basic_endpoint_logic(order=order, func=api_place_logic)


# noinspection PyUnresolvedReferences
@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_review',
    method=PUT,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_review(order) -> Order:
    """
    Triggers REVIEW action on the order
    """
    return basic_endpoint_logic(order=order,
                                func=lambda order: order.process(OrderAction.REVIEW))


# noinspection PyUnresolvedReferences
@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_archive',
    method=PUT,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_archive(order) -> Order:
    """
    Triggers ARCHIVE action on the order
    """
    return basic_endpoint_logic(order=order,
                                func=lambda order: order.process(OrderAction.ARCHIVE))


# noinspection PyUnresolvedReferences
@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/_cancel',
    method=PUT,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def shops_orders_cancel(order) -> Order:
    """
    Triggers CANCEL action on the order
    """
    return basic_endpoint_logic(order=order,
                                func=lambda order: order.process(OrderAction.CANCEL))


@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/product_lines/<uuid:product_guid>/modifiers/',
    method=PUT,
    input_schema=Order.API.update_modifiers,
    output_schema=Order.Schemas.ShopkeeperGet,
    automodify=False,
    authorisation=_admin_or_shopkeeper,
)
def put_order_modifier_lines(order, **kwargs):
    """Updates the modifier lines for the specific product line"""
    return basic_endpoint_logic(order=order,
                                func=partial(api_update_price_modifiers_logic, **kwargs))


@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/packages/<uuid:package_guid>/_<string:action>',
    method=PUT,
    input_schema=None,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def webshop_package_action(*, order, package_guid, action) -> Order:
    """
    Triggers an action on the package
    """
    try:
        package = next(x for x in order.packages if x.guid == package_guid)
    except StopIteration:
        raise NotFound(f'Package {package_guid} does not exist')

    action_error = NotFound(f'Invalid action `{action}`')
    try:
        action = PackageAction(action)
    except ValueError:
        raise action_error

    # Actions that are available for the admin/shopkeeper
    valid_actions = (
        PackageAction.CONFIRM,
        PackageAction.PACK,
    )
    if action not in valid_actions:
        raise action_error

    package.process(action)
    order.save()

    return order


@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'{base_route}<uuid:{Order.Table.hash_key}>/payments/<uuid:payment_guid>/_<string:action>',
    method=PUT,
    input_schema=None,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def webshop_payment_action(*, order, payment_guid, action) -> Order:
    """
    Triggers an action on the payment
    """
    try:
        payment = next(x for x in order.payments if x.guid == payment_guid)
    except StopIteration:
        raise NotFound(f'Payment {payment_guid} does not exist')

    action_error = NotFound(f'Invalid action `{action}`')
    try:
        action = PaymentAction(action)
    except ValueError:
        raise action_error

    # Actions that are available for the admin/shopkeeper
    valid_actions = (
        PaymentAction.PAY,
        # PaymentAction.REFUND,  # TODO: Implement
    )
    if action not in valid_actions:
        raise action_error

    payment.data.paid = datetime.datetime.now(tz=datetime.timezone.utc)
    payment.process(action)
    order.save()

    return order
