# -*- coding: utf-8 -*-
"""
Simple interface for handling pricing modifiers
"""

import logging

from flask import make_response
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
    authorise,
)
from gwio.ext import SYSTEM_GUID
from gwio.ext.marshmallow import GwioSchema
from gwio.models.pricing import Pricing

log = logging.getLogger(__name__)

_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='owner')


@app.register_resource
@app.path_params('uuid:owner', 'uuid:guid')
# pylint: disable=no-self-use
class WebshopPricingsAPI(MethodResource):
    """
    Resource for Pricing
    """

    @app.register_endpoint(
        Pricing,
        index=Pricing.by_guid,
        authorisation=_admin_or_shopkeeper,
    )
    def get(self, pricing, owner):
        """Return shop (webshop_guid) specific pricing rule identified by 'pricing_id'"""
        try:
            if pricing.owner not in [owner, SYSTEM_GUID]:
                raise NotFound
        except Pricing.DoesNotExist:
            raise NotFound
        return pricing

    @app.register_endpoint(
        Pricing,
        authorisation=_admin_or_shopkeeper,
    )
    def post(self, pricing):
        """Create a new pricing rule for shop (webshop_guid)"""
        try:
            Pricing.get(owner=pricing.owner, guid=pricing.guid)
            raise Conflict(description=f'Pricing ({pricing.guid}) already exists')
        except Pricing.DoesNotExist:
            pricing.save()
        return pricing, 201

    @app.register_endpoint(
        Pricing,
        automodify=False,
        authorisation=_admin_or_shopkeeper,
    )
    def put(self, pricing, **kwargs):
        """Update shop specific (webshop_guid) pricing rule identified by 'pricing_id'"""
        try:
            # pylint: disable=no-member
            pricing.modify(**kwargs)
        except Pricing.DoesNotExist:
            raise NotFound
        return pricing

    @app.register_endpoint(
        Pricing,
        authorisation=_admin_or_shopkeeper,
    )
    def delete(self, pricing):
        """Delete shops specific (webshop_guid) pricing rule identified by 'pricing_id'"""
        pricing.delete()
        return make_response('', 204)


class PricingsAllSchema(GwioSchema):
    pricings = fields.Nested(Pricing.Schemas.Get, many=True)


@app.register_endpoint(
    Pricing,
    autoload=False,
    route='/webshop_pricings/<uuid:owner>/_all',
    method=GET,
    output_schema=Pricing.Schemas.Get(many=True),
    authorisation=_admin_or_shopkeeper,
)
def list_available_pricings(owner):
    """Return list of all pricing rules available in the shop"""
    try:
        for pricing in Pricing.query(owner=SYSTEM_GUID):
            yield pricing
    except Pricing.DoesNotExist:
        pass
    try:
        for pricing in Pricing.query(owner=owner):
            yield pricing
    except Pricing.DoesNotExist:
        pass


@app.register_endpoint(
    Pricing,
    query=True,
    route='/webshop_pricings/<uuid:owner>/_list',
    method=GET,
    output_schema=Pricing.Schemas.Get(many=True),
    authorisation=_admin_or_shopkeeper,
)
def list_webshop_pricings(pricing):
    """Return all pricing rules only available in the shop identified by 'webshop_guid'"""
    for p in pricing:
        yield p
