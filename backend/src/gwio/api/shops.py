# coding=utf-8
"""
Endpoints for web shops.
"""
import logging
from enum import Enum

from flask import (
    g,
    jsonify,
    make_response,
)
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Unauthorized,
)

from gwio.core import rbac
from gwio.core.decorators.api import (
    AuditObject,
    GET,
    POST,
)
from gwio.utils.notify import GenericUserNotification
from gwio.core.cw_logging import LogEventType
from gwio.environment import Env
from gwio.ext.marshmallow import (
    EnumField,
    GwioSchema,
)
from gwio.schemas import (
    S3FileSchema,
)
from .flask_app import app

logger = logging.getLogger(__name__)


class NotificationTopic(Enum):
    SHOP_ADVERTISING = 'shop_advertising'
    BRAND_ADVERTISING = 'brand_advertising'
    PERSONALISED_ADVERTISING = 'personalised_advertising'
    LOCATION_BASED_ADVERTISING = 'location_based_advertising'
    ORDER_UPDATES_CUSTOMER = 'order_updates_customer'
    ORDER_UPDATES_SHOPKEEPER = 'order_updates_shopkeeper'
    TESTING = 'testing'


class NotificationTopics(GwioSchema):
    """
    name: topics
    description: list of notification topics.
    """
    topics = fields.List(EnumField(enum=NotificationTopic, by_value=True), many=True, required=True)


def check_customer_access(user, **_):
    """Requires admin or customer credentials"""
    if not (rbac.admin() or rbac.customer(user.guid)):
        raise Unauthorized


@app.register_endpoint(
    AuditObject.SELF,
    automodify=False,
    route='/notifications/topics',
    method=POST,
    event_type=LogEventType.UPDATE,
    output_schema=S3FileSchema(many=True),
    input_schema=NotificationTopics,
    authorisation=check_customer_access,
    description=f"""Subscribe to notification topics.

Supported topics: {', '.join([topic.value for topic in list(NotificationTopic)])}.""",
)
def subscribe_to_notification_topic(user, topics):
    user.add_notification_topic(map(lambda x: x.value, topics))
    return make_response('', 204)


@app.register_endpoint(
    AuditObject.SELF,
    route='/notifications/topics',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=None,
    authorisation=check_customer_access,
)
def get_notification_topics(user):
    """
    summary: List subscribed notification topics
    description: Get a list of all subscribed notification topics
    """
    return jsonify(user.notifications.get('topics', list()))


class TestNotification(GwioSchema):
    """
    name: test notification
    description: a test notification to be sent.
    """
    topic = fields.String(required=True)
    message_title = fields.String(required=True)
    message_body = fields.String(required=True)
    message_icon = fields.String()


class Testing:
    pass


# TODO: testing endpoint,should be removed at some point
@app.register_endpoint(
    Testing,
    autoload=False,
    route='/notify/',
    method=POST,
    output_schema=None,
    input_schema=TestNotification,
    authorisation=rbac.ALL,
)
def send_notification(topic, message_title, message_body, message_icon=None):
    """For testing notifications"""
    check_customer_access(user=g.user)

    context = dict(
        user=g.user,
        source=Env.SYSTEM_EMAIL,
        topic=topic,
        title=message_title,
        body=message_body,
        icon=message_icon)
    g.user.notify(GenericUserNotification(context))
    return make_response('', 204)
