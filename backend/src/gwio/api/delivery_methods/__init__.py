# -*- coding: utf-8 -*-
from gwio.models.products import Product

# This API only works if the Product uses BaseProductMixIn and has the appropriate index
if hasattr(Product, 'TypeIndex'):
    from ._api import list_all_delivery_methods, DeliveryMethodsAdminAPI

    assert list_all_delivery_methods
    assert DeliveryMethodsAdminAPI
