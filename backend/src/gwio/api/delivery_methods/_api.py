# -*- coding: utf-8 -*-
import logging

from flask import (
    g,
    make_response,
)
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
    PUT,
    authorise,
)
from gwio.core.cw_logging import LogEventType
from gwio.ext.marshmallow import GwioSchema
from gwio.models.pricing import PriceType
from gwio.models.products import Product
from gwio.models.products.base import (
    BaseProduct,
    PRODUCT_TYPE_DELIVERY_METHOD,
)
from gwio.models.webshop_product import WebshopProduct
from gwio.models.webshops import Webshop

log = logging.getLogger(__name__)

GetSchema = Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet


@app.register_endpoint(
    Product,
    route='/delivery_methods/',
    method=GET,
    event_type=LogEventType.LIST,
    index=Product.TypeIndex,
    query_params=dict(type=PRODUCT_TYPE_DELIVERY_METHOD),
    output_schema=GetSchema(many=True),
    authorisation=(rbac.admin, rbac.shopkeeper),
)
def list_all_delivery_methods(products):
    """Return all the available delivery methods"""
    try:
        for method in products:
            product = Product.by_guid(method.guid)
            yield product.API.Assessor.evaluate(product)
    except Product.DoesNotExist:
        pass


# pylint: disable=no-self-use
@app.register_resource
@app.base_path('/delivery_methods')
@app.path_params('uuid:guid')
class DeliveryMethodsAdminAPI(MethodResource):
    """
    Resource for Products
    """

    @staticmethod
    def _conflict(description=None):
        raise Conflict(description=description)

    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        output_schema=GetSchema,
        authorisation=(rbac.admin, rbac.shopkeeper),
    )
    def get(self, product):
        """
        summary: Get delivery method information.
        tags:
            - DeliveryMethod
         """
        if product.type != PRODUCT_TYPE_DELIVERY_METHOD:
            raise NotFound
        return product.API.Assessor.evaluate(product)

    @app.register_endpoint(
        Product,
        autoload=False,
        output_schema=GetSchema,
        input_schema=BaseProduct.Schemas.Post,
    )
    def post(self, **kwargs):
        """
        summary: Create a new delivery method
        tags:
            - DeliveryMethod
            """
        if kwargs.get('type') != PRODUCT_TYPE_DELIVERY_METHOD:
            raise Conflict(description='Only delivery methods allowed')
        product = Product(**kwargs)
        # Nothing is given then the delivery method price is always retail price
        if product.base_price_type is None:
            product.base_price_type = PriceType.RETAIL
        g.log_events_util.audit_object = product
        product.save()
        return product.API.Assessor.evaluate(product), 201

    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        output_schema=GetSchema,
        input_schema=BaseProduct.Schemas.Put(partial=True),
        automodify=False,
    )
    def put(self, product, **kwargs):
        """
        summary: Update existing delivery method
        tags:
            - DeliveryMethod
        """
        try:
            if product.type != PRODUCT_TYPE_DELIVERY_METHOD:  # this interface is only for delivery methods
                raise NotFound
            product.modify(**kwargs)
            # Nothing is given then the delivery method price is always retail price
            if product.base_price_type is None:
                product.base_price_type = PriceType.RETAIL
            product.refresh()
        except Product.DoesNotExist:
            raise NotFound
        return product.API.Assessor.evaluate(product)

    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        output_schema=GetSchema,
    )
    def delete(self, product):
        """
        summary: Delete an existing delivery method.
        tags:
            - DeliveryMethod
        """
        # TODO: remove the delivery method from all webshops
        if product.type != PRODUCT_TYPE_DELIVERY_METHOD:  # again this interface is only for delivery methods
            raise NotFound
        product.delete()
        return make_response('', 204)


_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid')


class WebshopDeliveryMethodSchema(GwioSchema):
    method_guid = fields.UUID(required=True)
    pricing_guid = fields.UUID()


class WebshopDeliveryMethodsUpdateSchema(GwioSchema):
    """
    name: delivery method
    description: the shop specific delivery method to be updated.
    """
    delivery_methods = fields.Nested(WebshopDeliveryMethodSchema, many=True, allow_none=True)


# TODO: or webshop-delivery-method, or something else?
@app.register_endpoint(
    Webshop,
    automodify=False,
    route='/webshops/<uuid:guid>/delivery_methods/',
    method=PUT,
    output_schema=GetSchema(many=True),
    input_schema=WebshopDeliveryMethodsUpdateSchema,
    authorisation=_admin_or_shopkeeper,
)
def update_all_webshop_delivery_methods(webshop, delivery_methods=None):
    """Set delivery methods available for the shop"""
    try:
        webshop.delivery_methods = delivery_methods if delivery_methods is not None else list()
        for method in webshop.delivery_methods:
            # pylint: disable=broad-except
            try:
                yield WebshopProduct.API.Assessor.evaluate(product=method, webshop_guid=webshop.guid)
            except Exception as e:
                log.exception(e)
    except Webshop.DoesNotExist:
        raise NotFound
