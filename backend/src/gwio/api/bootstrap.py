# coding=utf-8
"""
Endpoints for web shops.
"""
import logging

from flask import (
    current_app,
    g,
)
from gwio.models.tags import Tag
from marshmallow import (
    fields,
)

from gwio.core import rbac
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import (
    GET,
    PUT,
)
from gwio.environment import Env
from gwio.models.bootstrap import Bootstrap
from gwio.models.webshops import Webshop
from werkzeug.exceptions import BadRequest

from .flask_app import app

logger = logging.getLogger(__name__)


def set_tag_name(children, tag_cache=None):
    tag_cache = {} if tag_cache is None else tag_cache
    if children:
        for item in children:
            try:
                tag = tag_cache[item.guid]
            except KeyError:
                try:
                    tag = Tag.by_guid(item.guid)
                    tag_cache[item.guid] = tag
                except Tag.DoesNotExist:
                    raise BadRequest(f'Tag with guid {item.guid} is not valid')
            item.name = tag.name
            if item.children:
                set_tag_name(children=item.children, tag_cache=tag_cache)


class BootstrapResponse(Bootstrap.Schemas.Get):
    password_policy = fields.Dict()


@app.register_endpoint(
    Bootstrap,
    route='/bootstrap/',
    method=GET,
    output_schema=BootstrapResponse,
    authorisation=None,
    extra_kwargs=dict(vertical=Env.VERTICAL),
    catch_not_found=True,
    cache_for='10m'
)
def bootstrap(bootstrap):
    """Data to bootstrap the client application.
    """
    shops = Webshop.scan(enabled=True)
    response = dict(
        shops=shops,
        data=dict(),
        tag_hierarchy=list(),
    )
    try:
        response['data'] = bootstrap.data
        response['tag_hierarchy'] = bootstrap.tag_hierarchy
    except AttributeError:
        pass

    try:
        del response['data']['dashboard']
    except KeyError:
        pass
    response['password_policy'] = current_app.customer_pool.password_policy
    return response


@app.register_endpoint(
    Bootstrap,
    route='/bootstrap/',
    method=PUT,
    authorisation=rbac.admin,
    extra_kwargs=dict(vertical=Env.VERTICAL),
    catch_not_found=True,
)
def put_bootstrap_data(bootstrap, **values):
    """Modify (or create) bootstrap data"""
    if bootstrap is None:
        bootstrap = Bootstrap(vertical=Env.VERTICAL, **values)
        g.log_events_util.event_type = LogEventType.CREATE
        g.log_events_util.audit_object = bootstrap
    set_tag_name(bootstrap.tag_hierarchy)
    bootstrap.save()
    app.redis_cache.invalidate("*/bootstrap")
    return bootstrap


class DashboardBootsrapSchema(Bootstrap.Schema):
    class Meta:
        exclude = ('vertical',)


class BootstrapSchema(DashboardBootsrapSchema):
    shop = fields.Nested(Webshop.Schemas.Get)


@app.register_endpoint(
    Bootstrap,
    route='/dashboard/bootstrap/',
    method=GET,
    authorisation=(rbac.admin, rbac.shopkeeper, rbac.product_owner),
    extra_kwargs=dict(vertical=Env.VERTICAL),
    output_schema=DashboardBootsrapSchema,
    cache_for="10m",
)
def dashboard_bootstrap(bootstrap):
    try:
        data = bootstrap.data['dashboard']
    except KeyError:
        data = dict()
    return dict(
        data=data,
        tag_hierarchy=bootstrap.tag_hierarchy,
    )


@app.register_endpoint(
    Webshop,
    route='/bootstrap/<uuid:guid>/',
    method=GET,
    output_schema=BootstrapSchema,
    authorisation=None,
    cache_for="10m",
)
def get_shop_by_guid(webshop):
    """Get web shop record based on guid"""
    bootstrap = Bootstrap.get(vertical=Env.VERTICAL)
    response = dict(
        shop=webshop,
        tag_hierarchy=bootstrap.tag_hierarchy,
    )
    response['data'] = bootstrap.data if bootstrap is not None else dict()
    return response
