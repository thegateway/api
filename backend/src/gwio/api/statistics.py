# coding=utf-8
import datetime
from enum import Enum
from typing import Generator

import boto3
from elasticsearch import (
    Elasticsearch,
    helpers,
)

from gwio.core import rbac
from gwio.core.decorators.api import GET, authorise
from gwio.environment import Env
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD, PRODUCT_TYPE_DEFAULT
from gwio.models.statistics import Statistics
from gwio.models.tags import (
    TagType,
    Tag,
)
from gwio.models.webshop_product import WebshopProduct
from gwio.models.webshops import Webshop
from gwio.utils.table_names import make_table_name
from werkzeug.exceptions import BadRequest

from .flask_app import app

try:
    import pandas as pd
except ModuleNotFoundError:
    pass


class QueryType(Enum):
    PAYMENT_FINALIZE = 'payment_finalize'
    PAYMENT_REFUND = 'payment_refund'


def do_es_scan(query: dict, index: str) -> Generator[dict, None, None]:
    elastic_client = Elasticsearch(Env.ELASTICSEARCH_HOST)

    res = helpers.scan(elastic_client,
                       index=index,
                       query=query
                       )
    for item in res:
        yield item['_source']


def do_es_search(query: dict, index: str) -> dict:
    es = Elasticsearch(Env.ELASTICSEARCH_HOST)
    res = es.search(
        index=index,
        body=query
    )
    return res


# Get the very first log event's timestamp
def get_first_timestamp():
    query = {
        'size': 1,
        'query': {
            'match_all': {}
        },
        "sort": [{"timestamp": {"order": "asc"}}]
    }
    elastic_client = Elasticsearch(Env.ELASTICSEARCH_HOST)
    try:
        res = elastic_client.search(
            index=f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs",
            body=query
        )
    except BadRequest:
        return None
    return pd.to_datetime(res['hits']['hits'][0]['_source']['timestamp']).date()


def extract_product_guids_from_product_line(product_lines: list, shop_guid: str = '') -> list:
    if shop_guid:
        return [item['guid'] for item in product_lines if item['shop_guid'] == shop_guid and item['type'] == PRODUCT_TYPE_DEFAULT]

    return [item['guid'] for item in product_lines if item['type'] == PRODUCT_TYPE_DEFAULT]


def extract_delivery_method_from_product_line(product_lines: list) -> str:
    return next(item for item in product_lines if item['type'] == PRODUCT_TYPE_DELIVERY_METHOD)


def shop_specific_order_log_to_dataframe(data: Generator[dict, None, None]) -> Generator[dict, None, None]:
    order_dicts = []
    for d in data:
        order = d['obj']['Order']
        for shop_guid in order['shop_guids']:
            order_dict = dict()
            total_sale = next(shop['price'] for shop in order['summary']['shops'] if shop['shop_guid'] == shop_guid)
            try:
                order_dict['customer_name'] = order['customer']['name']
                order_dict['customer_id'] = order['buyer_identity_id']
                order_dict['delivery_method_name'] = extract_delivery_method_from_product_line(order['product_lines'])['name']
                order_dict['delivery_method_guid'] = extract_delivery_method_from_product_line(order['product_lines'])['guid']
                order_dict['product_guids'] = extract_product_guids_from_product_line(order['product_lines'], shop_guid=shop_guid)
                order_dict['payment_method_guid'] = order['payment_method_guid']
                order_dict['payment_method_type'] = order['payments'][0]['payment_type']
                order_dict['object_id'] = d['object_id']
                order_dict['sale'] = total_sale
                order_dict['shop_guid'] = shop_guid
                order_dict['timestamp'] = d['timestamp']
            except (AttributeError, KeyError):
                # AttributeError, KeyError happen, when trying to access empty object
                order_dict = dict()
            order_dicts.append(order_dict)

    for item in order_dicts:
        yield item


def all_order_log_to_dataframe(data: Generator[dict, None, None]) -> Generator[dict, None, None]:
    for d in data:
        order = d['obj']['Order']
        order_dict = dict()
        try:
            order_dict['customer_name'] = order['customer']['name']
            order_dict['customer_id'] = order['buyer_identity_id']
            order_dict['delivery_method_name'] = extract_delivery_method_from_product_line(order['product_lines'])['name']
            order_dict['delivery_method_guid'] = extract_delivery_method_from_product_line(order['product_lines'])['guid']
            order_dict['product_guids'] = extract_product_guids_from_product_line(order['product_lines'])
            order_dict['payment_method_guid'] = order['payment_method_guid']
            order_dict['payment_method_type'] = order['payments'][0]['payment_type']
            order_dict['object_id'] = d['object_id']
            order_dict['sale'] = order['summary']['price']
            order_dict['shop_guids'] = order['shop_guids']
            order_dict['timestamp'] = d['timestamp']
        except (AttributeError, KeyError):
            # AttributeError, KeyError happen, when trying to access empty object
            order_dict = dict()
        yield order_dict


def process_log(start_date: str, end_date: str, event_type: str, webshop_guid: str = '') -> Generator[dict, None, None]:
    query = dict(
        query=dict(
            bool=dict(
                must=[
                    dict(
                        term=dict(
                            event_type=event_type
                        )
                    ),
                    dict(
                        term=dict(
                            object_type='Order'
                        )
                    )
                ],
                filter=dict(
                    range=dict(
                        timestamp=dict(
                            gte=start_date,
                            lte=end_date
                        )
                    )
                )

            )
        )
    )
    index = f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs"

    process_log_to_dataframe = all_order_log_to_dataframe
    if webshop_guid:
        query['query']['bool']['must'].append({"term": {"obj.Order.shop_guids": webshop_guid}})
        process_log_to_dataframe = shop_specific_order_log_to_dataframe

    data = do_es_scan(query=query, index=index)
    return process_log_to_dataframe(data)


def get_order_df_for_admin(start_date: str, end_date: str) -> pd.DataFrame:
    order_df = pd.DataFrame(process_log(start_date=start_date, end_date=end_date, event_type=QueryType.PAYMENT_FINALIZE.value))
    try:
        # Change timestamp from string object to pandas datetime object
        order_df['timestamp'] = pd.to_datetime(order_df['timestamp'])

    except (AttributeError, KeyError):
        # AttributeError, KeyError happen, when trying to access empty dataframe's column value
        pass

    return order_df


def get_order_df_for_shop(start_date: str, end_date: str, webshop_guid: str) -> pd.DataFrame:
    order_df = pd.DataFrame(process_log(start_date=start_date, end_date=end_date, event_type=QueryType.PAYMENT_FINALIZE.value, webshop_guid=webshop_guid))
    try:
        # Keep webshop specific data only
        order_df = order_df[order_df['shop_guid'] == webshop_guid]

        # Change timestamp from string object to pandas datetime object
        order_df['timestamp'] = pd.to_datetime(order_df['timestamp'])

    except (AttributeError, KeyError):
        # AttributeError, KeyError happen, when trying to access empty dataframe's column value
        pass

    return order_df


def filter_order_df(order_df: pd.DataFrame, **kwargs) -> pd.DataFrame:
    try:
        # Keep delivery_method specific data only
        order_df = order_df[order_df['delivery_method_guid'] == str(kwargs['delivery_method_guid'])]
    except KeyError:
        pass
    try:
        # Keep payment_method specific data only
        order_df = order_df[order_df['payment_method_guid'] == str(kwargs['payment_method_guid'])]
    except KeyError:
        pass
    return order_df


def get_ordered_product_df(order_df: pd.DataFrame) -> pd.DataFrame:
    try:
        # Keep product specific data only
        product_df = order_df.loc[:, ['timestamp', 'product_guids']].explode('product_guids')
    except (AttributeError, KeyError):
        # AttributeError, KeyError happen, when trying to access empty dataframe's column value
        return pd.DataFrame()
    return product_df


def query_webshop_product(webshop_guid: str = '', result_size: int = 0) -> dict:
    basic_aggs = dict(
        product_count=dict(
            value_count=dict(
                field='guid'
            )
        ),
        active_product_count=dict(
            value_count=dict(
                field='activated'
            )
        ),
        for_sale_product_count=dict(
            value_count=dict(
                field='for_sale'
            )
        ),
        archived_product_count=dict(
            value_count=dict(
                field='archived'
            )
        )
    )

    query = dict(
        query=dict(
            bool=dict(
                must_not=dict(
                    exists=dict(
                        field='archived'
                    )
                )
            )
        ),
        size=0,
        aggs=dict(
            webshops_product_stats=dict(
                terms=dict(
                    field='shop_guid',
                    size=result_size
                ),
                aggs=basic_aggs
            ),
            **basic_aggs
        )
    )

    if webshop_guid:
        query = dict(
            query=dict(
                bool=dict(
                    must=dict(
                        term=dict(
                            shop_guid=webshop_guid
                        )
                    ),
                    must_not=dict(
                        exists=dict(
                            field='archived'
                        )
                    )
                )
            ),
            size=0,
            aggs=basic_aggs
        )
    index = make_table_name(Env.VERTICAL, Env.APPLICATION_ENVIRONMENT, 'webshop_product')
    res = do_es_search(index=index, query=query)
    return res['aggregations']


def set_dates(**kwargs):
    start_date = kwargs.get('start_date', get_first_timestamp())
    end_date = kwargs.get('end_date', datetime.date.today())
    if start_date > end_date:
        raise BadRequest('start_date should be lower then end_date')
    return dict(
        start_date=str(start_date),
        end_date=str(end_date),
    )


def time_agg_dict(**kwargs):
    agg_dict = dict(
        date_histogram=dict(
            field='created',
            format='yyyy-MM-dd',
            interval='day',
            min_doc_count=0,
            extended_bounds=dict(
                max=str(datetime.date.today())
            )
        )
    )
    agg_dict['date_histogram'].update(kwargs)
    return agg_dict


def time_aggs_for_all_product():
    query = dict(
        size=0,
        aggs=dict(
            daily_added_products=time_agg_dict(),
            weekly_added_products=time_agg_dict(interval='week'),
            monthly_added_products=time_agg_dict(interval='month'),
            yearly_added_products=time_agg_dict(interval='year')
        )
    )
    index = make_table_name(Env.VERTICAL, Env.APPLICATION_ENVIRONMENT, 'webshop_product')
    res = do_es_search(index=index, query=query)
    return res['aggregations']


# ==============================================================
# ========== Statistics endpoints for admin only ===============

# pylint: disable=no-member
@app.register_endpoint(
    None,
    route='/statistics/order_statistics',
    method=GET,
    input_schema=Statistics.Schemas.FilterParams,
    output_schema=Statistics.Schemas.OrderStatSchema,
)
def order_statistics_for_admin(**kwargs):
    """
    Return order statistics for admin. Accept 4 optional query parameters 'delivery_method_guid', 'payment_method_guid'. start_date and end_date.
    If optional parameters provided, return order statistics filtered by those parameters.
    Format of start_date and end_date parameter should be 'yyyy-mm-dd'
    """
    dates = set_dates(**kwargs)
    order_df = get_order_df_for_admin(
        start_date=dates['start_date'],
        end_date=dates['end_date']
    )

    return dict(
        df=filter_order_df(order_df=order_df, **kwargs),
        **dates
    )


# pylint: disable=no-member
@app.register_endpoint(
    None,
    route='/statistics/product_statistics',
    method=GET,
    input_schema=Statistics.Schemas.ProductStatisticsResultSize,
    output_schema=Statistics.Schemas.ProductStatSchema,
)
def product_statistics_for_admin(size):
    """
    Return products statistics for admin. Accept 1 optional query parameters 'size', default value is 20.
    size: how many webshop's product stats should be return. By default 20 webshop's product stats will return.
    Response contains only those webshops, which has any product.
    """
    return query_webshop_product(result_size=size)


# pylint: disable=no-member
@app.register_endpoint(
    None,
    route='/statistics/top_sold_products',
    method=GET,
    input_schema=Statistics.Schemas.TopProductsQuery,
    output_schema=Statistics.Schemas.TopProductsSchema,
)
def top_sold_products_for_admin(**kwargs):
    """
    Return a list contains top sold products combine all webshops for admin. Accept 3 optional query parameters 'count', start_date and end_date.
    count : how many top sold products should be listed. if not provided, listed top 5 most sold products.
    Format of start_date and end_date parameter should be 'yyyy-mm-dd'.
    """
    dates = set_dates(**kwargs)
    order_df = get_order_df_for_admin(
        start_date=dates['start_date'],
        end_date=dates['end_date']
    )
    product_df = get_ordered_product_df(order_df)
    try:
        # Calculate total count of each sold product
        top_products_df = pd.DataFrame(product_df['product_guids'].value_counts().head(kwargs.get('count', 5))).reset_index()
    except (AttributeError, KeyError):
        return product_df
    return top_products_df


# pylint: disable=no-member
@app.register_endpoint(
    None,
    route='/statistics/product_time_stats',
    method=GET,
    output_schema=Statistics.Schemas.ProductTimeStatSchema,
)
def products_time_series_stats_for_admin(**kwargs):
    """
    Return product's time series statistics, e.g. how many product added over time.
    """
    return time_aggs_for_all_product()


# ==============================================================================
# ========== Statistics endpoints for both shopkeepers and admin ===============
_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid')


# pylint: disable=no-member
# pylint: disable=no-value-for-parameter
@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/statistics/',
    method=GET,
    authorisation=_admin_or_shopkeeper,
    output_schema=Statistics.Schemas.BasicStatistics,
)
def shop_basic_statistics(webshop):
    """Return basic statistics for that webshop."""
    cognito_idp = boto3.client('cognito-idp', Env.COGNITO_USER_POOL_REGION)
    result = cognito_idp.describe_user_pool(UserPoolId=Env.COGNITO_POOL_ID)
    total_users_count = result['UserPool']['EstimatedNumberOfUsers']

    total_products_count = WebshopProduct.WebshopIndex.query(webshop_guid=webshop.guid).count()
    total_tags_count = Tag.OwnerTypeIndex.query(owner=webshop.guid).count()
    total_categories_count = Tag.OwnerTypeIndex.query(owner=webshop.guid, type=TagType.CATEGORY).count()

    return dict(
        total_users_count=total_users_count,
        total_products_count=total_products_count,
        total_tags_count=total_tags_count,
        total_categories_count=total_categories_count
    )


# pylint: disable=no-member
@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/statistics/orders',
    method=GET,
    authorisation=_admin_or_shopkeeper,
    input_schema=Statistics.Schemas.FilterParams,
    output_schema=Statistics.Schemas.OrderStatSchema,
)
def order_statistics_for_webshop(webshop, **kwargs):
    """
    Return order statistics for webshop.Accept 4 optional query parameters 'delivery_method_guid', 'payment_method_guid'. start_date and end_date.
    If optional parameters provided, return order statistics filtered by those parameters.
    Format of start_date and end_date parameter should be 'yyyy-mm-dd'
    """
    dates = set_dates(**kwargs)
    order_df = get_order_df_for_shop(
        start_date=dates['start_date'],
        end_date=dates['end_date'],
        webshop_guid=str(webshop.guid)
    )

    return dict(
        df=filter_order_df(order_df=order_df, **kwargs),
        **dates
    )


# pylint: disable=no-member
@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/statistics/top_sold_products',
    method=GET,
    authorisation=_admin_or_shopkeeper,
    input_schema=Statistics.Schemas.TopProductsQuery,
    output_schema=Statistics.Schemas.TopProductsSchema,
)
def top_sold_products_for_webshop(webshop, **kwargs):
    """
    Return a list contains top sold products for that webshop. Accept 3 optional query parameters 'count', start_date and end_date.
    If optional parameters provided, return order statistics filtered by those parameters.
    Format of start_date and end_date parameter should be 'yyyy-mm-dd'
    count : how many top sold products should be listed. if not provided, listed top 5 most sold products.
    """
    dates = set_dates(**kwargs)
    order_df = get_order_df_for_shop(
        start_date=dates['start_date'],
        end_date=dates['end_date'],
        webshop_guid=str(webshop.guid)
    )
    product_df = get_ordered_product_df(order_df)
    try:
        # Calculate total count of each sold product
        top_products_df = pd.DataFrame(product_df['product_guids'].value_counts().head(kwargs.get('count', 5))).reset_index()
    except (AttributeError, KeyError):
        return product_df
    return top_products_df


# pylint: disable=no-member
@app.register_endpoint(
    WebshopProduct,
    route='/webshops/<uuid:webshop_guid>/statistics/products/<uuid:product_guid>',
    method=GET,
    authorisation=_admin_or_shopkeeper,
    input_schema=Statistics.Schemas.FilterParams(partial=True, only=('start_date', 'end_date')),
    output_schema=Statistics.Schemas.OrderedProductStatSchema,
)
def product_sold_over_time(webshop_product, **kwargs):
    """
    Return specific products statistics (how many it sold over time) for that webshop.
    Accept 2 optional query parameters start_date and end_date.
    Format of start_date and end_date parameter should be 'yyyy-mm-dd'.
    """
    dates = set_dates(**kwargs)
    order_df = get_order_df_for_shop(
        start_date=dates['start_date'],
        end_date=dates['end_date'],
        webshop_guid=str(webshop_product.webshop_guid)
    )

    product_df = get_ordered_product_df(order_df)
    try:
        product_df.set_index('timestamp', inplace=True)
        product_df = product_df.loc[product_df['product_guids'] == str(webshop_product.product_guid)]
    except (AttributeError, KeyError):
        pass
    return dict(
        df=product_df,
        **dates
    )


# pylint: disable=no-member
@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/statistics/product_statistics',
    method=GET,
    authorisation=_admin_or_shopkeeper,
    output_schema=Statistics.Schemas.WebshopProductStatSchema,
)
def product_statistics(webshop, **kwargs):
    """
    Return products statistics for that webshop.
    """
    return query_webshop_product(webshop_guid=webshop.guid)
