# coding=utf-8
import logging

from flask import (
    g,
    make_response,
)
from flask_apispec import MethodResource
from marshmallow import fields
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.core import rbac
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import (
    GET,
    authorise,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.webshops import Webshop
from gwio.operations.users import add_to_organization
from gwio.utils.aws import CognitoIdpClient
from .flask_app import app

log = logging.getLogger(__name__)


@app.register_endpoint(
    Webshop,
    index=Webshop.scan,  # TODO: Probably shouldn't be returning `not enabled` webshops for anyone but admins
    route='/webshops/',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=Webshop.Schemas.Get(many=True),
    authorisation=None,
    cache_for='10m',
)
def all_shops(webshops):
    """Return all known webshops"""
    return webshops


_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid')


# pylint: disable=R0201
@app.register_resource
@app.base_path('/webshops')
@app.path_params(f'uuid:{Webshop.Table.hash_key}')
class WebshopAPI(MethodResource):
    @app.register_endpoint(
        Webshop,
        authorisation=_admin_or_shopkeeper,
    )
    def get(self, *, webshop):
        """Get webshop description"""
        return webshop

    @app.register_endpoint(
        Webshop,
        authorisation=rbac.ALL,
    )
    def post(self, webshop):
        """Add a new webshop"""
        if webshop.enabled and not rbac.admin():
            raise Conflict('Only admin can create shops that are enabled.')
        try:
            Webshop.get(guid=webshop.guid)
            raise Conflict(description=f'Webshop ({webshop.guid}) already exists')
        except Webshop.DoesNotExist:
            pass
        if not rbac.admin():
            add_to_organization(
                group_name=CognitoIdpClient.Group.SHOPKEEPER,
                organization_guid=webshop.guid,
                user_guid=g.user.guid,
            )
        webshop.save()
        app.redis_cache.invalidate('/webshops')
        app.redis_cache.invalidate('/bootstrap')
        return webshop, 201

    @app.register_endpoint(Webshop)
    def delete(self, *, webshop):
        """Remove webshop identified by 'shop_guid'"""
        if webshop:
            webshop.delete()
        app.redis_cache.invalidate('/webshops')
        app.redis_cache.invalidate('/bootstrap')
        return make_response('', 204)

    @app.register_endpoint(
        Webshop,
        authorisation=_admin_or_shopkeeper,
    )
    def put(self, webshop):
        """Update webshop identified by 'shop_guid'"""
        webshop.save()
        app.redis_cache.invalidate('/webshops')
        app.redis_cache.invalidate('/bootstrap')
        return webshop


# pylint: disable=R0201
@app.register_resource
@app.base_path('/webshops')
@app.path_params('uuid:guid', '/opening_hours')
class WebshopOpeningHoursAPI(MethodResource):
    @app.register_endpoint(
        Webshop,
        output_schema=Webshop.API.OpeningHours.marshal,
        authorisation=None,
    )
    def get(self, webshop):
        """Get webshop opening hours"""
        return webshop.opening_hours

    @app.register_endpoint(
        Webshop,
        output_schema=Webshop.API.OpeningHours.marshal,
        input_schema=Webshop.API.OpeningHours.update,
        automodify=False,
        authorisation=_admin_or_shopkeeper,
    )
    def put(self, webshop, **kwargs):
        """Update webshop opening hours"""
        webshop.opening_hours = kwargs
        webshop.save()
        return webshop.opening_hours


# pylint: disable=too-few-public-methods
class PaymentGuidsSchema(GwioSchema):
    """
    name: payment methods
    description: list of payment methods to enable for the shop.
    """
    payment_methods = fields.List(fields.UUID())


# pylint: disable=R0201
@app.register_resource
@app.base_path('/webshops')
@app.path_params('uuid:guid', '/payment_methods')
class WebshopPaymentMethods(MethodResource):
    @app.register_endpoint(
        Webshop,
        output_schema=Webshop.API.PaymentMethod.Schemas.Get(many=True),
        authorisation=None,
    )
    def get(self, webshop):
        """Get the webshop payment method"""
        return webshop.payment_methods

    @app.register_endpoint(
        Webshop,
        automodify=False,
        output_schema=Webshop.API.PaymentMethod.Schemas.Get(many=True),
        input_schema=PaymentGuidsSchema,
        authorisation=_admin_or_shopkeeper,
    )
    def put(self, webshop, payment_methods):
        """Update webshop payment methods"""
        try:
            webshop.payment_methods = payment_methods
            webshop.save()
            return webshop.payment_methods
        except Webshop.DoesNotExist:
            raise NotFound


class StaticPagesUpdateSchema(GwioSchema):
    """
    name: static pages
    description: static pages for the shop
    """
    pages = fields.Nested(Webshop.API.StaticPage.Schemas.Put, many=True)


# pylint: disable=R0201
@app.register_resource
@app.base_path('/webshops')
@app.path_params('uuid:guid', '/static_pages')
class WebshopStaticPages(MethodResource):
    @app.register_endpoint(
        Webshop,
        output_schema=Webshop.API.StaticPage.Schemas.Get(many=True),
        authorisation=None,
    )
    def get(self, webshop):
        """Get webshop static pages"""
        return webshop.static_pages

    @app.register_endpoint(
        Webshop,
        output_schema=Webshop.API.StaticPage.Schemas.Get(many=True),
        input_schema=StaticPagesUpdateSchema,
        authorisation=_admin_or_shopkeeper,
        automodify=False
    )
    def put(self, webshop, pages):
        """Set webshop static pages"""
        try:
            for page in pages:
                try:
                    page_guid = page['guid']
                except KeyError:
                    continue
                if page_guid is not None and page_guid not in map(lambda p: p.guid, webshop.static_pages):
                    raise Conflict(f'Unknown guid "{page_guid}" in static pages')

            webshop.static_pages = pages
            webshop.save()
            return webshop.static_pages
        except Webshop.DoesNotExist:
            raise NotFound
