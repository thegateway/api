# coding=utf-8
import logging
from functools import partial

import elasticsearch_dsl
from flask import (
    g,
    jsonify,
    make_response,
    redirect,
    request,
)
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.api.exception_handlers import BadDSLException
from gwio.api.flask_app import app
from gwio.core import (
    index_es_document,
    rbac,
)
from gwio.core.decorators.api import (
    GET,
    POST,
    authorise,
)
from gwio.ext.eshelpers import (
    QueryDDLSchema,
    WEBSHOP_PRODUCT_ES_INDEX,
    do_es_search,
    es_search,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.products import Product
from gwio.models.webshop_product import (
    ProductSearchCustomerResponseSchema,
    WebshopProduct,
)
from gwio.operations.products import organization_product_create
from gwio.core.cw_logging import LogEventType

log = logging.getLogger(__name__)


# pylint: disable=no-self-use


@app.register_resource
@app.path_params('uuid:guid')
class ProductsAPI(MethodResource):
    """
    Resource for Products
    """

    # @cached('1h') - Disabled as a workaround for #348
    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        authorisation=None,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
    )
    def get(self, **kwargs):
        """
        summary: Redirected to `/admin/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}dashboard{request.full_path}', code=308)

    @app.register_endpoint(
        Product,
        autoload=False,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
    )
    def post(self, **kwargs):
        """
        summary: Redirected to `/admin/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}admin{request.full_path}', code=308)

    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
        input_schema=Product.Schemas.Put(partial=True),
        automodify=False,
    )
    def put(self, **kwargs):
        """
        summary: Redirected to `/admin/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}admin{request.full_path}', code=308)

    @app.register_endpoint(
        Product,
        index=Product.by_guid,
    )
    def delete(self, **kwargs):
        """
        summary: Redirected to `/admin/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}admin{request.full_path}', code=308)


# TODO: What is the object here?
@app.register_endpoint(
    Product,
    index=Product.by_guid,
    route='/products/<uuid:guid>/suppliers',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=None,
    authorisation=None,
)
def get_providers(product):
    """Get the guids of all webshops providing the given product"""
    webshop_products = WebshopProduct.scan(product_guid=product.guid)
    return jsonify([wsp.webshop_guid for wsp in webshop_products])


_admin_or_shopkeeper_or_brand_owner = authorise(rbac.admin, rbac.shopkeeper, rbac.product_owner, guid_name='organization_guid')


# pylint: disable=no-self-use
@app.register_resource
@app.base_path('/organizations')
@app.path_params('uuid:organization_guid', '/products', 'uuid:guid')
class OrganizationProductsAPI(MethodResource):
    """
    Resource for Webshop owned Products
    """
    _evaluate_organization = Product.API.Assessor.evaluate

    # @cached('1h') - Disabled as a workaround for #348
    @app.register_endpoint(
        Product,
        index=Product.by_organization_product,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet,
        authorisation=_admin_or_shopkeeper_or_brand_owner,
    )
    def get(self, *, product):
        """Get product description for product identified by 'product_id' in the given organization context"""
        if product is None:
            raise NotFound
        try:
            return self._evaluate_organization(product=product)
        except Product.DoesNotExist:
            raise NotFound

    @app.register_endpoint(
        Product,
        autoload=False,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet,
        authorisation=_admin_or_shopkeeper_or_brand_owner,
    )
    def post(self, *, organization_guid, **kwargs):
        """Add a new product to the product database for the given organization"""
        product = organization_product_create(organization_guid, **kwargs)
        g.log_events_util.audit_object = product
        return self._evaluate_organization(product=product), 201

    @app.register_endpoint(
        Product,
        index=Product.by_organization_product,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet,
        automodify=False,
        authorisation=_admin_or_shopkeeper_or_brand_owner,
    )
    def put(self, *, product, **kwargs):
        """Update the description of a existing product identified by 'product_guid'"""
        if product is None:
            raise NotFound
        product.modify(**kwargs)
        return self._evaluate_organization(product=product)

    @app.register_endpoint(
        Product,
        autoload=False,
        authorisation=_admin_or_shopkeeper_or_brand_owner,
    )
    def delete(self, organization_guid, guid):
        """Remove product identified by "product_id" from the database"""
        try:
            product = Product.by_organization_product(organization_guid, guid)
            product.delete()
        except Product.DoesNotExist:
            # Deleted from DB but might exist in ES
            index_es_document.delete_product_es(product_guid=guid)
        return make_response('', 204)


class ProductQueryResults(GwioSchema):
    next = fields.String()
    total = fields.Integer(default=0)
    products = fields.Nested(Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet, many=True, default=list())


# @cached('15m') -- Disabled as a workaround for #348
@app.register_endpoint(
    WebshopProduct,
    autoload=False,
    route='/products/_query',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=QueryDDLSchema,
    output_schema=ProductSearchCustomerResponseSchema,
    authorisation=None,
)
def query_products(dsl=None):
    """
    Return list of products matching the the query (elasticsearch query DSL)

    If pagination is required pass the `sort` key with parameter[s] to sort by. If the `total` in response is greater
    than the number of items in returned `products` the next page can be fetched by giving `search_after` parameter the
    value of the last product's `sort` key in the dsl.
    https://www.elastic.co/guide/en/elasticsearch/reference/current//search-request-body.html#request-body-search-search-after
    """
    if dsl is None:
        raise Conflict(description='Missing dsl')
    # Add filters to the query to hide unwanted documents
    try:
        dsl = elasticsearch_dsl.Search.from_dict(dsl)
    except ValueError as e:
        raise BadDSLException(e) from e
    dsl = dsl.filter('exists', field='activated')
    dsl = dsl.filter('term', for_sale=True)
    dsl = dsl.exclude('exists', field='archived')
    result = do_es_search(partial(es_search, index=WEBSHOP_PRODUCT_ES_INDEX, dsl=dsl))
    return result
