# coding=utf-8
"""
Administrative views for Dashboard API
"""

from flask import (
    Response,
    request,
    url_for,
)
from marshmallow import (
    ValidationError,
    fields,
    post_load,
    pre_load,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    NotFound,
)

import gwio.user
from gwio.api.flask_app import app
from gwio.cognito import User as CognitoUser
from gwio.core import rbac
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import (
    DELETE,
    GET,
    POST,
)
from gwio.environment import Env
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.operations.users import add_to_organization
from gwio.schemas import (
    CognitoUserListQueryParam,
    CognitoUserListSchema,
    CognitoUserSchema,
)
from gwio.utils.aws import CognitoIdpClient
from gwio.utils.notify import GenericUserNotification

User = gwio.user.User


@app.register_endpoint(
    User,
    route='/shops/<uuid:shop_id>/add_user/<uuid:identity_guid>',
    method=POST,
    event_type=LogEventType.UPDATE,
    automodify=False,
    output_schema=None,
    input_schema=None,
)
def post_add_user_to_shop(shop_id, user):
    f"""
    summary: Add a user as an agent to a shop
    description: Set a user to be part of an organization (shop) and add the user to group `{CognitoIdpClient.Group.SHOPKEEPER}`
    """
    app.customer_pool.set_custom_attribute('organization_guid', str(shop_id), user.guid)
    app.customer_pool.add_user_to_group(user.guid, CognitoIdpClient.Group.SHOPKEEPER)

    return Response(status=204)


@app.register_endpoint(
    User,
    route='/users/',
    method=GET,
    autoload=False,
    output_schema=CognitoUserListSchema,
    input_schema=CognitoUserListQueryParam,
)
def list_users(**kwargs):
    """
    Lists all users.
    Accepts query parameter "pool" values "admin" and "customer" to choose the cognito pool to list.
    If response contains "next", the URL found in "next" would lead to next set of users.
    Requires admin privileges.
    """
    pool = app.customer_pool
    pool_name = kwargs.get('pool')
    if pool_name is not None:
        pools = {'admin': app.customer_pool,
                 'customer': app.customer_pool}
        pool = pools.get(pool_name)
        if pool is None:
            raise BadRequest
    resp = pool.list_users(pagination_token=kwargs.get('token'))
    user_list = [CognitoUser.from_cognito(user) for user in resp['Users']]
    token = resp.get('PaginationToken')
    next_url = None
    if token:
        next_url = '{origin}{base}'.format(
            origin=request.host_url.rstrip('/'),
            base=url_for('list_users', token=token)
        )
    return {'next': next_url, 'users': user_list}


class SearchUsersQueryParam(GwioSchema):
    """
    name: user search query parameters
    description: cognito user pool user search query parameters
    """
    phone_number = fields.String()
    email = fields.String()
    identity_id = fields.String()
    guid = fields.UUID(attribute='sub')

    # pylint: disable=no-self-use
    @pre_load
    def check_either(self, values, **kwargs):
        provided = list()
        for key in self.declared_fields.keys():
            if values.get(key) is not None:
                provided.append(values.get(key))
        if len(provided) != 1:
            raise ValidationError(
                f'One of the fields {", ".join(self.declared_fields.keys())} required for searching users')
        return values


@app.register_endpoint(
    User,
    autoload=False,
    route='/users/search',
    method=GET,
    output_schema=CognitoUserSchema(many=True),
    input_schema=SearchUsersQueryParam,
    authorisation=(rbac.admin, rbac.shopkeeper),
)
def search_users(**kwargs):
    """Search for a user by their phone number or e-mail address"""
    try:
        user = User.by_identity_id(kwargs['identity_id'])
        resp = app.customer_pool.list_users(f'sub = "{user.guid}"')
    except KeyError:
        filters = next(f'{key} = "{value}"' for key, value in kwargs.items())
        resp = app.customer_pool.list_users(filters)
    for u in resp['Users']:
        user = CognitoUser.from_cognito(u)
        if rbac.admin() or rbac.shopkeeper():  # TODO: Should check the shopkeeper handles the user's project
            yield user


@app.register_endpoint(
    User,
    route='/users/<uuid:identity_guid>',
    method=DELETE,
)
def delete_user(user):
    """Delete specific user permanently:param user_id:
    """
    app.customer_pool.delete_user(str(user.guid))
    return Response(None, 204)


class CognitoGroup(GwioSchema):
    """
    name: user group
    description: User group membership
    """
    organization_guid = fields.UUID(required=False, allow_none=False)
    group_name = GwioEnumField(enum=CognitoIdpClient.Group, by_value=True, required=True)

    # pylint: disable=no-self-use
    @post_load
    def check(self, values, **kwargs):
        if values['group_name'] in [CognitoIdpClient.Group.PRODUCT_OWNER,
                                    CognitoIdpClient.Group.SHOPKEEPER] and values.get('organization_guid') is None:
            raise Conflict(f'organization_guid required for {values["group_name"]}')
        return values


@app.register_endpoint(
    User,
    route='/users/<uuid:identity_guid>/organizations',
    method=POST,
    event_type=LogEventType.UPDATE,
    output_schema=None,
    input_schema=CognitoGroup,
    automodify=False,
    authorisation=(rbac.admin, rbac.shopkeeper, rbac.product_owner),
)
def add_user_to_organization(user, organization_guid, group_name):
    """Add the user to an organization and a user group"""
    if not any([rbac.admin(), rbac.shopkeeper(organization_guid), rbac.product_owner(organization_guid)]):
        raise NotFound
    add_to_organization(group_name=group_name, organization_guid=organization_guid, user_guid=user.guid)
    return Response(status=204)


class NotificationSchema(GwioSchema):
    """
    name: notification
    description: a notification
    """
    title = fields.String(required=True)
    body = fields.String(required=True)
    # icon = fields.String(required=False) #TODO Figure it out


@app.register_endpoint(
    User,
    route='/users/<uuid:user_identity_guid>/send_notification',
    method=POST,
    output_schema=None,
    input_schema=NotificationSchema,
)
def send_user_notification(user, **kwargs):
    """Send a notification to the user"""
    GenericUserNotification.emit(dict(
        source=Env.SYSTEM_EMAIL,
        user=user,
        **kwargs))
    return Response(status=204)


class GUserSchema(GwioSchema):
    uuid = fields.UUID()
    shop_uuid = fields.UUID()
    is_admin = fields.Bool()
    is_shopkeeper = fields.Bool()
    is_customer = fields.Bool()
