from flask import make_response
from flask_apispec import MethodResource

from gwio.core.api.app import app
from gwio.core.decorators.api import GET
from gwio.core.cw_logging import LogEventType
from gwio.models.coupons.promotions import Promotion


@app.register_endpoint(
    Promotion,
    route='/admin/promotions',
    method=GET,
    event_type=LogEventType.LIST,
    index=Promotion.scan,
    output_schema=Promotion.Schemas.Get(many=True),
)
def admin_list_promotions(promotions):
    """List all promotions"""
    return promotions


@app.register_resource
@app.base_path('/admin')
@app.path_params('/promotions', 'uuid:guid')
class PromotionsAPI(MethodResource):

    @app.register_endpoint(Promotion)
    def get(self, promotion):
        """Get an existing promotion"""
        return promotion

    @app.register_endpoint(Promotion)
    def post(self, promotion):
        """Create a new promotion"""
        promotion.save()
        return promotion

    @app.register_endpoint(Promotion)
    def put(self, promotion):
        """Update an existing promotion"""
        promotion.save()
        return promotion

    @app.register_endpoint(Promotion)
    def delete(self, promotion):
        """Delete an existing promotion"""
        promotion.delete()
        return make_response('', 204)
