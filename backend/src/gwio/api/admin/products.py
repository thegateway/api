from flask import (
    g,
    make_response,
)
from flask_apispec import MethodResource

from gwio.core import index_es_document
from gwio.core.api.app import app
from gwio.models.products import Product


@app.register_resource
@app.base_path('/admin')
@app.path_params('/products', 'uuid:guid')
class AdminProductsAPI(MethodResource):
    """
    Resource for Products
    """

    # Admin can use `dashboard.ProductsAPI.get` so no need for one here

    @app.register_endpoint(
        Product,
        autoload=False,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
    )
    def post(self, **kwargs):
        """Add a new product to the product database"""
        product = Product(**kwargs)
        g.log_events_util.audit_object = product
        product.save()
        return product.API.Assessor.evaluate(product), 201

    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
        automodify=False,
    )
    def put(self, product, **kwargs):
        """Update the description of a existing product identified by 'product_guid'"""
        product.modify(**kwargs)
        return product.API.Assessor.evaluate(product)

    @app.register_endpoint(
        Product,
        autoload=False,
    )
    def delete(self, guid):
        """Remove product identified by "product_id" from the database"""
        try:
            product = Product.by_guid(guid)
            # Invalidating before delete, because afterwards `product.tags` will be empty
            for product_tag in product.tags:
                app.redis_cache.invalidate(f'/product_tags/{product_tag.guid}')
            product.delete()

        except Product.DoesNotExist:
            # Deleted from DB but might exist in ES
            index_es_document.delete_product_es(product_guid=guid)
        return make_response('', 204)
