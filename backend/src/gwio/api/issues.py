import datetime
import logging
from urllib import (
    parse,
)

import elasticsearch_dsl
from flask import (
    g,
    jsonify,
    make_response,
)
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    Forbidden,
)

from gwio import environment
from gwio.api.flask_app import app
from gwio.core import cw_logging
from gwio.core.decorators.api import POST
from gwio.core.rbac import ALL
from gwio.environment import Env
from gwio.ext.eshelpers import es_search
from gwio.ext.marshmallow import GwioSchema
from gwio.models.webshops import Webshop
from gwio.utils import recaptcha
from gwio.utils.aws import (
    get_sender,
    message_devops,
)
from gwio.utils.email import EMail

logger = logging.getLogger(__name__)


class ReportRequest(GwioSchema):
    """
    name: error report
    description: an issue to be reported.
    """
    message = fields.String(required=False, missing=None, allow_none=True)


@app.register_endpoint(
    None,
    route='/issues/request/<uuid:request_guid>',
    method=POST,
    input_schema=ReportRequest,
    authorisation=ALL,
)
def issues_request_report(*, request_guid, message):
    """Report an issue with the given request"""
    vertical = environment.Env.VERTICAL
    app_env = environment.Env.APPLICATION_ENVIRONMENT
    try:
        query = dict(
            query=dict(
                bool=dict(
                    filter=[
                        dict(term={'request_guid': request_guid}),
                    ],
                ),
            ),
        )
        es_search_result = es_search(
            index=f'{vertical}_{app_env}_logs',
            dsl=elasticsearch_dsl.Search.from_dict(query),
        )
    except Exception as e:
        logger.exception(e)
        raise

    # 15 minutes in the past because the issue might not get reported immediately as it happens, but the user might look at error for a while before reporting
    dt_start = datetime.datetime.utcnow().replace(microsecond=0) + datetime.timedelta(minutes=-15)
    # a bit to the future (if reported immediately) so that if the request processing lasts long, all logs will be included
    dt_end = dt_start + datetime.timedelta(minutes=20)

    region = environment.Env.AWS_REGION
    url = f'https://{region}.console.aws.amazon.com/cloudwatch/home'
    filter = parse.urlencode(dict(
        filter=f'"{request_guid}"',
        start=dt_start.isoformat(),
        end=dt_end.isoformat(),
    )).replace('&', ';')  # AWS uses their own separator instead of standard
    link = f'{url}?region={region}#logEventViewer:group=/aws/lambda/{vertical}-{"master" if app_env == "dev" else app_env};{filter}'

    subject = f'Reporting error on request `{request_guid}`'
    message_devops(
        sender=get_sender(),
        subject=subject,
        # Including subject to the message because `subject` doesn't show up in Slack
        message=f'{subject}  {f"with user message `{message}`" if message else ""}\r\n{link}',
    )

    if environment.Env.APPLICATION_ENVIRONMENT != 'production':
        return jsonify(cw_logging.AuditEventSchema(many=True).dump(es_search_result['products']))
    return make_response('', 204)


class SupportEmailSchema(GwioSchema):
    """
    name: message to support
    description: e-mail message to support
    """
    subject = fields.Str()
    body = fields.Str()
    shop_guid = fields.UUID(required=False)
    g_recaptcha_response = fields.Str(required=False)
    reply_to = fields.Email(required=False)


@app.register_endpoint(
    None,
    route="/issues/report",
    method=POST,
    input_schema=SupportEmailSchema,
    authorisation=None,
)
def send_support_email(subject, body, shop_guid=None, g_recaptcha_response=None, reply_to=None):
    """
    name: send email to support
    description: send an email message to support
    """

    if shop_guid is None:
        recipient = Env.SUPPORT_EMAIL
    else:
        recipient = Webshop.get(guid=shop_guid).email

    if g.user is None:
        if reply_to is None:
            raise Conflict("anonymous user must provide a reply_to address")
        elif not recaptcha.validate(g_recaptcha_response, "support"):
            raise Forbidden
    else:
        reply_to = g.user.email

    EMail().send(
        src=Env.SYSTEM_EMAIL,
        dst=recipient,
        subject=subject,
        text=body,
        reply_to=reply_to
    )
    return make_response('', 204)
