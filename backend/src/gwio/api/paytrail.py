"""
Module containing the end points required for Paytrail integration.
Flow for Paytrail payments:
1. Customer is directed to a gwio payment page
2. Payment page requests the required form data input including a checksum calculated with the order data and a secret
   from the backend providing the success and canceled pages to redirect after the payment (POST /paytrail/)
3. Payment page runs a paytrail js script to initiate the payment
4. Paytrail either successfully finishes the payment or the user cancels the payment. The user is redirected to the
   associated gwio page
5. Paytrail issues a GET /notify/ with the relevant payment notification data providing the outcome of the payment and
   information about the payment (order number and such) and a checksum calculated over the data and a secret

Official Paytrail documentation can be found at https://docs.paytrail.com/en/index.html
"""
import json
import uuid
from datetime import datetime
from decimal import Decimal
from logging import getLogger
from urllib.parse import urljoin

from flask import (
    make_response,
    request,
)
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.core.cw_logging import LogEventType, Event
from gwio.ext.marshmallow import GwioSchema
from gwio.logic.payments.handler import (
    PaymentAction,
)
from gwio.logic.payments.paytrail import Paytrail
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.models.orders import Order
from gwio.models.payment_attribute import (
    PaymentData,
    PaytrailData,
)
from gwio.models.payment_method import PaymentMethod
from gwio.models.webshops import Webshop

log = getLogger(__name__)

NOTIFY_ROUTE = '/paytrail/_notify'


class PopulatedFormData(GwioSchema):
    MERCHANT_ID = fields.Str()
    CURRENCY = fields.Str()
    URL_SUCCESS = fields.Str()
    URL_CANCEL = fields.Str()
    ORDER_NUMBER = fields.Str()
    AMOUNT = fields.Str()
    PARAMS_OUT = fields.Str()
    ALG = fields.Str()
    URL_NOTIFY = fields.Str()
    LOCALE = fields.Str()
    MSG_UI_MERCHANT_PANEL = fields.Str()
    PARAMS_IN = fields.Str()
    AUTHCODE = fields.Str()


class PaytrailFormData(GwioSchema):
    order_id = fields.UUID()
    paytrail_form_data = fields.Nested(PopulatedFormData)


def _customer(identity_guid, **_):
    """Requires customer credentials"""
    # identity_id = f'{Env.AWS_REGION}:{identity_id}'
    if not rbac.customer(identity_id=identity_guid):
        raise NotFound


# pylint: disable=no-member
# TODO: is this order?
@app.register_endpoint(
    Order,
    route='/customers/<uuid:identity_guid>/orders/<uuid:guid>/paytrail/',
    method=GET,
    output_schema=PaytrailFormData,
    authorisation=_customer,
)
def paytrail_form_data(order, identity_guid):
    """Get paytrail form data"""
    assert identity_guid  # Used by the authorization
    if not rbac.customer(identity_id=order.buyer_identity_id):
        raise NotFound

    payment_method = PaymentMethod.get(guid=order.payment_method_guid)
    if payment_method.type != PaymentTypes.PAYTRAIL:
        log.warning('Attempting paytrail payment on a non paytrail order')
        raise Conflict

    data = dict(order_id=order.guid)
    data['paytrail_form_data'] = populate_payment_form_data_set(
        order=order,
        url_notify=urljoin(request.url_root, NOTIFY_ROUTE),
        url_success=order.data.paytrail.urls.success,
        url_cancel=order.data.paytrail.urls.cancel)
    return data


class PaytrailNotify(GwioSchema):
    """
    name: PayTrail webhook
    description: webhook input data for notifications from PayTrail (https://docs.paytrail.com/en/index-all.html)
    """
    ORDER_NUMBER = fields.Str(required=True)
    PAYMENT_ID = fields.Str(required=True)
    AMOUNT = fields.Str(required=True)
    TIMESTAMP = fields.Str(required=True)
    STATUS = fields.Str(required=True)
    RETURN_AUTHCODE = fields.Str(required=True)


@app.register_endpoint(
    Order,
    autoload=False,
    route=NOTIFY_ROUTE,
    method=GET,
    input_schema=PaytrailNotify,
    output_schema=None,
    authorisation=None,
)
def paytrail_notify_completed(**kwargs):
    """Paytrail notify callback end point"""
    order = Order.get(guid=uuid.UUID(kwargs['ORDER_NUMBER']))
    shop = Webshop.get(guid=order.shop_guid)
    # pylint: disable=W0212
    paytrail = Paytrail(merchant_secret=shop._private_data['paytrail']['hash'])
    paytrail_notify_data = dict(order_number=kwargs['ORDER_NUMBER'],
                                payment_id=kwargs['PAYMENT_ID'],
                                amount=kwargs['AMOUNT'],
                                timestamp=kwargs['TIMESTAMP'],
                                status=kwargs['STATUS'],
                                return_authcode=kwargs['RETURN_AUTHCODE'])
    if paytrail.validate_return_authcode(**paytrail_notify_data):
        if kwargs['STATUS'] == 'PAID':
            try:
                Event(
                    event_type=LogEventType.PAYMENT_FINALIZE,
                    obj=order,
                    data=paytrail_notify_data,
                ).emit()
            except Exception as e:  # pylint: disable=broad-except
                log.exception('Failed to log the payment information "%s" for order %s: %s', kwargs['PAYMENT_ID'], order.guid, str(e))

            if len(order.payments) > 1:
                raise NotImplementedError('Multiple payments per order not supported')

            payment = order.payments[0]
            payment.datetime = datetime.now()
            paytrail_data = PaytrailData(**dict(notify=json.dumps(kwargs)))
            payment_data = PaymentData(**dict(paytrail=paytrail_data))
            payment._data = payment_data
            payment.process(PaymentAction.CHARGE)
            order.save()
        return make_response('', 200)
    log.error('Paytrail validation notify failed with "%s"', kwargs)
    raise BadRequest


def populate_payment_form_data_set(order: Order,
                                   url_notify: str,
                                   url_success: str,
                                   url_cancel: str) -> dict:
    """Helper function to populate the paytrail payment form data.

    :param order: Order model instance.
    :param url_notify: Notify endpoint where paytrail will send a notify request after processing the payment.
    :param url_success: Success endpoint where paytrail will redirect end-user after successfully processing the payment.
    :param url_cancel: Cancel endpoint where paytrail will redirect end-user if the payment is canceled.
    :return: Dictionary of the paytrail form data
    """
    shop = Webshop.get(guid=order.shop_guid)

    paytrail = Paytrail(
        # pylint: disable=W0212
        merchant_secret=shop._private_data['paytrail']['hash'],
        # pylint: disable=W0212
        merchant_id=shop._private_data['paytrail']['id'],
        url_success=url_success,
        url_cancel=url_cancel,
        order_number=order.guid,
        amount="{:0.2f}".format(Decimal(order.total_price)),
        url_notify=url_notify,
        locale='fi_FI',
        msg_ui_merchant_panel=f'Order {order.guid} by Customer {order.buyer_identity_id}',
    )
    return paytrail.generate_payment_form_data_set()
