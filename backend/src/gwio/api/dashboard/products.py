# -*- coding: utf-8 -*-

import logging
from datetime import (
    datetime,
    timezone,
)
from decimal import Decimal
from functools import partial

import elasticsearch_dsl
from flask import (
    g,
    make_response,
)
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.cw_logging import (
    Event,
    LogEventType,
)
from gwio.core.decorators.api import (
    POST,
    authorise,
)
from gwio.ext.eshelpers import (
    BARE_PRODUCT_ES_INDEX,
    QueryDDLSchema,
    WEBSHOP_PRODUCT_ES_INDEX,
    do_es_search,
    es_search,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.pricing import (
    Pricing,
)
from gwio.models.products import Product
from gwio.models.tags import Tag
from gwio.models.webshop_product import (
    ProductSearchResponseSchema,
    WebshopProduct,
)
from gwio.operations.products import organization_product_create

log = logging.getLogger(__name__)

_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid')


class WebshopProductAddResultSchema(GwioSchema):
    product_guid = fields.UUID()
    product = fields.Nested(Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet())
    status_code = fields.Integer()


class WebshopProductAddResultsSchema(GwioSchema):
    results = fields.Nested(WebshopProductAddResultSchema, many=True)


@app.register_resource
@app.base_path('/dashboard')
@app.path_params('/products', 'uuid:guid')
class ProductsAPI(MethodResource):
    """
    Resource for Products
    """

    @staticmethod
    def _conflict(description=None):
        raise Conflict(description=description)

    # @cached('1h') - Disabled as a workaround for #348
    @app.register_endpoint(
        Product,
        index=Product.by_guid,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
        authorisation=(rbac.admin, rbac.shopkeeper),
    )
    def get(self, *, product):
        """Get product description for product identified by 'product_id'"""
        try:
            return Product.API.Assessor.evaluate(product)
        except Product.DoesNotExist:
            raise NotFound


@app.register_resource
@app.base_path('/dashboard')
@app.path_params('/webshops', 'uuid:webshop_guid', '/products', 'uuid:product_guid')
# pylint: disable=no-self-use
class DashboardWebshopProductsAPI(MethodResource):
    """
    Resource for webshop products
    """

    @app.register_endpoint(
        WebshopProduct,
        autoload=False,
        output_schema=WebshopProductAddResultsSchema,
        authorisation=_admin_or_shopkeeper,
    )
    def post(self, webshop_guid, products):
        """
        summary: Add the products to the webshop
        description: Enables / adds the product "for sale" in the given shop with optional shop specific (pricing) info.
        tags:
            - Product
            - Webshop
            - DeliveryMethod
        """
        response = list()
        tags_cache = dict()
        for item in products:
            # If we're creating a new Product, we need to log the creation since the automatically created log entry is for creation of WebshopProduct
            if 'product' in item:
                try:
                    tag_guids = item['product'].pop('tag_guids')
                except KeyError:
                    pass
                else:
                    item['product']['tags'] = list()
                    for tag_guid in tag_guids:
                        try:
                            tag = tags_cache[tag_guid]
                        except KeyError:
                            tag = Tag.by_guid(tag_guid)
                            tags_cache[tag_guid] = tag
                        item['product']['tags'].append(tag)

                product = organization_product_create(webshop_guid, **item['product'])
                product_guid = product.guid
                Event(
                    event_type=Event.API.Type.CREATE,
                    subject=g.user,
                    obj=product,
                    data=product.Schema().dump(item['product'])
                ).emit()
            else:
                product_guid = item['product_guid']
                try:
                    product = Product.by_guid(product_guid)
                except Product.DoesNotExist:
                    response.append(dict(product_guid=product_guid, status_code=404))
                    continue
                try:
                    WebshopProduct.get(product_guid=product.guid, webshop_guid=webshop_guid)
                    response.append(dict(product_guid=product.guid, status_code=409))
                    continue
                except WebshopProduct.DoesNotExist:
                    pass

            try:
                params = dict(
                    product_guid=product.guid,
                    webshop_guid=webshop_guid,
                    pricing_guid=item.get('pricing_guid'),
                    stock_level=item.get('stock_level', Decimal(0)),
                    for_sale=item.get('for_sale')
                )
                try:
                    params['archived'] = item['archived']
                except KeyError:
                    pass
                webshop_product = WebshopProduct(**params)
                webshop_product.save()
                try:
                    response.append(dict(product_guid=item['product_guid'], status_code=201))
                except KeyError:
                    evaluated = WebshopProduct.API.Assessor.evaluate(product=product, webshop_product=webshop_product, webshop_guid=webshop_guid)
                    response.append(dict(product=evaluated, status_code=201))
            except Pricing.DoesNotExist:
                response.append(dict(product_guid=product_guid, status_code=404))
            except (KeyError, ValueError):
                response.append(dict(product_guid=product_guid, status_code=400))

        return dict(results=response)

    @app.register_endpoint(
        WebshopProduct,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet,
        authorisation=_admin_or_shopkeeper,
    )
    def put(self, webshop_product):
        """
        summary: Change the pricing rule or stock level used for product in given shop
        description: Change the pricing rule or stock level for product in shop `webshop_guid` for the product identified by `product_guid`
        tags:
            - Product
            - Webshop
            - DeliveryMethod
        """
        try:
            product = Product.by_guid(webshop_product.product_guid)
            webshop_product.save()
        except (WebshopProduct.DoesNotExist, Product.DoesNotExist):
            raise NotFound

        return WebshopProduct.API.Assessor.evaluate(product=product, webshop_guid=webshop_product.webshop_guid)

    @app.register_endpoint(
        WebshopProduct,
        authorisation=_admin_or_shopkeeper,
    )
    def delete(self, webshop_product):
        """
        summary: Remove the product ("product_guid") from the shop ("webshop_guid").
        description: This makes the product no longer available for purchase in the given shop. It does not remove the product itself from product database.
        tags:
            - Product
            - Webshop
            - DeliveryMethod
            """
        try:
            if webshop_product.archived is not None:
                return make_response('', 204)
            webshop_product.archived = datetime.now(timezone.utc)
            webshop_product.save()
        except AttributeError:
            raise NotFound("Payment method not found")
        return make_response('', 204)


_admin_or_shopkeeper_or_brand_owner = (rbac.admin, rbac.shopkeeper, rbac.product_owner)


@app.register_endpoint(
    Product,
    autoload=False,
    route='/dashboard/products/_select',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=QueryDDLSchema,
    output_schema=ProductSearchResponseSchema,
    authorisation=_admin_or_shopkeeper_or_brand_owner,
)
def dashboard_select_products(dsl=None, **_):
    """
    summary: Return list of all products matching the the query. Uses ElasticSearch query DSL.
    description: If pagination is required pass the `sort` key with parameter[s] to sort by. If the `total` in response is greater
        than the number of items in returned `products` the next page can be fetched by giving `search_after` parameter the
        value of the last product's `sort` key in the dsl.
        https://www.elastic.co/guide/en/elasticsearch/reference/current//search-request-body.html#request-body-search-search-after

        NOTE! similar to the the /products/_query this will return all known products for admins. For shopkeepers/product owners
        only the products owned by the querying user's organization.
    tags:
        - Webshop
        - Product
    """
    if dsl is None:
        raise Conflict(description='Missing dsl')
    dsl = elasticsearch_dsl.Search.from_dict(dsl)
    if not rbac.admin():
        params = {'owner_guid': g.user.organization_guid}
        dsl = dsl.filter('term', **params)
    response = do_es_search(partial(es_search, index=BARE_PRODUCT_ES_INDEX, dsl=dsl))
    return response


@app.register_endpoint(
    Product,
    autoload=False,
    route='/dashboard/products/_query',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=QueryDDLSchema,
    output_schema=ProductSearchResponseSchema,
    authorisation=_admin_or_shopkeeper_or_brand_owner,
)
def dashboard_query_products(dsl=None, **_):
    """
    summary: Return list of products matching the the query. Uses ElasticSearch query DSL.
    description: If pagination is required pass the `sort` key with parameter[s] to sort by. If the `total` in response is greater
        than the number of items in returned `products` the next page can be fetched by giving `search_after` parameter the
        value of the last product's `sort` key in the dsl.
        https://www.elastic.co/guide/en/elasticsearch/reference/current//search-request-body.html#request-body-search-search-after

        NOTE! similar to the the /products/_query this will return all known products for admins. For shopkeepers/product owners
        only the products owned by the querying user's organization.
    tags:
       - Webshop
       - Product
    """
    if dsl is None:
        raise Conflict(description='Missing dsl')
    dsl = elasticsearch_dsl.Search.from_dict(dsl)
    # Organization people can only search their own products. Admin can see all products
    if not rbac.admin():
        params = {'owner_guid': g.user.organization_guid}
        dsl = dsl.filter('term', **params)
    result = do_es_search(partial(es_search, index=BARE_PRODUCT_ES_INDEX, dsl=dsl))
    return result


@app.register_endpoint(
    WebshopProduct,
    autoload=False,
    route='/dashboard/webshop_products/_query',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=QueryDDLSchema,
    output_schema=ProductSearchResponseSchema,
    authorisation=_admin_or_shopkeeper_or_brand_owner,
)
def dashboard_query_webshop_products(dsl=None, **_):
    """
    summary: Return list of webshop products matching the the query. Uses ElasticSearch query DSL.
    description: If pagination is required pass the `sort` key with parameter[s] to sort by. If the `total` in response is greater
        than the number of items in returned `products` the next page can be fetched by giving `search_after` parameter the
        value of the last product's `sort` key in the dsl.
        https://www.elastic.co/guide/en/elasticsearch/reference/current//search-request-body.html#request-body-search-search-after

        NOTE! similar to the the /products/_query this will return all known products for admins. For shopkeepers/product owners
        only the products owned by the querying user's organization.
    tags:
       - Webshop
       - Product
    """
    if dsl is None:
        raise Conflict(description='Missing dsl')
    dsl = elasticsearch_dsl.Search.from_dict(dsl)
    # Organization people can only search their own products. Admin can see all products
    if not rbac.admin():
        params = {'owner_guid': g.user.organization_guid}
        dsl = dsl.filter('term', **params)
    result = do_es_search(partial(es_search, index=WEBSHOP_PRODUCT_ES_INDEX, dsl=dsl))
    return result
