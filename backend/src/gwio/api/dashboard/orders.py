from decimal import Decimal

from marshmallow import (
    Schema,
    fields,
)
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    PUT,
    authorise,
)
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentState,
)
from gwio.logic.returns.handler import ReturnDeliveryAction
from gwio.models.orders import (
    Order,
    ReceivedReturn,
)

_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='shop_guid')


class ConfirmReceivedReturns(Schema):
    """
    name: receive order return
    description: the products and quantities to be confirmed as received
    """

    class _SubSchema(ReceivedReturn.Schemas.Post):
        product_guid = fields.UUID(required=True)

    received = fields.Nested(_SubSchema, many=True)


@app.register_endpoint(
    Order,
    index=Order.get_by_shop,
    route=f'/dashboard/webshops/<uuid:shop_guid>/orders/<uuid:{Order.Table.hash_key}>/product_return/<uuid:return_guid>',
    method=PUT,
    input_schema=ConfirmReceivedReturns,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def confirm_received_return_products(*, order, return_guid, received) -> Order:
    try:
        product_return = next(x for x in order.product_returns if x.guid == return_guid)
    except StopIteration:
        raise NotFound('Product return `{return_guid}` not found')
    for received_input in received:
        try:
            return_line = next(x for x in product_return.lines if x.product_guid == received_input['product_guid'])
            return_line.received.append(dict(qty=received_input['qty']))
        except StopIteration:
            # Product was not marked for return, but it was still returned with the
            product_return.lines.append(dict(
                product_guid=received_input['product_guid'],
                qty=Decimal('0'),
                received=[dict(qty=received_input['qty'])],
            ))
    product_return.process(ReturnDeliveryAction.CONFIRM_RETURN)
    order.save()
    return order


class RefundSchema(Schema):
    """
    name: refund
    description: amount to refund
    """
    amount = fields.Decimal(required=True)


REFUND_TOO_MUCH = 'Can not refund more than the customer paid'


@app.register_endpoint(
    Order,
    route=f'/dashboard/webshops/<uuid:shop_guid>/orders/<uuid:guid>/payments/_{PaymentAction.REFUND.value}',
    method=PUT,
    input_schema=RefundSchema,
    output_schema=Order.Schemas.ShopkeeperGet,
    authorisation=_admin_or_shopkeeper,
)
def refund(*, order, amount, shop_guid) -> Order:
    """
    name: refund
    description: refunds the order's payment
    """
    payments_balance = sum(x.amount for x in order.payments if x.current_state == PaymentState.COMPLETED)

    if payments_balance < amount:
        raise Conflict(REFUND_TOO_MUCH)

    try:
        payment_to_refund = next(x for x in reversed(order.payments) if x.amount > 0)
    except StopIteration:
        raise Conflict('Nothing to refund')

    payment_to_refund.new_refund(amount=amount, webshop_guid=shop_guid)

    order.save()
    return order
