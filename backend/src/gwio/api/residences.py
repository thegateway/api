# The methods here are used to represent different HTTP request methods
# thus no-self-use is acceptable.
# pylint: disable=R0201
from datetime import (
    datetime,
    timezone,
)

from flask import make_response
from flask_apispec import MethodResource
from werkzeug.exceptions import (
    Conflict,
    Forbidden,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.core.cw_logging import LogEventType
from gwio.models.residence import (
    OwnershipType,
    Residence,
)


def _admin_or_customer(owner_identity_guid, **_):
    """Requires admin or customer credentials"""
    if not rbac.admin() and not rbac.customer(identity_id=owner_identity_guid):
        raise Forbidden


@app.register_endpoint(
    Residence,
    index=Residence.get_by_owner,
    route='/customers/<uuid:owner_identity_guid>/residences/',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=Residence.Schemas.Get(many=True),
    authorisation=_admin_or_customer,
    query_params=dict(archived=True),
)
def list_customer_residences(residences):
    """Get list of the given customer's residences"""
    return residences


@app.register_endpoint(
    Residence,
    autoload=False,
    route='/residences',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=Residence.Schemas.Get(many=True),
    input_schema=Residence.Schemas.ListResidences,
)
def list_residences(include_archived=False):
    """Get list of residences"""
    if include_archived:
        return Residence.scan()
    return Residence.scan(archived__not_exists=True)


@app.register_resource
@app.path_params('uuid:guid')
class ResidencesAPI(MethodResource):
    """CRUD operations for  residences"""

    @app.register_endpoint(
        Residence,
        authorisation=rbac.ALL,
    )
    def get(self, residence):
        """Get residence by guid"""
        return residence

    @app.register_endpoint(
        Residence,
        authorisation=rbac.ALL,
        description=f"""Create a new residence.
                        Possible ownership values are {','.join([t.value for t in list(OwnershipType)])}""",
    )
    def post(self, residence):
        """Create new residence."""
        residence.save()
        return residence

    @app.register_endpoint(
        Residence,
        input_schema=Residence.Schemas.Put(partial=True),
        authorisation=rbac.ALL,
    )
    def put(self, residence):
        """Update the given residence's information"""
        residence.save()
        return residence

    # TODO: delete or update? the residence is not deleted after all
    @app.register_endpoint(
        Residence,
        authorisation=rbac.ALL,
    )
    def delete(self, residence):
        """Archive the given residence."""
        if residence.archived is not None:
            raise Conflict(f"Residence {residence.guid} already archived")
        residence.archived = datetime.now(tz=timezone.utc)
        residence.save()
        return make_response('', 204)
