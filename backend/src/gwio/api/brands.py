"""
API for operating on brands.
"""
from flask_apispec import MethodResource
from werkzeug.exceptions import (
    Conflict,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
    authorise,
)
from gwio.core.cw_logging import LogEventType
from gwio.models.brands import Brand
from gwio.models.tags import (
    TagType,
    Tag,
)


@app.register_endpoint(
    Brand,
    index=Brand.scan,
    route='/brands/',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=Brand.Schemas.Get(many=True),
)
def list_brands(brands):
    """List all brands"""
    return brands


_admin_or_product_owner = authorise(rbac.admin, rbac.product_owner, guid_name='brand_guid')


# pylint: disable=no-self-use
@app.register_resource
@app.path_params(f'uuid:{Brand.Table.hash_key}')
class BrandsAPI(MethodResource):
    @app.register_endpoint(
        Brand,
        authorisation=None,
    )
    def get(self, brand):
        """Get a brand by GUID"""
        return brand

    @app.register_endpoint(Brand)
    def post(self, *, brand):
        """Create a new brand"""
        try:
            Brand.get(guid=brand.guid)
            raise Conflict(description='Brand ({uuid}) already exists'.format(uuid=brand.guid))
        except Brand.DoesNotExist:
            pass
        tag = Tag(type=TagType.BRAND, name=brand.name, owner=brand.guid)
        tag.save()
        brand.save()
        return brand, 201

    @app.register_endpoint(
        Brand,
        authorisation=_admin_or_product_owner,
    )
    def put(self, *, brand):
        """Modify an existing brand"""
        brand.save()
        return brand

    @app.register_endpoint(
        Brand,
        authorisation=_admin_or_product_owner,
    )
    def delete(self, brand):
        """Delete existing brand"""
        brand.delete()
        return 204
