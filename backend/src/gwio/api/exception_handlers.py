import os
import traceback

from flask import (
    g,
    jsonify,
    request,
)
from transitions import MachineError
from werkzeug.exceptions import (
    InternalServerError,
)

from gwio.utils.aws import message_devops
from gwio.environment import Env
from gwio.ext.dynamorm import HotfixMixIn
from .flask_app import app


class BadDSLException(Exception):
    def __init__(self, cause_error):
        self._error = cause_error
        super().__init__()

    def __str__(self):
        return f'Badly formed DSL `{self._error}`'


@app.errorhandler(BadDSLException)
def handle_bad_dsl_exist(error):
    status_code = 400
    response = jsonify(dict(
        code=status_code,
        error="400 Bad Request",
        message=str(error),
    ))
    response.status_code = status_code
    return response


@app.errorhandler(HotfixMixIn.DoesNotExist)
def handle_does_not_exist(error):
    status_code = 404
    try:
        message = error.args[0]
    except IndexError:
        message = 'Does not exist'
    response = jsonify(dict(
        code=status_code,
        error="404 Conflict",
        message=message,
    ))
    response.status_code = status_code
    return response


@app.errorhandler(MachineError)
def handle_machine_error(error):
    status_code = 409
    response = jsonify(dict(code=status_code,
                            error="409 Conflict",
                            message=error.value))
    response.status_code = status_code
    return response


# pylint: disable=unused-argument
@app.errorhandler(InternalServerError)
def internal_server_error(e):
    try:
        error_id = g.aws_request_id
    except AttributeError:
        g.aws_request_id = request.headers.get('X-Amazon-Request-ID')
        error_id = g.aws_request_id
    lambda_name = os.environ.get('AWS_LAMBDA_FUNCTION_NAME', None)
    if lambda_name is not None:
        # noinspection PyBroadException
        # pylint: disable=broad-except
        try:
            trace = f'Server Error in {lambda_name}\n\n' + traceback.format_exc()
            message_devops(sender=f'{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}', subject=f'Internal server error {error_id}', message=trace)
        except Exception:  # Not allowed to fail here
            pass

    message = f"Internal Server Error from {lambda_name}"
    response = jsonify({'message': message, 'request_id': str(error_id)})
    response.status_code = 500
    return response
