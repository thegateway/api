# -*- coding: utf-8 -*-
import logging
from datetime import (
    datetime,
    timezone,
)

from flask import make_response
from flask_apispec import MethodResource
from werkzeug.exceptions import (
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.core.cw_logging import LogEventType
from gwio.models.payment_method import PaymentMethod
from gwio.models.webshops import Webshop

log = logging.getLogger(__name__)


@app.register_endpoint(
    PaymentMethod,
    route='/payment_methods/',
    method=GET,
    event_type=LogEventType.LIST,
    index=PaymentMethod.scan,
    input_schema=PaymentMethod.Schemas.GetInput,
    output_schema=PaymentMethod.Schemas.Get(many=True),
    authorisation=(rbac.admin, rbac.shopkeeper),
)
def list_all_payment_methods(payment_methods):
    """Return all the available payment methods"""
    return payment_methods


# pylint: disable=no-self-use
@app.register_resource
@app.base_path('/payment_methods')
@app.path_params('uuid:guid')
class PaymentMethodsAdminAPI(MethodResource):
    """
    Resource for PaymentMethods
    """

    @app.register_endpoint(PaymentMethod,
                           authorisation=None)
    def get(self, payment_method):
        """ Get payment method information """
        return payment_method

    @app.register_endpoint(PaymentMethod)
    def post(self, payment_method):
        """Create a new payment method"""
        payment_method.save()
        return payment_method, 201

    @app.register_endpoint(PaymentMethod)
    def put(self, payment_method):
        """Update existing payment method"""
        payment_method.save()
        return payment_method

    @app.register_endpoint(PaymentMethod)
    def delete(self, payment_method):
        """Archive the given payment method."""
        try:
            if payment_method.archived is not None:
                return make_response('', 204)
            payment_method.archived = datetime.now(timezone.utc)
            payment_method.save()
            for shop in Webshop.scan():
                for item_guid in shop.payment_method_guids:
                    if item_guid == payment_method.guid:
                        shop.payment_method_guids.remove(item_guid)
                        shop.save()
        except AttributeError:
            raise NotFound("Payment method not found")
        return make_response('', 204)
