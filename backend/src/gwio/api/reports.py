"""
API endpoints for various reporting tools.

"""
import logging

from marshmallow import (
    fields,
)
from werkzeug.exceptions import Forbidden

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.webshops import Webshop
from gwio.operations.reports import generate_sales_report

logger = logging.getLogger(__name__)


class PriceSchema(GwioSchema):
    """
    Schema for nesting sums with VAT info.
    """
    amount = fields.Decimal()
    vat_class = fields.Int()


class SalesByProductTypeSchema(GwioSchema):
    product_type = fields.Str()
    vatless = fields.Decimal()
    vat = fields.Decimal()


class BookkeepingQuerySchema(GwioSchema):
    """
    name: query
    description: query parameters for the bookkeeping report to generate.
    """
    start_date = fields.Date(required=False, default=None)
    end_date = fields.Date(required=False, default=None)


# TODO: Should object be report?
@app.register_endpoint(
    Webshop,
    route='/reports/',
    index=Webshop.scan,
    method=GET,
    output_schema=None,
    input_schema=BookkeepingQuerySchema,
)
def admin_sales_report(webshops, start_date=None, end_date=None):
    """Return sales reports"""
    return generate_sales_report(webshops, end_date, start_date)


def _shopkeeper(webshop_guid):
    """Requires shopkeeper credentials"""
    if not rbac.shopkeeper(webshop_guid):
        raise Forbidden


# TODO: Should object be report?
@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:webshop_guid>/reports/',
    method=GET,
    output_schema=None,
    input_schema=BookkeepingQuerySchema,
    authorisation=_shopkeeper,
)
def webshop_sales_report(webshop_guid, start_date=None, end_date=None):
    """Return sales reports"""
    return generate_sales_report((Webshop.get(guid=webshop_guid),), end_date, start_date)
