import hashlib
import logging
import uuid

import flask
from flask import g
from marshmallow import (
    Schema,
    fields,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
)

from gwio.api.flask_app import app
from gwio.core import (
    cw_logging,
    rbac,
)
from gwio.core.decorators.api import (
    GET,
    POST,
    authorise,
)
from gwio.core.utils.payu.payu import (
    PayuOrderStatus,
    SubmerchantDoesNotExist,
)
from gwio.environment import Env
from gwio.ext.dynamorm import (
    GwioModelField,
    NestedSchemaModel,
)
from gwio.ext.marshmallow import (
    GwioCurrencyField,
    GwioSchema,
)
from gwio.logic.payments import payu
from gwio.logic.payments.handler import PaymentAction
from gwio.models.orders import Order
from gwio.models.webshops import Webshop
from gwio.utils.aws import (
    lambda_function_name,
    message_devops,
)

LogEventType = cw_logging.LogEventType
log = logging.getLogger(__name__)

SIGNATURE_ALGORITHMS = dict(
    md5=lambda pt: hashlib.md5(pt.encode('utf-8')).hexdigest()
)


class PayuWebhook(Schema):
    """
    name: PayU callback input
    description: Schema for PayU callback input
    """
    order = fields.Dict()
    localReceiptDateTime = fields.Str()  # Really Datetime but saving is easier this way
    properties = fields.List(fields.Dict())


@app.register_endpoint(
    Order,
    autoload=False,
    route='/payu/webhook',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=PayuWebhook,
    output_schema=None,
    authorisation=None,
)
def payu_webhook(**kwargs):
    """
    Webhook accepting notifications about PayU Order Events.
    The events indicate how the payment proceeds and possibly nudges `Payment`s state forward.
    """
    try:
        openpayu_signature = flask.request.headers['OpenPayu-Signature']
    except KeyError:
        msg = 'No OpenPayu-Signature header set for PayU webhook'
        log.error(msg)
        message_devops(sender=lambda_function_name(), subject='PayU webhook', message=msg, ignore_errors=True)
        raise BadRequest('`OpenPayu-Signature` missing')
    fields = {f.split('=')[0].strip(): f.split('=')[1].strip() for f in openpayu_signature.split(';')}

    input_signature = fields['signature']
    # Use raw input body for signature verification
    signature_input = flask.request.data.decode('utf-8') + Env.PAYU_SECOND_KEY  # second key
    calculated_signature = hashlib.md5(signature_input.encode('utf-8')).hexdigest()
    if input_signature != calculated_signature:
        msg = 'PayU notification signatures do not match'
        log.error(msg)
        message_devops(sender=lambda_function_name(), subject='PayU webhook', message=msg, ignore_errors=True)
        return '', 200  # PayU always expects 200

    order_guid = kwargs['order']['extOrderId']
    try:
        order = Order.get(guid=uuid.UUID(order_guid))
    except Order.DoesNotExist:
        msg = f'Got PayU notification for non existent order {order_guid}'
        log.error(msg)
        message_devops(sender=lambda_function_name(), subject='PayU webhook', message=msg, ignore_errors=True)
        return '', 200

    g.log_events_util.audit_object = order
    try:
        payment = order.payments[0]
    except IndexError:
        msg = f'Got notification from PayU for order {order.guid} but no payments are saved for the order'
        log.error(msg)
        message_devops(sender=lambda_function_name(), subject='PayU webhook', message=msg, ignore_errors=True)
        return '', 200

    if not payment:
        msg = f'Got notification from PayU for order {order.guid} but no payments are saved for the order'
        log.error(msg)
        message_devops(sender=lambda_function_name(), subject='PayU webhook', message=msg, ignore_errors=True)
        return '', 200

    if PayuOrderStatus[kwargs['order']['status']] == PayuOrderStatus.PENDING:
        payment.data.payu_order_data = kwargs
    elif PayuOrderStatus[kwargs['order']['status']] == PayuOrderStatus.WAITING_FOR_CONFIRMATION:
        payment.data.waiting_for_confirmation_data = kwargs
        payment.process(PaymentAction.CHARGE)
    elif PayuOrderStatus[kwargs['order']['status']] == PayuOrderStatus.COMPLETED:
        payu.do_finalize_payment(payment=payment, **kwargs)
    elif PayuOrderStatus[kwargs['order']['status']] == PayuOrderStatus.CANCELED:
        # I don't think we need to do anything at this point as the order has not been charged and captured yet
        pass
    else:
        pass  # no-op ?
    payment.order.save()

    return '', 200


class PayuRegistrationLinkResponseSchema(GwioSchema):
    """
    name: PayU registration form link response
    description: Link to PayU registration form
    """
    link_url = fields.Url(required=True)


class PayuRegistrationLinkQuerySchema(GwioSchema):
    """
    name: PayU registration form link query schema
    description: Optionally provide language for the registration form
    """
    language = fields.Str()


@app.register_endpoint(
    Webshop,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/payment_methods/payu/registration_form_link',
    method=GET,
    event_type=LogEventType.READ,
    input_schema=PayuRegistrationLinkQuerySchema,
    output_schema=PayuRegistrationLinkResponseSchema,
    authorisation=authorise(rbac.shopkeeper, guid_name='webshop_guid'),
)
def payu_registration_form_link(webshop_guid: uuid.UUID, language=None):
    return dict(link_url=payu.register_form_link(webshop_guid, language))


class PayuMerchantStatusResponseSchema(GwioSchema):
    """
    name: PayU merchant status response schema
    description: Merchant status description
    """
    created = fields.Bool()
    verified = fields.Bool()
    name = fields.Str(allow_none=True)
    tax_id = fields.Str(allow_none=True, data_key='taxId')
    regon = fields.Str(allow_none=True)


class PayuMerchantStatusRequestSchema(GwioSchema):
    """
    name: PayU merchant status request schema
    description: Optionally provide currency for the merchant status query. Defaults to PLN
    """
    currency = fields.Str()


@app.register_endpoint(
    Webshop,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/payment_methods/payu/merchant_status',
    method=GET,
    event_type=LogEventType.READ,
    input_schema=PayuMerchantStatusRequestSchema,
    output_schema=PayuMerchantStatusResponseSchema,
    authorisation=authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid'),
)
def merchant_status(webshop_guid: uuid.UUID):
    return payu.get_merchant_status(webshop_guid)


class PayuMerchantBalanceResponseSchema(GwioSchema):
    """
    name: PayU merchant balance response schema
    description: PayU merchant balance separated into total balance, available balance and amount that can be paid out
    """
    total_amount = fields.Decimal()
    available_amount = fields.Decimal()
    amount_available_for_payout = fields.Decimal()


@app.register_endpoint(
    Webshop,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/payment_methods/payu/balance',
    method=GET,
    event_type=LogEventType.READ,
    output_schema=PayuMerchantBalanceResponseSchema,
    authorisation=authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid'),
)
def merchant_balance(webshop_guid: uuid.UUID):
    try:
        return payu.get_merchant_balance(webshop_guid)
    except SubmerchantDoesNotExist:
        raise Conflict(f'Submerchant {webshop_guid} does not exist in PayU')


class PayuMerchantPayoutRequestSchema(GwioSchema):
    """
    name: PayU merchant payout request schema
    description: PayU merchant payout request schema to indicate payout amount
    """
    amount = fields.Decimal(required=True, allow_none=False)
    currency = GwioCurrencyField(required=True)
    description = fields.Str()


class _PayuPayoutSchema(GwioSchema):
    payout_id = fields.String()
    full_payout = fields.Boolean()
    amount = fields.Decimal()
    currency = GwioCurrencyField()


class PayoutSchema(NestedSchemaModel):
    Schema = _PayuPayoutSchema

    class Schemas:
        Get = _PayuPayoutSchema


class PayuMerchantPayoutResponseSchema(GwioSchema):
    """
    name: PayU merchant payout response schema
    description: PayU merchant payout response schema to indicate paid out amount and balance left
    """
    balance = fields.Nested(PayuMerchantBalanceResponseSchema)
    payout = GwioModelField(PayoutSchema)


@app.register_endpoint(
    Webshop,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/payment_methods/payu/payout',
    method=POST,
    event_type=LogEventType.READ,
    input_schema=PayuMerchantPayoutRequestSchema,
    output_schema=PayuMerchantPayoutResponseSchema,
    authorisation=authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid'),
)
def merchant_payout(webshop_guid: uuid.UUID, **kwargs):
    try:
        return payu.merchant_payout(webshop_guid, **kwargs)
    except SubmerchantDoesNotExist:
        raise Conflict(f'Submerchant {webshop_guid} does not exist in PayU')
