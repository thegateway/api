import logging
import uuid
from typing import (
    Optional,
    List,
)

import flask
import stripe
from flask import (
    g,
    make_response,
    request,
)
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from stripe.error import StripeError
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    Forbidden,
    InternalServerError,
    NotFound,
)

import gwio.user
from gwio.api.flask_app import app
from gwio.core import (
    rbac,
    cw_logging,
)
from gwio.core.decorators.api import (
    GET,
    POST,
    authorise,
)
from gwio.environment import Env
from gwio.ext.marshmallow import GwioSchema
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentState,
)
from gwio.logic.payments.stripe import (
    accept_tos,
    create_custom_account,
    custom_account_link,
    add_external_account,
    requires_action,
)
from gwio.models.orders import Order
from gwio.models.webshops import Webshop

User = gwio.user.User
LogEventType = cw_logging.LogEventType

log = logging.getLogger(__name__)


class StripeCard(GwioSchema):
    id = fields.String()
    name = fields.String()
    brand = fields.String()
    exp_month = fields.Integer()
    exp_year = fields.Integer()
    last4 = fields.String()


def _customer(identity_id, **_):
    """Requires customer credentials"""
    if not rbac.customer(identity_id=identity_id):
        raise Forbidden


@app.register_endpoint(
    User,
    route='/customers/<uuid:identity_id>/stripe_cards/',
    method=GET,
    event_type=LogEventType.LIST,
    index=User.by_identity_id,
    output_schema=StripeCard(many=True),
    authorisation=_customer,
)
def list_stripe_cards(user):
    """
    summary: List user's stripe cards
    description: List all users stored cards at Stripe.
    security:
       - customer_auth
    """
    if user.stripe_id is None:
        return list()
    try:
        cards = stripe.Customer.retrieve(user.stripe_id, api_key=Env.STRIPE_API_KEY).sources.list()
    except StripeError as err:
        log.error('Failed to get stripe cards for user %s: %s', user.identity_id, str(err))
        raise Conflict
    return cards


class CardToken(GwioSchema):
    """
    name: card token
    description: a Stripe card token.
    """
    token = fields.String(required=True)


@app.register_resource
@app.base_path('/customers')
@app.path_params('uuid:identity_id', '/stripe_cards', 'string:card_id')
class CardsAPI(MethodResource):
    """
    Resource for stripe cards.
    """

    # pylint: disable=R0201
    @app.register_endpoint(
        User,
        output_schema=StripeCard,
        authorisation=_customer,
    )
    def get(self, user, card_id):
        """Get a Stripe card by the token"""
        if user.stripe_id is None:
            return NotFound
        try:
            card = stripe.Customer.retrieve(user.stripe_id, api_key=Env.STRIPE_API_KEY).sources.retrieve(card_id)
        except StripeError as err:
            log.error('Failed to get stripe card %s: %s', card_id, str(err))
            raise Conflict
        return card, 200

    # pylint: disable=R0201
    @app.register_endpoint(
        User,
        event_type=LogEventType.UPDATE,
        output_schema=StripeCard,
        input_schema=CardToken,
        automodify=False,
        authorisation=_customer,
    )
    def post(self, user, **kwargs):
        """Create Stripe card for the customer using the input token"""
        if user.stripe_id is None:
            try:
                stripe_customer = stripe.Customer.create(api_key=Env.STRIPE_API_KEY, description=str(user.guid))
                user.stripe_id = stripe_customer.id
            except StripeError as err:
                log.error('Failed to create stripe customer for user %s: %s', str(user.guid), err)
                raise Conflict
        try:
            customer = stripe.Customer.retrieve(user.stripe_id, api_key=Env.STRIPE_API_KEY)
            card = customer.sources.create(source=kwargs['token'])
        except StripeError as err:
            log.error('Failed to create stripe card: %s', str(err))
            raise Conflict
        return card, 201

    # pylint: disable=R0201
    @app.register_endpoint(
        User,
        event_type=LogEventType.UPDATE,
        authorisation=_customer,
    )
    def delete(self, user, card_id):
        """Delete the Stripe card with the given ID"""
        try:
            customer = stripe.Customer.retrieve(user.stripe_id, api_key=Env.STRIPE_API_KEY)
            customer.sources.retrieve(card_id).delete()
        except StripeError as err:
            log.error('Failed to delete stripe card: %s', str(err))
            raise Conflict
        return make_response('', 204)


class StripeWebhook(GwioSchema):
    """
    name: Stripe web-hook data.
    description: input data from a Stripe web-hook.
    """


@app.register_endpoint(
    Order,
    autoload=False,
    route='/stripe/webhook',
    method=POST,
    event_type=LogEventType.UPDATE,
    input_schema=StripeWebhook,
    output_schema=None,
    authorisation=None,
)
def stripe_webhook():
    """
    Webhook accepting notifications about Stripe Events.
    The events indicate how the payment proceeds and possibly nudges `Payment`s state forward.
    """
    # Function body is a modified version of stripe's webhook example
    # pylint: disable=no-member  # pylint apparently gets very confused from Order.get for some reason
    stripe_signature = flask.request.headers.get('Stripe-Signature')
    try:
        event = stripe.Webhook.construct_event(flask.request.data, stripe_signature, Env.STRIPE_ENDPOINT_SECRET).to_dict()
    except AttributeError as e:
        # Probably missing stripe endpoint secret
        log.error('Handling Stripe notification failed: %s. Missing STRIPE_ENDPOINT_SECRET?', e)
        raise InternalServerError('Failed calculating signature')
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        log.warning('Stripe webhook called with invalid signature: %s', e)
        raise BadRequest('Invalid signature')

    if event['type'] == 'payment_intent.amount_capturable_updated':
        intent = event['data']['object']
        # Fulfill the customer's purchase
        order_guid = intent['metadata']['order_guid']
        try:
            order = Order.get(guid=uuid.UUID(order_guid))
        except Order.DoesNotExist:
            log.error('Could not find order %s paid with stripe payment %s', str(order_guid), intent['id'])
            return "OK", 200

        g.log_events_util.audit_object = order
        try:
            payment = order.payments[0]
        except IndexError:
            log.error('Got webhook from stripe for order %s but no payments are saved for the order', order.guid)
            return "OK", 200

        if payment.current_state == PaymentState.WAITING_SCA:
            payment.data.sca_success = event
            payment.process(PaymentAction.CHARGE)
            if int(order.total_price * 100) != intent.amount_capturable:
                log.error("Order %s total price (%s) does not match stripe's capturable (%s)",
                          order.guid, int(order.total_price * 100), intent.amount_capturable)
            if payment.current_state != PaymentState.COMPLETED:
                log.error('Order %s payment status %s even after finalizing payment', order.guid, payment.current_state)
                return "OK", 200
        elif payment.current_state in (PaymentState.CHARGED, PaymentState.COMPLETED):
            log.info('Payment for order %s already charged or captured. No need for SCA veritifcation anymore', order.guid)
            return "OK", 200
        else:
            log.warning(
                'Unexpected payment status for Stripe webhook for order %s: %s.\nThis might be due to order not requiring SCA and being still captured',
                order.guid, payment.current_state
            )
            return "OK", 200

        order.save()
    elif event['type'] == 'payment_intent.payment_failed':
        log.error('Payment intent status `payment_failed` is not handled!')
        # TODO: Notify user
        # intent = event['data']['object']
        # error_message = intent['last_payment_error']['message'] if intent.get('last_payment_error') else None
        # Notify the customer that payment failed
        # user.notify()
    else:
        log.error('Unexpected payment intent status "%s" for order %s',
                  event['type'], event.get('data', {}).get('object', {}).get('metadata', {}).get('order_guid', {}))
        return 'Unexpected payment intent status', 200

    return "OK", 200


def _get_stripe_account_id(webshop: Webshop) -> Optional[str]:
    try:
        if webshop.private_data.stripe.account_id is not None:
            return webshop.private_data.stripe.account_id
    except AttributeError:
        pass
    return None


class StripeAccountLinkResponseSchema(GwioSchema):
    """
    name: Stripe account link response
    description: Link to stripe's account editing page
    """
    link_url = fields.Url(required=True)


class StripeAccountLinkRequestSchema(GwioSchema):
    """
    name: Stripe account link request
    description: Success and failure URLs for Stripe account link completing redirects.
                 Requested link type [custom_account_verification, custom_account_update]
    """
    success_url = fields.Url(required=True)
    failure_url = fields.Url(required=True)
    link_type = fields.String(required=True)
    capabilities = fields.List(fields.String())
    accept_tos = fields.Boolean(missing=False)


def accept_stripe_tos(webshop: Webshop) -> None:
    """
    Mark Stripe Terms of Service as accepted for the given webshop
    :param webshop: Webshop accepting the TOS
    """
    if webshop.private_data.stripe.account_id is None:
        raise BadRequest('No Stripe account for the given webshop')

    try:
        customer_ip = request.access_route[0]
    except IndexError:
        log.warning('Could not get customer ip address from `request.access_route')
        customer_ip = None
    user_agent = request.headers.get('User-Agent')
    accept_tos(account_id=webshop.private_data.stripe.account_id, ip=customer_ip, user_agent=user_agent, user=g.user, webshop=webshop)


@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/payment_methods/stripe/account_link',
    method=POST,
    event_type=LogEventType.READ,
    input_schema=StripeAccountLinkRequestSchema,
    output_schema=StripeAccountLinkResponseSchema,
    authorisation=authorise(rbac.shopkeeper, guid_name='webshop_guid'),
)
def stripe_account_link(webshop: Webshop, capabilities: List[str], accept_tos: bool, **kwargs):
    """
    Get a link to Stripe account verification/update form
    """
    if webshop.private_data.stripe.account_id is None:
        account = create_custom_account(country=Env.STRIPE_COUNTRY_CODE, email=webshop.email, capabilities=capabilities)
        try:
            webshop.private_data.stripe.account_id = account.id
        except AttributeError:
            webshop.private_data.stripe = dict(account_id=account.id)
        webshop.save()

    if accept_tos:
        accept_stripe_tos(webshop)

    # pylint: disable=missing-kwoa
    link_url = custom_account_link(account_id=webshop.private_data.stripe.account_id, **kwargs)
    return dict(link_url=link_url)


@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/payment_methods/stripe/accept_tos',
    method=POST,
    event_type=LogEventType.UPDATE,
    input_schema=None,
    output_schema=None,
    authorisation=authorise(rbac.shopkeeper, guid_name='webshop_guid'),
)
def stripe_account_accept_tos(webshop: Webshop):
    """
    Mark Stripe's Terms of Service accepted for the given webshop
    """
    try:
        if webshop.private_data.stripe.account_id is not None:
            accept_stripe_tos(webshop)
            return '', 200
    except AttributeError:
        pass
    raise Conflict(f'No Stripe account for webshop {webshop.guid}')


class ExternalAccountAddSchema(GwioSchema):
    """
    name: Stripe account external account connection
    description: Update the external account connection for a Stripe Custom account
    """
    external_account_token = fields.String(required=True)


@app.register_endpoint(
    Webshop,
    route='/webshops/<uuid:guid>/payment_methods/stripe/add_external_account',
    method=POST,
    event_type=LogEventType.UPDATE,
    input_schema=ExternalAccountAddSchema,
    output_schema=None,
    authorisation=authorise(rbac.shopkeeper, guid_name='webshop_guid'),
)
def stripe_update_payout(webshop, external_account_token):
    """
    Add new external account for the webshop's stripe account
    """
    account_id = _get_stripe_account_id(webshop)
    if account_id is None:
        return '', 204

    try:
        add_external_account(account_id=account_id,
                             external_account_token=external_account_token)
    except PermissionError:
        raise Forbidden("The requested Stripe account does not exist or you are not authorized to access it")

    return '', 200


class StripeRequiresActionSchema(GwioSchema):
    """
    name: Stripe fields requiring action from the shopkeeper
    description: Object containing the fields requiring action with additional description about timetables
    """
    # current_deadline, currently_due, disabled_reason, eventually_due, past_due, pending_verification
    current_deadline = fields.Int()
    currently_due = fields.List(fields.String())
    disabled_reason = fields.String()
    eventually_due = fields.List(fields.String())
    past_due = fields.List(fields.String())
    pending_verification = fields.List(fields.String())


@app.register_endpoint(
    Webshop,
    method=GET,
    route='/webshops/<uuid:guid>/payment_methods/stripe/requires_action',
    output_schema=StripeRequiresActionSchema,
    authorisation=authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid')
)
def required_actions(webshop):
    """
    Get information about the information required by Stripe to enable/continue payouts for the given webshop
    """
    account_id = _get_stripe_account_id(webshop)
    if account_id is None:
        return '', 204

    try:
        required_actions = requires_action(account_id=account_id)
    except PermissionError:
        raise Forbidden("The requested Stripe account does not exist or you are not authorized to access it")
    return required_actions
