# The methods here are used to represent different HTTP request methods
# thus no-self-use is acceptable.
# pylint: disable=R0201
from datetime import (
    datetime,
    timezone,
)

from flask import make_response
from flask_apispec import MethodResource
from werkzeug.exceptions import (
    BadRequest,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.core.cw_logging import LogEventType
from gwio.models.locations import Location
from gwio.models.tags import Tag


def check_parent_and_tag_guids(location):
    try:
        if location.parent is not None:
            try:
                Location.get(guid=location.parent)
            except Location.DoesNotExist:
                raise BadRequest('Parent guid is not valid')
    except KeyError:
        pass
    try:
        if location.tags is not None:
            for guid in location.tags:
                Tag.by_guid(guid)
    except KeyError:
        pass
    except Tag.DoesNotExist:
        raise BadRequest(description='Unknown tag guid')


@app.register_endpoint(
    Location,
    index=Location.scan,
    route='/locations/',
    method=GET,
    event_type=LogEventType.LIST,
    input_schema=Location.Schemas.ListQuery,
    output_schema=Location.Schemas.Get(many=True),
    authorisation=(rbac.admin, rbac.shopkeeper),
)
def list_locations(locations):
    """Get list of locations"""
    return locations


# pylint: disable=no-member
@app.register_resource
@app.base_path('/locations')
@app.path_params('uuid:guid')
class LocationsAPI(MethodResource):
    """CRUD operations for locations"""

    @app.register_endpoint(
        Location,
        authorisation=None,
    )
    def get(self, location):
        """Get specific Location."""
        return location

    @app.register_endpoint(
        Location,
        authorisation=rbac.ALL,
    )
    def post(self, location):
        """Create new location."""
        check_parent_and_tag_guids(location)
        location.save()
        Tag(guid=location.guid, name=location.name, type=Tag.Type.LOCATION).save()
        return location

    @app.register_endpoint(
        Location,
        input_schema=Location.Schemas.Put(partial=True),
    )
    def put(self, location):
        """Update the given Location's information."""
        check_parent_and_tag_guids(location)
        location.save()
        return location

    @app.register_endpoint(
        Location,
    )
    def delete(self, location):
        """Archive the given Location."""
        if location.archived is not None:
            return make_response('', 204)
        # try:  #todo when archived tag implemented uncomment this part
        #     tag = Tags.index.query(location.guid)
        #     tag.delete()
        # except AttributeError:
        #     pass
        location.archived = datetime.now(timezone.utc)
        location.save()
        return make_response('', 204)
