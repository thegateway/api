from flask import (
    Response,
    g,
)
from marshmallow import (
    fields,
)

import gwio.user
from gwio.core import rbac
from gwio.core.decorators import api
from gwio.ext.marshmallow import GwioSchema
from .flask_app import app

User = gwio.user.User


class TermsAcceptSchema(GwioSchema):
    """
    name: Terms
    description: Terms and conditions that the user is accepting.
    """
    terms = fields.String(required=True)


# TODO: Is the target User???
@app.register_endpoint(
    User,
    route='/terms/accept/',
    method=api.POST,
    output_schema=None,
    input_schema=TermsAcceptSchema,
    authorisation=rbac.ALL,
)
# pylint: disable=unused-argument
def accept_terms(*, terms, **_):
    """Marks the given ToS accepted"""
    # TODO: Add tests
    g.log_event_util.create_web_event('accept_terms', extra_data=dict(terms=terms))
    return Response(None, 204)

# @app.register
# @app.route('/audit_events/', methods=['GET'])
# @authenticated()
# def list_audit_events_for_user():
#     user = g.user
#     for ev in Event.scan(Event.subject_id == user.id):
#         yield ev
