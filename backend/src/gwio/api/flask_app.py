# coding=utf-8
"""
core module for dashboard API. This file should only contain App -specific functionality and view decorators.
"""
import logging
from collections import defaultdict
from urllib.parse import quote

import stripe
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from envs import env
from flask import (
    jsonify,
    redirect,
    request,
    json,
)
from gwio.environment import Env
from werkzeug.exceptions import (
    HTTPException,
    default_exceptions,
)

from gwio.core import rbac
from gwio.core.api.app import app
from gwio.core.decorators.api import GET
from gwio.utils.aws import (
    CognitoIdpClient,
)

stripe.api_key = env('STRIPE_API_KEY')
logger = logging.getLogger(__name__)


# noinspection PyShadowingNames
# pylint: disable=redefined-outer-name
def handle_http_error(e):
    code = 500
    error, message = str(e).split(':', 1)

    if isinstance(e, HTTPException):
        code = e.code
        try:
            message = json.dumps(e.description)
        except TypeError:
            pass

    response = jsonify(dict(error=error, message=message.strip(), code=code, path=request.path))
    response.status_code = code
    return response


# Register standard error handlers (for sending json output)
for code in default_exceptions:
    if code == 500:
        continue
    app.register_error_handler(code, handle_http_error)


@app.register_endpoint(
    None,
    route='/',
    method=GET,
    output_schema=None,
    authorisation=None,
)
def index():
    """
    summary: Redirects to Swagger UI.
    description: A convenience function at the root API url redirects a user to Swagger UI that is
        capable of rendering the documentation in a navigable web page.
        You probably are already there since you are reading this. \U0001F609
    """
    base_url = quote(request.host_url)
    url = f'{Env.SWAGGER_UI_URL}?url={base_url}swagger.json'
    logger.info(url)
    return redirect(url, 302)


@app.register_endpoint(
    None,
    route="/swagger.json",
    method=GET,
    output_schema=None,
    authorisation=None
)
def swagger_json():
    """This documentation in ApiSpec 3.0.2 format"""
    spec = APISpec(
        title="The Reseller Gateway API",
        version=app.documentation['version'],
        openapi_version="3.0.2",
        info=dict(
            description="The Reseller Gateway e-Commerce API",
            version=app.documentation['version'],
            contact=dict(email="dev@the.gw"),
        ),
        plugins=[MarshmallowPlugin()],
        host=request.host,
        basePath="/",
    )
    for role in sorted(f.__name__ for f in rbac.ALL):
        group_name = getattr(CognitoIdpClient.Group, role.upper()).value
        spec.components.security_scheme(
            f'{role}_auth',
            dict(
                name=f"{role.replace('_', ' ').capitalize()} authorisation",
                type="http",
                scheme="bearer",
                bearerFormat="JWT",
                description=f"A valid JSON Web Token issued either by cognito pool {Env.COGNITO_POOL_ID} with "
                f"`{group_name}` listed in `cognito:groups` claim, or a token issued by a partner with similar claims."
            ))
    spec.components.security_scheme(
        'custom_auth',
        dict(
            name="Custom authorization",
            type="http",
            scheme="bearer",
            bearerFormat="JWT",
            description="Custom authorization with extra checks that user's organization ID matches the owner of the resource that is being accessed.\n"
                        "Depending on the type of endpoint either `brand_owner_auth` or `shopkeeper_auth` is required. Token requirements are listed in the"
                        "respective securitey schemes."
        )
    )
    for func, route in app.doc_endpoints.items():
        app.generate_endpoint_documentation(
            route,
            func
        )
    for ep, doc in app.documentation.items():
        if not isinstance(doc, defaultdict):
            continue
        spec.path(ep, operations=doc)

    swagger = spec.to_dict()
    return jsonify(swagger)
