# -*- coding: utf-8 -*-
"""
/webshop_products endpoint
"""
import logging

from flask import (
    redirect,
    request,
)
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    NotFound,
)

from gwio.api.flask_app import app
from gwio.api.utils import path_var_as_uuid
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
    POST,
    authorise,
)
from gwio.cache.flask_http import cached
from gwio.core.cw_logging import LogEventType
from gwio.ext.decorators import static_vars
from gwio.ext.eshelpers import (
    QueryDDLSchema,
)
from gwio.ext.marshmallow import GwioSchema
from gwio.models.pricing import (
    PricingError,
)
from gwio.models.products import Product
from gwio.models.webshop_product import (
    ProductElasticsearchSchema,
    WebshopProduct,
)

log = logging.getLogger(__name__)

_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='webshop_guid')


@app.register_resource
@app.base_path('/webshops')
@app.path_params('uuid:webshop_guid', '/products', 'uuid:product_guid')
# pylint: disable=no-self-use
class WebshopProductsAPI(MethodResource):
    """
    Resource for webshop products
    """

    @app.register_endpoint(
        WebshopProduct,
        output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet,
        authorisation=None,
    )
    def get(self, webshop_product):
        """
        summary: Return shop (webshop_guid) specific product ("product_guid") information.
        description : If the product is not sold by the given shop this will return NotFound (404) to indicate that the
            product is not available, unlike the generic /product endpoint this does NOT mean that the product is unknown,
            ONLY that is not available in this shop
        tags:
           - Webshop
           - Product
        """
        try:
            product = Product.by_guid(webshop_product.product_guid)
        except (WebshopProduct.DoesNotExist, Product.DoesNotExist):
            raise NotFound
        evaluated = WebshopProduct.API.Assessor.evaluate(product=product, webshop_guid=webshop_product.webshop_guid)
        return evaluated

    @app.register_endpoint(
        WebshopProduct,
        autoload=False,
        input_schema=None,
        output_schema=None,
        authorisation=None,
    )
    def post(self, **kwargs):
        """
        summary: Redirect to `/dashboard/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}dashboard{request.full_path}', code=308)

    @app.register_endpoint(
        WebshopProduct,
        autoload=False,
        input_schema=None,
        output_schema=None,
        authorisation=None,
    )
    def put(self, **kwargs):
        """
        summary: Redirect to `/dashboard/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}dashboard{request.full_path}', code=308)

    @app.register_endpoint(
        WebshopProduct,
        autoload=False,
        output_schema=None,
        authorisation=None,
    )
    def delete(self, **kwargs):
        """
        summary: Redirect to `/dashboard/*` endpoint
        deprecated: true"""
        return redirect(f'{request.host_url}dashboard{request.full_path}', code=308)


class SelectQueryResults(GwioSchema):
    next = fields.String()
    total = fields.Integer(default=0)
    products = fields.Nested(ProductElasticsearchSchema, many=True, default=list())


@static_vars(assessor=None)
def _in_shop_assessed(product_guid, webshop_guid):
    product_guid = path_var_as_uuid(product_guid)
    webshop_guid = path_var_as_uuid(webshop_guid)

    product = Product.by_guid(product_guid)
    assessed = product.API.Assessor.evaluate(product)
    try:
        assessed = WebshopProduct.API.Assessor.evaluate(webshop_guid=webshop_guid, product_guid=product_guid)
        assessed.in_shop = True
    except (WebshopProduct.DoesNotExist, PricingError):
        assessed.in_shop = False
    return assessed


def _cut_path_starting(path, starting_from):
    return path[path.find(starting_from):]


@app.register_endpoint(
    WebshopProduct,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/products/_select',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=QueryDDLSchema,
    output_schema=None,
    authorisation=None,
)
def old_select_webshop_products(webshop_guid, dsl=None, **_):
    """
    summary: Redirected to `/dashboard/*` endpoint
    deprecated: true"""
    return redirect(f'{request.host_url}dashboard{_cut_path_starting(request.full_path, "/products")}', code=308)


@app.register_endpoint(
    WebshopProduct,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/products/_continue/<token>',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=None,
)
def old_continue_webshop_select(webshop_guid, token):
    """
    summary: Redirected to `/dashboard/*` endpoint
    deprecated: true"""
    return redirect(f'{request.host_url}dashboard{_cut_path_starting(request.full_path, "/products")}', code=308)


class QueryResults(GwioSchema):
    next = fields.String()
    total = fields.Integer(default=0)
    products = fields.Nested(Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet, many=True, default=list())


@app.register_endpoint(
    WebshopProduct,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/products/_query',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=QueryDDLSchema,
    output_schema=None,
)
def old_query_webshop_products(webshop_guid, dsl=None, **_):
    """
    summary: Redirected to `/dashboard/*` endpoint
    deprecated: true"""
    return redirect(f'{request.host_url}dashboard{_cut_path_starting(request.full_path, "/products")}', code=308)


@app.register_endpoint(
    WebshopProduct,
    autoload=False,
    route='/webshops/<uuid:webshop_guid>/products/_fetch/<token>',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=None,
    authorisation=None,
)
def old_continue_webshop_query(webshop_guid, token):
    """
    summary: Redirected to `/dashboard/*` endpoint
    deprecated: true"""
    return redirect(f'{request.host_url}dashboard{_cut_path_starting(request.full_path, "/products")}', code=308)


# pylint: disable=unused-argument
@cached('1d')
# TODO: What should be the object?
@app.register_endpoint(
    Product,
    index=Product.by_guid,
    route='/webshops/<uuid:webshop_guid>/product_substitutes/<uuid:guid>',
    method=GET,
    output_schema=Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet(many=True),
    authorisation=None,
)
def get_product_substitutes(product, webshop_guid):
    """
    Return list of products that can be used to substitute product identified by "product_id" available in the given shop.
    """
    if not int(product.substitution_group):
        return list()
    substitute_products = []
    for substitute in Product.substitutions.query(product.substitution_group):
        if substitute.guid == product.guid:
            # Skip the product itself
            continue
        try:
            # Will raise DoesNotExist and skip to the next product if the product is not in the shop's inventory
            WebshopProduct.get(product_guid=substitute.guid, webshop_guid=webshop_guid)
            substitute_products.append(Product.by_guid(substitute.guid))
        except Product.DoesNotExist:
            log.warning('Could not find product %s that exists in the product substitution index', substitute.guid)
        except WebshopProduct.DoesNotExist:
            pass
    return (WebshopProduct.API.Assessor.evaluate(product=product, webshop_guid=webshop_guid) for product in substitute_products)
