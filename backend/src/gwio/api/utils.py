# -*- coding: utf-8 -*-
"""
Helpers used in all product/pricing APIs
"""
import logging
import uuid

from werkzeug.exceptions import NotFound

log = logging.getLogger(__name__)


def path_var_as_uuid(arg):
    if isinstance(arg, uuid.UUID):
        return arg
    try:
        return uuid.UUID(str(arg))
    except ValueError:
        raise NotFound
    except Exception as e:
        log.exception(e)
        raise NotFound
