import logging

from functools import partial
from marshmallow import fields
from typing import List
from werkzeug.exceptions import BadRequest

import gwio.logic.deliveries.xpress_couriers
from gwio.api.flask_app import app
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import POST
from gwio.ext.dynamorm import GwioModelField
from gwio.ext.marshmallow import (
    GwioSchema,
    GwioEnumField,
)
from gwio.integrations.xpress_couriers import (
    Location,
    ServiceType,
)
from gwio.logic.deliveries.xpress_couriers import (
    get_available_delivery_methods,
)
from gwio.models.orders import (
    Order,
    Address,
    OrderProductLineInputSchema,
)
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.webshop_product import WebshopProduct

logger = logging.getLogger(__name__)


class DeliveryMethodAddress(Address):
    class Meta:
        exclude = ('phone', 'email')


class DeliveryMethodProductLine(OrderProductLineInputSchema):
    pass
    # guid, qty, shop_guid


class DeliveryMethodRequest(GwioSchema):
    """
    name: Supported delivery methods request scheme
    description: Schema for requesting supported delivery methods
    """
    delivery_address = GwioModelField(DeliveryMethodAddress, required=True)
    products = fields.Nested(DeliveryMethodProductLine, many=True)
    cash_on_delivery = fields.Boolean(required=True)
    is_private_person = fields.Boolean(required=True)
    service_type = GwioEnumField(enum=ServiceType, by_value=True)


class DeliveryMethodResponse(GwioSchema):
    """
    name: Supported delivery methods response scheme
    description: Schema for supported delivery methods' response
    """
    delivery_methods = fields.Dict(keys=fields.UUID(), values=fields.Nested(Product.Schemas.Get, many=True))


def product_xpc_id_in_dm(xpc_dm_ids: List[str], dm: Product):
    try:
        return dm.data['xpress_couriers']['service_id'] in xpc_dm_ids
    except KeyError:
        return False


@app.register_endpoint(
    Order,
    autoload=False,
    route='/delivery_methods/supported',
    method=POST,
    event_type=LogEventType.LIST,
    input_schema=DeliveryMethodRequest,
    output_schema=DeliveryMethodResponse,
    authorisation=None,
)
def get_delivery_methods(delivery_address, products, cash_on_delivery, is_private_person, service_type):
    evaluated_products = []
    for p in products:
        product = Product.get(owner_guid=p['shop_guid'], guid=p['guid'])
        webshop_product = WebshopProduct.get(product_guid=product.guid, webshop_guid=p['shop_guid'])
        evaluated = WebshopProduct.API.Assessor.evaluate(
            product=product,
            webshop_product=webshop_product,
            schema=product.Schemas.Elasticsearch(),
            qty=p['qty'])
        evaluated_products.append(dict(
            product_guid=p['guid'],
            shop_guid=p['shop_guid'],
            qty=p['qty'],
            amount=evaluated.price.final.total.amount,
        ))
    try:
        city = gwio.logic.deliveries.xpress_couriers.city_from_postal_code(delivery_address.country_code, delivery_address.postal_code)
    except ValueError as e:
        raise BadRequest from e
    xpc_delivery_methods = get_available_delivery_methods(
        delivery_location=Location(
            name=delivery_address.name,
            address=delivery_address.street_address,
            city=city,
            post_code=delivery_address.postal_code,
            country_code=delivery_address.country_code,
            is_private_person=is_private_person,
        ),
        product_list=evaluated_products,
        cash_on_delivery=cash_on_delivery,
        service_type=service_type,
    )
    logger.debug('Got delivery methods from Xpress Couriers: %s', xpc_delivery_methods)
    delivery_methods = list(Product.TypeIndex.query(type=PRODUCT_TYPE_DELIVERY_METHOD))
    supported_delivery_methods = {}
    for shop_guid, xpc_dm in xpc_delivery_methods.items():
        supported_delivery_methods[shop_guid] = list(filter(partial(product_xpc_id_in_dm, [dm.id for dm in xpc_dm]), delivery_methods))
    return dict(delivery_methods=supported_delivery_methods)
