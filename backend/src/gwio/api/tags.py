# -*- coding: utf-8 -*-
"""
Simple interface for handling tags
"""

import logging

from flask import (
    jsonify,
    make_response,
)
from flask_apispec import MethodResource
from marshmallow import (
    fields,
)
from werkzeug.exceptions import (
    BadRequest,
    Conflict,
    NotFound,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.cw_logging import LogEventType
from gwio.core.decorators.api import (
    DELETE,
    GET,
    POST,
    PUT,
    authorise,
)
from gwio.ext import SYSTEM_GUID
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
)
from gwio.models.products import Product
from gwio.models.tags import (
    Tag,
    TagType,
)

log = logging.getLogger(__name__)


class QuerySchema(GwioSchema):
    """
    name: tag query params
    description: query params for tag list.
    """
    type = GwioEnumField(enum=Tag.Type, by_value=True, allow_none=True)
    include_children = fields.Boolean(default=False)  # TODO: remove once frontend is not using this

    def handle_error(self, error, data, **kwargs):
        raise NotFound(error.messages)


def create_tag(tag):
    try:
        Tag.by_guid(tag.guid)
        raise Conflict(description=f'Tag ({tag.guid}) already exists')
    except Tag.DoesNotExist:
        tag.save()
    return tag, 201


def delete_tag(tag):
    if tag is not None:
        tag.delete()
    app.redis_cache.invalidate("/tags/")
    return make_response('', 204)


@app.register_endpoint(
    Tag,
    route='/tags',
    method=GET,
    event_type=LogEventType.LIST,
    autoload=False,
    input_schema=QuerySchema,
    output_schema=Tag.Schemas.Get(many=True),
    authorisation=None,
    cache_for='5m',
)
def list_global_tags(**kwargs):
    try:
        # pylint: disable=no-value-for-parameter
        tags = Tag.TypeIndex.query(type=kwargs['type'].value)
    except KeyError:
        # pylint: disable=no-value-for-parameter
        tags = Tag.OwnerTypeIndex.query(owner=str(SYSTEM_GUID))
    return tags


list_global_tags.__doc__ = f"""
List all system wide top level category tags by default.
Query parameter `filter` can be passed to get the following type parameters: {', '.join([t.value for t in TagType])}
"""


@app.register_endpoint(
    Tag,
    route='/tags/',
    method=POST,
)
def admin_create_tag(tag):
    """Create new tag with the default owner"""
    app.redis_cache.invalidate("/tags/")
    return create_tag(tag)


@app.register_endpoint(
    Tag,
    index=Tag.by_guid,
    route='/tags/<uuid:guid>/',
    method=PUT,
)
def admin_update_tag(tag):
    """Update a tag with the default owner"""
    tag.update()
    app.redis_cache.invalidate("/tags/")
    return tag


@app.register_endpoint(
    Tag,
    index=Tag.by_guid,
    route='/tags/<uuid:guid>/',
    method=DELETE,
)
def admin_delete_tag(tag):
    """Delete a tag with the default owner"""
    app.redis_cache.invalidate("/tags/")
    return delete_tag(tag)


@app.register_endpoint(
    Tag,
    index=Tag.by_guid,
    route='/tags/<uuid:guid>/',
    method=GET,
    authorisation=None,
)
def get_tag_by_uuid(tag):
    """Get tag information by uuid"""
    return tag


@app.register_endpoint(
    Tag,
    route='/webshops/<uuid:owner>/tags/',
    method=GET,
    event_type=LogEventType.LIST,
    index=Tag.OwnerTypeIndex,
    output_schema=Tag.Schemas.Get(many=True),
    input_schema=QuerySchema,
    authorisation=None,
)
def list_shop_tags(tags):
    """
    List all shop owned tags.
    Can be filtered to only return category or campaign tags using "type" query parameter.
    """
    return tags


_admin_or_shopkeeper = authorise(rbac.admin, rbac.shopkeeper, guid_name='owner')


@app.register_resource
@app.base_path('/webshops')
@app.path_params(f'uuid:{Tag.Table.hash_key}', '/tags', f'uuid:{Tag.Table.range_key}')
# pylint: disable=no-self-use
class TagsAPI(MethodResource):
    """
    Resource for Tags
    """

    @app.register_endpoint(
        Tag,
        authorisation=None,
    )
    def get(self, tag):
        """ Get tag information"""
        return tag

    @app.register_endpoint(
        Tag,
        authorisation=_admin_or_shopkeeper,
    )
    def post(self, tag):
        """Create a new tag"""
        try:
            Tag.by_guid(tag.guid)
            raise Conflict(description=f'Tag ({tag.guid}) already exists')
        except Tag.DoesNotExist:
            tag.save()
        app.redis_cache.invalidate(f"/webshops/{tag.owner}/tags/")
        return tag, 201

    @app.register_endpoint(
        Tag,
        authorisation=_admin_or_shopkeeper,
    )
    def put(self, tag):
        """Update a tag"""
        tag.save()
        app.redis_cache.invalidate(f"/webshops/{tag.owner}/tags/")
        return tag

    # TODO: does not check that the tag is owned by the webshop
    @app.register_endpoint(
        Tag,
        index=Tag.by_guid,
        authorisation=_admin_or_shopkeeper,
    )
    def delete(self, tag, owner):
        """Delete an existing tag"""
        try:
            tag.delete()
        except Tag.DoesNotExist:
            raise Conflict(description='Tag ({guid}) does not exist'.format(guid=tag.guid))
        app.redis_cache.invalidate(f"/webshops/{tag.owner}/tags/")
        return make_response('', 204)


class ProductTagsUpdateSchema(GwioSchema):
    """
    name: product tags
    description: product tags to be added or deleted.
    """
    add = fields.List(fields.UUID)
    delete = fields.List(fields.UUID)


class LimitSchema(GwioSchema):
    """
    name: query parameters
    description: query parameters to filter product tags with.
    """
    limit = fields.Integer(missing=None)


# TODO: Remove these endpoints as they are useless
# pylint: disable=no-self-use
@app.register_resource
@app.path_params('uuid:guid')
class ProductTagsAPI(MethodResource):
    # @cached('10m')  - DISABLED UNTIL INVALIDATION
    @app.register_endpoint(
        Tag,
        index=Tag.by_guid,
        event_type=LogEventType.LIST,
        input_schema=LimitSchema,
        output_schema=None,
        authorisation=None,
        cache_for='10m',
    )
    def get(self, tag, limit):
        """
        summary: Return the guids of the products tagged with by *tag_guid*
        tags:
            - Product
            - Tags
        """
        return jsonify(tag.product_guids)

    # TODO: How should this be authorized? We should probably have separate end points for global and webshop specific tags.
    @app.register_endpoint(
        Tag,
        index=Tag.by_guid,
        output_schema=None,
        input_schema=ProductTagsUpdateSchema,
        authorisation=(rbac.admin, rbac.shopkeeper)
    )
    # pylint: disable=unused-argument
    def put(self, tag, add=None, delete=None):
        """
        summary: Manage products related to a tag
        tags:
            - Product
            - Tags
        """

        # noinspection PyShadowingNames
        if add is None:
            add = list()
        if delete is None:
            delete = list()
        if not (add or delete):
            raise BadRequest(description="No tag operations given")

        products_to_save = set()
        for product_guid in add:
            try:
                product = Product.by_guid(product_guid)
            except Product.DoesNotExist:
                raise NotFound(f'Product {product_guid} does not exist')
            if not (rbac.admin() or rbac.product_owner(product.owner_guid) or rbac.shopkeeper(product.owner_guid)):
                raise NotFound(f'Product {product_guid} does not exist')
            if product.type not in Product.API.valid_campaign_types:
                raise Conflict(description=f"Can not add product of '{product.type}' type to campaign")
            tag.add_product(product)
            products_to_save.add(product)
        for product_guid in delete:
            try:
                product = Product.by_guid(product_guid)
                tag.remove_product(product)
                products_to_save.add(product)
            except Product.DoesNotExist:
                tag.product_guids.remove(product_guid)
        tag.save()

        for product in products_to_save:
            product.save()

        app.redis_cache.invalidate("/tags/")
        return make_response('', 204)


def _product_owner(owner, **_):
    """Requires product owner credentials"""
    if not rbac.product_owner(owner):
        raise NotFound


@app.register_endpoint(
    Tag,
    route='/brands/<uuid:owner>/tags/',
    method=POST,
    input_schema=Tag.Schemas.Post(exclude=('type',)),
    authorisation=_product_owner,
    extra_kwargs=dict(type=Tag.Type.BRAND),
)
def brand_create_tag(tag):
    """Create new tag owned by the requester"""
    app.redis_cache.invalidate(f"/brands/{tag.owner}/tags/")
    app.redis_cache.invalidate("/tags/")
    return create_tag(tag)


@app.register_endpoint(
    Tag,
    route='/brands/<uuid:owner>/tags/<uuid:guid>/',
    method=PUT,
    input_schema=Tag.Schemas.Put(only=('name',)),
    authorisation=_product_owner,
)
def brand_update_tag(tag):
    """Update a tag owned by the requester"""
    tag.update()
    app.redis_cache.invalidate(f"/brands/{tag.owner}/tags/")
    return tag
    # return update_tag(partial(Tags.get, brand_guid), tag_guid, **values)


@app.register_endpoint(
    Tag,
    route='/brands/<uuid:owner>/tags/<uuid:guid>',
    method=DELETE,
    authorisation=_product_owner,
)
def brand_delete_tag(tag):
    """Delete the tag owned by the requester"""
    return delete_tag(tag)
