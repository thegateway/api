# -*- coding: utf-8 -*-
"""
Simple interface for handling pricing modifiers
"""

import logging

from flask import (
    make_response,
)
from flask_apispec import MethodResource
from werkzeug.exceptions import (
    Conflict,
)

from gwio.api.flask_app import app
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.core.cw_logging import LogEventType
from gwio.ext import SYSTEM_GUID
from gwio.models.pricing import Pricing

log = logging.getLogger(__name__)


@app.register_resource
@app.path_params('uuid:guid')
# pylint: disable=no-self-use
class PricingsAPI(MethodResource):
    """
    Resource for Pricing
    """

    @app.register_endpoint(
        Pricing,
        index=Pricing.by_guid
    )
    def get(self, pricing):
        """ Get pricing information """
        return pricing

    @app.register_endpoint(
        Pricing,
        extra_kwargs=dict(owner=SYSTEM_GUID),
    )
    def post(self, pricing):
        """Create a new pricing rule"""
        try:
            Pricing.get(owner=pricing.owner, guid=pricing.guid)
            raise Conflict(description=f'Pricing ({pricing.guid}) already exists')
        except Pricing.DoesNotExist:
            pricing.save()
        return pricing, 201

    @app.register_endpoint(
        Pricing,
        automodify=False,
        extra_kwargs=dict(owner=SYSTEM_GUID),
    )
    def put(self, pricing, **kwargs):
        """Update the pricing rule identified by 'pricing_guid'"""
        Pricing.API.update_object(pricing, **kwargs)
        pricing.save()
        return pricing

    @app.register_endpoint(
        Pricing,
        extra_kwargs=dict(owner=SYSTEM_GUID),
    )
    def delete(self, pricing):
        """Removed the pricing rule identified by 'pricing_guid'"""
        # TODO: check that rule is not in use by andy product
        pricing.delete()
        return make_response('', 204)


@app.register_endpoint(
    Pricing,
    index=Pricing.query,
    query_params=dict(owner=SYSTEM_GUID),
    route='/pricings/',
    method=GET,
    event_type=LogEventType.LIST,
    output_schema=Pricing.Schemas.Get(many=True),
    authorisation=(rbac.admin, rbac.shopkeeper),
)
def list_system_pricings(pricing):
    """Return all system pricings"""
    return pricing
