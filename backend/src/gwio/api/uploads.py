import logging
import os
import urllib
import uuid
from urllib.parse import urlparse

import boto3
import requests
from flask import (
    g,
    json,
    make_response,
)
from flask_apispec import MethodResource
from gwio.core import rbac
from gwio.core.api.app import app
from gwio.core.decorators.api import (
    AuditObject,
    GET,
    POST,
)
from gwio.environment import Env
from gwio.ext import SYSTEM_GUID
from gwio.ext.marshmallow import GwioSchema
from gwio.schemas import (
    S3EndpointSchema,
    S3FileSchema,
    S3FileUploadSchema,
)
from gwio.utils.io import ResponseStream
from marshmallow import (
    fields,
)
from werkzeug.exceptions import Unauthorized

_upload_authorisation = (rbac.shopkeeper, rbac.admin, rbac.product_owner)
logger = logging.getLogger(__name__)

PARSED_S3_URL = urlparse(Env.UPLOAD_LOCATION)
try:
    BUCKET = PARSED_S3_URL.netloc.strip('/')
    PREFIX = PARSED_S3_URL.path.strip('/')
except TypeError:  # Stupid urllib treats None -> bytes.
    logger.fatal("UPLOAD_LOCATION is not set!")
    BUCKET = None
    PREFIX = None
S3_BUCKET_FOR_UPLOADS = Env.UPLOAD_BUCKET


def _clean_path_join(*parts):
    return '/'.join(x.strip('/') for x in parts)


#
# Uploads
#
@app.register_resource
class UploadsAPI(MethodResource):
    """
    API for file and image uploads
    """
    client = boto3.client('s3')

    @staticmethod
    def get_url(key=''):
        """
        Get a full URL for key.
        """
        if '.' in S3_BUCKET_FOR_UPLOADS:
            return 'https://{}/{}'.format(S3_BUCKET_FOR_UPLOADS, key)
        return 'https://{}.s3-{}.amazonaws.com/{}'.format(S3_BUCKET_FOR_UPLOADS, Env.AWS_REGION, key)

    @app.register_endpoint(
        AuditObject.UPLOAD,
        output_schema=S3EndpointSchema,
        input_schema=S3FileUploadSchema,
        authorisation=_upload_authorisation,
    )
    def post(self, filename, content_type, acl='public-read'):
        """
        summary: Create a new file upload entry for S3
        tags:
           - Uploads
        """
        s3_fields = {'acl': acl,
                     'Content-Type': content_type}
        conditions = []
        for key, val in s3_fields.items():
            conditions.append({key: val})
        key = f'{uuid.uuid4()}.{filename.split(".")[-1]}'
        if not rbac.admin():
            if not g.user.organization_guid:
                logger.error("User %s has no organization ID set, can not upload", g.user.name)
                raise Unauthorized
            key = os.path.join(str(g.user.organization_guid), key)
        presigned_resp = self.client.generate_presigned_post(Bucket=S3_BUCKET_FOR_UPLOADS,
                                                             Key=key,
                                                             Fields=s3_fields,
                                                             Conditions=conditions,
                                                             ExpiresIn=600)
        presigned_resp['raw_original_url'] = UploadsAPI.get_url()
        return presigned_resp

    @app.register_endpoint(
        AuditObject.UPLOAD,
        authorisation=_upload_authorisation,
    )
    def delete(self, upload_id):
        """
        summary: Delete an uploaded file from S3
        tags:
          - Uploads
        """
        key = os.path.join(str(g.user.organization_guid), upload_id)
        res = self.client.delete_object(
            Bucket=S3_BUCKET_FOR_UPLOADS,
            Key=key
        )
        status_code = res['ResponseMetadata']['HTTPStatusCode']
        if status_code == 204:
            return make_response('', 204)
        return make_response(json.dumps(res), status_code)


@app.register_endpoint(
    AuditObject.UPLOAD,
    route='/uploads/',
    method=GET,
    output_schema=S3FileSchema(many=True),
    authorisation=_upload_authorisation,
)
def list_uploads():  # TODO: Add tests for this
    """
    summary: List all uploaded files for organization
    tags:
      - Uploads
    """
    results = UploadsAPI.client.list_objects_v2(
        Bucket=BUCKET,
        MaxKeys=1000,
        Prefix=str(g.user.organization_guid))
    for key_dict in results.get('Contents', list()):
        yield dict(
            modified=key_dict['LastModified'],
            url=UploadsAPI.get_url(key_dict['Key']),
            size=key_dict['Size']
        )


class DownloadSchema(GwioSchema):
    """
    name: raw_original_url
    description: a URL to be downloaded and re-hosted on S3.
    """
    url = fields.URL()


@app.register_endpoint(
    AuditObject.UPLOAD,
    authorisation=_upload_authorisation,
    route='/downloads/',
    method=POST,
    input_schema=DownloadSchema,
    output_schema=DownloadSchema
)
def upload_for_download(url):
    """
    summary: Make a S3 copy of an image.
    tags:
      - Uploads
    """
    raw_original_url = url
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(BUCKET)  # pylint: disable=no-member
    session = requests.Session()
    # Let's get headers first so we see 40x without opening a stream for the full file download.
    head_response = session.head(raw_original_url, verify=False)
    try:
        org_id = g.user.organization_guid if g.user.organization_guid else None
    except AttributeError:
        org_id = None

    if org_id is None:
        if not rbac.admin():
            raise ValueError('Lacking organization guid')
        org_id = SYSTEM_GUID

    filename = uuid.uuid5(org_id, raw_original_url)

    url_parts = urllib.parse.urlsplit(raw_original_url)
    try:
        content_type = head_response.headers['Content-Type']
        suffix = content_type.rsplit('/')[-1]
    except KeyError:
        suffix = url_parts.path.rsplit('.')[-1]
        content_type = f'image/{suffix}'
    key = _clean_path_join(str(org_id), f'{filename}.{suffix}')
    encoded_original_url = urllib.parse.urlunsplit(
        [url_parts.scheme, url_parts.netloc.encode('idna').decode('ascii')] + [urllib.parse.quote(p) for p in url_parts[2:]]
    )
    # OK, we are good to go.
    get_response = session.get(raw_original_url, stream=True, verify=False)
    # Let's wrap the response so that it support seek(), which put_object() will need to calculate checksums.
    stream = ResponseStream(get_response.iter_content(64))
    key_ = _clean_path_join(PREFIX, key)
    bucket.put_object(
        Body=stream,
        Key=key_,
        ContentType=content_type,
        Metadata=dict(
            original_url=encoded_original_url,
        ),
    )
    # TODO: Change this to use the "public" upload location
    # note that just blindly changing the env var to work in a different way  will break /uploads/ ...
    return dict(url=f'https://{BUCKET}.s3-{Env.AWS_REGION}.amazonaws.com/{key_}')
