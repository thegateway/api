# -*- coding: utf-8 -*-
"""Module that contains customers related API endpoints."""

from flask import (
    current_app,
    request,
    url_for,
)
from gwio.environment import Env
from werkzeug.exceptions import Forbidden

import gwio.user
from gwio.cognito import User as CognitoUser
from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.cache.flask_http import cached
from gwio.schemas import (
    CognitoCustomerListSchema,
    CognitoUserListQueryParam,
)
from .flask_app import app

User = gwio.user.User


def _admin_or_imeds_shopkeeper():
    """Requires admin (or imeds shopkeeper) credentials"""
    if not (rbac.admin() or (Env.VERTICAL == 'imeds' and rbac.shopkeeper())):
        raise Forbidden


@cached('1m')
@app.register_endpoint(
    User,
    autoload=False,
    route='/customers/',
    method=GET,
    output_schema=CognitoCustomerListSchema,
    input_schema=CognitoUserListQueryParam,
    authorisation=_admin_or_imeds_shopkeeper)
def list_customers(*, token=None):
    """List all customers"""
    resp = current_app.customer_pool.list_users(pagination_token=token)
    user_list = [CognitoUser.from_cognito(user) for user in resp['Users']]
    token = resp.get('PaginationToken')
    next_url = None
    if token:
        next_url = '{origin}{base}'.format(
            origin=request.host_url.rstrip('/'),
            base=url_for('list_customers', token=token)
        )
    return {'next': next_url, 'customers': user_list}
