# coding=utf-8
"""
Entry point for running the Flask Application.

When creating new views in new modules, remember to import them in __init__.py and add to __all__ so they get
registered correctly!
"""
import logging
import os

from gwio.api.flask_app import app


logger = logging.getLogger(__name__)


def setup():
    """
    This function configure dynamorm for local dynamodb . This should only be called when testing backend locally.
    """
    from tools.create_missing_tables import setup_local_dynamodb
    logging.basicConfig(level=logging.DEBUG)
    try:
        setup_local_dynamodb(os.environ['DYNAMODB_HOST'])
    except KeyError:
        logger.warning('DYNAMODB_HOST is not set. Local dynamodb is not working. Connecting to AWS DynamoDB')


setup()
if __name__ == '__main__':
    app.run(port=8080)
