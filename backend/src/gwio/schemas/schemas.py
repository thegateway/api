# coding=utf-8
# pylint: disable=C0111,R0903
import json
from enum import Enum

from marshmallow import (
    fields,
    validate,
)
from marshmallow.validate import OneOf

from gwio.utils.aws import (
    lambda_function_name,
    message_devops,
)
from gwio.ext.marshmallow import GwioSchema

DEFAULT_ORDER_LIMIT = 25
DEFAULT_GOOGLE_ANALYTICS = 'UA-92531722-4'  # XXX: This really does not belong here!


def schema_dump(schema, values):
    try:
        dump, errors = schema().dump(values)
    except TypeError:
        dump, errors = schema.dump(values)

    if errors:
        message_devops(sender=lambda_function_name(), subject='Schema dump errors', message=json.dumps(errors))
    return dump


class Events(fields.Nested):
    def serialize(self, attr, obj, accessor=None):
        # TODO Generate
        return list()


class AddressSchema(GwioSchema):
    """
    name: Address
    description: An physical address of an user, shop or such.
    """
    id = fields.Integer()
    country = fields.String()
    postal_area = fields.String()
    postal_code = fields.String()
    name = fields.String()
    street_address = fields.String()
    label = fields.String()


class _ProductSchema(GwioSchema):
    id = fields.Integer()
    name = fields.String()
    type = fields.String()


class ListParamsSchema(GwioSchema):
    limit = fields.Int(default=DEFAULT_ORDER_LIMIT, validate=validate.Range(1))
    state = fields.String(default=None)


class OrderProcessSchema(GwioSchema):
    notes = fields.Str(allow_none=True)


class OrderCreateInputSchema(GwioSchema):
    class OrderCreateProductInput(GwioSchema):
        uuid = fields.UUID(required=True)
        qty = fields.Int(required=True)

    products = fields.List(fields.Nested(OrderCreateProductInput), many=True, required=True)


class DeliveryAddress(GwioSchema):
    name = fields.String(required=True)
    street_address = fields.String(required=True)
    postal_code = fields.String(required=True)
    country = fields.String(required=True)


class OrderUpdateInputSchema(GwioSchema):
    delivery_address = fields.Nested(DeliveryAddress)
    delivery_method_uuid = fields.UUID(allow_none=True)
    payment_method_uuid = fields.UUID(allow_none=True)
    stripe_card_id = fields.String(allow_none=True)
    promotion_code = fields.String(allow_none=True)


class NewOrderLineSchema(GwioSchema):
    # name = fields.String(required=True)
    quantity = fields.Decimal(default=1)
    # unit_price = fields.Decimal(required=True)
    # vat = fields.Decimal(required=True, validate=validate.Range(0, 1))
    product_uuid = fields.UUID(default=None)
    notes = fields.Str(allow_none=True)
    order_line_uuid = fields.UUID(default=None)
    # price_includes_vat = fields.Boolean(required=True)
    type = fields.String(default='product', validate=validate.OneOf(
        ('product', 'delivery_fee', 'prescription_handling_fee', 'manual_change')))


class PaymentMethodFilterSchema(GwioSchema):
    # XXX: Why is this actor_id ?!!
    shop_id = fields.Int()


class PaymentMethodSchema(GwioSchema):
    uuid = fields.UUID(attribute='id')
    name = fields.Str()
    description = fields.Str()
    logo = fields.Str()


class WebShopDeliveryMethodSchema(GwioSchema):
    id = fields.UUID()
    name = fields.String()
    description = fields.String()
    logo_url = fields.String()
    price = fields.Decimal()
    data = fields.String()


class WebShopPaymentMethodSchema(GwioSchema):
    id = fields.UUID()
    name = fields.String()
    logo = fields.String()
    description = fields.String()
    data = fields.Dict()


class WebShopBaseSchema(GwioSchema):
    """
    An online store.
    """
    id = fields.UUID()
    name = fields.String(required=True)
    brand_color = fields.String()
    facebook_analytics = fields.String(allow_none=True)
    google_analytics = fields.String(allow_none=True, default=DEFAULT_GOOGLE_ANALYTICS)
    logo = fields.String()
    person_in_charge = fields.String(required=True)


class CognitoUserSchema(GwioSchema):
    organization_guid = fields.UUID()
    created_at = fields.DateTime()
    email = fields.Email()
    email_verified = fields.Bool()
    enabled = fields.Bool()
    id = fields.UUID()
    name = fields.String()
    phone_number = fields.String()
    status = fields.String()
    updated_at = fields.DateTime()
    username = fields.String()
    identity_id = fields.String()


class CognitoCustomerListSchema(GwioSchema):
    next = fields.String()
    customers = fields.List(fields.Nested(CognitoUserSchema))


class CognitoUserListSchema(GwioSchema):
    next = fields.String()
    users = fields.List(fields.Nested(CognitoUserSchema))


class CognitoUserListQueryParam(GwioSchema):
    """
    name: user list query parameters
    description: cognito user pool list users query parameters
    """
    token = fields.String()


class ContactInputSchema(GwioSchema):
    type = fields.String()
    value = fields.String()
    rank = fields.Integer()
    label = fields.String()


class S3FileSchema(GwioSchema):
    modified = fields.DateTime()
    url = fields.String()
    size = fields.Int()


class S3FileUploadSchema(GwioSchema):
    """
    name: S3 file upload
    description: settings and metadata for the file to be uploaded to S3.
    """
    acl = fields.String(validate=OneOf(('public-read', 'private')))
    content_type = fields.String(required=True)
    filename = fields.String(required=True)


class S3EndpointSchema(GwioSchema):
    url = fields.String()
    fields = fields.Dict()


class ShopsFilterSchema(GwioSchema):
    actor_id = fields.Int()
    id = fields.UUID()


class PackagesFilterSchema(GwioSchema):
    shop_id = fields.UUID()


class PackageSchema(GwioSchema):
    id = fields.Int()
    description = fields.String()
    name = fields.String()
    enabled = fields.Bool()


class ProductPricingSchema(GwioSchema):
    price = fields.Float()
    price_type = fields.String()
    start = fields.Date(allow_none=True)
    end = fields.Date(allow_none=True)


class PriceModifier(Enum):
    BASE_PERCENTAGE = 'pre'
    SELL_PERCENTAGE = 'post'
    BASE_PRICE = 'base'
    SELL_PRICE = 'sell'
    BASE_MULTIPLY = 'bmul'
    SELL_MULTIPLY = 'smul'
    UNMODIFIED = 'void'
    UNAVAILABLE = None


class WebshopProductPriceModifier(GwioSchema):
    price_modifier = fields.Float(default=0)
    modifier_type = fields.String(default=PriceModifier.UNAVAILABLE.value)


class StripeCardSchema(GwioSchema):
    id = fields.String()
    customer = fields.String()
    last4 = fields.String()


class StripeChargeSchema(GwioSchema):
    id = fields.String()
    amount = fields.Integer()
    amount_refunded = fields.Integer()
    captured = fields.Boolean()
    created = fields.Integer()
    currency = fields.String()
    customer = fields.String()
    description = fields.String()
    failure_code = fields.String()
    failure_message = fields.String()
    paid = fields.Boolean()
