# -*- coding: utf-8 -*-
import os
import re


def _static_vars(**kwargs):
    """
    Decorator for adding static variable to a function

    :param kwargs: variables and their initializers
    :return: decorated function
    """

    def _decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func

    return _decorate


@_static_vars(matchers=None, enabled=None, disabled=None)
def include_suites(suites=None):
    """Check if any of the test suites (given as comma separated list of values) is enabled

    NOTE! it is assumed that if running inside CI builder then all tests are enabled by default
    """
    if include_suites.matchers is None:
        include_suites.matchers = list()
        for entry in os.getenv('TGW_ENABLED_TESTSETS', '.*' if os.getenv('CI') else '').split(','):
            if entry:
                include_suites.matchers.append(re.compile(r'^' + entry.strip() + '$'))
    if include_suites.enabled is None:
        include_suites.enabled = set()
    if include_suites.disabled is None:
        include_suites.disabled = set()

    for suite in [x.strip() for x in suites.split(',')]:
        if suite in include_suites.disabled:
            continue
        if suite in include_suites.enabled:
            return True
        for m in include_suites.matchers:
            if suite and m.match(suite):
                include_suites.enabled.add(suite)
                return True
        include_suites.disabled.add(suite)
    return False
