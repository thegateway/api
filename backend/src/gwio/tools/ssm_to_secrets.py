import json

import boto3

from gwio.environment import Env
from gwio.environment.environment import Store

secrets_client = boto3.client('secretsmanager')
ssm_client = boto3.client('ssm')


def main():
    secrets = dict()
    keys = [f'{Env.NAMESPACE}/{s.name}' for s in Env.SETTINGS if s.store is Store.SECRETS_MANAGER]
    for val in Env._ssm_load_vars(keys):
        secrets[val['Name'].split('/')[-1]] = val['Value']

    print(json.dumps(secrets, sort_keys=True, indent='  '))
    try:
        # Try updating existing secrets
        secrets_client.put_secret_value(
            SecretId=Env.NAMESPACE,
            SecretString=json.dumps(secrets)
        )
    except Exception as e:
        if 'ResourceNotFoundException' in str(e):
            # No secrets yet. Create them
            secrets_client.create_secret(
                Name=Env.NAMESPACE,
                SecretString=json.dumps(secrets),
            )
        else:
            raise


if __name__ == '__main__':
    main()
