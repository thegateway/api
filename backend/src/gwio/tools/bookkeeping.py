import datetime
import logging
import uuid
from decimal import Decimal
from operator import itemgetter

import boto3
import dateutil
import openpyxl
import pytz
from amazon.ion.simple_types import (
    IonPyBool,
    IonPyDecimal,
    IonPyDict,
    IonPyFloat,
    IonPyInt,
    IonPyList,
    IonPyNull,
    IonPyText,
)
from botocore.exceptions import ClientError
from openpyxl.utils import get_column_letter
from pyqldb.execution.executor import Executor

from gwio.environment import Env
from gwio.utils import qldb

logger = logging.getLogger(__name__)

client = boto3.client('logs')


def get_transactions(executor: Executor, *, table_name: str, year: int, month: int, timezone: datetime.tzinfo) -> list:
    # Start and end times in microsecond timestamps
    start_dt = datetime.datetime(year, month, 1, tzinfo=timezone)
    start = int(start_dt.timestamp() * qldb.TIMESTAMP_MULTIPLIER)
    end_dt = datetime.datetime(year, month + 1, 1, tzinfo=timezone)
    end = int(end_dt.timestamp() * qldb.TIMESTAMP_MULTIPLIER)

    query = f"""
        SELECT *
        FROM "{table_name}"
        WHERE {qldb.TIMESTAMP_COLUMN} >= {start} AND {qldb.TIMESTAMP_COLUMN} < {end}
    """

    s_iso = start_dt.isoformat()
    e_iso = end_dt.isoformat()
    logger.info(f'Querying for transactions from `{table_name}` table that were saved to the QLDB between {s_iso} (inclusive) and {e_iso} (exclusive)')
    try:
        return sorted(executor.execute_statement(query), key=itemgetter(qldb.TIMESTAMP_COLUMN))
    except ClientError as e:
        if f"No such variable named '{table_name}'" in str(e):
            # Table doesn't exist, which means the store doesn't have any data yet
            return []
        raise


def _ion_to_python(value):
    ion_to_python_types = {
        IonPyBool: bool,
        IonPyDecimal: Decimal,
        IonPyDict: lambda x: {k: _ion_to_python(v) for k, v in x.items()},
        IonPyFloat: float,
        IonPyList: list,
        IonPyNull: lambda x: None,
        IonPyText: str,
        IonPyInt: int,
    }
    try:
        return ion_to_python_types[type(value)](value)
    except KeyError:
        raise TypeError(f'Could not convert `{type(value).__name__}` to a native python type')


def _set_cell_value(*, value, timezone, columns, column_name, sheet, row_num):
    try:
        value = dateutil.parser.parse(value).replace(tzinfo=pytz.UTC).astimezone(timezone)
    except (TypeError, OverflowError, ValueError):
        pass

    try:
        column = columns[column_name]
    except KeyError:
        # The column "numbering" starts from 1 (= A)
        column = get_column_letter(len(columns) + 1)
        columns[column_name] = column
        sheet[f'{column}1'] = column_name  # Set the header cell since this is a new column

    if column_name == qldb.TIMESTAMP_COLUMN:
        value = datetime.datetime.utcfromtimestamp(value / qldb.TIMESTAMP_MULTIPLIER).replace(tzinfo=pytz.UTC).astimezone(timezone)
        # Set the date as a string as well, because openpyxl converts the date to UTC, because excel doesn't support timezones
        sheet[f'{columns["reserved_for_datetime_str"]}{row_num}'] = str(value)

    sheet[f'{column}{row_num}'] = value  # Set the value to the cell


def generate_sales_reports(*, file, subject_guid: uuid.UUID, year: int, month: int, timezone: datetime.tzinfo):
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = 'Transactions'
    ledger_name = f'{Env.APPLICATION_ENVIRONMENT}-{Env.VERTICAL}'
    session = qldb.get_session(ledger_name)
    transactions = session.execute_lambda(
        lambda executor: get_transactions(executor, table_name=str(subject_guid), year=year, month=month, timezone=timezone),
        lambda _: logger.info('Retrying due to OCC conflict...'),
    )

    # Reserving some columns so that at least the most important information will be in the same order every time
    columns = dict(
        reserved_for_datetime_str='A',
    )

    # Setting titles for the reserved columns
    ws[f'{columns["reserved_for_datetime_str"]}1'] = 'full datetime'

    row_i = 2  # First row is headers
    for t in transactions:
        transaction = _ion_to_python(t)
        vats = transaction.pop('vats')
        extra_fields = transaction.pop('data')
        for column_name, value in transaction.items():
            _set_cell_value(
                column_name=column_name,
                columns=columns,
                row_num=row_i,
                sheet=ws,
                timezone=timezone,
                value=value,
            )
        for column_name, value in vats.items():
            _set_cell_value(
                column_name=f'VAT {column_name}',
                columns=columns,
                row_num=row_i,
                sheet=ws,
                timezone=timezone,
                value=value,
            )
        for column_name, value in extra_fields.items():
            _set_cell_value(
                column_name=f'{column_name} (extended data)',  # To avoid accidentally overlapping names
                columns=columns,
                row_num=row_i,
                sheet=ws,
                timezone=timezone,
                value=value,
            )
        row_i += 1

    def as_text(value):
        if value is None:
            return ""
        return str(value)

    # Resizing the columns so the excel is a bit more readable without modifications needed
    for column_cells in ws.columns:
        length = max(len(as_text(cell.value)) for cell in column_cells)
        ws.column_dimensions[column_cells[0].column_letter].width = length

    # ws.protection.enable()  # To prevent accidental edits, this is financial bookkeeping data after all

    # Adding sorting options to headers to enable easier sorting if needed, though the users do need to unlock the sheet if they want to sort
    ws.auto_filter.ref = f'A1:{get_column_letter(ws.max_column)}{ws.max_row}'

    # Styling
    headers_fill = openpyxl.styles.PatternFill('solid', fgColor='D7D7D7')
    # Can not use row fill, because the filters interfere and fill doesn't get applied
    for i in range(ws.max_column):
        ws[f'{get_column_letter(i + 1)}1'].fill = headers_fill

    # Creating a sheet for information about the file to help the user understand what they are looking at
    helper_sheet = wb.create_sheet(title='Help')
    helper_sheet['A1'] = f'This excel contains transactions for webshop `{subject_guid}` for {year}-{month} (in `{timezone}` timezone)'
    helper_sheet['A2'] = 'All times are in UTC, unless otherwise specified, because Excel does not support timezones.'
    wb.save(file)
