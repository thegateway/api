import logging

import boto3
import click
from gwio.environment import Env

logger = logging.getLogger(__name__)


@click.command()
def main(key_id='903c0c29-407a-4910-9ecc-f59db0d66d5b'):
    """
    Tool to set SSM parameters in the /<appenv>/<vertical>/<param_name> hierarchy.
    :param key_id:
    :return:
    """
    keytypes = {False: 'String', True: 'SecureString'}
    vertical = click.prompt('Vertical: ', default=Env.VERTICAL)
    appenv = click.prompt('Application environment: ', default=Env.APPLICATION_ENVIRONMENT)
    Env.VERTICAL = vertical
    Env.APPLICATION_ENVIRONMENT = appenv
    client = boto3.client('ssm')
    for param_name in Env.SSM_STORED_KEYS:
        with_decryption = 'SECRET' in param_name or 'PRIVATE_KEY' in param_name
        logger.info(param_name)
        try:
            old_value = getattr(Env, param_name)
            logger.info(f"Found '{old_value}' for '{param_name}'")
        except AttributeError:
            old_value = None
            pass
        value = click.prompt(f'{param_name}', default=old_value)
        params = dict(Type=keytypes[with_decryption])
        if with_decryption:
            params['KeyId'] = key_id
        client.put_parameter(Name=f'/{appenv}/{vertical}/{param_name}', Value=value, **params)


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR)
    main()
