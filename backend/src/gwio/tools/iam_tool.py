from pprint import pprint

import click
from gwio.environment import Env


@click.group()
def cli():
    pass


@cli.command()
def create_ssm_read_policy(key_id='903c0c29-407a-4910-9ecc-f59db0d66d5b', aws_region='eu-west-1'):
    account_id = '430384725534'
    vertical = click.prompt('Vertical: ', default=Env.VERTICAL)
    appenv = click.prompt('Application environment: ', default=Env.APPLICATION_ENVIRONMENT)
    statement = {'Sid': 'VisualEditor0',
                 'Effect': 'Allow',
                 'Action': ['kms:Decrypt',
                            'ssm:GetParametersByPath',
                            'ssm:GetParameters',
                            'ssm:GetParameter'],
                 'Resource': [
                     f'arn:aws:kms:{aws_region}:{account_id}:key/{key_id}',
                     f'arn:aws:ssm:{aws_region}:{account_id}:parameter/{appenv}/{vertical}/*'
                 ]}

    policy = {'Version': '2012-10-17', 'Statement': [statement]}
    pprint(policy)


if __name__ == '__main__':
    cli()
