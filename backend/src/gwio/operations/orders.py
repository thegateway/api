import logging
import uuid
from decimal import Decimal
from typing import List

from flask import g
from werkzeug.exceptions import (
    Conflict,
    NotFound,
)

from gwio.logic.orders.types import OrderAction
from gwio.logic.payments.types import PaymentTypes
from gwio.models.orders import Order
from gwio.models.payment_method import PaymentMethod
from gwio.models.pricing import PricingError
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD

log = logging.getLogger(__name__)


def _evaluated_line(product: Product, qty: Decimal):
    ep = product.API.Assessor.evaluate(product, qty=qty)
    return product.API.Assessor.EvaluatedProduct.ProductLine.mapped(ep)


def _update_product_lines(*, order: Order, products: list):
    order.product_lines = []
    for product_data in products:
        try:
            product = Product.get(owner_guid=product_data['shop_guid'], guid=product_data['guid'])
            order.product_lines.append(_evaluated_line(product, qty=product_data['qty']))

        except (NotFound, PricingError) as e:
            log.exception(e)
            raise Conflict(description=f'Product {product_data["guid"]} not available')
        except KeyError as e:
            log.exception(e)
            raise Conflict(description=f'Error "{e}" from reading product data')
    order.shop_guids = list(set([product_line.shop_guid for product_line in order.product_lines]))


def _update_coupon(*, order: Order, coupon):
    # Resetting used status on old coupon
    try:
        old_coupon = g.user.get_coupon(order.coupon.guid)
        old_coupon.used = False
    except AttributeError:
        pass

    # Applying new coupon to order and marking it used
    order.coupon = coupon
    coupon.used = True


def _update_customer(*, order: Order, customer):
    for attr_name in ('name', 'phone', 'business_id'):
        try:
            setattr(order.customer, attr_name, customer[attr_name])
        except KeyError:
            pass


def _update_customer_pin(order: Order, *, pin: str):
    order.customer.pin = pin


def _update_shipping_address(*, order: Order, address: dict):
    order.shipping_address = address
    if not order.billing_address:
        order.billing_address = address


def _update_billing_address(*, order: Order, address: dict):
    order.billing_address = address


def _update_delivery_method(*, order: Order, delivery_method: Product):
    num_shops = len(set(line.shop_guid for line in order.product_lines if line.shop_guid != uuid.UUID(int=0)))
    product = _evaluated_line(delivery_method, qty=Decimal(num_shops))
    for line in order.product_lines:
        if line.type == PRODUCT_TYPE_DELIVERY_METHOD:
            order.product_lines.remove(line)
    order.product_lines.append(product)


def _update_payment_method(*, order: Order, payment_method: PaymentMethod, payment_method_data: dict):
    def _stripe_update(order: Order, *, card: str):
        order.flags.prepayment = True
        order.data.stripe.card_id = card

    def _paytrail_update(order: Order, *, urls: dict):
        order.flags.prepayment = True
        order.data.paytrail.urls = urls

    def _payu_update(order: Order, *, continue_url: str):
        order.flags.prepayment = True
        order.data.payu.continue_url = continue_url

    def _cod_update(order: Order):
        order.flags.prepayment = False

    _pm_specific_updates = {
        PaymentTypes.STRIPE: _stripe_update,
        PaymentTypes.PAYTRAIL: _paytrail_update,
        PaymentTypes.PAYU: _payu_update,
        PaymentTypes.COD: _cod_update,
    }

    order.payment_method_guid = payment_method.guid
    try:
        _pm_specific_updates[payment_method.type](order, **payment_method_data)
    except TypeError as e:
        raise Conflict(e)


def create_order(*, customer_name: str, products: List, customer_identity_id: str, customer_pin: str = None) -> Order:
    order = Order(buyer_identity_id=customer_identity_id,
                  customer=dict(
                      name=customer_name,
                      pin=customer_pin))
    _update_product_lines(order=order, products=products)
    order.process(OrderAction.CREATE)
    return order


def update_order(order: Order, **kwargs):
    updates = [
        lambda order, kwargs: _update_coupon(order=order, coupon=kwargs['coupon']),
        lambda order, kwargs: _update_customer(order=order, customer=kwargs['customer']),
        lambda order, kwargs: _update_customer_pin(order=order, pin=kwargs['customer_pin']),
        lambda order, kwargs: _update_product_lines(order=order, products=kwargs['products']),
        lambda order, kwargs: _update_shipping_address(order=order, address=kwargs['shipping_address']),
        lambda order, kwargs: _update_billing_address(order=order, address=kwargs['billing_address']),
        lambda order, kwargs: _update_delivery_method(order=order, delivery_method=kwargs['delivery_method']),
        lambda order, kwargs: _update_payment_method(order=order,
                                                     payment_method=kwargs['payment_method'].pop('instance'),
                                                     payment_method_data=kwargs['payment_method'])
    ]

    for u in updates:
        try:
            u(order, kwargs)
        except KeyError:
            pass
