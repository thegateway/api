# -*- coding: utf-8 -*-

import uuid

from gwio.models import user

UserData = user.UserData


# pylint: disable=W0613
# For AWS Cognito Pool pre token generation trigger function,
# event and context are always passed.
def setup_user_pin(event, context):
    try:
        # For tupas use case the pin is saved just prior to this get
        user = UserData.get(guid=uuid.UUID(event['userName']), consistent_read=True)
    except UserData.DoesNotExist:
        return event
    if user.pin:
        event['response']['claimsOverrideDetails'] = {
            'claimsToAddOrOverride': {
                'pin': user.pin
            }
        }
    return event
