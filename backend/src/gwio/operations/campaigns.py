from typing import Any, Union
from uuid import UUID

from gwio.ext import SYSTEM_GUID
# from gwio.models.pricing import PricingType
from gwio.models.tags import ProductTags, Tag

AnyProduct = Any


# pylint: disable=too-few-public-methods
class Campaign:
    """Class defining the campaign information."""
    # TODO: redo after basic assessor / evaluator is done
    pass


#    _PRICING_OPERATOR_KEY = 'operator'
#    _PRICING_VALUE_KEY = 'value'
#
#    # TODO: Copy-paste from models/pricing.py. Should be moved somewhere to be shared
#    _pricing_op = {PricingType.MULT: lambda price, value: price * value,
#                   PricingType.ADD: lambda price, value: price + Price(value, currency=price.currency),
#                   PricingType.SET: lambda price, value: Price(value, currency=price.currency),
#                   PricingType.SUB: lambda price, value: price - Price(value, currency=price.currency)}
#
#    def __init__(self, *, pricing_data: dict = None, assessor_id: UUID = None):
#        if sum(x is None for x in (pricing_data, assessor_id)) != 1:
#            raise RuntimeError('Either pricing data or assessor ID has to be defined for Campaign')
#
#        self.pricing_data = pricing_data
#        self.assessor_id = assessor_id
#
#        self._get_price = self._get_price_using_pricing_data if self.pricing_data is not None \
#            else self._get_price_using_assessor
#
#    def _get_price_using_pricing_data(self, product: AnyProduct) -> Price:
#        # data={'pricing_data': {'operator': 'MULT', 'value': 0.8}})
#        operator_value = self.pricing_data.get(self._PRICING_OPERATOR_KEY, None)
#        try:
#            operator = PricingType[operator_value]
#            operation = self._pricing_op[operator]
#        except KeyError:
#            raise RuntimeError(f'Unknown pricing data operator "#{operator_value}"')
#
#        # TODO: how to indicate missing value or price
#        value = self.pricing_data.get(self._PRICING_VALUE_KEY, None)
#        price = Price(getattr(product, 'base_price', None))
#        campaign_price = operation(price, value)
#
#        return campaign_price
#
#    def _get_price_using_assessor(self, product: AnyProduct) -> Price:
#        raise NotImplementedError
#
#    def get_price_for(self, product: AnyProduct) -> Price:
#        """Get the price for the product based on the campaign."""
#        return self._get_price(product)
#
#    @classmethod
#    def from_campaign_data(cls, data: dict) -> 'Campaign':
#        """Create a Campaign object from Tag.data."""
#        return cls(**data)
#
#
def get_preferred_campaign(shop_id: UUID, product_id: AnyProduct) -> Union[None, Campaign]:
    """
    Get the preferred campaign for the given product id. Preferred order being:
    1. Brand
    2. Shop
    3. Global
    """
    tags = ProductTags.part_of_campaigns(product_id)
    active_local_tags = [tag for tag in tags if
                         tag.owner == shop_id and tag.active]
    active_global_tags = [tag for tag in tags if tag.owner == SYSTEM_GUID and tag.active]
    # brand_tags = [...]

    # pylint: disable=no-else-return
    if len(active_local_tags) == 1:
        raise NotImplementedError
        # return Campaign.from_campaign_data(active_local_tags[0].data)
    elif len(active_global_tags) == 1:
        raise NotImplementedError
        # return Campaign.from_campaign_data(active_global_tags[0].data)
    if not active_local_tags and not active_global_tags:
        return None
    raise RuntimeError(f'Multiple local campaigns (#{len(active_local_tags)}) or global campaigns'
                       f'(#{len(active_global_tags)}) cannot be assigned for a product simultaneously')


def get_campaigns_for_shop(shop_id: UUID):
    # TODO: brand campaign
    return (
        [tag for tag in Tag.query(shop_id) if tag.active],
        [tag for tag in Tag.query(SYSTEM_GUID) if tag.active]
    )
