from gwio.models.products import Product


def organization_product_create(organization_guid, **kwargs):
    tags = kwargs.pop('tags', list())
    product = Product(owner_guid=organization_guid, **kwargs)
    for tag in tags:
        product.add_tag(tag)
    product.save()
    # Saving tags after product because if something fails here it's the product saving
    for tag in tags:
        tag.save()
    return product
