import io
import logging
from collections import Counter
from datetime import date, datetime
from decimal import Decimal
from typing import Sequence, Mapping

import xlsxwriter
from flask import send_file, Response
from xlsxwriter.worksheet import Worksheet

from gwio.logic.payments.types import PaymentTypes
from gwio.models.orders import Order
from gwio.models.webshops import Webshop

logger = logging.getLogger(__name__)


def get_vat_data(order: Order) -> Mapping[int, Decimal]:
    vat_data = Counter()
    for vat_info in order.summary.vats:
        key = vat_info.vat
        value = vat_info.amount
        vat_data[key] += value
        logger.info('%f: %f', key, value)
    return vat_data


class BookKeepingExcel:
    def __init__(self, filename='bookkeeping.xlsx'):
        self.worksheets = dict()
        self.workbook = xlsxwriter.Workbook(filename)
        self.date_format = self.workbook.add_format(dict(num_format='yyyy-mm-dd HH:mm'))
        self.row_counter = Counter()

    def get_worksheet(self, name: str) -> Worksheet:
        try:
            return self.worksheets[name]
        except KeyError:
            ws = self.workbook.add_worksheet(name)
            ws.write_row(0, 0, ('pvm', 'summa', 'vat0', 'vat10', 'vat14', 'vat24'))
            self.worksheets[name] = ws
            self.row_counter[name] += 1
            return self.worksheets[name]

    def write_line(self,
                   shop_name: str,
                   ts: datetime,
                   amount: Decimal,
                   vat_data: Mapping[int, Decimal],
                   description: str) -> None:
        ws = self.get_worksheet(shop_name)
        logger.info(vat_data)
        row = self.row_counter[shop_name]
        ws.write_datetime(row, 0, ts, self.date_format)
        ws.write(row, 1, amount)
        for i, vat in enumerate((0, 10, 14, 24), start=2):
            ws.write(row, i, vat_data.pop(vat, 0))
        if vat_data:
            logger.fatal(vat_data)
        ws.write(row, i + 1, description)
        self.row_counter[shop_name] += 1
        logger.info('%s Amount charged: %.02f - %s', ts, amount, description)

    def close(self):
        self.workbook.close()


def generate_sales_report_for_shop(excel: BookKeepingExcel,
                                   shop: Webshop,
                                   included_payment_types: Sequence[PaymentTypes] = (PaymentTypes.PAYTRAIL,
                                                                                     PaymentTypes.STRIPE),
                                   start_date: date = None,
                                   end_date: date = None) -> None:
    orders = Order.shop_index.query(shop.guid)
    for order in orders:
        if not order.status_paid:
            logger.debug("Skipping order, not paid")
            continue
        elif order.payment_method.type not in included_payment_types:
            logger.debug('PaymentType not included, skipping')
            continue
        try:
            ts = order.timestamps.paid.replace(tzinfo=None)
        except AttributeError:
            # In staging (at least) there are orders with payments but without .timestamps.paid
            ts = order.payments[-1].timestamp
        if (start_date is not None and start_date > ts.date()) or (end_date is not None and ts.date() > end_date):
            logger.debug("Skipping, not %s < %s < %s", start_date, ts, end_date)
            continue
        logger.info(order)
        vat_data = get_vat_data(order)
        excel.write_line(shop.name, ts, order.summary.price, vat_data, f'{order.reference} ({order.guid})')


def generate_sales_report(shops: Sequence[Webshop], end_date: date, start_date: date) -> Response:
    """
    Generates sales reports for shops
    """
    output = io.BytesIO()
    excel = BookKeepingExcel(output)
    for shop in shops:
        generate_sales_report_for_shop(excel, shop, start_date=start_date, end_date=end_date)
    excel.close()  # This is mandatory, otherwise the buffer will be empty!
    output.seek(0)
    return send_file(output, mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                     as_attachment=True, attachment_filename="sales_report.xlsx")
