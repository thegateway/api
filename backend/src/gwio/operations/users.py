import uuid

from gwio.api.flask_app import app


def add_to_organization(*, group_name: str, organization_guid: uuid.UUID, user_guid: uuid.UUID):
    app.customer_pool.add_user_to_group(user_guid, group_name)
    if organization_guid is not None:
        app.customer_pool.set_custom_attribute('organization_guid', str(organization_guid), user_guid)
