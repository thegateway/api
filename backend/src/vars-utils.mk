include ../../vars.mk
include build-vars.mk

GWIO_MODULES:=environment ext security tools utils
ifndef CI_PROJECT_DIR
	REPO_ROOT=$(shell git rev-parse --show-toplevel)
else
	REPO_ROOT=$(CI_PROJECT_DIR)
endif
