ifeq ($(OS),Windows_NT)
	PYPATHSH = $Env:GWIO_MODULES="$(GWIO_MODULES)"; $Env:REPO_ROOT="$(REPO_ROOT)"; $Env:GWIO_PACKAGES="$(PREFIXED_MODULES)"; $(PWSH) . $(REPO_ROOT)/.env;
else
	PYPATHSH = export GWIO_MODULES="$(GWIO_MODULES)";export REPO_ROOT="$(REPO_ROOT)"; export GWIO_PACKAGES="$(PREFIXED_MODULES)"; $(SHELL) . $(REPO_ROOT)/.env;
endif

#PIPWHEEL = $(PYPATHSH) pip wheel -f $(REPO_ROOT)/dist/ --extra-index-url=https://pypi.gwapi.eu --wheel-dir $(REPO_ROOT)/dist
PIPWHEEL = $(PYPATHSH) pip wheel --extra-index-url=https://pypi.gwapi.eu --wheel-dir $(REPO_ROOT)/dist --build $(REPO_ROOT)/pipbuild --no-deps
