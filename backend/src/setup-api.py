# coding=utf-8
import envs
from setuptools import (
    find_namespace_packages,
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'apispec>=3.0.0',
    'attrdict',
    'bcrypt',
    'boto',
    'boto3',
    'cryptography',
    'defusedxml',
    'elasticsearch>=7.0.0',
    'elasticsearch_dsl>=7.0.0',
    'envs',
    'flask',
    'flask-apispec',  # Needed for @marshal_with  TODO: reimplement the functionality in our code!
    'flask_cors',
    'flask_marshmallow',
    'gwio-dynamorm',
    'gwio-utils',
    'jinja2',
    'jinja2_s3loader',
    'marshmallow==3.0.0',
    'marshmallow_enum',
    'memoization',
    'pycountry',
    'pyfcm',
    'pyjwt',
    'python-dateutil>=2.7.0',
    'python-jose[cryptography]',
    'pytz',
    'pywebpush',
    'pyyaml>4.12',  # Can't build Linux wheels for older versions.
    'redis',
    'requests',
    'simplejson',
    'stringcase',
    'stripe',
    'transitions==0.6.8',
    'urllib3',
    'warrant>=0.6.2',
    'webargs<6.0',
    'Werkzeug',
    'xlsxwriter',
    'zeep',
]
packages = []
for module in envs.env('GWIO_PACKAGES').split(' '):
    packages.append(module)
    packages.append(f'{module}.*')
setup(
    name='gwio-api',
    version=__VERSION__,
    description='The Reseller Gateway API',
    url='https://www.gwapi.eu/',
    author='The Reseller Gateway',
    author_email='devops@gwapi.eu',
    platforms=['POSIX'],
    packages=find_namespace_packages(include=packages),
    package_data={},
    include_package_data=False,
    install_requires=install_requires,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'gw-dynamodbtool = tools.dynamodb_utils:cli',
            'gw-route53tool = tools.route53_tool:cli',
        ],
    }
)
