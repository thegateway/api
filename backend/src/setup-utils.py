# coding=utf-8

import envs
from setuptools import (
    find_namespace_packages,
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'attrdict',
    'bcrypt',
    'boto3',
    'elasticsearch>=7.0.0',
    'elasticsearch_dsl>=7.0.0',
    'envs',
    'flask',
    'jinja2_s3loader',
    'marshmallow==3.0.0',
    'marshmallow_enum',
    'openpyxl',
    'py_vapid',
    'pycountry',
    'pyfcm',
    'pyjwt',
    'pyqldb',
    'python-jose[cryptography]',
    'pytz',
    'pywebpush',
    'redis',
    'simplejson',
    'stringcase',
    'transitions',
]
packages = []
for module in envs.env('GWIO_PACKAGES').split(' '):
    packages.append(module)
    packages.append(f'{module}.*')
setup(
    name='gwio-utils',
    version=__VERSION__,
    description='The Reseller Gateway Utilities',
    url='https://www.gwapi.eu/',
    author='The Reseller Gateway',
    author_email='devops@gwapi.eu',
    platforms=['POSIX'],
    # The modules that are part of this package are defined in the `vars.mk` file
    packages=find_namespace_packages(include=packages),
    package_data={},
    include_package_data=False,
    install_requires=install_requires,
    entry_points={
        'console_scripts': [
            'gw-iamtool = gwio.tools.iam_tool:cli',
        ],
    },
    zip_safe=False,
)
