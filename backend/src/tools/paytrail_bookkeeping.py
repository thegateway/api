import logging

import click

from gwio.logic.payments.types import PaymentTypes
from gwio.models.webshops import Webshop
from gwio.operations.reports import BookKeepingExcel, generate_sales_report_for_shop

logger = logging.getLogger()


@click.group()
def cli():
    logging.basicConfig(level=logging.INFO)


@cli.command()
def fetch():
    excel = BookKeepingExcel('paytrail_reports.xlsx')
    for shop in Webshop.scan():
        generate_sales_report_for_shop(excel, shop, included_payment_types=(PaymentTypes.PAYTRAIL))
    excel.close()


if __name__ == '__main__':
    cli()
