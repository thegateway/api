import logging

import sqlalchemy
from envs import env
from gwio.core.models.erp import OrderEvent
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlservice import declarative_base

SQLALCHEMY_DATABASE_URI = env('SQLALCHEMY_DATABASE_URI')

engine = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)

logger = logging.getLogger(__name__)

EVENT_ATTRS = (
    ('ordered_at', 'placed', 'comment'),
    ('shipped_at', 'shipped', 'shipping_notes'),
    ('pharmacy_shipped_at', 'shipped', 'pharmacy_tracking_code'),
    ('accepted_at', 'accepted', None),
    ('customer_med_advice_given_at', 'medicine_advice_given', 'customer_med_advisor'),
)
Base = declarative_base()
metadata = Base.metadata


class Order(Base):
    """
    Use this only for data migration below.
    """
    __tablename__ = 'order'

    id = Column(Integer, primary_key=True)
    ordered_at = Column(DateTime)
    shipped_at = Column(DateTime)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)
    comment = Column(String(800))
    notes = Column(String(5000))
    packed_at = Column(DateTime)
    shipping_notes = Column(String(2000))
    packing_notes = Column(String(2000))
    pharmacy_shipped_at = Column(DateTime)
    accepted_at = Column(DateTime)
    customer_med_advice_given_at = Column(DateTime)
    customer_med_advisor = Column(String(255))
    pharmacy_tracking_code = Column(String(255))


def process_orders():
    session = Session()
    for c, order in enumerate(session.query(Order).all()):
        for attr, ev_type, field in EVENT_ATTRS:
            ts = getattr(order, attr)
            if ts is None:
                continue
            logger.debug('{}: {}'.format(ev_type, ts))
            try:
                value = getattr(order, field)
                if value is None:
                    raise TypeError
                data = {field: value}
            except TypeError:
                data = None
            event = OrderEvent(order_id=order.id, ts=ts, data=data, event_type=ev_type)
            session.add(event)
        logger.debug(order)
        if c % 50 == 0:
            logger.info("Committing {}".format(c))
            session.commit()
    session.commit()


def main():
    create_table()
    process_orders()
    print("Completed. Now run sql/drop_at_fields_from_order.sql")


def create_table():
    try:
        OrderEvent.__table__.drop(engine)
    except Exception:
        pass
    OrderEvent.__table__.create(engine)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
