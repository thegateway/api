import json
import sys
import urllib.error
import urllib.request
from collections import defaultdict
from datetime import datetime
from random import choice

import boto3
import botocore
from gwio.environment import Env
from sqlalchemy import create_engine

from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DEFAULT
from gwio.models.webshop_product import WebshopProduct
from gwio.models.webshops import Webshop

s3 = boto3.resource('s3')


def _print_progression(verbose: bool, character: str = None) -> None:
    if verbose:
        sys.stdout.write(character if character else choice(('.', ',')))
        sys.stdout.flush()


def _remove_product_from_stores(product: Product, webshops: list, counts: dict) -> None:
    try:
        counts['remove'][str(product.guid)] = []
    except TypeError:  # `counts` is defaultdict and defaults to list
        counts['remove'] = {str(product.guid): []}

    for webshop in webshops:
        try:
            webshop_product = WebshopProduct.get(product.guid, webshop.guid)
            webshop_product.delete()
            counts['remove'][(str(product.guid))].append(str(webshop_product.webshop_guid))
        except WebshopProduct.DoesNotExist:
            pass


def _update_product_image(product: Product, webshops: list, urls: dict, counts: dict) -> bool:
    try:
        code = product.data['yksilointitunnus']
    except (KeyError, TypeError):  # Data might be None
        code = None

    try:
        name = f"{product.data['kauppanimi']} ({product.data['kokoteksti']})".lower()
    except (KeyError, TypeError):  # Data might be None
        name = None

    if not code and not name:
        counts['no name/code'].append(str(product.guid))
        return False

    try:
        matches = [x for x in urls.values() if (code is not None and x['code'] == code) or (name is not None and x['name'] == name)]
        if not matches:
            _remove_product_from_stores(product, webshops, counts)
            return False

        if 'prescription_medicine' in [x['type'] for x in matches]:
            counts['prescription_medicine'].append(str(product.guid))
            return False

        url = next((url for x in matches for url in x['images']))
    except StopIteration:
        counts['missing_old_image'].append(str(product.guid))
        return False

    product.image = url
    return True


def _import_images(db_conn, verbose: bool):
    counts = defaultdict(list)

    if verbose:
        print('Querying old database')
    result = db_conn.execute(f'''
SELECT p.id, COALESCE(p.vnr_code, p.supplier_sku_code, p.sku_code), p.name, pi.url, p.type
FROM product p 
LEFT JOIN product_image pi ON p.id = pi.product_id''')

    if verbose:
        print('Converting results to dict and set')

    urls = dict()
    for id, code, name, url, type in result:
        print(str(id))

        try:
            urls[id]['code'] = code
        except KeyError:
            urls[id] = dict(code=code, name=name.lower(), type=type, images=list())

        if not url:
            continue
        _, host, path, *__ = urllib.parse.urlsplit(url)
        try:
            s3.Object(host.split('.', 1)[0], path[1:]).load()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == '404':
                continue
            raise
        urls[id]['images'].append(url)
    assert urls

    if verbose:
        print('Querying webshops')
    webshops = list(Webshop.scan())
    if verbose:
        print('Starting product loop')
    for product in Product.TypeIndex.query(PRODUCT_TYPE_DEFAULT):
        product = Product.by_guid(product.guid)

        if not _update_product_image(product, webshops, urls, counts):
            _print_progression(verbose)
            continue

        try:
            product.save()
            counts['imported'].append(str(product.guid))
            _print_progression(verbose, '+')
        except Exception:
            counts['save_failed'].append(str(product.guid))
            _print_progression(verbose, '!')

    if verbose:
        print(' Done')
        print('Counts')
        for name, guids in counts.items():
            print(f'{name}: {len(guids)}')

    return counts


if __name__ == '__main__':
    print(f'''Make sure the following information is correct before running the import.

product table: {Product.Meta.table_name}
elastic search: {Env.ELASTICSEARCH_HOST}

Run the import from with `python -c "from _import_old_imeds_product_images import main; main('mysql+pymysql://user:password@localhost:3306/db_name', verbose=True)"`
''')


def main(db_connection_string, *, verbose=False):
    assert 'imeds' == Env.DEFAULT_VERTICAL_NAME

    engine = create_engine(db_connection_string)
    connection = engine.connect()

    start = datetime.now()
    counts = _import_images(db_conn=connection, verbose=verbose)
    execution_time = (datetime.now() - start).seconds

    if verbose:
        print(f'Execution time {int(execution_time / 60)}m {execution_time % 60}s')

    connection.close()

    with open(f'product_image_import_{str(datetime.now())}.json', 'w') as fp:
        json.dump(counts, fp)
