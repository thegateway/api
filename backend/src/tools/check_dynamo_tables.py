# coding=utf-8

from gwio.ext.dynamorm import all_tables
from gwio.models import *
from gwio.models.user import UserData

try:
    from gwio.remppa.models import *

    assert offer
except ModuleNotFoundError:
    pass

assert orders
assert UserData

from gwio.ext.pynamodb import all_models

for model in all_models + all_tables:
    try:
        if not model.exists():
            print(f"Missing table {model.Meta.table_name}")
        else:
            print(f"Have table {model.Meta.table_name}")
    except TypeError:
        if not model.exists:
            print(f"Missing table {model.name}")
        else:
            print(f"Have table {model.name}")
