"""
For testing purposes you can export the logs using this guide:

https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/S3ExportTasksConsole.html

There is a CWLExportUser, ask kimvais for password.

Bucket gwio-scratch with prefix 9116ab98-dbc2-4fac-a1ef-45dbe593f32f can be used.

To download the logs, cd to tests/data/ and do

Read-S3Object -bucket gwio-scratch -KeyPrefix 9116ab98-dbc2-4fac-a1ef-45dbe593f32f -Folder cw_logs
(or aws s3 sync s3://gwio-scratch/ 9116ab98-dbc2-4fac-a1ef-45dbe593f32f cw_logs/

use convert_logs_to_json.py to process the gzip file hierarchy.


"""
import json
import logging
from decimal import Decimal

import click
import marshmallow
import pandas as pd

from gwio.core.cw_logging import Event, LogEventType

logger = logging.getLogger(__name__)


def get_messages(infile):
    for message in json.load(infile):
        if message['audit']:
            yield Event.audit_schema.load(message)
        else:
            try:
                yield Event.schema.load(message)
            except marshmallow.exceptions.ValidationError as e:
                message['data'] = json.loads(message['data'])
                yield Event.schema.load(message)


@click.command()
@click.argument('infile', type=click.File('r'), default="cw_insights.json")
def main(infile):
    freq = '1d'
    df = pd.DataFrame(get_messages(infile))
    # Index by timestamp
    df.set_index('timestamp', inplace=True)

    # Popular products
    print("\nPopular products")
    # Move the "object" to top level of the dataframe.
    with_unnested_objects = pd.concat([df.drop(['object'], axis=1), df['object'].apply(pd.Series)], axis=1)
    pop_products = with_unnested_objects.groupby(pd.Grouper(key='object_type')).get_group('Product').groupby(pd.Grouper(key='name'))
    print(pop_products.size().sort_values(ascending=False))

    # Activity over time
    print('\nActivity over time')
    # This puts the number of events grouped by 'freq' in to buckets by count.
    activity_over_time = df.groupby(pd.Grouper(level='timestamp', freq=freq)).size()
    print(activity_over_time)

    # Payments
    print('\nPayments over time')
    df2 = df.loc[df['event_type'] == LogEventType.PAYMENT_FINALIZE]
    payments = pd.concat([df2.drop(['data'], axis=1), df2['data'].apply(pd.Series)], axis=1)
    payments['amount'] = payments['amount'].apply(lambda amount: Decimal(amount) / 100)
    payments_over_time = payments.groupby(pd.Grouper(level='timestamp', freq=freq))['amount'].sum()
    print(payments_over_time)


if __name__ == '__main__':
    main()
