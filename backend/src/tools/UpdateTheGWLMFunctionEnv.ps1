Function Update-TheGwLMFunctionEnv
{
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true,
                HelpMessage = "Function name.")]
        [string]$FunctionName,

        [Parameter(Mandatory = $true,
                HelpMessage = "Gateway namespace (/<application_environment>/<vertical_name>) ")]
        [string]$Namespace
    )
    Begin {
        $lambda_function_env = (Get-LMFunctionConfiguration -FunctionName $FunctionName).Environment.Variables
        $ssm_params = Get-SSMParametersByPath -Path $Namespace -WithDecryption $true|Select-Object @{
            l = "Key"; e = {
                $_.Name.Substring($Namespace.Length + 1)
            }
        }, Value
        $ssm_params|ForEach-Object {
            $lambda_function_env.($_.Key) = $_.Value
        }
    }
    Process {
        Update-LMFunctionConfiguration -FunctionName $FunctionName -Environment_Variables $lambda_function_env
    }
    End {
    }

}


