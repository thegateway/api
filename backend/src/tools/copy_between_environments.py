# coding=utf-8
import logging
import sys
import time

import boto3

import gwio.models
from gwio.ext.pynamodb import all_models

logger = logging.getLogger(__name__)

assert gwio.models

MAX_RETRIES = 10
WAIT_TIME = 0


def main(source_env, target_env):
    client = boto3.client('dynamodb')

    for model in all_models:
        table_name = model.Meta.table_name
        print(f'Copying information from {source_env} to {target_env} table {table_name}')
        backup_response = client.create_backup(TableName=table_name, BackupName=f'{table_name}-to-dev')
        backup_arn = backup_response['BackupDetails']['BackupArn']

        backup_status = backup_response['BackupDetails']['BackupStatus']
        retries = 0
        while backup_status != 'AVAILABLE' and retries < MAX_RETRIES:
            backup_status = client.describe_backup(BackupArn=backup_arn)['BackupDescription']['BackupDetails']['BackupStatus']
            time.sleep(retries)
            retries += 1

        in_use = False
        split_table_name = table_name.split('-')
        split_table_name[1] = target_env
        target_table_name = '-'.join(split_table_name)
        try:
            delete_response = client.delete_table(TableName=target_table_name)
            print(f'Deleting existing target table {target_table_name}')
            try:
                retries = 0
                while retries < MAX_RETRIES:
                    describe_response = client.describe_table(TableName=target_table_name)
                    time.sleep(retries)
                    retries += 1
            except Exception as e:
                if 'Requested resource not found' not in str(e):
                    raise
        except Exception as e:
            in_use = True
            if 'Requested resource not found' not in str(e) and 'Attempt to change a resource which is still in use' not in str(e):
                raise

        retries = 0
        while retries < MAX_RETRIES:
            try:
                restore_response = client.restore_table_from_backup(TargetTableName=target_table_name, BackupArn=backup_arn)
                print(restore_response)
            except Exception as e:
                if 'Subscriber limit exceeded' in str(e):
                    retries +=1
                    time.sleep(retries * 60)
                    continue
                elif 'Attempt to change a resource which is still in use' not in str(e) and 'is already being restored from backup' not in str(e) and (not in_use and ('Table already exists' not in str(e))):
                    # Resource in use means it's being created
                    raise
                break


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main(sys.argv[1], sys.argv[2])
