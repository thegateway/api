import logging

from envs import env
from gwio.core.models import erp
from sqlalchemy import create_engine
from sqlalchemy.exc import InternalError
from sqlalchemy.orm import sessionmaker

from gwio.models.webshops import WebShop, WebShopPaymentMethod, WebShopHasPaymentMethod

SQLALCHEMY_DATABASE_URI = env('SQLALCHEMY_DATABASE_URI')
engine = create_engine(SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)

logger = logging.getLogger(__name__)


def main():
    try:
        WebShopHasPaymentMethod.__table__.drop(bind=engine)
        WebShopPaymentMethod.__table__.drop(bind=engine)
    except InternalError:
        pass
    WebShopPaymentMethod.__table__.create(bind=engine)
    engine.execute("""
    CREATE TABLE ws_shop_has_payment_method (
    webshop_id CHAR(32) NOT NULL REFERENCES ws_shops(id),
    payment_method_id CHAR(32) NOT NULL REFERENCES ws_payment_methods (id),
    data TEXT,
    PRIMARY KEY (webshop_id, payment_method_id)
    )
    """)

    session = Session()
    paytrail = WebShopPaymentMethod(name="paytrail", description="Verkkopankki",
                                    logo="https://www.paytrail.com/hs-fs/hub/335946/file-493287103-png/images/paytrail-logo-200x200.png")
    stripe = WebShopPaymentMethod(name="stripe", description="Pankki- tai luottokortti",
                                  logo="https://stripe.com/img/about/logos/logos/blue.png")
    session.add(stripe)
    session.add(paytrail)
    for webshop in session.query(WebShop).all():
        actor_paytrail = session.query(erp.ActorPaytrail).filter(erp.ActorPaytrail.actor_id == webshop.actor_id).one()
        session.add(WebShopHasPaymentMethod(shop=webshop, payment_method=stripe, data=dict(key=webshop.stripe_key)))
        try:
            if not all((actor_paytrail.merchant_id, actor_paytrail.merchant_hash)):
                raise AttributeError
            session.add(WebShopHasPaymentMethod(shop=webshop, payment_method=paytrail,
                                                data=dict(merchant_id=actor_paytrail.merchant_id,
                                                          merchant_hash=actor_paytrail.merchant_hash)))
        except AttributeError as e:
            logger.error("{} has no paytrail ".format(webshop.name))
            pass
    session.commit()


if __name__ == '__main__':
    main()
