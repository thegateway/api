import sys
import time
from collections import defaultdict

import click
import uuid
from gwio.core.models.base import Model
from gwio.core.models.erp import Order as OldOrder, OrderEvent
from gwio.core.models.web_shop import WebShop
from gwio.utils.uuid import get_uuid
from gwio.models.orders import Order as NewOrder
from gwio.operations.ddb_orders import new_order_reference
from gwio.schemas import WebShopSchema, schema_dump
from pynamodb.exceptions import PutError
from sqlalchemy import text
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm.exc import NoResultFound
from sqlservice import SQLClient
from typing import Union


class ClickDbConnectionParamType(click.ParamType):
    """
    Class for creating custom click.argument type
    """
    name = 'sqlalchemy-url'

    def convert(self, value, param, ctx):
        try:
            config = {'SQL_DATABASE_URI': value}
            db = SQLClient(config=config, model_class=Model)

        except OperationalError as e:
            self.fail('Could not connect to {url} ({msg})'.format(
                url=value, msg=e), param, ctx)

        return db


def _set_buyer_uuid(*, new_order, old_order, migration_tracker):
    try:
        new_order.buyer_uuid = uuid.UUID(old_order.buyer.aws_guid)
    except TypeError:
        migration_tracker['errors'][old_order.id].append('Customer not migrated to cognito')
        return None
    except AttributeError:
        migration_tracker['skips'][old_order.id].append('Order has no buyer')
        return None


def _placed_event_to_state(old_order):
    if old_order.require_acceptance:
        return NewOrder.State.SELLER_PROCESSING.value
    elif 'paid' in (e.event_type for e in old_order.events):
        return NewOrder.State.WAITING_SHIPPING.value
    else:
        return NewOrder.State.WAITING_PAYMENT.value


def _payment_event_to_state(old_order):
    if 'paid' in (e.event_type for e in old_order.events):
        return NewOrder.State.WAITING_SHIPPING.value
    else:
        return NewOrder.State.WAITING_PAYMENT.value


def _set_state(*, new_order, old_order):
    event_to_state = dict(placed=_placed_event_to_state,
                          medicine_advice_given=NewOrder.State.SELLER_PROCESSING.value if old_order.require_acceptance else NewOrder.State.WAITING_SHIPPING.value,
                          accepted=NewOrder.State.WAITING_SHIPPING.value,
                          rejected=NewOrder.State.CANCELED.value,
                          smartpost=_placed_event_to_state,
                          modified=NewOrder.State.SELLER_PROCESSING.value,
                          payment=_payment_event_to_state,
                          paid=NewOrder.State.WAITING_SHIPPING.value,
                          stripe_charge_error=NewOrder.State.CUSTOMER_PROCESSING.value,  # charge creation failure
                          shipped=NewOrder.State.SHIPPED.value)
    try:
        new_order.state = event_to_state[old_order.latest_event.event_type](old_order)
    except TypeError:
        new_order.state = event_to_state[old_order.latest_event.event_type]


def _add_event(*, new_order, event, old_event):
    # TODO: Check data structure
    new_order.events.append(dict(type=event.value,
                                 ts=str(old_event.ts),
                                 data=old_event.data))


def _migrate_events(*, new_order, old_order):
    # placed
    placed_event = next(e for e in old_order.events if e.event_type == OrderEvent.type_placed)
    _add_event(new_order=new_order, event=NewOrder.Case.CREATE, old_event=placed_event)
    _add_event(new_order=new_order, event=NewOrder.Trigger.PLACE, old_event=placed_event)

    # modified
    # medicine_advice_given
    for old_update_event in (e for e in old_order.events if e.event_type in (OrderEvent.type_modified,
                                                                             OrderEvent.type_medicine_advice_given)):
        _add_event(new_order=new_order, event=NewOrder.Trigger.UPDATE, old_event=old_update_event)

    # accepted
    old_accepted_event = next(e for e in old_order.events if e.event_type == OrderEvent.type_accepted)
    _add_event(new_order=new_order, event=NewOrder.Trigger.SELLER_CONFIRM, old_event=old_accepted_event)

    # rejected
    old_rejected_event = next(e for e in old_order.events if e.event_type == OrderEvent.type_rejected)
    _add_event(new_order=new_order, event=NewOrder.Trigger.CANCEL, old_event=old_rejected_event)

    # charge
    old_charge_event = next(e for e in old_order.events if e.event_type == OrderEvent.charge)
    _add_event(new_order=new_order, event=NewOrder.Trigger.CREATE_STRIPE_CHARGE, old_event=old_charge_event)

    # payment
    # paid (ignored because it is always preceded by payment event and it contains no extra information)
    for old_payment_event in (e for e in old_order.events if e.event_type == OrderEvent.type_payment):
        _add_event(new_order=new_order, event=NewOrder.Trigger.PAY, old_event=old_payment_event)

    # shipped
    old_shipped_event = next(e for e in old_order.events if e.event_type == OrderEvent.type_shipped)
    _add_event(new_order=new_order, event=NewOrder.Trigger.SHIP, old_event=old_shipped_event)

    # stripe_charge_error
    for old_charge_error_event in (e for e in old_order.events if e.event_type == 'stripe_charge_error'):
        _add_event(new_order=new_order, event=NewOrder.Case.FAILED_STRIPE_CHARGE, old_event=old_charge_error_event)

    # stripe_charge_success
    # smartpost

    # TODO:
    # stripe_charge_success
    #   capture=True
    #       -> NewOrder.Trigger.CREATE_STRIPE_CHARGE
    #       -> NewOrder.Trigger.PAY
    #       paid is empty, ignore
    #   capture=False
    #       -> NewOrder.Trigger.CREATE_STRIPE_CHARGE
    #       paid -> NewOrder.Trigger.PAY


def _set_data(*, new_order, old_order, shop):
    new_order.data = dict(seller=schema_dump(WebShopSchema, shop),
                          lines=[])

    try:
        new_order.data['delivery_address'] = dict(name=old_order.delivery_address.name,
                                                  street_address=old_order.delivery_address.street_address,
                                                  postal_code=old_order.delivery_address.postal_code,
                                                  country=old_order.delivery_address.country)
    except AttributeError:
        pass

    # TODO: Migrate order line data
    for old_product_line in old_order.product_lines:
        new_product_line = dict()
        new_order.data['lines'].append(new_product_line)

    _migrate_events(new_order=new_order, old_order=old_order)



def migrate_order(old_order: OldOrder, *, migration_tracker, verbose, db) -> Union[NewOrder, None]:
    try:
        shop = db.query(WebShop).filter(WebShop.actor_id == old_order.seller_id).one()
    except NoResultFound:
        migration_tracker['errors'][old_order.id].append('Shop has no WebShop record')
        return None

    try:
        new_order = NewOrder.get(old_order.uuid)
    except ValueError:
        reference = new_order_reference()
        new_order = NewOrder(uuid=get_uuid(str(reference), ns=shop.id),
                             reference=reference)

    new_order.shop_uuid = shop.id

    _set_buyer_uuid(new_order=new_order, old_order=old_order, migration_tracker=migration_tracker)
    _set_state(new_order=new_order, old_order=old_order)
    _set_data(new_order=new_order, old_order=old_order, shop=shop)

    saved = False
    while not saved:
        try:
            new_order.save()
            saved = True
        except PutError as e:
            if e.cause.response['Error']['Code'] == 'ProvisionedThroughputExceededException':
                if verbose:
                    print(f'Chilling for a bit because DynamoDb throughput was exceeded\r')
                time.sleep(2)
            else:
                raise

    old_order.uuid = str(new_order.uuid)
    db.session.commit()
    return new_order


@click.command(name='migrate-orders-to-ddb')
@click.option('--verbose', type=bool, default=False, required=False)
@click.option('--skip-migrated', type=bool, default=True, required=False)
@click.option('--where', type=click.STRING, required=True)
@click.argument('db', metavar='database', envvar='SQL_CONNECT', type=ClickDbConnectionParamType())
def migrate_orders_to_ddb(verbose, skip_migrated, where, db):
    query = db.query(OldOrder)
    if where:
        query = query.filter(text(where))
    if skip_migrated:
        old_order_count_no_skip = query.count()
        query = query.filter(text(f'uuid IS NULL'))
    old_order_count = query.count()
    old_orders = query.all()
    if verbose:
        print(f'Starting migration for {old_order_count} orders\r')

    migration_tracker = dict(skips=defaultdict(list),
                             errors=defaultdict(list))

    for old_order in old_orders:
        if verbose:
            sys.stdout.write(f'Migrating order #{old_order.id}\r')
            sys.stdout.flush()
        try:
            new_order = migrate_order(old_order,
                                      verbose=verbose,
                                      migration_tracker=migration_tracker,
                                      db=db)
            if new_order is None:
                continue
            if verbose:
                print(f'Migrated order #{old_order.id} to {new_order.uuid}\r')
        except Exception:
            print(f'Failed migrating order #{old_order.id}\r')
            raise

    if verbose:
        migrated_count = old_order_count - len(migration_tracker['skips']) - len(migration_tracker['errors'])
        print(f'Successfully migrated {migrated_count}/{old_order_count} of the orders\r')
        print(f"Skipped {len(migration_tracker['skips'])}/{old_order_count} of the orders\r")
        print(f"Failed to migrate {len(migration_tracker['errors'])}/{old_order_count} of the orders\r")
        if migration_tracker['errors']:
            print(migration_tracker['errors'][next(iter(migration_tracker['errors']))])
        if skip_migrated:
            print(
                f'{old_order_count_no_skip - old_order_count} orders matching the migration condition were not included because they were already migrated\r')


if __name__ == '__main__':
    migrate_orders_to_ddb()
