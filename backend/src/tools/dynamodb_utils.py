import logging
import pkgutil
import time

import boto3
import click

from gwio.core import env

client = boto3.client('dynamodb')
logger = logging.getLogger(__name__)


@click.group()
@click.option('-i', '--interactive', is_flag=True)
@click.option('-f', '--filter')
@click.pass_context
def cli(ctx, interactive, filter):
    """
    A tool to perform operations on DynamoDB tables.
    """
    ctx.ensure_object(dict)
    ctx.obj['INTERACTIVE'] = interactive
    ctx.obj['FILTER'] = filter


@cli.command()
@click.pass_context
def add_field(ctx):
    """
    Add a new field with a specific value for all existing items
    """

    def import_model(model_name: str):
        """
        Import the given model
        :param model_name: The name of the model to import
        :return:
        """
        for importer, modname, ispkg in pkgutil.walk_packages(path=models.__path__, prefix=models.__name__ + '.', onerror=lambda x: None):
            _temp = __import__(modname, fromlist=[''])
            try:
                return getattr(_temp, model_name)
            except AttributeError:
                pass
        return None

    # Getting the inputs that are required before vertical specific models can be imported
    env.VERTICAL = click.prompt('Vertical name', default=env.VERTICAL)
    env.APPLICATION_ENVIRONMENT = click.prompt('Environment name', default=env.APPLICATION_ENVIRONMENT)

    # Importing after the env variable are defined so that products don't break
    try:
        from gwio import models

        assert models
    except AttributeError:
        print('Invalid `VERTICAL` name!')
        exit()

    # Getting rest of the inputs
    model_name = click.prompt('Model name')
    attribute_name = click.prompt('Attribute name')
    attribute_value_eval = click.prompt('Field value (`eval`)')
    imports_for_attr_eval = click.prompt('Imports statements required by attribute eval (separate by ,)')

    # Executing import statements so the evaluated attribute value can be confirmed, unless it uses `item`
    import_statements = imports_for_attr_eval.split(',')
    for import_statement in import_statements:
        exec(import_statement.strip())

    # Just used as the default value, because `None` might be the default value for the new attribute
    class NoValue:
        pass

    # Trying to evaluate the value for attribute so that it can be confirmed with the user
    evaluated_value = NoValue
    try:
        evaluated_value = eval(attribute_value_eval)
    except NameError as e:
        if str(e) != "name 'item' is not defined":
            raise

    # Just for the confirmation text and outside of it to help readability a bit
    imports_for_print = '\n'.join([' ' * 7 + x for x in import_statements]).strip()

    # Confirming the inputs are correct before running the script
    print(f"""
    !!!MAKE SURE THESE ARE CORRECT!!!
    Vertical    {env.VERTICAL}
    Env         {env.APPLICATION_ENVIRONMENT}
    Model       {model_name}
    Attribute   {attribute_name}
    {f'Value       {evaluated_value}' if evaluated_value != NoValue else f'Could not evaluate attribute value example because it uses `item`'}

    Eval logic
        {imports_for_print}
        value = {attribute_value_eval}
    """)
    click.confirm('Run the script?', abort=True)

    klass = import_model(model_name)

    count = 0
    try:
        for item in klass.scan():
            # We don't want to override any existing information
            if not getattr(item, attribute_name):
                # Preferring the evaluated value that was confirmed already
                if evaluated_value == NoValue:
                    evaluated_value = eval(attribute_value_eval)
                setattr(item, attribute_name, evaluated_value)
                item.save()
                count += 1
    except AttributeError:
        print('Invalid model name!')
        exit()

    print(f'Updated {count} items')


@cli.command()
@click.pass_context
def bill_by_request(ctx):
    """
    Set table's billing mode to PAY_PER_REQUEST
    """
    for table in get_tables(ctx.obj['INTERACTIVE'], ctx.obj['FILTER']):
        try:
            billing_mode = table['BillingModeSummary']['BillingMode']
        except KeyError:
            billing_mode = 'PROVISIONED'
        if billing_mode != 'PAY_PER_REQUEST':
            table_name = table['TableName']
            logger.info(f'Setting {table_name} to PAY_PER_REQUEST billing mode')
            while True:
                try:
                    logger.info(f'Updating billing mode on table {table_name}')
                    client.update_table(TableName=table_name, BillingMode='PAY_PER_REQUEST')
                    break
                except Exception as e:
                    if 'LimitExceededException' in str(type(e)):
                        # Wait for a moment before attempting again
                        logger.warn('Limit exceeded. Trying again in a moment')
                        time.sleep(30)
                    else:
                        raise


@cli.command()
@click.pass_context
def delete_temp_tables(ctx):
    """
    Delete tables with name starting with '__TEMP'
    """
    prompt = ctx.obj['INTERACTIVE']
    for table in get_tables(False, ctx.obj['FILTER']):
        table_name = table['TableName']
        if table_name.startswith('__TEMP') and prompt and click.confirm(f'{table_name}'):
            logger.info(f'Deleting table {table_name}')
            client.delete_table(TableName=table_name)


@cli.command()
@click.pass_context
def delete_tables(ctx):
    """
    Delete all tables. Always interactive.
    """
    for table in get_tables(True, ctx.obj['FILTER']):
        table_name = table['TableName']
        if click.prompt(f'Enter the table name to delete it:') == table_name:
            client.delete_table(TableName=table_name)


@cli.command()
@click.pass_context
def list_tables(ctx):
    """
    List all tables.
    """
    for table in get_tables(False, ctx.obj['FILTER']):
        table_name = table['TableName']
        print(table_name)


@cli.command()
@click.pass_context
def enable_backups(ctx):
    """
    Enable point-in-time backups for tables with 'production' in their name.
    """
    for table in get_tables(ctx.obj['INTERACTIVE'], ctx.obj['FILTER']):
        table_name = table['TableName']
        if 'production' in table_name:
            try:
                enabled = table['PointInTimeRecoverySpecification']['PointInTimeRecoveryEnabled']
                if enabled:
                    continue
            except KeyError:
                pass
            logger.info(f'Enabling point-in-time backups for {table_name}')
            response = client.update_continuous_backups(
                TableName=table_name,
                PointInTimeRecoverySpecification={
                    'PointInTimeRecoveryEnabled': True
                }
            )
            try:
                status = response['ContinuousBackupsDescription']['ContinuousBackupsStatus']
                if status != 'ENABLED':
                    raise KeyError
            except KeyError:
                logger.fatal(f'Failed with: {response}')


def get_tables(prompt, filter=None):
    """
    A helper function to get table descriptions.
    """
    tables_info = client.list_tables()
    while True:
        for table_name in (t for t in tables_info.get('TableNames', list()) if filter is None or filter in t):
            table = client.describe_table(TableName=table_name)
            if not prompt or click.confirm(f'{table_name}'):
                yield table['Table']
        exclusive_start = tables_info.get('LastEvaluatedTableName')
        if exclusive_start is None:
            break
        tables_info = client.list_tables(ExclusiveStartTableName=exclusive_start)


if __name__ == '__main__':
    # pylint: disable=E1120, E1123
    cli(obj={})
