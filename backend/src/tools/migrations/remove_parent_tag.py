import concurrent
import uuid
from collections import defaultdict

import backoff
import click
from dynamorm import (
    DynaModel,
    GlobalIndex,
    ProjectAll,
)
from elasticsearch import (
    ConnectionError,
    TransportError,
)
from marshmallow import fields
from urllib3.exceptions import (
    NewConnectionError,
    ReadTimeoutError,
)

from gwio.ext.dynamorm import (
    GwioModelField,
    HotfixMixIn,
    adapted_dynamodel,
)
from gwio.ext.marshmallow import GwioEnumField
from gwio.models import (
    modifiers,
    tags,
)
from gwio.models.products import Product
from gwio.models.tags import Tag

old_tag_cache = dict()
products_to_save = dict()
tags_to_save = dict()
tag_base_schema = Tag.Schema()
tag_base_schema_for_comparison = Tag.Schema(exclude=['product_guids'])
tag_conversions = defaultdict(list)


@adapted_dynamodel
class OldTag(HotfixMixIn, DynaModel):
    class Table:
        name = 'defined-tags'
        hash_key = 'owner'
        range_key = 'guid'

    class Schema:
        guid = fields.UUID()
        owner = fields.UUID()
        name = fields.String(required=True)
        data = GwioModelField(modifiers.Modifier, allow_none=True)
        parent_guid = fields.UUID()
        duration = GwioModelField(tags.Duration, allow_none=True)
        type = GwioEnumField(enum=tags.Tag.API.Type, by_value=True, missing=tags.Tag.API.Type.CATEGORY)
        images = fields.Nested(tags.TagImageSchema, many=True, missing=list)
        item_count = fields.Integer(required=False, allow_none=True)  # Deprecated
        product_guids = fields.List(fields.UUID(), missing=list)

    @adapted_dynamodel
    class GuidIndex(GlobalIndex):
        """Index for quickly accessing individual tags"""
        name = 'tags-index'
        hash_key = 'guid'
        projection = ProjectAll()

    @classmethod
    def by_guid(cls, guid: uuid.UUID) -> 'OldTag':
        """Get the Tag by id"""
        # pylint: disable=no-value-for-parameter
        for item in cls.GuidIndex.query(guid=str(guid)):
            return item
        raise cls.DoesNotExist

    def parents_generator(self, visited):
        if self.guid in visited:
            return
        visited.add(self.guid)
        if self.parent_guid is not None and self.guid != self.parent_guid:
            try:
                yield from OldTag.get_cached(self.parent_guid, old_tag_cache).parents_generator(visited)
            except OldTag.DoesNotExist:
                pass
        yield self

    @classmethod
    def get_cached(cls, guid, cache: dict):
        try:
            return cache[guid]
        except KeyError:
            cache[guid] = cls.by_guid(guid=guid)
        return cache[guid]


def cached_product(guid, cache: dict):
    try:
        return cache[guid]
    except KeyError:
        cache[guid] = Product.by_guid(guid)
    return cache[guid]


tags_done = set()
tags_total = OldTag.Table.table.item_count
duplicate_non_matching_tag_guids = set()


def giveup(details):
    print(details)
    exit(1)


def handle_tag(old_tag, child_product_guids=None):
    try:
        product_guids = set(list(child_product_guids) + list(old_tag.product_guids))
    except TypeError:
        product_guids = list(old_tag.product_guids)

    for product_guid in product_guids:
        try:
            product = cached_product(product_guid, products_to_save)
            product.remove_tag(old_tag)
        except Product.DoesNotExist:
            pass

    old_tag_serialised = tag_base_schema.dump(old_tag)
    del old_tag_serialised['guid']
    new_tag = Tag(**tag_base_schema.load(old_tag_serialised))

    try:
        new_tag = Tag.by_guid(new_tag.guid)
    except Tag.DoesNotExist:
        pass

    try:
        # same owner and name, but different parent, will cause the tags to being merged
        existing_new_tag = tags_to_save[new_tag.guid]
        if tag_base_schema_for_comparison.dump(new_tag) == tag_base_schema_for_comparison.dump(existing_new_tag):
            new_tag = existing_new_tag
        else:
            # merged tags had different data so need to change guid to random
            new_tag.guid = uuid.uuid4()
            duplicate_non_matching_tag_guids.add(new_tag.guid)
    except KeyError:
        tags_to_save[new_tag.guid] = new_tag
    tag_conversions[new_tag.guid].append(old_tag)

    for product_guid in product_guids:
        try:
            product = cached_product(product_guid, products_to_save)
            product.add_tag(new_tag)
        except Product.DoesNotExist:
            pass

    for parent_tag in old_tag.parents_generator(set()):
        if parent_tag.guid == old_tag.guid:
            continue  # `parents_generator` returns also the tag itself
        handle_tag(parent_tag, product_guids)


@backoff.on_exception(
    backoff.expo,
    (TransportError, ReadTimeoutError, ConnectionError, NewConnectionError, ConnectionRefusedError),
    max_time=60 * 60,  # an hour
    on_giveup=giveup
)
def migrate_tag(old_tag):
    old_tag_cache[old_tag.guid] = old_tag
    handle_tag(old_tag)
    tags_done.add(old_tag.guid)
    click.echo(f'\r{len(tags_done)}/{tags_total}', nl=False)  # so there is some way to monitor the progress of the


@backoff.on_exception(
    backoff.expo,
    (TransportError, ReadTimeoutError, ConnectionError, NewConnectionError, ConnectionRefusedError),
    max_time=60 * 60,  # an hour
    on_giveup=giveup
)
def save_object(obj):
    click.echo(f'\r{obj.guid}', nl=False)
    obj.save()


@click.command()
@click.option('--dry_run', default=True, type=bool, prompt='Dry run?')
def run_migration(dry_run):
    click.echo('Migrating the tags to a new table and updating...')
    for old_tag in OldTag.scan_all():
        migrate_tag(old_tag)
    click.echo(' DONE!')

    if not dry_run:
        click.echo('Saving products and tags...', nl=False)
        with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
            executor.map(save_object, tags_to_save.values())

        with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
            executor.map(save_object, products_to_save.values())
        click.echo(' DONE!')
    click.echo(f'There were {len(duplicate_non_matching_tag_guids)} tags that were duplicates, but had differences in data. Below the new guids')
    click.echo(duplicate_non_matching_tag_guids)


if __name__ == '__main__':
    run_migration()
