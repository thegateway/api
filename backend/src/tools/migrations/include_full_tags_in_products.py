import concurrent
from collections import defaultdict

import backoff
import click
from dynamorm import DynaModel
from elasticsearch import (
    ConnectionError,
    TransportError,
)
from marshmallow import fields
from urllib3.exceptions import (
    NewConnectionError,
    ReadTimeoutError,
)

from gwio.ext.dynamorm import (
    HotfixMixIn,
    adapted_dynamodel,
)
from gwio.models.products import Product
from gwio.models.tags import Tag


@adapted_dynamodel
class ProductTags(HotfixMixIn, DynaModel):
    class Table:
        name = 'tags-products'

        hash_key = 'tag_guid'
        range_key = 'product_guid'

    class Schemas:
        class BaseSchema:
            tag_guid = fields.UUID()
            product_guid = fields.UUID()

    Schema = Schemas.BaseSchema


def giveup(details):
    print(details)
    exit(1)


tag_product_guids = defaultdict(list)
product_tags = defaultdict(list)

pts = ProductTags.scan_all()
tags = Tag.scan_all()
products = Product.scan_all()


@backoff.on_exception(
    backoff.expo,
    (TransportError, ReadTimeoutError, ConnectionError, NewConnectionError, ConnectionRefusedError),
    max_time=60 * 60,  # an hour
    on_giveup=giveup
)
def update_product(product_guid):
    click.echo(f'\r{product_guid}', nl=False)  # Just to provide a way to see something is happening when running the script
    try:
        product = next(x for x in products if product_guid == x.guid)
    except StopIteration:
        click.echo(f"\rProduct `{product_guid}` doesn't exist, so skipping to the next one")
        return
    product.tags = product_tags[product_guid]
    product.save()


@backoff.on_exception(
    backoff.expo,
    (TransportError, ReadTimeoutError, ConnectionError, NewConnectionError, ConnectionRefusedError),
    max_time=60 * 60,  # an hour
    on_giveup=giveup
)
def update_tag(tag_guid):
    click.echo(f'\r{tag_guid}', nl=False)  # Just to provide a way to see something is happening when running the script
    try:
        tag = next(x for x in tags if tag_guid == x.guid)
    except StopIteration:
        click.echo(f"\rTag `{tag_guid}` doesn't exist, so skipping to the next one")
        return
    tag.product_guids = tag_product_guids[tag_guid]
    tag.save()


@click.command()
def run_migration():
    click.echo('Collecting ProductTag information...')
    i = 1
    for pt in pts:
        click.echo(f'\r{i}', nl=False)
        i += 1
        try:
            tag = next(x for x in tags if pt.tag_guid == x.guid)
        except StopIteration:
            click.echo(f' Tag `{pt.tag_guid}` does not exist. Set for product `{pt.product_guid}`')
            continue
        product_tags[pt.product_guid].append(tag)
        tag_product_guids[pt.tag_guid].append(pt.product_guid)
    click.echo('\nProductTags looping done!')

    click.echo('Updating products...')
    with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
        executor.map(update_product, product_tags)
    click.echo('\rDONE!')

    click.echo('Updating tags...')
    with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
        executor.map(update_tag, tag_product_guids)
    click.echo('\rDONE!')


if __name__ == '__main__':
    run_migration()
