import click

from gwio.models.bootstrap import (
    Bootstrap,
    BootstrapTag,
)
from gwio.models.tags import Tag

category_hierarchy = [
    {
        "old_guid": "5a574bb2-8e50-53ea-83cd-99a31cdd64ab",
        "name": "KOBIETA",
        "children": [
            {
                "old_guids": [
                    "7094f497-ee65-5671-bb05-eba0f2335345",
                    "a95659d2-4db6-596f-8500-27c43fe45714",
                    "70471aef-52a1-523a-8f4f-23a6575df6ee",
                    "2005ead4-43ef-5e81-a644-b979602c51f2"
                ],
                "name": "ODZIEŻ",
                "children": [
                    {
                        "old_guid": "760a10de-6200-5220-9949-daf3e2c21fae",
                        "name": "SUKIENKI"
                    },
                    {
                        "old_guid": "a5125fa2-260a-59e3-9ad1-5f08fb01bee3",
                        "name": "KOSZULE"
                    },
                    {
                        "old_guid": "138a2e28-f734-5980-8e57-69103b09517d",
                        "name": "T SHIRT I TOPY"
                    },
                    {
                        "old_guids": [
                            "996e0f00-0d0b-51d7-8a1c-ff5ee23e313d",
                            "3511d9b8-01c1-5085-9229-f7f891a2cbce"
                        ],
                        "name": "SWETRY I BLUZY"
                    },
                    {
                        "old_guid": "0782b2d1-486f-59b3-b4bb-f17462acd139",
                        "name": "ODZIEŻ WIERZCHNIA"
                    },
                    {
                        "old_guids": [
                            "fabcb1e6-926a-5f7e-b0a2-f939c624e67c",
                            "ea400bd9-364a-57b7-a230-d25f03fbd1c3"
                        ],
                        "name": "MARYNARKI"
                    },
                    {
                        "old_guids": [
                            "bfd92300-bd97-53bc-a6cb-2eca17e228c0",
                            "aef53672-3c45-5b90-9a04-1d2549bf1751"
                        ],
                        "name": "JEANSY"
                    },
                    {
                        "old_guids": [
                            "f5139547-9ead-548c-a47f-05e862537828",
                            "7521808e-37ca-5ed5-bd9c-f6d55fc93b94"
                        ],
                        "name": "SPODNIE I SPODENKI"
                    },
                    {
                        "name": "SPÓDNICE"
                    },
                    {
                        "old_guid": "fda012e1-9ee3-5fec-a952-f76d0fd91645",
                        "name": "KOMBINEZONY"
                    },
                    {
                        "old_guid": "00bea84b-5b44-5713-9e8f-abd475e1ad0c",
                        "name": "BIELIZNA I PIŻAMY"
                    },
                    {
                        "old_guids": [
                            "38b05a8a-f7f1-540b-91e5-61cf926b6f72",
                            "3c1b4a36-1216-5ecc-bb15-b6830824cc20"
                        ],
                        "name": "ODZIEŻ SPORTOWA"
                    }
                ]
            },
            {
                "old_guids": [
                    "b7a4a688-5aec-5070-be46-5b27e6b3e15b",
                    "525e5154-32c6-557b-9f62-239b7062dcee",
                    "0fd08bc1-0674-51ad-9cd2-75d388ac1ed6",
                    "37905db4-bfcd-53b7-be75-afa87c9dd5c0"
                ],
                "name": "OBUWIE",
                "children": [
                    {
                        "old_guids": [
                            "8297b0d0-6064-5942-a667-4946ff61c56e",
                            "1d54e2c1-33b5-5b10-8ed7-a75703edde69",
                            "65e7114c-f844-531a-8e9e-2cd9864e4dfa"
                        ],
                        "name": "OBUWIE SPORTOWE"
                    },
                    {
                        "old_guid": "1ca91465-0b07-5991-8599-d6637859c18f",
                        "name": "OBUWIE NA PŁASKIEJ PODESZWIE"
                    },
                    {
                        "name": "OBUWIE NA OBCASIE"
                    },
                    {
                        "name": "SANDAŁY I KLAPKI"
                    },
                    {
                        "old_guid": "f23cd177-3cf7-5360-a330-590d5fb75189",
                        "name": "KOZAKI"
                    },
                    {
                        "old_guids": [
                            "a9926247-75ec-54ba-9cf4-702e2c849f84",
                            "4a55e8e5-261b-5249-b819-fa3cbe176471",
                            "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                            "45d81b76-1e19-5acd-b719-fe62ae102bff"
                        ],
                        "name": "AKCESORIA"
                    }
                ]
            },
            {
                "old_guids": [
                    "a9926247-75ec-54ba-9cf4-702e2c849f84",
                    "4a55e8e5-261b-5249-b819-fa3cbe176471",
                    "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                    "45d81b76-1e19-5acd-b719-fe62ae102bff"
                ],
                "name": "AKCESORIA",
                "children": [
                    {
                        "name": "TOREBKI"
                    },
                    {
                        "old_guids": [
                            "aebb90b6-e810-536e-aa46-3c9f620f6b91",
                            "c1fb2431-3760-5029-b0d3-26837fe50c77"
                        ],
                        "name": "BIŻUTERIA"
                    },
                    {
                        "old_guids": [
                            "2f7a6cef-2c87-587d-a83c-bde1c46cab7f",
                            "45791157-cf6a-5039-9448-a8217add7754"
                        ],
                        "name": "ZEGARKI"
                    },
                    {
                        "old_guids": [
                            "d1b7b077-18eb-52ab-aa9d-a4b80b69d17b",
                            "13ae1f85-cc01-5374-a4d6-f4798c136c3b"
                        ],
                        "name": "OKULARY"
                    },
                    {
                        "name": "PORTFELE I ETUI"
                    },
                    {
                        "old_guid": "402a2fb9-201c-5117-8887-ecc4ad9d3328",
                        "name": "PASKI"
                    },
                    {
                        "name": "SZALIKI I CHUSTY"
                    },
                    {
                        "old_guid": "3e036443-15de-54b3-b58d-cbab67cc6e78",
                        "name": "CZAPKI I KAPELUSZE"
                    },
                    {
                        "old_guid": "8304aeb3-c427-5109-b476-2abe1377274f",
                        "name": "RĘKAWICZKI"
                    },
                    {
                        "old_guids": [
                            "9a6a1600-808f-5902-9308-1be512d90a78",
                            "5376e071-e9c6-53f0-b002-8a0b7dd854cd"
                        ],
                        "name": "INNE"
                    }
                ]
            }
        ]
    },
    {
        "old_guid": "2006cb37-cce1-5493-b938-07da14da6345",
        "name": "MĘŻCZYZNA",
        "children": [
            {
                "old_guids": [
                    "7094f497-ee65-5671-bb05-eba0f2335345",
                    "a95659d2-4db6-596f-8500-27c43fe45714",
                    "70471aef-52a1-523a-8f4f-23a6575df6ee",
                    "2005ead4-43ef-5e81-a644-b979602c51f2"
                ],
                "name": "ODZIEŻ",
                "children": [
                    {
                        "name": "TSHIRT I KOSZULKI"
                    },
                    {
                        "old_guid": "a5125fa2-260a-59e3-9ad1-5f08fb01bee3",
                        "name": "KOSZULE"
                    },
                    {
                        "old_guids": [
                            "996e0f00-0d0b-51d7-8a1c-ff5ee23e313d",
                            "3511d9b8-01c1-5085-9229-f7f891a2cbce"
                        ],
                        "name": "SWETRY I BLUZY"
                    },
                    {
                        "old_guids": [
                            "fabcb1e6-926a-5f7e-b0a2-f939c624e67c",
                            "ea400bd9-364a-57b7-a230-d25f03fbd1c3"
                        ],
                        "name": "MARYNARKI"
                    },
                    {
                        "old_guids": [
                            "f5139547-9ead-548c-a47f-05e862537828",
                            "7521808e-37ca-5ed5-bd9c-f6d55fc93b94"
                        ],
                        "name": "SPODNIE I SPODENKI"
                    },
                    {
                        "old_guids": [
                            "bfd92300-bd97-53bc-a6cb-2eca17e228c0",
                            "aef53672-3c45-5b90-9a04-1d2549bf1751"
                        ],
                        "name": "JEANSY"
                    },
                    {
                        "old_guid": "0782b2d1-486f-59b3-b4bb-f17462acd139",
                        "name": "ODZIEŻ WIERZCHNIA"
                    },
                    {
                        "name": "GARNITURY"
                    },
                    {
                        "old_guid": "daca2c94-fc2c-5230-863b-5289e3656747",
                        "name": "BIELIZNA"
                    },
                    {
                        "old_guids": [
                            "38b05a8a-f7f1-540b-91e5-61cf926b6f72",
                            "3c1b4a36-1216-5ecc-bb15-b6830824cc20"
                        ],
                        "name": "ODZIEŻ SPORTOWA"
                    }
                ]
            },
            {
                "old_guids": [
                    "b7a4a688-5aec-5070-be46-5b27e6b3e15b",
                    "525e5154-32c6-557b-9f62-239b7062dcee",
                    "0fd08bc1-0674-51ad-9cd2-75d388ac1ed6",
                    "37905db4-bfcd-53b7-be75-afa87c9dd5c0"
                ],
                "name": "OBUWIE",
                "children": [
                    {
                        "old_guids": [
                            "8297b0d0-6064-5942-a667-4946ff61c56e",
                            "1d54e2c1-33b5-5b10-8ed7-a75703edde69",
                            "65e7114c-f844-531a-8e9e-2cd9864e4dfa"
                        ],
                        "name": "OBUWIE SPORTOWE"
                    },
                    {
                        "old_guid": "dd63d994-41ed-554b-a2e8-ab01828f110d",
                        "name": "OBUWIE WIZYTOWE"
                    },
                    {
                        "old_guid": "8acf222e-3fb2-5dee-be09-3e57bd27cc0c",
                        "name": "OBUWIE CASUAL"
                    },
                    {
                        "name": "SANDAŁY I BOTKI"
                    },
                    {
                        "old_guids": [
                            "a9926247-75ec-54ba-9cf4-702e2c849f84",
                            "4a55e8e5-261b-5249-b819-fa3cbe176471",
                            "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                            "45d81b76-1e19-5acd-b719-fe62ae102bff"
                        ],
                        "name": "AKCESORIA"
                    }
                ]
            },
            {
                "old_guids": [
                    "a9926247-75ec-54ba-9cf4-702e2c849f84",
                    "4a55e8e5-261b-5249-b819-fa3cbe176471",
                    "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                    "45d81b76-1e19-5acd-b719-fe62ae102bff"
                ],
                "name": "AKCESORIA",
                "children": [
                    {
                        "old_guid": "4474e975-816d-5aca-b790-f18447547af4",
                        "name": "TORBY I PLECAKI"
                    },
                    {
                        "old_guids": [
                            "aebb90b6-e810-536e-aa46-3c9f620f6b91",
                            "c1fb2431-3760-5029-b0d3-26837fe50c77"
                        ],
                        "name": "BIŻUTERIA"
                    },
                    {
                        "old_guids": [
                            "2f7a6cef-2c87-587d-a83c-bde1c46cab7f",
                            "45791157-cf6a-5039-9448-a8217add7754"
                        ],
                        "name": "ZEGARKI"
                    },
                    {
                        "old_guids": [
                            "d1b7b077-18eb-52ab-aa9d-a4b80b69d17b",
                            "13ae1f85-cc01-5374-a4d6-f4798c136c3b"
                        ],
                        "name": "OKULARY"
                    },
                    {
                        "old_guids": [
                            "00eb51c1-b09d-5836-bb39-0fe15813d973",
                            "0f3b04cf-0d4d-5f67-9a7b-eee8bb0a98cd"
                        ],
                        "name": "PORTFELE I ETUI"
                    },
                    {
                        "old_guid": "402a2fb9-201c-5117-8887-ecc4ad9d3328",
                        "name": "PASKI"
                    },
                    {
                        "name": "SZALIKI/CZAPKI/RĘKAWICZKI"
                    },
                    {
                        "old_guid": "750251ee-697d-5671-addd-8dc11bc36d8c",
                        "name": "AKCESORIA GARNITUROWE"
                    }
                ]
            }
        ]
    },
    {
        "old_guid": "611d5585-7642-5fb1-ad57-e8fff565d5af",
        "name": "DZIECKO",
        "children": [
            {
                "old_guids": [
                    "7094f497-ee65-5671-bb05-eba0f2335345",
                    "a95659d2-4db6-596f-8500-27c43fe45714",
                    "70471aef-52a1-523a-8f4f-23a6575df6ee",
                    "2005ead4-43ef-5e81-a644-b979602c51f2"
                ],
                "name": "ODZIEŻ",
                "children": [
                    {
                        "old_guid": "2c938565-ec5e-5be5-827c-a799f5c74fc2",
                        "name": "0-2 LAT"
                    },
                    {
                        "old_guid": "00b8fe4f-efe5-54c7-88e7-3c6b1a17ca97",
                        "name": "2-9 LAT"
                    },
                    {
                        "name": "9-16 LAT"
                    }
                ]
            },
            {
                "old_guids": [
                    "b7a4a688-5aec-5070-be46-5b27e6b3e15b",
                    "525e5154-32c6-557b-9f62-239b7062dcee",
                    "0fd08bc1-0674-51ad-9cd2-75d388ac1ed6",
                    "37905db4-bfcd-53b7-be75-afa87c9dd5c0"
                ],
                "name": "OBUWIE",
                "children": [
                    {
                        "old_guids": [
                            "8297b0d0-6064-5942-a667-4946ff61c56e",
                            "1d54e2c1-33b5-5b10-8ed7-a75703edde69",
                            "65e7114c-f844-531a-8e9e-2cd9864e4dfa"
                        ],
                        "name": "OBUWIE SPORTOWE"
                    },
                    {
                        "old_guid": "b5b5c468-1dbe-57aa-a96f-67ab180a74b3",
                        "name": "OBUWIE CODZIENNE"
                    },
                    {
                        "old_guid": "5a4927e0-f4c3-5d9e-948b-daac0ea2388e",
                        "name": "OBUWIE LETNIE"
                    },
                    {
                        "name": "OBUWIE ZIMOWE"
                    }
                ]
            },
            {
                "old_guids": [
                    "a9926247-75ec-54ba-9cf4-702e2c849f84",
                    "4a55e8e5-261b-5249-b819-fa3cbe176471",
                    "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                    "45d81b76-1e19-5acd-b719-fe62ae102bff"
                ],
                "name": "AKCESORIA"
            },
            {
                "old_guids": [
                    "4bf38d45-dc21-5341-9df4-8a6cd2d31139",
                    "1e5b0f90-c632-578a-9cd5-954740635177"
                ],
                "name": "ZABAWKI"
            }
        ]
    },
    {
        "name": "DOM i OGRÓD",
        "children": [
            {
                "old_guid": "b5a5a95c-b28d-516b-93e8-7f07f62c19c1",
                "name": "SALON"
            },
            {
                "old_guid": "e55e6e8b-aaff-5437-8ccf-6010073fdbcf",
                "name": "KUCHNIA I JADALNIA"
            },
            {
                "old_guid": "5bb09a1e-b39e-506e-a889-447d794f9441",
                "name": "SYPIALNIA"
            },
            {
                "old_guid": "29fcf83a-5aba-553b-a675-1302eaa2f6e3",
                "name": "ŁAZIENKA"
            },
            {
                "old_guid": "ceaa8e36-7bd6-5540-a1d8-1b9eb39c8239",
                "name": "POKÓJ DZIECKA"
            },
            {
                "name": "MEBLE"
            },
            {
                "name": "OGRÓD"
            },
            {
                "old_guids": [
                    "9a6a1600-808f-5902-9308-1be512d90a78",
                    "5376e071-e9c6-53f0-b002-8a0b7dd854cd"
                ],
                "name": "INNE"
            }
        ]
    },
    {
        "old_guid": "4f238d46-5a3f-5f33-9c5b-c24e357d5a5b",
        "name": "MULTIMEDIA",
        "children": [
            {
                "old_guid": "80cda102-7e9d-5b2f-ac31-773f3e914a0b",
                "name": "RTV"
            },
            {
                "old_guid": "c03fee1c-b4fa-5c55-b883-dde5b67288ec",
                "name": "AGD"
            },
            {
                "name": "SMARTFONY"
            },
            {
                "name": "LOPTOPY I NOTEBOOKI"
            },
            {
                "old_guids": [
                    "a9926247-75ec-54ba-9cf4-702e2c849f84",
                    "4a55e8e5-261b-5249-b819-fa3cbe176471",
                    "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                    "45d81b76-1e19-5acd-b719-fe62ae102bff"
                ],
                "name": "AKCESORIA"
            },
            {
                "old_guids": [
                    "9a6a1600-808f-5902-9308-1be512d90a78",
                    "5376e071-e9c6-53f0-b002-8a0b7dd854cd"
                ],
                "name": "INNE"
            },
        ]
    },
    {
        "old_guids": [
            "924a1f4e-d7e9-5aa2-9a2c-35f2ba656e3f",
            "30350771-20d0-5b82-a9dd-d24ce0ae1d4d"
        ],
        "name": "ZDROWIE I URODA",
        "children": [
            {
                "old_guid": "39e88e4e-1c84-5a14-9e90-425a0156abad",
                "name": "PIELĘGNACJA",
                "children": [
                    {
                        "old_guid": "47243840-763d-50e8-974c-7f0a9299c301",
                        "name": "CIAŁO"
                    },
                    {
                        "old_guid": "e702796c-3ec3-5972-a841-bee814753155",
                        "name": "TWARZ"
                    },
                    {
                        "old_guid": "7025127b-8508-51f2-b159-f950a20488a0",
                        "name": "DŁONIE I STOPY"
                    },
                    {
                        "name": "PIELĘGNACJA DZIECI"
                    }
                ]
            },
            {
                "old_guid": "d3181402-5635-5b56-bfca-fc8352aea20f",
                "name": "MAKIJAŻ"
            },
            {
                "old_guid": "4c24d7d1-02ab-5c6b-9e24-0606a04caffd",
                "name": "PERFUMY"
            },
            {
                "name": "KOREKCJA WZROKU"
            }
        ]
    },
    {
        "old_guid": "4123314c-6ecc-5e78-9e10-9a69712a8add",
        "name": "INNE ARTYKUŁY",
        "children": [
            {
                "old_guid": "5fff635a-ba91-5510-9bc7-9e73f877a7d1",
                "name": "ART. PRZEMYSŁOWE"
            },
            {
                "name": "ART. DLA ZWIERZĄT"
            },
            {
                "name": "UPOMINKI"
            },
            {
                "name": "PRASA I KSIĄŻKI"
            },
            {
                "name": "ART. PAPIERNICZO BIUROWE"
            }
        ]
    },
    {
        "name": "ART. SPOŻYWCZE",
        "children": [
            {
                "old_guid": "5cc3e64e-14c4-501d-bef2-96562a9989e4",
                "name": "SPOŻYWCZE"
            },
            {
                "old_guid": "d0defd1b-5588-506c-8eaa-482c80544322",
                "name": "ZDROWA ŻYWNOŚĆ"
            },
        ]
    },
    {
        "old_guid": "097cc645-61ab-5742-9d4b-22d29e032359",
        "name": "SPORT",
        "children": [
            {
                "old_guids": [
                    "7094f497-ee65-5671-bb05-eba0f2335345"
                    "a95659d2-4db6-596f-8500-27c43fe45714",
                    "70471aef-52a1-523a-8f4f-23a6575df6ee",
                    "2005ead4-43ef-5e81-a644-b979602c51f2"
                ],
                "name": "ODZIEŻ",
                "children": [
                    {
                        "name": "DAMSKIE"
                    },
                    {
                        "name": "MĘSKIE"
                    },
                    {
                        "name": "DZIECKO"
                    }
                ]
            },
            {
                "old_guids": [
                    "b7a4a688-5aec-5070-be46-5b27e6b3e15b",
                    "525e5154-32c6-557b-9f62-239b7062dcee",
                    "0fd08bc1-0674-51ad-9cd2-75d388ac1ed6",
                    "37905db4-bfcd-53b7-be75-afa87c9dd5c0"
                ],
                "name": "OBUWIE",
                "children": [
                    {
                        "name": "DAMSKIE"
                    },
                    {
                        "name": "MĘSKIE"
                    },
                    {
                        "name": "DZIECKO"
                    }
                ]
            },
            {
                "old_guids": [
                    "a9926247-75ec-54ba-9cf4-702e2c849f84",
                    "4a55e8e5-261b-5249-b819-fa3cbe176471",
                    "fb73718e-636f-55f4-a94f-d34a50c9bb54",
                    "45d81b76-1e19-5acd-b719-fe62ae102bff"
                ],
                "name": "AKCESORIA"
            },
            {
                "old_guid": "1d432400-7170-53e0-be2d-3f6b6deaf1ca",
                "name": "ODŻYWKI I SUPLEMENTY"
            },
            {
                "old_guid": "ba1992ed-e23f-5502-8bb5-ba262240ebd7",
                "name": "SPRZĘT"
            }
        ]
    }
]


def create_tag_tree(tag_data):
    tag = Tag(name=tag_data['name'])
    try:
        Tag.by_guid(tag.guid)
    except Tag.DoesNotExist:
        tag.save()

    bt = BootstrapTag(guid=tag.guid, name=tag.name)
    try:
        children = tag_data['children']
    except KeyError:
        children = []

    for child in children:
        child_bt = create_tag_tree(child)
        bt.children.append(child_bt)
    return bt


@click.command()
def run_migration():
    bs = Bootstrap.get(vertical='sma')
    bs.tag_hierarchy = dict()
    for tag_data in category_hierarchy:
        tag_in_hierarchy = create_tag_tree(tag_data)
        bs.tag_hierarchy[tag_in_hierarchy.guid] = tag_in_hierarchy
    bs.save()


if __name__ == '__main__':
    run_migration()
