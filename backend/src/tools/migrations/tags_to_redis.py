import boto3
from boto3.dynamodb.types import TypeDeserializer

from gwio.environment import Env
from gwio.models.tags import Tag

dynamodb = boto3.client('dynamodb')
deser = TypeDeserializer()


def migrate_tags(start_key):
    scan_kwargs = dict(
        TableName=f'sma-{Env.APPLICATION_ENVIRONMENT}-tags',
    )
    if start_key is not None:
        scan_kwargs['ExclusiveStartKey'] = start_key

    scan_result = dynamodb.scan(**scan_kwargs)
    for dynamodb_tag in scan_result['Items']:
        python_native_tag = {k: deser.deserialize(v) for k, v in dynamodb_tag.items()}
        try:
            Tag.by_guid(guid=python_native_tag['guid'])
            continue  # Tag already migrated so skipping
        except Tag.DoesNotExist:
            pass
        tag = Tag(**python_native_tag)
        assert python_native_tag['guid'] == str(tag.guid)  # Just making sure guid doesn't change
        tag.save()
        print(f'\r{tag.guid}', end='')

    try:
        return scan_result['LastEvaluatedKey']
    except KeyError:
        return None


start_key = None
while True:
    start_key = migrate_tags(start_key)
    if start_key is None:
        break
