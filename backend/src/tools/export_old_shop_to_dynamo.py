import datetime
import uuid
import requests

import click
from pprint import pprint

from gwio.models.webshops import Webshop


def _get_shops_from_old(url: str = 'https://v2018-5-dev.api.imeds.fi/shops/', output: bool = False):
    resp = requests.get(url)
    data = resp.json()
    if output:
        pprint(data)
    return data


def _import_shop(shop_data: dict):
    """Import a single shop's data via PynamoDB Webshop model."""
    # TODO: alert, private_data, promoted_products and vat_code
    social_media = [{'label': x['label'],
                     'link': x['value']} for x in shop_data['social_media']]
    if shop_data['facebook_analytics']:
        social_media.append({'label': 'facebook',
                             'analytics': shop_data['facebook_analytics']})
    if shop_data['google_analytics']:
        social_media.append({'label': 'google',
                             'analytics': shop_data['google_analytics']})
    opening_hours_normal = [{'day': x['day'],
                             'start_at': datetime.time(int(x['start_at'].split(':')[0]), int(x['start_at'].split(':')[1])),
                             'end_at': datetime.time(int(x['end_at'].split(':')[0]), int(x['end_at'].split(':')[1]))} for x in
                            shop_data['normal_opening_hours']]
    opening_hours_exceptions = [{'date': x['date'],
                                 'start_at': datetime.time(int(x['start_at'].split(':')[0]), int(x['start_at'].split(':')[1])),
                                 'end_at': datetime.time(int(x['end_at'].split(':')[0]), int(x['end_at'].split(':')[1])),
                                 'desc': '-'} for x in shop_data['exceptional_opening_hours']]
    ws = Webshop(uuid.UUID(shop_data['id']),
                 alert={},
                 vat_code='-',
                 name=shop_data['name'],
                 logo=shop_data['logo'],
                 addresses=[{'label': x['label'],
                             'name': x['name'],
                             'street': x['street_address'],
                             'city': x['postal_area'],
                             'zipcode': x['postal_code'],
                             'country': x['country']} for x in shop_data['addresses']],
                 phones=[{'label': x['label'],
                          'phone': x['value']} for x in shop_data['phones']],
                 location=shop_data.get('location', []),
                 banners={'main': '',
                          'side': [{'title': x['title'],
                                    'image': x['img_url'],
                                    'link': x['url'],
                                    'text': x['description']} for x in shop_data['banners'] if x['type'] != 'top'],
                          'top': [{'title': x['title'],
                                   'image': x['img_url'],
                                   'link': x['url'],
                                   'text': x['description']} for x in shop_data['banners'] if x['type'] == 'top']},
                 brand_color=shop_data['brand_color'],
                 emails=[{'label': x['label'],
                          'email': x['value']} for x in shop_data['emails']],
                 enabled=True if shop_data.get('enabled') else False,
                 timezone='Europe/Helsinki',
                 person_in_charge=shop_data['person_in_charge'],
                 social_media=social_media,
                 public_data={'stripe': shop_data['stripe_key']},
                 websites=[x['value'] for x in shop_data['websites']],
                 opening_hours_normal=opening_hours_normal,
                 opening_hours_exceptions=opening_hours_exceptions)
    ws.save()


@click.group()
def run_click():
    pass


@run_click.command()
def create_webshop_table():
    """Creates the Webshop table if one does not exist.\n
    Environment value to take note:\n
    - APPLICATION_ENVIRONMENT\n
    - GW_VERTICAL_NAME\n
    - DYNAMODB_HOST
    """
    if not Webshop.exists():
        Webshop.create_table()
    pprint(Webshop.describe_table())


@run_click.command()
@click.option('--url', default='https://v2018-5-dev.api.imeds.fi/shops/', help='URL to get shops from.')
@click.option('--output', is_flag=True, help='Output result to console.')
def get_shops_from_old(url: str, output: bool):
    """Gets all the shops from the given URL."""
    _get_shops_from_old(url=url, output=output)


@run_click.command()
@click.option('--url', default='https://v2018-5-dev.api.imeds.fi/shops/', help='URL to get shops from.')
def save_old_shops_to_dynamodb(url: str):
    """Export web shop data from old place to the new dynamodb."""
    summary = {'success': [],
               'fail': []}
    for shop in _get_shops_from_old(url=url):
        try:
            _import_shop(shop_data=shop)
            summary['success'].append(f'{shop["name"]} ({shop["id"]})')
        except Exception as e:
            pprint(e)
            summary['fail'].append(f'{shop["name"]} ({shop["id"]})')
    pprint(summary)


if __name__ == '__main__':
    run_click()
