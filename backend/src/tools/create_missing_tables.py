# coding=utf-8
import logging
import boto3
import os
from gwio.ext.dynamorm import all_models
from gwio.models import *
try:
    from gwio.remppa.models import *
except ModuleNotFoundError:
    pass
logger = logging.getLogger(__name__)


def setup_local_dynamodb(DYNAMODB_HOST):
    try:
        for model in all_models:
            model.Table.get_resource(
                aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
                aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY'),
                region_name=os.environ.get('AWS_REGION'),
                endpoint_url=DYNAMODB_HOST
            )
    except AttributeError:
        pass


def main():
    setup_local_dynamodb(os.environ.get('DYNAMODB_HOST'))
    vertical_specific_models = [model for model in all_models if os.environ.get('GW_VERTICAL_NAME') in model.Table.name]

    client = boto3.client('dynamodb')
    for model in vertical_specific_models:
        table_name = model.Table.name
        if not model.exists():
            print(f"Creating table {table_name}")
            model.Table.create(wait=True)
        else:
            logger.info(f'Skipping {table_name}, already exists.')
        logger.info(f'Changing billing mode of {table_name} to PAY_PER_REQUEST.')
        try:
            logger.debug(client.update_table(TableName=table_name, BillingMode='PAY_PER_REQUEST'))
        except Exception as e:
            logger.exception(e)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
