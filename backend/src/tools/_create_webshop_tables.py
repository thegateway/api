import logging

import sqlalchemy
from envs import env
from gwio.core.models.erp import Actor
from sqlalchemy.orm import sessionmaker

from gwio.api.shops import WebShopContact
from gwio.models.webshops import WebShop, WebShopBanner, WebShopPaymentMethod, WebShopHasPaymentMethod, shop_has_banner

logger = logging.getLogger(__name__)
SQLALCHEMY_DATABASE_URI = env('SQLALCHEMY_DATABASE_URI')
engine = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)

BANNERS = {
    "10002": {
        "top": [
            {
                "uuid": "8ac784f9-63de-51b3-8bd8-000a4ac84005",
                "title": "Ympyrätalon apteekki",
                "description": "Uuden ajan apteekki",
                "url": "/prescription",
                "image_url": "img/yt-splash1.jpg"
            },
            {
                "uuid": "216331b8-de40-52be-bdbb-ed9ab820ce65",
                "title": "Tehokkaat",
                "description": "Ihonhoidot",
                "rl": "http://static.imeds.fi/upload/156/UUSI%20YMP_%20TALO%20HINNASTO%21.pdf",
                "image_url": "img/yt-splash3.jpg"
            },
            {
                "uuid": "59457d32-f96b-5ee9-85d7-6b836ae737fa",
                "title": "Terveyttä edistävät",
                "description": "Jalkojen hoidot",
                "url": "http://static.imeds.fi/upload/156/UUSI%20YMP_%20TALO%20HINNASTO%21.pdf",
                "image_url": "img/yt-splash2.jpg"
            },
            {
                "image_url": "img/Cafe_tehden.jpg"
            }
        ],
        "brandColor": "#b9f6ca",
        "hasExpress": True
    },
    "10003": {
        "fbTrackingCode": "552475758418682",
        "ga": "UA-109900447-1",
        "hasExpress": True
    },
    "10004": {
        "brandColor": "#CCFF90",
        "hasExpress": True
    },
    "10005": {
        "brandColor": "#4DB6AC",
        "hasExpress": True
    },
    "10006": {
        "brandColor": "#4CAF50"
    },
    "10007": {
        "top": [
            {
                "uuid": "216331b8-de40-52be-bdbb-ed9ab820ce65",
                "title": "Video",
                "description": "Video",
                "url": "/topProducts",
                "video_url": "videos/ivalon_apteekki.mp4"
            }
        ],
        "brandColor": "#e040fb"
    },
    "10037": {
        "brandColor": "#FF6C0C"
    },
    "82708": {
        "brandColor": "#FF80AB"
    },
    "86172": {
        "brandColor": "#00E676"
    },
    "85416": {
        "brandColor": "#1a9d48"
    },
    "default": {
        "top": [
            {
                "uuid": "59457d32-f96b-5ee9-85d7-6b836ae737fa",
                "title": "Saatavilla kaikki",
                "description": "Itsehoitolääkkeet",
                "url": "/medicines",
                "image_url": "img/new_frontend_splasher2.jpg"
            },
            {
                "uuid": "8ac784f9-63de-51b3-8bd8-000a4ac84005",
                "title": "Voit myös tilata suoraan kotiin",
                "description": "Reseptilääkkeet",
                "url": "/prescription",
                "image_url": "img/new_frontend_splasher1.jpg",
                "button": "white"
            }
        ],
        "sub": [
            {
                "uuid": "ba9a9b09-390f-521a-9c10-3f3e06f220ba",
                "title": "Katso TV-mainokset",
                "description": "Tästä",
                "url": "https://www.youtube.com/channel/UC8QHGq3ortTI6uoq_RvKcXw",
                "image_url": "img/side_splasher2.jpg"
            },
            {
                "uuid": "498d6931-7dda-512c-86fb-56bd51dbae8e",
                "title": "Lataa",
                "description": "Apteekki-sovellus",
                "url": "https://hae.apteekkisovell.us",
                "image_url": "img/side_splasher1.jpg"
            }
        ],
        "brandColor": "#fad100",
        "apiUrl": "https://api.imeds.fi/",
        "ga": "UA-92531722-4",
        "stripe": "pk_live_XIFVhmqZpsvoN26MuLnvf1Ri"
    },
    "dev": {
        "apiUrl": "http://127.0.0.1:5000/",
        "stripe": "pk_test_5bj2Qb5SzTsw8R8rG3prps3p"
    },
    "1": {
        "stripe": "pk_test_5bj2Qb5SzTsw8R8rG3prps3p"
    }
}
VPK_BANNERS = {
    "default": {
        "top": [
            {
                "uuid": "216331b8-de40-52be-bdbb-ed9ab820ce65",
                "title": "Esittelyssä",
                "description": "UUSINTA KALUSTOA",
                "url": "/categorie/465",
                "image_url": "img/vpk/paloauto.jpg"
            },
            {
                "uuid": "8ac784f9-63de-51b3-8bd8-000a4ac84005",
                "title": "Turvallisuusviestintää",
                "description": "VERKOSSA",
                "url": "/staticpage/8",
                "image_url": "img/vpk/Ambulanssi.jpg"
            }
        ],
        "sub": [
            {
                "uuid": "ba9a9b09-390f-521a-9c10-3f3e06f220ba",
                "title": "",
                "description": "",
                "url": "http://www.nouhata.fi/",
                "image_url": "img/vpk/NOUHATA_logo.jpg"
            },
            {
                "uuid": "498d6931-7dda-512c-86fb-56bd51dbae8e",
                "title": "",
                "description": "",
                "url": "http://www.paloturvallisuusviikko.fi",
                "image_url": "img/vpk/Paloturvallisuusviikko.jpg"
            }
        ],
        "brandColor": "#ff6600",
        "apiUrl": "https://api.vpkgateway.fi/",
        "ga": "UA-92531722-4",
        "stripe": "pk_live_aPQ7dKjKtu9HleJ8MEE98eXQ"
    },
    "dev": {
        "apiUrl": "https://api.vpkgateway.fi/",
        "stripe": "pk_test_6dr0D30JtagGzUW9GiRT7zzT"
    },
    "vpk": {
        "apiUrl": "https://api.vpkgateway.fi/",
        "stripe": "pk_test_6dr0D30JtagGzUW9GiRT7zzT"
    }
}

VPK_BANNERS['85262'] = VPK_BANNERS['default'].copy()
def create_banners(defaults):
    pass


def create_delivery_options(shop, has_express, has_pickup_box):
    pass


def create_shops():
    session = Session()
    defaults = VPK_BANNERS.pop('default')
    for actor_id, data in VPK_BANNERS.items():
        try:
            actor_id = int(actor_id)
        except ValueError:
            logger.error('Non-numeric actor_id: {}'.format(actor_id))
            continue
        logger.info(actor_id)

        actor = session.query(Actor).get(actor_id)
        shop = WebShop(
            actor_id=actor_id,
            name=actor.company_name,
            logo=actor.logo_filename,
            brand_color=data.get('brandColor', defaults['brandColor']),
            google_analytics=data.get('ga', defaults['ga']),
            facebook_analytics=data.get('fbTrackingCode', None),
            stripe_key=data.get('stripe', defaults['stripe'])
        )
        create_delivery_options(
            shop,
            has_express=data.get('hasExpress', False),
            has_pickup_box=bool(actor.has_pickup_box)
        )
        session.add(WebShopContact(webshop=shop, type='phone', value=actor.phone))
        session.add(WebShopContact(webshop=shop, type='email', value=actor.email))
        session.add(WebShopContact(webshop=shop, type='person', value='{} {}'.format(actor.first_name, actor.last_name)))
        session.add(WebShopContact(webshop=shop, type='website', value=actor.domain))
        session.add(WebShopContact(webshop=shop, type='website', value=actor.web_site, rank=2))
        session.add(shop)
        session.commit()
        create_banners(defaults)


def main():
    create_tables()
    create_shops()


def create_tables():
    shop_has_banner.drop(engine)
    WebShopContact.__table__.drop(engine)
    WebShop.__table__.drop(engine)
    WebShopBanner.__table__.drop(engine)
    WebShop.__table__.create(engine)
    WebShopBanner.__table__.create(engine)
    WebShopContact.__table__.create(engine)
    shop_has_banner.create(engine)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
