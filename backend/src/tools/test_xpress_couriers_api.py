import datetime
from decimal import Decimal

import faker
import pytest
from lxml import etree

from gwio.integrations.xpress_couriers import (
    COD,
    Location,
    LogisticsAPI,
    Parcel,
    ServiceType,
    ServiceId,
)

fake = faker.Faker("pl_PL")

REGON = "382444620"
KRS = "0000769372"
CLIENT_NUMBER = "4035957"
NIP = "5272880839"
LOCATIONS = dict(
    payer=Location(
        client_tax_id=NIP,
        client_no=CLIENT_NUMBER,
        name="LUSABI INVESTMENTS SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ SPÓŁKA KOMANDYTOWA",
        address="Aleja Jana Pawla II 43a/37b",
        city="Warszawa",
        post_code="01-100",
        country_code="pl",
        is_private_person=False,
    ),
    echo=Location(
        name=fake.name(),
        address="Świętokrzyska 20",
        city="Kielce",
        post_code="25-406",
        country_code="pl",
        is_private_person=False,
    ),
    mlociny=Location(
        name=fake.name(),
        address='Zgrupowania AK "Kampinos" 15',
        city="Warszawa",
        post_code="01-943",
        country_code="pl",
        is_private_person=False,
    ),
    gw=Location(
        name="The Reseller Gateway Oy",
        address="Kaisaniemenkatu 2b",
        city="Helsinki",
        post_code="00100",
        country_code="fi",
        is_private_person=False
    ),
    jan_kowalski=Location(
        name="Jan Kowalski",
        address="Jastrzębia 31",
        city="Warsaw",
        post_code="02-804",
        country_code="PL",
        is_private_person=True
    ),
    janski=Location(
        name="Jan Kowalski",
        address="Karabeli 21",
        city='Lipków',
        post_code='05-080',
        country_code='PL',
        is_private_person=True
    ),
    markus=Location(
        name='Markus',
        address='Składowa 2',
        city='Kielce',
        post_code='25-505',
        country_code='PL',
        is_private_person=True
    )
)

small_parcel = Parcel(type=Parcel.Type.PACKAGE,
                      weight=Decimal("0.1"),
                      length=Decimal("0.1"),
                      heigth=Decimal("0.1"),
                      width=Decimal("0.1"),
                      )
sample_cod = COD(
    amount=Decimal("0.0"),
    ret_account_no=""
)


class TestAPI:
    @pytest.mark.parametrize("service_type", list(ServiceType))
    @pytest.mark.parametrize("delivery_loc", ("jan_kowalski",))
    @pytest.mark.parametrize("cod", (None, sample_cod))
    def test_get_available_services(self, delivery_loc, service_type, cod):
        api1 = LogisticsAPI("http://api.x-press.com.pl:85/Service.asmx?WSDL", debug=True)
        api2 = LogisticsAPI("http://api002.x-press.com.pl:86/Service.asmx?WSDL", debug=True)
        api3 = LogisticsAPI("http://api003.x-press.com.pl:89/Service.asmx?WSDL", debug=True)
        api = api1
        expected = api.get_available_services(
            service_type=service_type,
            payer=LOCATIONS['payer'],
            pickup_location=LOCATIONS['mlociny'],
            delivery_location=LOCATIONS[delivery_loc],
            parcels=[small_parcel],
            cash_on_delivery=cod,
            ready_date=datetime.datetime.now(),
            insurance_amount=Decimal(10),
        )
        response_xml = etree.tostring(api.history.last_received['envelope'], pretty_print=True)
        assert expected['AvailableServices'] is not None, response_xml

    @pytest.mark.parametrize("service_type", list(ServiceType))
    def test_create_order(self, service_type):
        api1 = LogisticsAPI("http://api.x-press.com.pl:85/Service.asmx?WSDL", debug=True)
        api3 = LogisticsAPI("http://api003.x-press.com.pl:89/Service.asmx?WSDL", debug=True)
        api = api3
        expected = api.get_available_services(
            service_type=ServiceType.DOMESTIC,
            payer=LOCATIONS['payer'],
            pickup_location=LOCATIONS['mlociny'],
            # delivery_location=LOCATIONS['gw'],
            delivery_location=LOCATIONS['gw'],
            parcels=[small_parcel],
            cash_on_delivery=sample_cod,
            ready_date=datetime.datetime.now(),
        )
        response_xml = etree.tostring(api.history.last_received['envelope'], pretty_print=True)
        assert expected['available_services'] is not None, response_xml

        for service in expected['available_services']:
            order_res = api.create_order(
                payer=LOCATIONS['payer'],
                service_type=service_type,
                service_id=ServiceId(service.id),
                pickup_location=LOCATIONS['echo'],
                delivery_location=LOCATIONS['gw'],
                ready_date=datetime.datetime.now(),
                cash_on_delivery=COD(amount=Decimal(0), ret_account_no=None),
                parcels=[small_parcel],
            )
            print(order_res)
