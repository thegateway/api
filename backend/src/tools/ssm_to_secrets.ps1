param($NAMESPACE)

if ($null -eq $NAMESPACE)
{
    Write-Host "Usage: ssm_to_secrets.ps1 <namespace>"
    exit
}
Write-Host "Using namespace '$NAMESPACE'"

$ssm_params=Get-SSMParametersByPath -Path $NAMESPACE -WithDecryption $true|Select-Object @{l="Key";e={$_.Name.Substring($NAMESPACE.Length + 1)}},Value
$secrets = @{}
$ssm_params|ForEach-Object { $secrets.($_.Key) = $_.Value }
$value = $secrets|ConvertTo-Json
# $secret = Get-SECSecretList|where -Property Name -eq $NAMESPACE
Write-SECSecretValue -SecretString $value -SecretId $NAMESPACE

