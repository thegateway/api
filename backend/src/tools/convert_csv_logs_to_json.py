import csv
import glob
import gzip
import json
import logging
import os.path

import click

logger = logging.getLogger(__name__)


@click.command()
@click.argument('directory')
@click.option('-o', '--outfile', default="cw_insights.json")
def main(directory, outfile):
    messages = list(_convert(directory))
    with click.open_file(outfile, 'w') as f:
        json.dump(messages, f)


def _convert(directory):
    for filename in glob.glob(os.path.join(directory, '**/*.gz'), recursive=True):
        with gzip.open(filename, 'rt') as f:
            for line in f:
                yield json.loads(line.split(None, 1)[-1])


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
