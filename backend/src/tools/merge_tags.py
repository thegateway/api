import uuid

import click

from gwio.environment import Env
from gwio.models.bootstrap import Bootstrap
from gwio.models.products import Product
from gwio.models.tags import (
    Tag,
    TagImageSchema,
)


@click.command()
@click.option(
    '--source_guid',
    prompt='Merge tag ',
    help="The guid for the tag that will have it's product_guids merged to the other tag and then be deleted",
    type=uuid.UUID,
)
@click.option(
    '--target_guid',
    prompt='... into tag ',
    help="The tag that the other tag's product guids get added to",
    type=uuid.UUID,
)
def main(source_guid: uuid.UUID, target_guid: uuid.UUID):
    source_tag = Tag.by_guid(source_guid)
    target_tag = Tag.by_guid(target_guid)
    
    if source_tag.type != target_tag.type:
        # TODO: Add a `force` option to ignore this, preferably a ask for confirmation at this stage if possible
        raise ValueError('The tags have a different type, can not merge!')

    if source_tag.owner != target_tag.owner:
        # TODO: Add a `force` option to ignore this, preferably a ask for confirmation at this stage if possible
        raise ValueError('The tags have a different owner, can not merge!')

    click.echo('Updating non-related tag data...')
    image_schema = TagImageSchema()
    for source_image in source_tag.images:
        serialisez_src_image = image_schema.dump(source_image)
        try:
            next(x for x in target_tag.images if serialisez_src_image == image_schema.dump(x))
        except StopIteration:
            target_tag.images.append(source_image)
    click.echo('DONE!')

    click.echo('Updating products...')
    i = 0
    product_count = len(source_tag.product_guids)
    for product_guid in source_tag.product_guids:
        i += 1
        try:
            product = Product.by_guid(product_guid)
            target_tag.add_product(product)
            product.save()
        except Product.DoesNotExist:
            pass
        click.echo(f'\r{i}/{product_count}', nl=False)  # so there is some way to monitor the progress of the
    target_tag.save()
    source_tag.delete()
    click.echo('DONE!')

    click.echo('Checking if tag hierarchy in Bootstrap needs to be updated...')
    bs = Bootstrap.get(vertical=Env.VERTICAL)

    def check_hierarchy(tag_list, modified_count=0):
        for hierarchy_tag in tag_list:
            if hierarchy_tag.guid == source_tag.guid:
                hierarchy_tag.guid = target_tag.guid
                hierarchy_tag.name = target_tag.name
                modified_count += 1
            modified_count = check_hierarchy(hierarchy_tag.children, modified_count)

        return modified_count

    modified_count = check_hierarchy(bs.tag_hierarchy.values())
    bs.save()

    click.echo(f'DONE! {modified_count} entires in hierarchy updated.')


if __name__ == '__main__':
    main()
