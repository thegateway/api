$VER = (Get-LMLayerVersionList -LayerName gwio-api | Select -First 1).Version 
Invoke-WebRequest (Get-LMLayerVersion -LayerName gwio-api -VersionNumber $VER).Content.Location -OutFile layer.zip
docker build -t api-$VER .
