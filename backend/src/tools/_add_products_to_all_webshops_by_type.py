import sys
from collections import defaultdict

from gwio.environment import Env

from gwio.models.products import Product
from gwio.models.webshop_product import WebshopProduct
from gwio.models.webshops import Webshop


def _print(content: str, verbose: bool) -> None:
    if verbose:
        sys.stdout.write(content)
        sys.stdout.flush()


if __name__ == '__main__':
    print(f'''Make sure the following information is correct before running the import.

environment: {Env.APPLICATION_ENVIRONMENT}
vertical: {Env.VERTICAL}
example table : {Product.Meta.table_name}

Run the import from with `python -c "from _add_products_to_all_webshops_by_type import main; main('PRODUCT_TYPE', verbose=True)"`
''')


def main(type: str, *, verbose: bool = False):
    if verbose:
        print('Querying database')
    products = Product.TypeIndex.query(type)
    webshops = list(Webshop.scan())
    counters = defaultdict(int)

    for product in products:
        _print(f'Adding {product.guid} ', verbose)
        for webshop in webshops:
            try:
                _ = WebshopProduct.get(product.guid, webshop.guid)
                _print('.', verbose)
                counters['exists'] += 1
            except WebshopProduct.DoesNotExist:
                wsp = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
                wsp.save()
                counters['added'] += 1
                _print('+', verbose)
        _print(' Done\n', verbose)
        counters['products'] += 1

    if verbose:
        print(f"'Handled {counters['products']} products for {len(webshops)} webshops'")
        print('Webshops')
        for index, webshop in enumerate(webshops):
            print(f'  {index}: {webshop.guid}')
        print('Counts')
        for key, value in counters.items():
            print(f'  {key}: {value}')
