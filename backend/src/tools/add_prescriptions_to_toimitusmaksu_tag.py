import sys
from datetime import datetime, timedelta
from decimal import Decimal

from gwio.models.modifiers import ModifierMap, ModifierMethodType
from gwio.models.products import Product
from gwio.models.tags import DurationAttribute, ProductTags, Tag

tag = Tag(
    name='Toimitusmaksu',
    type=Tag.Type.CAMPAIGN,
    duration=DurationAttribute(
        start=datetime.today() + timedelta(days=-1),
        end=datetime.today() + timedelta(days=356 * 100),
    ),
    data=ModifierMap(
        desc='Toimitusmaksu',
        method=ModifierMethodType.FIXED_UNIT,
        amount=Decimal('2.387'),
    ))
tag.save()

for p in Product.scan(Product.type == 'prescription_medicine'):
    pt = ProductTags(tag_guid=tag.guid, product_guid=p.guid)
    pt.save()
    sys.stdout.write('.')
    sys.stdout.flush()
print('Done!')
