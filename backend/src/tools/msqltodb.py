# -*- coding: utf-8 -*-
"""
Quick hack tool to import current product database to elastic cloud
"""
import datetime
from logging import getLogger

import sqlalchemy
from elasticsearch import Elasticsearch
from envs import env
from flask_restplus.marshalling import marshal
from gwio.core.models import erp
from gwio.utils.uuid import uuid_over_values, get_namespace
from gwio.legacy.api.serializers import item_detailed_legacy
from sqlalchemy.orm import sessionmaker

from gwio.models.products import Product
from gwio.models.products._vertical.imeds import DynamoProductSchema, ProductAssessor

log = getLogger(__name__)


def _mapped(product):
    d = dict(name=product['name'], type=product['type'], code=product.get('vnr_code', ''),
             desc=product.get('short_description', ''), data=product)
    for key in d:
        if d[key] is None:
            d[key] = ''
    return d


def main():
    host = env('ES_HOST', '127.0.0.1')
    port = env('ES_PORT', 9200)
    es = Elasticsearch(hosts=[{'host': host, 'port': port}])
    schema = DynamoProductSchema()
    ns = get_namespace('product')
    engine = sqlalchemy.create_engine(env('SQL_CONNECT', 'mysql+pymysql://root:mysecretpassword@localhost/imeds'))
    Session = sessionmaker(bind=engine)
    s = Session()
    i = 0
    t = 0
    assessor = ProductAssessor()
    for p in s.query(erp.Product):
        t += 1
        if p.deleted_at > datetime.datetime.now():
            product = _mapped(marshal(p, item_detailed_legacy))
            if not assessor.supported(product['type']):
                product['type'] = 'default'
            uuid = uuid_over_values(schema.ApiMeta.uuid_keys, product, ns=ns)
            try:
                pdb_product = Product.by_guid(uuid)
            except Product.DoesNotExist:
                pdb_product = Product(uuid, **product)

            try:
                pdb_product.save()
                assessed = assessor.evaluate(pdb_product)
                try:
                    es.index(index=pdb_product.type, doc_type='product', id=uuid, body=schema.dump(assessed).data)
                except Exception as e:  # Don't fail on elasticsearch failures
                    log.exception(e)
                i += 1
                print('{i}/{t}'.format(i=i, t=t), end='\r', flush=True)
            except Exception as e:
                log.exception(e)
    print('')
    print('Completed ({i}/{t})'.format(i=i, t=t))


if __name__ == '__main__':
    main()
