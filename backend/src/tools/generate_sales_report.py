import datetime
import uuid

import click
import dateutil
import pytz
import tzlocal

from gwio.tools.bookkeeping import generate_sales_reports

date = datetime.date.today().replace(day=1) - dateutil.relativedelta.relativedelta(months=1)


@click.command()
@click.option('--subject', prompt='Subject', help='The guid for the webshop or other entity to generate the bookkeeping report for')
@click.option('--year', default=date.year, prompt='Year')
@click.option('--month', default=date.month, prompt='Month')
@click.option('--timezone', default=tzlocal.get_localzone().zone, prompt='Timezone')
@click.argument('output', type=click.File('wb'))
def main(subject: str, year: int, month: int, timezone: datetime.tzinfo, output):
    subject_guid = uuid.UUID(subject)
    generate_sales_reports(
        file=output,
        subject_guid=subject_guid,
        year=year,
        month=month,
        timezone=pytz.timezone(timezone),
    )


if __name__ == '__main__':
    main()
