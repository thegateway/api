# coding=utf-8
import os
import sys

import envs
from elasticsearch import Elasticsearch
from gwio.environment import Env


# Accept WEBSHOP_PRODUCT_INDEX, BARE_PRODUCT_INDEX, LOG_EVENT_INDEX env variables
# Create new indexes based on env variables provided

def main():
    try:
        from elasticsearch_mapping.es_mapping_for_bare_product import es_mapping as bare_product_map
        from elasticsearch_mapping.es_mapping_for_webshop_product import es_mapping as webshop_product_map
        from elasticsearch_mapping.es_mapping_for_log_events import es_mapping as log_event_map
    except ModuleNotFoundError:
        es_mapping_dir = ''.join([os.path.abspath('').split('backend')[0], '/elasticsearch_mapping'])
        sys.path.append(es_mapping_dir)
        from es_mapping_for_bare_product import es_mapping as bare_product_map
        from es_mapping_for_webshop_product import es_mapping as webshop_product_map
        from es_mapping_for_log_events import es_mapping as log_event_map

    def create_index(index_name, index_map):
        elastic_client = Elasticsearch(Env.ELASTICSEARCH_HOST)

        # make an API call to the Elasticsearch cluster
        # and have it return a response:
        response = elastic_client.indices.create(
            index=index_name,
            body=index_map,
        )

        # print out the response:
        print('response:', response)

    try:
        if envs.env('WEBSHOP_PRODUCT_INDEX', var_type='boolean'):
            index = f"{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}-webshop_product"
            es_map = webshop_product_map
            create_index(index_name=index, index_map=es_map)

        if envs.env('BARE_PRODUCT_INDEX', var_type='boolean'):
            index = f"{Env.VERTICAL}-{Env.APPLICATION_ENVIRONMENT}-bare_product"
            es_map = bare_product_map
            create_index(index_name=index, index_map=es_map)

        if envs.env('LOG_EVENT_INDEX', var_type='boolean'):
            index = f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs"
            es_map = log_event_map
            create_index(index_name=index, index_map=es_map)
    except ValueError:
        pass


if __name__ == '__main__':
    main()
