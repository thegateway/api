# https://stackoverflow.com/a/43612035
import sys

import boto3

import gwio.models
from gwio.ext.pynamodb import all_models

DYNAMO_CLIENT = boto3.client('dynamodb')

assert gwio.models


def main(target_environment_name: str):
    for table in all_models:
        source_table_name = table.Meta.table_name
        dynamo_paginator = DYNAMO_CLIENT.get_paginator('scan')

        response = dynamo_paginator.paginate(
            TableName=source_table_name,
            Select='ALL_ATTRIBUTES',
            ReturnConsumedCapacity='NONE',
            ConsistentRead=True
        )

        split_table_name = source_table_name.split('-')
        split_table_name[1] = target_environment_name
        target_table_name = '-'.join(split_table_name)

        for page in response:
            for item in page['Items']:
                DYNAMO_CLIENT.put_item(
                    TableName=target_table_name,
                    Item=item
                )


if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except IndexError:
        print(f'Usage: GW_VERTICAL_NAME=vertical_name APPLICATION_ENVIRONMENT=environment {sys.argv[0]} target_environment'
              'Copies the rows from the table defined in the environment to target environment given as a parameter.'
              'Target tables have to exist before running this script and they will not be emptied before inserting new rows.')
