import envs
from setuptools import (
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'attrdict',
    'chalice',
    'gwio-utils',
    'jinja2',
    'jinja2-s3loader'
]

setup(
    name='gwio-email-notifications',
    version=__VERSION__,
    packages=['gwio', 'gwio.services', 'gwio.services.notification_to_email'],
    url='https://www.gwapi.eu/',
    license='',
    install_requires=install_requires,
    author='The Reseller Gateway Oy',
    author_email='dev@the.gw',
    description=''
)
