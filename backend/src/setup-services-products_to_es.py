import envs
from setuptools import (
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'attrdict',
    'backoff',
    'boto3',
    'certifi',
    'chalice',
    'elasticsearch',
    'gwio-utils',
    'pynamodb',
    'urllib3',
]

setup(
    name='gwio-products_to_es',
    version=__VERSION__,
    packages=['gwio', 'gwio.services', 'gwio.services.products_to_es'],
    url='https://www.gwapi.eu/',
    license='',
    install_requires=install_requires,
    author='The Reseller Gateway Oy',
    author_email='dev@the.gw',
    description=''
)
