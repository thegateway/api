include vars-api.mk

PREFIXED_MODULES=$(foreach module, $(GWIO_MODULES), gwio.$(module))
MODULE_DIRS=$(foreach module, $(GWIO_MODULES), $(REPO_ROOT)/backend/src/gwio/$(module))
CFG_PATH=$(REPO_ROOT)/backend/src/setup-api.cfg

.PHONY: build

all: build check lint

build:
		$(PYPATHSH) python $(REPO_ROOT)/backend/src/setup-api.py bdist_wheel --dist-dir $(REPO_ROOT)/dist/

check:
		pytest --verbose --cov-report=html $(foreach module, $(PREFIXED_MODULES), --cov=$(module)) -c $(CFG_PATH)

lint:
		pycodestyle --config $(CFG_PATH) $(MODULE_DIRS)
		flake8 --config=$(CFG_PATH) $(MODULE_DIRS)
		pylint $(MODULE_DIRS) --rcfile $(CFG_PATH) --reports yes || pylint-exit $$?

install:
		pip install --find-links $(REPO_ROOT)/dist/ --extra-index-url=https://pypi.gwapi.eu gwio-api

install-local:
		pip download gwio-api --extra-index-url=https://pypi.gwapi.eu --dest $(REPO_ROOT)/dist/
		export REPO_ROOT="$(REPO_ROOT)"; export GWIO_PACKAGES="$(PREFIXED_MODULES)"; python $(REPO_ROOT)/backend/src/setup-api.py develop --find-links $(REPO_ROOT)/dist/
