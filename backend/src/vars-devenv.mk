include ../../vars.mk
include build-vars.mk

ifeq ($(OS),Windows_NT)
	PWSH = powershell -NoProfile
	RM = $(PWSH) Remove-Item -recurse -EA Ignore -Path
	CP = $(PWSH) Copy-Item
	PIP = py -m pip
	ECRLOGINCMD = $(PWSH) Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process\; Import-Module AWSPowerShell\; Invoke-Expression -Command \(Get-ECRLoginCommand -Region eu-west-1\).Command
	MKDIR = $(PWSH) New-Item -ItemType directory
	CAT = $(PWSH) Get-Content
else
	PIP = pip
	RM = rm -rf
	CP = cp
	ECRLOGINCMD = $(shell $(aws ecr get-login --no-include-email --region eu-west-1))
	MKDIR = mkdir
	CAT = cat
endif

APPENV=${APPLICATION_ENVIRONMENT}
VERTICAL=${GW_VERTICAL_NAME}

ECR = 430384725534.dkr.ecr.eu-west-1.amazonaws.com

GWIO_MODULES:=devtools pytest
ifndef CI_PROJECT_DIR
	REPO_ROOT=$(shell git rev-parse --show-toplevel)
else
	REPO_ROOT=$(CI_PROJECT_DIR)
endif
