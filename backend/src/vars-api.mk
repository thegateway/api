include ../../vars.mk
include build-vars.mk

GWIO_MODULES:=api cache cognito core geoip integrations logic models operations schemas user
ifndef CI_PROJECT_DIR
	REPO_ROOT=$(shell git rev-parse --show-toplevel)
else
	REPO_ROOT=$(CI_PROJECT_DIR)
endif
