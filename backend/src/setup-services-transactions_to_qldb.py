import envs
from setuptools import (
    setup,
)

from gwio.devtools.utils import make_calver

__VERSION__ = make_calver(envs.env('REPO_ROOT'))

install_requires = [
    'chalice',
    'gwio-utils',
    'pyqldb',
]

setup(
    name='gwio-transactions_to_qldb',
    version=__VERSION__,
    packages=['gwio', 'gwio.services', 'gwio.services.transactions_to_qldb'],
    url='https://www.gwapi.eu/',
    license='',
    install_requires=install_requires,
    author='The Reseller Gateway Oy',
    author_email='dev@the.gw',
    description=''
)
