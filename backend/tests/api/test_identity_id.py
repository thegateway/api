import uuid

from gwio.core.authentication import replace_existing_idp_guid_references
from gwio.models.residence import Residence


class TestIdentityId:
    def test_identity_id(self, mock_aws, create_random_residence):
        user_guid = uuid.uuid4()
        identity_id = f'eu-west-1:{uuid.uuid4()}'
        residence = create_random_residence(owner_identity_id=str(user_guid))
        residence.save()
        assert residence.owner_identity_id == str(user_guid)
        residence_guid = residence.guid

        replace_existing_idp_guid_references(str(user_guid), identity_id)
        residence = Residence.get(guid=residence_guid)
        assert residence.owner_identity_id == identity_id
