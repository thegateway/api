import string
import unittest

from gwio.geoip import geohash, pluscode
from gwio.geoip.utils import check_charset


class TestGeohash(unittest.TestCase):
    def test_encode(self):
        lat, lng = 57.64911, 10.40744
        loc = 'u4pruydqqvj8'
        for l in range(5, len(loc)):
            assert loc.startswith(geohash.encode(lat, lng, l))

    def test_decode(self):
        lat, lng = 24.7475004, 89.12238004
        loc = 'turku10uturn'
        (lat_min, lat_max), (lng_min, lng_max) = geohash._decode(loc)
        assert lat_min < lat < lat_max
        assert lng_min < lng < lng_max


class TestPluscode(unittest.TestCase):
    def test_encode(self):
        assert pluscode.encode(51.476813, -0.000562) == '9C3XFXGX+PQ'  # Greenwich observatory
        assert pluscode.encode(60.170563, 24.764937) == '9GG65QC7+6X'  # Hima :)

    def test_decode(self):
        locs = (
            ('9GV6H67G+M8', 67.564187, 24.225815),  # Ylläs 718
            ('6VW9PPFQ+2W', 8.722562, 167.739815)  # Kwajalein 24
        )
        for loc, lat, lng in locs:
            (lat_min, lat_max), (lng_min, lng_max) = pluscode._decode(loc)
            assert lat_min < lat < lat_max
            assert lng_min < lng < lng_max

    def test_validate(self):
        with self.assertRaises(ValueError):
            pluscode.validate_and_normalize('foobar')


class TestUtils(unittest.TestCase):
    def test_invalid_characters(self):
        with self.assertRaises(ValueError):
            check_charset('foobar', 'abc')
        assert check_charset(string.ascii_lowercase, string.ascii_letters)



if __name__ == '__main__':
    unittest.main()
