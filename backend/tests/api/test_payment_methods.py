# coding=utf-8
"""
Tests for payment method handling
"""

import random
import uuid
from datetime import (
    datetime,
    timezone,
)

import pytest
from attrdict.merge import merge
from dateutil.parser import parse

from gwio.core.utils.testing.helpers import as_roles
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentState,
)
from gwio.logic.payments.types import PaymentTypes
from gwio.models.payment_attribute import Payment
from gwio.models.payment_method import PaymentMethod
from gwio.models.webshops import Webshop
from ._fakes.payment_methods import random_payment_method_data


@pytest.fixture(scope='class')
def test_suite(mock_aws, create_random_webshop, random_payment_methods):
    assert mock_aws
    methods = []
    for method in random_payment_methods(random.randint(10, 20)):
        method.save()
        methods.append(method)

    archived_payment_methods = random.sample(methods, random.randint(1, len(methods) - 5))
    for method in archived_payment_methods:
        method.archived = datetime.now(timezone.utc)
        method.save()

    webshop = create_random_webshop()
    webshop.payment_method_guids = [x.guid for x in PaymentMethod.scan(archived__not_exists=True)]
    webshop.save()


@pytest.mark.usefixtures('test_suite')
class TestPaymentMethods:
    dynamo_mock = None

    @pytest.mark.parametrize('as_role', [pytest.lazy_fixture('as_admin'),
                                         pytest.lazy_fixture('as_shopkeeper')])
    @pytest.mark.parametrize('archived', [True, False])
    def test_list_payment_methods(self, test_suite, archived, as_role):
        if archived:
            payment_methods = PaymentMethod.scan()
        else:
            payment_methods = PaymentMethod.scan(archived__not_exists=True)
        method_guids = [x.guid for x in payment_methods]
        r = as_role.get(f'/payment_methods?archived={archived}')
        assert 200 == r.status_code
        respond_guids = [uuid.UUID(x['guid']) for x in r.json]
        assert sorted(method_guids) == sorted(respond_guids)

    @as_roles(pytest)
    def test_get_payment_method(self, test_suite, as_role):
        payment_method = random.choice(list(PaymentMethod.scan(archived__not_exists=True)))
        r = as_role.get(f'/payment_methods/{payment_method.guid}')
        assert 200 == r.status_code
        assert PaymentMethod.Schemas.Get().dump(payment_method) == r.json

    def test_add_payment_method(self, as_admin, create_random_payment_method):
        data = PaymentMethod.Schemas.Post().dump(create_random_payment_method())
        r_post = as_admin.post('/payment_methods/', json=data)
        assert 201 == r_post.status_code
        payment_method_guid = r_post.json['guid']
        r_get = as_admin.get(f'/payment_methods/{payment_method_guid}/')
        assert 200 == r_get.status_code
        assert r_post.json == r_get.json
        for k, v in data.items():
            assert v == r_get.json[k], k
        # Adding it twice should be ok because guid is random
        r = as_admin.post('/payment_methods/', json=data)
        assert 201 == r.status_code

    def test_add_nothing(self, as_admin):
        r = as_admin.post('/payment_methods/', json=dict())
        assert r.status_code == 409, r.json
        assert 'Missing data for required field.' in r.json['message']

    @pytest.mark.parametrize('field_name', [x for x in PaymentMethod.Schemas.Put().fields.keys()] + [dict(), None])
    def test_put_payment_method(self, as_admin, test_suite, field_name):
        method = next(x for x in PaymentMethod.scan())
        payment_method_data = PaymentMethod.Schemas.Put().dump(random_payment_method_data())
        try:
            if isinstance(field_name, dict):
                data = payment_method_data
            else:
                data = {field_name: payment_method_data[field_name]}
        except KeyError:
            data = {}
        r_get_original = as_admin.get(f'/payment_methods/{str(method.guid)}/')
        r_put = as_admin.put(f'/payment_methods/{method.guid}/', json=data)
        assert 200 == r_put.status_code, r_put.json
        assert all(v == r_put.json[k] for k, v in data.items())
        r_get = as_admin.get(f'/payment_methods/{str(method.guid)}/')
        assert 200 == r_get.status_code
        assert merge(r_get_original.json, r_put.json) == r_get.json

    def test_delete_payment_method(self, as_admin, test_suite):
        # Delete a payment method will archive it
        shop = next(x for x in Webshop.scan() if x.payment_methods and any(z.archived is None for z in x.payment_methods))
        method_guid = next(x.guid for x in shop.payment_methods if x.archived is None)
        start_of_test = datetime.now(timezone.utc)
        r = as_admin.delete(f'/payment_methods/{method_guid}/')
        assert 204 == r.status_code, r.json
        r = as_admin.get(f'/payment_methods/{method_guid}/')
        assert 200 == r.status_code, r.json
        archived_date = parse(r.json['archived'])
        assert type(archived_date) == datetime
        assert start_of_test <= archived_date <= datetime.now(timezone.utc)
        # Archiving a payment method should remove it from webshop payment_methods
        r = as_admin.get(f'/webshops/{shop.guid}/payment_methods/')
        assert str(method_guid) not in [x['guid'] for x in r.json]
        r = as_admin.get(f'/webshops/{shop.guid}/')
        assert str(method_guid) not in [x['guid'] for x in r.json['payment_methods']]
        # Deleting the same payment method again should return 204, but shouldn't modify the archive date
        r = as_admin.delete(f'/payment_methods/{method_guid}/')
        assert 204 == r.status_code, r.json
        r = as_admin.get(f'/payment_methods/{method_guid}/')
        assert archived_date == parse(r.json['archived'])

    @pytest.mark.parametrize('method', ['get', 'put', 'delete'])
    def test_invalid_get_put_delete_payment_method(self, as_admin, method):
        url = f'/payment_methods/{uuid.uuid4()}/'
        r = getattr(as_admin, method)(url, json={} if method == 'put' else None)
        assert 404 == r.status_code, r.json


@pytest.mark.usefixtures('mock_stripe')
@pytest.mark.usefixtures('mock_payu')
@pytest.mark.parametrize('payment_type', list(PaymentTypes))
def test_defined_payment_types_are_supported(app, mocker, customer_identity_id, webshop, payment_type, create_random_payment_method, create_test_order):
    """
    Testing that all the payment related classes are aware of all the types and that payment can be processed all the way through.
    This test doesn't care what steps need to be manually triggered or happen automatically, just that the payment ends up in `COMPLETED` state eventually.
    """
    assert payment_type in PaymentMethod.API.Type
    assert payment_type in Payment.API.Type

    pm = create_random_payment_method(type=payment_type)
    pm.save()
    order = create_test_order(
        buyer_identity_id=customer_identity_id,
        shop_guid=webshop.guid,
        payment_method_guid=pm.guid,
        data=dict(stripe=dict(card_id='foobar')),
    )
    order.save()

    payment = pm.new_payment_from_order(order=order)
    # To prevent triggering order state machine, since we don't care about that in this test
    mocker.patch.object(payment.ProcessLogic, 'completed', lambda **_: True)

    with app.application.test_request_context():
        app.application.preprocess_request()
        payment.create_charge()
        if payment.current_state == PaymentState.NOT_STARTED:
            payment.process(PaymentAction.CHARGE)
        if payment.current_state == PaymentState.CHARGED:
            payment.process(PaymentAction.CAPTURE)
        if payment.current_state == PaymentState.WAITING_PAYMENT:
            # If we end up in waiting payment, we can mock `status_paid` because it's already been checked now and it means it exists for the payment type
            mocker.patch.object(Payment, 'status_paid', True)
            payment.process(PaymentAction.PAY)

    assert payment.current_state == PaymentState.COMPLETED
