import fakeredis
from mock import mock

from gwio.environment import Env


def mock_redis():
    server = fakeredis.FakeServer()

    redis_dbs = {x: fakeredis.FakeStrictRedis(server=server, db=x.value) for x in list(Env.RedisDatabases)}

    def mocked_get_redis(db: Env.RedisDatabases = Env.RedisDatabases.CACHE):
        return redis_dbs[db]

    mock_get_redis = mock.patch.object(Env, 'get_redis', new=mocked_get_redis)
    mock_get_redis.start()
    return server


mocked_services = dict(
    redis=mock_redis(),
)
