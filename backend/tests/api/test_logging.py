# -*- coding: utf-8 -*-
import logging

import pytest
from pytest_lazyfixture import LazyFixture

from gwio.ext.decorators import static_vars
from gwio.utils.logging import (
    _iconic_slack_message,
    configure,
    deactivate,
)
from gwio.core.utils.testing.helpers import (
    as_roles,
    endpoints_from_app,
)
from ._helpers.app import setup_app

CRITICAL_MESSAGE = 'Critical message'
ERROR_MESSAGE = 'Error message'
WARNING_MESSAGE = 'Warning message'
INFO_MESSAGE = 'Info message'
DEBUG_MESSAGE = 'Debug message'


@static_vars(last_msg=None)
def message_devops(**kwargs):
    """Record the last message that would've been sent to devops channel"""
    message_devops.last_msg = kwargs


@pytest.fixture(scope='function')
def logger():
    configure(lambda_only=False)
    yield logging.getLogger('logging_test')
    deactivate()


def test_logging_configure(monkeypatch, logger):
    monkeypatch.setattr('gwio.utils.logging.message_devops', message_devops)
    logger.critical(CRITICAL_MESSAGE)
    assert message_devops.last_msg.get('message') == _iconic_slack_message(logging.CRITICAL, CRITICAL_MESSAGE)
    logger.error(ERROR_MESSAGE)
    assert message_devops.last_msg.get('message') == _iconic_slack_message(logging.ERROR, ERROR_MESSAGE)
    logger.warning(WARNING_MESSAGE)
    assert message_devops.last_msg.get('message') == _iconic_slack_message(logging.WARNING, WARNING_MESSAGE)

    # Using the default gwio.core.utils.logging.configure() Messages of level less that WARNING should not
    # have been sent so the last_msg sent should still be the warning message
    logger.debug(DEBUG_MESSAGE)
    assert message_devops.last_msg.get('message') == _iconic_slack_message(logging.WARNING, WARNING_MESSAGE)
    logger.info(INFO_MESSAGE)
    assert message_devops.last_msg.get('message') == _iconic_slack_message(logging.WARNING, WARNING_MESSAGE)


def test_formatted_logging(monkeypatch, logger):
    monkeypatch.setattr('gwio.utils.logging.message_devops', message_devops)
    logger.critical('the %s', 'message')
    assert message_devops.last_msg.get('message') == _iconic_slack_message(logging.CRITICAL, 'the message')


class FailingBoto3:
    def client(self, *_, **__):
        raise RuntimeError('This always fails')


def test_logging_failure_to_send(logger, mocker):
    mocker.patch('gwio.utils.aws.boto3', wraps=FailingBoto3)
    logger.critical(CRITICAL_MESSAGE)


class TestLogging:
    @pytest.mark.parametrize(
        'as_role, identity_id',
        [
            (pytest.lazy_fixture('as_anonymous'), None),
            # TODO: enable once none of the endpoints try to connect to real aws
            # (aka moto has improved coverage or we have patched the required stuff)
            # (pytest.lazy_fixture('as_admin'), pytest.lazy_fixture('admin_identity_id')),
        ],
        ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x),
    )
    @pytest.mark.parametrize('method, url', endpoints_from_app(setup_app()))
    def test_endpoint_audit(self, check_endpoint_audits, as_role, identity_id, method, url):
        check_endpoint_audits(
            as_role=as_role,
            identity_id=identity_id,
            method=method,
            url=url,
        )

    # TODO: Fix to check no audit log event is sent to EventBridge for swagger access
    # def test_swagger_no_audit(self, mock_aws, as_anonymous):
    #     pre_events = mock_aws['clients']['logs'].filter_log_events(logGroupName=LogEventsUtil.event_bus)['events']
    #     r = as_anonymous.get('/swagger.json')
    #     assert r.status_code == 200
    #     post_events = mock_aws['clients']['logs'].filter_log_events(logGroupName=LogEventsUtil.event_bus)['events']
    #     assert len(pre_events) == len(post_events)

    @as_roles(pytest, get=('as_role', 'identity_id'))
    def test_bogus_url_audit(self, faker, audit_request, as_role, identity_id):
        r, audit_log, _ = audit_request(
            request_method=as_role.get,
            url=f'/{"/".join(faker.words(nb=3))}',
        )
        assert r.status_code == 404
        assert audit_log.subject_id == identity_id
