import random
import uuid
from decimal import Decimal

import pytest
from dynamorm import DynaModel
from marshmallow import (
    ValidationError,
    fields,
)

from gwio.core.utils.testing.helpers import fake
from gwio.ext.dynamorm import (
    GwioDict,
    GwioDictField,
    GwioList,
    GwioListField,
    GwioModelField,
    NestedSchemaModel,
    _ModelMixIn,
    all_models,
)
from gwio.ext.marshmallow import (
    GwioEnumField,
    GwioSchema,
    HexColor,
)
from gwio.ext.vat import (
    Vat,
    VatField,
)
from gwio.models.brands import Brand
from gwio.models.orders import Order
from gwio.models.pricing import Pricing
from gwio.models.tags import Tag
from gwio.models.user import UserData


def _random_enum(field):
    return random.choice(list(field.enum))


def _random_nested(field):
    return _fake_data_for_schema(field.nested)


def _random_model_data(field):
    return _fake_data_for_schema(field._model.Schema)


field_values = {
    GwioEnumField: _random_enum,
    GwioModelField: _random_model_data,
    HexColor: fake.color(),
    VatField: Vat(10),
    fields.Decimal: Decimal('0'),
    fields.Integer: 0,
    fields.Nested: _random_nested,
    fields.String: '',  # This is the whole point of the test
    fields.Url: fake.url(),
    fields.UUID: uuid.uuid4(),
}


def _fake_data_for_schema(schema):
    values = dict()
    for field_name, field in schema().declared_fields.items():
        if not (isinstance(field, fields.String) or field.required == True):
            continue
        value = field_values[field.__class__]
        if callable(value):
            value = value(field)
        values[field_name] = value
    return values


# TODO: Fix the stupid checks and/or generate better fake data so these can be included as well
skip_models = (Brand, Order, Pricing, Tag, UserData)


@pytest.mark.parametrize('model', [x for x in all_models if x not in skip_models])
def test_zero_length_string_save(model):
    values = _fake_data_for_schema(model.Schema)

    inst = model(**values)
    required_error_msg = fields.String().error_messages['required']
    try:
        inst.save()
        inst.delete()
    except ValidationError as e:
        # Some of zero length strings that got converted to None, might have been required.
        # Only allowing those validation errors, because otherwise error might be coming from the init and we're getting to test the correct thing
        for field_name, messages in e.messages.items():
            assert isinstance(model.Schema().declared_fields[field_name], fields.String), field_name
            assert len(messages) == 1, field_name
            assert messages[0] == required_error_msg, field_name


@pytest.mark.parametrize('model', all_models)
def test_using_correct_struct_fields(model):
    error_msg_fns = {
        fields.Nested: dict(
            check=lambda x: True,
            msg=lambda x, msg: f'{msg} is {fields.Nested.__name__}, should be {GwioListField.__name__ if x.many else GwioModelField.__name__}',
        ),
        fields.Dict: dict(
            check=lambda x: hasattr(x, 'values') and isinstance(x.values, fields.Nested),
            msg=lambda _, msg: f'{msg} is {fields.Dict.__name__}, should be {GwioModelField.__name__} or {GwioDictField.__name__}',
        ),
    }

    def assert_no_nested_field(model, assert_msg=None, parent_models=None):
        fields_dict = model.Schema().declared_fields
        if assert_msg is None:
            assert_msg = model.__name__
        if parent_models is None:
            parent_models = {model}

        for field_name, field in fields_dict.items():
            for assert_not_field, error_fns in error_msg_fns.items():
                assert not (field.__class__ is assert_not_field and error_fns['check'](field)), error_fns['msg'](field, f'{assert_msg}.{field_name}')

            if isinstance(field, (GwioDictField, GwioListField, GwioModelField)) and field.model_cls not in parent_models:
                parent_models.add(field.model_cls)
                assert_no_nested_field(field.model_cls, f'{assert_msg}.{field_name}', parent_models),

    assert_no_nested_field(model)


def test_set_structures_none():
    class TestSchema:
        guid = fields.UUID()
        dict_struct = GwioDictField(keys_type=str, keys_field=fields.String(), values=lambda: TestNestedModel, schema='_BootstrapTagBaseSchema')
        list_struct = GwioListField(lambda: TestNestedModel, schema='_BootstrapTagBaseSchema')

    class TestNestedModel(NestedSchemaModel):
        class Schema(TestSchema, GwioSchema):
            pass

    class TestModel(_ModelMixIn, DynaModel):
        Schema = TestSchema

        class Table:
            name = 'test-structures'
            hash_key = 'guid'

    def assert_structs(inst):
        assert isinstance(inst.dict_struct, GwioDict)
        assert isinstance(inst.list_struct, GwioList)

    # Test structures are initialised correctly after model is initialised without values
    model = TestModel()
    assert_structs(model)
    # Test setting to None initialises structure instead of setting it to None
    model.dict_struct = None
    model.list_struct = None
    assert_structs(model)
    # and same directly during initialisation of the model
    assert_structs(TestModel(dict_struct=None, list_struct=None))

    # Test setting structures to None initialises structures inside GwioDict
    model.dict_struct['foo'] = dict()
    dict_item = model.dict_struct['foo']
    assert_structs(dict_item)  # Testing init with no values given
    dict_item.dict_struct = None
    dict_item.list_struct = None
    assert_structs(dict_item)
    # and same directly during initialisation of the dict
    model.dict_struct['bar'] = dict(dict_struct=None, list_struct=None)
    assert_structs(model.dict_struct['bar'])

    # Test setting structures to None initialises structures inside GwioList
    model.list_struct.append(dict())
    list_item = model.list_struct[-1]
    assert_structs(list_item)  # Testing init with no values given
    list_item.dict_struct = None
    list_item.list_struct = None
    assert_structs(list_item)
    # and same directly during initialisation of the list
    model.list_struct.append(dict(dict_struct=None, list_struct=None))
    assert_structs(model.list_struct[-1])
