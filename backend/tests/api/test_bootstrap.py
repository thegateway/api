# coding=utf-8
import uuid
from random import randint

import pytest
from gwio.models.bootstrap import Bootstrap, normalise_display_name_key
from gwio.models.tags import Tag

from gwio.utils.aws import CognitoIdpClient
from gwio.core.utils.testing.helpers import fake
from gwio.models.webshops import Webshop

from ._fakes.bootstrap import random_bootstrap_data


@pytest.fixture
def bootstrap_shops(mock_aws, webshop, random_webshops):
    def _all():
        yield webshop
        yield from random_webshops(randint(1, 8))

    assert mock_aws
    shops = list()
    for shop in _all():
        shop.save()
        shops.append(shop)
    return shops


@pytest.fixture(scope='function')
def bootstrap(mock_aws, create_bootstrap):
    bootstrap = create_bootstrap()
    bootstrap.save()
    yield bootstrap
    bootstrap.delete()


@pytest.mark.usefixtures('bootstrap_shops')
class TestBootstrap:
    def test_bootstrap_without_origin(self, app, as_anonymous, bootstrap_shops, bootstrap):
        r = as_anonymous.get('/bootstrap/')
        assert 200 == r.status_code
        assert set([shop.guid for shop in Webshop.scan() if shop.enabled]) == set([uuid.UUID(shop['guid']) for shop in r.json['shops']])
        del bootstrap.data['dashboard']
        assert bootstrap.data == r.json['data']
        assert 'dashboard' not in r.json['data']
        assert sorted(CognitoIdpClient._whitelist) == sorted(r.json['password_policy'].keys())
        for key in CognitoIdpClient._blacklist:
            assert key not in r.json['password_policy']

    @pytest.mark.parametrize('fixture', [pytest.lazy_fixture('bootstrap'), None])
    def test_bootstrap_put(self, app, as_admin, fixture, bootstrap_data):
        put_data = bootstrap_data()
        r = as_admin.put('/bootstrap/', json=put_data)
        assert 200 == r.status_code
        r = as_admin.get('/bootstrap/')
        dashbord_data = put_data['data'].pop('dashboard')
        assert put_data['data'] == r.json['data']
        r = as_admin.get('/dashboard/bootstrap')
        assert r.json['data'] == dashbord_data

    def test_boostrap_by_shop_guid(self, app, as_anonymous, webshop_guid, bootstrap):
        r = as_anonymous.get(f'/bootstrap/{str(webshop_guid)}')
        assert 200 == r.status_code
        assert webshop_guid == uuid.UUID(r.json['shop']['guid'])
        assert bootstrap.data == r.json['data']

    def test_bootstrap_with_unknown_referer(self, app, as_anonymous):
        r = as_anonymous.get('/bootstrap/', headers={'Origin': fake.hostname()})
        assert 200 == r.status_code

    def test_bootstrap_tag_hierarchy_post_put(self, as_admin):
        put_data = Bootstrap.Schemas.Put().dump(random_bootstrap_data())
        r = as_admin.put('/bootstrap/', json=put_data)
        assert 200 == r.status_code
        normalise_display_name_key(put_data['tag_hierarchy'])
        assert put_data == Bootstrap.Schemas.Put().dump(r.json)
        assert r.json['tag_hierarchy'][0]['children'][0]['name'] == Tag.by_guid(put_data['tag_hierarchy'][0]['children'][0]['guid']).name
        # Sending tags name in Put request should raise conflict
        r = as_admin.put('/bootstrap/', json=random_bootstrap_data())
        assert 409 == r.status_code

    def test_bootstrap_tag_hierarchy_hidden_field(self, as_admin):
        put_data = Bootstrap.Schemas.Put().dump(random_bootstrap_data())
        r = as_admin.put('/bootstrap/', json=put_data)
        assert 200 == r.status_code
        assert not r.json['tag_hierarchy'][0]['hidden']
        put_data['tag_hierarchy'][0]['hidden'] = True
        r = as_admin.put('/bootstrap/', json=put_data)
        assert 200 == r.status_code
        assert r.json['tag_hierarchy'][0]['hidden']
