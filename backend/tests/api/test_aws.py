# coding=utf-8
import base64
import os
import random

import pytest
import requests
from flask import json
# AWS has both of these in their documentation and no-one seems to know why.
from gwio.environment import Env


# TODO: Refactor or remove the test
@pytest.mark.skip('No unit tests against the real aws')
class TestAWS:
    filename = base64.b32encode(os.urandom(10)).decode('ascii')

    def test_environment(self):
        assert Env.UPLOAD_LOCATION

    def test_as_shopkeeper(self, as_shopkeeper):
        self._test_upload(as_shopkeeper)

    def test_as_admin(self, as_admin):
        self._test_upload(as_admin)

    def test_as_brand_owner(self, as_brand_owner):
        self._test_upload(as_brand_owner)

    def _test_upload(self, client):
        test_data = 'foobar'
        content_type = random.choice(('image/png', 'image/jpeg', 'text/plain'))
        res = client.post('/uploads/', json={'filename': self.filename, 'content_type': content_type})
        data = json.loads(res.data)
        payload = data['fields']
        r = requests.post(data['url'], data=payload, files={'file': test_data})
        assert r.status_code == 204, r.text
        fileurl = '{}{}'.format(data['url'], data['fields']['key'])
        r = requests.get(fileurl)
        assert r.status_code == 200, r.text
        assert r.text == test_data
        assert content_type in r.headers['Content-Type']
