import os
import unittest

import boto3
import pytest
from gwio.environment import Env

from gwio.core.utils.testing.helpers import fake
from gwio.environment.environment import (
    Store,
    Setting,
)


class TestEnvironment(unittest.TestCase):
    @pytest.mark.usefixtures('mock_aws')
    @pytest.mark.skip('Cannot test due to mocking breaking with `Env`')
    def test_ssm_param(self):
        c = boto3.client('ssm')
        paramname = fake.word()
        value = fake.email()
        Env.SETTINGS.update((Setting(name=paramname, store=Store.SSM),))
        Env.__class__._store = None
        c.put_parameter(Name=f'{Env.NAMESPACE}/{paramname}', Type='String', Value=value)
        Env._load()
        assert getattr(Env, paramname) == value

    def test_nonexistent_param(self):
        with self.assertRaises(AttributeError):
            assert Env.KUKKAKAALI

    def test_env_var(self):
        assert Env.NAMESPACE
        assert Env.VERTICAL
        assert Env.APPLICATION_ENVIRONMENT
        assert(os.environ['NAMESPACE'].startswith(Env.NAMESPACE))


if __name__ == '__main__':
    unittest.main()
