# -*- coding: utf-8 -*-
"""
Test webshop specific pricing
"""
import random

import pytest

from gwio.ext import SYSTEM_GUID
from gwio.models.pricing import Pricing
from ._fakes.pricing import random_pricing


@pytest.fixture(scope='class')
def system_pricing(create_random_pricing):
    return create_random_pricing()


@pytest.fixture(scope='class')
def webshop_pricings(webshop_guid, create_random_pricing):
    pricings = list()
    for _ in range(3):
        pricing = create_random_pricing(owner=webshop_guid)
        pricings.append(pricing)
    return pricings


@pytest.fixture(scope='class')
def test_suite(mock_aws, webshop_pricings, system_pricing):
    assert mock_aws
    system_pricing.save()
    for pricing in webshop_pricings:
        pricing.save()


@pytest.mark.usefixtures('test_suite')
class TestWebshopPricingAPI:
    schema = Pricing.Schemas.Get()

    def test_get_single_pricing(self, as_admin, webshop_guid, webshop_pricings):
        pricing_guid = random.choice(webshop_pricings).guid
        r = as_admin.get(f'/webshop_pricings/{webshop_guid}/{pricing_guid}')
        assert r.status_code == 200

    def test_get_system_pricing(self, as_admin, webshop_guid, system_pricing):
        r = as_admin.get(f'/webshop_pricings/{webshop_guid}/{system_pricing.guid}')
        assert r.status_code == 200

    def test_list_pricings(self, as_admin, webshop_guid, webshop_pricings):
        r = as_admin.get(f'/webshop_pricings/{webshop_guid}/_list')
        assert r.status_code == 200
        assert {str(x.guid) for x in webshop_pricings} == {x['guid'] for x in r.json}

        r = as_admin.get(f'/webshop_pricings/{webshop_guid}/_all')
        assert r.status_code == 200
        assert len([x for x in Pricing.scan() if x.owner in (SYSTEM_GUID, webshop_guid)]) == len(r.json)

    def test_add_and_delete(self, as_admin, create_random_pricing, webshop_guid):
        pricing = create_random_pricing(owner=webshop_guid)
        r = as_admin.post(f'/webshop_pricings/{webshop_guid}/', json=Pricing.Schemas.Post().dump(pricing))
        assert r.status_code == 201, r.json
        r = as_admin.delete(f'/webshop_pricings/{webshop_guid}/{pricing.guid}')
        assert r.status_code == 204
        r = as_admin.get(f'/webshop_pricings/{webshop_guid}/{pricing.guid}/')
        assert r.status_code == 404

    def test_put_pricing(self, as_admin, webshop_guid, webshop_pricings):
        pricing = random.choice(webshop_pricings)
        methods = Pricing.Schemas.Post().dump(random_pricing())['methods']
        r = as_admin.put(f'/webshop_pricings/{webshop_guid}/{pricing.guid}/', json=dict(methods=methods))
        assert r.status_code == 200
