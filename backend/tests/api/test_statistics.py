import datetime
import json
import os
import random
import uuid
from unittest.mock import patch

import boto3
import pytest
from gwio.environment import Env
from gwio.models.user import UserData

from .data.test_verify_data import (
    product_sold_stat_over_time,
    webshop_order_stat,
    webshop_top_sold_products,
    webshop_specific_product_stats,
    order_statistics_for_admin,
    webshops_product_stat_for_admin,
    top_sold_product_for_admin,
)

from gwio.api.statistics import (
    shop_specific_order_log_to_dataframe,
    all_order_log_to_dataframe,
)
from gwio.utils.aws import CognitoIdpClient
from gwio.core.utils.testing.helpers import fake
from gwio.models.tags import Tag
from gwio.models.webshop_product import WebshopProduct

assert CognitoIdpClient


@pytest.fixture(scope='function')
def cognitoidp_suite(mock_aws):
    conn = boto3.client("cognito-idp", Env.AWS_REGION)

    user_pool_details = conn.create_user_pool(
        PoolName='test_cognito',
        LambdaConfig={
            "PreSignUp": str(uuid.uuid4())
        }
    )
    user_pool_id = user_pool_details["UserPool"]["Id"]
    Env.COGNITO_POOL_ID = user_pool_id
    for _ in range(10):
        username = str(uuid.uuid4())
        conn.admin_create_user(UserPoolId=user_pool_id, Username=username)
    yield user_pool_details


@pytest.fixture(scope='class')
def webshop(mock_aws, create_random_webshop):
    assert mock_aws
    webshop = create_random_webshop(guid=uuid.UUID('20eb1043-8108-5400-9eac-9b31d2902740'))
    webshop.save()
    return webshop


@pytest.fixture(scope='class')
def webshops_suite(mock_aws, webshop, create_random_webshop, random_products):
    assert mock_aws
    random_shop = create_random_webshop()
    random_shop.save()
    webshop_products = []
    webshop_tags = []

    for product in random_products(random.randint(10, 20)):
        webshop_product = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
        webshop_product.save()
        webshop_products.append(webshop_product)
        WebshopProduct(product_guid=product.guid, webshop_guid=random_shop.guid).save()

    for i in range(random.randint(5, 20)):
        tag = Tag(name='-'.join(fake.words(2)), type=Tag.Type.CATEGORY if i % 2 == 0 else Tag.Type.CAMPAIGN, owner=webshop.guid)
        tag.save()
        webshop_tags.append(tag)
        Tag(name='-'.join(fake.words(2)), type=Tag.Type.CATEGORY if i % 2 == 0 else Tag.Type.CAMPAIGN, owner=random_shop.guid).save()

    return dict(webshop=webshop, webshop_products=webshop_products, webshop_tags=webshop_tags)


def mock_process_log(start_date='', end_date='', event_type='', webshop_guid=''):
    file_path = os.path.join(os.path.dirname(__file__), 'data/order_logs.json')
    with open(file_path) as json_file:
        data = json.load(json_file)
        return shop_specific_order_log_to_dataframe(data)


def mock_process_log_for_admin(start_date='', end_date='', event_type='', webshop_guid=''):
    file_path = os.path.join(os.path.dirname(__file__), 'data/order_logs.json')
    with open(file_path) as json_file:
        data = json.load(json_file)
        return all_order_log_to_dataframe(data)


def mock_process_product_for_shop(webshop_guid=''):
    product_stat = {
        'for_sale_product_count': {'value': 1524},
        'product_count': {'value': 1525},
        'active_product_count': {'value': 104}
    }
    return product_stat


def mock_process_product_for_admin(webshop_guid='', result_size=''):
    product_stat = {
        'for_sale_product_count': {'value': 21805},
        'product_count': {'value': 21806},
        'active_product_count': {'value': 230}
    }
    return product_stat


def mock_time_aggs_for_all_product():
    return dict()


def start_date():
    return datetime.date(2020, 1, 20)


def end_date():
    return datetime.date(2020, 2, 10)


def mock_dates(**kwargs):
    return dict(
        start_date=start_date(),
        end_date=end_date()
    )


@pytest.fixture(scope='class')
def order_stat_webshops(mock_aws, webshop, create_random_webshop, create_random_product):
    product = create_random_product(guid=uuid.UUID('4f95ebe3-f27c-5cee-9296-860bceb4832a'))
    product.save()
    webshop_product = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
    webshop_product.save()
    return dict(
        webshop=webshop,
        webshop_product=webshop_product
    )


@pytest.fixture(scope='class')
def shopkeeper_identifiers():
    guid = uuid.uuid4()
    identity_id = f'eu-west-1:{guid}'
    return dict(guid=guid, identity_id=identity_id)


@pytest.fixture(scope='class')
def as_webshop_specific_shopkeeper(create_auth_client, shopkeeper_identifiers, webshop, faker):
    user_data = UserData(guid=shopkeeper_identifiers['guid'], identity_id=shopkeeper_identifiers['identity_id'])
    user_data.save()

    yield create_auth_client(**{
        "custom:organization_guid": str(webshop.guid),
        'cognito:groups': ['shopkeeper'],
        'cognito:username': str(shopkeeper_identifiers['guid']),
        'email': faker.email(),
    })

    user_data.delete()


@pytest.mark.usefixtures('mock_aws')
class TestStatistics:
    @patch('gwio.api.statistics.process_log', new=mock_process_log_for_admin)
    @patch('gwio.api.statistics.set_dates', new=mock_dates)
    def test_statistics_for_admin(self, as_admin):
        res = as_admin.get(f'/statistics/order_statistics?start_date={start_date()}&end_date={end_date()}')
        assert 200 == res.status_code
        assert res.json == order_statistics_for_admin

    @patch('gwio.api.statistics.query_webshop_product', new=mock_process_product_for_admin)
    def test_product_statistics(self, as_admin):
        res = as_admin.get(f'/statistics/product_statistics')
        assert 200 == res.status_code
        res.json.pop('webshops_product_stats')
        assert res.json == webshops_product_stat_for_admin

    @patch('gwio.api.statistics.process_log', new=mock_process_log_for_admin)
    @patch('gwio.api.statistics.set_dates', new=mock_dates)
    def test_top_sold_products_for_admin(self, as_admin):
        res = as_admin.get(f'/statistics/top_sold_products?count={3}&start_date={start_date()}&end_date={end_date()}')
        assert 200 == res.status_code
        assert res.json == top_sold_product_for_admin

    @patch('gwio.api.statistics.time_aggs_for_all_product', new=mock_time_aggs_for_all_product)
    def test_products_time_stats_for_admin(self, as_admin):
        res = as_admin.get(f'/statistics/product_time_stats')
        assert 200 == res.status_code


@pytest.mark.usefixtures('mock_aws')
class TestShopStatistics:
    def test_shop_basic_statistics(self, as_webshop_specific_shopkeeper, webshops_suite, cognitoidp_suite):
        shop = webshops_suite['webshop']
        with pytest.raises(KeyError):  # Moto doesn't support 'EstimatedNumberOfUsers' for describe_user_pool, so this is a stupid test for now.
            as_webshop_specific_shopkeeper.get(f'/webshops/{shop.guid}/statistics')

        # res = as_admin.get(f'/webshops/{shop.guid}/statistics')
        # assert 200 == res.status_code
        # assert len(webshops_suite['webshop_products']) == res.json['total_products_count']
        # assert len(webshops_suite['webshop_tags']) == res.json['total_tags_count']
        # webshop_category_tags = len([x for x in webshops_suite['webshop_tags'] if x.type == TagType.CATEGORY])
        # assert webshop_category_tags == res.json['total_categories_count']

    @patch('gwio.api.statistics.process_log', new=mock_process_log)
    @patch('gwio.api.statistics.set_dates', new=mock_dates)
    def test_order_statistics(self, as_shopkeeper, as_webshop_specific_shopkeeper, order_stat_webshops):
        shop = order_stat_webshops['webshop']
        res = as_webshop_specific_shopkeeper.get(f'/webshops/{shop.guid}/statistics/orders?start_date={start_date()}&end_date={end_date()}')
        assert 200 == res.status_code
        assert res.json == webshop_order_stat
        # try to access this endpoint as random shopkeeper
        res = as_shopkeeper.get(f'/webshops/{shop.guid}/statistics/orders')
        assert 403 == res.status_code

    @patch('gwio.api.statistics.process_log', new=mock_process_log)
    @patch('gwio.api.statistics.set_dates', new=mock_dates)
    def test_top_sold_products(self, as_shopkeeper, as_webshop_specific_shopkeeper, order_stat_webshops):
        shop = order_stat_webshops['webshop']
        res = as_webshop_specific_shopkeeper.get(f'/webshops/{shop.guid}/statistics/top_sold_products?count={3}&end_date={end_date()}')
        assert 200 == res.status_code
        assert res.json == webshop_top_sold_products
        # try to access this endpoint as random shopkeeper
        res = as_shopkeeper.get(f'/webshops/{shop.guid}/statistics/top_sold_products?count={3}')
        assert 403 == res.status_code

    @patch('gwio.api.statistics.process_log', new=mock_process_log)
    @patch('gwio.api.statistics.set_dates', new=mock_dates)
    def test_ordered_product_statistics(self, as_shopkeeper, as_webshop_specific_shopkeeper, order_stat_webshops):
        shop_guid = order_stat_webshops['webshop_product'].webshop_guid
        product_guid = order_stat_webshops['webshop_product'].product_guid
        res = as_webshop_specific_shopkeeper.get(f'/webshops/{shop_guid}/statistics/products/{product_guid}?start_date={start_date()}&end_date={end_date()}')
        assert 200 == res.status_code
        assert res.json == product_sold_stat_over_time
        # try to access this endpoint as random shopkeeper
        res = as_shopkeeper.get(f'/webshops/{shop_guid}/statistics/products/{product_guid}')
        assert 403 == res.status_code

    @patch('gwio.api.statistics.query_webshop_product', new=mock_process_product_for_shop)
    def test_product_statistics(self, as_shopkeeper, as_webshop_specific_shopkeeper, order_stat_webshops):
        shop_guid = order_stat_webshops['webshop_product'].webshop_guid
        res = as_webshop_specific_shopkeeper.get(f'/webshops/{shop_guid}/statistics/product_statistics')
        assert 200 == res.status_code
        assert res.json == webshop_specific_product_stats
        # try to access this endpoint as random shopkeeper
        res = as_shopkeeper.get(f'/webshops/{shop_guid}/statistics/product_statistics')
        assert 403 == res.status_code
