from unittest import TestCase

import werkzeug
from flask import Flask, Response, jsonify, json
from flask_apispec import marshal_with
from marshmallow import fields
from werkzeug.datastructures import ResponseCacheControl

from gwio.cache.flask_http import cached, parse_age
from gwio.ext.marshmallow import GwioSchema

app = Flask('testapp')


def parse_cc(response):
    header = response.headers['Cache-Control']
    return werkzeug.http.parse_cache_control_header(header, cls=ResponseCacheControl)


class TestParseAge(TestCase):
    def test_parsing(self):
        assert parse_age(30) == 30
        assert parse_age('5m') == 300
        assert parse_age('60m') == parse_age('1h')
        assert parse_age('24h') == parse_age('1d')
        assert parse_age('7d') == parse_age('1w')


class TestCacheDecorator(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app.test_client()
        cls.route = app.route

    def test_number(self):
        @app.route('/x')
        @cached(300)
        def cache_test():
            return jsonify(dict())

        resp = self.app.get('/x')
        cc = parse_cc(resp)
        assert cc.max_age == 300
        assert cc.public

    def test_private(self):
        @app.route('/y')
        @cached(1, private=True)
        def cache_test2():
            return Response("foo")

        resp = self.app.get('/y')
        cc = parse_cc(resp)
        assert cc.private
        assert not cc.public

    def test_nocache(self):
        @app.route('/z')
        @cached(False)
        def cache_test3():
            return Response("foo")

        resp = self.app.get('/z')
        cc = parse_cc(resp)
        assert cc.no_cache
        assert cc.no_store
        assert cc.must_revalidate
        assert not cc.max_age

    def test_with_marshmallow(self):
        class FooSchema(GwioSchema):
            foo = fields.Str()

        @app.route('/å')
        @cached('10m')
        @marshal_with(FooSchema())
        def cache_test4():
            return dict(foo="bar")

        resp = self.app.get('/å')
        cc = parse_cc(resp)
        assert cc.max_age == 60 * 10
        assert resp.content_type == 'application/json'
        assert json.loads(resp.data.decode('utf-8')) == dict(foo='bar')
