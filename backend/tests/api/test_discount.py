# -*- coding: utf-8 -*-
import random
from decimal import Decimal

import pytest
from attrdict import AttrDict

from gwio.logic.orders.types import OrderState
from gwio.models.modifiers import (
    ModifierLineSource,
    ModifierMethodType,
    _method_conversions,
)
from gwio.models.products import Product
from gwio.models.webshop_product import WebshopProduct


@pytest.fixture(scope='class')
def ordered_products(random_products):
    return [product for product in random_products(random.randrange(2, 5))]


@pytest.fixture(scope='class')
def webshop_products(ordered_products, webshop_guid):
    return [WebshopProduct(webshop_guid=webshop_guid, product_guid=product.guid) for product in ordered_products]


@pytest.fixture(scope='class')
def shop_order(webshop_guid, ordered_products, create_test_order, random_identity_id):
    mapped = Product.API.Assessor.EvaluatedProduct.ProductLine.mapped
    product_lines = list()
    for product in ordered_products:
        ep = mapped(product.API.Assessor.evaluate(product, qty=Decimal(1)))
        ep.price.final = AttrDict(unit=ep.price.original.unit, total=ep.price.original.total)
        product_lines.append(ep)
    order = create_test_order(product_lines=product_lines,
                              _current_state=OrderState.CREATED,
                              shop_guid=webshop_guid,
                              buyer_identity_id=random_identity_id())
    yield order


@pytest.fixture(scope='class')
def test_suite(mock_aws, webshop, ordered_products, webshop_products, shop_order):
    assert mock_aws
    webshop.save()
    for item in ordered_products + webshop_products:
        item.save()
    shop_order.save()


@pytest.mark.usefixtures('test_suite')
class TestDiscount:
    @pytest.mark.parametrize('method', list(ModifierMethodType))
    def test_add_discount(self, as_shopkeeper, shop_order, ordered_products, faker, method):
        product = random.choice(ordered_products)
        data = [dict(
            desc=faker.sentence(),
            method=method.value,
            amount=str(random.randint(-4000, 4000) / 100)
        ) for _ in range(random.randint(1, 5))]
        resp = as_shopkeeper.put(
            f'/webshops/{shop_order.shop_guids[0]}/orders/{shop_order.guid}/product_lines/{product.guid}/modifiers/',
            json=data)
        assert resp.status_code == 200, resp.json['message']
        shop_order.refresh()
        assert len(data) == len(next(x.price.original.modifiers for x in shop_order.product_lines if x.guid == product.guid))
        res_order_line = next(x for x in resp.json['lines'] if x['guid'] == str(product.guid))
        # Random data so just check that number of lines match
        assert len(data) == len(res_order_line['price']['original']['modifiers'])
        assert all(ModifierLineSource.MANUAL == ModifierLineSource(x['source']) for x in res_order_line['price']['original']['modifiers'])

        for sent_modifier in data:
            res_modifier = next(x for x in res_order_line['price']['original']['modifiers'] if x['desc'] == sent_modifier['desc'])
            try:
                assert _method_conversions[method].value == res_modifier['method']
            except KeyError:
                assert all(y == res_modifier[x] for x, y in sent_modifier.items())

            price_attributes = ('amount', 'currency', 'vat', 'vat0')
            # TODO: Should have a test that checks the values and then this is not needed
            assert all(res_modifier['fixed_modifiers'][x][y] is not None for x in ('unit', 'total') for y in price_attributes)

    def test_discount_order_total(self, as_shopkeeper, shop_order, ordered_products):
        product = random.choice(ordered_products)
        data = [dict(desc='Test discount',
                     method=ModifierMethodType.FIXED_TOTAL.value,
                     amount=2.4)]
        resp = as_shopkeeper.put(
            f'/webshops/{shop_order.shop_guids[0]}/orders/{shop_order.guid}/product_lines/{product.guid}/modifiers/',
            json=data)
        assert resp.status_code == 200, resp.json['message']
        resp = as_shopkeeper.get(f'/webshops/{shop_order.shop_guids[0]}/orders/{shop_order.guid}')
        assert resp.status_code == 200, resp.json['message']
