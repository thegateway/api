import uuid

import pytest
from flask import (
    g,
    make_response,
)

from gwio.core import rbac
from gwio.core.decorators.api import (
    GET,
)
from gwio.core.utils.testing._fixtures.as_role import ISS_PYTEST
from gwio.models import user

UserData = user.UserData


@pytest.fixture(scope='class')
@pytest.mark.usefixtures('mock_aws')
def user_data():
    data = UserData(guid=uuid.uuid4(), pin='020202-YYYY')
    data.save()
    return data


@pytest.mark.usefixtures('mock_aws')
def test_user_pin(app, user_data):
    @app.application.register_endpoint(
        None,
        route='/testing',
        method=GET,
        input_schema=None,
        output_schema=None,
        authorisation=(rbac.customer,),
        description='',
    )
    def test_api():
        return make_response('', 200)

    token_payload = {
        'iss': ISS_PYTEST,
        'cognito:groups': ['customer'],
        'cognito:username': str(user_data.guid),
        'pin': '010101-XXXX',
    }
    assert user_data.pin is not None
    assert user_data.pin != token_payload['pin']  # Making sure token pin is different from user data pin to make sure g.user gets the pin from token
    with app.application.test_request_context():
        r = app._get(
            '/testing',
            token_payload=token_payload,
            content_type='application/json')
        assert 200 == r.status_code
        assert token_payload['pin'] == g.user.pin
