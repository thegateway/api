import pytest

from gwio import api


def mocked_add_to_organization(group_name, organization_guid, user_guid):
    return True


@pytest.fixture(autouse=True)
def ato_patched(monkeypatch):
    monkeypatch.setattr(api.webshops, 'add_to_organization', mocked_add_to_organization)
