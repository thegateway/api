import base64
import os

from envs import env
from flask import json
from jose import (
    jwk,
    jwt,
)
from jose.jws import get_unverified_claims

import gwio.api.flask_app
from gwio.core.utils.testing.helpers import FlaskTestClient

global app
app = None

SECRET = os.urandom(32)
JWK = jwk.get_key(jwk.ALGORITHMS.HS256)(SECRET, jwk.ALGORITHMS.HS256)
KID = 'foo'
REGION = env('AWS_REGION', 'eu-west-1')
USER_POOL_ID = gwio.api.flask_app.app.config['COGNITO_POOL_ID']
ISS_PYTEST = 'http://localhost/pytest'


class TestIssuer:
    @staticmethod
    def verify_token(token):
        return json.loads(get_unverified_claims(token).decode('utf-8'))


def setup_app():
    """Session-wide test `Flask` application."""
    if app is not None:
        return app

    key_dict = dict(
        kty="oct",
        k=base64.b64encode(SECRET).decode('ascii'),
        alg="HS256",
        kid=KID
    )
    settings_override = dict(
        TESTING=True,
        JWKS=dict(keys=[key_dict]),
    )

    gwio.api.flask_app.CONFIG = settings_override
    _app = gwio.api.flask_app.app
    _app.testing = True
    _app.test_client_class = FlaskTestClient
    _app.config.update(settings_override)
    _app.token_issuers[ISS_PYTEST] = TestIssuer()

    def auth_token(payload):
        return jwt.encode(payload, SECRET)

    _app.get_auth_token = auth_token
    # Establish an application context before running the tests.
    with _app.app_context():
        client = _app.test_client()
        return client


app = setup_app()
