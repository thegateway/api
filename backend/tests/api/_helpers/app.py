import base64
import os

import simplejson as json
from jose import (
    jwk,
    jwt,
)
from jose.jws import get_unverified_claims

from gwio.core.api import app as core_app
from gwio.core.utils.testing._fixtures.as_role import ISS_PYTEST
from gwio.core.utils.testing.helpers import FlaskTestClient

SECRET = os.urandom(32)
JWK = jwk.get_key(jwk.ALGORITHMS.HS256)(SECRET, jwk.ALGORITHMS.HS256)
KID = 'foo'
App = core_app.App

global app
app = None


class TestIssuer:
    @staticmethod
    def verify_token(token):
        return json.loads(get_unverified_claims(token).decode('utf-8'))


def setup_app():
    if app is not None:
        return app
    key_dict = dict(
        kty="oct",
        k=base64.b64encode(SECRET).decode('ascii'),
        alg="HS256",
        kid=KID
    )
    settings_override = dict(
        TESTING=True,
        JWKS=dict(keys=[key_dict]),
    )

    core_app.CONFIG = settings_override
    _app = core_app.app
    _app.testing = True
    _app.test_client_class = FlaskTestClient
    _app.config.update(settings_override)
    _app.token_issuers[ISS_PYTEST] = TestIssuer()

    def auth_token(payload):
        return jwt.encode(payload, SECRET)

    _app.get_auth_token = auth_token
    # Establish an application context before running the tests.
    with _app.app_context():
        client = _app.test_client()
        return client


app = setup_app()
