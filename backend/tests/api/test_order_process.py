import json
import random
import uuid
from decimal import Decimal

import pytest
from gwio.core.cw_logging import LogEventType

from gwio.api.dashboard.orders import REFUND_TOO_MUCH
from gwio.core.utils.testing._fixtures.aws import MockedEventsStore
from gwio.environment import Env
from gwio.logic.orders.types import (
    OrderState,
)
from gwio.logic.packages.handler import (
    PackageAction,
    PackageState,
)
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentState,
)
from gwio.logic.returns.handler import ReturnDeliveryState
from gwio.logic.returns.notifications import ReturnDeliveryReceivedUserNotification
from gwio.models.orders import Order
from gwio.models.payment_method import PaymentMethod
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.webshop_product import WebshopProduct
from gwio.user import User
from gwio.utils.fsm import FsmState
from ._fixtures.orders import valid_order_create_post_data


def get_existing_products_and_delivery_methods(webshop):
    products = []
    delivery_methods = []
    for wp in WebshopProduct.scan():
        # Try is to avoid issues with products having been deleted with webshop products being left in the database
        try:
            if wp.webshop_guid == webshop.guid:
                product = Product.by_guid(wp.product_guid)
                if product.type != PRODUCT_TYPE_DELIVERY_METHOD and product.owner_guid == webshop.guid:
                    products.append(product)
                elif product.type == PRODUCT_TYPE_DELIVERY_METHOD:
                    delivery_methods.append(product)
        except Product.DoesNotExist:
            pass

    return products, delivery_methods


@pytest.fixture(scope='class')
def test_suite(create_random_product, create_random_delivery_method, create_random_payment_method, webshop, delivery_method_webshop_guid,
               create_random_webshop):
    pm = create_random_payment_method(
        type=PaymentMethod.API.Type.STRIPE,
    )
    pm.save()

    delivery_method_webshop = create_random_webshop(guid=delivery_method_webshop_guid)
    delivery_method_webshop.save()

    dm = create_random_delivery_method(owner_guid=delivery_method_webshop_guid)
    dm.save()
    dmwsp = WebshopProduct(webshop_guid=webshop.guid, product_guid=dm.guid)
    dmwsp.save()

    products = []
    webshop_products = []
    for _ in range(random.randint(20, 30)):
        product = create_random_product(owner_guid=webshop.guid)
        product.save()
        products.append(product)
        wp = WebshopProduct(webshop_guid=webshop.guid, product_guid=product.guid)
        wp.save()
        webshop_products.append(wp)

    yield dict(products=products, payment_methods=[pm], delivery_methods=[dm])

    pm.delete()
    delivery_method_webshop.delete()
    dm.delete()
    dmwsp.delete()
    for p in products:
        p.delete()
    for wsp in webshop_products:
        wsp.delete()


@pytest.mark.skip(reason='Not supporting any non-prepayment payment methods yet')
@pytest.mark.usefixtures('test_suite')
def test_order_no_prepayment_pickup_payment_issues(mocker, faker, as_customer, webshop, customer_identity_guid, as_shopkeeper):
    products, delivery_methods = get_existing_products_and_delivery_methods(webshop)
    r = as_customer.post(f'/webshops/{webshop.guid}/orders/', json=Order.Schemas.Post().dump(valid_order_create_post_data(products=products)))
    assert r.status_code == 201, r.json

    order = Order.get(uuid.UUID(r.json['guid']))
    assert OrderState(r.json['state']) == OrderState.CREATED

    update_data = dict(
        payment_method=dict(
            guid=next(x.guid for x in PaymentMethod.scan() if x.type == PaymentMethod.API.Type.INVOICE),
            card='a',
        ),
        delivery_method=random.choice(delivery_methods).guid,
        customer=dict(
            name=faker.name(),
            phone=faker.phone_number(),
        ),
    )
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}', json=Order.Schemas.Put().dump(update_data))
    assert r.status_code == 200, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED

    # When this was written, there was no support for non-prepayment order
    order.refresh()
    order.flags.prepayment = False
    order.save()

    # When this was written, there was no support for pickup delivery method
    # mocker.patch.object(Order, 'status_pickup', True)

    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}/_place')
    assert r.status_code == 200, r.json
    if OrderState(r.json['state']) == OrderState.WAITING_REVIEW:
        r = as_shopkeeper.put(f'/webshops/{webshop.guid}/orders/{order.guid}/_accept')
    assert r.status_code == 200, r.json

    assert PackageState(r.json['packages'][0]['state']) == PackageState.WAITING_PACKING
    package_guid = r.json['packages'][0]['guid']
    r = as_shopkeeper.put(f'/webshops/{webshop.guid}/orders/{order.guid}/packages/{package_guid}/_{PackageAction.PACK}')
    assert r.status_code == 200, r.json
    assert PackageState(r.json['packages'][0]['state']) == PackageState.WAITING_DELIVERY

    # Testing automated payment capture failure
    r = as_shopkeeper.put(f'/webshops/{webshop.guid}/orders/{order.guid}/packages/{package_guid}/_{PackageAction.CONFIRM}')
    assert r.status_code == 200, r.json
    assert PackageState(r.json['packages'][0]['state']) == PackageState.DELIVERED
    assert order.status_paid == False
    assert PaymentState(r.json['payment']['state']) == PaymentState.WAITING_PAYMENT

    # Manual insufficient payment should not change order state
    # When this was written, there was no support for manually triggering payment capture
    order.refresh()
    payment = order.payments[0]
    payment.process(PaymentAction.PAY)
    order.save()
    assert order.status_paid == False
    assert order.current_state == OrderState.COMPLETED
    assert payment.current_state == PaymentState.WAITING_PAYMENT


def check_order_place_event_in_produced_events():
    return any([f'"event_type": "{LogEventType.ORDER_PLACE.value}"' in item['Detail'] for item in MockedEventsStore._events])


def test_create_place_order(mock_stripe, faker, webshop, as_customer, as_shopkeeper, test_customer, customer_identity_guid, test_suite, stripe_payment_method):
    data = dict(customer_name=faker.name(),
                products=[dict(guid=str(p.guid), qty=random.randint(1, 5), shop_guid=str(p.owner_guid)) for p in
                          random.choices(test_suite['products'], k=random.randint(3, 8))])
    r = as_customer.post(f'/customers/{customer_identity_guid}/orders/', json=data)
    assert 201 == r.status_code, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED
    order_guid = r.json['guid']

    update_data = dict(
        payment_method=dict(
            guid=str(stripe_payment_method.guid),
            card='a',
        ),
        delivery_method=str(random.choice(test_suite['delivery_methods']).guid),
        customer=dict(
            name=faker.name(),
            phone=faker.phone_number(),
        ),
    )
    assert not check_order_place_event_in_produced_events()  # Assert event_type LogEventType.ORDER_PLACE hasn't created at this stage yet

    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order_guid}', json=update_data)
    assert r.status_code == 200, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED
    assert not check_order_place_event_in_produced_events()  # Assert event_type LogEventType.ORDER_PLACE hasn't created at this stage yet

    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order_guid}/_place')
    assert r.status_code == 200, r.json
    assert check_order_place_event_in_produced_events()  # Assert event_type LogEventType.ORDER_PLACE has created at this stage


def test_create_place_order_sca_required(mock_stripe, faker, webshop, as_customer, as_shopkeeper, as_anonymous, test_customer, customer_identity_guid,
                                         test_suite, stripe_payment_method, stripe_webhook_payload):
    # Force source action
    mock_stripe['mock_payment_intent'].confirm_response = mock_stripe['mock_payment_intent'].confirm_responses['requires_action']

    data = dict(customer_name=faker.name(),
                products=[dict(guid=str(p.guid), qty=random.randint(1, 5), shop_guid=str(p.owner_guid)) for p in
                          random.choices(test_suite['products'], k=random.randint(3, 8))])
    r = as_customer.post(f'/customers/{customer_identity_guid}/orders/', json=data)
    assert 201 == r.status_code, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED
    order_guid = r.json['guid']

    update_data = dict(
        payment_method=dict(
            guid=str(stripe_payment_method.guid),
            card='a',
        ),
        delivery_method=str(random.choice(test_suite['delivery_methods']).guid),
        customer=dict(
            name=faker.name(),
            phone=faker.phone_number(),
        ),
    )
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order_guid}', json=update_data)
    assert r.status_code == 200, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED
    order_amount = int(r.json['summary']['price'] * 100)

    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order_guid}/_place')
    assert 200 == r.status_code, r.json
    assert OrderState(r.json['state']) == OrderState.PROCESSING
    assert PaymentState(r.json['payment']['state']) == PaymentState.WAITING_SCA
    assert not r.json['packages']
    assert r.json.get('payment', dict()).get('client_secret') is not None
    assert r.json.get('payment', dict()).get('intent_id') is not None

    payload, headers = stripe_webhook_payload(stripe_key=Env.STRIPE_ENDPOINT_SECRET,
                                              order_guid=order_guid,
                                              amount=order_amount,
                                              intent_type='payment_intent.amount_capturable_updated',
                                              amount_capturable=order_amount)
    r = as_anonymous.post('/stripe/webhook', data=payload, headers=headers)
    assert 200 == r.status_code, r.json

    order = Order.get(guid=uuid.UUID(order_guid))
    assert order.current_state == OrderState.PROCESSING
    assert order.payments[0].current_state == PaymentState.COMPLETED
    assert order.packages[0].current_state == PackageState.WAITING_PACKING


@pytest.mark.usefixtures('test_suite', 'mock_stripe')
def test_order_place_reduce_stock_levels(as_customer, webshop, customer_identity_guid, faker, stripe_payment_method):
    # Setting up for the actual test step
    products, delivery_methods = get_existing_products_and_delivery_methods(webshop)
    r = as_customer.post(f'/customers/{customer_identity_guid}/orders/',
                         json=Order.Schemas.Post().dump(valid_order_create_post_data(products=products, webshop_guid=webshop.guid)))
    assert r.status_code == 201, r.json

    order = Order.get(guid=uuid.UUID(r.json['guid']))
    assert OrderState(r.json['state']) == OrderState.CREATED

    update_data = dict(
        payment_method=dict(
            guid=str(stripe_payment_method.guid),
            card='a',
        ),
        delivery_method=random.choice(delivery_methods).guid,
        customer=dict(
            name=faker.name(),
            phone=faker.phone_number(),
        ),
    )
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}', json=Order.Schemas.Put().dump(update_data))
    assert r.status_code == 200, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED

    # Collecting the stock levels before they are updated
    stock_levels = dict()
    for product_line in order.product_lines:
        wp = WebshopProduct.get(product_guid=product_line.guid, webshop_guid=webshop.guid)
        stock_levels[product_line.guid] = dict(
            wp=wp,
            stock=wp.stock_level,
        )

    # Trigger stock levels update by placing the order
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}/_place')
    assert r.status_code == 200, r.json
    assert OrderState(r.json['state']) not in (OrderState.CREATED, FsmState._FAILED)

    # Confirming the stock levels actually changed
    order.refresh()
    product_lines = [x for x in order.product_lines if x.type != PRODUCT_TYPE_DELIVERY_METHOD]
    assert product_lines
    for product_line in product_lines:
        wp = stock_levels[product_line.guid]['wp']
        wp.refresh()
        assert wp.stock_level == stock_levels[product_line.guid]['stock'] - product_line.qty


@pytest.mark.usefixtures('mock_stripe')
def test_order_product_return(faker, mocker, create_order_via_api, as_customer, as_shopkeeper, customer_identity_guid, webshop_guid):
    # Setup
    order = create_order_via_api(
        payment_method=dict(type=PaymentMethod.API.Type.STRIPE),
    )
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}/_place')

    assert 200 == r.status_code, r.json
    order.refresh()
    assert len(order.payments) == 1

    # Customer reports desire to return products
    product_lines = [x for x in order.product_lines if x.type != PRODUCT_TYPE_DELIVERY_METHOD]
    return_product_lines = [random.choice(product_lines) for _ in range(2)]
    qty_shop = {x.guid: dict(qty=random.randint(2, x.qty - 1), shop_guid=x.shop_guid) for x in return_product_lines}
    product_return_data = dict(
        lines=[dict(product_guid=product_guid, **data) for product_guid, data in qty_shop.items()],
        reason=' '.join(faker.sentences(nb=5)),
    )
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}/product_return', json=product_return_data)
    assert 200 == r.status_code, r.json
    order.refresh()
    assert len(order.payments) == 1
    product_return = order.product_returns[0]
    assert product_return.reason == product_return_data['reason']
    assert r.json['product_returns'][0]['guid']
    assert r.json['product_returns'][0]['reason'] == product_return_data['reason']

    # TODO: No support for accepting the return request yet
    # assert order.product_returns[0].state == ReturnDeliveryState.NOT_STARTED
    assert order.product_returns[0].current_state == ReturnDeliveryState.WAITING_RETURN

    def _received_qtys(order):
        returned_qtys = dict()
        for product_return in order.product_returns:
            for return_line in product_return.lines:
                returned_qtys[return_line.product_guid] = sum([x.qty for x in return_line.received])
        return returned_qtys

    # Confirming less than the customer reported (as customer can send in multiple shipments, make a mistake, or try to scam)
    confirm_return_data = dict(
        received=[dict(product_guid=x['product_guid'], qty=1) for x in product_return_data['lines']],
    )
    r = as_shopkeeper.put(f'/dashboard/webshops/{webshop_guid}/orders/{order.guid}/product_return/{product_return.guid}', json=confirm_return_data)
    assert 200 == r.status_code, r.json
    order.refresh()
    assert len(order.product_returns) == 1
    returned_qtys = _received_qtys(order)
    for confirmed_return_line in confirm_return_data['received']:
        assert returned_qtys[confirmed_return_line['product_guid']] == confirmed_return_line['qty']

    assert order.product_returns[0].current_state == ReturnDeliveryState.PARTIALLY_RECEIVED

    # Confirming the rest of what the customer reported
    confirm_return_data2 = dict(
        received=[dict(product_guid=x['product_guid'], qty=x['qty'] - 1) for x in product_return_data['lines']],
    )
    spy = mocker.spy(User, 'notify')
    r = as_shopkeeper.put(f'/dashboard/webshops/{webshop_guid}/orders/{order.guid}/product_return/{product_return.guid}', json=confirm_return_data2)
    assert 200 == r.status_code, r.json
    order.refresh()
    assert len(order.product_returns) == 1
    returned_qtys = _received_qtys(order)
    for product_guid, data in qty_shop.items():
        assert returned_qtys[product_guid] == data['qty']

    assert order.product_returns[0].current_state == ReturnDeliveryState.RECEIVED

    called = False
    for mock_call in spy.mock_calls:
        call_args = mock_call[1]
        if str(call_args[1]) == ReturnDeliveryReceivedUserNotification.__name__:
            called = True
            break
    assert called, 'User was not notified about the return delivery having been received'

    # Confirming more than the customer reported (as customer might report too little accidentally)
    confirm_return_data3 = dict(
        received=[dict(product_guid=x['product_guid'], qty=1) for x in product_return_data['lines']],
    )
    r = as_shopkeeper.put(f'/dashboard/webshops/{webshop_guid}/orders/{order.guid}/product_return/{product_return.guid}', json=confirm_return_data3)
    assert 200 == r.status_code, r.json
    order.refresh()
    assert len(order.product_returns) == 1
    returned_qtys = _received_qtys(order)
    for confirmed_return_line in confirm_return_data3['received']:
        assert returned_qtys[confirmed_return_line['product_guid']] == confirmed_return_line['qty'] + qty_shop[confirmed_return_line['product_guid']]['qty']

    assert order.product_returns[0].current_state == ReturnDeliveryState.RECEIVED

    # Try to refund more than the customer paid
    refund_too_much_data = dict(
        amount=order.summary.price + Decimal('1')
    )
    r = as_shopkeeper.put(f'/dashboard/webshops/{webshop_guid}/orders/{order.guid}/payments/_refund', json=refund_too_much_data)
    assert 409 == r.status_code, r.json
    assert REFUND_TOO_MUCH in r.json['message']
    order.refresh()
    assert len(order.payments) == 1  # Only the payment so far

    # Refund the money
    refund_data = dict(
        amount=1
    )
    r = as_shopkeeper.put(f'/dashboard/webshops/{webshop_guid}/orders/{order.guid}/payments/_refund', json=refund_data)
    assert 200 == r.status_code, r.json
    order.refresh()
    assert len(order.payments) == 2  # Payment and refund
    refund = order.payments[-1]
    assert refund.amount == refund_data['amount'] * -1
    assert json.loads(order.payments[-1].data.refund)['amount'] == int(refund_data['amount'] * 100)
    assert refund.current_state == PaymentState.COMPLETED


@pytest.fixture(scope='function')
def zero_commission_delivery_method(create_random_delivery_method, create_random_webshop):
    webshop = create_random_webshop()
    webshop._private_data.stripe.commission_percentage = 0
    webshop.save()
    delivery_method = create_random_delivery_method(owner_guid=webshop.guid)
    delivery_method.save()
    yield delivery_method
    delivery_method.delete()
    webshop.delete()


def test_zero_commission(mock_stripe, faker, webshop, as_customer, as_shopkeeper, test_customer, customer_identity_guid, test_suite, stripe_payment_method,
                         zero_commission_delivery_method):
    data = dict(customer_name=faker.name(),
                products=[dict(guid=str(p.guid), qty=random.randint(1, 5), shop_guid=str(p.owner_guid)) for p in
                          random.choices(test_suite['products'], k=random.randint(3, 8))])
    r = as_customer.post(f'/customers/{customer_identity_guid}/orders/', json=data)
    assert 201 == r.status_code, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED
    order_guid = r.json['guid']

    update_data = dict(
        payment_method=dict(
            guid=str(stripe_payment_method.guid),
            card='a',
        ),
        delivery_method=zero_commission_delivery_method.guid,
        customer=dict(
            name=faker.name(),
            phone=faker.phone_number(),
        ),
    )
    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order_guid}', json=update_data)
    assert r.status_code == 200, r.json
    assert OrderState(r.json['state']) == OrderState.CREATED

    r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order_guid}/_place')
    assert r.status_code == 200, r.json
    assert r.status_code == 200, r.json

    order = Order.get(guid=order_guid)
    try:
        delivery_method_transfer = next(transfer for transfer in order.payments[-1].data.transfers if
                                        uuid.UUID(transfer['metadata']['webshop_guid']) == zero_commission_delivery_method.owner_guid)
        assert int(delivery_method_transfer['amount']) == 0
    except StopIteration:
        # If the delivery fee is the only product from the webshop, then there is no transfer for the webshop
        # This shouldn't ever be a case in production, but (at least currently) it is possible in tests
        pass
    delivery_method = next(line for line in r.json['lines'] if uuid.UUID(line['shop_guid']) == zero_commission_delivery_method.owner_guid)

    payment = order.payments[-1]
    distributions = order.payments[-1].money_distribution
    # The platform keeps all the delivery method money since the delivery company bills platform owner
    assert distributions['platform']['delivery_fee']['amount'] == Decimal(str(delivery_method['price']['final']['total']['amount']))
    # Making sure payment distributions don't add to more than the payment total
    assert payment.amount == sum([x['total'] for x in distributions['shops'].values()]) + distributions['platform']['delivery_fee']['amount']
