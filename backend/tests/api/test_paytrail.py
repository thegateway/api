import uuid
from decimal import Decimal
from hashlib import sha256

import pytest
from attrdict import AttrDict
from pytest import fixture
from pytest_lazyfixture import LazyFixture

from gwio.core.utils.testing.helpers import THE_ONE_CURRENCY
from gwio.ext import vat
from gwio.ext.vat import Currency
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.logic.packages.handler import PackageState
from gwio.logic.payments.handler import PaymentState
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.models.orders import Order
from gwio.models.products.base import (
    PRODUCT_TYPE_DEFAULT,
    PRODUCT_TYPE_DELIVERY_METHOD,
)
from gwio.models.user import UserData


@fixture(scope='function')
def test_order(mock_aws, create_test_order, customer_identity_id, create_random_payment_method, random_webshop, create_random_product):
    assert mock_aws
    payment_method = create_random_payment_method(type=PaymentTypes.PAYTRAIL)
    payment_method.save()
    random_webshop._private_data = dict(paytrail=dict(hash='e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', id=str(uuid.uuid4())))
    random_webshop.save()
    order = create_test_order(buyer_identity_id=customer_identity_id,
                              statement_descriptor='Moneys gone',
                              payment_method_guid=payment_method.guid,
                              shop_guid=random_webshop.guid,
                              _current_state=OrderState.CREATED,
                              data=dict(paytrail=dict(urls=dict(success='https://success.local', cancel='https://cancel.local'))))
    for line in order.product_lines:
        product = create_random_product()
        product.save()
        line.guid = product.guid
    order.save()
    yield order
    order.delete()
    payment_method.delete()
    random_webshop.delete()


@fixture(scope='function')
def other_test_order(mock_aws, create_test_order, create_random_payment_method, random_webshop, random_identity_id):
    assert mock_aws
    payment_method = create_random_payment_method(type=PaymentTypes.PAYTRAIL)
    payment_method.save()
    random_webshop._private_data = dict(paytrail=dict(hash='e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', id=str(uuid.uuid4())))
    random_webshop.save()
    order = create_test_order(buyer_identity_id=random_identity_id(),
                              statement_descriptor='Moneys gone',
                              payment_method_guid=payment_method.guid,
                              shop_guid=random_webshop.guid,
                              data=dict(paytrail=dict(urls=dict(success='https://success.local', cancel='https://cancel.local'))))
    order.save()
    yield order
    order.delete()


@fixture(scope='function')
def customer(mock_aws, test_customer, customer_identity_id):
    assert mock_aws
    test_customer.save()
    yield test_customer
    test_customer.delete()


@pytest.mark.skip('Paytrail is not currently supported')
@pytest.mark.usefixtures('mock_aws')
class TestPaytrail:
    @pytest.mark.parametrize(
        "user, order, expected_status",
        [
            (pytest.lazy_fixture('customer'), pytest.lazy_fixture('test_order'), 200),
            (AttrDict(identity_id=f'eu-west-1:{uuid.uuid4()}'), pytest.lazy_fixture('test_order'), 404),
            (pytest.lazy_fixture('customer'), AttrDict(guid=uuid.uuid4()), 404),
            (pytest.lazy_fixture('customer'), pytest.lazy_fixture('other_test_order'), 404)
        ],
        ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_paytrail_form_data(self, as_customer, user, order, expected_status):
        res = as_customer.get(f'/customers/{user.identity_id.split(":")[1]}/orders/{order.guid}/paytrail/')
        assert expected_status == res.status_code
        if expected_status == 200:
            order = Order.get(guid=order.guid)
            assert 0 == len(order.payments)

    def test_paytrail_url_success_and_url_cancel_after_order_update(self, customer, as_customer, test_order, faker):
        urls = {
            'url_success': faker.url(),
            'url_cancel': faker.url()
        }
        data = {
            'payment_method': {
                'guid': str(test_order.payment_method_guid),
                'urls': {
                    'success': urls['url_success'],
                    'cancel': urls['url_cancel']}}}
        res = as_customer.put(f'/customers/{test_order.buyer_identity_id.split(":")[1]}/orders/{test_order.guid}', json=data)
        assert 200 == res.status_code
        resp_data = as_customer.get(f'/customers/{test_order.buyer_identity_id.split(":")[1]}/orders/{test_order.guid}/paytrail/').json
        assert 200 == res.status_code
        for key, val in urls.items():
            assert resp_data['paytrail_form_data'][key.upper()] in val

    def test_receive_paytrail_notify(
            self,
            app,
            as_anonymous,
            customer_guid,
            customer_identity_id,
            random_webshop,
            create_random_payment_method,
            create_test_order,
            create_random_product,
            create_random_delivery_method,
    ):
        user = UserData(guid=customer_guid, notifications={})
        user.identity_id = customer_identity_id
        user.save()
        random_webshop._private_data = dict(paytrail=dict(hash='6pKF4jkv97zmqBJ3ZL8gUw5DfT2NMQ', id='13466'))
        random_webshop.save()

        payment_method = create_random_payment_method(type=PaymentTypes.PAYTRAIL)
        payment_method.save()
        delivery_method = create_random_delivery_method()
        delivery_method.save()
        product = create_random_product()
        product.save()

        price_data = dict(
            currency=THE_ONE_CURRENCY,
            vat_class=Decimal('10'),
            final=dict(
                unit=vat.VatPrice.from_price_and_vat("1 EUR", vat=2),
                total=vat.VatPrice.from_price_and_vat("1 EUR", vat=2),
            ),
            original=dict(
                unit=vat.VatPrice.from_price_and_vat("1 EUR", vat=2),
                total=vat.VatPrice.from_price_and_vat("1 EUR", vat=2),
            ),
        )

        order = create_test_order(
            guid=uuid.UUID('f10522df-f106-466b-a06a-ef049c941098'),
            _current_state=OrderState.CREATED,
            shop_guid=random_webshop.guid,
            buyer_identity_id=user.identity_id,
            summary=dict(price=Decimal('2.4'),
                         vat0_price=Decimal('0'),
                         currency=Currency.EUR),
            statement_descriptor='Moneys gone',
            payment_method_guid=payment_method.guid,
            product_lines=[
                dict(
                    guid=product.guid,
                    name=product.name,
                    qty=Decimal('1'),
                    type=PRODUCT_TYPE_DEFAULT,
                    price=price_data,
                ),
                dict(
                    guid=delivery_method.guid,
                    name=delivery_method.name,
                    qty=Decimal('1'),
                    type=PRODUCT_TYPE_DELIVERY_METHOD,
                    price=price_data,
                ),
            ],
            data=dict(paytrail=dict(urls=dict(success='https://success.local', cancel='https://cancel.local')))
        )
        for line in order.product_lines:
            product = create_random_product()
            product.save()
            line.guid = product.guid
        merchant_hash = random_webshop._private_data['paytrail']['hash']
        params = dict(
            ORDER_NUMBER=str(order.guid),
            PAYMENT_ID='103918297912',
            AMOUNT=str(order.summary.price),
            TIMESTAMP='1537170786',
            STATUS='PAID',
        )
        params_in = ['ORDER_NUMBER', 'PAYMENT_ID', 'AMOUNT', 'TIMESTAMP', 'STATUS']
        concatenated_string = '|'.join([params[key] for key in params_in] + [merchant_hash])
        params['RETURN_AUTHCODE'] = sha256(concatenated_string.encode('utf-8')).hexdigest().upper()

        assert not order.status_invalid

        with app.application.test_request_context():
            order.process(OrderAction.PLACE)
        order.save()

        query = '&'.join([f'{key}={value}' for key, value in params.items()])
        res = as_anonymous.get(f'/paytrail/_notify?{query}')
        assert 200 == res.status_code, res.json
        order.refresh()
        assert 1 == len(order.payments)
        assert order.current_state == OrderState.PROCESSING
        assert order.payments[0].current_state == PaymentState.COMPLETED
        assert order.packages[0].current_state == PackageState.WAITING_PACKING
