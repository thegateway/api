# -*- coding: utf-8 -*-
"""
Test the basic pricing functionality
"""
from decimal import Decimal
from random import choice

import pytest

from gwio.core.utils.testing.helpers import THE_ONE_CURRENCY
from gwio.ext.vat import (
    Price,
    VatPrice,
)
from gwio.models.pricing import (
    PriceType,
    Pricing,
    PricingMethod,
    PricingOperation,
)
from ._fakes.pricing import random_pricing


@pytest.fixture(scope='class')
def test_pricing(mock_aws):
    assert mock_aws

    pricing = Pricing(**random_pricing())
    yield pricing


_CURRENCY = THE_ONE_CURRENCY


@pytest.mark.usefixtures('test_pricing')
class TestPricing:
    @pytest.mark.parametrize(
        "method,value,expected",
        [(PricingMethod(op=PricingOperation.MULT, value=Decimal(1.8)), Price(1.0, _CURRENCY), Price(1.8, _CURRENCY)),
         (PricingMethod(op=PricingOperation.ADD, value=Decimal(0.3)), Price(f"2.0 {_CURRENCY.name}"), Price(2.3, _CURRENCY)),
         (PricingMethod(op=PricingOperation.SUB, value=Decimal(0.2)), Price(10, _CURRENCY), Price(9.8, _CURRENCY)),
         (PricingMethod(op=PricingOperation.SET, value=Decimal(30)), Price(Decimal(20), _CURRENCY), Price(30, _CURRENCY))])
    def test_pricing_methods(self, method, value, expected):
        assert expected == method.apply(value)

    @pytest.mark.parametrize("price_class, test_value", [(Price, "10.0 EUR"), (VatPrice, "20.0 EUR")])
    def test_pricing_model_type_consistency(self, test_pricing, price_class, test_value):
        try:
            inst = price_class.from_price_and_vat(test_value)
        except AttributeError:
            inst = price_class(test_value)
        assert isinstance(test_pricing.apply(inst, choice(list(PriceType))), price_class)

    @pytest.mark.parametrize("value", [Decimal(100), 10.0, 25])
    def test_pricing_default_type_conversion(self, test_pricing, value):
        assert isinstance(test_pricing.apply(value, choice(list(PriceType))), Price)
