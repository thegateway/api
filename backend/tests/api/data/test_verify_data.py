webshop_order_stat = {
    'daily_stat': {
        '2020-01-20': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-21': {'order_count': 0.0, 'sale': 0.0},
        '2020-01-22': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-23': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-24': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-25': {'order_count': 2.0, 'sale': 2030.18},
        '2020-01-26': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-27': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-28': {'order_count': 0.0, 'sale': 0.0},
        '2020-01-29': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-30': {'order_count': 1.0, 'sale': 1015.09},
        '2020-01-31': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-01': {'order_count': 2.0, 'sale': 2030.18},
        '2020-02-02': {'order_count': 1.0, 'sale': 1015.09},
        '2020-02-03': {'order_count': 1.0, 'sale': 1015.09},
        '2020-02-04': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-05': {'order_count': 1.0, 'sale': 1015.09},
        '2020-02-06': {'order_count': 2.0, 'sale': 392.2},
        '2020-02-07': {'order_count': 4.0, 'sale': 3066.27},
        '2020-02-08': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-09': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-10': {'order_count': 0.0, 'sale': 0.0}
    },
    'delivery_count': {'Collect from store': 19, 'Pick up center': 2},
    'last_seven_days': {
        '2020-02-04': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-05': {'order_count': 1.0, 'sale': 1015.09},
        '2020-02-06': {'order_count': 2.0, 'sale': 392.2},
        '2020-02-07': {'order_count': 4.0, 'sale': 3066.27},
        '2020-02-08': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-09': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-10': {'order_count': 0.0, 'sale': 0.0}},
    'monthly_stat': {
        '2020-01-31': {'order_count': 10.0, 'sale': 10150.9},
        '2020-02-29': {'order_count': 11.0, 'sale': 8533.92}
    },
    'order_count': 21,
    'payment_count': {'stripe': 21},
    'since': '2020-01-20',
    'total_sale': 18684.82,
    'weekly_stat': {
        '2020-01-26': {'order_count': 7.0, 'sale': 7105.63},
        '2020-02-02': {'order_count': 6.0, 'sale': 6090.54},
        '2020-02-09': {'order_count': 8.0, 'sale': 5488.65}
    },
    'yearly_stat': {
        '2020-12-31': {'order_count': 21.0, 'sale': 18684.82}
    },
    'delivery_method_stat': {
        'delivery_method_daily_stat': {'2020-01-20': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-21': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-01-22': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-23': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-24': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-25': {'Collect from store': 2, 'Pick up center': 0},
                                       '2020-01-26': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-27': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-28': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-01-29': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-30': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-31': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-01': {'Collect from store': 2, 'Pick up center': 0},
                                       '2020-02-02': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-02-03': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-02-04': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-05': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-02-06': {'Collect from store': 1, 'Pick up center': 1},
                                       '2020-02-07': {'Collect from store': 3, 'Pick up center': 1},
                                       '2020-02-08': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-09': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-10': {'Collect from store': 0, 'Pick up center': 0}},
        'delivery_method_monthly_stat': {'2020-01-31': {'Collect from store': 10, 'Pick up center': 0},
                                         '2020-02-29': {'Collect from store': 9, 'Pick up center': 2}},
        'delivery_method_weekly_stat': {'2020-01-26': {'Collect from store': 7, 'Pick up center': 0},
                                        '2020-02-02': {'Collect from store': 6, 'Pick up center': 0},
                                        '2020-02-09': {'Collect from store': 6, 'Pick up center': 2}},
        'delivery_method_yearly_stat': {'2020-12-31': {'Collect from store': 19, 'Pick up center': 2}}
    },
    'payment_method_stat': {
        'payment_method_daily_stat': {'2020-01-20': {'stripe': 1}, '2020-01-21': {'stripe': 0}, '2020-01-22': {'stripe': 1}, '2020-01-23': {'stripe': 1},
                                      '2020-01-24': {'stripe': 1}, '2020-01-25': {'stripe': 2}, '2020-01-26': {'stripe': 1}, '2020-01-27': {'stripe': 1},
                                      '2020-01-28': {'stripe': 0}, '2020-01-29': {'stripe': 1}, '2020-01-30': {'stripe': 1}, '2020-01-31': {'stripe': 0},
                                      '2020-02-01': {'stripe': 2}, '2020-02-02': {'stripe': 1}, '2020-02-03': {'stripe': 1}, '2020-02-04': {'stripe': 0},
                                      '2020-02-05': {'stripe': 1}, '2020-02-06': {'stripe': 2}, '2020-02-07': {'stripe': 4}, '2020-02-08': {'stripe': 0},
                                      '2020-02-09': {'stripe': 0}, '2020-02-10': {'stripe': 0}},
        'payment_method_monthly_stat': {'2020-01-31': {'stripe': 10}, '2020-02-29': {'stripe': 11}},
        'payment_method_weekly_stat': {'2020-01-26': {'stripe': 7}, '2020-02-02': {'stripe': 6}, '2020-02-09': {'stripe': 8}},
        'payment_method_yearly_stat': {'2020-12-31': {'stripe': 21}}
    }
}

webshop_top_sold_products = {
    'top_sold_products': [
        {'count': 20, 'product_guid': '4f95ebe3-f27c-5cee-9296-860bceb4832a'},
        {'count': 18, 'product_guid': '1b632c69-fe9d-5230-aab4-1a300ed9adf9'},
        {'count': 2, 'product_guid': '925b5ad8-7379-4372-a611-0e130707dd06'},
    ]
}

product_sold_stat_over_time = {
    'daily_sold': {
        '2020-01-20': {'product_sold': 1},
        '2020-01-21': {'product_sold': 0},
        '2020-01-22': {'product_sold': 1},
        '2020-01-23': {'product_sold': 1},
        '2020-01-24': {'product_sold': 1},
        '2020-01-25': {'product_sold': 2},
        '2020-01-26': {'product_sold': 1},
        '2020-01-27': {'product_sold': 1},
        '2020-01-28': {'product_sold': 0},
        '2020-01-29': {'product_sold': 1},
        '2020-01-30': {'product_sold': 1},
        '2020-01-31': {'product_sold': 0},
        '2020-02-01': {'product_sold': 2},
        '2020-02-02': {'product_sold': 1},
        '2020-02-03': {'product_sold': 1},
        '2020-02-04': {'product_sold': 0},
        '2020-02-05': {'product_sold': 1},
        '2020-02-06': {'product_sold': 2},
        '2020-02-07': {'product_sold': 3},
        '2020-02-08': {'product_sold': 0},
        '2020-02-09': {'product_sold': 0},
        '2020-02-10': {'product_sold': 0}
    },
    'monthly_sold': {
        '2020-01-31': {'product_sold': 10},
        '2020-02-29': {'product_sold': 10}
    },
    'sold_in_last_seven_days': {
        '2020-02-04': {'product_sold': 0},
        '2020-02-05': {'product_sold': 1},
        '2020-02-06': {'product_sold': 2},
        '2020-02-07': {'product_sold': 3},
        '2020-02-08': {'product_sold': 0},
        '2020-02-09': {'product_sold': 0},
        '2020-02-10': {'product_sold': 0}
    },
    'weekly_sold': {
        '2020-01-26': {'product_sold': 7},
        '2020-02-02': {'product_sold': 6},
        '2020-02-09': {'product_sold': 7}
    },
    'yearly_sold': {
        '2020-12-31': {'product_sold': 20}
    }
}

webshop_specific_product_stats = {
    'active_products_count': 104,
    'for_sale_products_count': 1524,
    'inactive_products_count': 1421,
    'total_products_count': 1525
}
webshops_product_stat_for_admin = {
    'active_products_count': 230,
    'for_sale_products_count': 21805,
    'inactive_products_count': 21576,
    'total_products_count': 21806
}

order_statistics_for_admin = {
    'daily_stat': {
        '2020-01-20': {'order_count': 1.0, 'sale': 1347.59}, '2020-01-21': {'order_count': 0.0, 'sale': 0.0},
        '2020-01-22': {'order_count': 1.0, 'sale': 1347.59}, '2020-01-23': {'order_count': 1.0, 'sale': 1347.59},
        '2020-01-24': {'order_count': 1.0, 'sale': 1347.59}, '2020-01-25': {'order_count': 2.0, 'sale': 2695.18},
        '2020-01-26': {'order_count': 1.0, 'sale': 1347.59}, '2020-01-27': {'order_count': 1.0, 'sale': 1347.59},
        '2020-01-28': {'order_count': 0.0, 'sale': 0.0}, '2020-01-29': {'order_count': 1.0, 'sale': 1347.59},
        '2020-01-30': {'order_count': 1.0, 'sale': 1347.59}, '2020-01-31': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-01': {'order_count': 2.0, 'sale': 2695.18}, '2020-02-02': {'order_count': 1.0, 'sale': 1347.59},
        '2020-02-03': {'order_count': 1.0, 'sale': 1347.59}, '2020-02-04': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-05': {'order_count': 1.0, 'sale': 1347.59}, '2020-02-06': {'order_count': 3.0, 'sale': 1076.2},
        '2020-02-07': {'order_count': 6.0, 'sale': 4958.27}, '2020-02-08': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-09': {'order_count': 0.0, 'sale': 0.0}, '2020-02-10': {'order_count': 0.0, 'sale': 0.0}
    },
    'delivery_count': {'Collect from store': 21, 'Pick up center': 3},
    'delivery_method_stat': {
        'delivery_method_daily_stat': {'2020-01-20': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-21': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-01-22': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-23': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-24': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-25': {'Collect from store': 2, 'Pick up center': 0},
                                       '2020-01-26': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-27': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-28': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-01-29': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-30': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-01-31': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-01': {'Collect from store': 2, 'Pick up center': 0},
                                       '2020-02-02': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-02-03': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-02-04': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-05': {'Collect from store': 1, 'Pick up center': 0},
                                       '2020-02-06': {'Collect from store': 1, 'Pick up center': 2},
                                       '2020-02-07': {'Collect from store': 5, 'Pick up center': 1},
                                       '2020-02-08': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-09': {'Collect from store': 0, 'Pick up center': 0},
                                       '2020-02-10': {'Collect from store': 0, 'Pick up center': 0}},
        'delivery_method_monthly_stat': {'2020-01-31': {'Collect from store': 10, 'Pick up center': 0},
                                         '2020-02-29': {'Collect from store': 11, 'Pick up center': 3}},
        'delivery_method_weekly_stat': {'2020-01-26': {'Collect from store': 7, 'Pick up center': 0},
                                        '2020-02-02': {'Collect from store': 6, 'Pick up center': 0},
                                        '2020-02-09': {'Collect from store': 8, 'Pick up center': 3}},
        'delivery_method_yearly_stat': {'2020-12-31': {'Collect from store': 21, 'Pick up center': 3}}},
    'last_seven_days': {
        '2020-02-04': {'order_count': 0.0, 'sale': 0.0}, '2020-02-05': {'order_count': 1.0, 'sale': 1347.59},
        '2020-02-06': {'order_count': 3.0, 'sale': 1076.2}, '2020-02-07': {'order_count': 6.0, 'sale': 4958.27},
        '2020-02-08': {'order_count': 0.0, 'sale': 0.0}, '2020-02-09': {'order_count': 0.0, 'sale': 0.0},
        '2020-02-10': {'order_count': 0.0, 'sale': 0.0}
    },
    'monthly_stat': {
        '2020-01-31': {'order_count': 10.0, 'sale': 13475.9}, '2020-02-29': {'order_count': 14.0, 'sale': 12772.42}
    },
    'order_count': 24,
    'payment_count': {'stripe': 24},
    'payment_method_stat': {
        'payment_method_daily_stat': {'2020-01-20': {'stripe': 1}, '2020-01-21': {'stripe': 0}, '2020-01-22': {'stripe': 1}, '2020-01-23': {'stripe': 1},
                                      '2020-01-24': {'stripe': 1}, '2020-01-25': {'stripe': 2}, '2020-01-26': {'stripe': 1}, '2020-01-27': {'stripe': 1},
                                      '2020-01-28': {'stripe': 0}, '2020-01-29': {'stripe': 1}, '2020-01-30': {'stripe': 1}, '2020-01-31': {'stripe': 0},
                                      '2020-02-01': {'stripe': 2}, '2020-02-02': {'stripe': 1}, '2020-02-03': {'stripe': 1}, '2020-02-04': {'stripe': 0},
                                      '2020-02-05': {'stripe': 1}, '2020-02-06': {'stripe': 3}, '2020-02-07': {'stripe': 6}, '2020-02-08': {'stripe': 0},
                                      '2020-02-09': {'stripe': 0}, '2020-02-10': {'stripe': 0}},
        'payment_method_monthly_stat': {'2020-01-31': {'stripe': 10}, '2020-02-29': {'stripe': 14}},
        'payment_method_weekly_stat': {'2020-01-26': {'stripe': 7}, '2020-02-02': {'stripe': 6}, '2020-02-09': {'stripe': 11}},
        'payment_method_yearly_stat': {'2020-12-31': {'stripe': 24}}
    },
    'since': '2020-01-20',
    'total_sale': 26248.32,
    'weekly_stat': {'2020-01-26': {'order_count': 7.0, 'sale': 9433.13}, '2020-02-02': {'order_count': 6.0, 'sale': 8085.54},
                    '2020-02-09': {'order_count': 11.0, 'sale': 8729.65}
                    },
    'yearly_stat': {'2020-12-31': {'order_count': 24.0, 'sale': 26248.32}
                    }
}

top_sold_product_for_admin = {
    'top_sold_products':
        [
            {'count': 20, 'product_guid': '4f95ebe3-f27c-5cee-9296-860bceb4832a'},
            {'count': 19, 'product_guid': 'fd2dd14f-8ffc-46fe-b5aa-883a8670f2e7'},
            {'count': 18, 'product_guid': '1b632c69-fe9d-5230-aab4-1a300ed9adf9'}
        ]
}
