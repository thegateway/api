import datetime
import uuid
from decimal import Decimal
import random

from elasticsearch import Elasticsearch

from gwio.environment import Env

from gwio.core.utils.testing.helpers import fake, image_url, n_times

from gwio.ext.vat import Vat


# Fake data to test log index on Elasticsearch

def _transition_history():
    return dict(
        action=fake.word(),
        timestamp=datetime.datetime.now(),
        state=fake.word()
    )


def random_product_line():
    return dict(
        guid=uuid.uuid4(),
        shop_guid=uuid.uuid4(),
        name=fake.name(),
        images=[fake.image_url() for _ in range(5)],
        qty=fake.pyint(),
        type=fake.word(),
        price=dict(
            original=dict(
                total=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                ),
                unit=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                ),
                modifiers=list(),
            ),
            final=dict(
                total=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                ),
                unit=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                ),
            ),
            base=dict(
                total=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                ),
                unit=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                )
            ),
            cost=dict(
                total=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                ),
                unit=dict(
                    amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
                    currency='€',
                    vat_percent=random.choice([10, 14, 24]),
                    vat=random.choice([10, 14, 24]),
                    vat0=random.choice([10, 14, 24]),
                )
            ),
            currency=fake.currency_code(),
            vat_class=random.randint(1, 30)
        )
    )


def random_package():
    return dict(
        guid=uuid.uuid4(),
        contents=dict(
            product_guid=uuid.uuid4(),
            qty=random.randint(1, 10)
        ),
        _current_state=fake.word(),
        _transition_history=_transition_history()
    )


def product_return():
    return dict(
        guid=uuid.uuid4(),
        lines=[dict(
            product_guid=uuid.uuid4(),
            shop_guid=uuid.uuid4(),
            qty=random.randint(10, 20),
            received=[]
        ) for _ in range(2)],
        reason=fake.sentence(10)
    )


def order_shop():
    return dict(
        guid=uuid.uuid4(),
        name=fake.sentence(nb_words=3),
        business_id=fake.ssn(),
        email=fake.email(),
        phone=fake.phone_number(),
        logo=fake.url()
    )


def random_address_data():
    return dict(label=random.choice((None, fake.word())),
                name=fake.company(),
                street=fake.street_address(),
                city=fake.city(),
                zipcode=fake.postcode(),
                country='FI')


def random_phone_data():
    return dict(label=fake.word(), phone=fake.phone_number())


def random_email_data():
    return dict(label=fake.word(), email=fake.email())


def random_banner_data():
    return dict(
        title=random.choice((None, fake.word())),
        image=image_url(),
        link=random.choice((None, fake.url())),
        text=random.choice((None, fake.sentence(random.randint(1, 8)))),
        type=random.choice(['web', 'mobile']))


def random_social_media_data():
    return dict(
        label=fake.word(),
        link=fake.url(),
        analytics=fake.ssn()
    )


def random_opening_hours_normal_data(**kwargs):
    start_hour = random.randint(0, 23)
    return {**dict(day=random.randint(0, 6),
                   start_at=datetime.time(start_hour, random.randint(0, 0 if start_hour == 23 else 59)).strftime("%H:%M"),
                   end_at=datetime.time(start_hour).strftime("%H:%M")),
            **kwargs}


def random_opening_hours_exception_data(**kwargs):
    start_hour = random.randint(0, 23)
    params = dict(date=(datetime.date.today() + datetime.timedelta(days=random.randint(0, 6))),
                  start_at=datetime.time(start_hour, random.randint(0, 0 if start_hour == 23 else 59)).strftime("%H:%M"),
                  desc=fake.sentence(),
                  end_at=datetime.time(random.randint(start_hour, 23), random.randint(0, 59)).strftime("%H:%M"))
    if random.choice([True, False]):
        params['desc'] = fake.word()
    return {**params,
            **kwargs}


# =========================================== Objects ====================================================================
bootstrap = dict(
    data={},
    vertical=fake.word()
)
brand = dict(
    name=fake.sentence(nb_words=3),
    contact=dict(
        name=fake.name(),
        address=fake.address(),
        phone_number=fake.phone_number(),
        email=fake.email()),
    data={},
    guid=uuid.uuid4(),
    tags=[]
)
location = dict(
    name=fake.name(),
    description=fake.sentence(),
    archived=None,
    guid=uuid.uuid4(),
    tags=[uuid.uuid4() for _ in range(3)],
    latitude=fake.latitude(),
    longitude=fake.longitude(),
)
order = dict(
    available_actions=[fake.word() for _ in range(5)],
    billing_address=dict(
        phone=fake.phone_number(),
        name=fake.company(),
        street_address=fake.street_address(),
        email=fake.email(),
        postal_code=fake.postcode(),
        country_code='FI'
    ),
    buyer_identity_id=uuid.uuid4(),
    coupon=dict(
        benifit=dict(
            amount=random.randint(10, 100),
            desc=fake.sentence(),
            method=fake.word()
        ),
        conditions=[],
        guid=uuid.uuid4(),
        name=fake.word(),
        used=fake.boolean(),
        images=[fake.image_url() for _ in range(5)]
    ),
    _current_state=fake.word(),
    customer=dict(
        name=fake.word(),
        phone=fake.phone_number(),
        pin=uuid.uuid4(),
        business_id=dict(
            id=fake.word(),
            country=fake.country()
        )
    ),
    data=dict(),
    description=fake.sentence(),
    flags=dict(
        require_review=fake.boolean(),
        prepayment=fake.boolean(),
    ),
    guid=uuid.uuid4(),
    product_lines=[random_product_line() for _ in range(2)],
    packages=[random_package() for _ in range(3)],
    payments=dict(
        guid=uuid.uuid4(),
        payment_type=fake.word(),
        currency=fake.word(),
        _current_state=fake.word(),
        _data=dict(),
        datetime=fake.past_datetime(),
        amount=fake.pyint(),
        _transition_history=_transition_history()
    ),
    payment_link=fake.url(),
    payment_success_link=fake.url(),
    payment_cancelled_link=fake.url(),
    payment_method_guid=uuid.uuid4(),
    product_returns=[product_return() for _ in range(2)],
    reference=fake.word(),
    shop_guids=[uuid.uuid4() for _ in range(2)],
    source=fake.word(),
    state=fake.word(),
    summary=dict(
        currency=fake.currency_code(),
        price=fake.pyint(),
        vat0_price=random.choice([10, 14, 24]),
        vats=[dict(
            vat=random.choice([10, 14, 24]),
            amount=fake.pyint()
        ) for _ in range(2)]
    ),
    shipping_address=dict(
        phone=fake.phone_number(),
        name=fake.company(),
        street_address=fake.street_address(),
        email=fake.email(),
        postal_code=fake.postcode(),
        country_code='FI'
    ),
    _transition_history=_transition_history()
)
payment_method = dict(
    guid=uuid.uuid4(),
    name=fake.name(),
    logo=image_url(),
    description=fake.sentence(6),
    data={},
    type=fake.word(),
    archived=None,
)
pricing = dict(
    guid=uuid.uuid4(),
    name=fake.sentence(2),
    owner=uuid.uuid4(),
    methods=dict(
        cost=dict(
            op=fake.word(),
            value=20.30
        ),
        retail=dict(
            op=fake.word(),
            value=30.30
        )
    )
)
product = dict(
    activated=fake.boolean(),
    base_price=dict(
        amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
        currency='€',
        vat_percent=random.choice([10, 14, 24])),
    base_price_type=fake.word(),
    brief=fake.word(),
    cost_price=dict(
        amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
        currency='€',
        vat_percent=random.choice([10, 14, 24])),
    created=fake.past_datetime(),
    data={},
    desc=fake.sentence(10),
    guid=uuid.uuid4(),
    images=[fake.image_url() for _ in range(random.randint(3, 7))],
    name=fake.sentence(3),
    pricing=uuid.uuid4(),
    sku=fake.ean(),
    type='default',
    vat=str(Vat(random.choice([10, 14, 24]))),
    owner_guid=uuid.uuid4(),
    tags=[],
    tag_guids=[uuid.uuid4() for _ in range(3)],
    timestamps=dict(
        created=fake.past_datetime()
    )
)
promotion = dict(
    guid=uuid.uuid4(),
    name=fake.sentence(),
    images=[fake.image_url() for _ in range(random.randint(3, 7))],
    benefit=dict(
        desc=fake.sentence(),
        method='percentage',
        amount=fake.pydecimal(),
    ),
    conditions=[]
)
residence = dict(
    guid=uuid.uuid4(),
    ownership=fake.word(),
    name=fake.street_name(),
    owner_identity_id=uuid.uuid4(),
    year_built=fake.year(),
    house_type=fake.word(),
    address=dict(street_address=fake.street_address(),
                 postal_code=fake.postcode()),
    files=[dict(name=fake.file_name(),
                url=fake.url()) for _ in range(random.randint(0, 5))],
    data=fake.pydict(4, True, 'str', 'int'),
    archived=datetime.datetime.now(tz=datetime.timezone.utc) if random.randint(1, 5) == 1 else None,
)
tags = dict(
    guid=uuid.uuid4(),
    name=fake.sentence(nb_words=3),
    active=fake.boolean(),
    images=[fake.image_url() for _ in range(random.randint(3, 7))],
    owner=uuid.uuid4(),
    parent_guid=uuid.uuid4(),
    product_guids=[uuid.uuid4() for _ in range(3)],
    duration=dict(
        start=fake.past_datetime(),
        end=fake.future_datetime(),
    ),
    type=fake.word(),
    data=fake.pydict(4, True, 'str', 'int'),
)
user = dict()
webshop_product = dict(
    product_guid=uuid.uuid4(),
    webshop_guid=uuid.uuid4(),
    pricing_guid=uuid.uuid4(),
    for_sale=fake.boolean(),
    stock_level=fake.pyint(),
)
webshop = dict(
    addresses=n_times(random_address_data, random.randint(1, 3)),
    alert=dict(
        text=fake.sentence(random.randint(1, 8)),
        link=random.choice((None, fake.url())),
        color=fake.safe_hex_color()),
    banners=dict(main=n_times(random_banner_data, random.randint(1, 3)),
                 side=n_times(random_banner_data, random.randint(1, 3)),
                 top=n_times(random_banner_data, random.randint(1, 3))),
    brand_color=fake.safe_hex_color(),
    business_id=fake.company_vat(),
    description=fake.sentence(10),
    emails=n_times(random_email_data, random.randint(1, 3)),
    enabled=fake.boolean(),
    guid=uuid.uuid4(),
    locations=[uuid.uuid4() for _ in range(5)],
    logo=fake.url(),
    name=fake.sentence(nb_words=3),
    delivery_methods=[],
    phones=n_times(random_phone_data, random.randint(1, 2)),
    opening_hours_normal=[random_opening_hours_normal_data(day=x) for x in range(0, 7)],
    opening_hours_exceptions=[random_opening_hours_exception_data()],
    payment_method_guids=[uuid.uuid4() for _ in range(3)],
    person_in_charge=fake.name(),
    _private_data=dict(
        paytrail=dict(
            hash=fake.iban(),
            id=fake.iban(),
        ),
        stripe=dict(
            account_id=fake.iban(),
            commission_percentage=random.randint(1, 10),
        ),
        email=fake.email()
    ),
    public_data=dict(stripe=None, domain=fake.domain_name()),
    promotion=dict(
        local=[],
        general=[]
    ),
    shorthand_name=fake.pystr(min_chars=1, max_chars=10),
    social_media=n_times(random_social_media_data, random.randint(1, 3)),
    static_pages=[dict(
        guid=uuid.uuid4(),
        title=fake.word(),
        link=fake.url(),
        inline=fake.boolean(),
    )],
    timezone=fake.timezone(),
    tags=[uuid.uuid4() for _ in range(5)],
    websites=n_times(fake.url, random.randint(1, 3)),
)

object_dicts = dict(
    bootstrap=bootstrap,
    brand=brand,
    location=location,
    order=order,
    payment_method=payment_method,
    pricing=pricing,
    promotion=promotion,
    product=product,
    residence=residence,
    tags=tags,
    webshop_product=webshop_product,
    webshop=webshop,
    user=user
)

object_names = ['bootstrap', 'brand', 'location', 'order', 'payment_method', 'pricing', 'promotion', 'product', 'residence', 'tags', 'webshop_product',
                'webshop', 'user']


# ================================================== Event Log Data ==================================================================


def main():
    for obj in object_names:
        log_event = dict()
        log_event['audit'] = False
        log_event['data'] = dict()
        log_event['event_type'] = fake.word()
        log_event['obj'] = {obj: object_dicts[obj]}
        log_event['object_id'] = uuid.uuid4()
        log_event['object_type'] = obj
        log_event['request'] = dict(
            guid=uuid.uuid4(),
            method=fake.word(),
            remote_ips=fake.ipv4(),
            url_path=fake.word(),
            url_root=fake.word(),
            user_agent=fake.word()
        )
        log_event['request_guid'] = uuid.uuid4()
        log_event['response'] = dict(
            data=fake.sentence(10),
            response_code=200
        )
        log_event['subject_id'] = uuid.uuid4()
        log_event['subject_type'] = fake.word()
        log_event['timestamp'] = fake.past_datetime()

        elastic_client = Elasticsearch(Env.ELASTICSEARCH_HOST)

        response = elastic_client.index(
            index=f"{Env.VERTICAL}_{Env.APPLICATION_ENVIRONMENT}_logs",
            id=uuid.uuid4(),
            body=log_event,
        )

        # print out the response:
        print('response:', response)


if __name__ == '__main__':
    main()
