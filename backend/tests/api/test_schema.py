# -*- coding: utf-8 -*-
import datetime
import json
import random
from decimal import Decimal

import pytest

from gwio.core.utils.testing.helpers import (
    THE_ONE_CURRENCY,
    fake,
    image_url,
)
from gwio.ext.vat import (
    Vat,
    VatPrice,
)
from gwio.logic.orders.types import (
    OrderAction,
    OrderAssessment,
    OrderState,
)
from gwio.logic.payments.handler import PaymentState
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.models.modifiers import (
    ModifierLine,
    ModifierLineSource,
    ModifierMethodType,
)
from gwio.models.orders import Order
from gwio.models.payment_attribute import Payment
from gwio.models.products import Product


def _fake_address(faker):
    return {'country_code': faker.country_code(),
            'email': faker.email(),
            'name': faker.name(),
            'phone': faker.phone_number(),
            'postal_code': faker.postcode(),
            'street_address': faker.street_address()}


def _fake_charge():
    return {
        "amount": 95491,
        "amount_refunded": 0,
        "application": None,
        "application_fee": None,
        "balance_transaction": "txn_19ifwXGM5IcY8A79ocOMsKbP",
        "captured": True,
        "created": 1486020419,
        "currency": "eur",
        "customer": "cus_A1cZL9uu0N1rtN",
        "description": "Order #531",
        "destination": None,
        "dispute": None,
        "failure_code": None,
        "failure_message": None,
        "fraud_details": {},
        "id": "ch_19iezfGM5IcY8A79FQqvYFqc",
        "invoice": None,
        "livemode": False,
        "metadata": {},
        "object": "charge",
        "on_behalf_of": None,
        "order": None,
        "outcome": {
            "network_status": "approved_by_network",
            "reason": None,
            "risk_level": "normal",
            "seller_message": "Payment complete.",
            "type": "authorized"
        },
        "paid": True,
        "payment_intent": None,
        "receipt_email": None,
        "receipt_number": None,
        "refunded": False,
        "refunds": {
            "data": [],
            "has_more": False,
            "object": "list",
            "total_count": 0,
            "url": "/v1/charges/ch_19iezfGM5IcY8A79FQqvYFqc/refunds"
        },
        "review": None,
        "shipping": None,
        "source": {
            "address_city": None,
            "address_country": None,
            "address_line1": None,
            "address_line1_check": None,
            "address_line2": None,
            "address_state": None,
            "address_zip": None,
            "address_zip_check": None,
            "brand": "Visa",
            "country": "US",
            "customer": "cus_A1cZL9uu0N1rtN",
            "cvc_check": None,
            "dynamic_last4": None,
            "exp_month": 5,
            "exp_year": 2017,
            "fingerprint": "9oyOKJ6RkBTJtSgO",
            "funding": "credit",
            "id": "card_19hZKSGM5IcY8A79tq3umNmH",
            "last4": "4242",
            "metadata": {},
            "name": "Panda Bear #758",
            "object": "card",
            "tokenization_method": None
        },
        "source_transfer": None,
        "statement_descriptor": None,
        "status": "succeeded",
        "transfer_group": None
    }


def _fake_payment():
    payment = Payment(
        payment_type=PaymentTypes('stripe'),
        _current_state=PaymentState.COMPLETED,
        datetime=datetime.datetime.now(),
        amount=Decimal('954.91'),
        currency=THE_ONE_CURRENCY,
    )
    payment.data = {
        'charge_capture': _fake_charge(),
        'charge_id': _fake_charge()['id'],
    }
    return payment


def _fake_modifier_lines():
    return [ModifierLine(
        desc=fake.sentence(),
        source=random.choice(list(ModifierLineSource)),
        method=random.choice(list(ModifierMethodType)),
        amount=Decimal(random.randint(100, 4000) / 100)) for _ in range(random.randrange(1, 2))]


def _fake_product_lines(faker, random_products):
    def mapped_product(func, product):
        vat = Vat(random.choice([0, 10, 24]))
        random_price = f"{random.randint(5000, 1000000) / 100} EUR"
        product.base_price = VatPrice.from_price_and_vat(random_price, vat=vat)
        product.image = image_url()
        product.name = faker.file_name().title()
        product.vat = vat
        return func(product.API.Assessor.evaluate(product, qty=Decimal(random.randint(1, 10))))

    map_func = Product.API.Assessor.EvaluatedProduct.ProductLine.mapped
    fake_product_lines = [mapped_product(map_func, product) for product in random_products(random.randrange(3, 6))]
    for x in fake_product_lines:
        x.price_modifiers = _fake_modifier_lines()
    return fake_product_lines


@pytest.fixture(scope='function')
def customer(mock_aws, test_customer, customer_identity_id):
    assert mock_aws
    test_customer.save()
    yield test_customer
    test_customer.delete()


@pytest.fixture(scope='class')
def shop_order(create_test_order, faker, webshop, customer_identity_id, random_products, create_random_product):
    fake_product_lines = _fake_product_lines(faker, random_products)
    order = create_test_order(shop_guid=webshop.guid,
                              buyer_identity_id=customer_identity_id,
                              product_lines=fake_product_lines,
                              _current_state=OrderState.CREATED,
                              payments=[_fake_payment()],
                              billing_address=_fake_address(faker),
                              shipping_address=_fake_address(faker))
    for line in order.product_lines:
        product = create_random_product()
        product.save()
        line.guid = product.guid
    Order.API.assess(model=order, action=OrderAction.CREATE, assessment=OrderAssessment.PRICES)
    webshop.save()
    order.save()
    yield order
    order.delete()
    webshop.delete()


@pytest.mark.usefixtures('mock_aws')
class TestSchema:
    def _keys(self, schema_type):
        # These are the keys that are to be expected as proposed in the draft in task Gitlab v2018#200.
        top_level_keys = {'addresses',  # Tested for elsewhere
                          'customer',  # Tested for elsewhere
                          'coupon',  # Tested for elsewhere
                          'buyer_identity_id',  # Tested for elsewhere
                          'flags',  # Tested for elsewhere
                          'guid',  # Tested for elsewhere
                          'lines',  # Tested for elsewhere
                          'payment',
                          'packages',
                          'product_returns',  # Tested for elsewhere
                          'reference',  # Tested for elsewhere
                          'shops',  # Tested for elsewhere
                          'state',  # Tested for elsewhere
                          'summary',  # Tested for elsewhere
                          'payment_method',  # Tested for elsewhere
                          'available_actions',  # Tested for elsewhere
                          'timestamps'}  # Tested for elsewhere

        payment_keys = {'type',
                        'last4',
                        'date',
                        'amount',
                        'state',
                        'client_secret',
                        'intent_id'}

        return top_level_keys, payment_keys

    @pytest.mark.parametrize(('client', 'route', 'guid_func', 'keys_for'), [
        # (pytest.lazy_fixture('as_shopkeeper'), '/webshops/{guid}/orders/{order.guid}/', lambda o: o.shop_guid, 'shopkeeper'),
        (pytest.lazy_fixture('as_customer'), '/customers/{guid}/orders/{order.guid}/', lambda o: o.buyer_identity_id.split(':')[1], 'customer'),
    ])
    def test_order_shop_schema(self, shop_order, customer, client, route, guid_func, keys_for):
        # TODO: This test should be incorporated into other tests.
        # For example a test that updates delivery address should check the response for it and the check for that key should be removed from here
        resp = client.get(route.format(guid=guid_func(shop_order), order=shop_order))
        assert resp.status_code == 200
        resp_data = json.loads(resp.data)

        top_level_keys, payment_keys = self._keys(keys_for)
        assert top_level_keys == set(resp_data), set(resp).difference(set(top_level_keys))

        assert payment_keys == set(resp_data['payment'])

        assert len(resp_data['lines']) == len(shop_order.product_lines)

        assert 'pin' not in resp_data['customer'] if keys_for == 'customer' else 'pin' in resp_data['customer']
