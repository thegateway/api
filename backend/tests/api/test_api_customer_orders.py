import random
from collections import defaultdict
from datetime import (
    datetime,
    timezone,
)
from decimal import Decimal
from uuid import UUID

import dateutil
import pytest
import pytz
from attrdict import AttrDict
from marshmallow.fields import Field
from pytest_lazyfixture import LazyFixture

from gwio.core.utils.testing.helpers import (
    fake,
)
from gwio.ext.vat import Currency
from gwio.logic.orders.handler import OrderHandler
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.logic.payments.types import PaymentTypes
from gwio.models.orders import Order
# This is just to get better type hints for the functions
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.user import UserData
from gwio.utils import uuid
from gwio.utils.uuid import get_random_uuid
from ._fixtures.orders import (
    valid_order_create_post_data,
    valid_update_put_data,
)
from ._mocks import mock_order_fsm_triggered_logic

A = OrderAction
S = OrderState


def listify(value):
    if isinstance(value, list):
        return value
    return [value]


@pytest.fixture(scope='class')
def test_shop(random_webshop):
    random_webshop.save()
    return random_webshop


@pytest.fixture(scope='class')
def test_customer_order(create_test_order, customer_identity_id, test_shop, create_random_product):
    order = create_test_order(buyer_identity_id=customer_identity_id, shop_guid=test_shop.guid, _current_state=OrderState.CREATED)
    for line in order.product_lines:
        product = create_random_product(owner_guid=test_shop.guid)
        product.save()
        line.guid = product.guid
    order.save()
    return order


@pytest.fixture(scope='class')
def test_non_customer_order(create_test_order, test_shop, create_random_product, random_identity_id):
    order = create_test_order(buyer_identity_id=random_identity_id(), shop_guid=test_shop.guid, _current_state=OrderState.CREATED)
    for line in order.product_lines:
        product = create_random_product(owner_guid=test_shop.guid)
        product.save()
        line.guid = product.guid
    order.save()
    return order


@pytest.fixture(scope='class')
def test_orders(random_orders, test_customer_order, test_non_customer_order, test_shop, customer_identity_id, create_random_product):
    orders = random_orders(5,
                           buyer_identity_id=customer_identity_id,
                           _current_state=OrderState.CREATED,
                           shop_guid=test_shop.guid)

    for o in orders:
        for line in o.product_lines:
            product = create_random_product(owner_guid=test_shop.guid)
            product.save()
            line.guid = product.guid
        o.save()

    return [test_customer_order, test_non_customer_order] + orders


@pytest.fixture(scope='class')
def test_products(mock_aws, random_products, webshop_guid):
    assert mock_aws
    products = random_products(10, type='default', owner_guid=webshop_guid) + random_products(10, owner_guid=webshop_guid)
    for product in products:
        product.save()
    return products


@pytest.fixture(scope='class')
def test_user_datas(mock_aws, customer_guid, customer_identity_id):
    assert mock_aws
    user_data = UserData(guid=customer_guid,
                         notifications=dict())
    user_data.identity_id = customer_identity_id
    user_data.save()
    return [user_data]


@pytest.fixture(scope='class')
def test_payment_method_stripe(mock_aws, create_random_payment_method):
    assert mock_aws
    payment_method = create_random_payment_method(
        name='Stripe',
        type=PaymentTypes.STRIPE)
    payment_method.save()
    return payment_method


@pytest.fixture(scope='class')
def test_payment_method_paytrail(mock_aws, create_random_payment_method):
    assert mock_aws
    payment_method = create_random_payment_method(
        name='Paytrail',
        type=PaymentTypes.PAYTRAIL)
    payment_method.save()
    return payment_method


@pytest.fixture(scope='class')
def test_payment_method_payu(mock_aws, create_random_payment_method):
    assert mock_aws
    payment_method = create_random_payment_method(
        name='PayU',
        type=PaymentTypes.PAYU)
    payment_method.save()
    return payment_method


@pytest.fixture(scope='class')
def test_delivery_method(create_random_delivery_method):
    delivery_method = create_random_delivery_method()
    delivery_method.save()
    return delivery_method


@pytest.fixture(scope='class')
def test_product(create_random_product):
    product = create_random_product()
    product.save()
    return product


@pytest.fixture(scope='function')
def customer(test_customer):
    test_customer.save()
    yield test_customer
    test_customer.delete()


def check_possible_actions_from_response(fsm, order: Order, response_order: dict):
    # Create is triggered when the order is created, no existing order should ever have this available
    skip_actions = ('create', 'payment')
    available_actions = {x for x in fsm.get_triggers(order.current_state.value) if not x.startswith(('to_', '_')) and x not in skip_actions}
    assert available_actions == set(response_order['available_actions'])


@pytest.mark.usefixtures('mock_aws', 'test_orders')
class TestApiCustomerOrders():
    """ Tests for GET /customers/<customer_guid>/orders/<order_guid>/ """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    def test_list_customer(self, customer, as_customer, test_orders, customer_identity_id):
        res = as_customer.get(f"/customers/{customer_identity_id.split(':')[1]}/orders")
        assert 200 == res.status_code, res.json

        expected_order_count = len([o for o in test_orders if o.buyer_identity_id == customer_identity_id])
        assert 0 < expected_order_count
        assert expected_order_count == len(res.json)

    def test_list_customer_incorrect_guid(self, as_customer):
        res = as_customer.get(f"/customers/{get_random_uuid()}/orders")
        assert 403 == res.status_code, res.json

    def test_orders_single_customer(self, customer, as_customer, customer_identity_id, test_orders):
        orders = [o for o in test_orders if o.buyer_identity_id == customer_identity_id]
        assert orders
        for o in orders:
            res = as_customer.get(f"/customers/{customer_identity_id.split(':')[1]}/orders/{o.guid}/")
            assert 200 == res.status_code, res.json
            assert o.guid == uuid.UUID(res.json['guid'])
            check_possible_actions_from_response(self.fsm, o, res.json)

    def test_orders_single_customer_not_own(self, customer, as_customer, customer_identity_id, test_orders):
        orders = [o for o in test_orders if o.buyer_identity_id != customer_identity_id]
        assert orders
        for o in orders:
            res = as_customer.get(f"/customers/{customer_identity_id.split(':')[1]}/orders/{o.guid}/")
            assert 404 == res.status_code, res.json

    def test_orders_single_unauthenticated(self, as_anonymous, customer_identity_id, test_orders):
        assert test_orders
        for o in test_orders:
            res = as_anonymous.get(f"/customers/{customer_identity_id.split(':')[1]}/orders/{o.guid}/")
            assert 401 == res.status_code, res.json


@pytest.mark.usefixtures('mock_aws')
class TestApiCustomersOrdersCreate():
    """ Tests for POST /shops/<shop_guid>/orders/ """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    @pytest.mark.parametrize('order_webshop', [None, pytest.lazy_fixture('webshop_guid')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_order_create_shopkeeper(self, order_webshop, mocker, as_shopkeeper, webshop, webshop_guid, test_products, customer_identity_id, test_user_datas):
        assert test_user_datas
        mock_order_fsm_triggered_logic(mock_assess=False, mocker=mocker, force_exception=False)

        post_data = valid_order_create_post_data(products=test_products, webshop_guid=webshop_guid)

        if order_webshop is None:
            params = dict(headers=dict())
        else:
            token_payload = as_shopkeeper.token_payload.copy()
            del [token_payload['custom:organization_guid']]
            params = dict(headers={'X-TheGw-Shop-GUID': str(order_webshop)}, token_payload=token_payload)
        res = as_shopkeeper.post(f"/customers/{customer_identity_id.split(':')[1]}/orders/", json=post_data, **params)
        assert 201 == res.status_code, res.json
        order = Order.get(guid=uuid.UUID(res.json['guid']))
        assert S.CREATED == order.current_state
        assert S.CREATED == S(res.json['state'])

        assert customer_identity_id == order.buyer_identity_id
        assert webshop_guid in order.shop_guids

        assert post_data['customer_name'] == res.json['customer']['name']
        assert post_data['customer_name'] == order.customer.name
        assert customer_identity_id == res.json['buyer_identity_id']
        assert customer_identity_id == order.buyer_identity_id
        assert res.json['reference'] is not None

        assert 1 == len(res.json['shops'])
        assert webshop.guid == UUID(res.json['shops'][0]['guid'])
        for attr_name in {'phone', 'name', 'email', 'business_id', 'logo'}:
            assert getattr(webshop, attr_name) == res.json['shops'][0][attr_name]

        # Test products and quantities got updated correctly
        qtys = dict([(x.guid, x.qty) for x in order.product_lines])
        assert len(post_data['products']) == len(order.product_lines)
        assert all([x['qty'] == qtys[x['guid']] for x in post_data['products']])

        # Testing all saved attributes are included in the response
        assert all(isinstance(res.json['flags'][x], bool) for x in order.flags)
        # Testing all the order statuses are included in the response flags
        for attr in dir(order):
            if attr.startswith('status_') and attr != 'status_need_review':
                assert isinstance(res.json['flags'][attr[7:]], bool), attr
                # Testing all the order timestamps are included in the response flags
        assert all(isinstance(res.json['flags'][x], bool) for x in dir(order.timestamps) if isinstance(getattr(order.timestamps.__class__, x), property))

        check_possible_actions_from_response(self.fsm, order, res.json)

    def test_order_create_shopkeeper_invalid_customer(self, as_shopkeeper, test_products, webshop_guid):
        res = as_shopkeeper.post(f"/customers/{get_random_uuid()}/orders/",
                                 json=valid_order_create_post_data(products=test_products, webshop_guid=webshop_guid))
        assert 404 == res.status_code, res.json

    @pytest.mark.parametrize('id_guid', [pytest.lazy_fixture('customer_identity_guid'), get_random_uuid()],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_order_create_non_shopkeeper(self, as_anonymous, id_guid):
        res = as_anonymous.post(f"/customers/{id_guid}/orders/")
        assert 401 == res.status_code, res.json


@pytest.mark.usefixtures('mock_aws', 'test_products', 'test_orders', 'mock_stripe')
class TestApiCustomersOrdersUpdate():
    """ Tests for PUT /customers/<customer_guid>/orders/<order_guid>/ """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    def test_pin_is_updated(self, order_finder, customer, customer_identity_id, test_orders, as_customer):
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            lambda order: order.customer.pin is None,
            orders=test_orders)
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=dict())
        assert 200 == res.status_code, res.json
        order.refresh()
        assert as_customer.token_payload['pin'] == order.customer.pin
        assert 'pin' not in res.json['customer']

    @pytest.mark.parametrize('attr_name, value', [
        ('name', fake.name()),
        ('phone', '+358 00 000 0000'),
        ('business_id', dict(
            country=fake.country(),
            id=fake.word()
        ))
    ])
    def test_update_customer_values(self, order_finder, customer, customer_identity_id, test_orders, as_customer, attr_name, value):
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            lambda order: order.current_state == S.CREATED,
            orders=test_orders)
        sent_data = dict(customer={attr_name: value})
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=sent_data)
        assert 200 == res.status_code, res.json
        order.refresh()
        assert sent_data['customer'][attr_name] == res.json['customer'][attr_name]
        assert sent_data['customer'][attr_name] == order.customer.Schema().dump(order.customer)[attr_name]
        check_possible_actions_from_response(self.fsm, order, res.json)

    def test_update_products(self, mocker, as_customer, customer, customer_identity_id, test_products, test_orders, order_finder, webshop):
        mock_order_fsm_triggered_logic(mock_assess=False, mocker=mocker, force_exception=False)

        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            lambda order: order.current_state == S.CREATED,
            # Making sure old lines are overwritten is tested
            lambda order: order.product_lines,
            orders=test_orders)

        # Making sure new product line count is different from the current
        while True:
            # Only default products because the prices can be expected
            post_data = valid_update_put_data(products=[x for x in test_products if x.type == 'default'])
            # Sending multiple lines with same guid to see if they are combined
            post_data['products'].append(dict(guid=post_data['products'][0]['guid'],
                                              qty=random.randint(1, 20),
                                              shop_guid=post_data['products'][0]['shop_guid']))
            if len({x['guid'] for x in post_data['products']}) != len({x.guid for x in order.product_lines if x.type != PRODUCT_TYPE_DELIVERY_METHOD}):
                break

        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=post_data)

        assert 200 == res.status_code, res.json
        post_qtys = {y['guid']: sum([x['qty'] for x in post_data['products'] if x['guid'] == y['guid']]) for y in post_data['products']}

        order.refresh()

        expected_summary = dict(price=Decimal(0), vat0_price=Decimal(0), vats=defaultdict(Decimal))

        assert len(post_qtys) == len(res.json['lines'])
        for sent_guid, expected_quantity in post_qtys.items():
            res_line = next(x for x in res.json['lines'] if UUID(x['guid']) == sent_guid)
            product = Product.by_guid(sent_guid)

            assert expected_quantity == res_line['qty']
            for attr_name in ['name', 'images', 'type']:
                assert getattr(product, attr_name) == res_line[attr_name], attr_name
            assert product.base_price.price.currency == Currency(res_line['price']['currency'])
            assert product.vat.percentage == res_line['price']['vat_class']

            for x in ['original', 'final']:
                res_price = res_line['price'][x]
                product_price = product.base_price @ product.vat
                assert float(product_price.amount) == res_price['unit']['amount'], x
                assert float(product_price.included_vat.amount) == res_price['unit']['vat'], x
                assert float(product_price.without_vat.amount) == res_price['unit']['vat0'], x
                assert product_price.vat_percent == res_price['unit']['vat_percent'], x
                total_price = product.base_price @ product.vat * expected_quantity
                assert float(total_price.amount) == res_price['total']['amount'], x
                assert float(total_price.included_vat.amount) == res_price['total']['vat'], x
                assert float(total_price.without_vat.amount) == res_price['total']['vat0'], x
                assert total_price.vat_percent == res_price['total']['vat_percent'], x

                if x == 'final':
                    expected_summary['price'] += total_price.amount
                    expected_summary['vat0_price'] += total_price.without_vat.amount
                    expected_summary['vats'][product.vat.percentage] += total_price.included_vat.amount

        # TODO: Where to get expected value?
        # assert CURRENCY == Currency(res.json['summary']['currency'])
        assert float(expected_summary['price']) == res.json['summary']['price']
        assert float(expected_summary['vat0_price']) == res.json['summary']['vat0_price']

        for vat_code, vat_total in expected_summary['vats'].items():
            res_vat_data = next(x for x in res.json['summary']['vats'] if x['vat'] == vat_code)
            assert float(vat_total) == res_vat_data['amount']

        assert S.CREATED == order.current_state

        # Test products and quantities got updated correctly
        assert len({x['guid'] for x in post_data['products']}) == len({x.guid for x in order.product_lines if x.type != PRODUCT_TYPE_DELIVERY_METHOD})
        qtys = dict([(x.guid, x.qty) for x in order.product_lines])
        assert all([qty == qtys[guid] for guid, qty in post_qtys.items()])

    @pytest.mark.parametrize('request_data', [dict(shipping_address=None),
                                              dict(billing_address=None),
                                              dict(shipping_address=None,
                                                   billing_address=None)])
    def test_update_address(self, order_finder, customer, customer_identity_id, test_orders, as_customer, faker, request_data, mocker):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)

        for x in request_data:
            request_data[x] = dict(name=faker.name(),
                                   phone='+358 00 000 0000',
                                   email='trash@the.gw',
                                   street_address=faker.street_address(),
                                   postal_code=faker.postcode(),
                                   country_code='FI')

        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            lambda order: order.shipping_address is None and order.billing_address is None,
            orders=test_orders)
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=request_data)
        assert 200 == res.status_code, res.json

        order.refresh()
        try:
            for key, value in request_data['shipping_address'].items():
                assert value == res.json['addresses']['shipping'][key]
                assert value == getattr(order.shipping_address, key)
                if 'billing_address' not in request_data:
                    assert value == getattr(order.billing_address, key)
        except KeyError:
            for key, value in request_data.get('shipping_address', dict()).items():
                assert res.json['addresses']['shipping'][key] is None
            assert order.shipping_address is None

        try:
            for key, value in request_data['billing_address'].items():
                assert value == res.json['addresses']['billing'][key]
                assert value == getattr(order.billing_address, key)
        except KeyError:
            if 'shipping_address' not in request_data:  # If shipping address was sent, it was saved as billing address as well
                for key, value in request_data['billing_address'].items():
                    assert res.json['addresses']['billing'][key] is None
                assert order.billing_address is None

    @pytest.mark.parametrize('delivery_method, expected_code', [
        (pytest.lazy_fixture('test_delivery_method'), 200),
        (pytest.lazy_fixture('random_product'), 409),
        (AttrDict(guid=get_random_uuid()), 409)
    ])
    def test_update_delivery_method(self, mock_aws, order_finder, customer, customer_identity_id, test_orders, as_customer, delivery_method,
                                    expected_code, create_random_delivery_method):
        def test_update(*, order, customer_identity_id, expected_code, delivery_method):
            res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/",
                                  json=dict(delivery_method=delivery_method.guid))
            assert expected_code == res.status_code, res.json

            if expected_code != 200:
                assert 'Delivery method does not exist' in res.json['message']
                return

            order.refresh()
            # TODO: The next 3 lines are basically testing the Order.delivery_method_guid property
            delivery_method_lines = [x for x in order.product_lines if x.type == PRODUCT_TYPE_DELIVERY_METHOD]
            assert 1 == len(delivery_method_lines)
            assert delivery_method.guid == delivery_method_lines[0].guid
            assert delivery_method.guid == order.delivery_method_guid

        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            lambda order: order.delivery_method_guid is None,
            orders=test_orders)

        test_update(order=order, customer_identity_id=customer_identity_id, expected_code=expected_code,
                    delivery_method=delivery_method)

        if expected_code == 200:
            # Testing that updating the delivery method for an order that already has a delivery method
            # does not result in 2 product lines with delivery method on them
            another_delivery_method = create_random_delivery_method()
            another_delivery_method.save()
            test_update(order=order, customer_identity_id=customer_identity_id, expected_code=expected_code,
                        delivery_method=another_delivery_method)

    @pytest.mark.parametrize('payment_method, update_data',
                             [(pytest.lazy_fixture('test_payment_method_stripe'),
                               dict(payment_method=dict(guid=None, card='abc'))),
                              (pytest.lazy_fixture('test_payment_method_paytrail'),
                               dict(payment_method=dict(guid=None,
                                                        urls=dict(success='aaa', cancel='bbb')))),
                              (pytest.lazy_fixture('test_payment_method_payu'),
                               dict(payment_method=dict(guid=None, continue_url='https://a-test-url.com/path')))])
    def test_update_payment_method(self, order_finder, customer, customer_identity_id, test_orders, as_customer,
                                   test_payment_method_stripe, test_payment_method_paytrail, test_payment_method_payu, payment_method, update_data, mocker):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)
        update_data['payment_method']['guid'] = payment_method.guid  # Lazy fixture does not work inside a dict
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            # lambda order: order.current_state != OrderState.FAILED,
            lambda order: order.payment_method_guid != update_data['payment_method']['guid'],
            orders=test_orders)
        start_state = order.current_state
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=update_data)
        assert 200 == res.status_code, res.json
        order.refresh()
        assert start_state == order.current_state
        assert update_data['payment_method']['guid'] == order.payment_method_guid
        payment_method_attributes = ('description', 'logo', 'name', 'type', 'guid', 'archived')
        for attr_name in payment_method_attributes:
            expected_value = getattr(order.payment_method, attr_name)
            # Archived value could be None, which will throw an error if compare
            try:
                assert expected_value == expected_value.__class__(res.json['payment_method'][attr_name]), attr_name
            except TypeError:
                pass

        if update_data['payment_method']['guid'] == test_payment_method_stripe.guid:
            assert update_data['payment_method']['card'] == res.json['payment_method']['card']

        if update_data['payment_method']['guid'] == test_payment_method_paytrail.guid:
            assert update_data['payment_method']['urls']['success'] == order.data.paytrail.urls.success
            assert update_data['payment_method']['urls']['cancel'] == order.data.paytrail.urls.cancel

        if update_data['payment_method']['guid'] == test_payment_method_payu.guid:
            assert update_data['payment_method']['continue_url'] == order.data.payu.continue_url

        assert len(payment_method_attributes + ('card', 'continue_url')) == len(res.json['payment_method'])  # Making sure nothing extra is returned

    def test_delete_payment_method(self, order_finder, customer, customer_identity_id, test_orders, as_customer, as_admin,
                                   test_payment_method_stripe):
        payment_method_guid = test_payment_method_stripe.guid
        put_data = dict(payment_method=dict(guid=payment_method_guid, card='abc'))
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            orders=test_orders)
        # Add payment method to order
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=put_data)
        assert 200 == res.status_code, res.json
        assert payment_method_guid == uuid.UUID(res.json['payment_method']['guid'])
        assert res.json['payment_method']['archived'] is None
        # Archive payment method
        res = as_admin.delete(f'/payment_methods/{str(payment_method_guid)}/')
        assert 204 == res.status_code, res.json
        # Check order with archived payment method
        res = as_customer.get(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/")
        assert 200 == res.status_code, res.json
        assert res.json['payment_method']['archived'] is None  # Saved in the order and not updated, when payment method is updated

    def test_update_payment_method_invalid_guid(self, order_finder, customer, customer_identity_id, test_orders, as_customer):
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            orders=test_orders)
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/",
                              json=dict(payment_method=dict(guid=get_random_uuid())))
        assert 409 == res.status_code, res.json
        assert 'Invalid payment method guid.' in res.json['message']

    def test_update_payment_method_stripe_fail(self, order_finder, customer, customer_identity_id, test_orders, test_payment_method_stripe, as_customer):
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            orders=test_orders)
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/",
                              json=dict(payment_method=dict(guid=test_payment_method_stripe.guid)))
        assert 409 == res.status_code, res.json
        assert f'{{"payment_method": {{"card": ["{Field.default_error_messages["required"]}"]}}}}' in res.json[
            'message']

    @pytest.mark.parametrize('remove_key', ['success', 'cancel'])
    def test_update_payment_method_paytrail_fail(self, order_finder, customer, customer_identity_id, test_orders,
                                                 test_payment_method_paytrail, as_customer, remove_key):
        order = order_finder.find_order(
            lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
            lambda order: order.buyer_identity_id == customer_identity_id,
            orders=test_orders)

        update_data = dict(
            payment_method=dict(
                guid=test_payment_method_paytrail.guid,
                urls=dict(success='aaa', cancel='bbb')))
        update_data['payment_method']['urls'].pop(remove_key)

        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/", json=update_data)
        assert 409 == res.status_code, res.json
        assert f'{{"payment_method": {{"urls": {{"{remove_key}": ["{Field.default_error_messages["required"]}"]}}}}}}' in res.json['message']

    def test_update_invalid_customer_guid(self, as_customer, customer, test_customer_order, test_products):
        post_data = valid_update_put_data(products=test_products)
        res = as_customer.put(f"/customers/{get_random_uuid()}/orders/{test_customer_order.guid}/", json=post_data)
        assert 403 == res.status_code, res.json

    @pytest.mark.parametrize('order_guid', [None, get_random_uuid()],
                             ids=lambda x: 'existing_order' if x is None else 'non_existing_order')
    def test_update_invalid_order_guid(self, customer, as_customer, customer_identity_id, test_non_customer_order, test_products,
                                       order_guid):
        if order_guid is None:
            order_guid = test_non_customer_order.guid
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order_guid}/",
                              json=valid_update_put_data(products=test_products))
        assert 404 == res.status_code, res.json

    @pytest.mark.parametrize('as_x, user_guid, expected_code',
                             [(pytest.lazy_fixture('as_admin'), pytest.lazy_fixture('admin_guid'), 403),
                              (pytest.lazy_fixture('as_shopkeeper'), pytest.lazy_fixture('shopkeeper_guid'), 403),
                              (pytest.lazy_fixture('as_anonymous'), get_random_uuid(), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    @pytest.mark.parametrize('buyer_identity_id', [None, get_random_uuid()],
                             ids=lambda x: 'existing_customer' if x is None else 'non_existing_customer')
    def test_update_non_customer(self, as_x, customer, user_guid, expected_code, buyer_identity_id):
        buyer = buyer_identity_id if buyer_identity_id is not None else user_guid
        res = as_x.put(f"/customers/{buyer}/orders/{get_random_uuid()}/")
        assert expected_code == res.status_code, res.json

    def test_order_archive_customer(self, webshop, as_customer, customer_identity_guid, test_products, test_payment_method_stripe, test_delivery_method):
        # Test that order can not be archived if it's past CREATED state
        post_data = valid_order_create_post_data(products=test_products, webshop_guid=webshop.guid)
        r = as_customer.post(f"/customers/{customer_identity_guid}/orders/", json=post_data)
        assert r.status_code == 201, r.json
        order = Order.get(guid=r.json['guid'])
        assert order.current_state == OrderState.CREATED
        r = as_customer.put(f"/customers/{customer_identity_guid}/orders/{order.guid}/_{OrderAction.ARCHIVE.value}")
        assert r.status_code == 200, r.json

        # Test that order can not be archived if it's past CREATED state
        r = as_customer.post(f"/customers/{customer_identity_guid}/orders/", json=post_data)
        assert r.status_code == 201, r.json
        put_data = dict(
            payment_method=dict(
                guid=test_payment_method_stripe.guid,
                card='test_card',
            ),
            delivery_method=test_delivery_method.guid,
        )
        r = as_customer.put(f"/customers/{customer_identity_guid}/orders/{r.json['guid']}", json=put_data)
        assert r.status_code == 200, r.json
        r = as_customer.put(f"/customers/{customer_identity_guid}/orders/{r.json['guid']}/_place")
        assert r.status_code == 200, r.json
        order = Order.get(guid=r.json['guid'])
        assert order.current_state not in (OrderState.CREATED, OrderState.ARCHIVED)
        r = as_customer.put(f"/customers/{customer_identity_guid}/orders/{r.json['guid']}/_{OrderAction.ARCHIVE.value}")
        assert r.status_code == 409, r.json
        assert 'Order is not in \\"created\\" state, can not archive' in r.json['message']


@pytest.mark.usefixtures('mock_aws')
class TestApiShopsOrdersActions():
    """ Tests for PUT /shops/<shop_guid>/orders/<order_guid>/_place """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    def test_customer_order_place(self, mocker, order_finder, test_orders, customer, as_customer, customer_identity_id):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)
        mocker.patch.object(Order, 'status_invalid', False)

        order = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=OrderAction.PLACE),
                                        lambda order: order.buyer_identity_id == customer_identity_id,
                                        lambda order: order.customer.pin is None,
                                        orders=test_orders)
        start_state = order.current_state
        response_sent_at = datetime.now(timezone.utc)
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/_place")
        assert 200 == res.status_code, res.json
        order.refresh()
        assert start_state != order.current_state

        ordered_time = dateutil.parser.parse(res.json['timestamps']['ordered']).replace(tzinfo=pytz.UTC)
        assert response_sent_at <= ordered_time
        assert datetime.now(timezone.utc) >= ordered_time

        assert as_customer.token_payload['pin'] == order.customer.pin
        assert 'pin' not in res.json['customer']

    def test_customer_order_place_invalid(self, mocker, order_finder, test_orders, customer, as_customer, customer_identity_id):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)
        mocker.patch.object(Order, 'status_invalid', True)

        order = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=OrderAction.PLACE),
                                        lambda order: order.buyer_identity_id == customer_identity_id,
                                        lambda order: order.current_state == OrderState.CREATED,
                                        orders=test_orders)
        start_state = order.current_state
        res = as_customer.put(f"/customers/{customer_identity_id.split(':')[1]}/orders/{order.guid}/_{OrderAction.PLACE}")
        assert 409 == res.status_code, res.json
        assert 'Order is invalid for placement.' in res.json['message']
        order.refresh()
        assert start_state == order.current_state
