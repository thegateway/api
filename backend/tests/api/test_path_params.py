import pytest
from flask_apispec import MethodResource

from gwio.models import user

UserData = user.UserData


@pytest.fixture(scope='class')
def an_app(app):
    # pylint: disable=W0612
    @app.application.register_resource
    @app.application.path_params('foo', '/in', 'middle', '/param', 'bar')
    class TestAPI(MethodResource):

        @app.application.register_endpoint(
            UserData,
            autoload=False,
            output_schema=None,
            authorisation=None,
            description='',
        )
        def get(self, foo, middle, bar):
            return f'{foo}:{middle}:{bar}'

    @app.application.register_resource
    @app.application.base_path('/kukkakaali')
    @app.application.path_params('param')
    class AnOtherAPI(MethodResource):
        has_listing = True

        @app.application.register_endpoint(
            UserData,
            autoload=False,
            output_schema=None,
            authorisation=None,
            description='',
        )
        def get(self, param=None):
            if param is None:
                return r'kukkakaali on pahaa'
            return f'{param} on pahaa'

    yield app.application.test_client()


@pytest.mark.usefixtures('mock_aws', 'an_app')
class TestMethodResourceDecorators:
    def test_path_params(self, as_admin):
        r = as_admin.get('/test/1/in/2/param/3')
        assert 200 == r.status_code
        assert '"1:2:3"' == r.data.decode().strip()

    def test_path_params_with_slashes(self, as_admin):
        r = as_admin.get('/test/1/in/2/param/3/')
        assert 200 == r.status_code
        assert '"1:2:3"' == r.data.decode().strip()

    @pytest.mark.parametrize('url', ['/kukkakaali/papuruoka', '/kukkakaali/papuruoka/'])
    def test_base_path(self, as_admin, url):
        r = as_admin.get(url)
        assert 200 == r.status_code
        assert '"papuruoka on pahaa"' == r.data.decode().strip()

    def test_mid_param(self, simple_app):
        @simple_app.register_resource
        @simple_app.base_path('/testing')
        @simple_app.path_params('uuid:foo', '/first', 'uuid:bar')
        class FirstApi(MethodResource):
            def get(self, foo, bar):
                pass

        @simple_app.register_resource
        @simple_app.base_path('/testing')
        @simple_app.path_params('uuid:foo', '/second', 'uuid:bar')
        class SecondApi(MethodResource):
            def get(self, foo, bar):
                pass
