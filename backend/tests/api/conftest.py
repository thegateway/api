# -*- coding: utf-8 -*

# This needs to be run before importing *anything* that touches either "flask.current_app" or SSM.
# In other words: absolutely no imports from gwio until environment is set up.
import copy
import datetime
import os
import random
import re
import uuid
from decimal import Decimal
from functools import partial

import responses

from gwio import environment
from gwio.integrations.xpress_couriers import (
    OrderResponse,
    Service,
)
from . import consts

# Defining ssm variables so that tests don't break on not finding them
Env = environment.Env

# The settings marked as saved in secrets manager are defined in aws fixture
Env.AWS_LAMBDA_FUNCTION_NAME = 'testing-lambda-function-01'
Env.AWS_REGION = 'eu-west-1'
Env.COGNITO_CLIENT_ID = consts.COGNITO_CLIENT_ID
Env.COGNITO_POOL_ID = consts.COGNITO_POOL_ID
Env.DEFAULT_CURRENCY = "PLN"
Env.DEFAULT_LANGUAGE = 'en'
Env.ELASTICSEARCH_HOST = None
Env.FCM_NOTIFICATION_API_KEY = 'AA=='
Env.GOOGLE_MAPS_API_KEY = 'AA=='
Env.PAYU_CLIENT_ID = '368189'
Env.PAYU_CLIENT_SECRET = '03d84776f759f085b0aeb7d4dade8993'
Env.PAYU_COMMISSION_PERCENTAGE = Decimal('0.99')
Env.PAYU_COMMISSION_VAT_PERCENTAGE = 20
Env.PAYU_HOST = 'https://secure.snd.payu.com'
Env.PAYU_MARKETPLACE_ID = '123456'
Env.PAYU_NOTIFY_URL = '123456'
Env.PAYU_POS_ID = '368189'
Env.PAYU_SHOP_ID = '234567'
Env.PLATFORM_COMMISSION_VAT_PERCENTAGE = 20
Env.PLATFORM_PAYU_PAYMENT_PROCESSING_COMMISSION = '1.4'
Env.REDIS_HOST = 'localhost'
Env.STRIPE_API_KEY = None
Env.STRIPE_COMMISSION_VAT_PERCENTAGE = 20
Env.STRIPE_COUNTRY_CODE = 'pl'
Env.STRIPE_ENDPOINT_SECRET = consts.STRIPE_ENDPOINT_SECRET
Env.SWAGGER_UI_URL = None
Env.SYSTEM_EMAIL = consts.SYSTEM_EMAIL
Env.SYSTEM_SECRET_KEY = 'secret'
Env.UPLOAD_BUCKET = 's3://test_location_no_3'
Env.UPLOAD_LOCATION = 's3://test_location_no_5'
Env.VAPID_PRIVATE_KEY = consts.VAPID_PRIVATE_KEY
os.environ['XPC_PAYER_TEST_LOCATION'] = '{"name":"","address":"","city":"","post_code":"","country_code":""}'
Env.XPRESS_COURIERS_API_PASSWORD = 'xpress_api_password'
Env.XPRESS_COURIERS_API_URL = 'https://a_test_example_xpress_api_url.com/'
Env.XPRESS_COURIERS_API_USERNAME = 'xpress_api_username'
Env.XPRESS_COURIERS_CLIENT_NUMBER = 'xpress_client_number'

from gwio.core.utils.testing.helpers import fake

Env.COGNITO_FEDERATED_IDENTITY_POOL_ID = fake.md5()
Env.PAYU_SECOND_KEY = fake.md5()

# Spinning up mocks
from ._mock_services import mocked_services
from ._mocks import (
    MockAccount,
    MockAccountLink,
    MockCharge,
    MockCustomer,
    MockPaymentIntent,
    MockRefund,
    MockTransfer,
    mock_stripe_construct_event,
)
from gwio.core.utils.testing._fixtures import *

# Imports
import os

import pytest
from envs import env
from flask import json
from mock import mock
from typing import (
    Callable,
)

import gwio.models as gwio_models
import gwio.user as core_user
from . import _fixtures as test_fixtures
from gwio.utils.notify import Notification
from gwio.core.utils.testing.helpers import (
    fake,
    image_url,
)
from gwio.logic.orders.types import OrderState
from gwio.models import user
from gwio.models.bootstrap import Bootstrap
from ._fakes.stripe import generate_stripe_webhook_payload
from ._fixtures import *
from ._fixtures.orders import (
    OrderFinder,
    fake_order,
)
from ._helpers import *
from ._helpers.app import (
    App,
    setup_app,
)

assert mocked_services
assert test_fixtures
assert gwio_models
assert random_product
assert customer_guid  # To prevent auto reformat from removing `from gwio.core.utils.testing import *`

__copyright__ = "Copyright 2017, The Reseller Gateway oy"
__version__ = ""
ENVIRONMENT = Env.APPLICATION_ENVIRONMENT
VERTICAL = Env.VERTICAL

S = OrderState


@pytest.fixture(scope='class')
def simple_app():
    yield App('test_authentication_module_app', conf=dict(
        COGNITO_CLIENT_ID=Env.COGNITO_CLIENT_ID,
        COGNITO_POOL_ID=Env.COGNITO_POOL_ID,
    ))


# noinspection PyUnusedLocal
@pytest.fixture(scope='class', autouse=True)
def app(request):
    """Session-wide test `Flask` application."""
    yield setup_app()


@pytest.fixture(scope="session")
def faker():
    return fake


@pytest.fixture(scope="session")
def fake_image_url():
    return image_url


@pytest.fixture(scope="session")
def cognito_test_user():
    username = env('COGNITO_TEST_USER')
    password = env('COGNITO_TEST_PASSWORD')
    assert username, 'COGNITO_TEST_USER environment variable not set!'
    assert password, 'COGNITO_TEST_PASSWORD environment variable not set!'
    yield (dict(username=username, password=password))


# noinspection PyUnusedLocal
@pytest.fixture(scope='class', autouse=True)
def app(request):
    """Session-wide test `Flask` application."""
    yield test_app.setup_app()


@pytest.fixture(scope='session')
def setup_customer_request(customer_guid):
    def f(test_class):
        region = env('AWS_REGION', 'eu-west-1')
        user_pool_id = test_class.app.application.config['COGNITO_POOL_ID']
        test_class.token_payload = {
            'iss': test_app.ISS_PYTEST,
            'cognito:username': str(customer_guid),
        }

    return f


@pytest.fixture(scope='session')
def setup_admin_request():
    def f(test_class):
        region = env('AWS_REGION', 'eu-west-1')
        user_pool_id = test_class.app.application.config['COGNITO_POOL_ID']
        test_class.token_payload = {
            'iss': test_app.ISS_PYTEST,
            'cognito:groups': ['admin'],
        }

    return f


@pytest.fixture(scope='session')
def setup_shopkeeper_request(webshop_guid):
    def f(test_class, shop_id=webshop_guid):
        region = env('AWS_REGION', 'eu-west-1')
        user_pool_id = test_class.app.application.config['COGNITO_POOL_ID']
        test_class.token_payload = {
            'iss': test_app.ISS_PYTEST,
            "custom:organization_guid": shop_id,
            "name": "Veijo Farmaseutti",
            'cognito:groups': ['not_admin'],
        }

    return f


@pytest.fixture(scope='session')
def create_test_order() -> Callable:
    return fake_order


@pytest.fixture(scope='session')
def random_orders() -> Callable:
    def n_orders(n, **kwargs):
        return [fake_order(**kwargs) for _ in range(n)]

    return n_orders


@pytest.fixture(scope='class')
def order_finder():
    return OrderFinder


@pytest.fixture(scope='class')
def bootstrap_data(faker):
    def func(**attributes):
        return {
            'data': {
                'dashboard': {fake.word(): fake.sentence()},
                'info': faker.sentence(),
                'links': [faker.url() for _ in range(5)]
            },
            **attributes
        }

    return func


@pytest.fixture(scope='class')
def create_bootstrap(bootstrap_data):
    def func(**attributes):
        return Bootstrap(**bootstrap_data(**attributes))

    return func


@pytest.fixture(scope='session')
def stripe_webhook_payload():
    return generate_stripe_webhook_payload()


@pytest.fixture(scope='class')
def mock_stripe():
    mocked_stripe = dict(retrieve_charge=mock.patch('stripe.Charge.retrieve', new=MockCharge.get_instance),
                         create_customer=mock.patch('stripe.Customer.create', new=MockCustomer),
                         charge=mock.patch('stripe.Charge.create', new=MockCharge.get_instance),
                         create_refund=mock.patch('stripe.Refund.create', new=MockRefund),
                         create_intent=mock.patch('stripe.PaymentIntent.create', new=MockPaymentIntent.create),
                         retrieve_intent=mock.patch('stripe.PaymentIntent.retrieve', new=MockPaymentIntent.retrieve),
                         capture_intent=mock.patch('stripe.PaymentIntent.capture', new=MockPaymentIntent.capture),
                         create_transfer=mock.patch('stripe.Transfer.create', new=MockTransfer.create),
                         create_account=mock.patch('stripe.Account.create', new=MockAccount.create),
                         retrieve_account=mock.patch('stripe.Account.retrieve', new=MockAccount.retrieve),
                         update_account=mock.patch('stripe.Account.modify', new=MockAccount.modify),
                         add_bank_account=mock.patch('stripe.Account.create_external_account', new=MockAccount.create_external_account),
                         create_account_link=mock.patch('stripe.AccountLink.create', new=MockAccountLink.create),
                         construct_event=mock.patch('stripe.Webhook.construct_event', new=mock_stripe_construct_event),
                         retrieve_payment_method=mock.patch('stripe.PaymentMethod.retrieve', new=lambda *args, **kwargs: dict(card=dict(last4='4242'))),
                         mock_payment_intent=MockPaymentIntent)
    for stripe_mock in mocked_stripe.values():
        if isinstance(stripe_mock, mock._patch):
            stripe_mock.start()
    yield mocked_stripe
    for stripe_mock in mocked_stripe.values():
        if isinstance(stripe_mock, mock._patch):
            stripe_mock.stop()


class MockLogisticsApi:
    def __init__(self, *args, **kwargs):
        pass

    def get_available_services(self, *args, **kwargs):
        return dict(
            available_services=[
                Service(
                    id=89,
                    max_delivery_time=datetime.datetime.now() + datetime.timedelta(days=3),
                    max_pickup_time=datetime.datetime.now() + datetime.timedelta(days=1),
                    name='Test delivery method #1',
                    price_with_tax=Decimal(10),
                    price_without_tax=Decimal(8),
                    tax=Decimal(2),
                ),
                Service(
                    id=81,
                    max_delivery_time=datetime.datetime.now() + datetime.timedelta(days=1),
                    max_pickup_time=datetime.datetime.now() + datetime.timedelta(hours=4),
                    name='Test delivery method #2',
                    price_with_tax=Decimal(20),
                    price_without_tax=Decimal(15),
                    tax=Decimal(5)
                ),
            ])

    def create_order(self, **kwargs):
        params = dict(
            no=str(random.randint(10 ** 6, (10 ** 7) - 1)),
            letter_no=str(random.randint(10 ** 11, (10 ** 12) - 1)),
            mime_data=bytes('this is faking pdf data', 'utf-8'),
            ready_date=datetime.datetime.now() + datetime.timedelta(hours=3),
            price_elements=None,
        )
        try:
            params['order_price'] = kwargs['cash_on_delivery'].amount
        except KeyError:
            params['order_price'] = Decimal(0)
        return OrderResponse(
            **params
        )


@pytest.fixture(scope='session')
def mock_xpress_couriers():
    mocked_xpress_couriers = dict(
        logistics_api=mock.patch('gwio.integrations.xpress_couriers.LogisticsAPI', wraps=MockLogisticsApi),
        city_from_postal_code=mock.patch('gwio.logic.deliveries.xpress_couriers.city_from_postal_code', new=lambda *args, **kwargs: fake.city()),
    )
    for xpress_couriers_mock in mocked_xpress_couriers.values():
        xpress_couriers_mock.start()
    yield mocked_xpress_couriers
    for xpress_couriers_mock in mocked_xpress_couriers.values():
        xpress_couriers_mock.stop()


class Payu():
    def __init__(self):
        self.orders = dict()
        self.submerchants = dict()

    def new_order(self, request_body):
        order_id = str(uuid.uuid4())
        request_body['orderId'] = order_id
        request_body['orderCreateDate'] = datetime.datetime.now().isoformat()
        request_body['status'] = "NEW"
        self.orders[order_id] = request_body
        return order_id

    def get_order(self, order_id):
        return self.orders[order_id]

    def delete_order(self, order_id):
        return self.orders.pop(order_id)

    def add_submerchant(self, data):
        self.submerchants[str(data['submerchant_id'])] = data

    def submerchant_status(self, submerchant_id):
        submerchant = copy.deepcopy(self.submerchants[submerchant_id])
        del submerchant['submerchant_id']
        return submerchant

    def submerchant_balance(self, submerchant_id):
        submerchant = self.submerchants[submerchant_id]
        return copy.deepcopy(submerchant['balance'])

    def payout(self, submerchant_id, amount):
        submerchant = self.submerchants[submerchant_id]
        for amount_field in ['availableAmount', 'totalAmount']:
            submerchant['balance'][amount_field] = submerchant['balance'][amount_field] - amount


def payu_order_get(payu_db, request):
    headers = {'Content-Type': 'application/json'}
    try:
        payu_order = payu_db.get_order(request.path_url.split('/')[-1])
    except KeyError:
        return (404, headers, '')

    body = json.dumps(dict(
        orders=[payu_order],
        status=dict(statusCode="SUCCESS",
                    statusDesc='Request processing successful')))
    status = 200
    return (status, headers, body)


def payu_order_delete(payu_db, request):
    headers = {'Content-Type': 'application/json'}
    try:
        payu_order = payu_db.delete_order(request.path_url.split('/')[-1])
    except KeyError:
        return (404, headers, '')

    status = 200
    body = json.dumps(dict(
        orderId=payu_order['orderId'],
        extOrderId=payu_order['extOrderId'],
        status=dict(statusCode="SUCCESS"),
    ))
    return (status, headers, body)


def payu_order_post(payu_db, request):
    request_body = json.loads(request.body)
    order_id = payu_db.new_order(request_body)
    body = json.dumps(
        dict(
            status=dict(
                statusCode="SUCCESS"
            ),
            redirectUri="payment_summary_redirection_url",
            orderId=order_id,
            extOrderId=request_body['extOrderId'],
        ))
    status = 302
    headers = dict(location='http://redirect.url/')
    return (status, headers, body)


def payu_submerchant_status(payu_db, request):
    headers = {'Content-Type': 'application/json'}
    submerchant_id = request.path_url.split('/')[-2]
    try:
        submerchant = payu_db.submerchant_status(submerchant_id)
        return (200, headers, json.dumps(submerchant))
    except KeyError:
        return (404, headers, '')


def payu_submerchant_balance(payu_db, request):
    headers = {'Content-Type': 'application/json'}
    submerchant_id = request.path_url.split('/')[-2]
    try:
        balance = payu_db.submerchant_balance(submerchant_id)
        return (200, headers, json.dumps(dict(balance=balance, status=dict(statusCode="SUCCESS"))))
    except KeyError:
        return (404, headers, '')


def payu_payout(payu_db, request):
    request_body = json.loads(request.body)
    headers = {'Content-Type': 'application/json'}
    params = dict(submerchant_id=request_body['account']['extCustomerId'],
                  amount=request_body['payout']['amount'])
    try:
        payu_db.payout(**params)
        return (200, headers, json.dumps(dict(payout=dict(payoutId=uuid.uuid4()), status=dict(statusCode="SUCCESS"))))
    except KeyError:
        return (404, headers, '')


@pytest.fixture(scope='class', autouse=True)
def mock_payu(webshop_guid):
    payu_db = Payu()
    order_re = re.compile(f'{Env.PAYU_HOST}/api/v2_1/orders/\w+')
    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
        rsps.add(responses.POST,
                 f'{Env.PAYU_HOST}/pl/standard/user/oauth/authorize',
                 body=json.dumps(dict(
                     access_token="7524f96e-2d22-45da-bc64-778a61cbfc26",
                     token_type="bearer",
                     expires_in=43199,
                     grant_type="client_credentials"
                 )),
                 status=200,
                 content_type='application/json')
        rsps.add_callback(responses.POST,
                          f'{Env.PAYU_HOST}/api/v2_1/orders/',
                          content_type='application/json',
                          callback=partial(payu_order_post, payu_db))
        rsps.add_callback(responses.GET,
                          order_re,
                          content_type='application/json',
                          callback=partial(payu_order_get, payu_db))
        rsps.add_callback(responses.DELETE,
                          order_re,
                          content_type='application/json',
                          callback=partial(payu_order_delete, payu_db))
        rsps.add_callback(responses.GET,
                          f'{Env.PAYU_HOST}/api/v2_1/customers/ext/{webshop_guid}/status',
                          content_type='application/json',
                          callback=partial(payu_submerchant_status, payu_db),
                          match_querystring=False)
        rsps.add_callback(responses.GET,
                          f'{Env.PAYU_HOST}/api/v2_1/customers/ext/{webshop_guid}/balances',
                          content_type='application/json',
                          callback=partial(payu_submerchant_balance, payu_db),
                          match_querystring=False)
        rsps.add_callback(responses.POST,
                          f'{Env.PAYU_HOST}/api/v2_1/payouts',
                          content_type='application/json',
                          callback=partial(payu_payout, payu_db))
        yield dict(rsps=rsps, payu_db=payu_db)


@pytest.fixture(autouse=True, scope='function')
def setup_basic_data(
        mock_aws,
        as_customer,
        customer_guid,
        customer_identity_id,
        customer_identity_guid,
        random_notification_subscription_payload,
        mock_fcm,
        mock_webpush,
        mock_email,
):
    try:
        user.UserData.get(guid=customer_guid)
    except user.UserData.DoesNotExist:
        user_data = user.UserData(
            guid=customer_guid,
            identity_id=customer_identity_id,
        )
        user_data.save()
    user_ = core_user.User.by_identity_id(customer_identity_guid)

    if 'types' not in user_.data.notifications or not user_.data.notifications['types']:
        res = as_customer.post(f'/notifications/types', json=dict(notification_types=[
            random_notification_subscription_payload(x.value)['notification_types'] for x in Notification.Type
        ]))
        assert res.status_code < 300, res.json['message']
        user_.data.refresh()
        assert user_.data.notifications['types']

    def mocked_get_texts_dict(*_, **__):
        try:
            path = json.loads(open(os.path.join(os.getcwd(), 'templates', 'notifications.json'), 'r').read())
        except FileNotFoundError:
            path = json.loads(open(os.path.join(os.getcwd(), '..', 'tests', 'api', 'templates', 'notifications.json'), 'r').read())
        return path

    mock_event_publish = mock.patch('gwio.utils.notify.Notification.get_texts_dict', new=mocked_get_texts_dict)
    mock_event_publish.start()


@pytest.fixture(scope='session', autouse=True)
def mock_stripe_commissions():
    def mocked_get_commission(payment):
        return payment.amount * Decimal('0.02'), payment.currency

    def mocked_get_platform_payment_commission(payment):
        by_shop = dict()
        for shop_summary in payment.order.summary.shops:
            by_shop[shop_summary.shop_guid] = shop_summary.price * Decimal('0.02')
        return by_shop

    mock_stripe_commission = mock.patch('gwio.logic.payments.stripe.get_payment_platform_commission', new=mocked_get_commission)
    mock_stripe_commission.start()

    mock_platform_commission = mock.patch('gwio.logic.payments.stripe.get_platform_payment_commission', new=mocked_get_platform_payment_commission)
    mock_platform_commission.start()
