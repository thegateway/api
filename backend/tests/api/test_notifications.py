from enum import IntEnum

import pytest
from faker import Faker
from flask import (
    g,
    json,
)
import gwio.user
from gwio.logic.orders.types import OrderNotificationType
from gwio.utils import (
    uuid,
    notify,
)

GenericUserNotification = notify.GenericUserNotification
Notification = notify.Notification
NotificationFailException = notify.NotificationFailException
UserNotification = notify.UserNotification
User = gwio.user.User

TEST_TOPIC = 'test_topic'


class MockUser:
    received = None

    def __init__(self):
        self.received = set()

    def notify(self, notification):
        self.received.add(notification.ctx['notification_guid'])


class MockNotificationType(IntEnum):
    MOCK = 1


mocked_notification_text = 'Mock notification'


class MockNotification(UserNotification):
    type = MockNotificationType.MOCK

    def title(self):
        return mocked_notification_text

    def email_html(self):
        return mocked_notification_text

    def email_text(self):
        return mocked_notification_text

    def simple_body(self):
        return mocked_notification_text


def test_notifications(faker):
    user = MockUser()
    notification_guid = uuid.uuid4()
    context = dict(
        user=user,
        source=faker.email(),
        title=faker.sentence(),
        notification_guid=notification_guid)
    for handler in Notification.from_type(MockNotificationType.MOCK):
        handler.emit(context)
    assert notification_guid in user.received


@pytest.fixture(scope='function')
def user_with_notification_subscription_and_topic(mock_aws, faker):
    assert mock_aws

    def add_subscription(*, topic=None, notification_type=None):
        user = User(guid=uuid.uuid4(), organization_guid=None)
        user.notifications = {}
        user.add_notification_topic([topic])
        user.add_notification_types([dict(type=notification_type, data=dict(registration_key=faker.sha1(), endpoint=faker.url()))])
        return user

    return add_subscription


@pytest.fixture(scope='function')
def mock_notification_render_template(mocker, faker):
    sentence = faker.sentence()

    def mock_render(*_, **__):
        return sentence

    mocker.patch.object(Notification, '_render_template', mock_render)
    return sentence


class TestNotification:
    def test_icon(self, faker):
        ctx = dict(
            user=None,
            source=faker.email(),
            title=faker.sentence(),
        )
        x = MockNotification(ctx)
        assert not x.icon
        test_icon = faker.url()
        ctx['icon'] = test_icon
        x = MockNotification(ctx)
        assert test_icon == x.icon


@pytest.mark.usefixtures('mock_aws')
@pytest.mark.usefixtures('mock_notification_render_template')
class TestNotify:

    @staticmethod
    def notification_context(user):
        fake = Faker('fi_FI')
        return dict(
            user=user,
            source=fake.email(),
            topic='order_updates',
            title='Test',
            body='A test notification',
            icon='./assets/images/icon.png')

    def test_send_email_notification(self, app, mock_email, faker):
        email = faker.email()
        source = faker.email()
        subscription = dict(
            type='email',
            data=dict(email_address=email))
        notification = GenericUserNotification(dict(
            user=User(guid=uuid.uuid4()),
            source=source,
            title=faker.sentence(nb_words=5),
            body='\n'.join(faker.paragraphs(nb=3)),
        ))
        with app.application.app_context():
            g.aws_event_source = 'test'
            notification.send([subscription])

        event_payload = json.loads(mock_email.eb_event['Detail'])['payload']
        assert email == event_payload['dst']
        assert source == event_payload['src']
        assert notification.type == notification.type.__class__(event_payload['type'])
        assert event_payload['context']

    @pytest.mark.usefixtures('create_test_order', 'create_random_webshop')
    def test_webshop_email_notification(self, faker, create_test_order, app, create_random_webshop, test_customer, mock_email):
        with app.application.test_request_context():
            g.aws_event_source = 'test'
            webshop = create_random_webshop()
            user = User(guid=uuid.uuid4())

            context = dict(
                order=create_test_order(),
                user=user,
                shops=[webshop]
            )
            notification = Notification.from_type(OrderNotificationType.ORDER_RECEIVED)
            for x in notification:
                x.emit(context)
        event_payload = json.loads(mock_email.eb_event['Detail'])['payload']
        assert webshop.email == event_payload['dst']

    def test_fcm_notify(self, mock_aws, faker, mock_fcm, user_with_notification_subscription_and_topic):
        assert mock_aws
        user = user_with_notification_subscription_and_topic(topic=TEST_TOPIC, notification_type=Notification.Type.FCM_PUSH.value)
        user.notify(GenericUserNotification(self.notification_context(user)))
        assert mock_fcm.notified

    def test_webpush_notify(self, mock_webpush, user_with_notification_subscription_and_topic):
        user = user_with_notification_subscription_and_topic(topic=TEST_TOPIC, notification_type=Notification.Type.WEBPUSH.value)
        user.notify(GenericUserNotification(self.notification_context(user)))
        assert mock_webpush.webpush_called

    def test_empty_notifications_notify(self, mock_webpush, user_with_notification_subscription_and_topic):
        user = user_with_notification_subscription_and_topic(topic=TEST_TOPIC, notification_type=Notification.Type.WEBPUSH.value)
        user.notify(GenericUserNotification(self.notification_context(user)))
        assert mock_webpush.webpush_called

    def test_multiple_push_notify(self, user_with_notification_subscription_and_topic, mock_webpush, mock_fcm):
        _ = user_with_notification_subscription_and_topic(topic=TEST_TOPIC, notification_type=Notification.Type.FCM_PUSH.value)
        user = user_with_notification_subscription_and_topic(topic=TEST_TOPIC, notification_type=Notification.Type.WEBPUSH.value)
        user.notify(GenericUserNotification(self.notification_context(user)))
        assert mock_webpush.webpush_called
        assert mock_fcm.notified

    def test_remove_not_working_subscription(self, mock_fcm, user_with_notification_subscription_and_topic):
        user = user_with_notification_subscription_and_topic(topic=TEST_TOPIC, notification_type=Notification.Type.FCM_PUSH.value)
        assert 1 == len(user.notifications['types'])
        mock_fcm.exception = NotificationFailException("test exception")
        user.notify(GenericUserNotification(self.notification_context(user)))
        user = User(user.guid, organization_guid=None)
        assert mock_fcm.notified
        assert 0 == len(user.notifications['types'])


@pytest.mark.usefixtures('mock_aws')
class TestNotificationAPI:
    def test_add_user_notification_subscription(self, as_customer, random_notification_subscription_payload):
        payload = random_notification_subscription_payload(Notification.Type.FCM_PUSH.value)
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 204 == res.status_code

        payload = random_notification_subscription_payload(Notification.Type.WEBPUSH.value)
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 204 == res.status_code

    def test_add_duplicate_user_notification_subscription(self, customer_guid, as_customer, random_notification_subscription_payload):
        user = User(customer_guid, organization_guid=None)
        user.notifications = {}
        payload = random_notification_subscription_payload(Notification.Type.FCM_PUSH.value)
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 204 == res.status_code
        assert 1 == len(User(customer_guid).notifications['types'])
        assert [payload['notification_types']] == User(customer_guid).notifications['types']
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 204 == res.status_code
        assert 1 == len(User(customer_guid).notifications['types'])

    def test_add_user_notification_subscription_bad_data(self, as_customer, faker, random_notification_subscription_payload):
        payload = random_notification_subscription_payload(notification_type=faker.word())
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 409 == res.status_code

        payload = random_notification_subscription_payload(Notification.Type.FCM_PUSH.value,
                                                           surely_not_registration_key=faker.sha256())
        del payload['notification_types']['data']['registration_key']
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 409 == res.status_code

        payload = random_notification_subscription_payload(Notification.Type.WEBPUSH.value,
                                                           surely_not_endpoint=faker.uri())
        del payload['notification_types']['data']['endpoint']
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 409 == res.status_code

    def test_get_user_notification_subscriptions(self, customer_guid, as_customer, random_notification_subscription_payload):
        user = User(customer_guid, organization_guid=None)
        user.notifications = {}
        payload = random_notification_subscription_payload(Notification.Type.FCM_PUSH.value)
        res = as_customer.post(f'/notifications/types', json=payload)
        assert 204 == res.status_code

        res = as_customer.get(f'/notifications/types')
        assert 200 == res.status_code
        assert 1 == len(res.json)
        assert payload['notification_types'] == res.json[0]

    def test_get_user_notifications_no_notifications(self, as_admin):
        res = as_admin.get(f'/notifications/types')
        assert 200 == res.status_code
        assert 0 == len(res.json)
