import json
import random
import uuid

import pytest
from flask import Response
from mock import mock
from stripe.error import StripeError

from gwio.environment import Env
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.logic.packages.handler import PackageState
from gwio.logic.payments import stripe
from gwio.logic.payments.handler import (
    PaymentState,
)
from gwio.logic.payments.types import (
    PaymentTypes,
)
from gwio.operations.orders import update_order
from ._mocks import MockPaymentIntent


@pytest.fixture(scope='function')
def test_order(random_webshop,
               create_random_payment_method,
               create_test_order,
               create_random_delivery_method,
               test_userdata_shortlived,
               random_products,
               faker):
    products = random_products(3, owner_guid=random_webshop.guid)
    for product in products:
        product.type = 'medicine'  # To avoid review
        product.save()
    random_webshop.save()
    payment_method = create_random_payment_method(type=PaymentTypes.STRIPE)
    payment_method.save()
    test_userdata_shortlived.stripe_id = str(uuid.uuid4())
    test_userdata_shortlived.save()
    delivery_method = create_random_delivery_method(owner_guid=random_webshop.guid)
    delivery_method.save()
    order = create_test_order(shop_guid=random_webshop.guid,
                              buyer_identity_id=test_userdata_shortlived.identity_id,
                              data=dict(stripe=dict(card_id='tok_visa')),
                              statement_descriptor='Moneys gone',
                              payment_method_guid=payment_method.guid,
                              _current_state=OrderState.START,
                              shipping_address=dict(name=faker.name(), phone=faker.phone_number()))
    products_for_order = [dict(guid=delivery_method.guid, qty=1, shop_guid=delivery_method.owner_guid)]
    products_for_order.extend([dict(guid=product.guid, qty=random.randint(1, 5), shop_guid=product.owner_guid) for product in products])
    update_order(order, products=products_for_order)
    order.process(OrderAction.CREATE)
    order.save()
    yield order
    order.delete()
    delivery_method.delete()
    test_userdata_shortlived.delete()
    payment_method.delete()
    random_webshop.delete()
    for product in products:
        product.delete()


@pytest.mark.usefixtures('mock_stripe')
class TestStripeAccount:
    def test_stripe_no_account(self, webshop, as_shopkeeper, faker):
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/accept_tos')
        assert 409 == res.status_code

        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/add_external_account',
                                 json=dict(external_account_token=f'btok_{faker.md5()[0:13]}'))
        assert 204 == res.status_code

        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/stripe/requires_action')
        assert 204 == res.status_code

    def test_get_stripe_account_link(self, webshop, stripe_payment_method, as_shopkeeper, faker):
        payload = dict(success_url=faker.url(),
                       failure_url=faker.url(),
                       link_type='custom_account_verification',
                       capabilities=['card_payments', 'transfers'],
                       accept_tos=True)
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/account_link', json=payload)
        assert 200 == res.status_code
        assert res.json['link_url'] is not None

    def test_stripe_accept_tos(self, webshop, as_shopkeeper):
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/accept_tos')
        assert 200 == res.status_code

    def test_stripe_add_bank_connection(self, webshop, as_shopkeeper, faker):
        payload = dict(success_url=faker.url(),
                       failure_url=faker.url(),
                       link_type='custom_account_verification',
                       capabilities=['card_payments', 'transfers'],
                       accept_tos=True)
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/account_link', json=payload)

        assert 200 == res.status_code
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/add_external_account',
                                 json=dict(external_account_token=f'btok_{faker.md5()[0:13]}'))
        assert 200 == res.status_code

    def test_stripe_get_requires_action(self, webshop, as_shopkeeper, faker):
        payload = dict(success_url=faker.url(),
                       failure_url=faker.url(),
                       link_type='custom_account_verification',
                       capabilities=['card_payments', 'transfers'],
                       accept_tos=True)
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/stripe/account_link', json=payload)
        assert 200 == res.status_code

        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/stripe/requires_action')
        assert 200 == res.status_code
        for key, types in [('current_deadline', (int, type(None))), ('currently_due', (list,)), ('disabled_reason', (str, type(None))),
                           ('eventually_due', (list,)), ('past_due', (list,)), ('pending_verification', (list,))]:
            assert type(res.json[key]) in types

@pytest.mark.usefixtures('mock_stripe')
class TestStripePayment:
    def test_successful_stripe_charge_capture(self, app, test_order):
        # Create charge
        test_order.current_state = OrderState.PROCESSING
        payment = test_order.payment_method.payment_method.new_payment_from_order(order=test_order)

        # Create capture
        with app.application.test_request_context():
            app.application.preprocess_request()  # For log events to not break
            stripe.create_charge(payment=payment)
            payment.current_state = PaymentState.WAITING_PAYMENT
            stripe.finalize_payment(payment=test_order.payments[0])
            app.application.process_response(Response())  # To make sure no issues with saving log events
        assert PaymentState.COMPLETED == test_order.payments[0].current_state
        assert '4242' == test_order.payment_data['last4']

    def test_stripe_charge_capture_requires_action(self, app, test_order):
        MockPaymentIntent.next_response(MockPaymentIntent.confirm_responses['requires_action'])
        # Create charge
        payment = test_order.payment_method.payment_method.new_payment_from_order(order=test_order)
        stripe.create_charge(payment=payment)
        assert test_order.payments[0].data.intent_id is not None

        # Create capture
        with app.application.test_request_context():
            app.application.preprocess_request()  # For log events to not break
            stripe.finalize_payment(payment=test_order.payments[0])
            app.application.process_response(Response())  # To make sure no issues with saving log events
            test_order.refresh()
        assert PaymentState.WAITING_SCA == test_order.payments[0].current_state
        assert test_order.payments[0].data.intent_id is not None

    @pytest.mark.skip(reason='Not implemented. Only needed for non registered users')
    def test_stripe_unsuccessful_create_customer_successful_charge_capture(self, test_order):
        payment = test_order.payment_method.new_payment_from_order(order=test_order)

        # Create charge
        with pytest.raises(StripeError):
            with mock.patch('stripe.Customer.create') as mock_customer_create:
                mock_customer_create.side_effect = StripeError('Test exception')
                stripe.create_charge(payment=payment)

        assert not test_order.payments

    def test_stripe_unsuccessful_charge_successful_charge_capture(self, test_order):
        payment = test_order.payment_method.payment_method.new_payment_from_order(order=test_order)

        # Create charge
        with pytest.raises(StripeError):
            with mock.patch('stripe.PaymentIntent.create') as mock_charge_create:
                mock_charge_create.side_effect = StripeError('Test exception')
                stripe.create_charge(payment=payment)

        assert payment.current_state == PaymentState.NOT_STARTED

    def test_stripe_unsuccessful_charge_unsuccessful_capture(self, test_order):
        payment = test_order.payment_method.payment_method.new_payment_from_order(order=test_order)

        # Create charge
        with pytest.raises(StripeError):
            with mock.patch('stripe.PaymentIntent.create') as mock_payment_intent_create:
                mock_payment_intent_create.side_effect = StripeError('Test exception')
                stripe.create_charge(payment=payment)

        # Create capture
        with pytest.raises(ValueError, match='has not been charged'):
            stripe.finalize_payment(payment=payment)

        assert payment.current_state == PaymentState.NOT_STARTED

    def test_stripe_charge_unsuccessful_capture_successful_capture(self, app, test_order):
        payment = test_order.payment_method.payment_method.new_payment_from_order(order=test_order)
        test_order.current_state = OrderState.PROCESSING

        # Create charge
        with app.application.test_request_context():
            app.application.preprocess_request()  # For log events to not break

            # Create capture
            with mock.patch('stripe.PaymentIntent.capture') as mock_payment_intent_capture:
                mock_payment_intent_capture.side_effect = StripeError('Test exception')
                stripe.create_charge(payment=payment)

            assert PaymentState.WAITING_PAYMENT == payment.current_state

            stripe.finalize_payment(payment=payment)
            app.application.process_response(Response())  # To make sure no issues with saving log events

        assert PaymentState.COMPLETED == payment.current_state

    def test_stripe_charge_refund(self, app, test_order):
        payment = test_order.payment_method.payment_method.new_payment_from_order(order=test_order)
        test_order.current_state = OrderState.PROCESSING

        with app.application.test_request_context():
            app.application.preprocess_request()
            # Create charge
            stripe.create_charge(payment=payment)
            payment.new_refund(payment.amount)
            # stripe.cancel_charge(payment=payment)
        refund_payment = next(x for x in payment.order.payments if x.amount < 0)
        assert PaymentState.COMPLETED == refund_payment.current_state

    def test_stripe_capture_place_order(self, app, test_order):
        # Create charge
        with app.application.test_request_context():
            app.application.preprocess_request()  # For log events to not break
            test_order.process(OrderAction.PLACE)
            app.application.process_response(Response())  # To make sure no issues with saving log events

        assert OrderState.PROCESSING == test_order.current_state
        assert PaymentState.COMPLETED == test_order.payments[0].current_state
        assert PackageState.WAITING_PACKING == test_order.packages[0].current_state


class TestStripeAPI:
    def test_stripe_webhook(self, mock_stripe, as_anonymous, test_order, stripe_webhook_payload):
        payload, headers = stripe_webhook_payload(stripe_key=Env.STRIPE_ENDPOINT_SECRET)
        response = as_anonymous.post('/stripe/webhook', data=payload, headers=headers)
        assert 200 == response.status_code, response.json
        assert 'OK' in json.dumps(response.json)

    def test_stripe_webhook_payment_failed(self, mock_stripe, as_anonymous, test_order, stripe_webhook_payload):
        payload, headers = stripe_webhook_payload(stripe_key=Env.STRIPE_ENDPOINT_SECRET, intent_type='payment_intent.payment_failed')
        response = as_anonymous.post('/stripe/webhook', data=payload, headers=headers)
        assert 200 == response.status_code, response.json
        assert 'OK' in json.dumps(response.json)

    def test_stripe_webhook_invalid_signature(self, mock_stripe, as_anonymous, test_order, stripe_webhook_payload):
        payload, headers = stripe_webhook_payload(stripe_key=Env.STRIPE_ENDPOINT_SECRET)
        headers['Stripe-Signature'] = 'a_failure_for_tests'
        response = as_anonymous.post('/stripe/webhook', data=payload, headers=headers)
        assert 400 == response.status_code, response.json
        assert 'Invalid signature' in json.dumps(response.json)
