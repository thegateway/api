from decimal import Decimal

import pytest

from gwio.logic.orders.handler import OrderHandler
from gwio.logic.orders.types import OrderAction, OrderState
from gwio.logic.payments.types import PaymentTypes
from gwio.operations.orders import create_order


@pytest.mark.usefixtures('mock_aws')
class TestOrderPayment:
    def ignore_test_pay_order_stripe(self, test_webshop, test_userdata_shortlived, random_products, mock_stripe, create_random_payment_method):
        # Create order
        # Add lines
        payment_method = create_random_payment_method(payment_type=PaymentTypes.STRIPE)
        payment_method.save()
        test_webshop.save()
        products = random_products(3)
        for product in products:
            product.save()
        product_inputs = [dict(guid=product.uuid, qty=Decimal(1)) for product in products]
        order = create_order(products=product_inputs, webshop_guid=test_webshop.guid, customer_guid=test_userdata_shortlived.guid)
        order.payment_method_guid = payment_method.guid
        order.data['summary'] = dict(order_price=4.2)
        order.data['summary'] = dict(currency='eur')
        order.data['stripe_payment_source'] = 'tok_visa'
        order.save()
        # Send order
        order.process(OrderAction.PLACE)
        order.save()
        assert OrderState.COMPLETED == order.current_state

        # Check payment
