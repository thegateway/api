import random
import uuid

import pytest

from gwio.core.utils.testing.helpers import as_roles
from gwio.models.brands import Brand
from gwio.models.tags import (
    TagType,
    Tag,
)
from ._fakes.brands import random_brand_data


@pytest.mark.usefixtures('mock_aws')
@pytest.fixture(scope='class')
def brands_suite(create_random_brands):
    for brand in create_random_brands(random.randint(10, 20)):
        brand.save()


@pytest.mark.usefixtures('mock_aws', 'brands_suite')
class TestBrandsAPI():
    def test_brand_crud_as_admin(self, as_admin, as_customer):
        payload = Brand.Schemas.Post().dump(random_brand_data())
        r = as_admin.post(f'/brands/', json=payload)
        assert 201 == r.status_code, r.json
        created = r.json
        guid = created.pop('guid')
        assert None is not guid
        assert 1 == len(r.json['tags'])
        tag = Tag.by_guid(uuid.UUID(r.json['tags'][0]['guid']))
        assert TagType.BRAND == tag.type
        assert uuid.UUID(guid) == tag.owner
        r = as_customer.get(f'/brands/{guid}')
        assert 200 == r.status_code
        gotten = r.json
        assert guid == gotten.pop('guid')
        assert created == r.json
        update_payload = Brand.Schemas.Put().dump(random_brand_data())
        r = as_admin.put(f'/brands/{guid}', json=update_payload)
        assert 200 == r.status_code
        updated = r.json
        assert guid == updated.pop('guid')
        assert all(y == updated[x] for x, y in update_payload.items())

        update_payload = Brand.Schemas.Put().dump(random_brand_data())
        update_payload['tags'] = r.json['tags']
        r = as_admin.put(f'/brands/{guid}', json=update_payload)
        assert 200 == r.status_code, r.json

        r = as_admin.delete(f'/brands/{guid}')
        assert 200 == r.status_code, r.json
        r = as_customer.get(f'/brands/{guid}')
        assert 404 == r.status_code, r.json

    def test_brand_crud_as_brand_owner(self, as_admin, create_brand_owner_client, as_customer):
        payload = Brand.Schemas.Post().dump(random_brand_data())
        r = as_admin.post(f'/brands/', json=payload)
        assert 201 == r.status_code, r.json
        created = r.json
        guid = created.pop('guid')
        assert None is not guid
        r = as_customer.get(f'/brands/{guid}')
        assert 200 == r.status_code, r.json
        gotten = r.json
        assert guid == gotten.pop('guid')
        as_brand_owner = create_brand_owner_client(guid)
        assert created == r.json
        update_payload = Brand.Schemas.Put().dump(random_brand_data())
        r = as_brand_owner.put(f'/brands/{guid}', json=update_payload)
        assert 200 == r.status_code
        updated = r.json
        assert guid == updated.pop('guid')
        assert all(y == updated[x] for x, y in update_payload.items())
        r = as_brand_owner.delete(f'/brands/{guid}')
        assert 200 == r.status_code
        r = as_customer.get(f'/brands/{guid}')
        assert 404 == r.status_code

    @pytest.mark.usefixtures('brands_suite')
    def test_put_brand_owner_wrong_brand(self, as_brand_owner):
        brand = random.choice(list(Brand.scan()))
        r = as_brand_owner.put(f'/brands/{brand.guid}', json=Brand.Schemas.Post().dump(random_brand_data()))
        assert 403 == r.status_code

    @as_roles(pytest, get=('as_role', 'guid'), skip={'admin'})
    def test_post_no_access(self, as_role, guid):
        r = as_role.post(f'/brands/', json=Brand.Schemas.Post().dump(random_brand_data()))
        assert 403 if guid else 401 == r.status_code

    def test_list_brands(self, as_admin, as_customer):
        create_schema = Brand.Schemas.Post()
        first_payload = create_schema.dump(random_brand_data())
        r = as_admin.post(f'/brands/', json=first_payload)
        assert 201 == r.status_code, r.json
        first_payload['guid'] = r.json['guid']
        second_payload = create_schema.dump(random_brand_data())
        r = as_admin.post(f'/brands/', json=second_payload)
        assert 201 == r.status_code, r.json
        second_payload['guid'] = r.json['guid']
        r = as_customer.get('/brands/')
        assert 403 == r.status_code, r.json
        r = as_admin.get('/brands/')
        assert 200 == r.status_code, r.json
        assert len(list(Brand.scan())) == len(r.json)
        for payload in (first_payload, second_payload):
            assert any(all(y == res[x] for x, y in payload.items()) for res in r.json)
