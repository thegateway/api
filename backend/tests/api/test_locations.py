import random
import uuid
from datetime import datetime, timezone

import pytest
from dateutil.parser import parse
from pytest_lazyfixture import LazyFixture

from gwio.models.locations import Location
from gwio.models.tags import Tag
from ._fixtures.locations import create_random_location
from ._fakes.locations import random_location_data
from .test_tags import tags_suite

assert create_random_location
assert tags_suite


@pytest.fixture(scope='class')
def locations_suite(create_random_location, create_random_webshop):
    locations = []
    for _ in range(random.randint(10, 20)):
        location = create_random_location()
        location.save()
        locations.append(location)

    archived_locations = random.sample(locations, random.randint(1, len(locations) - 3))
    for location in archived_locations:
        location.archived = datetime.now(timezone.utc)
        location.save()

    yield dict(locations=locations, archived_locations=archived_locations)

    for l in locations:
        l.delete()


@pytest.mark.usefixtures('mock_aws')
class TestLocations:
    @pytest.mark.parametrize('archived', [True, False])
    def test_list_locations(self, as_admin, locations_suite, archived):
        locations = Location.scan(archived__not_exists=True)
        if archived:
            locations = Location.scan()

        location_guids = [x.guid for x in locations]
        r = as_admin.get(f'/locations?archived={archived}')
        assert 200 == r.status_code
        respond_guids = [uuid.UUID(x['guid']) for x in r.json]
        assert sorted(location_guids) == sorted(respond_guids)

    @pytest.mark.parametrize("as_non_admin, status_code",
                             [(pytest.lazy_fixture('as_customer'), 403),
                              (pytest.lazy_fixture('as_anonymous'), 401)])
    def test_list_locations_rbac(self, as_non_admin, locations_suite, status_code):
        r = as_non_admin.get(f'/locations')
        assert status_code == r.status_code

    def test_create_location(self, as_admin):
        data = Location.Schemas.Post().dump(random_location_data())
        r_post = as_admin.post('/locations/', json=data)
        assert 200 == r_post.status_code
        location_guid = r_post.json['guid']
        r_get = as_admin.get(f'/locations/{location_guid}')
        assert 200 == r_get.status_code
        assert r_post.json == r_get.json
        for k in ['latitude', 'longitude']:
            assert float(data[k]) == r_get.json[k]
            data.pop(k)
        assert all(v == r_get.json[k] for k, v in data.items())
        # Check a tag should created with same location guid.
        r = as_admin.get(f'/tags/{location_guid}/')
        assert 200 == r.status_code
        assert location_guid == r.json['guid']
        assert Tag.Type.LOCATION.value == r.json['type']

    @pytest.mark.parametrize('field_name', [x for x in Location.Schemas.Put().fields.keys()] + [dict(), None])
    def test_update_location(self, as_admin, locations_suite, field_name):
        location = random.choice(locations_suite['locations'])
        location_data = location.Schemas.Put().dump(random_location_data())
        try:
            if isinstance(field_name, dict):
                data = location_data
            else:
                data = {field_name: location_data[field_name]}
        except KeyError:
            data = {}
        r_put = as_admin.put(f'/locations/{location.guid}', json=data)
        r_get = as_admin.get(f'/locations/{location.guid}')
        assert r_put.json == r_get.json
        if field_name in ['latitude', 'longitude']:
            assert float(data[field_name]) == r_get.json[field_name]
        else:
            assert all(v == r_get.json[k] for k, v in data.items() if k not in ['latitude', 'longitude'])

    def test_delete_location(self, as_admin, locations_suite):
        # Delete a location will archive it
        location_guid = next(x.guid for x in locations_suite['locations'] if x.archived is None)
        start_of_test = datetime.now(timezone.utc)
        r = as_admin.delete(f'/locations/{str(location_guid)}/')
        assert 204 == r.status_code
        r = as_admin.get(f'/locations/{str(location_guid)}/')
        assert 200 == r.status_code
        archived_date = parse(r.json['archived'])
        assert type(archived_date) == datetime
        assert start_of_test <= archived_date <= datetime.now(timezone.utc)
        # Deleting the same location again should return 204, but shouldn't modify the archive date
        r = as_admin.delete(f'/locations/{str(location_guid)}/')
        assert 204 == r.status_code
        r = as_admin.get(f'/locations/{location_guid}/')
        assert archived_date == parse(r.json['archived'])
        # # Archiving a location should deleted the location-tag  todo: uncomment when archive tag implemented
        # r = as_admin.get(f'/tags/{location_guid}/')
        # assert 404 == r.status_code

    @pytest.mark.parametrize('parent_guid, response', [
        ('valid_guid', 200),
        (uuid.uuid4(), 400),
        (None, 200)
    ])
    def test_parent_guid_post_put(self, as_admin, locations_suite, parent_guid, response):
        location = random.choice(locations_suite['locations'])
        if parent_guid == 'valid_guid':
            parent_guid = next(x.guid for x in locations_suite['locations'] if x.guid is not location.guid)
        data = Location.Schemas.Post().dump(random_location_data(parent=parent_guid))
        r_post = as_admin.post('/locations/', json=data)
        assert response == r_post.status_code, r_post.json
        r_put = as_admin.put(f'/locations/{location.guid}', json=data)
        assert response == r_put.status_code, r_put.json

    @pytest.mark.parametrize('tag_guids, response', [
        ('valid_guids', 200),
        ([uuid.uuid4() for _ in range(5)], 400),
        (None, 200)
    ])
    def test_tag_guids_post_put(self, as_admin, locations_suite, tags_suite, tag_guids, response):
        location = random.choice(locations_suite['locations'])
        if tag_guids == 'valid_guids':
            tag_guids = random.sample([tag.guid for tag in tags_suite], len(tags_suite) - 1)
        data = Location.Schemas.Post().dump(random_location_data(tags=tag_guids))
        r_post = as_admin.post('/locations/', json=data)
        assert response == r_post.status_code
        r_put = as_admin.put(f'/locations/{location.guid}', json=data)
        assert response == r_put.status_code

    @pytest.mark.parametrize("as_non_admin, post_status_code, put_delete_status_code",
                             [(pytest.lazy_fixture('as_shopkeeper'), 200, 403),
                              (pytest.lazy_fixture('as_customer'), 200, 403),
                              (pytest.lazy_fixture('as_anonymous'), 401, 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_location_post_put_delete_rbac(self, as_non_admin, post_status_code, put_delete_status_code, locations_suite):
        location = random.choice(locations_suite['locations'])
        data = location.Schemas.Put().dump(random_location_data())
        r = as_non_admin.post('/locations/', json=data)
        assert post_status_code == r.status_code
        r = as_non_admin.put(f'/locations/{location.guid}', json=data)
        assert put_delete_status_code == r.status_code
        r = as_non_admin.delete(f'/locations/{location.guid}')
        assert put_delete_status_code == r.status_code
