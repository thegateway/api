import os.path
from decimal import Decimal

import pytest

from gwio.environment import Env
from gwio.ext.vat import Vat, VatPrice
from gwio.models.products._vertical.imeds.product import Product
from gwio.models.webshop_product import WebshopProduct

MEDICINE_PRICES_FN = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'medicine_prices.csv')

VERTICAL = Env.VERTICAL


@pytest.mark.skipif(VERTICAL != 'imeds', reason=f'Not applicable to "{VERTICAL}"')
@pytest.mark.usefixtures('mock_aws')
class TestAssessors:
    @pytest.mark.parametrize('product_type, expected, base_price',
                             [
                                 ('prescription_medicine', Decimal('657.54'), Decimal('564.45')),
                                 ('prescription_medicine', Decimal('163.98'), Decimal('129.00')),
                                 ('prescription_medicine', Decimal('33.32'), Decimal('24.00')),
                                 ('prescription_medicine', Decimal('13.05'), Decimal('9.00')),
                                 ('prescription_medicine', Decimal('5.63'), Decimal('3.88')),
                                 ('prescription_medicine', Decimal('0.00'), Decimal('0.00')),
                                 ('medicine', Decimal('813.19'), Decimal('680.45')),
                                 ('medicine', Decimal('140.95'), Decimal('104.00')),
                                 ('medicine', Decimal('87.95'), Decimal('63.00')),
                                 ('medicine', Decimal('18.73'), Decimal('12.36')),
                                 ('medicine', Decimal('5.87'), Decimal('3.58')),
                                 ('medicine', Decimal('0.50'), Decimal('0.00')),
                             ],
                             ids=lambda x: str(x))
    def test_calculation(cls, product_type, expected, base_price, create_random_product, webshop_guid):
        product = create_random_product(base_price=VatPrice(base_price), type=product_type, vat=Vat(10))
        product.save()
        webshop_product = WebshopProduct(product.guid, webshop_guid)
        webshop_product.save()
        evaluated = Product.API.Assessor.evaluate(product)
        assert expected == evaluated.price.original.unit.without_vat.amount
