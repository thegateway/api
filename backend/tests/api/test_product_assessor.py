# -*- coding: utf-8 -*-
from decimal import Decimal

import pytest

from gwio.ext import SYSTEM_GUID
from gwio.ext.vat import Vat
from gwio.models.pricing import PricingMethod, PricingOperation, Pricing


@pytest.mark.usefixtures('mock_aws', 'create_random_product')
def test_default_product_pricing(create_random_product):
    product = create_random_product(type='default')
    ep = product.API.Assessor.evaluate(product, qty=2)
    assert product.base_price.amount == ep.price.original.unit.without_vat.amount
    # TODO: Figure out wtf is wrong with this?!
    # assert product.base_price @ Vat(product.vat) == ep.price.original.unit.amount

@pytest.mark.usefixtures('mock_aws', 'create_random_pricing')
def test_product_pricing(create_random_product):
    """Set the pricing method for predictable value, for testing"""
    set_method = PricingMethod(op=PricingOperation.SET, value=Decimal(10.00))
    pricing = Pricing(guid=Pricing.API.generate_guid(name='default', owner=SYSTEM_GUID),
                      name='default', owner=SYSTEM_GUID, methods=dict(cost=set_method, retail=set_method))
    pricing.save()
    product = create_random_product(type='default')
    ep = product.API.Assessor.evaluate(product, qty=1)
    assert Decimal(10.00) == ep.price.original.unit.without_vat.amount
