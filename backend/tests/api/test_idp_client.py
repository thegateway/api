# -*- coding: utf-8 -*-
import uuid

import pytest
from faker import Faker
from gwio.environment import Env

from gwio.core.authentication import verify_token
from gwio.utils.aws import CognitoIdpClient


@pytest.fixture(scope='function')
def dummy_user_no_group(faker):
    email = faker.email()
    yield {
        'username': email,
        'password': 'panda_password_!234',
        'family_name': 'Bear',
        'name': 'Panda',
        'phone_number': '+358987654321',
        'email': email}


@pytest.fixture(scope='function')
def dummy_cognito_user(cognito_client):
    fake = Faker('fi_FI')
    email = fake.email()
    dummy_user = {
        'username': email,
        'password': fake.password(length=10),
        'name': fake.name(),
        'phone_number': '+358987654321',
        'email': email,
        'custom:organization_guid': str(uuid.uuid4())
    }
    yield dummy_user
    try:
        cognito_client.client.admin_delete_user(UserPoolId=cognito_client.pool_id, Username=dummy_user['username'])
    except Exception:
        pass


@pytest.fixture(scope='function')
def cognito_client():
    return CognitoIdpClient(pool_id=Env.COGNITO_POOL_ID,
                            client_id=Env.COGNITO_CLIENT_ID)


# TODO: create mocks or enable after moto has implemented support for the required calls
@pytest.mark.skip(reason="Cognito related test against real aws disabled")
class TestCognitoIdp():

    def _login(self, user, client):
        user_attr = {
            'name': user['name'],
            'phone_number': user['phone_number'],
            'email': user['email'],
            'custom:organization_guid': user['custom:organization_guid']
        }
        client.add_user(username=user['username'],
                        password=user['password'],
                        user_attributes=user_attr)
        return client.auth_with_password(username=user['username'],
                                         password=user['password'])

    def test_cognito_idp_client(self, dummy_cognito_user, cognito_client):
        auth_response = self._login(user=dummy_cognito_user, client=cognito_client)
        parsed_jwt = cognito_client.parse_jwt(jwt=auth_response['AuthenticationResult']['IdToken'])
        assert parsed_jwt['custom:organization_guid'] == dummy_cognito_user['custom:organization_guid']

        refresh_token_auth_response = cognito_client.auth_with_refresh_token(
            auth_response['AuthenticationResult']['RefreshToken'])
        cognito_client.parse_jwt(jwt=refresh_token_auth_response['AuthenticationResult']['IdToken'])

    def test_compatibility_with_verify_token(self, dummy_cognito_user, cognito_client, simple_app):
        auth_response = self._login(user=dummy_cognito_user, client=cognito_client)
        with simple_app.app_context():
            verify_token(auth_response['AuthenticationResult']['IdToken'])

    def test_set_organization_guid_add_to_group(self, cognito_client, dummy_user_no_group):
        user_attr = {
            'name': dummy_user_no_group['name'],
            'family_name': dummy_user_no_group['family_name'],
            'phone_number': dummy_user_no_group['phone_number'],
            'email': dummy_user_no_group['email']
        }
        cognito_client.add_user(username=dummy_user_no_group['username'],
                                password=dummy_user_no_group['password'],
                                user_attributes=user_attr)

        auth_response = cognito_client.auth_with_password(username=dummy_user_no_group['username'],
                                                          password=dummy_user_no_group['password'])
        parsed_jwt = cognito_client.parse_jwt(jwt=auth_response['AuthenticationResult']['IdToken'])
        assert parsed_jwt.get('custom:organization_guid') is None
        username = uuid.UUID(parsed_jwt.get('cognito:username'))

        cognito_pool = CognitoIdpClient(Env.COGNITO_POOL_ID, client_id=Env.COGNITO_CLIENT_ID)
        organization_guid = str(uuid.uuid4())
        group = CognitoIdpClient.Group.SHOPKEEPER
        cognito_pool.add_user_to_group(username, group)
        cognito_pool.set_custom_attribute('organization_guid', organization_guid, username)

        auth_response = cognito_client.auth_with_password(username=str(username),
                                                          password=dummy_user_no_group['password'])
        parsed_jwt = cognito_client.parse_jwt(jwt=auth_response['AuthenticationResult']['IdToken'])
        assert parsed_jwt.get('custom:organization_guid') == organization_guid
        assert 1 == len(parsed_jwt.get('cognito:groups', []))
        assert parsed_jwt.get('cognito:groups', [])[0] == group.value
