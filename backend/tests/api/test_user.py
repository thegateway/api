import random
import uuid
from datetime import date
from random import randint

import pytest

import gwio.user
from gwio.api import shops as shops_api
from gwio.core.utils.testing.helpers import (
    as_roles,
    fake,
)
from gwio.models import user
from gwio.utils import notify
from ._fakes.coupons import random_coupon_data

Notification = notify.Notification
User = gwio.user.User
UserData = user.UserData


def random_user_data_dict():
    return dict(
        birthday=date(*[int(x) for x in fake.date().split('-')]),
        tax_number=''.join([str(randint(0, 9)) for _ in range(12)]),
        coupons=[random_coupon_data() for _ in range(3)],
        business_id=dict(
            id=fake.word(),
            country=fake.country()
        ),
        language=random.choice(user.COUNTRY_CODES)
    )


@pytest.mark.usefixtures('mock_aws')
class TestUser:
    @pytest.mark.parametrize('attr_name', random_user_data_dict().keys())
    def test_get_data_attribute(self, attr_name):
        user_guid = uuid.uuid4()
        user_data_dict = random_user_data_dict()
        user_data = UserData(guid=user_guid, **user_data_dict)
        user_data.save()

        user = User(guid=user_guid)
        attr_value = getattr(user, attr_name)
        if isinstance(user_data_dict[attr_name], list):
            assert len(user_data_dict[attr_name]) == len(attr_value)
        else:
            try:
                compare_value = attr_value.Schema().dump(attr_value)
            except AttributeError:
                compare_value = attr_value
            assert user_data_dict[attr_name] == compare_value

    # No need to ever set coupons, it's a list that is modifier one item at a time
    @pytest.mark.parametrize('attr_name, value', [(x, y) for x, y in random_user_data_dict().items() if x != 'coupons'])
    def test_set_data_attribute(self, attr_name, value):
        user_guid = uuid.uuid4()
        user = User(guid=user_guid)
        setattr(user, attr_name, value)
        user_data = UserData.get(guid=user_guid)
        compare_value = getattr(user_data, attr_name)
        try:
            compare_value = compare_value.Schema().dump(compare_value)
        except AttributeError:
            pass
        assert value == compare_value


@pytest.mark.usefixtures('mock_aws')
class TestUsersApi:
    @as_roles(pytest, get=('as_role', 'guid'), skip={'anonymous'})
    def test_get_put_own_data(self, as_role, guid):
        deserialised_data = {k: v for k, v in random_user_data_dict().items() if k in UserData.Schemas.Put().fields.keys()}
        serialized_data = None
        if deserialised_data is None:
            user_data = UserData.get(guid=guid)
            for k, v in random_user_data_dict().items():
                setattr(user_data, k, v)
            user_data.save()
        else:
            serialized_data = UserData.Schemas.Put().dump(deserialised_data)
            assert serialized_data

        r = as_role.put(f'/users/{guid}/data', json=serialized_data)
        assert 200 == r.status_code
        # Removing `coupons` from PUT response for the compare, since it wasn't sent
        del r.json['coupons']

        assert serialized_data == r.json
        r = as_role.get(f'/users/{guid}/data')
        assert 200 == r.status_code
        del r.json['coupons']
        assert serialized_data == r.json

    @pytest.mark.parametrize('method, serialised_data', [
        ('get', None),
        ('put', UserData.Schemas.Get().dump(random_user_data_dict())),
    ])
    def test_get_put_someone_else_data_admin(self, as_admin, method, serialised_data):
        r = getattr(as_admin, method)(f'/users/{uuid.uuid4()}/data', json=serialised_data)
        assert 200 == r.status_code

    @pytest.mark.parametrize('method, serialised_data', [
        ('get', None),
        ('put', UserData.Schemas.Get().dump(random_user_data_dict())),
    ])
    @as_roles(pytest, skip={'admin', 'anonymous'})
    def test_get_put_someone_else_data_not_admin(self, as_role, method, serialised_data):
        r = getattr(as_role, method)(f'/users/{uuid.uuid4()}/data', json=serialised_data)
        assert 404 == r.status_code

    @pytest.mark.parametrize('method, serialised_data', [
        ('get', None),
        ('put', UserData.Schemas.Get().dump(random_user_data_dict())),
    ])
    def test_get_put_data_anonymous(self, as_anonymous, method, serialised_data):
        r = getattr(as_anonymous, method)(f'/users/{uuid.uuid4()}/data', json=serialised_data)
        assert 401 == r.status_code


class FakeFcmNotification:
    def __init__(self):
        self.notified = False
        self.exception = None

    def __call__(self, *args, **kwargs):
        return self

    def notify_single_device(self, **kwargs):
        self.notified = True
        self.notification = kwargs
        if self.exception:
            raise self.exception


@pytest.fixture(scope='function')
def random_notification_topic_payload(faker):
    def topic_payload(valid=True, topic=None):
        if topic is None:
            topic = random.choice(list(shops_api.NotificationTopic)).value if valid else faker.word()
        return dict(topics=[topic])

    return topic_payload


@pytest.fixture(scope='function')
def add_notification_subscription_and_topic(as_customer, customer_guid, random_notification_topic_payload, faker):
    def add_subscription(*, topic=None, notification_type=None):
        user = User(customer_guid, organization_guid=None)
        user.notifications = {}
        user.add_notification_types([dict(type=notification_type, data=dict(registration_key=faker.sha1(), endpoint=faker.url()))])

        payload = random_notification_topic_payload(topic)
        res = as_customer.post(f'/notifications/topics', json=payload)
        assert 204 == res.status_code

    return add_subscription


@pytest.mark.usefixtures('mock_aws')
class TestUserNotifications():

    def test_add_user_notification_topic(self, as_customer, random_notification_topic_payload):
        payload = random_notification_topic_payload()
        res = as_customer.post(f'/notifications/topics', json=payload)
        assert 204 == res.status_code

    def test_add_duplicate_user_notification_topic(self, customer_guid, as_customer, random_notification_topic_payload):
        user = User(customer_guid, organization_guid=None)
        user.notifications = {}
        payload = random_notification_topic_payload()
        res = as_customer.post(f'/notifications/topics', json=payload)
        assert 204 == res.status_code
        assert 1 == len(User(customer_guid).notifications['topics'])
        assert payload['topics'] == User(customer_guid).notifications['topics']

        res = as_customer.post(f'/notifications/topics', json=payload)
        assert 204 == res.status_code
        assert 1 == len(User(customer_guid).notifications['topics'])

    def test_add_user_notification_topic_bad_data(self, as_customer, random_notification_topic_payload):
        payload = random_notification_topic_payload(valid=False)
        res = as_customer.post(f'/notifications/topics', json=payload)
        assert 409 == res.status_code

    def test_get_user_notification_topics(self, customer_guid, as_customer, random_notification_topic_payload):
        user = User(customer_guid, organization_guid=None)
        user.notifications = {}
        payload = random_notification_topic_payload()
        res = as_customer.post(f'/notifications/topics', json=payload)
        assert 204 == res.status_code

        res = as_customer.get(f'/notifications/topics')
        assert 200 == res.status_code
        assert 1 == len(res.json)
        assert payload['topics'] == res.json

    def test_send_test_notification(self, as_customer, mock_fcm, add_notification_subscription_and_topic, faker):
        test_topic = 'testing'
        add_notification_subscription_and_topic(topic=test_topic, notification_type=Notification.Type.FCM_PUSH.value)
        title = " ".join(faker.words(2))
        notification = dict(topic=test_topic, message_title=title, message_body=faker.sentence(), message_icon='./assets/icon.png')
        _ = as_customer.post('/notify', json=notification)
        assert mock_fcm.notified
        mock_notification = next(x for x in mock_fcm.notifications if x['message_title'] == title)
        del mock_notification['registration_id']
        del notification['topic']
        assert notification == mock_notification
