import datetime
import random
import uuid
from decimal import Decimal

import pytest

from gwio.core.utils.testing.helpers import request_redirected_uri
from gwio.models.pricing import PriceType
from gwio.models.products import Product
from gwio.models.products.base import (
    PRODUCT_TYPE_DEFAULT,
    PRODUCT_TYPE_DELIVERY_METHOD,
)
from gwio.models.tags import (
    TagType,
    Tag,
)
from gwio.models.webshop_product import WebshopProduct


def get_es_entries(client, *, webshop_guid=None, query=None):
    if query is None:
        query = dict(dsl=dict(query=dict(match=dict(name=''))))
    if webshop_guid:
        resp = request_redirected_uri(client.post, f'/webshops/{webshop_guid}/products/_query', json=query)
    else:
        resp = client.post(f'/products/_query', json=query)
    return resp.json['products']


class TestProductsElasticsearchAPI:
    def _get_es_entries(self, *args, **kwargs):
        return get_es_entries(*args, **kwargs)

    def _check_bare_product_fields(self, product, es_entry):
        for (field, func) in [('guid', uuid.UUID),
                              ('name', lambda x: x),
                              ('desc', lambda x: x),
                              ('brief', lambda x: x),
                              ('images', lambda x: x),
                              ('pricing', lambda x: x if x is None else uuid.UUID(x)),
                              ('data', lambda x: x)]:
            assert getattr(product, field) == func(es_entry[field]), f'{field} does not match'
        for price_field in ['cost', 'base', 'original', 'final']:
            assert es_entry['price'][price_field]

    def test_add_product(self, as_admin, faker, webshop_guid):
        num_es_entries = len(self._get_es_entries(as_admin, webshop_guid=webshop_guid))

        data = dict(
            desc=faker.sentence(nb_words=10),
            name=faker.sentence(nb_words=4),
            base_price=dict(
                amount=Decimal('10'),
                vat_percent=Decimal('24'),
                currency='€',
            ),
            base_price_type=PriceType.COST.value,
            cost_price=dict(
                amount=Decimal('10'),
                vat_percent=Decimal('24'),
                currency='€',
            ),
            vat=Decimal(20),
        )
        resp = request_redirected_uri(as_admin.post, '/products', json=data)
        assert 201 == resp.status_code
        product_guid = resp.json['guid']

        assert num_es_entries + 1 == len(self._get_es_entries(as_admin, webshop_guid=webshop_guid))

        resp = request_redirected_uri(as_admin.post, f'/webshops/{webshop_guid}/products/_query',
                                      json=dict(dsl=dict(query=dict(match=dict(name=data['name'])))))
        assert 200 == resp.status_code
        assert 1 == resp.json['total']
        assert product_guid == resp.json['products'][0]['guid']

    def test_update_product(self, as_admin, create_random_product, faker, webshop_guid):
        num_es_entries = len(self._get_es_entries(as_admin, webshop_guid=webshop_guid))

        product = create_random_product()
        product.save()

        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid, query=dict(dsl=dict(query=dict(match=dict(name=product.name)))))
        assert 1 == len(es_entries)
        es_entry = es_entries[0]
        self._check_bare_product_fields(product, es_entry)

        data = dict(
            desc=faker.sentence(nb_words=10),
            name=faker.sentence(nb_words=4),
            base_price=dict(
                amount=Decimal('10'),
                vat_percent=Decimal('24'),
                currency='€',
            ),
            base_price_type=PriceType.COST.value,
            cost_price=dict(
                amount=Decimal('10'),
                vat_percent=Decimal('24'),
                currency='€',
            ),
            vat=Decimal(20),
        )
        resp = request_redirected_uri(as_admin.put, f'/products/{product.guid}', json=data)
        assert 200 == resp.status_code
        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid)
        assert num_es_entries + 1 == len(es_entries)

        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid, query=dict(dsl=dict(query=dict(match=dict(name=data['name'])))))
        es_entry = es_entries[0]
        for field in ['desc', 'name']:
            assert data[field] == es_entry[field]

        product.refresh()
        self._check_bare_product_fields(product, es_entry)

    def test_delete_product(self, as_admin, create_random_product, webshop_guid):
        product = create_random_product()
        product.save()

        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid)
        assert product.guid in [uuid.UUID(e['guid']) for e in es_entries]
        num_es_entries = len(es_entries)

        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid, query=dict(dsl=dict(query=dict(match=dict(name=product.name)))))
        assert 1 == len(es_entries)
        es_entry = es_entries[0]
        self._check_bare_product_fields(product, es_entry)
        assert num_es_entries == len(self._get_es_entries(as_admin, webshop_guid=webshop_guid))

        resp = request_redirected_uri(as_admin.delete, f'/products/{product.guid}')
        assert 204 == resp.status_code

        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid)
        assert product.guid not in [uuid.UUID(e['guid']) for e in es_entries]
        assert num_es_entries - 1 == len(es_entries)

        es_entries = self._get_es_entries(as_admin, webshop_guid=webshop_guid, query=dict(dsl=dict(query=dict(match=dict(name=product.name)))))
        assert not es_entries


class TestProductTagsElasticsearchAPI:
    def _get_es_entries(self, *args, **kwargs):
        return get_es_entries(*args, **kwargs)

    @pytest.mark.parametrize('tag_type', [Tag.Type.CATEGORY, Tag.Type.BRAND])
    def test_product_add_tag(self, as_admin, webshop, products_suite, create_random_tag, tag_type):
        tag = create_random_tag(type=tag_type)
        tag.save()
        pre_entries = self._get_es_entries(as_admin)
        wsp_guids = [wsp.product_guid for wsp in WebshopProduct.scan()]
        product = random.choice([p for p in Product.scan() if p.guid not in wsp_guids and p.type != PRODUCT_TYPE_DELIVERY_METHOD])
        wsp = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
        wsp.save()
        es_entries = self._get_es_entries(as_admin)
        assert len(pre_entries) + 1 == len(es_entries)
        assert tag.guid not in [uuid.UUID(p['guid']) for p in es_entries[0]['tags']]

        tag.add_product(product)
        tag.save()
        product.save()

        es_entries = self._get_es_entries(as_admin)
        assert len(pre_entries) + 1 == len(es_entries)
        es_prod = next(filter(lambda p: uuid.UUID(p['guid']) == product.guid, es_entries))
        try:
            prod_tag = next(t for t in es_prod['tags'] if uuid.UUID(t['guid']) == tag.guid)
        except StopIteration:
            assert False, "Couldn't find the added tag in product tags"
        assert tag.type == TagType(prod_tag['type'])
        assert tag.name == prod_tag['name']
        if tag_type == Tag.Type.BRAND:
            assert tag.name == es_prod['brand_name']
        with pytest.raises(KeyError):
            _ = prod_tag['owner']


class TestWebshopProductsElasticsearchAPI:
    def _get_es_entries(self, *args, **kwargs):
        return get_es_entries(*args, **kwargs)

    def test_webshop_product_add(self, as_admin, webshop, products_suite):
        product = random.choice([p for p in products_suite if p.type == PRODUCT_TYPE_DEFAULT])
        product.activated = datetime.datetime.now(tz=datetime.timezone.utc)
        product.save()
        # data = dict(products=[dict(product_guid=product.guid)])
        # This fails miserably for url reason?
        # resp = as_admin.put(f'/webshops/{webshop.guid}/products', data)
        wsp = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
        wsp.save()

        entries = self._get_es_entries(as_admin, query=dict(dsl=dict(query=dict(match=dict(name=product.name)))))
        assert 1 == len(entries)
        entry = entries[0]
        for price_field in ['original', 'final']:
            assert entry['price'].get(price_field) is not None
        with pytest.raises(KeyError):
            _ = entry['price']['cost']
        with pytest.raises(KeyError):
            _ = entry['price']['base']
        assert uuid.UUID(entry['shop_guid']) == webshop.guid
        assert entry['shop_name'] == webshop.name

        es_entries = self._get_es_entries(as_admin,
                                          webshop_guid=webshop.guid,
                                          query=dict(dsl=dict(query=dict(match=dict(guid=str(product.guid))))))
        assert 1 == len(es_entries)
        entry = es_entries[0]
        assert webshop.guid in [uuid.UUID(s) for s in entry['suppliers']]

        wsp.delete()

        es_entries = self._get_es_entries(as_admin,
                                          webshop_guid=webshop.guid,
                                          query=dict(dsl=dict(query=dict(match=dict(guid=str(product.guid))))))
        assert 1 == len(es_entries)
        entry = es_entries[0]
        assert webshop.guid not in [uuid.UUID(s) for s in entry['suppliers']]
