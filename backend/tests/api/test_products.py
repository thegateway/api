# coding=utf-8
import datetime
import random
import uuid
from collections import defaultdict
from decimal import Decimal
from random import choice

import dateutil
import pytest
import pytz
from marshmallow.fields import Field
from pytest_lazyfixture import LazyFixture

from gwio.core.utils.testing.helpers import request_redirected_uri, fake
from gwio.environment import Env
from gwio.models.pricing import PriceType
from gwio.models.products import Product
from gwio.models.webshop_product import WebshopProduct
from gwio.models.webshops import Webshop
from ._fakes.products import random_product_data
from .test_tags import (
    product_tags_suite,
    tags_suite,
)
from ._fixtures.products import products_suite

assert products_suite
assert tags_suite
assert product_tags_suite


@pytest.fixture(scope='class')
def test_suite(mock_aws, random_products, webshop_guid):
    products = list()
    for product in random_products(10):
        product.save()
        products.append(product)
        WebshopProduct(product_guid=product.guid, webshop_guid=webshop_guid).save()
    yield products
    for product in products:
        product.delete()
        WebshopProduct(product_guid=product.guid, webshop_guid=webshop_guid).delete()


@pytest.fixture(scope='class')
def test_suite_no_substitutes(test_suite):
    guids = {p.guid: p for p in test_suite}
    products = [product for guid, product in guids.items() if list(guids.keys()).count(guid) == 1]
    yield products


@pytest.fixture(scope='class')
def test_suite_substitutes_in_other_shop(test_suite):
    product = choice(test_suite)
    random_webshop_guid = uuid.uuid4()

    substitute_products = random_substitute_products(product.substitution_group)
    for sp in substitute_products:
        WebshopProduct(product_guid=sp.guid, webshop_guid=random_webshop_guid).save()
        sp.save()

    yield [product]

    for sp in substitute_products:
        sp.delete()
        WebshopProduct(product_guid=sp.guid, webshop_guid=random_webshop_guid).delete()


def random_substitute_products(substitution_group):
    substitute_products = []
    for _ in range(3):
        product_data = random_product_data(substitution_group=substitution_group)
        substitute_product = Product(Product.API.generate_guid(**product_data), **product_data)
        substitute_products.append(substitute_product)

    return substitute_products


@pytest.fixture(scope='function')
def substitute_products(test_suite, webshop_guid):
    product = choice(test_suite)
    wsps = []

    substitute_products = random_substitute_products(product.substitution_group)
    for sp in substitute_products:
        wsp = WebshopProduct(product_guid=sp.guid, webshop_guid=webshop_guid)
        wsp.save()
        wsps.append(wsp)
        sp.save()

    yield dict(original=product, substitutes=substitute_products)
    for sp in substitute_products:
        sp.delete()

    for wsp in wsps:
        wsp.delete()


@pytest.fixture(scope='class')
def products_without_substitutes(mock_aws, webshop_guid):
    assert mock_aws
    substitute_products = random_substitute_products('00000')
    for sp in substitute_products:
        WebshopProduct(product_guid=sp.guid, webshop_guid=webshop_guid).save()
        sp.save()

    yield substitute_products
    for sp in substitute_products:
        WebshopProduct(product_guid=sp.guid, webshop_guid=webshop_guid).delete()
        sp.delete()


@pytest.fixture(scope='function')
def webshops_products(mock_aws, products_suite, create_random_webshop, webshop):
    assert mock_aws
    product = random.choice(products_suite)
    webshops = []
    for _ in range(random.randint(6, 10)):
        ws = create_random_webshop()
        ws.save()
        webshops.append(ws)

    try:
        WebshopProduct.get(product_guid=product.guid, webshop_guid=webshop.guid).delete()
    except WebshopProduct.DoesNotExist:
        pass

    for ws in webshops:
        webshop_product = WebshopProduct(product_guid=product.guid, webshop_guid=ws.guid)
        webshop_product.save()

    yield dict(product=product, webshops=webshops)

    for ws in webshops:
        WebshopProduct(product_guid=product.guid, webshop_guid=ws.guid).delete()
        ws.delete()


def assert_data_dict_has_normalise_variants_dict(res_data: dict, json_dict: dict):
    # Different endpoint response differently.
    try:
        # Checking response contain variants fields with lower field name
        assert res_data == json_dict['data']
        return json_dict['guid']
    except KeyError:
        assert res_data == json_dict['results'][0]['product']['data']
        return json_dict['results'][0]['product']['guid']


@pytest.mark.usefixtures('test_suite')
class TestProductAPI:
    schema = None

    @classmethod
    def setup_class(cls):
        cls.schema = Product.Schemas.Get()

    def test_get_product(self, as_shopkeeper, test_suite):
        product = choice(test_suite)
        r = request_redirected_uri(as_shopkeeper.get, f'/products/{product.guid}')
        assert 200 == r.status_code

    def test_get_non_existing_product(self, as_anonymous):
        r = as_anonymous.get(f'/products/{uuid.uuid4()}')
        assert 404 == r.status_code

    @pytest.mark.parametrize('missing_field', [
        'name',
        'base_price.amount',
        'base_price.vat_percent',
        'base_price_type',
        'cost_price.amount',
        'cost_price.vat_percent',
        'vat',
    ])
    def test_add_missing_param(self, as_admin, faker, missing_field):
        data = dict(
            desc=faker.sentence(nb_words=10),
            name=faker.sentence(nb_words=4),
            base_price=dict(
                amount=Decimal('10'),
                vat_percent=Decimal('24'),
                currency='€',
            ),
            base_price_type=PriceType.COST.value,
            cost_price=dict(
                amount=Decimal('10'),
                vat_percent=Decimal('24'),
                currency='€',
            ),
            vat=Decimal(20),
        )
        try:
            base_key, missing_field = missing_field.split('.')
            data[base_key].pop(missing_field)
        except ValueError:
            data.pop(missing_field)
        r = as_admin.post('/admin/products/', json=data)
        assert 409 == r.status_code, r.json

        expected_error_message = f'"{missing_field}": ["{Field.default_error_messages["required"]}"]'
        try:
            expected_error_message = f'{{"{base_key}": {{{expected_error_message}}}}}'
        except UnboundLocalError:
            pass

        assert expected_error_message in r.json['message']

    @pytest.mark.skipif(Env.VERTICAL not in ('imeds', 'vpk'), reason='Other verticals have random product guid')
    def test_add_existing(self, as_admin, test_suite):
        data = self.schema.dump(random_product_data())
        r = as_admin.post('/products/', json=data)
        assert 201 == r.status_code, r.json
        r = as_admin.post('/products/', json=data)
        assert 409 == r.status_code, r.json
        assert 'already exists' in r.json['message']

    def request_or_redirect(self, request_method, url, **kwargs):
        response = request_method(url, **kwargs)
        if 308 == response.status_code:
            return request_method(response.location, **kwargs)
        return response

    @pytest.mark.parametrize('as_role, url', [
        (pytest.lazy_fixture('as_admin'), '/products'),
        (pytest.lazy_fixture('as_shopkeeper'), '/organizations/{organization_guid}/products'),
        (pytest.lazy_fixture('as_brand_owner'), '/organizations/{organization_guid}/products'),
    ], ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_add_put_delete_product(self, create_random_product, as_role, url):
        organization_guid = None
        if '{organization_guid}' in url:
            organization_guid = as_role.token_payload['custom:organization_guid']
            url = url.format(organization_guid=organization_guid)
        data = Product.Schemas.Post().dump(create_random_product())
        r = self.request_or_redirect(as_role.post, url, json=data)
        assert 201 == r.status_code
        guid = r.json['guid']

        data = Product.Schemas.Put().dump(create_random_product())
        r = self.request_or_redirect(as_role.put, f'{url}/{guid}', json=data)
        assert 200 == r.status_code

        if organization_guid is not None:
            assert uuid.UUID(organization_guid) == Product.get(owner_guid=uuid.UUID(organization_guid), guid=uuid.UUID(guid)).owner_guid
        r = self.request_or_redirect(as_role.delete, f'{url}/{guid}')
        assert 204 == r.status_code

    def test_add_delete_product(self, random_product, as_admin):
        time_start = datetime.datetime.now(tz=pytz.utc)
        data = Product.Schemas.Post().dump(random_product)
        r = request_redirected_uri(as_admin.post, '/products/', json=data)
        assert 201 == r.status_code
        guid = r.json['guid']
        time_end = datetime.datetime.now(tz=pytz.utc)
        assert time_start < dateutil.parser.parse(r.json['timestamps']['created']) < time_end
        r = request_redirected_uri(as_admin.delete, f'/products/{guid}')
        assert 204 == r.status_code

    @pytest.mark.parametrize('as_role, url', [
        (pytest.lazy_fixture('as_shopkeeper'), '/products'),
        (pytest.lazy_fixture('as_customer'), '/products'),
        (pytest.lazy_fixture('as_brand_owner'), '/organizations/{organization_guid}/products'),
        (pytest.lazy_fixture('as_customer'), '/organizations/{organization_guid}/products'),
    ], ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_add_put_delete_product_wrong_role(self, webshop, create_random_product, as_role, url):
        if '{organization_guid}' in url:
            url = url.format(organization_guid=webshop.guid)
        data = self.schema.dump(create_random_product())
        r = as_role.post(url, json=data)
        assert 403 == r.status_code
        product = create_random_product()
        product.save()
        guid = product.guid

        data = self.schema.dump(create_random_product())
        r = self.request_or_redirect(as_role.put, f'{url}/{guid}', json=data)
        assert 403 == r.status_code

        r = as_role.delete(url + f'/{guid}')
        assert 403 == r.status_code

    @pytest.mark.parametrize('as_role, url', [
        (pytest.lazy_fixture('as_brand_owner'), '/organizations/{organization_guid}/products'),
        (pytest.lazy_fixture('as_customer'), '/organizations/{organization_guid}/products'),
    ], ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_add_put_delete_product_wrong_organizationi(self, webshop, create_random_product, as_role, url):
        url = url.format(organization_guid=str(uuid.uuid4()))
        data = self.schema.dump(create_random_product())
        r = as_role.post(url, json=data)
        assert 403 == r.status_code
        product = create_random_product()
        product.save()
        guid = product.guid

        data = self.schema.dump(create_random_product())
        r = self.request_or_redirect(as_role.put, f'{url}/{guid}', json=data)
        assert 403 == r.status_code

        r = as_role.delete(url + f'/{guid}')
        assert 403 == r.status_code

    def test_delete_product_with_tags(self, as_admin, product_tags_suite, products_suite, tags_suite):
        product_tag = random.choice(product_tags_suite)
        product_guid = str(product_tag.product_guids[0])
        # Check product_guid IS available in related product_tag request
        r = as_admin.get(f'/product_tags/{product_tag.guid}')
        assert 200 == r.status_code
        assert product_guid in r.json
        # Now delete the product and all related product_tags
        r = request_redirected_uri(as_admin.delete, f'/products/{product_guid}')
        assert 204 == r.status_code
        # Now check product_guid IS NOT available in related product_tag request
        r = as_admin.get(f'/product_tags/{product_tag.guid}')
        assert 200 == r.status_code
        assert product_guid not in r.json

    def test_query_product(self, as_anonymous):
        r = as_anonymous.post('/products/_query', json=dict(dsl=dict(query=dict(match=dict(name=' ')))))
        assert 200 == r.status_code
        assert r.json['products']
        assert all(
            x['price'][y]['unit'][z] is not None for x in r.json['products'] for y in ('original', 'final') for z in ('amount', 'currency', 'vat', 'vat0'))

    def test_query_product_no_matches(self, as_anonymous):
        r = as_anonymous.post('/products/_query',
                              json=dict(dsl=dict(query=dict(match=dict(name='not_possible_to_find_this')))))
        assert 200 == r.status_code
        assert not r.json['products']

    # Separate test for `activated` because you send boolean even though it is datetime
    @pytest.mark.parametrize('field_name', [x for x in Product.Schemas.Put().fields.keys() if '_price' not in x and x != 'activated'] + [dict(), None])
    def test_put(self, as_admin, test_suite, random_product, field_name):
        product = choice(test_suite)
        random_product = Product.Schemas.Put().dump(Product.API.Assessor.evaluate(random_product))
        random_product.pop('activated')
        try:
            if isinstance(field_name, dict):
                data = {k: v for k, v in random_product.items() if '_price' not in k}
            else:
                data = {field_name: random_product[field_name]}
        except KeyError:
            data = {}
        r_put = request_redirected_uri(as_admin.put, f'/products/{product.guid}', json=data)
        r_get = request_redirected_uri(as_admin.get, f'/products/{product.guid}')
        assert r_put.json == r_get.json
        assert all(v == r_get.json[k] for k, v in data.items())

    def test_put_activated(self, as_admin):
        product = choice(list(Product.scan()))
        product.activated = None
        product.save()

        start = datetime.datetime.now(tz=pytz.UTC)
        r = as_admin.put(f'/admin/products/{product.guid}', json=dict(activated=True))
        assert r.status_code == 200, r.json['message']
        r_activated = datetime.datetime.fromisoformat(r.json['activated'])
        assert r_activated > start
        assert r_activated < datetime.datetime.now(tz=pytz.UTC)

        r = as_admin.put(f'/admin/products/{product.guid}', json=dict(activated=False))
        assert r.status_code == 200, r.json['message']
        assert r.json['activated'] == None

    @pytest.mark.skipif(Env.VERTICAL != 'imeds', reason=f'Not applicable to "{Env.VERTICAL}"')
    def test_product_substitutions(self, as_anonymous, webshop_guid, substitute_products):
        r = as_anonymous.get(f'/webshops/{webshop_guid}/product_substitutes/{substitute_products["original"].guid}')
        assert 200 == r.status_code
        actual_substitutes = r.json
        assert len(substitute_products['substitutes']) == len(actual_substitutes)

        for substitute in substitute_products['substitutes']:
            substitute_product = Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet().dump(Product.API.Assessor.evaluate(substitute))
            actual_product = next((x for x in actual_substitutes if uuid.UUID(x['guid']) == substitute.guid))
            # Below tests are to check 'Product.API.Assessor.EvaluatedProduct.API.customer_marshal' schema should not return base and cost price.
            actual_product_price_keys = list(actual_product['price'].keys())
            assert all(x not in actual_product_price_keys for x in ['base', 'cost'])
            assert set(actual_product) == set(substitute_product)
            assert sorted(actual_product_price_keys) == sorted(list(substitute_product['price'].keys()))

    @pytest.mark.skipif(Env.VERTICAL != 'imeds', reason=f'Not applicable to "{Env.VERTICAL}"')
    @pytest.mark.parametrize("product_suite",
                             [pytest.lazy_fixture('test_suite_no_substitutes'),  # Products not sharing substitution number
                              pytest.lazy_fixture('products_without_substitutes'),  # Products not having substitution ('00000')
                              pytest.lazy_fixture('test_suite_substitutes_in_other_shop')],  # Products not sharing substitution number in the given shop
                             ids=['no_substitute_products', 'non_substitutable_product', 'no_substitutes_in_shop'])
    def test_product_substitutions_empty(self, as_anonymous, webshop_guid, product_suite):
        assert products_suite
        product = choice(product_suite)
        r = as_anonymous.get(f'/webshops/{webshop_guid}/product_substitutes/{product.guid}')
        assert 200 == r.status_code

        def list_wsp():
            return [dict(product=wsp.product_guid, webshop=wsp.webshop_guid) for wsp in WebshopProduct.scan()]

        assert not r.json, f'''Expected no substitutions for code {product.substitution_group}, got {len(r.json)} products: {r.json}\n {list_wsp()}'''

    @pytest.mark.usefixtures('webshops_products')
    def test_get_webshop_by_product(self, as_customer):
        shops_by_product = defaultdict(list)
        for wsp in WebshopProduct.scan():
            try:
                # Some previous test removes the product but not the webshop products
                # TODO: The webshop products should probably be removed automatically when the related product is removed
                Product.by_guid(wsp.product_guid)
                shops_by_product[wsp.product_guid].append(wsp.webshop_guid)
            except Product.DoesNotExist:
                pass
        product_guid = choice([x for x, webshops in shops_by_product.items() if len(webshops) >= 1])
        other_webshop_guid = choice([ws.guid for ws in Webshop.scan() if ws.guid not in shops_by_product[product_guid]])
        # Make sure the product has multiple suppliers
        WebshopProduct(product_guid=product_guid, webshop_guid=other_webshop_guid).save()
        res = as_customer.get(f'/products/{product_guid}/suppliers')
        if res.status_code != 200:
            assert False
        assert 200 == res.status_code
        assert {ws.webshop_guid for ws in WebshopProduct.query(product_guid=product_guid)} == {uuid.UUID(ws) for ws in res.json}
        # assert {webshop_guid for webshop_guid in shops_by_product[product_guid]} == {uuid.UUID(ws) for ws in res.json}

    def test_add_product_with_owner(self, webshop, as_shopkeeper, random_product):
        data = Product.Schemas.Post().dump(random_product)
        r = as_shopkeeper.post(f'/organizations/{webshop.guid}/products/', json=data)
        assert 201 == r.status_code
        assert webshop.guid == Product.by_guid(uuid.UUID(r.json['guid'])).owner_guid

    @pytest.mark.parametrize('url', [
        '/products',
        '/admin/products',
        '/organizations/{organization_guid}/products',
        '/webshops/{webshop_guid}/products',
        '/dashboard/webshops/{webshop_guid}/products'
    ])
    def test_variants_field_normalization_inside_product_data(self, as_admin, create_random_product, create_random_webshop, url):
        webshop = create_random_webshop()
        webshop.save()
        # data dict with whitespace and capital variants fields name. Need it for crate and update product
        data = {
            "variants": {
                "  SIZE    ": f'{random.randint(50, 100)} cm',
                "COLOR    ": fake.safe_color_name()
            }
        }
        post_json = Product.Schemas.Post().dump(create_random_product())
        post_json['data'] = data.copy()  # Need to add data field separately as dumping convert variants field name in lower case
        put_json = Product.Schemas.Put().dump(create_random_product())
        put_json['data'] = data.copy()

        # Create and update product, work differently for different endpoints
        if '{organization_guid}' in url:
            url = url.format(organization_guid=webshop.guid)
        if '{webshop_guid}' in url:
            url = url.format(webshop_guid=webshop.guid)
            post_json = dict(products=[dict(product=post_json)])
            put_json = dict(products=[dict(product=put_json)])

        # Post product with capital letter variants fields name
        r = self.request_or_redirect(as_admin.post, url, json=post_json)
        assert 201 == r.status_code or 200 == r.status_code
        # data dict with lower variants fields name. Need it for asserting response
        res_data = dict(
            variants={k.lower().strip(): v for k, v in data['variants'].items()}
        )
        guid = assert_data_dict_has_normalise_variants_dict(res_data=res_data, json_dict=r.json)
        # Put product with capital letter variants fields name
        r = self.request_or_redirect(as_admin.put, f'{url}/{guid}', json=put_json)
        assert 200 == r.status_code
        assert_data_dict_has_normalise_variants_dict(res_data=res_data, json_dict=r.json)
