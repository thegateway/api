import mock
import pytest

from ._mocks import MockCustomer


@pytest.fixture(scope='function')
def customer(mock_aws, test_customer, customer_identity_id):
    assert mock_aws
    test_customer.save()
    yield test_customer
    test_customer.delete()


@pytest.mark.usefixtures('customer', 'mock_aws')
class TestStripeCard:
    def test_add_get_delete_card(self, as_customer, test_customer):
        with mock.patch('stripe.Customer.retrieve') as mock_retrieve_customer, \
                mock.patch('stripe.Customer.create') as mock_create_customer:
            mock_retrieve_customer.return_value = MockCustomer()
            mock_create_customer.return_value = MockCustomer()
            test_card = dict(token='tok_visa')
            res = as_customer.post(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/', json=test_card)
            assert 201 == res.status_code

            res = as_customer.get(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/')
            assert 200 == res.status_code
            assert 1 == len(res.json)
            card_id = res.json[0]['id']

            res = as_customer.get(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/{str(card_id)}/')
            assert 200 == res.status_code

            res = as_customer.delete(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/{str(card_id)}')
            assert 204 == res.status_code

    def test_add_card_no_id(self, as_customer, test_customer):
        with mock.patch('stripe.Customer.retrieve') as mock_retrieve_customer, \
                mock.patch('stripe.Customer.create') as mock_create_customer:
            mock_retrieve_customer.return_value = MockCustomer()
            mock_create_customer.return_value = MockCustomer()

            res = as_customer.get(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/')
            assert 200 == res.status_code
            expected_len = len(res.json)

            test_card = dict()
            res = as_customer.post(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/', json=test_card)
            assert 409 == res.status_code

            res = as_customer.get(f'/customers/{test_customer.identity_id.split(":")[1]}/stripe_cards/')
            assert 200 == res.status_code
            assert expected_len == len(res.json)
