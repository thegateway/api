# coding=utf-8
import os
import random
import string
import uuid

import pytest
from faker import Faker
from jinja2 import Environment, FileSystemLoader


class TestEmailTemplates:
    env = None
    order = None

    @classmethod
    def setup_class(cls):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        cls.env = Environment(loader=FileSystemLoader(dir_path + '/templates'))
        cls.order = blank
        setattr(cls.order, 'data', order_data_dict())

    def test_invoice_notification(self):
        for ext in ('html', 'txt'):
            self.env.get_template('ddb_invoice_order_notification.{ext}'.format(ext=ext)).render(order=self.order)


def blank():
    """Just so we have some object to set an attribute with."""
    pass


def order_data_dict():
    """Data dict of order provided as is with faker modified data."""
    fake = Faker('fi_FI')
    buyer_address = fake.street_address()
    buyer_city = fake.city()
    buyer_email = fake.email()
    buyer_name = fake.name()
    buyer_phone = '+358401234567'
    buyer_post_code = fake.postcode()
    seller_address = fake.street_address()
    seller_city = fake.city()
    seller_company = fake.company()
    seller_email = fake.email()
    seller_email_uuid = str(uuid.uuid4())
    seller_name = fake.name()
    seller_phone = '+358509999999'
    seller_phone_uuid = str(uuid.uuid4())
    seller_post_code = fake.postcode()
    seller_stripe_key = 'pk_test_' + ''.join(random.choices(string.ascii_letters + string.digits, k=24))
    seller_uuid = str(uuid.uuid4())

    delivery_1_uuid = str(uuid.uuid4())
    delivery_2_uuid = str(uuid.uuid4())
    google_analytics = 'UA-' + ''.join(random.choices(string.digits, k=8)) + '-' + random.choice(string.digits)
    line_1_uuid = str(uuid.uuid4())
    line_2_uuid = str(uuid.uuid4())
    line_3_uuid = str(uuid.uuid4())
    host_provider = fake.url()
    host_seller = fake.url()
    payment_method_uuid = str(uuid.uuid4())
    pricing_uuid = str(uuid.uuid4())
    product_1_uuid = str(uuid.uuid4())
    product_2_uuid = str(uuid.uuid4())
    product_3_uuid = str(uuid.uuid4())
    website_uuid = str(uuid.uuid4())
    website_domain = fake.domain_name()
    return {'buyer': {'email': buyer_email,
                      'name': buyer_name,
                      'phone': buyer_phone},
            'delivery_address': {'country': 'Suomi',
                                 'name': buyer_name,
                                 'postal_code': ' '.join((buyer_post_code, buyer_city)),
                                 'street_address': buyer_address},
            'delivery_method': {'base_price': 4.52,
                                'currency': '€',
                                'description': 'Tuotteet toimitetaan Postin '
                                               'määrittelemään lähimpään toimipaikkaan.',
                                'included_vat': 1.08,
                                'logo_url': host_provider + 'img/nouto_postista.jpg',
                                'name': 'Posti - Economy Parcel',
                                'price': 5.6,
                                'price_type': 'VAT24',
                                'uuid': delivery_1_uuid,
                                'vat0_price': 4.52},
            'lines': [{'base_price': 13.2,
                       'children': [],
                       'code': 'SV22',
                       'currency': '€',
                       'ean_code': '6420611525947',
                       'included_vat': 4.75,
                       'name': 'Köysi 20m/12mm',
                       'price': 24.55,
                       'price_type': 'VAT24',
                       'pricing': pricing_uuid,
                       'product_uuid': product_1_uuid,
                       'quantity': 3,
                       'total_included_vat': 14.25,
                       'total_price': 73.65,
                       'type': 'product',
                       'uuid': line_1_uuid,
                       'vat0_price': 19.8,
                       'vat0_total_price': 59.4},
                      {'base_price': 28.6,
                       'children': [],
                       'code': 'SV2',
                       'currency': '€',
                       'ean_code': '6420611525572',
                       'included_vat': 10.3,
                       'name': 'Ensiapulaukku Talosuojelu',
                       'price': 53.2,
                       'price_type': 'VAT24',
                       'pricing': pricing_uuid,
                       'product_uuid': product_2_uuid,
                       'quantity': 1,
                       'total_included_vat': 10.3,
                       'total_price': 53.2,
                       'type': 'product',
                       'uuid': line_2_uuid,
                       'vat0_price': 42.9,
                       'vat0_total_price': 42.9},
                      {'base_price': 29.67,
                       'children': [],
                       'code': 'SV15',
                       'currency': '€',
                       'ean_code': '6420611525770',
                       'included_vat': 10.68,
                       'name': 'Jodi -100 (130mg)',
                       'price': 55.19,
                       'price_type': 'VAT24',
                       'pricing': pricing_uuid,
                       'product_uuid': product_3_uuid,
                       'quantity': 2,
                       'total_included_vat': 21.36,
                       'total_price': 110.38,
                       'type': 'product',
                       'uuid': line_3_uuid,
                       'vat0_price': 44.5,
                       'vat0_total_price': 89.02}],
            'payment_method': {'description': 'Lasku sähköpostilla.',
                               'logo': None,
                               'name': 'invoice',
                               'uuid': payment_method_uuid},
            'require_acceptance': True,
            'seller': {'actor_id': 85262,
                       'addresses': [{'country': 'FI',
                                      'id': 429,
                                      'label': None,
                                      'name': seller_company,
                                      'postal_area': seller_city,
                                      'postal_code': seller_post_code,
                                      'street_address': seller_address}],
                       'banners': [],
                       'brand_color': '#ff6600',
                       'delivery_methods': [{'data': None,
                                             'description': 'Postin '
                                                            'Flex-kotiinkuljetus vie '
                                                            'keskimäärin 1-2 '
                                                            'arkipäivää.',
                                             'id': delivery_2_uuid,
                                             'logo_url': host_provider + 'img/kotiinkuljetus.jpg',
                                             'name': 'Kotiinkuljetus',
                                             'price': 9.9},
                                            {'data': None,
                                             'description': 'Tuotteet toimitetaan '
                                                            'Postin määrittelemään '
                                                            'lähimpään toimipaikkaan.',
                                             'id': delivery_1_uuid,
                                             'logo_url': host_provider + 'img/nouto_postista.jpg',
                                             'name': 'Posti - Economy Parcel',
                                             'price': 5.6}],
                       'emails': [{'id': seller_email_uuid,
                                   'label': None,
                                   'rank': 1,
                                   'value': seller_email}],
                       'enabled': None,
                       'exceptional_opening_hours': [],
                       'facebook_analytics': None,
                       'google_analytics': google_analytics,
                       'id': seller_uuid,
                       'location': [62.4845505, 21.7456382],
                       'logo': host_seller + 'logo.png',
                       'name': 'VPK-kauppa',
                       'normal_opening_hours': [{'day': 3,
                                                 'end_at': '20:00',
                                                 'start_at': '18:00'}],
                       'payment_methods': [{'description': 'Lasku sähköpostilla.',
                                            'logo': None,
                                            'name': 'invoice',
                                            'uuid': payment_method_uuid}],
                       'person_in_charge': seller_name,
                       'phones': [{'id': seller_phone_uuid,
                                   'label': None,
                                   'rank': 1,
                                   'value': seller_phone}],
                       'social_media': [],
                       'stripe_key': seller_stripe_key,
                       'websites': [{'id': website_uuid,
                                     'label': None,
                                     'rank': 1,
                                     'value': website_domain}]},
            'summary': {'currency': '€',
                        'order_base_price': 132.06,
                        'order_price': 242.83,
                        'order_vats': [{'amount': 46.99, 'vat': 24}],
                        'vat0_order_price': 195.83}}
