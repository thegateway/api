# -*- coding: utf-8 -*-
"""
Mock classes for use by the tests
"""
import copy
import datetime
import json
import random
import uuid

import stripe
from attrdict import AttrDict
from botocore.exceptions import ClientError
from stripe.error import (
    APIConnectionError,
    StripeError,
)

from gwio.core.utils.testing.helpers import fake
from gwio.ext.vat import Currency
from gwio.utils.uuid import get_random_uuid
from ._fakes.products.stripe_data import (
    CAPTURE_RESPONSE_SUCCEEDED,
    STRIPE_ACCOUNT_LINK_OBJECT,
    STRIPE_ACCOUNT_OBJECT,
    STRIPE_EXTERNAL_ACCOUNT_OBJECT,
    STRIPE_TRANSFER_OBJECT,
    StripeIntentConfirmResponse,
    stripe_intent_confirm_response,
)


class MockedCoreCognitoIdpClient:
    _users = dict()

    def _generate_fake_users(self, x):
        for i in range(x):
            user_guid = uuid.uuid4()
            verified = random.choices([True, False])
            while True:
                phone = fake.phone_number()
                if phone[:4] == '+358':
                    break
            phone = ''.join(phone.split())
            yield {
                'Attributes': [{
                    'Name': 'sub',
                    'Value': str(user_guid)
                },
                    {'Name': 'email_verified', 'Value': 'true' if verified else 'false'},
                    {'Name': 'name', 'Value': fake.name()},
                    {'Name': 'phone_number_verified', 'Value': 'true' if verified else 'false'},
                    {'Name': 'phone_number', 'Value': phone},
                    {'Name': 'email', 'Value': fake.email()}],
                'Enabled': True,
                'UserCreateDate': datetime.datetime.now(),
                'UserLastModifiedDate': datetime.datetime.now(),
                'UserStatus': 'CONFIRMED' if verified else 'UNCONFIRMED',
                'Username': str(user_guid)
            }

    def _setup_error_response(self):
        return {
            'Error': {
                'Code': 'MockClientException',
                'Message': 'Exception raised within Mock'
            }
        }

    def __init__(self, **_):
        for x in range(3):
            self._users[x] = list(self._generate_fake_users(50 if x != 2 else 10))
        first = ''.join(random.choice('panda') for _ in range(5))
        second = ''.join(random.choice('squirrel') for _ in range(5))
        self._map = {
            '': (0, first),
            first: (1, second),
            second: (2, None)
        }

    def list_users(self, attr_filter: str = None, pagination_token: str = None):
        if pagination_token is None:
            pagination_token = ''
        try:
            target = self._map[pagination_token]
        except KeyError:
            raise ClientError(self._setup_error_response(), 'ListUsers')
        response_guid = uuid.uuid4()
        response = {
            'ResponseMetaData': {
                'HTTPHeaders': {
                    'connection': 'keep-alive',
                    'content-length': '40000',
                    'content-type': 'application/x-amz-json-1.1',
                    'date': datetime.datetime.now().strftime('%a, %d %b %Y %H:%M:%S'),
                    'x-amzn-requestid': str(response_guid),
                    'HTTPStatusCode': 200,
                    'RequestId': str(response_guid),
                    'RetryAttempts': 0
                }
            },
            'Users': self._users[target[0]]
        }
        if target[1] is not None:
            response['PaginationToken'] = target[1]
        return response

    def add_user_to_group(self, username: uuid.UUID, group: str = None) -> None:
        pass

    def set_custom_attribute(self, _attribute_name: str, _value: str, _username: uuid.UUID) -> dict:
        return {}

    def delete_user(self, username: str) -> None:
        pass

    def describe_user_pool(self, UserPoolId):
        return dict(UserPool=dict(Policies=dict(PasswordPolicy=dict(MinimumLength=8,
                                                                    RequireUppercase=False,
                                                                    RequireLowercase=False,
                                                                    RequireNumbers=False,
                                                                    RequireSymbols=False,
                                                                    TemporaryPasswordValidityDays=7))))


class MockStripeSource:
    valid_card_id = 'card_7357c4rD'
    correct_customer = None

    @classmethod
    def retrieve(cls, source_id):
        if source_id == cls.valid_card_id:
            return dict(id=cls.valid_card_id,
                        customer=cls.correct_customer,
                        last4='0123')


class MockStripeCustomer:
    sources = MockStripeSource

    @classmethod
    def retrieve(cls, *_, **__):
        return cls()


class MockStripeCharge:
    id = None
    storage = {}

    fail_capture = False

    def __getitem__(self, key):
        return getattr(self, key)

    @classmethod
    def create(cls, *_, **kwargs):
        mock_charge = cls()
        mock_charge.id = 'ch_mock_{guid}'.format(guid=str(get_random_uuid()))
        mock_charge.__populate_data(**kwargs)
        cls.storage[mock_charge.id] = mock_charge
        return mock_charge

    @classmethod
    def retrieve(cls, charge_id, **__):
        return cls.storage[charge_id]

    def __populate_data(self, **kwargs):
        try:
            self.captured = kwargs.pop('capture')
        except KeyError:
            pass
        [setattr(self, key, value) for key, value in kwargs.items()]
        return self

    def capture(self, **kwargs):
        if self.fail_capture:
            raise APIConnectionError
        self.__populate_data(**kwargs)
        self.captured = True
        MockStripeCharge.storage[self.id] = self
        return self


class MockInvoiceNotificationEmail():
    counter = 0

    @classmethod
    def _mock_send_invoice_notification(cls, _):
        cls.counter += 1


class MockPaytrailEmail():
    counter = 0

    @classmethod
    def _mock_send_paytrail_payment_link(cls, _):
        cls.counter += 1


class MockedEMail:
    def send(self, src: str, dst: str, *, cc: list = None, bcc: list = None,
             subject: str, html: str = None, text: str = None,
             reply_to: str = None,
             charset: str = 'UTF-8') -> str:
        return 'Message ID'

    def send_templated_email(self, src: str, dst: str, template: str, context: dict, *, cc: list = None,
                             bcc: list = None,
                             subject: str, reply_to: str = None,
                             charset: str = 'UTF-8') -> str:
        return self.send(src=src, dst=dst, subject=subject)


class MockStripeRefund:
    def __init__(self, **_):
        pass

    def to_dict(self):
        return dict(id='re_mock_stripe_fund')


class MockCard:
    def __init__(self):
        self.id = "card_1DBdC0GNOk34JrH4v4xHqjp4"
        self.object = "card"
        self.address_city = None
        self.address_country = None
        self.address_line1 = None
        self.address_line1_check = None
        self.address_line2 = None
        self.address_state = None
        self.address_zip = None
        self.address_zip_check = None
        self.brand = "Visa"
        self.country = "US"
        self.customer = "cus_DcsxJtciYEXqzd"
        self.cvc_check = None
        self.dynamic_last4 = None
        self.exp_month = 8
        self.exp_year = 2019
        self.fingerprint = "XSBwxX8rj88ONnVi"
        self.funding = "credit"
        self.last4 = "4242"
        self.metadata = {}
        self.name = None
        self.tokenization_method = None

    def delete(self):
        pass


class MockSources:
    def create(self, *args, **kwargs):
        return MockCard()

    def list(self):
        return [MockCard()]

    def retrieve(self, *args):
        return MockCard()


class MockCustomer(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self['id'] = str(uuid.uuid4())

    @property
    def sources(self):
        return MockSources()

    @property
    def id(self):
        return self['id']


class MockCharge(AttrDict):
    CHARGES = dict()

    @classmethod
    def get_instance(cls, id=None, **kwargs):
        try:
            return cls.CHARGES[str(id)]
        except KeyError:
            if id is None:
                id = uuid.uuid4()
            cls.CHARGES[str(id)] = cls(id=str(id), **{**kwargs, 'source': dict(last4='4242')})
            return cls.CHARGES[str(id)]

    def __init__(self, **kwargs):
        super().__init__()
        self.update(**kwargs)

    @staticmethod
    def retrieve(id, **kwargs):
        obj = MockCharge.get_instance(id=id, last4='4242')
        obj.update(dict(information='Captured moneys!'))
        return obj

    @staticmethod
    def create(*args, **kwargs):
        return MockCharge.get_instance(id=str(uuid.uuid4()), **{**kwargs, 'source': dict(last4='4242')})

    def capture(self, *args, **kwargs):
        self.update(dict(captured=True))
        return self

    @property
    def data(self):
        return 'fake stripe charge data here'


class MockRefund(dict):
    def __init__(self, **kwargs):
        checks = dict(amount=lambda x: isinstance(x, int),
                      currency=lambda x: x is None or (isinstance(x, str) and x in [c.abbr for c in list(Currency)]))
        for key, func in checks.items():
            assert func(kwargs.get(key)), key
        super().__init__()
        self.update(**kwargs, id='refund_id')


class MockTransfer(AttrDict):
    @staticmethod
    def create(**kwargs):
        transfer = copy.deepcopy(STRIPE_TRANSFER_OBJECT)
        transfer.update(kwargs)
        return transfer


class MockAccount(AttrDict):
    accounts = dict()

    @classmethod
    def create(cls, **kwargs):
        account = AttrDict(copy.deepcopy(STRIPE_ACCOUNT_OBJECT))
        account.update(kwargs)
        account.id = f'acct_{fake.md5()[0:16]}'  # 'acct_1G1rcpDZlZfB4wu5'
        cls.accounts[account.id] = account
        return account

    @classmethod
    def retrieve(cls, id, **kwargs):
        return cls.accounts[id]

    @classmethod
    def modify(cls, id, **kwargs):
        account = cls.accounts[id]
        account.update(kwargs)
        return account

    @classmethod
    def create_external_account(cls, id, external_account, **kwargs):
        try:
            account = cls.accounts[id]
        except KeyError:
            raise PermissionError from KeyError

        account['external_accounts'] = dict(data=STRIPE_EXTERNAL_ACCOUNT_OBJECT,
                                            has_more=False,
                                            object='list',
                                            total_count=1,
                                            url=f'/v1/accounts/{id}/external_accounts')
        return external_account


class MockAccountLink(AttrDict):
    @staticmethod
    def create(**kwargs):
        account = AttrDict(copy.deepcopy(STRIPE_ACCOUNT_LINK_OBJECT))
        account.update(kwargs)
        return account


class MockPaymentIntent(AttrDict):
    intents = dict()
    confirm_responses = dict(requires_capture=stripe_intent_confirm_response(StripeIntentConfirmResponse.CONFIRM_RESPONSE_REQUIRES_CAPTURE),
                             requires_action=stripe_intent_confirm_response(StripeIntentConfirmResponse.CONFIRM_RESPONSE_REQUIRES_ACTION))
    confirm_response = confirm_responses['requires_capture']
    capture_response = CAPTURE_RESPONSE_SUCCEEDED

    def __init__(self, **kwargs):
        super().__init__()
        self.update(**kwargs)

    @classmethod
    def next_response(cls, response: AttrDict):
        cls.confirm_response = response

    @classmethod
    def create(cls, **kwargs):
        checks = dict(amount=lambda x: isinstance(x, int),
                      currency=lambda x: isinstance(x, str) and x in [c.abbr for c in list(Currency)],
                      capture_method=lambda x: isinstance(x, str) and x in ['manual', 'automatic'],
                      payment_method=lambda x: isinstance(x, str))
        for key, func in checks.items():
            assert func(kwargs[key]), key
        intent_id = f'pi_{fake.pystr(24, 24)}'
        intent = cls(id=intent_id, **kwargs)
        cls.intents[intent_id] = intent
        return cls.confirm(intent.id)

    @classmethod
    def confirm(cls, intent_id, **kwargs):
        try:
            intent = cls.intents[intent_id]
        except KeyError:
            raise StripeError(f'No payment intent with id {intent_id}')
        response = cls.confirm_response
        cls.confirm_response = cls.confirm_responses['requires_capture']
        response['id'] = intent_id
        return AttrDict({**response,
                         'amount': intent.amount,
                         'source': AttrDict(last4='4242'),
                         })

    @classmethod
    def capture(cls, intent_id, **kwargs):
        try:
            intent = cls.intents[intent_id]
        except KeyError:
            raise StripeError(f'No payment intent with id {intent_id}')
        response = cls.capture_response
        return AttrDict({**response,
                         'amount': intent.amount,
                         'source': AttrDict(last4='4242'),
                         })

    @classmethod
    def retrieve(cls, intent_id, **kwargs):
        try:
            intent = cls.intents[intent_id]
            intent['status'] = 'requires_capture'
        except KeyError:
            raise StripeError(f'No payment intent with id {intent_id}')
        return intent


class StripeEvent(AttrDict):
    def __init__(self, request_data):
        self.update(**json.loads(request_data))
        self['data']['object'] = AttrDict(self['data']['object'])
        super().__init__()

    def to_dict(self):
        return self


def mock_stripe_construct_event(request_data: bytes, stripe_signature: str, secret: str):
    if stripe_signature == 'a_failure_for_tests':
        raise stripe.error.SignatureVerificationError("test failure", stripe_signature)
    return StripeEvent(request_data)


class _MockNotifyContext:
    received_notifications = None

    def __init__(self):
        self.received_notifications = []

    def notify(self, notification):
        self.received_notifications.append(notification)


class MockUser(_MockNotifyContext):
    email = None
    language = 'fi'

    def __init__(self):
        super().__init__()
        self.email = fake.email()


class MockPayment(_MockNotifyContext):
    payment_type = None

    def __init__(self):
        super().__init__()
        self.payment_type = random.choice(['stripe', 'paytrail', 'cash'])


MockShop = MockUser


def mock_order_fsm_triggered_logic(*, mock_assess=True, mocker, force_exception: bool):
    from gwio.models.orders import Order

    def mock_exception(*args, **kwargs):
        raise Exception('Mocked test exception')

    def mocked_avoid_exceptions(*args, **kwargs):
        pass

    if force_exception:
        if mock_assess:
            mocker.patch.object(Order.API, 'assess', mocked_avoid_exceptions)
        mocker.patch.object(Order.API, 'notify', mock_exception)
        # TODO: Update this mock so that an exception is raised and handled by `_run_explicit_logic_for_state`
        mocker.patch('gwio.utils.fsm.FsmHandler._run_explicit_logic_for_state')
    else:
        if mock_assess:
            mocker.patch.object(Order.API, 'assess', mocked_avoid_exceptions)
        mocker.patch.object(Order.API, 'notify', mocked_avoid_exceptions)
        mocker.patch('gwio.utils.fsm.FsmHandler._run_explicit_logic_for_state')
