import random
import uuid
from datetime import (
    datetime,
    timedelta,
)

import pytest
import pytz
from marshmallow.fields import Field
from pytest_lazyfixture import LazyFixture

from gwio.utils import uuid
from gwio.core.utils.testing.helpers import fake
from gwio.utils.uuid import get_random_uuid
from gwio.utils.fsm import FsmState
from gwio.logic.orders.handler import OrderHandler
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.models import *
from gwio.models.orders import Order
from ._fixtures.orders import (
    valid_order_create_post_data,
    valid_update_put_data,
)
from ._mocks import mock_order_fsm_triggered_logic

assert orders
UserData = user.UserData

# This is just to get better type hints for the functions
A = OrderAction
S = OrderState


def listify(value):
    if isinstance(value, list):
        return value
    return [value]


@pytest.fixture(scope='class')
def test_products(mock_aws, random_products, webshop_guid):
    assert mock_aws
    products = random_products(10, owner_guid=webshop_guid)
    for product in products:
        product.save()
    return products


@pytest.fixture(scope='class')
def test_orders(mock_aws, webshop, customer_identity_id, random_orders, random_webshop):
    random_webshop.save()
    assert mock_aws

    orders = (
            random_orders(
                5,
                shop_guid=webshop.guid,
                _current_state=OrderState.CREATED,
            ) +
            random_orders(
                5,
                buyer_identity_id=customer_identity_id,
                _current_state=OrderState.CREATED,
                shop_guid=random_webshop.guid,
            ) +
            random_orders(
                15,
                shop_guid=webshop.guid,
                _current_state=OrderState.PROCESSING,
            ) +
            random_orders(
                5,
                buyer_identity_id=customer_identity_id,
                shop_guid=random_webshop.guid,
                _current_state=OrderState.PROCESSING,
            ) +
            random_orders(
                5,
                buyer_identity_id=customer_identity_id,
                shop_guid=webshop.guid,
                _current_state=OrderState.COMPLETED,
            ) +
            random_orders(
                5,
                buyer_identity_id=customer_identity_id,
                shop_guid=random_webshop.guid,
                _current_state=OrderState.COMPLETED,
            ) +
            random_orders(
                5,
                shop_guid=webshop.guid,
                _current_state=FsmState._FAILED,
            ) +
            random_orders(
                5,
                shop_guid=random_webshop.guid,
                _current_state=FsmState._FAILED,
            )
    )
    for o in orders:
        o.save()

    yield orders

    for o in orders:
        o.delete()


@pytest.fixture(scope='class')
def test_orders_all_states(random_orders, customer_identity_id, webshop_guid, test_orders):
    new_orders = (random_orders(5,
                                buyer_identity_id=customer_identity_id,
                                _current_state=OrderState.ARCHIVED,
                                shop_guid=webshop_guid) +
                  random_orders(5,
                                buyer_identity_id=customer_identity_id,
                                _current_state=OrderState.CANCELLED,
                                shop_guid=webshop_guid) +
                  random_orders(5,
                                buyer_identity_id=customer_identity_id,
                                _current_state=OrderState.COMPLETED,
                                shop_guid=webshop_guid) +
                  random_orders(5,
                                buyer_identity_id=customer_identity_id,
                                _current_state=OrderState.START,
                                shop_guid=webshop_guid)
                  )

    for order in new_orders:
        order.save()

    yield (test_orders + new_orders)

    for order in new_orders:
        order.delete()


@pytest.fixture(scope='function')
def customer(mock_aws, test_customer, customer_identity_id):
    assert mock_aws
    test_customer.save()
    yield test_customer
    test_customer.delete()


@pytest.mark.usefixtures('mock_aws', 'mock_stripe')
class TestApiShopsOrders():
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    # Tests for GET /shops/<shop_guid>/orders/
    def test_orders_list_admin(self, as_admin, test_orders):
        shop_guids = []
        for order in test_orders:
            shop_guids.extend(order.shop_guids)
        shop_guids = set(shop_guids)
        assert shop_guids
        for shop_guid in shop_guids:
            expected_order_count = len([o for o in Order.scan() if shop_guid in o.shop_guids])
            assert expected_order_count, 'Test broken'
            res = as_admin.get(f"/webshops/{shop_guid}/orders/")
            assert 200 == res.status_code, res.json

            assert expected_order_count == len(res.json)

    def test_orders_list_shopkeeper(self, as_shopkeeper, test_orders, webshop_guid):
        res = as_shopkeeper.get(f"/webshops/{webshop_guid}/orders/")
        assert 200 == res.status_code, res.json

        assert 0 < len([o for o in Order.scan(shop_guids__contains=str(webshop_guid))]) == len(res.json)

    def test_orders_list_shopkeeper_wrong_shop(self, as_shopkeeper, test_orders, webshop_guid):
        shop_guids = next(o.shop_guids for o in test_orders if webshop_guid not in o.shop_guids)
        res = as_shopkeeper.get(f"/webshops/{shop_guids[0]}/orders/")
        assert 403 == res.status_code, res.json

    def test_orders_list_customer(self, as_customer, webshop_guid):
        res = as_customer.get(f"/webshops/{webshop_guid}/orders/")
        assert 403 == res.status_code, res.json

    def test_orders_list_unauthenticated(self, as_anonymous, webshop_guid):
        res = as_anonymous.get(f"/webshops/{webshop_guid}/orders/")
        assert 401 == res.status_code, res.json

    # Tests for GET /shops/<shop_guid>/orders/<order_guid>/
    def test_orders_single_admin(self, as_admin, test_orders):
        assert test_orders
        order = random.choice(test_orders)
        for o in [order, random.choice([o for o in test_orders if not set(order.shop_guids).intersection(o.shop_guids)])]:
            res = as_admin.get(f"/webshops/{o.shop_guids[0]}/orders/{o.guid}/")
            assert 200 == res.status_code, res.json
            assert o.guid == uuid.UUID(res.json['guid'])

    def test_orders_single_shopkeeper(self, as_shopkeeper, webshop_guid, test_orders):
        order = next(o for o in test_orders if webshop_guid in o.shop_guids)
        res = as_shopkeeper.get(f"/webshops/{webshop_guid}/orders/{order.guid}/")
        assert 200 == res.status_code, res.json
        assert order.guid == uuid.UUID(res.json['guid'])

    def test_orders_single_shopkeeper_wrong_shop(self, as_shopkeeper, webshop_guid, test_orders):
        order = next(o for o in test_orders if webshop_guid not in o.shop_guids)
        res = as_shopkeeper.get(f"/webshops/{webshop_guid}/orders/{order.guid}/")
        assert 404 == res.status_code, res.json

    def test_orders_single_unauthenticated(self, as_anonymous, webshop_guid, test_orders):
        assert test_orders
        for o in test_orders:
            res = as_anonymous.get(f"/webshops/{webshop_guid}/orders/{o.guid}/")
            assert 401 == res.status_code, res.json

    @pytest.mark.parametrize('state', [state for state in OrderState if not state.value.startswith('_') and state != FsmState._ANY])
    def test_get_orders_in_state(self, as_shopkeeper, webshop_guid, test_orders_all_states, state):
        assert test_orders_all_states
        res = as_shopkeeper.get(f"/webshops/{webshop_guid}/orders?state={state}")
        assert 200 == res.status_code, res.json
        orders = list(Order.scan())
        expected_orders = list(filter(lambda order: order.current_state == state and
                                                    webshop_guid in order.shop_guids,
                                      orders))
        assert len(expected_orders) == len(res.json)
        assert 0 < len(expected_orders)
        assert len(orders) > len(expected_orders)

    def test_get_orders_in_state_after(self, as_shopkeeper, webshop_guid, test_orders):
        state = OrderState.CREATED
        orders = list(filter(lambda order: order.current_state == state and webshop_guid in order.shop_guids, test_orders))
        orders[0]._transition_history.append(dict(
            action=OrderAction.CREATE,
            state=OrderState.CREATED,
            timestamp=datetime.now(tz=pytz.UTC) + timedelta(hours=-2),
        ))
        orders[0].save()
        orders[1]._transition_history.append(dict(
            action=OrderAction.CREATE,
            state=OrderState.CREATED,
            timestamp=datetime.now(tz=pytz.UTC) + timedelta(hours=-1),
        ))
        orders[1].save()
        orders[2]._transition_history.append(dict(
            action=OrderAction.CREATE,
            state=OrderState.CREATED,
            timestamp=datetime.now(tz=pytz.UTC) + timedelta(minutes=-30),
        ))
        orders[2].save()
        url = f"/webshops/{webshop_guid}/orders?state={state}&after={(datetime.utcnow() + timedelta(minutes=-90)).isoformat()}"
        res = as_shopkeeper.get(url)
        assert 200 == res.status_code, res.json
        response_guids = [uuid.UUID(x['guid']) for x in res.json]
        assert orders[0].guid not in response_guids
        assert all(x in response_guids for x in (orders[1].guid, orders[2].guid))

    def test_get_orders_after_no_state(self, as_shopkeeper, webshop_guid):
        url = f"/webshops/{webshop_guid}/orders?after={(datetime.utcnow() + timedelta(minutes=-90)).isoformat()}"
        res = as_shopkeeper.get(url)
        assert 409 == res.status_code, res.json
        assert "`state` is required if `after` is given" in res.json['message']


@pytest.mark.usefixtures('mock_aws', 'test_products', 'test_orders')
class TestApiWebshopsOrdersCreate():
    """ Tests for POST /customers/<customer_guid>/orders/ """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    def test_create(self, mocker, customer, as_customer, webshop, test_products, customer_identity_id, customer_identity_guid):
        mock_order_fsm_triggered_logic(mock_assess=False, mocker=mocker, force_exception=False)

        post_data = valid_order_create_post_data(products=test_products, webshop_guid=webshop.guid)

        res = as_customer.post(f"/customers/{customer_identity_guid}/orders/", json=post_data)
        assert 201 == res.status_code, res.json
        order = Order.get(guid=uuid.UUID(res.json['guid']))
        assert S.CREATED == order.current_state

        assert customer_identity_id == order.buyer_identity_id
        assert webshop.guid in order.shop_guids

        assert post_data['customer_name'] == res.json['customer']['name']

        # Test products and quantities got saved correctly
        qtys = dict([(x.guid, x.qty) for x in order.product_lines])
        assert len(post_data['products']) == len(order.product_lines)
        assert all([x['qty'] == qtys[x['guid']] for x in post_data['products']])

        assert as_customer.token_payload['pin'] == order.customer.pin
        assert 'pin' not in res.json['customer']

    @pytest.mark.parametrize('post_data, missing_key', [
        (dict(customer_name=fake.name(), products=[dict(qty=1, shop_guid=uuid.uuid4())]), 'products.guid'),
        (dict(customer_name=fake.name(), products=[dict(guid=get_random_uuid(), shop_guid=uuid.uuid4())]), 'products.qty'),
        (dict(customer_name=fake.name(), products=[dict(guid=get_random_uuid(), qty=1)]), 'products.shop_guid'),
        (dict(customer_name=fake.name()), 'products'),
        (dict(products=[dict(guid=get_random_uuid(), qty=1, shop_guid=uuid.uuid4())]), 'customer_name')
    ])
    def test_lacking_params(self, as_customer, webshop_guid, post_data, missing_key, customer_identity_guid):
        res = as_customer.post(f"/customers/{customer_identity_guid}/orders/", json=post_data)
        assert 409 == res.status_code, res.json

        try:
            parent_key, missing_key = missing_key.split('.')
        except ValueError:
            pass
        expected_error_message = f'{{"{missing_key}": ["{Field.default_error_messages["required"]}"]}}'
        try:
            expected_error_message = f'{{"{parent_key}": {{"0": {expected_error_message}}}}}'
        except UnboundLocalError:
            pass

        assert expected_error_message == res.json['message']


@pytest.mark.usefixtures('mock_aws')
class TestApiShopsOrdersUpdate():
    """ Tests for PUT /webshops/<webshop_guid>/orders/<order_guid>/ """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    @pytest.mark.parametrize('as_x, as_x_name', [(pytest.lazy_fixture('as_admin'), 'as_admin'),
                                                 (pytest.lazy_fixture('as_shopkeeper'), 'as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_update(self, mocker, order_finder, test_products, test_orders, webshop_guid, as_x, as_x_name):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False, mock_assess=False)

        post_data = valid_update_put_data(products=test_products)

        webshop_order = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
                                                lambda order: webshop_guid in order.shop_guids ,
                                                lambda order: order.current_state == S.CREATED,  # Need to see the state change to FAILED is saved
                                                orders=test_orders)
        orders = dict(as_admin=[webshop_order,
                                order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
                                                        lambda order: webshop_guid in order.shop_guids,  # Admin can update any shop's orders
                                                        lambda order: order.current_state == S.CREATED,  # Need to see the state change to FAILED is saved
                                                        orders=test_orders)],
                      as_shopkeeper=[webshop_order])

        for order in orders[as_x_name]:
            res = as_x.put(f"/webshops/{webshop_guid}/orders/{order.guid}/", json=post_data)
            assert 200 == res.status_code, res.json
            order.refresh()
            assert S.CREATED == order.current_state

            # Test products and quantities got updated correctly
            qtys = dict([(x.guid, x.qty) for x in order.product_lines])
            assert len(post_data['products']) == len(order.product_lines)
            assert all([x['qty'] == qtys[x['guid']] for x in post_data['products']])

    def test_update_invalid_webshop_guid(self, as_shopkeeper, order_finder, test_products, test_orders, webshop_guid):
        order = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
                                        lambda order: webshop_guid in order.shop_guids,
                                        orders=test_orders)
        res = as_shopkeeper.put(f"/webshops/{get_random_uuid()}/orders/{order.guid}/", json=valid_update_put_data(products=test_products))
        assert 403 == res.status_code, res.json

    @pytest.mark.parametrize('as_x', [pytest.lazy_fixture('as_admin'),
                                      pytest.lazy_fixture('as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    @pytest.mark.parametrize('order_guid', [None, get_random_uuid()],
                             ids=lambda x: 'existing_order' if x is None else 'non_existing_order')
    def test_update_invalid_order_guid(self, webshop_guid, test_products, order_finder, test_orders, as_x, order_guid):
        if order_guid is None:
            order_guid = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=A.UPDATE),
                                                 lambda order: webshop_guid not in order.shop_guids,
                                                 orders=test_orders).guid
        res = as_x.put(f"/webshops/{webshop_guid}/orders/{order_guid}/", json=valid_update_put_data(products=test_products))
        assert 404 == res.status_code, res.json

    @pytest.mark.parametrize('as_x, expected_code', [(pytest.lazy_fixture('as_customer'), 403),
                                                     (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    @pytest.mark.parametrize('shop_guid', [pytest.lazy_fixture('webshop_guid'), get_random_uuid()],
                             ids=lambda x: 'existing_customer' if isinstance(x, LazyFixture) else 'non_existing_customer')
    def test_update_non_shopkeeper(self, as_x, expected_code, shop_guid):
        res = as_x.put(f"/webshops/{shop_guid}/orders/{get_random_uuid()}/")
        assert expected_code == res.status_code, res.json


_admin_shopkeeper_actions = [A.PLACE, A.ARCHIVE, A.CANCEL]


@pytest.mark.usefixtures('mock_aws')
class TestApiShopsOrdersActions():
    """ Tests for PUT /shops/<shop_guid>/orders/<order_guid>/_accept
        Tests for PUT /shops/<shop_guid>/orders/<order_guid>/_place
        Tests for PUT /shops/<shop_guid>/orders/<order_guid>/_archive
        Tests for PUT /shops/<shop_guid>/orders/<order_guid>/_cancel
     """
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    def test_order_action_admin(self, mocker, action, as_admin, order_finder, test_orders, webshop):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)
        mocker.patch.object(Order, 'status_invalid', False)

        # Checking that the admin can access the action endpoint for different shops
        for o in [order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=action),
                                          lambda order: webshop.guid in order.shop_guids,
                                          orders=test_orders),
                  order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=action),
                                          lambda order: webshop.guid not in order.shop_guids,
                                          orders=test_orders)]:
            start_state = o.current_state
            res = as_admin.put(f"/webshops/{o.shop_guids[0]}/orders/{o.guid}/_{action}")
            assert 200 == res.status_code, res.json
            order = Order.get(guid=o.guid)
            assert start_state != order.current_state

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    def test_order_action_shopkeeper(self, mocker, order_finder, test_orders, as_shopkeeper, webshop, action):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)
        mocker.patch.object(Order, 'status_invalid', False)
        o = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=action),
                                    lambda order: webshop.guid in order.shop_guids,
                                    orders=test_orders)
        start_state = o.current_state
        res = as_shopkeeper.put(f"/webshops/{webshop.guid}/orders/{o.guid}/_{action}")
        assert 200 == res.status_code, res.json
        order = Order.get(guid=o.guid)
        assert start_state != order.current_state

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    def test_order_action_shopkeeper_wrong_shop(self, order_finder, test_orders, as_shopkeeper, webshop, action):
        o = order_finder.find_order(lambda order: webshop.guid not in order.shop_guids,
                                    orders=test_orders)
        res = as_shopkeeper.put(f"/webshops/{o.shop_guids[0]}/orders/{o.guid}/_{action}")
        assert 403 == res.status_code, res.json

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    @pytest.mark.parametrize("as_x",
                             [pytest.lazy_fixture('as_admin'), pytest.lazy_fixture('as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_order_action_authorized_action_unavailable(self, order_finder, test_orders, webshop, action, as_x):
        o = order_finder.find_order(lambda order: order_finder.check_invalid_action(fsm=self.fsm, order=order, action=action),
                                    lambda order: webshop.guid in order.shop_guids,
                                    orders=test_orders)
        res = as_x.put(f"/webshops/{o.shop_guids[0]}/orders/{o.guid}/_{action}")
        assert 409 == res.status_code, res.json

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    @pytest.mark.parametrize("as_x",
                             [pytest.lazy_fixture('as_admin'), pytest.lazy_fixture('as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_order_action_authorized_shop_order_mismatch(self, test_orders, webshop, action, as_x):
        o = next(o for o in test_orders if webshop.guid not in o.shop_guids)
        res = as_x.put(f"/webshops/{webshop.guid}/orders/{o.guid}/_{action}")
        assert 404 == res.status_code, res.json

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    @pytest.mark.parametrize("as_x, expected_code",
                             [(pytest.lazy_fixture('as_admin'), 404), (pytest.lazy_fixture('as_shopkeeper'), 403)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_order_action_authorized_nonexisting_shop(self, test_orders, action, as_x, expected_code):
        res = as_x.put(f"/webshops/{get_random_uuid()}/orders/{test_orders[0].guid}/_{action}")
        assert expected_code == res.status_code, res.json

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    @pytest.mark.parametrize("as_x",
                             [pytest.lazy_fixture('as_admin'), pytest.lazy_fixture('as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_order_action_authorized_nonexisting_order(self, webshop, action, as_x):
        res = as_x.put(f"/webshops/{webshop.guid}/orders/{get_random_uuid()}/_{action}")
        assert 404 == res.status_code, res.json

    # TODO: Find out why the test takes ridiculous amounts of memory and fix it
    # @pytest.mark.parametrize('action', [OrderAction.PLACE, OrderAction.ACCEPT, OrderAction.REVIEW, OrderAction.ARCHIVE, OrderAction.CANCEL])
    # def test_order_action_customer(self, test_orders, webshop_guid, as_customer, action):
    #    assert test_orders
    #    for o in test_orders:
    #        res = as_customer.put(f"/webshops/{webshop_guid}/orders/{o.guid}/_{action}")
    #        assert 403 == res.status_code

    @pytest.mark.parametrize('action', _admin_shopkeeper_actions)
    def test_order_action_unauthenticated(self, test_orders, webshop_guid, as_anonymous, action):
        assert test_orders
        for o in test_orders:
            res = as_anonymous.put(f"/webshops/{webshop_guid}/orders/{o.guid}/_{action}")
            assert 401 == res.status_code, res.json

    def test_webshops_order_place_invalid(self, mocker, order_finder, test_orders, as_shopkeeper, webshop_guid):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)
        mocker.patch.object(Order, 'status_invalid', True)

        order = order_finder.find_order(lambda order: order_finder.check_valid_action(fsm=self.fsm, order=order, action=OrderAction.PLACE),
                                        lambda order: webshop_guid in order.shop_guids,
                                        lambda order: order.current_state == OrderState.CREATED,
                                        orders=test_orders)
        start_state = order.current_state
        res = as_shopkeeper.put(f"/webshops/{webshop_guid}/orders/{order.guid}/_{OrderAction.PLACE}")
        assert 409 == res.status_code, res.json
        assert 'Order is invalid for placement.' in res.json['message']
        order.refresh()
        assert start_state == order.current_state
