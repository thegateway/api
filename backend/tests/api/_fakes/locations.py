from gwio.core.utils.testing.helpers import fake


def random_location_data(**kwargs):
    return {'name': fake.name(),
            'description': fake.sentence(),
            'archived': None,
            'tags': [],
            'latitude': fake.latitude(),
            'longitude': fake.longitude(),
            **kwargs}
