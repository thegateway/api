# -*- coding: utf-8 -*-
"""
Random data for base product
"""
import random
from decimal import Decimal

from gwio.core.utils.testing.helpers import (
    THE_ONE_CURRENCY,
    fake,
    image_url,
)
from gwio.ext.vat import (
    Vat,
    VatPrice,
)
from gwio.models.pricing import PriceType

DEFAULT_VAT_CATEGORIES = [10, 14, 24]


def random_base_product_data(vat_categories=DEFAULT_VAT_CATEGORIES):
    return dict(
        name=fake.sentence(3),
        sku=fake.ean(),
        type='default',
        brief=fake.word(),
        desc=fake.paragraph(),
        images=[image_url() for _ in range(random.randint(0, 10))],
        data={},
        pricing=None,
        base_price_type=random.choice(list(PriceType)),
        base_price=VatPrice(amount=round(Decimal(random.uniform(0.1, 100.0)), 2), currency=THE_ONE_CURRENCY),
        cost_price=VatPrice(amount=round(Decimal(random.uniform(0.1, 100.0)), 2), currency=THE_ONE_CURRENCY),
        vat=Vat(random.choice(vat_categories)),
        size=dict(
            height=random.randint(50, 100),
            length=random.randint(50, 100),
            width=random.randint(50, 100)
        ),
        weight=random.randint(100, 1000)
    )


def random_base_product_json(vat_categories=DEFAULT_VAT_CATEGORIES):
    return dict(
        name=fake.sentence(3),
        sku=fake.ean(),
        type='default',
        brief=fake.word(),
        desc=fake.paragraph(),
        image=image_url(),
        data={},
        pricing=None,
        base_price_type=random.choice([str(x) for x in list(PriceType)]),
        base_price=dict(
            amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
            currency='€',
            vat_percent=random.choice(vat_categories)),
        cost_price=dict(
            amount=round(Decimal(random.uniform(0.1, 100.0)), 2),
            currency='€',
            vat_percent=random.choice(vat_categories)),
        vat=str(Vat(random.choice(vat_categories)))
    )
