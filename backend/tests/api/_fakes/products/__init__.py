# -*- coding: utf-8 -*-
__all__ = [
    'random_base_product_data',
    'random_base_product_json',
    'random_product_data',
]

from .base import random_base_product_data, random_base_product_json
from ._vertical import random_product_data
