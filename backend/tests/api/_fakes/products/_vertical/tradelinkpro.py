import random
import uuid
from decimal import Decimal

from gwio.core.utils.testing.helpers import fake
from gwio.models.products._vertical.tradelinkpro.product import Category, Version
from .. import random_base_product_data
from ._registry import random_data_for


@random_data_for('tradelinkpro')
def random_remppa_data(**kwargs):
    d = random_base_product_data()
    d.update(
        # category = random.choice(list(Category)),
        category = Category.LED,
        size = f'{random.randint(100, 10000)}mm',
        technology = ' '.join(fake.words(2)),
        version = random.choice(list(Version)),
        watts = Decimal(random.randint(10, 1000))/10,
        light_type = ' '.join(fake.words(2)),
    )
    if d['category'] == Category.EXISTING:
        d.update(
            # Fields specific for product type 'existing'
            adapter = ' '.join(fake.words(2)),
            replacement = '-'.join(fake.words(random.randint(1,5))),
            replacement_guid = uuid.uuid4(),
        )
        d.pop('base_price')
        d.pop('base_price_type')
    else:
        d.update(
            # Fields specific for product type 'led'
            code = '-'.join(fake.words(random.randint(1,5))),
            colour_temperature = f'{random.randint(40, 50)*100}K',
            motion_sensor = ' '.join(fake.words(2)),
        )

    d.update(**kwargs)
    return d
