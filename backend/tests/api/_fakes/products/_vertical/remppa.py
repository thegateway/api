# -*- coding: utf-8 -*-
from random import choice

from .. import random_base_product_data
from ._registry import random_data_for


@random_data_for('remppa')
def random_remppa_data(**kwargs):
    d = random_base_product_data()
    d.update(dict(
        type=choice(['service', 'default']),
    ))
    d.update(**kwargs)
    return d
