# -*- coding: utf-8 -*-
from typing import Callable

from gwio.environment import Env

_random_data = dict()

VERTICAL = Env.VERTICAL


def random_product_data(vertical: str = None, **kwargs) -> Callable:
    return _random_data[VERTICAL if vertical is None else vertical](**kwargs)


def random_data_for(vertical: str) -> Callable:
    def _register(fn: Callable) -> Callable:
        _random_data[vertical] = fn
        return fn

    return _register
