# -*- coding: utf-8 -*-

from .. import random_base_product_data
from ._registry import random_data_for


@random_data_for('sma')
def random_sma_data(**kwargs):
    d = random_base_product_data(vat_categories=[5, 8, 23])
    d.update(**kwargs)
    return d
