# -*- coding: utf-8 -*-
from random import choice

from gwio.core.utils.testing.helpers import fake
from .. import random_base_product_data
from ._registry import random_data_for


@random_data_for('imeds')
def random_imeds_data(**kwargs):
    d = dict(**random_base_product_data())
    d.update(dict(
        code=fake.ssn(),
        type=choice(['medicine', 'prescription_medicine', 'default']),
        valid_until=None,
        substitution_group=str(int(fake.pydecimal(left_digits=10, right_digits=0) // 1)),
    ))
    d.update(**kwargs)
    return d
