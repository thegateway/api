import collections
import copy
import enum

from faker import Faker

FAKER = Faker('fi_FI')

CONFIRM_RESPONSE_REQUIRES_ACTION = {
    "id": None,
    "object": "payment_intent",
    "allowed_source_types": [
        "card"
    ],
    "amount": 420,
    "canceled_at": None,
    "cancellation_reason": None,
    "capture_method": "automatic",
    "client_secret": None,
    "confirmation_method": "automatic",
    "created": 1563190627,
    "currency": "eur",
    "description": None,
    "last_payment_error": None,
    "livemode": False,
    "next_action": {
        "type": "use_stripe_sdk",
        "use_stripe_sdk": {
            "type": "three_d_secure_redirect",
            "stripe_js": None
        }
    },
    "next_source_action": {
        "type": "use_stripe_sdk",
        "use_stripe_sdk": {
            "type": "three_d_secure_redirect",
            "stripe_js": None
        }
    },
    "payment_method": None,
    "payment_method_types": [
        "card"
    ],
    "receipt_email": None,
    "shipping": None,
    "source": None,
    "status": "requires_action",
    "transfer_group": None
}

CONFIRM_RESPONSE_REQUIRES_CAPTURE = {
    "id": None,
    "object": "payment_intent",
    "allowed_source_types": [
        "card"
    ],
    "amount": 420,
    "canceled_at": None,
    "cancellation_reason": None,
    "capture_method": "automatic",
    "client_secret": None,
    "confirmation_method": "automatic",
    "created": 1563190627,
    "currency": "eur",
    "description": None,
    "last_payment_error": None,
    "livemode": False,
    "next_action": {
        "type": "use_stripe_sdk",
        "use_stripe_sdk": {
            "type": "three_d_secure_redirect",
            "stripe_js": None
        }
    },
    "next_source_action": {
        "type": "use_stripe_sdk",
        "use_stripe_sdk": {
            "type": "three_d_secure_redirect",
            "stripe_js": None
        }
    },
    "payment_method": None,
    "payment_method_types": [
        "card"
    ],
    "receipt_email": None,
    "shipping": None,
    "source": None,
    "status": "requires_capture",
    "transfer_group": None
}

CAPTURE_RESPONSE_SUCCEEDED = {
    "id": None,
    "object": "payment_intent",
    "allowed_source_types": [
        "card"
    ],
    "amount": 420,
    "canceled_at": None,
    "cancellation_reason": None,
    "capture_method": "automatic",
    "client_secret": None,
    "confirmation_method": "automatic",
    "created": 1563277975,
    "currency": "eur",
    "description": None,
    "last_payment_error": None,
    "livemode": False,
    "next_action": None,
    "next_source_action": None,
    "payment_method": None,
    "payment_method_types": [
        "card"
    ],
    "receipt_email": None,
    "shipping": None,
    "source": None,
    "status": "succeeded",
    "transfer_group": None
}

RETRIEVE_RESPONSE = {
    "id": "pi_1EwSURGNOk34JrH4bkyLYWeI",
    "object": "payment_intent",
    "amount": 420,
    "amount_capturable": 0,
    "amount_received": 0,
    "application": None,
    "application_fee_amount": None,
    "canceled_at": None,
    "cancellation_reason": None,
    "capture_method": "automatic",
    "charges": {
        "object": "list",
        "data": [],
        "has_more": False,
        "total_count": 0,
        "url": "/v1/charges?payment_intent=pi_1EwSURGNOk34JrH4bkyLYWeI"
    },
    "client_secret": "pi_1EwSURGNOk34JrH4bkyLYWeI_secret_TDcjGmKrifgsDLuidffjycpZH",
    "confirmation_method": "automatic",
    "created": 1563190627,
    "currency": "eur",
    "customer": None,
    "description": None,
    "invoice": None,
    "last_payment_error": {
        "code": "payment_intent_authentication_failure",
        "doc_url": "https://stripe.com/docs/error-codes/payment-intent-authentication-failure",
        "message": "The provided PaymentMethod has failed authentication. You can provide payment_method_data or a new PaymentMethod to attempt to fulfill this PaymentIntent again.",
        "payment_method": {
            "id": "pm_1EwSUuGNOk34JrH4jaEvlyeh",
            "object": "payment_method",
            "billing_details": {
                "address": {
                    "city": None,
                    "country": None,
                    "line1": None,
                    "line2": None,
                    "postal_code": "55555",
                    "state": None
                },
                "email": None,
                "name": "adfs",
                "phone": None
            },
            "card": {
                "brand": "visa",
                "checks": {
                    "address_line1_check": None,
                    "address_postal_code_check": "unchecked",
                    "cvc_check": "unchecked"
                },
                "country": "DE",
                "exp_month": 10,
                "exp_year": 2051,
                "fingerprint": "wZfjagvRKUSyt4GJ",
                "funding": "credit",
                "generated_from": None,
                "last4": "3184",
                "three_d_secure_usage": {
                    "supported": True
                },
                "wallet": None
            },
            "created": 1563190656,
            "customer": None,
            "livemode": False,
            "metadata": {},
            "type": "card"
        },
        "type": "invalid_request_error"
    },
    "livemode": False,
    "metadata": {
        "user_identity_id": "fake_identity",
        "order_guid": "1234qwerasdf"
    },
    "next_action": None,
    "on_behalf_of": None,
    "payment_method": None,
    "payment_method_options": {
        "card": {
            "request_three_d_secure": "automatic"
        }
    },
    "payment_method_types": [
        "card"
    ],
    "receipt_email": None,
    "review": None,
    "setup_future_usage": None,
    "shipping": None,
    "source": None,
    "statement_descriptor": "kauppa: qwer1234",
    "status": "requires_capture",
    "transfer_data": None,
    "transfer_group": None
}

STRIPE_TRANSFER_OBJECT = {
    "id": "tr_1G0ilg2eZ4KYloaCK8EBFrVO",
    "object": "transfer",
    "amount": 8821,
    "amount_reversed": 0,
    "balance_transaction": "txn_39XiJ02eZTKYdo2CpwuJ1rbA",
    "created": 1578982845,
    "currency": "usd",
    "description": None,
    "destination": "acct_3AopmwI9P2w6jkMZ",
    "destination_payment": "py_3m0ilbIbPbw68kMZByYUpmNa",
    "livemode": False,
    "metadata": {},
    "reversals": {
        "object": "list",
        "data": [],
        "has_more": False,
        "url": "/v1/transfers/tr_1G0ilg2eZ4KYloaCK8EBFrVO/reversals"
    },
    "reversed": False,
    "source_transaction": "ch_4G0ild2eZvKYlo2CHnFEoG4K",
    "source_type": "card",
    "transfer_group": "group_ch_4G0ild2NZvKYl92CHnFEoOeK"
}

STRIPE_ACCOUNT_OBJECT = {
  "business_profile": {
    "mcc": None,
    "name": None,
    "product_description": None,
    "support_address": None,
    "support_email": None,
    "support_phone": None,
    "support_url": None,
    "url": None
  },
  "business_type": None,
  "capabilities": {
    "card_payments": "inactive",
    "transfers": "inactive"
  },
  "charges_enabled": False,
  "country": "PL",
  "created": 1579255207,
  "default_currency": "pln",
  "details_submitted": False,
  "email": None,
  "external_accounts": {
    "data": [],
    "has_more": False,
    "object": "list",
    "total_count": 0,
    "url": "/v1/accounts/acct_1G1rcpDZlZfB4wu5/external_accounts"
  },
  "id": "acct_1G1rcpDZlZfB4wu5",
  "metadata": {},
  "object": "account",
  "payouts_enabled": False,
  "requirements": {
    "current_deadline": None,
    "currently_due": [
      "business_profile.mcc",
      "business_profile.url",
      "business_type",
      "external_account",
      "relationship.account_opener",
      "tos_acceptance.date",
      "tos_acceptance.ip"
    ],
    "disabled_reason": "requirements.past_due",
    "eventually_due": [
      "business_profile.mcc",
      "business_profile.url",
      "business_type",
      "external_account",
      "relationship.account_opener",
      "tos_acceptance.date",
      "tos_acceptance.ip"
    ],
    "past_due": [
      "business_profile.mcc",
      "business_profile.url",
      "business_type",
      "external_account",
      "relationship.account_opener",
      "tos_acceptance.date",
      "tos_acceptance.ip"
    ],
    "pending_verification": []
  },
  "settings": {
    "branding": {
      "icon": None,
      "logo": None,
      "primary_color": None
    },
    "card_payments": {
      "decline_on": {
        "avs_failure": False,
        "cvc_failure": False
      },
      "statement_descriptor_prefix": None
    },
    "dashboard": {
      "display_name": None,
      "timezone": "Etc/UTC"
    },
    "payments": {
      "statement_descriptor": "",
      "statement_descriptor_kana": None,
      "statement_descriptor_kanji": None
    },
    "payouts": {
      "debit_negative_balances": False,
      "schedule": {
        "delay_days": 7,
        "interval": "daily"
      },
      "statement_descriptor": None
    }
  },
  "tos_acceptance": {
    "date": None,
    "ip": None,
    "user_agent": None
  },
  "type": "custom"
}

STRIPE_EXTERNAL_ACCOUNT_OBJECT = {
  "account": "acct_1G2UsbBys6Do5vik",
  "account_holder_name": "Test account #1",
  "account_holder_type": "company",
  "bank_name": "STRIPE TEST BANK",
  "country": "PL",
  "currency": "pln",
  "default_for_currency": True,
  "fingerprint": "yeS2MOkYUVmhJ8Pb",
  "id": "ba_1G5VCzBy46dz5vIKnOfoSEuC",
  "last4": "3000",
  "metadata": {},
  "object": "bank_account",
  "routing_number": "110000000",
  "status": "new"
}

STRIPE_ACCOUNT_LINK_OBJECT = {
    'object': 'account_link',
    'created': 1579255227,
    'expires_at': 1579255527,
    'url': 'https://connect.stripe.com/setup/c/t9S9PrCoNlKm'
}

class StripeIntentConfirmResponse(enum.Enum):
    CONFIRM_RESPONSE_REQUIRES_ACTION = enum.auto()
    CONFIRM_RESPONSE_REQUIRES_CAPTURE = enum.auto()


STRIPE_INTENT_CONFIRM_RESPONSES = {
    StripeIntentConfirmResponse.CONFIRM_RESPONSE_REQUIRES_ACTION: dict(
        template=CONFIRM_RESPONSE_REQUIRES_ACTION,
        update_fields=dict(id=FAKER.pystr(),
                           client_secret=FAKER.pystr(),
                           next_action=dict(use_source_sdk=
                                            dict(stripe_js=FAKER.url())),
                           next_source_action=dict(use_source_sdk=
                                                   dict(stripe_js=FAKER.url())),
                           source=FAKER.pystr()
                           )),
    StripeIntentConfirmResponse.CONFIRM_RESPONSE_REQUIRES_CAPTURE: dict(
        template=CONFIRM_RESPONSE_REQUIRES_CAPTURE,
        update_fields=dict(id=FAKER.pystr(),
                           client_secret=FAKER.pystr(),
                           payment_method=FAKER.pystr(),
                           )),
}


def update(d: dict, u: dict) -> dict:
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


def stripe_intent_confirm_response(response_type: StripeIntentConfirmResponse = StripeIntentConfirmResponse.CONFIRM_RESPONSE_REQUIRES_CAPTURE,
                                   update_fields: dict = None):
    if update_fields is None:
        update_fields = dict()
    update_fields = {**copy.deepcopy(STRIPE_INTENT_CONFIRM_RESPONSES[response_type]['update_fields']), **update_fields}
    return update(STRIPE_INTENT_CONFIRM_RESPONSES[response_type]['template'], update_fields)
