from gwio.core.utils.testing.helpers import fake
from gwio.models.brands import Brand


def random_brand_data(**values):
    data = {
        **dict(
            name=fake.sentence(nb_words=3),
            contact=dict(
                name=fake.name(),
                address=fake.address(),
                phone_number=fake.phone_number(),
                email=fake.email()),
            data={},
        ),
        **values
    }
    data['guid'] = Brand.API.generate_uuid(name=data['name'], ns=Brand.API.namespace)
    return data
