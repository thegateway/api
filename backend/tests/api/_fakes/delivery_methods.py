# -*- coding: utf-8 -*-
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from .products import random_base_product_data, random_base_product_json


def random_delivery_method_data(**kwargs):
    d = random_base_product_data()
    d.update(type=PRODUCT_TYPE_DELIVERY_METHOD)
    d.update(**kwargs)
    return d

def random_delivery_method_json(**kwargs):
    d = random_base_product_json()
    d.update(type=PRODUCT_TYPE_DELIVERY_METHOD)
    d.update(**kwargs)
    return d

