import datetime
from decimal import Decimal
from random import choice, randint

from gwio.core.utils.testing.helpers import fake
from gwio.models.modifiers import ModifierMethodType
from gwio.models.tags import Tag


def random_tag_data(**values):
    data = {
        **dict(
            name=fake.sentence(nb_words=3),
            type=choice(list(Tag.Type)),
            data=None,
            duration=None),
        **values}

    if data['type'] == Tag.Type.CAMPAIGN and not data['duration']:
        data['duration'] = dict(
            start=datetime.datetime.utcnow(),
            end=datetime.datetime.utcnow(),
        )
        data['data'] = dict(
            desc=fake.sentence(),
            method=choice(list(ModifierMethodType)),
            amount=Decimal(randint(1, 10)),
        )

    return data
