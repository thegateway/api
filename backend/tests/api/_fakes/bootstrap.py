# coding=utf-8
from gwio.models.tags import Tag

from gwio.core.utils.testing.helpers import fake


def random_tag_name():
    return '-'.join(fake.words(2))


def create_random_category_tag():
    tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
    tag.save()
    return tag


def display_name():
    return dict(
        EN=fake.word(),
        PL=fake.word(),
        FI=fake.word()
    )


def children():
    return [
        {
            'guid': create_random_category_tag().guid,
            'name': fake.word(),
            'display_name': display_name(),
            'children': [
                {
                    'guid': create_random_category_tag().guid,
                    'name': fake.word(),
                    'display_name': display_name(),
                    'children': []
                } for _ in range(5)
            ]

        } for _ in range(5)
    ]


def random_bootstrap_data(**kwargs):
    return {
        'data': {
            'dashboard': {fake.word(): fake.sentence()},
            'info': fake.sentence(),
            'links': [fake.url() for _ in range(5)]
        },
        'tag_hierarchy': [
            {
                'guid': create_random_category_tag().guid,
                'name': fake.word(),
                'display_name': display_name(),
                'children': children()
            } for _ in range(5)
        ],
        **kwargs
    }
