# -*- coding: utf-8 -*-
import datetime
from random import choice, randint

from gwio.core.utils.testing.helpers import fake, image_url, n_times
from gwio.models.webshops import SupportedSocialMedia


def random_address_data():
    return dict(label=choice((None, fake.word())),
                name=fake.company(),
                street=fake.street_address(),
                city=fake.city(),
                zipcode=fake.postcode(),
                country='FI')


def random_phone_data():
    return dict(label=fake.word(), phone=fake.phone_number())


def random_email_data():
    return dict(label=fake.word(), email=fake.email())


def random_banner_data():
    return dict(
        title=choice((None, fake.word())),
        image=image_url(),
        link=choice((None, fake.url())),
        text=choice((None, fake.sentence(randint(1, 8)))),
        type=choice(['web', 'mobile']))


def random_social_media_data(**kwargs):
    return {
        **dict(label=choice([x for x in SupportedSocialMedia])),
        **choice((dict(link=fake.url(), analytics=None),
                  dict(link=None, analytics=fake.ssn()),
                  dict(link=fake.url(), analytics=fake.ssn()))),
        **kwargs}


def random_webshop_data(**kwargs):
    """Generate a random webshop"""
    name = fake.name()
    ret = dict(
        name=name,
        logo=fake.url(),
        enabled=fake.boolean(),
        addresses=n_times(random_address_data, randint(1, 3)),
        phones=n_times(random_phone_data, randint(1, 2)),
        alert=dict(
            text=fake.sentence(randint(1, 8)),
            link=choice((None, fake.url())),
            color=fake.safe_hex_color()),
        banners=dict(main=n_times(random_banner_data, randint(1, 3)),
                     side=n_times(random_banner_data, randint(1, 3)),
                     top=n_times(random_banner_data, randint(1, 3))),
        brand_color=fake.safe_hex_color(),
        emails=n_times(random_email_data, randint(1, 3)),
        timezone=fake.timezone(),
        opening_hours_normal=[random_opening_hours_normal_data(day=x) for x in range(0, 7)],
        opening_hours_exceptions=[random_opening_hours_exception_data()],
        payment_method_guids=list(),
        person_in_charge=fake.name(),
        social_media=n_times(random_social_media_data, randint(1, 3)),
        static_pages=[random_static_pages_data()],
        public_data=dict(stripe=None, domain=fake.domain_name()),
        _private_data=dict(stripe=dict(commission_percentage=9),
                           payu=dict(commission_percentage=9),
                           email=fake.email(),
                           paytrail=None,
                           xpress_couriers=dict(client_id=fake.md5())),
        business_id=fake.company_vat(),
        websites=n_times(fake.url, randint(1, 3)),
        description=fake.sentence(),
        shorthand_name=fake.pystr(min_chars=1, max_chars=10),
    )
    ret.update(**kwargs)
    return ret


def random_opening_hours_normal_data(**kwargs):
    start_hour = randint(0, 23)
    return {**dict(day=randint(0, 6),
                   start_at=datetime.time(start_hour, randint(0, 0 if start_hour == 23 else 59)),
                   end_at=datetime.time(start_hour)),
            **kwargs}


def random_opening_hours_exception_data(**kwargs):
    start_hour = randint(0, 23)
    params = dict(date=(datetime.date.today() + datetime.timedelta(days=randint(0, 6))),
                  start_at=datetime.time(start_hour, randint(0, 0 if start_hour == 23 else 59)),
                  desc=fake.sentence(),
                  end_at=datetime.time(randint(start_hour, 23), randint(0, 59)))
    if choice([True, False]):
        params['desc'] = fake.word()
    return {**params,
            **kwargs}


def random_static_pages_data(**kwargs):
    return dict(
        title=fake.sentence(),
        link=fake.uri(),
        inline=fake.boolean(),
        **kwargs)
