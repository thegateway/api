# -*- coding: utf-8 -*-
"""
Fake data generators used for pricing fixtures and testing
"""
from decimal import Decimal
from random import choice, uniform

from gwio.core.utils.testing.helpers import fake
from gwio.ext import SYSTEM_GUID
from gwio.models.pricing import PricingOperation


def random_pricing_method():
    return dict(op=choice(list(PricingOperation)), value=round(Decimal(uniform(0.1, 100.0)), 2))


def random_pricing():
    return dict(
        name=fake.sentence(2),
        owner=SYSTEM_GUID,
        methods=dict(
            cost=random_pricing_method(),
            retail=random_pricing_method()))
