import random

from gwio.core.utils.testing.helpers import fake
from .modifiers import random_modifier_data


def random_coupon_data(**kwargs):
    return {
        **dict(
            benefit=random_modifier_data(**(kwargs.pop('benefit') if 'benefit' in kwargs else dict())),
        ),
        **kwargs,
    }


def random_promotion_condition_data(**kwargs):
    return {
        **dict(
        ),
        **kwargs
    }


def random_promotion_data(**kwargs):
    return {
        **dict(
            name=fake.sentence(),
            images=[fake.image_url() for _ in range(random.randint(3, 7))],
            benefit=random_modifier_data(),
            conditions=[random_promotion_condition_data() for _ in range(random.randint(1, 3))],
        ),
        **kwargs,
    }
