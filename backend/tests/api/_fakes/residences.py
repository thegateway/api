import datetime
import random

from gwio.core.utils.testing.helpers import fake
from gwio.models.residence import HouseType, OwnershipType


def random_residence_data(**kwargs):
    return {'ownership': random.choice(list(OwnershipType)),
            'name': fake.street_name(),
            'year_built': random.randint(1156, datetime.date.today().year),
            'house_type': random.choice(list(HouseType)),
            'address': dict(street_address=fake.street_address(),
                            postal_code=fake.postcode()),
            'files': [dict(name=fake.file_name(),
                           url=fake.url()) for _ in range(random.randint(0, 5))],
            'data': fake.pydict(4, True, 'str', 'int'),
            'archived': datetime.datetime.now(tz=datetime.timezone.utc) if random.randint(1, 5) == 1 else None,
            **kwargs}
