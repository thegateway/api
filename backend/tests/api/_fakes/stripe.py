import copy
import hashlib
import hmac
import json
import random
import uuid
from datetime import datetime
from typing import Union

from gwio.core.utils.testing.helpers import fake

# Modified from actual webhook request
STRIPE_WEBHOOK_SUCCESS = {
    'id': None,
    'object': 'event',
    'api_version': '2018-09-06',
    'created': 1563277975,
    'data': {
        'object': {
            'id': None,
            'object': 'payment_intent',
            'allowed_source_types': [
                'card'
            ],
            'amount': 1260,
            'amount_capturable': 0,
            'amount_received': 0,
            'application': None,
            'application_fee_amount': None,
            'canceled_at': None,
            'cancellation_reason': None,
            'capture_method': 'automatic',
            'charges': {
                'object': 'list',
                'data': [],
                'has_more': False,
                'total_count': 0,
                'url': '/v1/charges?payment_intent={intent_id}'
            },
            'client_secret': None,
            'confirmation_method': 'automatic',
            'created': 1563277975,
            'currency': 'eur',
            'customer': None,
            'description': None,
            'invoice': None,
            'last_payment_error': None,
            'livemode': False,
            'metadata': {
                'user_identity_id': 'fake_identity',
                'order_guid': None
            },
            'next_action': None,
            'next_source_action': None,
            'on_behalf_of': None,
            'payment_method': None,
            'payment_method_types': [
                'card'
            ],
            'receipt_email': None,
            'review': None,
            'setup_future_usage': None,
            'shipping': None,
            'source': None,
            'statement_descriptor': 'kauppa: qwer1234',
            'status': 'requires_source',
            'transfer_data': None,
            'transfer_group': None
        }
    },
    'livemode': False,
    'pending_webhooks': 1,
    'request': {
        'id': None,
        'idempotency_key': 'fd558dfc-fecb-4785-a616-9f60e82b9e34'
    },
    'type': None
}


def generate_stripe_webhook_payload():
    def func(stripe_key: str,
             evt_id: Union[str, callable] = fake.pystr,
             intent_id: Union[str, callable] = fake.pystr,
             client_secret: Union[str, callable] = fake.pystr,
             order_guid: Union[uuid.UUID, callable] = uuid.uuid4,
             request_id: Union[str, callable] = fake.pystr,
             intent_type: str = None,
             amount: Union[int, callable] = lambda: random.randint(100, 10000),
             amount_capturable: [int, callable] = lambda: random.randint(100, 10000)):
        assert stripe_key is not None, 'Set STRIPE_ENDPOINT_SECRET environment variable'
        payload = copy.deepcopy(STRIPE_WEBHOOK_SUCCESS)
        payload['id'] = evt_id if not callable(evt_id) else evt_id()
        payload['data']['object']['id'] = intent_id if not callable(intent_id) else intent_id()
        payload['data']['object']['charges']['url'] = payload['data']['object']['charges']['url'].format(
            intent_id=intent_id if callable(intent_id) else intent_id())
        payload['data']['object']['amount'] = amount if not callable(amount) else amount()
        payload['data']['object']['amount_capturable'] = amount_capturable if not callable(amount_capturable) else amount_capturable()
        payload['data']['object']['client_secret'] = client_secret if not callable(client_secret) else client_secret()
        payload['data']['object']['metadata']['order_guid'] = str(order_guid) if not callable(order_guid) else str(order_guid())
        payload['request']['id'] = request_id if not callable(request_id) else request_id()
        payload['type'] = intent_type if intent_type is not None else 'payment_intent.amount_capturable_updated'
        timestamp = int(datetime.timestamp(datetime.now()))
        body = json.dumps(payload)
        signature = hmac.new(stripe_key.encode('utf-8'),
                             f'{timestamp}.{json.dumps(payload)}'.encode('utf-8'),
                             hashlib.sha256)
        return body, {'Stripe-Signature': f't={timestamp},v1={signature.hexdigest()}'}

    return func
