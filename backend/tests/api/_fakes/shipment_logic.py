import random
import uuid
from decimal import Decimal

from gwio.core.utils.testing.helpers import fake


def random_product_line(**kwargs):
    product_data = dict(
        guid=uuid.uuid4(),
        shop_guid=uuid.uuid4(),
        name=fake.name(),
        images=[fake.image_url() for _ in range(5)],
        qty=1,
        type='default',
        size=dict(
            height=random.randint(1, 50),
            length=random.randint(1, 50),
            width=random.randint(1, 50),
        ),
        weight=5,
        price=dict(
            original=dict(
                total=random.randint(50, 900),
                unit=random.randint(50, 900),
                modifiers=list(),
            ),
            final=dict(
                total=random.randint(50, 1000),
                unit=random.randint(50, 1000),
            ),
            currency='€',
            vat_class=random.randint(10, 30)
        )
    )
    product_data.update(kwargs)
    return product_data


def random_order(shop_count, mall_count, **kwargs):
    location_guid = uuid.uuid4()
    order_data = dict(
        description=fake.sentence(),
        guid=uuid.uuid4(),
        product_lines=[random_product_line() for _ in range(3)],
        shop_guids=[uuid.uuid4() for _ in range(shop_count)],
        summary=dict(
            currency=fake.currency_code(),
            price=fake.pyint(),
            vat0_price=random.choice([10, 14, 24]),
            vats=[dict(
                vat=random.choice([10, 14, 24]),
                amount=fake.pyint()
            ) for _ in range(2)],
            shops=[dict(
                currency='€',
                price=round(Decimal(random.uniform(10, 100.0)), 2),
                shop_guid=uuid.uuid4(),
                location_guid=location_guid if mall_count == 1 else uuid.uuid4(),
                vat0_price=round(Decimal(random.uniform(10, 90.0)), 2),
                vats=[dict(
                    amount=round(Decimal(random.uniform(0.1, 10.0)), 2),
                    vat=23
                )]
            ) for _ in range(shop_count)]
        ),
        shipping_address=dict(
            phone=fake.phone_number(),
            name=fake.company(),
            street_address=fake.street_address(),
            email=fake.email(),
            postal_code=fake.postcode(),
            country_code='FI'
        ),
    )
    order_data.update(kwargs)
    return order_data
