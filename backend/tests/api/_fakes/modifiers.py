import random

from gwio.core.utils.testing.helpers import fake
from gwio.models.modifiers import ModifierMethodType


def random_modifier_data(**kwargs):
    return {
        **dict(
            desc=fake.sentence(),
            method=random.choice(list(ModifierMethodType)),
            amount=fake.pydecimal(),
        ),
        **kwargs
    }
