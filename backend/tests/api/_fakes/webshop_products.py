

def random_webshop_product_data(**values):
    return {
        **values,
        **dict(
            # No defaults currently as all values are guids
        ),
    }
