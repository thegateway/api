# -*- coding: utf-8 -*-
from random import (
    choice,
    randint,
)

from gwio.core.utils.testing.helpers import (
    fake,
    image_url,
)
from gwio.models.payment_method import PaymentMethod


def random_payment_method_data(**kwargs):
    d = dict(name=fake.name(),
             logo=image_url(),
             description=fake.sentence(randint(1, 8)),
             data={},
             type=choice(list(PaymentMethod.API.Type)))
    d.update(kwargs)
    return d
