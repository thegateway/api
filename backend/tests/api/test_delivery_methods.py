# coding=utf-8
import datetime
import hashlib
import json
import random
import uuid
from decimal import Decimal
from random import (
    choice,
    randint,
    sample,
)

import pytest
import pytz
from marshmallow.fields import Field

from gwio.core.utils.payu.payu import PayuOrderStatus
from gwio.core.utils.testing.helpers import THE_ONE_CURRENCY
from gwio.environment import Env
from gwio.integrations.xpress_couriers import (
    Location,
    ServiceType,
)
from gwio.logic.deliveries.xpress_couriers import (
    get_available_delivery_methods,
)
from gwio.logic.orders.types import OrderState
from gwio.logic.packages.handler import PackageState
from gwio.models.orders import Order
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.webshop_product import WebshopProduct
from ._fakes.delivery_methods import random_delivery_method_json

DeliveryMethodSchema = Product.API.Assessor.EvaluatedProduct.Schemas.WebshopGet


@pytest.fixture(scope='class')
def test_suite(mock_aws, random_delivery_methods, webshop):
    assert mock_aws
    methods = list()
    for method in random_delivery_methods(5):
        method.save()
        methods.append(method)
    webshop.save()
    return methods


@pytest.mark.skipif(Env.VERTICAL == 'remppa', reason=f'Not applicable to "remppa"')
@pytest.mark.usefixtures('test_suite')
class TestAdminDeliveryMethods:

    def test_list_available_delivery_methods(self, as_shopkeeper, test_suite):
        assert test_suite
        r = as_shopkeeper.get('/delivery_methods/')
        assert 200 == r.status_code
        assert len([x for x in Product.scan() if x.type == PRODUCT_TYPE_DELIVERY_METHOD]) == len(r.json)
        schema = DeliveryMethodSchema()
        assert all(schema.load({k: v for k, v in x.items() if k != 'price'}) for x in r.json)

    def test_get_delivery_method(self, as_shopkeeper, test_suite):
        method = choice(test_suite)
        r = as_shopkeeper.get(f"/delivery_methods/{method.guid}")
        assert 200 == r.status_code

    def test_get_non_existing_product(self, as_shopkeeper):
        r = as_shopkeeper.get('/delivery_methods/{}'.format(str(uuid.uuid4())))
        assert 404 == r.status_code

    def test_add_delete_delivery_method(self, as_admin):
        data = random_delivery_method_json(
            base_price=dict(
                amount=Decimal('12.40'),
                vat_percent=24,
                currency='€',
            )
        )
        r = as_admin.post('/delivery_methods/', json=data)
        assert 201 == r.status_code
        r = as_admin.delete(f"/delivery_methods/{r.json['guid']}/")
        assert 204 == r.status_code

    @pytest.mark.parametrize('missing_field', [
        'name',
        'base_price.amount',
        'base_price.vat_percent',
        'base_price_type',
        'vat',
    ])
    def test_add_delivery_method_missing_field(self, as_admin, missing_field):
        data = random_delivery_method_json()

        try:
            base_key, missing_field = missing_field.split('.')
            data[base_key].pop(missing_field)
        except ValueError:
            data.pop(missing_field)
        r = as_admin.post('/delivery_methods/', json=data)
        assert 409 == r.status_code, r.json

        expected_error_message = f'{{"{missing_field}": ["{Field.default_error_messages["required"]}"]}}'
        try:
            expected_error_message = f'{{"{base_key}": {expected_error_message}}}'
        except UnboundLocalError:
            pass

        assert expected_error_message == r.json['message']

    def test_update_delivery_method(self, as_admin, test_suite, faker):
        data = dict(
            desc=faker.sentence(randint(1, 4)),
            base_price=dict(
                currency=str(THE_ONE_CURRENCY),
                amount=random.randint(100, 4000) / 100,
                vat_percent=random.choice([24, 14, 10])))
        method = choice(test_suite)
        r = as_admin.put(f'/delivery_methods/{method.guid}/', json=data)
        assert 200 == r.status_code
        method.refresh()
        assert data['desc'] == method.desc
        r = as_admin.get(f'/delivery_methods/{method.guid}/')
        assert 200 == r.status_code

        assert data['base_price']['currency'] == r.json['price']['base']['unit']['currency']
        assert data['base_price']['amount'] == r.json['price']['base']['unit']['amount']
        assert data['base_price']['vat_percent'] == r.json['price']['base']['unit']['vat_percent']

    def test_deactivate_delivery_method(self, as_admin):
        delivey_method = random.choice([x for x in Product.scan() if x.type == PRODUCT_TYPE_DELIVERY_METHOD])
        if not delivey_method.activated:
            delivey_method.activated = datetime.datetime.now(tz=pytz.UTC)
            delivey_method.save()

        r = as_admin.put(f'/delivery_methods/{delivey_method.guid}/', json=dict(
            activated=False,
        ))

        assert r.json['activated'] == None


@pytest.fixture(scope='class')
def webshop_test_suite(test_suite, webshop):
    webshop.save()
    methods = list()
    for method in sample(test_suite, randint(1, 3)):
        wsp = WebshopProduct(product_guid=method.guid, webshop_guid=webshop.guid)
        wsp.save()
        methods.append(wsp)
    return methods


@pytest.mark.usefixtures('mock_xpress_couriers')
class TestDeliveryMethods:
    def test_get_available_delivery_methods(self, faker, create_random_webshop, random_products, create_random_location):
        location = create_random_location(description=f'{faker.street_address()}, {faker.postcode()} {faker.city()}, PL')
        location.save()
        webshop = create_random_webshop(locations=[location.guid])
        webshop.save()
        products = [dict(shop_guid=p.owner_guid,
                         product_guid=p.guid,
                         qty=random.randint(1,3),
                         amount=Decimal(random.randint(10,1000))/100) for p in random_products(random.randint(2,5), owner_guid=webshop.guid)]
        get_available_delivery_methods(
            delivery_location=Location(
                name=faker.name(),
                address=faker.street_address(),
                city=faker.city(),
                post_code=faker.postcode(),
                country_code='PL'
            ),
            product_list=products,
            cash_on_delivery=True,
            service_type=ServiceType.LOCAL,
        )


@pytest.fixture(scope='class')
def xpress_couriers_delivery_methods(create_random_delivery_method):
    supported_delivery_methods = [6260, 81, 89]
    delivery_methods = []
    for service_id in supported_delivery_methods:
        dm = create_random_delivery_method(
            data=dict(
                xpress_couriers=dict(
                    service_id=service_id,
                    service_type=1,
                )))
        dm.save()
        delivery_methods.append(dm)
    yield delivery_methods
    for dm in delivery_methods:
        dm.delete()


@pytest.mark.usefixtures('mock_xpress_couriers')
class TestDeliveryMethodsApi:
    def test_get_available_delivery_methods(self, faker, create_random_webshop, random_products, create_random_location, as_anonymous,
                                            xpress_couriers_delivery_methods):
        location = create_random_location(description=f'{faker.street_address()}, {faker.postcode()} {faker.city()}, PL')
        location.save()
        webshop = create_random_webshop(locations=[location.guid])
        webshop.save()
        products = []
        for p in random_products(random.randint(2, 5), owner_guid=webshop.guid):
            p.save()
            products.append(dict(shop_guid=p.owner_guid,
                                 guid=p.guid,
                                 qty=random.randint(1,3)))
            WebshopProduct(webshop_guid=webshop.guid, product_guid=p.guid).save()
        payload=dict(
            delivery_address=dict(
                name=faker.name(),
                street_address=faker.street_address(),
                postal_code=faker.postcode(),
                country_code='PL'),
            products=products,
            cash_on_delivery=True,
            is_private_person=True,
            service_type=ServiceType.LOCAL.value,
        )
        res = as_anonymous.post('/delivery_methods/supported', json=payload)
        assert 200 == res.status_code, res.json
        assert res.json

    def test_create_xpc_order(self, faker, as_anonymous, as_shopkeeper, mock_payu, xpress_couriers_delivery_methods, webshop, random_products,
                              payment_method_payu, create_random_location):
        location = create_random_location(description=f'{faker.street_address()}, {faker.postcode()} {faker.city()}, PL')
        location.save()
        webshop.locations=[location.guid]
        webshop.save()

        # Create order
        products = []
        for product in random_products(3, owner_guid=webshop.guid):
            product.save()
            products.append(dict(product_guid=product.guid))
        res = as_shopkeeper.post(f'/dashboard/webshops/{webshop.guid}/products/', json=products)
        assert 200 == res.status_code

        delivery_method = random.choice(xpress_couriers_delivery_methods)
        customer_name = faker.name()
        res = as_anonymous.post(f'/customers/orders',
                                json=dict(customer_name=customer_name,
                                          payment_method=dict(guid=payment_method_payu.guid,
                                                              continue_url='http://localhost/test'),
                                          delivery_method=delivery_method.guid,
                                          customer=dict(name=customer_name,
                                                        phone=faker.phone_number()),
                                          shipping_address=dict(name=customer_name,
                                                                email=faker.email()),
                                          products=[dict(guid=p['product_guid'],
                                                         shop_guid=webshop.guid,
                                                         qty=random.randint(1, 3)) for p in products]))
        assert 201 == res.status_code, res.json
        order = Order.get(guid=uuid.UUID(res.json['guid']))
        assert order.current_state == OrderState.PROCESSING

        # Send webhook
        payload = json.dumps(dict(order=dict(extOrderId=str(order.guid),
                                             status=PayuOrderStatus.COMPLETED.name),
                                  localReceiptDateTime=datetime.datetime.now(tz=datetime.timezone.utc).isoformat(),
                                  properties=list()))
        signature_input = payload + Env.PAYU_SECOND_KEY
        signature = hashlib.md5(signature_input.encode('utf-8')).hexdigest()
        openpayu_header = ';'.join([f'{k}={v}' for k, v in dict(sender='checkout', signature=signature, algorithm='MD5', content='DOCUMENT').items()])
        res = as_anonymous.post('/payu/webhook', data=payload,
                                headers={'OpenPayu-Signature': openpayu_header,
                                         'Content-Type': 'application/json'})
        assert 200 == res.status_code, res.json
        order.refresh()
        assert order.packages[0].current_state is PackageState.WAITING_PACKING

        packages_route_root = f'/webshops/{webshop.guid}/orders/{order.guid}/packages/{order.packages[0].guid}'
        res = as_shopkeeper.put(f'{packages_route_root}/_pack')
        assert 200 == res.status_code
        order.refresh()
        assert order.packages[0].current_state is PackageState.WAITING_DELIVERY

        res = as_shopkeeper.put(f'{packages_route_root}/_confirm')
        assert 200 == res.status_code
        order.refresh()
        assert order.current_state == OrderState.COMPLETED
        assert order.packages[0].current_state is PackageState.DELIVERED
        order.delete()
