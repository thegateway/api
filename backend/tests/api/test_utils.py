# -*- coding: utf-8 -*-
"""
Test utility functions
"""
import os
import tempfile

import boto3
import pytest

from gwio.utils.email import EMail, get_jinja_env


def test_string_enum():
    from gwio.utils.strings import StringEnum

    class TestConstants(StringEnum):
        FOO = 'foo'
        BAR = 'bar'

    assert TestConstants.BAR < TestConstants.FOO
    assert TestConstants.BAR == 'bar'
    assert 'foo' == TestConstants.FOO
    assert not TestConstants.FOO == TestConstants.BAR


@pytest.fixture('class')
def template_fixture(mock_aws):
    bucket = 'unittest'

    conn = boto3.resource('s3', region_name=mock_aws['clients']['s3'].meta.region_name)
    conn.create_bucket(Bucket=bucket)

    tmpdir = tempfile.TemporaryDirectory()
    template_location = 'file:///{path}'.format(path=tmpdir.name)
    for key, body in [('test_template.html', '<html><body>Name: {{ name }}</body></html>'),
                      ('test_template.txt', 'Name: {{ name }}')]:
        mock_aws['clients']['s3'].put_object(Bucket=bucket, Key=key, Body=body)
        with open(os.path.join(tmpdir.name, key), 'w') as f:
            f.write(body)

    yield dict(template_location=template_location, s3_url=f's3://{bucket}/')

    if tmpdir is not None:
        tmpdir.cleanup()


class TestEmail():

    @pytest.fixture(scope='function')
    def mailer(self):
        yield EMail()

    def test_s3_scheme_loader(self, template_fixture, mailer):
        mailer._render(template='test_template', jinja_env=get_jinja_env(template_fixture['s3_url']))

    def test_email_render(self, template_fixture, mailer):
        rendered = 'Name: Panda'
        templates = mailer._render(
            template='test_template',
            context=dict(name='Panda'),
            jinja_env=get_jinja_env(template_fixture['template_location']))
        for template in templates.values():
            assert rendered in template

    def test_email_render_with_no_template(self, template_fixture, mailer):
        with pytest.raises(ValueError):
            mailer._render(
                template='template_that_does_not_exist',
                context=dict(name='Panda'),
                jinja_env=get_jinja_env(template_fixture['template_location']))
