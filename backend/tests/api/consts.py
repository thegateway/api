# -*- coding: utf-8 -*
from envs import env

DEMO_SHOP_ID = env('DEMO_SHOP_ID', '928b4c5249ab448bb75815472f7ec532')

STRIPE_ENDPOINT_SECRET = 'BOGUS'
VAPID_PRIVATE_KEY = ('MHcCAQEEIGGTcDPHqbELjgsXDT2SgwEltX5FIU5X7p90WwbRw2KLoAoGCCqGSM49AwEHoUQDQgAEYq'
                     'WZQMBY5OUJNIe2E9pgyO+2Xzn0zx/NYvrP+p25gRRZQcoI4N9T/zaGhmGuLrCojsO0FkWG/N1L+ZAP/pI1EA==')
COGNITO_POOL_ID = 'eu-west-1_XfY5hMxVY'  # Ci-testing
COGNITO_CLIENT_ID = '1mjodi20lvjcisg13gm41tc2ti'
SYSTEM_EMAIL = "devops@gwapi.eu"
PREDEFINED_JWKS = {'keys': [
    # api-test.mehilainen.fi
    {'e': 'AQAB',
     'kty': 'RSA',
     'n': ('sfyyKxGFCrwDX2vvrm7YSmsxrn17pLbeLheaowA-djORS63zL8zZzC8zpBNa_P0mv65COk9Og3ZZFik_SwmljzeZgqBFDIOPNwM'
           'Ha46pDgoasRikd1MwBTSGSGH5nMZHfFh5W-qT1l610PPSFvmyieBBzxbpqBPXAE7Po0Ik9TByhLz6Nrh9KN9OWR2LPW3hWcMdWK'
           '-ASGxXpdXmuOaSJkAOtv5f8cBU7erdTGdKqCtv3v3w6hc245rVsl_oBNF-jUjaYsm-ncW_Lc0eQfQWT9e6NUcR1BmuW1ZQYYil3'
           'inNjU-EWxzsaJEY1J0VBz9lj3HEWsc9eOAQCnx04FQ3QMnp6PTlvBgzwUaN2FR7MRPc7B4Z7r4eYsBk4SCZbUKSe5hEC055askK'
           'VBxcK-KOoSErQJ5E6xH-e876DKiEf_S3ARETitk4jTLRlIpDEiWYwyLAghZ-C3Oe05jHsN46QZP-V85f8dLlnsV3XY4DGXGSqdS'
           'xGv8AgUcAnQ7ELOrqT6NSCRIkj_U-l0ciDsIewNeenOsstcprM628p9O8WzfqFDL3G30w2yZ0Crvc9drVzQoIxovlFy8p44n9RS'
           '1_wvOhff-M046TdUHzApu-9Iwd8K8RDeRjVJgwEYb9unmoaFVMzXJ2c7xoKSumFi2q0G61I2bfQjp8YVDGLzUItg0')
     },
    # Cognito pool eu-west-1_3z6FTIWBk
    {'alg': 'RS256',
     'e': 'AQAB',
     'kid': 'ZHnx604rL22rwNDT3uoKBj2YmwP/mvCWzu5klXvavWc=',
     'kty': 'RSA',
     'n': 'zN_GiKi4-_1iPEp_zVTplr4xThgjfnF5FK4tAa0mXogEYeVf-2kDuuvvrjkAdrsiX8WK7ej9SqlDfdzS3ki6yyen66zyFKc_Oni'
          '49Xph3D59o4N34ThGkQrLNIFk6GCVmp510ujiNtsMy55Jg3ImtJP0b0imAe4A0MdXTN-LXsASejuCZNXGD4G0zz4llzc11Xp90x'
          'IYJ9gnOJqmw6pGr36dcK6YClFkvHcbGY6_huLJ3I4QL5Ws6qUGtT02AuCcZVoR4KC1MQ_T0QauG53z9WzQ15vM160Ojo44CfY14'
          'p3HUnLJFQS7J2nPPeWQAIFYh5n4vTdme7G1wOHKLy-GUQ',
     'use': 'sig'},
    {'alg': 'RS256',
     'e': 'AQAB',
     'kid': 'bkCmRcsSKMfj0y5iIs0JmeMap5gO2dfVN43+Vuax57E=',
     'kty': 'RSA',
     'n': 'j0GE51_X4Vdhe86AmJ7Az5RisTLHyovPZKYFetc-P0D-yyKTjQ1mZwtAZHDDZcUWSrLxANWIk-wevBZluxPyJEXPGYQWWQcmxQN'
          '7ZmNNGCs8HuChwoqn_qLBlH-ZZnTEA1sF7elVo6eVUtM471eYQMOnwiZm073ekHw9V6Ac5akAfYaS-HtGEoUtDlIfFufqss174B'
          'hjLqrTtwxGCdPPBzSFseIzXduRxTobBSa0EWHp02u1H4m_9sBqnOtg_8I3R2EFaW4S_HzmiuYAt8tyOQqWMS4jE6_QJ_nwwNHJJ'
          'vfDFnVdDY1UAdgpt_YLC1Q1N-4hdra23xH77YY-fq7EzQ',
     'use': 'sig'},
    # Cognito pool eu-west-1_vyUmeJKmU
    {
        "alg": "RS256",
        "e": "AQAB",
        "kid": "TlK9BKopnnzWJk+YQYS4XXN+k/auB5lqR+EN5n+Y7DU=",
        "kty": "RSA",
        "n": "5bicinNipw7fq9Lp6D__XrhpBgpJbSGM8xuv60UP4EX49xFvYsf741B-hjKMRNK68zLnr65879Cu-Q_pFijU-gBcz1SVeX4JqDchvaVPFGX_kqlvPGtG9fMC3zr3SYC_Fr_WJKVZEkTPQlclJhdVNbTuYl4XYFYkpTjnrIFCxyw9ZeLMsqPTY4meVO-lEBMgxhoQ3RdB3ukmxCN_ehWmDAEL_VQT9wEgcWRh_BgpyzZhuQwq8U-ARICpN-kOq45jRvAS1rPf4nba1pY2Cb5BxNjZ0PvGErn88iGV1XnEap42c-i1V6QL6FBc97U_SHsIYXvo4JainEHuUszq-wMIsQ",
        "use": "sig"
    }, {
        "alg": "RS256",
        "e": "AQAB",
        "kid": "7yJBCHMG+Vj4I14zhU4ap77kCwY9Xr0xXznlKHZHpDU=",
        "kty": "RSA",
        "n": "mYhiceQXt4A81PzLebylaabAIfRcWX9An1fIa35ffM-AciFP4g5o_1upXWRGIxTbcfqyYgX3kLVrWal_8qxyUZlhLVjsSdaENWqdQcgqS5Sqs_wOYbHauoIUf24CffzmY7sVnVco4Ft1bN75xdDuz_8uieOpy9ikhyDWEETzvv21TmFD84W4l3AQKm6fOvmo14ftjx0cao9DXT5YJ-p5uqPngXRdhfBsUxJUsGUk97HONRasqRndr-S3Mm0uHtfOSA8-ChjeB1qnJf2DIyE7JGJdghq5GQ5IB312z9TcfvR6WSrCwhBGQmn3g-C3utrYla_7CoWQrGDrTGwCq_Oxww",
        "use": "sig"
    }
]}
