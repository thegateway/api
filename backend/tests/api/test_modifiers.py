from decimal import Decimal

import pytest

from gwio.utils.notify import _all_subclasses
from gwio.ext.vat import (
    Vat,
    VatPrice,
)
from gwio.models.modifiers import (
    Modifier,
    ModifierMethodType,
    _method_conversions,
)
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DEFAULT
from ._fakes.modifiers import random_modifier_data


@pytest.fixture(scope='class')
def test_suite(create_random_product, faker):
    product = create_random_product(
        base_price=VatPrice.from_price_and_vat("160.0 EUR", vat=Vat(0)),  # 200 with VAT
        vat=Vat(25),
        type=PRODUCT_TYPE_DEFAULT,
    )
    return dict(
        product=product,
        modifiers=[
            Modifier(
                amount=Decimal(-50),
                method=ModifierMethodType.PERCENTAGE,
                desc=faker.sentence(),
            ),
            Modifier(
                amount=Decimal(-50),
                method=ModifierMethodType.FIXED_UNIT,
                desc=faker.sentence(),
            ),
            Modifier(
                amount=Decimal(-50),
                method=ModifierMethodType.FIXED_TOTAL,
                desc=faker.sentence(),
            ),
            Modifier(
                amount=Decimal(50),
                method=ModifierMethodType.OVERRIDE_UNIT,
                desc=faker.sentence(),
            ),
            Modifier(
                amount=Decimal(50),
                method=ModifierMethodType.OVERRIDE_TOTAL,
                desc=faker.sentence(),
            )
        ],
        expected_units={
            ModifierMethodType.PERCENTAGE: Decimal(-100),
            ModifierMethodType.FIXED_UNIT: Decimal(-50),
            ModifierMethodType.FIXED_TOTAL: Decimal(-25),
            ModifierMethodType.OVERRIDE_UNIT: Decimal(-150),
            ModifierMethodType.OVERRIDE_TOTAL: Decimal(-175),
        },
        expected_totals={
            ModifierMethodType.PERCENTAGE: Decimal(-200),
            ModifierMethodType.FIXED_UNIT: Decimal(-100),
            ModifierMethodType.FIXED_TOTAL: Decimal(-50),
            ModifierMethodType.OVERRIDE_UNIT: Decimal(-300),
            ModifierMethodType.OVERRIDE_TOTAL: Decimal(-350),
        },
    )


@pytest.mark.usefixtures('mock_aws')
class TestModifierMap:
    @pytest.mark.parametrize('method', list(ModifierMethodType))
    def test_calc_fixed_modifiers(self, test_suite, method):
        modifier = next((x for x in test_suite['modifiers'] if x.method == method))
        fixed_modifiers = modifier.calc_fixed_modifiers(Product.API.Assessor.evaluate(test_suite['product'], qty=Decimal(2)))

        assert test_suite['expected_units'][method] == fixed_modifiers.unit.amount
        assert test_suite['expected_totals'][method] == fixed_modifiers.total.amount

    @pytest.mark.parametrize('set_method, instance_method', [(x, _method_conversions[x] if x in _method_conversions else x) for x in list(ModifierMethodType)])
    @pytest.mark.parametrize('map_cls', [Modifier] + list(_all_subclasses(Modifier)))
    def test_method_conversions(self, faker, test_suite, set_method, instance_method, map_cls):
        orig_amount = Decimal(-50)
        instance = map_cls(amount=orig_amount, method=set_method, desc=faker.sentence())
        instance.calc_fixed_modifiers(Product.API.Assessor.evaluate(test_suite['product'], qty=Decimal(2)))

        assert instance_method == instance.method
        if set_method in _method_conversions:
            assert orig_amount != instance.amount
        else:
            assert orig_amount == instance.amount

        try:
            assert getattr(instance, 'fixed_modifiers')
        except AttributeError:
            pass

    def test_fixed_modifiers_invalid_method(self, faker):
        modifier = Modifier(**random_modifier_data())
        modifier.method = faker.word()
        with pytest.raises(KeyError, match=f"'{modifier.method}'"):
            _ = modifier.calc_fixed_modifiers(None)
