import copy
from datetime import datetime
from decimal import Decimal
from time import sleep

import pytest
import pytz

from gwio.utils.uuid import get_random_uuid
from gwio.ext import vat
from gwio.ext.vat import Currency
from gwio.logic.orders.types import (
    OrderAction,
    OrderAssessment,
    OrderState,
)
from gwio.logic.payments.handler import PaymentState
from gwio.models.orders import (
    Flags,
    Order,
)
from gwio.models.products.base import (
    PRODUCT_TYPE_DEFAULT,
    PRODUCT_TYPE_DELIVERY_METHOD,
)
from ._mocks import mock_order_fsm_triggered_logic


@pytest.fixture(scope='class')
def test_product(create_random_product):
    product = create_random_product()
    product.save()
    yield product
    product.delete()


@pytest.mark.usefixtures('mock_aws')
class TestOrderModel():
    @pytest.mark.parametrize('summary, payments, expected_result', [
        (dict(), [], True),
        (dict(price=Decimal('9.99')), [], False),
        (dict(price=Decimal('9.99')), [dict(_current_state=PaymentState.CHARGED, amount=Decimal('9.99'))], False),
        (dict(price=Decimal('9.99')), [dict(_current_state=PaymentState.COMPLETED, amount=Decimal('9.99'))], True),
        (dict(price=Decimal('9.99')), [dict(_current_state=PaymentState.COMPLETED, amount=Decimal('9.98'))], False),
        (dict(price=Decimal('9.99')), [dict(_current_state=PaymentState.COMPLETED, amount=Decimal('10'))], True),
        (dict(price=Decimal('9.99')), [dict(_current_state=PaymentState.COMPLETED, amount=Decimal('5')),
                                       dict(_current_state=PaymentState.COMPLETED, amount=Decimal('4.99'))], True),

    ])
    def test_status_paid(self, summary, payments, expected_result):
        order = Order(summary=summary, payments=payments)
        assert expected_result == order.status_paid

    @pytest.mark.parametrize('data, expected_result', [
        (dict(), True),
        (dict(flags=dict(prepayment=True)), True),
        (dict(flags=dict(prepayment=False)), False),
    ])
    def test_status_prepayment(self, data, expected_result):
        order = Order(**data)
        assert expected_result == order.status_prepayment

    @pytest.mark.parametrize('data, mock_flags, expected_result', [
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name', phone='0'),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)]),
         dict(require_phone=True),
         False),
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name'),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)]),
         dict(require_phone=False),
         False),
        # phone number required, but missing
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name'),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)]),
         dict(require_phone=True),
         True),
        # delivery_method missing
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name'),
              product_lines=[dict(type='a')]),
         dict(require_phone=False),
         True),
        # payment_method missing
        (dict(customer=dict(name='Name'),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)]),
         dict(require_phone=False),
         True),
        # buyer name missing
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)]),
         dict(require_phone=False),
         True),
        # no products
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name'),
              product_lines=[dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)]),
         dict(require_phone=False),
         True),
        # no pin set with `require_strong_authentication` = True
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name'),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)],
              flags=dict(require_strong_authentication=True)),
         dict(require_phone=False),
         True),
        # pin set with `require_strong_authentication` = True
        (dict(payment_method_guid=get_random_uuid(),
              customer=dict(name='Name', pin='1234'),
              product_lines=[dict(type='a'), dict(guid=get_random_uuid(), type=PRODUCT_TYPE_DELIVERY_METHOD)],
              flags=dict(require_strong_authentication=True)),
         dict(require_phone=False),
         False),
    ])
    def test_status_invalid(self, mocker, data, mock_flags, expected_result):
        for key, value in mock_flags.items():
            mocker.patch.object(Flags, key, value)

        data['_current_state'] = OrderState.CREATED
        pls = data.pop('product_lines')
        order = Order(**data)
        for pl in pls:
            pl['price'] = dict(
                currency=Currency.EUR,
                vat_class=Decimal('10'),
                final=dict(
                    unit= vat.VatPrice.from_price_and_vat("1 PLN", vat=2),
                    total=vat.VatPrice.from_price_and_vat("1 PLN", vat=2),
                ),
                original=dict(
                    unit=vat.VatPrice.from_price_and_vat("1 PLN", vat=2),
                    total=vat.VatPrice.from_price_and_vat("1 PLN", vat=2),
                ),
            )
            order.product_lines.append(order.Schema._declared_fields['product_lines']._model(**pl))
        assert expected_result == order.status_invalid

    OrdA = OrderAction

    @pytest.mark.parametrize('property_name, actions, mock_statuses', [
        ('ordered', [OrdA.CREATE, OrdA.PLACE], []),
    ])
    def test_timestamp_properties(self, mocker, property_name, actions, mock_statuses):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=False)

        for status, value in mock_statuses:
            mocker.patch.object(Order, status, value)

        order = Order()
        for action in actions:
            assert not getattr(order.timestamps, property_name)
            start_time = datetime.now(tz=pytz.UTC)
            sleep(0.005)
            order.process(action)
        timestamp = getattr(order.timestamps, property_name)
        # Depending how fast the test is being run, it can fail here if the time hasn't elapsed long enough before asserting.
        # thus adding sleep 5 milliseconds.
        assert start_time < timestamp
        sleep(0.005)
        assert datetime.now(tz=pytz.UTC) > timestamp

    @pytest.mark.parametrize('product_types, expected_result', [
        ([PRODUCT_TYPE_DEFAULT], False),
        (['prescription_medicine'], True),
        ([PRODUCT_TYPE_DEFAULT, 'prescription_medicine', PRODUCT_TYPE_DEFAULT], True),
    ])
    def test_require_strong_authentication(self, create_test_order, product_types, expected_result, test_product):
        order = create_test_order()
        orig_line = order.product_lines[0]
        order.product_lines = list()
        for product_type in product_types:
            line = copy.deepcopy(orig_line)
            line.guid = test_product.guid
            line.type = product_type
            order.product_lines.append(line)
        order.API.assess(model=order, action=OrderAction.CREATE, assessment=OrderAssessment.PRICES)
        assert expected_result == order.flags.require_strong_authentication
