import uuid

import pytest

from gwio import environment


@pytest.mark.parametrize('post_data', [None, dict(message='Is broke!')])
@pytest.mark.parametrize('app_env', ['dev', 'production'])
def test_report_issue(as_customer, app_env, mocker, post_data):
    mocker.patch.object(environment.Env, 'APPLICATION_ENVIRONMENT', app_env)
    r = as_customer.post(f'/issues/request/{uuid.uuid4()}', json=post_data)
    assert r.status_code < 300, r.json

    if app_env == 'production':
        assert r.json is None
    else:
        assert isinstance(r.json, list)
