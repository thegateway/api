# -*- encoding: utf-8 -*-
import json
import random
import re
import uuid
from datetime import (
    datetime,
    timedelta,
    timezone,
)
from decimal import Decimal

import pytest
from marshmallow_enum import EnumField

from gwio.core.utils.testing.helpers import (
    fake,
)
from gwio.environment import Env
from gwio.ext import SYSTEM_GUID
from gwio.models.modifiers import ModifierMethodType
from gwio.models.products import Product
from gwio.models.tags import (
    Tag,
    TagType,
)
from gwio.utils.uuid import get_random_uuid
from ._fakes.tags import random_tag_data
from ._fixtures.products import products_suite

assert products_suite


def random_tag_name():
    return '-'.join(fake.words(2))


@pytest.fixture(scope='class')
def saved_products(random_products, mock_aws):
    assert mock_aws
    products = list(type for x in Product.API.searchable_types for type in random_products(4, type=x))
    products_to_add = list()
    for product in products:
        if product.type in Product.API.valid_campaign_types:
            products_to_add.append(product.guid)
        product.save()
    yield dict(all=products, to_add=products_to_add)
    for product in products:
        product.delete()


@pytest.mark.usefixtures('mock_aws')
class TestTags:
    def test_empty_create_failing(self, as_admin):
        tag = Tag()
        with pytest.raises(AssertionError):
            tag.save()

    def test_create(self, as_admin):
        tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
        tag.save()
        assert SYSTEM_GUID == tag.owner

    def test_create_same_name_different_owner(self, as_admin):
        tag_name = random_tag_name()
        tag1 = Tag(name=tag_name, owner=uuid.uuid4(), type=Tag.Type.CATEGORY)
        tag2 = Tag(name=tag_name, owner=uuid.uuid4(), type=Tag.Type.CATEGORY)
        tag3 = Tag(name=tag_name, type=Tag.Type.CATEGORY)
        assert tag1.guid != tag2.guid
        assert tag2.guid != tag3.guid

    def test_get_tag_by_guid(self, as_admin):
        tag1 = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
        tag1.save()
        tag2 = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
        tag2.save()
        assert tag1.guid != tag2.guid
        tag = Tag.by_guid(tag1.guid)
        assert tag1.guid == tag.guid
        tag = Tag.by_guid(tag2.guid)
        assert tag2.guid == tag.guid

    def test_types(self):
        start = datetime.now(tz=timezone.utc)
        end = datetime.now(tz=timezone.utc) + timedelta(hours=2)
        first_tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
        first_tag.save()
        second_tag = Tag(
            name=random_tag_name(),
            type=Tag.Type.CAMPAIGN,
            duration=dict(start=start, end=end),
        )
        second_tag.save()

        tag = Tag.by_guid(first_tag.guid)
        assert Tag.Type.CATEGORY == tag.type

        tag = Tag.by_guid(second_tag.guid)
        assert Tag.Type.CAMPAIGN == tag.type

    def test_duration(self):
        start = datetime.now(tz=timezone.utc)
        end = datetime.now(tz=timezone.utc) + timedelta(hours=2)
        tag = Tag(
            name=random_tag_name(),
            type=Tag.Type.CATEGORY,
            duration=dict(start=start, end=end),
        )
        tag.save()

        loaded_tag = Tag.by_guid(tag.guid)
        assert start == loaded_tag.duration.start
        assert end == loaded_tag.duration.end

    def test_data(self, faker):
        tag = Tag(
            name=random_tag_name(),
            type=Tag.Type.CATEGORY,
            data=dict(desc=faker.name(), method=ModifierMethodType.PERCENTAGE, amount=Decimal('0.8')),
        )
        tag.save()

        loaded_tag = Tag.by_guid(tag.guid)
        assert ModifierMethodType.PERCENTAGE == loaded_tag.data.method
        assert Decimal('0.8') == loaded_tag.data.amount


@pytest.fixture(scope='class')
def tags_suite(mock_aws):
    assert mock_aws
    tags = list()
    # let's create few tags
    for i in range(1, 200):
        tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY if i % 2 == 0 else Tag.Type.CAMPAIGN)
        tag.save()
        tags.append(tag)
    return tags


@pytest.fixture(scope='class')
def untagged_product(random_product):
    random_product.save()
    return random_product


@pytest.fixture(scope='class')
def product_tags_suite(tags_suite, products_suite):
    products = random.sample(products_suite, random.randint(1, len(products_suite)))
    n_tags = len(tags_suite)
    product_tags = list()
    for product in products:
        randnum = random.randint(1, n_tags)
        for tag in random.sample(tags_suite, randnum):
            tag.add_product(product)
            tag.save()
            product_tags.append(tag)
            assert tag.product_guids
        product.save()

    return product_tags


@pytest.fixture(scope='class')
def test_product_tags_suite(mock_aws, tags_suite, products_suite, product_tags_suite, untagged_product):
    assert mock_aws
    assert tags_suite
    assert products_suite
    assert product_tags_suite
    assert untagged_product
    return


@pytest.mark.usefixtures('test_product_tags_suite')
class TestProductTags:
    def test_no_products_in_tag(self, as_admin):
        tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
        tag.save()
        assert not tag.product_guids
        tag.delete()

    def test_add_and_delete_product_in_tag(self, as_admin, untagged_product):
        tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY)
        tag.add_product(untagged_product)
        tag.save()
        assert untagged_product.guid in tag.product_guids
        tags = list(untagged_product.tags)
        assert 1 == len(tags)
        assert tag.guid == tags[0].guid
        assert tag.name == tags[0].name
        assert 1 == tag.num_items
        assert 1 == tag.Schemas.Get().dump(tag)['item_count']
        tag.remove_product(untagged_product)
        tag.save()
        assert untagged_product.guid not in tag.product_guids
        assert 0 == tag.num_items
        assert 0 == tag.Schemas.Get().dump(tag)['item_count']

    def test_part_of_campaign(self, products_suite):
        tag = Tag(
            name=random_tag_name(),
            type=Tag.Type.CAMPAIGN,
            duration=dict(
                start=datetime.now(timezone.utc),
                end=(datetime.now(timezone.utc) + timedelta(hours=2)),
            ),
            data=dict(desc='nop', method='percentage', amount='1.0'),
        )
        tag.save()
        product = random.choice(products_suite)
        tag.add_product(product)
        assert product.guid in tag.product_guids
        assert tag.guid in [tag.guid for tag in product.campaigns()]
        tag.remove_product(product)
        assert tag.guid not in product.campaigns()


@pytest.fixture(scope='function')
def filled_product_tag(product_tags_suite, random_products):
    products = random_products(10)
    product_tag = random.choice(product_tags_suite)
    for product in products:
        product.add_tag(product_tag)
        product.save()
    product_tag.save()

    yield product_tag

    for product in products:
        product.delete()


@pytest.fixture(scope='class')
def tags_test_suite(mock_aws, tags_suite):
    assert mock_aws
    assert tags_suite
    return tags_suite


@pytest.fixture(scope='class')
def webshop_tags_suite(mock_aws, webshop_guid):
    assert mock_aws
    tags = list()
    # let's create few tags
    for i in range(5, 20):
        tag = Tag(name=random_tag_name(), type=Tag.Type.CATEGORY if i % 2 == 0 else Tag.Type.CAMPAIGN, owner=webshop_guid)
        tag.save()
        tags.append(tag)
    return tags


@pytest.mark.usefixtures('tags_test_suite')
@pytest.mark.usefixtures('mock_aws')
class TestTagsAPI:
    webshop_path = '/webshops'

    def test_get_top_level_tags(self, as_anonymous, tags_suite):
        assert tags_suite
        r = as_anonymous.get('/tags/')
        assert 200 == r.status_code
        tags = Tag.OwnerTypeIndex.query(owner=str(SYSTEM_GUID))
        top_level_tags = list(tags)
        assert top_level_tags
        assert len(top_level_tags) == len(r.json)

    def test_get_top_level_tags_filtered(self, as_admin, as_anonymous, tags_suite, faker):
        duration = {
            'start': datetime.now(timezone.utc).isoformat(),
            'end': (datetime.now(timezone.utc) + timedelta(hours=2)).isoformat()
        }
        data = dict(desc=faker.name(), method='percentage', amount=Decimal('0.8'))
        type = Tag.Type.CAMPAIGN.name.lower()
        r = as_admin.post(f'/tags/', json=dict(name=random_tag_name(), type=type, data=data, duration=duration))
        assert 201 == r.status_code, r.json['message']
        r = as_admin.post(f'/tags/', json=dict(name=random_tag_name(), type=type, data=data, duration=duration))
        assert 201 == r.status_code, r.json['message']

        tag_type = Tag.Type.CATEGORY
        r = as_anonymous.get(f'/tags/?type={tag_type.value}')
        assert 200 == r.status_code, r.json['message']
        all_tags = list(Tag.scan())
        assert len([x for x in all_tags if x.type == tag_type]) == len(r.json)
        assert 0 == len(list(filter(lambda tag: tag['type'] != tag_type.value, r.json)))

        tag_type = Tag.Type.CAMPAIGN
        r = as_anonymous.get(f'/tags/?type={tag_type.value}')
        assert 200 == r.status_code, r.json['message']
        assert len([x for x in all_tags if x.type == tag_type]) == len(r.json)
        assert 0 == len(list(filter(lambda tag: tag['type'] != tag_type.value, r.json)))

    def test_list_tags_invalid_filter(self, as_anonymous, faker):
        invalid_type = faker.word()
        r = as_anonymous.get(f'/tags/?type={invalid_type}')
        assert 404 == r.status_code
        assert EnumField  # Pycharm doesn't understand it's being used on the next line
        assert re.search(f'type": \["{EnumField.default_error_messages["by_value"].format(input=invalid_type)}', r.json['message'])

    def test_get_tag(self, as_anonymous, tags_test_suite):
        tag = random.choice(tags_test_suite)
        tag_id = str(tag.guid)
        r = as_anonymous.get(f'/tags/{tag_id}/')
        assert 200 == r.status_code

    def test_try_create_existing_tag(self, as_admin, webshop_guid):
        post_data = Tag.Schemas.Post().dump(random_tag_data())
        r = as_admin.post(f'{self.webshop_path}/{webshop_guid}/tags', json=post_data)
        tag_guid = r.json['guid']
        assert 201 == r.status_code
        r = as_admin.post(f'{self.webshop_path}/{webshop_guid}/tags', json=post_data)
        assert 409 == r.status_code
        assert 'exists' in r.json['message']
        r = as_admin.delete(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 204 == r.status_code

    def test_create_delete_toplevel_tag(self, as_admin, webshop_guid):
        r = as_admin.post(f'/tags', json=Tag.Schemas.Post().dump(random_tag_data()))
        assert 201 == r.status_code
        tag_guid = r.json['guid']
        assert '00000000-0000-0000-0000-000000000000' == r.json['owner']
        r = as_admin.get(f'/tags/{tag_guid}/')
        assert 200 == r.status_code
        assert '00000000-0000-0000-0000-000000000000' == r.json['owner']
        r = as_admin.delete(f'/tags/{tag_guid}')
        assert 204 == r.status_code

    def test_create_delete_shop_level_tag(self, as_admin, webshop_guid):
        r = as_admin.post(f'{self.webshop_path}/{webshop_guid}/tags', json=Tag.Schemas.Post().dump(random_tag_data()))
        assert 201 == r.status_code
        tag_guid = r.json['guid']
        assert r.json['owner'] == str(webshop_guid)
        r = as_admin.get(f'/tags/{tag_guid}/')
        assert 200 == r.status_code
        assert str(webshop_guid) == r.json['owner']
        r = as_admin.get(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 200 == r.status_code
        assert str(webshop_guid) == r.json['owner']
        r = as_admin.delete(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 204 == r.status_code

    def test_create_get_delete_shop_owned_tag(self, as_shopkeeper, as_anonymous, webshop_guid):
        r = as_anonymous.get(f'{self.webshop_path}/{webshop_guid}/tags/')
        assert 200 == r.status_code
        start_tag_count = len(r.json)

        post_schema = Tag.Schemas.Post()
        r = as_shopkeeper.post(f'{self.webshop_path}/{webshop_guid}/tags', json=post_schema.dump(random_tag_data()))
        assert 201 == r.status_code
        r = as_shopkeeper.post(f'{self.webshop_path}/{webshop_guid}/tags', json=post_schema.dump(random_tag_data()))
        assert 201 == r.status_code
        tag_guid = r.json['guid']
        r = as_anonymous.get(f'{self.webshop_path}/{webshop_guid}/tags/')
        assert 200 == r.status_code
        assert len(r.json) == start_tag_count + 2
        assert str(webshop_guid) == r.json[0]['owner']
        r = as_shopkeeper.get(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 200 == r.status_code
        assert str(webshop_guid) == r.json['owner']
        r = as_shopkeeper.delete(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 204 == r.status_code

    def test_update_category_tag(self, as_admin, webshop_guid, faker):
        post_schema = Tag.Schemas.Post()
        random_data = random_tag_data(images=[dict(url=faker.url(), type=random.choice(list(Tag.ImageType)))])
        post_data = post_schema.dump(random_data)
        r = as_admin.post(f'{self.webshop_path}/{webshop_guid}/tags', json=post_data)
        assert 201 == r.status_code
        tag = Tag.by_guid(uuid.UUID(r.json['guid']))
        assert Tag.API.unify_name(post_data['name']) == r.json['name']
        ImageType = Tag.ImageType
        assert all(ImageType(x['type']) == ImageType(next(z for z in r.json['images'] if x['url'] == z['url'])['type']) for x in post_data['images'])
        random_data['images'].append(dict(url=faker.url(), type=random.choice(list(Tag.ImageType))))
        new_post_data = post_schema.dump(random_tag_data(images=random_data['images']))
        r = as_admin.put(f'{self.webshop_path}/{webshop_guid}/tags/{tag.guid}', json=new_post_data)
        assert 200 == r.status_code
        assert Tag.API.unify_name(new_post_data['name']) == r.json['name']
        tag.refresh()
        assert tag.name == r.json['name']
        assert all(ImageType(x['type']) == ImageType(next(z for z in r.json['images'] if x['url'] == z['url'])['type']) for x in new_post_data['images'])
        r = as_admin.delete(f'{self.webshop_path}/{webshop_guid}/tags/{tag.guid}')
        assert 204 == r.status_code

    def test_update_campaign_tag(self, as_admin, webshop_guid, faker):
        name = random_tag_name()
        duration = {
            'start': datetime.now(timezone.utc).isoformat(),
            'end': (datetime.now(timezone.utc) + timedelta(hours=2)).isoformat()
        }
        data = dict(desc=faker.name(), method='percentage', amount=Decimal('0.8'))
        type = Tag.Type.CAMPAIGN.name.lower()
        r = as_admin.post(f'{self.webshop_path}/{webshop_guid}/tags', json=dict(name=name, type=type,
                                                                                data=data, duration=duration))
        assert 201 == r.status_code, r.json['message']
        tag_guid = r.json['guid']
        assert Tag.API.unify_name(name) == r.json['name']
        assert r.json['active']
        assert r.json.get('data', None) is not None
        assert type == r.json['type']

        new_name = random_tag_name()
        new_duration = {
            'start': (datetime.now(timezone.utc) + timedelta(hours=1)).isoformat(),
            'end': (datetime.now(timezone.utc) + timedelta(hours=2)).isoformat()
        }
        new_data = dict(desc=faker.name(), method='percentage', amount=Decimal('0.6'))
        r = as_admin.put(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}',
                         json=dict(name=new_name, duration=new_duration,
                                   data=new_data))
        tag_guid = r.json['guid']
        assert Tag.API.unify_name(new_name) == r.json['name']
        assert False == r.json['active']
        assert type == r.json['type']
        assert 200 == r.status_code, r.json['message']

        r = as_admin.delete(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 204 == r.status_code, r.json['message']

    @pytest.mark.parametrize('url', ['/tags/{tag_guid}/', '/webshops/{webshop_tag_owner}/tags/{webshop_tag_guid}'])
    @pytest.mark.parametrize('data', [dict(name=random_tag_name())])
    def test_put_parent_to_subtag(self, as_admin, webshop_tags_suite, tags_suite, faker, url, data):
        tag = random.choice(tags_suite)
        webshop_tag = random.choice(webshop_tags_suite)
        url = url.format(
            tag_guid=tag.guid,
            webshop_tag_owner=webshop_tag.owner,
            webshop_tag_guid=webshop_tag.guid,
        )
        r_put = getattr(as_admin, 'put')(url, json=data)
        r_get = getattr(as_admin, 'get')(url)
        assert r_put.json == r_get.json

    def test_get_products_from_tag_with_no_products(self, as_admin, tags_suite):
        tag_id = next(x.guid for x in tags_suite if not x.product_guids)
        r = as_admin.get(f'/product_tags/{tag_id}')
        assert 200 == r.status_code
        assert [] == r.json

    def test_add_check_and_delete_product_tags(self, as_admin, tags_suite, saved_products):
        tag = random.choice(tags_suite)
        tag_id = str(tag.guid)
        r = as_admin.put(f'/product_tags/{tag_id}', json=dict(add=saved_products['to_add']))
        assert 204 == r.status_code, r.json

    @pytest.mark.skipif(Env.VERTICAL != 'imeds', reason=f'Not applicable to "{Env.VERTICAL}"')
    @pytest.mark.parametrize('product_type', ['medicine', 'prescription_medicine'])
    def test_add_medicine_to_campaign(self, as_admin, tags_suite, saved_products, product_type):
        tag = next(x for x in tags_suite if x.type == Tag.Type.CAMPAIGN)
        product = next(x for x in saved_products['all'] if x.type == product_type)
        r = as_admin.put(f'/product_tags/{tag.guid}', json=dict(add=[product.guid]))
        assert 409 == r.status_code, r.json
        assert f"Can not add product of '{product.type}' type to campaign" in r.json['message']

    def test_add_get_remove_campaign_tag_as_shopkeeper(self, as_shopkeeper, webshop_guid, faker):
        duration = {
            'start': datetime.now(timezone.utc).isoformat(),
            'end': (datetime.now(timezone.utc) + timedelta(hours=2)).isoformat()
        }
        data = dict(desc=faker.name(), method='percentage', amount=Decimal('0.8'))
        type = Tag.Type.CAMPAIGN.name.lower()
        r = as_shopkeeper.post(f'{self.webshop_path}/{str(webshop_guid)}/tags',
                               json=dict(
                                   name=random_tag_name(),
                                   type=type,
                                   data=data,
                                   duration=duration))
        assert 201 == r.status_code, r.json['message']
        tag_guid = r.json['guid']
        r = as_shopkeeper.get(f'/tags/{tag_guid}/')
        assert 200 == r.status_code, r.json['message']
        assert True == r.json['active']
        assert type == r.json['type']
        r = as_shopkeeper.delete(f'{self.webshop_path}/{webshop_guid}/tags/{tag_guid}')
        assert 204 == r.status_code, r.json['message']
        r = as_shopkeeper.get(f'/tags/{tag_guid}/')
        assert 404 == r.status_code

    def test_remove_campaign_tag_as_wrong_shopkeeper(self, as_admin, as_shopkeeper, faker):
        duration = {
            'start': datetime.now(timezone.utc).isoformat(),
            'end': (datetime.now(timezone.utc) + timedelta(hours=2)).isoformat()
        }
        data = dict(desc=faker.name(), method='percentage', amount=Decimal('0.8'))
        type = Tag.Type.CAMPAIGN.name.lower()
        tag_guid = None
        other_shop_guid = uuid.uuid4()

        r = as_admin.post(f'{self.webshop_path}/{other_shop_guid}/tags', json=dict(name=random_tag_name(), type=type,
                                                                                   data=data, duration=duration))
        assert 201 == r.status_code, r.json['message']
        tag_guid = r.json['guid']
        r = as_shopkeeper.get(f'/tags/{tag_guid}/')
        assert 200 == r.status_code, r.json['message']
        assert True == r.json['active']
        assert type == r.json['type']
        r = as_shopkeeper.delete(f'{self.webshop_path}/{other_shop_guid}/tags/{tag_guid}')
        assert 403 == r.status_code
        r = as_admin.delete(f'{self.webshop_path}/{other_shop_guid}/tags/{tag_guid}')
        assert 204 == r.status_code, r.json['message']
        r = as_shopkeeper.get(f'/tags/{tag_guid}/')
        assert 404 == r.status_code

    def test_try_create_campaign_tag_without_duration(self, as_admin, tags_suite, webshop_guid):
        r = as_admin.post(f'{self.webshop_path}/{webshop_guid}/tags',
                          json=dict(name=tags_suite[0].name, type=TagType.CAMPAIGN.name.lower()))
        assert r.status_code == 409, r.json
        assert any('is required' in x for x in json.loads(r.json['message'].replace("'", '"'))['duration'])

    def test_global_tag_crud(self, as_admin, random_tag):
        post_data = Tag.Schemas.Post().dump(random_tag_data())
        r = as_admin.post(f'/tags/', json=post_data)
        assert 201 == r.status_code
        tag_guid = r.json['guid']
        assert Tag.API.unify_name(post_data['name']) == r.json['name']
        new_name = random_tag_name()
        r = as_admin.put(f'/tags/{tag_guid}/', json=dict(name=new_name))
        assert 200 == r.status_code
        assert Tag.API.unify_name(new_name) == r.json['name']
        r = as_admin.delete(f'/tags/{tag_guid}/')
        assert 204 == r.status_code

    @pytest.mark.parametrize('tag_type', [t for t in TagType])
    def test_get_brand_tags(self, brand_tags_suite, tags_suite, as_anonymous, tag_type):
        root_type_tag_guids = [t.guid for t in Tag.scan() if t.type == tag_type]
        res = as_anonymous.get(f'/tags?type={tag_type.value}')
        assert 200 == res.status_code
        assert set(root_type_tag_guids) == set([uuid.UUID(t['guid']) for t in res.json])


@pytest.fixture(scope='session')
def another_brand_guid():
    return get_random_uuid()


@pytest.fixture(scope='class')
def brand_tags_suite(brand_guid, another_brand_guid, create_random_tag):
    tags = [create_random_tag(owner=owner_guid, type=Tag.Type.BRAND) for owner_guid in (brand_guid, another_brand_guid) for _ in range(3)]
    for tag in tags:
        tag.save()
    return tags


@pytest.mark.usefixtures('mock_aws', 'brand_tags_suite')
class TestBrandTagsAPI:
    @pytest.mark.parametrize('tag_type', [x.value for x in list(Tag.Type) if x != Tag.Type.BRAND])
    def test_post(self, as_brand_owner, brand_guid, tag_type):
        tag_data = random_tag_data(type=tag_type)
        res = as_brand_owner.post(f'/brands/{brand_guid}/tags', json=tag_data)
        assert 201 == res.status_code, res.json
        tag_data['owner'] = str(brand_guid)
        tag_data['type'] = Tag.Type.BRAND.value
        tag_data['name'] = Tag.API.unify_name(tag_data['name'])
        assert all(res.json[x] == y for x, y in tag_data.items())
        assert Tag.get(owner=brand_guid, guid=uuid.UUID(res.json['guid']))  # Testing the tag is saved

    def test_post_invalid_brand_guid(self, as_brand_owner):
        res = as_brand_owner.post(f'/brands/{get_random_uuid()}/tags', json=Tag.Schemas.Post().dump(random_tag_data()))
        assert 404 == res.status_code

    def test_put(self, as_brand_owner, brand_guid, brand_tags_suite):
        tag_data = random_tag_data(type=next((x.value for x in list(Tag.Type) if x != Tag.Type.BRAND)))
        tag = next(x for x in brand_tags_suite if x.owner == brand_guid)
        tag_values = Tag.Schema().load(tag_data)
        tag_values['name'] = tag_values['name'].lower()
        put_res = as_brand_owner.put(f'/brands/{brand_guid}/tags/{tag.guid}', json=tag_data)
        assert 200 == put_res.status_code
        tag_values['name'] = Tag.API.unify_name(tag_data['name'])
        assert put_res.json['name'] == tag_values['name']
        # TODO: GET should be done on the same path as PUT, but GET doesn't exist for that
        get_res = as_brand_owner.get(f'/tags/{tag.guid}')
        assert 200 == get_res.status_code
        assert put_res.json == get_res.json  # Testing the change is saved

    def test_delete(self, as_brand_owner, brand_guid, brand_tags_suite):
        tag = next(x for x in brand_tags_suite if x.owner == brand_guid)
        get_tag = lambda: Tag.get(owner=brand_guid, guid=tag.guid)
        assert get_tag()
        res = as_brand_owner.delete(f'/brands/{brand_guid}/tags/{tag.guid}')
        brand_tags_suite.remove(tag)
        assert 204 == res.status_code
        with pytest.raises(Tag.DoesNotExist):
            get_tag()

    @pytest.mark.parametrize('_brand_guid, tag_guid, res_code', [
        (pytest.lazy_fixture('another_brand_guid'), None, 404),
        (pytest.lazy_fixture('brand_guid'), get_random_uuid(), 204),
    ])
    def test_delete_not_own(self, as_brand_owner, brand_tags_suite, _brand_guid, tag_guid, res_code):
        if tag_guid is None:
            tag_guid = next(x.guid for x in Tag.scan() if x.owner == _brand_guid)

        res = as_brand_owner.delete(f'/brands/{_brand_guid}/tags/{tag_guid}')
        assert res_code == res.status_code
