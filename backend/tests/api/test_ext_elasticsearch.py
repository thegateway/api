# -*- coding: utf-8 -*-
"""
Test the elastic search extension
"""
import pytest
from gwio.environment import Env

from gwio.ext.elasticsearch import es_hosts


def test_es_hosts(monkeypatch):
    monkeypatch.setattr(Env, 'ELASTICSEARCH_HOST', 'http://localhost:9200')
    monkeypatch.setenv('ELASTICSEARCH_HOST', 'http://localhost:9200')
    assert [dict(host='localhost', port=9200)] == es_hosts()
    monkeypatch.setenv('ELASTICSEARCH_HOST', 'http://localhost')
    assert [dict(host='localhost', port=80)] == es_hosts()
    monkeypatch.setenv('ELASTICSEARCH_HOST', 'https://localhost')
    assert [dict(host='localhost', port=443, use_ssl=True)] == es_hosts()
    monkeypatch.setenv('ELASTICSEARCH_HOST', 'https://localhost:9200')
    assert [dict(host='localhost', port=9200, use_ssl=True)] == es_hosts()


@pytest.mark.parametrize('mock_env_value, port, use_ssl', [
    ('http://localhost:9200', 9200, False),
    ('http://localhost', 80, False),
    ('https://localhost', 443, True),
    ('https://localhost:9200', 9200, True),
])
def test_es_hosts(monkeypatch, mock_env_value, port, use_ssl):
    monkeypatch.setattr(Env, 'ELASTICSEARCH_HOST', mock_env_value)
    expected = dict(host='localhost', port=port)
    if use_ssl:
        expected['use_ssl'] = use_ssl
    assert [expected] == es_hosts()
