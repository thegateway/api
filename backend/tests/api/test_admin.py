# coding=utf-8
import uuid

import pytest
from flask import json
from gwio.environment import Env as env
from gwio.pytest.suites import include_suites
from pytest_lazyfixture import LazyFixture

from gwio.utils.notify import GenericUserNotification
from gwio.models.user import UserData
from gwio.utils.aws import CognitoIdpClient
from ._mocks import MockedCoreCognitoIdpClient


@pytest.fixture(scope='function')
def dummy_cognito_user(faker):
    email = faker.email()
    yield {
        'username': email,
        'password': 'panda_password_!234',
        'family_name': 'Bear',
        'name': 'Panda',
        'phone_number': '+358987654321',
        'email': email
    }


@pytest.fixture(scope='function')
@pytest.mark.usefixtures('mock_aws')
def cognito_client(dummy_cognito_user):
    clnt = CognitoIdpClient(pool_id=env.COGNITO_POOL_ID, client_id=env.COGNITO_CLIENT_ID)
    yield clnt
    try:
        clnt.client.admin_delete_user(UserPoolId=clnt.pool_id, Username=dummy_cognito_user['username'])
    except Exception:
        pass


@pytest.fixture(scope='function')
def test_user_guid(cognito_client, dummy_cognito_user):
    user_attr = {
        'name': dummy_cognito_user['name'],
        'phone_number': dummy_cognito_user['phone_number'],
        'email': dummy_cognito_user['email'],
    }
    cognito_client.add_user(username=dummy_cognito_user['username'],
                            password=dummy_cognito_user['password'],
                            user_attributes=user_attr)

    auth_response = cognito_client.auth_with_password(username=dummy_cognito_user['username'],
                                                      password=dummy_cognito_user['password'])
    refresh_token_auth_response = cognito_client.auth_with_refresh_token(
        auth_response['AuthenticationResult']['RefreshToken'])
    refresh_token_parsed_jwt = cognito_client.parse_jwt(
        jwt=refresh_token_auth_response['AuthenticationResult']['IdToken'])
    yield uuid.UUID(refresh_token_parsed_jwt['cognito:username'])


def get_user_in_response(client, test_user_guid, res):
    test_user_response = []
    while not test_user_response:
        test_user_response = list(filter(lambda user: user['id'] == test_user_guid, res.json['users']))
        if test_user_response:
            return test_user_response

        try:
            split_next = res.json["next"].split("/")
        except AttributeError:
            return list()

        res = client.get(f'/{split_next[-2]}/{split_next[-1]}')


class TestAdminAPIAdmin:
    @pytest.mark.skip(range='Unreliable test (random failures)')
    @pytest.mark.skipif(not include_suites('cognito'), reason="Cognito tests disabled")
    @pytest.mark.parametrize('as_x, expected_post_status', [(pytest.lazy_fixture('as_admin'), 204),
                                                            (pytest.lazy_fixture('as_customer'), 403),
                                                            (pytest.lazy_fixture('as_shopkeeper'), 403),
                                                            (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_add_user_to_shop(self, as_x, test_user_guid, mocker, webshop_guid, expected_post_status):
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        res = as_x.post(f'/shops/{webshop_guid}/add_user/{test_user_guid}')
        assert expected_post_status == res.status_code
        if expected_post_status == 204:
            res = as_x.get('/users/')
            assert 200 == res.status_code
            test_user_response = get_user_in_response(as_x, test_user_guid, res)
            assert 1 == len(test_user_response)
            assert webshop_guid == uuid.UUID(test_user_response[0]['organization_guid'])

    @pytest.mark.parametrize('as_x, expected_status', [(pytest.lazy_fixture('as_admin'), 200),
                                                       (pytest.lazy_fixture('as_customer'), 403),
                                                       (pytest.lazy_fixture('as_shopkeeper'), 403),
                                                       (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_list_users(self, as_x, expected_status):
        res = as_x.get('/users/')
        assert expected_status == res.status_code


class TestAdminCustomerAPIAdmin:
    @pytest.mark.parametrize('client, expected_status', [(pytest.lazy_fixture('as_admin'), 204),
                                                         (pytest.lazy_fixture('as_customer'), 403),
                                                         (pytest.lazy_fixture('as_shopkeeper'), 204),
                                                         (pytest.lazy_fixture('as_brand_owner'), 204),
                                                         (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_add_user_to_organization(self, client, expected_status, mocker, test_customer, customer_identity_guid, brand_guid, webshop):
        test_customer.save()
        if 'product_owner' in client.token_payload.get('cognito:groups', []):
            data = dict(group_name='product_owner', organization_guid=brand_guid)
        else:
            data = dict(group_name='shopkeeper', organization_guid=webshop.guid)
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        res = client.post(f'/users/{customer_identity_guid}/organizations', json=data)
        assert expected_status == res.status_code


@pytest.mark.skip(range='Unreliable test (random failures)')
class TestAdminAPIAdmin:

    @pytest.mark.skipif(not include_suites('cognito'), reason="Cognito tests disabled")
    def test_add_to_organization_as_admin(self, as_admin, test_user_guid, mocker, webshop_guid):
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        res = as_admin.post(f'/users/{test_user_guid}/organizations',
                            json=dict(organization_guid=webshop_guid, group_name='shopkeeper'))
        assert 204 == res.status_code
        res = as_admin.get('/users/')
        assert 200 == res.status_code
        test_user_response = get_user_in_response(as_admin, test_user_guid, res)
        assert 1 == len(test_user_response)
        assert webshop_guid == uuid.UUID(test_user_response[0]['organization_guid'])

    @pytest.mark.usefixtures('mock_customer_pool')
    def test_aws_cognito_list_users(self, as_admin, mocker):
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        response = as_admin.get('/users/')
        assert response.status_code == 200
        data = json.loads(response.data)
        assert {'next', 'users'} == set(data)

        next_url = data['next']
        while True:
            next_page_response = as_admin.get(next_url)
            assert response.status_code == 200
            next_page_data = json.loads(next_page_response.data)
            if next_page_data['next'] is None:
                break
            next_url = next_page_data['next']

    def test_user_notify(self, mock_aws, customer_guid, faker, mocker, as_admin):
        assert mock_aws

        class MockWrapper:
            _notification = None
            _subscriptions = None

            @staticmethod
            def mock_send(notification, subscriptions):
                MockWrapper._notification = notification
                MockWrapper._subscriptions = subscriptions

        mocker.patch.object(GenericUserNotification, 'send', MockWrapper.mock_send)

        user_data = UserData(
            guid=customer_guid,
            notifications=dict(types=[dict(type='email', data=dict(email_address=faker.email()))]))
        user_data.save()

        post_data = dict(
            title=faker.sentence(nb_words=5),
            body='\n'.join(faker.paragraphs(nb=3))
        )
        res = as_admin.post(f'/users/{customer_guid}/send_notification', json=post_data)
        assert 204 == res.status_code
        assert post_data['title'] == MockWrapper._notification.title
        assert post_data['body'] == MockWrapper._notification.email_html
        assert post_data['body'] == MockWrapper._notification.email_text
        assert post_data['body'] == MockWrapper._notification.simple_body
        assert user_data.notifications['types'] == MockWrapper._subscriptions


@pytest.fixture(scope='function')
def cognito_customer(customer_identity_id, customer_guid):
    user = UserData(guid=customer_guid, identity_id=customer_identity_id)
    user.save()
    yield user
    user.delete()


class TestSearchUser:
    @pytest.mark.parametrize('as_role', [pytest.lazy_fixture('as_admin'), pytest.lazy_fixture('as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_user_search(self, mock_aws, as_role, dummy_cognito_user, mocker, cognito_customer):
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        query_params = {**{key: dummy_cognito_user[key] for key in ['phone_number', 'email']},
                        'guid': str(uuid.uuid4()),
                        'identity_id': cognito_customer.identity_id}
        for key, value in query_params.items():
            res = as_role.get(f'/users/search?{key}={value}')
            assert 200 == res.status_code
            assert len(res.json) > 0


@pytest.mark.usefixtures('mock_aws')
class TestAdminAPINonAdmin:
    non_admins_parametrize = dict(
        argnames='as_x',
        argvalues=(
            pytest.lazy_fixture('as_shopkeeper'),
            pytest.lazy_fixture('as_brand_owner'),
            pytest.lazy_fixture('as_customer')),
        ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x)
    )

    @pytest.mark.parametrize(**non_admins_parametrize)
    def test_add_user_to_shop(self, webshop_guid, as_x):
        if callable(as_x):
            as_x = as_x()
        user_guid = uuid.UUID(int=1)
        res = as_x.post(f'/shops/{webshop_guid}/add_user/{user_guid}')
        assert 403 == res.status_code

    @pytest.mark.parametrize(**non_admins_parametrize)
    def test_list_users(self, as_x):
        if callable(as_x):
            as_x = as_x()
        res = as_x.get('/users/')
        assert 403 == res.status_code

    @pytest.mark.parametrize(**non_admins_parametrize)
    def test_user_notify(self, customer_guid, as_x):
        if callable(as_x):
            as_x = as_x()
        res = as_x.post(f'/users/{customer_guid}/send_notification')
        assert 403 == res.status_code

    @pytest.mark.skip(range='Unreliable test (random failures)')
    @pytest.mark.skipif(not include_suites('cognito'), reason="Cognito tests disabled")
    def test_add_to_organization_as_shopkeeper(self, as_shopkeeper, as_admin, test_user_guid, mocker, webshop_guid):
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        res = as_shopkeeper.post(f'/users/{test_user_guid}/organizations',
                                 json=dict(organization_guid=webshop_guid, group_name='shopkeeper'))
        assert 204 == res.status_code
        res = as_admin.get('/users/')
        assert 200 == res.status_code
        test_user_response = get_user_in_response(as_admin, test_user_guid, res)
        assert 1 == len(test_user_response)
        assert webshop_guid == uuid.UUID(test_user_response[0]['organization_guid'])

    @pytest.mark.skip(range='Unreliable test (random failures)')
    @pytest.mark.skipif(not include_suites('cognito'), reason="Cognito tests disabled")
    def test_add_to_other_organization_as_shopkeeper(self, as_shopkeeper, test_user_guid, mocker):
        mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
        res = as_shopkeeper.post(f'/users/{test_user_guid}/organizations',
                                 json=dict(organization_guid=uuid.uuid4(), group_name='shopkeeper'))
        assert 404 == res.status_code
