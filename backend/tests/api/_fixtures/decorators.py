# -*- coding: utf-8 -*-
"""
Helper decorators
"""
from typing import Callable, Union

from pynamodb.models import Model

from gwio.environment import VERTICAL

_product_fixtures = dict()


def random_product_for_vertical(vertical: Union[str, None] = None) -> Callable[[Callable], Model]:
    """Simple decorator for registering vertical specific product fixture"""

    if vertical is None:
        try:
            return _product_fixtures[VERTICAL]()
        except KeyError:
            return None

    def _register(cls: Model) -> Model:
        _product_fixtures[vertical] = cls
        return cls

    return _register
