# -*- coding: utf-8 -*-
import pytest

from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DEFAULT
from gwio.models.webshop_product import WebshopProduct
from .._fakes.products import random_product_data
from .._fakes.webshop_products import random_webshop_product_data


@pytest.fixture(scope='class')
def create_random_product():
    def fn(**kwargs):
        params = random_product_data(**kwargs)
        return Product(**params)

    return fn


@pytest.fixture(scope='class')
def random_product(create_random_product):
    return create_random_product()


@pytest.fixture(scope='class')
def random_products(create_random_product):
    def n_products(n, **kwargs):
        return [create_random_product(**kwargs) for _ in range(n)]

    return n_products


@pytest.fixture(scope='class')
def products_suite(random_products, faker):
    products = list()
    # Default products must have a space in the name for the query tests to not fail
    for product in random_products(10) + random_products(6, type=PRODUCT_TYPE_DEFAULT, name=faker.sentence(nb_words=4, variable_nb_words=False)):
        product.save()
        products.append(product)
    return products


@pytest.fixture(scope='class')
def create_random_webshop_product():
    def fn(**kwargs):
        params = random_webshop_product_data(**kwargs)
        return WebshopProduct(**params)

    return fn
