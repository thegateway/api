# -*- coding: utf-8 -*-
import uuid

import pytest

from gwio.models.products import Product
from .._fakes.delivery_methods import random_delivery_method_data


@pytest.fixture(scope='class')
def create_random_delivery_method():
    def fn(**kwargs):
        params = random_delivery_method_data(**kwargs)
        return Product(**params)

    return fn


@pytest.fixture(scope='class')
def random_delivery_method(create_random_delivery_method):
    return create_random_delivery_method()


@pytest.fixture(scope='class')
def random_delivery_methods(create_random_delivery_method):
    def n_delivery_methods(n, **kwargs):
        return [create_random_delivery_method(**kwargs) for _ in range(n)]

    return n_delivery_methods


@pytest.fixture(scope='class')
def delivery_method_xpress_couriers(create_random_delivery_method):
    delivery_method = create_random_delivery_method(
        owner_guid=uuid.UUID(int=0),
        data=dict(
            xpress_couriers=dict(
                service_type=1,
                service_id=89,
            )
        )
    )
    delivery_method.save()
    yield delivery_method
    delivery_method.delete()
