# -*- coding: utf-8 -*-
"""
Fixtures (interfaces for these should be in conf test)
"""
from .._fixtures.coupons import (
    create_random_promotion,
)
from .._fixtures.orders import create_order_via_api
from .._fixtures.residences import create_random_residence
from .brands import (
    create_random_brand,
    create_random_brands,
)
from .customer import (
    test_customer,
    test_userdata_shortlived,
)
from .delivery_methods import (
    create_random_delivery_method,
    delivery_method_xpress_couriers,
    random_delivery_method,
    random_delivery_methods,
)
from .locations import (
    create_random_location,
    location
)
from .payment_methods import (
    create_random_payment_method,
    random_payment_method,
    random_payment_methods,
    stripe_payment_method,
    payment_method_payu,
    payment_method_cod,
)
from .pricing import (
    create_random_pricing,
    create_random_pricings,
)
from .products import (
    create_random_product,
    create_random_webshop_product,
    products_suite,
    random_product,
    random_products,
)
from .tags import (
    create_random_tag,
    random_tag,
)
from .webshops import (
    create_random_webshop,
    random_webshop,
    random_webshops,
    webshop,
)

all = [
    'create_order_via_api',
    'create_random_brand',
    'create_random_brands',
    'create_random_delivery_method',
    'create_random_location',
    'create_random_payment_method',
    'create_random_pricing',
    'create_random_pricings',
    'create_random_product',
    'create_random_promotion',
    'create_random_residence',
    'create_random_tag',
    'create_random_webshop',
    'create_random_webshop_product',
    'delivery_method_xpress_couriers',
    'location',
    'payment_method_payu',
    'payment_method_cod',
    'products_suite',
    'random_delivery_method',
    'random_delivery_methods',
    'random_payment_method',
    'random_payment_methods',
    'random_product',
    'random_products',
    'random_tag',
    'random_webshop',
    'random_webshops',
    'stripe_payment_method',
    'test_customer',
    'test_userdata_shortlived',
    'webshop',
]
