import pytest

from gwio.models import user
from gwio.utils import uuid

UserData = user.UserData


@pytest.fixture(scope='function')
def test_customer(customer_guid, customer_identity_id):
    user = UserData(guid=customer_guid, notifications={})
    user.identity_id = customer_identity_id
    yield user


@pytest.fixture(scope='function')
def test_userdata_shortlived(customer_identity_id):
    user = UserData(guid=uuid.uuid4(), notifications={})
    user.identity_id = customer_identity_id
    user.save()
    yield user
    user.delete()
