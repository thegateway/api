# -*- coding: utf-8 -*-

import pytest

from gwio.logic.payments.types import PaymentTypes
from gwio.models.payment_method import PaymentMethod
from .._fakes.payment_methods import random_payment_method_data


@pytest.fixture(scope='class')
def create_random_payment_method():
    def _fn(**kwargs):
        params = random_payment_method_data(**kwargs)
        return PaymentMethod(**params)

    return _fn


@pytest.fixture(scope='class')
def random_payment_method(create_random_payment_method):
    return create_random_payment_method()


@pytest.fixture(scope='class')
def random_payment_methods(create_random_payment_method):
    def n_methods(n, **kwargs):
        return [create_random_payment_method(**kwargs) for _ in range(n)]

    return n_methods


@pytest.fixture(scope='class')
def stripe_payment_method(create_random_payment_method):
    pm = create_random_payment_method(
        type=PaymentMethod.API.Type.STRIPE,
    )
    pm.save()
    yield pm
    pm.delete()


@pytest.fixture(scope='class')
def payment_method_payu(mock_aws, create_random_payment_method):
    assert mock_aws
    payment_method = create_random_payment_method(
        name='PayU',
        type=PaymentTypes.PAYU)
    payment_method.save()
    yield payment_method

@pytest.fixture(scope='class')
def payment_method_cod(mock_aws, create_random_payment_method):
    assert mock_aws
    payment_method = create_random_payment_method(
        name='COD',
        type=PaymentTypes.COD)
    payment_method.save()
    yield payment_method
