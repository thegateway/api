# -*- coding: utf-8 -*-

import pytest

from gwio.models.pricing import Pricing
from .._fakes.pricing import random_pricing


@pytest.fixture(scope='session')
def create_random_pricing():
    def fn(**kwargs):
        params = random_pricing()
        params.update(kwargs)
        pricing = Pricing(guid=Pricing.API.generate_guid(**params), **params)
        return pricing

    return fn


@pytest.fixture(scope='session')
def create_random_pricings(create_random_pricing):
    def fn(n):
        for _ in range(n):
            yield create_random_pricing()

    return fn
