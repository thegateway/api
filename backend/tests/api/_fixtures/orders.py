import random
import uuid
from decimal import Decimal
from typing import (
    List,
    Union,
)

import pytest

from gwio.core.utils.testing.helpers import (
    THE_ONE_CURRENCY,
    fake,
    image_url,
)
from gwio.ext.vat import (
    Price,
    VatPrice,
)
from gwio.logic.orders.handler import (
    OrderHandler,
    _notifications as fsm_notifications,
)
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.models.orders import Order
from gwio.models.payment_method import PaymentMethod
from gwio.models.webshop_product import WebshopProduct
from gwio.utils.uuid import get_random_uuid


def listify(value):
    if isinstance(value, list):
        return value
    return [value]


def get_random_identity_id():
    return f'eu-west-1:{uuid.uuid4()}'


def fake_order(**kwargs):
    vat = Decimal(random.choice([24, 14, 10]))
    currency = THE_ONE_CURRENCY
    webshop_guid = kwargs.get('shop_guid', get_random_uuid())
    return Order(**{
        **dict(
            guid=get_random_uuid(),
            shop_guids=[webshop_guid],
            buyer_identity_id=get_random_identity_id(),
            customer=dict(
                name=fake.name()),
            _current_state=OrderState.START,
            product_lines=[dict(
                guid=get_random_uuid(),
                shop_guid=webshop_guid,
                type=random.choice(['medicine', 'prescription_medicine', 'default']),
                name=fake.word(),
                images=[image_url() for _ in range(random.randint(0, 10))],
                qty=Decimal(random.randint(1, 20)),
                price=dict(
                    original=dict(
                        unit=VatPrice.from_price_and_vat(Price(Decimal(random.randint(1, 10)), currency), vat=vat),
                        total=VatPrice.from_price_and_vat(Price(Decimal(random.randint(1, 10)), currency), vat=vat)),
                    final=dict(
                        unit=VatPrice.from_price_and_vat(Price(Decimal(random.randint(1, 10)), currency), vat=vat),
                        total=VatPrice.from_price_and_vat(Price(Decimal(random.randint(1, 10)), currency), vat=vat)),
                    currency=currency,
                    vat_class=vat)
            )],
            summary=dict(
                currency=currency,
                price=Decimal(random.randint(100, 1000)),
                vat0_price=Decimal(random.randint(10, 100)),
            )),
        **kwargs,
    })


def valid_order_create_post_data(*, products, webshop_guid):
    return dict(customer_name=fake.name(),
                products=[dict(guid=x.guid, qty=random.randint(5, 20), shop_guid=webshop_guid) for x in random.sample(products, random.randint(3, 5))])


def valid_update_put_data(*, products):
    return dict(products=[dict(guid=x.guid, qty=random.randint(1, 20), shop_guid=x.owner_guid) for x in random.sample(products, random.randint(3, 5))])


class OrderFinder():
    @staticmethod
    def check_valid_action(*, fsm: OrderHandler, order: Order, action: OrderAction):
        return str(action) in fsm.get_triggers(str(order.current_state))

    @staticmethod
    def check_invalid_action(*, fsm: OrderHandler, order: Order, action: OrderAction):
        return str(action) not in fsm.get_triggers(str(order.current_state))

    @staticmethod
    def find_notification(*, start_state: OrderState, action: OrderAction, end_state: OrderState, fail_safe: bool):
        for x in fsm_notifications:
            ctx = x.ctx if x.ctx is not None else dict()
            if ctx.get('fail_safe', False) != fail_safe:
                continue
            if x.by != OrderAction._ANY and action not in listify(x.by):
                continue
            if x.state != OrderState._ANY and end_state not in listify(x.state):
                continue
            if x.via != OrderState._ANY and start_state not in listify(x.via):
                continue
            return x
        return None

    @staticmethod
    def find_order(*checks, orders: List[Order]):
        for order in orders:
            order.refresh()
            if all(check(order) for check in checks):
                return order
        assert False, 'Could not find order to test'


@pytest.fixture(scope='class')
def create_order_via_api(
        create_random_product,
        create_random_payment_method,
        create_random_delivery_method,
        webshop,
        as_customer,
        faker,
        customer_identity_guid,
        delivery_method_webshop_guid,
        create_random_webshop,
):
    def fn(payment_method: Union[PaymentMethod, dict] = None, update_input=None):
        if update_input is None:
            update_input = dict()

        # Create the records, that are used in the order, in the database
        if not isinstance(payment_method, PaymentMethod):
            try:
                payment_method = create_random_payment_method(**payment_method)
            except TypeError:
                payment_method = create_random_payment_method()
            payment_method.save()

        delivery_webshop = create_random_webshop(guid=delivery_method_webshop_guid)
        delivery_webshop.save()
        delivery_method = create_random_delivery_method(owner_guid=delivery_method_webshop_guid)
        delivery_method.save()

        products = []
        for _ in range(random.randint(10, 15)):
            product = create_random_product(owner_guid=webshop.guid)
            wp = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
            product.save()
            wp.save()
            products.append(product)

        # Create the order
        r = as_customer.post(f'/customers/{customer_identity_guid}/orders/',
                             json=Order.Schemas.Post().dump(valid_order_create_post_data(products=products, webshop_guid=webshop.guid)))
        assert r.status_code == 201, r.json

        order = Order.get(guid=uuid.UUID(r.json['guid']))
        assert OrderState(r.json['state']) == OrderState.CREATED

        # Update the order to include payment and delivery methods etc.
        update_data = dict(
            payment_method=dict(
                guid=payment_method.guid,
            ),
            delivery_method=delivery_method.guid,
            customer=dict(
                name=faker.name(),
                phone=faker.phone_number(),
            ),
        )
        payment_method_data = {
            PaymentMethod.API.Type.STRIPE: dict(card='card_id'),
            PaymentMethod.API.Type.PAYU: dict(continue_url=fake.url()),
        }
        update_data['payment_method'].update(payment_method_data[payment_method.type])
        update_data = {
            **update_data,
            **update_input,
        }
        r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}', json=Order.Schemas.Put().dump(update_data))
        assert r.status_code == 200, r.json
        assert OrderState(r.json['state']) == OrderState.CREATED

        return Order.get(guid=uuid.UUID(r.json['guid']))

    return fn
