# -*- coding: utf-8 -*-
"""
Fixtures for webshops
"""

import pytest

from gwio.models.webshops import Webshop
from .._fakes.webshops import random_webshop_data


@pytest.fixture(scope='class')
def webshop(webshop_guid, create_random_webshop):
    """This is the webshop that as_shopkeeper() has access to"""
    webshop = create_random_webshop()
    webshop.guid = webshop_guid  # TODO: Remove once core and v2018 are merged
    webshop.save()
    yield webshop
    webshop.delete()


@pytest.fixture(scope='session')
@pytest.mark.usefixtures('mock_aws')
def create_random_webshop(faker):
    def fn(**kwargs):
        params = random_webshop_data()
        params.update(**kwargs)
        webshop = Webshop(**params)
        return webshop

    return fn


@pytest.fixture(scope='class')
def random_webshop(create_random_webshop):
    return create_random_webshop()


@pytest.fixture(scope='class')
def random_webshops(create_random_webshop):
    def n_webshops(n, **kwargs):
        return [create_random_webshop(**kwargs) for _ in range(n)]

    return n_webshops
