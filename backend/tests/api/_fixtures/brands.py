import pytest

from gwio.models.brands import Brand
from .._fakes.brands import random_brand_data


@pytest.fixture(scope='class')
def create_random_brand():
    def fn(**kwargs):
        return Brand(**random_brand_data(**kwargs))

    return fn


@pytest.fixture(scope='class')
def create_random_brands(create_random_brand):
    def fn(n, **kwargs):
        return [create_random_brand(**kwargs) for _ in range(n)]

    return fn
