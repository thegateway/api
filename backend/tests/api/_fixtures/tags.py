import pytest

from gwio.models.tags import Tag
from .._fakes.tags import random_tag_data


@pytest.fixture(scope='function')
def random_tag(**values):
    values = {**random_tag_data(), **values}
    return Tag(**values)

@pytest.fixture(scope='session')
def create_random_tag():
    def func(**values):
        values = random_tag_data(**values)
        return Tag(**values)
    return func
