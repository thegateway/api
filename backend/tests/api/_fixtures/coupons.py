import pytest

from gwio.models.coupons import promotions
from .._fakes.coupons import (
    random_promotion_data,
)


@pytest.fixture(scope='session')
def create_random_promotion():
    def fn(**kwargs):
        return promotions.Promotion(**random_promotion_data(**kwargs))

    return fn
