import pytest

from gwio.models.residence import Residence
from .._fakes.residences import random_residence_data


@pytest.fixture(scope='class')
def create_random_residence():
    def func(**kwargs):
        return Residence(**random_residence_data(**kwargs))

    return func
