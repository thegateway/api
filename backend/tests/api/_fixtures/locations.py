import pytest

from gwio.models.locations import Location
from .._fakes.locations import random_location_data


@pytest.fixture(scope='session')
def location(location_guid):
    """This location is created for whole test session"""
    return Location(**random_location_data(guid=location_guid))


@pytest.fixture(scope='class')
def create_random_location():
    def func(**kwargs):
        return Location(**random_location_data(**kwargs))

    return func
