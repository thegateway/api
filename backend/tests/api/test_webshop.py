# coding=utf-8
import copy
import datetime
import json
import random
import uuid
from decimal import Decimal
from random import (
    choice,
    randint,
    sample,
)

import pytest
import pytz
from pytest_lazyfixture import LazyFixture

from gwio.core.utils.testing.helpers import (
    fake,
)
from gwio.models.modifiers import ModifierMethodType
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.tags import (
    Tag,
)
from gwio.models.webshop_product import WebshopProduct
from gwio.models.webshops import Webshop
from gwio.utils.uuid import (
    get_random_uuid,
)
from ._fakes.webshops import (
    random_opening_hours_exception_data,
    random_opening_hours_normal_data,
    random_social_media_data,
    random_static_pages_data,
    random_webshop_data,
)
from ._fixtures.locations import create_random_location
from .test_tags import tags_suite

assert Webshop
assert create_random_location
assert tags_suite


@pytest.mark.usefixtures('mock_aws')
class TestWebshop:
    def test_webshop_opening_hours(self, create_random_webshop):
        webshop = create_random_webshop(
            opening_hours_normal=[
                random_opening_hours_normal_data(day=1),
                random_opening_hours_normal_data(day=3),
                random_opening_hours_normal_data(day=5)
            ],
            opening_hours_exceptions=[])

        for x in webshop.opening_hours_normal:
            for y in webshop.opening_hours['opening_hours']:
                if x['day'] == y['day']:
                    assert x['start_at'] == y['start_at']
                    assert x['end_at'] == y['end_at']

    def test_social_media(self, faker, create_random_webshop):
        webshop = create_random_webshop(
            social_media=[
                random_social_media_data(link=None, analytics='test'),
                random_social_media_data(link=faker.url(), analytics=None)])
        webshop.save()

    def test_social_media_link_or_analytics_required(self, create_random_webshop):
        webshop = create_random_webshop(social_media=[random_social_media_data(link=None, analytics=None)])
        with pytest.raises(ValueError, match="Attributes 'link' and 'analytics' cannot be both None"):
            webshop.save()


@pytest.fixture(scope='class')
def payment_methods_suite(random_payment_methods):
    methods = list()
    for method in random_payment_methods(3):
        method.save()
        methods.append(method)
    return methods


@pytest.fixture(scope='class')
def locations_suite(create_random_location):
    locations = []
    for _ in range(random.randint(10, 20)):
        location = create_random_location()
        location.save()
        locations.append(location)

    yield locations

    for l in locations:
        l.delete()


@pytest.fixture(scope='class')
def webshop_with_closed_exceptional_hours(create_random_webshop):
    yield create_random_webshop(opening_hours_exceptions=[random_opening_hours_exception_data(start_at=None, end_at=None)])


@pytest.fixture(scope='class')
def webshops_suite(
        mock_aws,
        create_random_delivery_method,
        random_webshops,
        webshop_with_closed_exceptional_hours,
        locations_suite,
        create_random_pricing,
        create_random_webshop_product,
):
    assert mock_aws
    shops = list()

    # Creating global delivery methods
    delivery_method_guids = dict(active=[], inactive=[])
    for activated in (None, datetime.datetime.now(tz=pytz.UTC)):
        for _ in range(randint(7, 10)):
            global_active_delivery_method = create_random_delivery_method(activated=activated)
            global_active_delivery_method.save()
            delivery_method_guids['active' if activated else 'inactive'].append(global_active_delivery_method.guid)

    for shop in random_webshops(2):
        shop.save()
        shops.append(shop)

        # Adding shop specific pricing for some global delivery methods
        for delivery_method_status in ('active', 'inactive'):
            for _ in range(randint(1, 3)):
                delivery_method_guid = random.choice(delivery_method_guids[delivery_method_status])
                pricing = create_random_pricing(owner=shop.guid)
                pricing.save()
                webshop_product = create_random_webshop_product(
                    product_guid=delivery_method_guid,
                    webshop_guid=shop.guid,
                    pricing_guid=pricing.guid,
                )
                webshop_product.save()

    webshop_with_closed_exceptional_hours.save()
    shops.append(webshop_with_closed_exceptional_hours)
    return shops


@pytest.fixture(scope='function')
def bootstrap(mock_aws, create_bootstrap):
    bootstrap = create_bootstrap()
    bootstrap.save()
    yield
    bootstrap.delete()


@pytest.mark.usefixtures('webshops_suite')
class TestWebshopApi:
    schema = Webshop.Schemas.Post()

    def test_get_webshops(self, as_anonymous, webshops_suite):
        r = as_anonymous.get('/webshops')
        assert 200 == r.status_code, r.json
        assert len(list(Webshop.scan())) == len(r.json)
        assert r.json[0].get('private_data') is None
        assert r.json[0].get('requires_action') is None

    def test_get_webshop(self, as_anonymous, as_admin, webshops_suite):
        shop = choice(webshops_suite)
        r = as_anonymous.get(f'/webshops/{shop.guid}/')
        assert 401 == r.status_code, r.json
        r = as_admin.get(f'/webshops/{shop.guid}/')
        assert 200 == r.status_code, r.json
        assert r.json.get('private_data')
        assert r.json.get('requires_action')

        # Test delivery methods contain all global active delivery methods
        active_delivery_methods = [x.guid for x in Product.TypeIndex.query(type=PRODUCT_TYPE_DELIVERY_METHOD, activated__exists=True)]
        assert active_delivery_methods
        active_delivery_methods.sort()
        res_method_guids = [uuid.UUID(x['guid']) for x in r.json['delivery_methods']]
        res_method_guids.sort()
        assert active_delivery_methods == res_method_guids, f'{len(active_delivery_methods)} == {len(res_method_guids)}'

    def test_get_webshop_as_shopkeeper(self, as_anonymous, as_admin, as_shopkeeper, webshops_suite, webshop):
        # Test shopkeeper gets their data
        r = as_shopkeeper.get(f'/webshops/{webshop.guid}/')
        assert 200 == r.status_code, r.json
        assert r.json.get('requires_action')

    @pytest.mark.parametrize('attr_name', ['logo', 'alert', 'private_data', 'promotions', 'description'])
    def test_add_delete_webshop(self, app, as_admin, attr_name, locations_suite):
        with app.application.test_request_context():
            data = Webshop.Schemas.Post().dump(random_webshop_data())
            # Check all those non required field can accept None
            data[attr_name] = None
            r = as_admin.post('/webshops/', json=data)
            assert 201 == r.status_code, r.json
            guid = r.json['guid']
            r = as_admin.delete(f'/webshops/{guid}')
            assert 204 == r.status_code, r.json

    def test_add_webshop_without_logo(self, app, as_admin, random_webshop, webshop_guid, locations_suite):
        with app.application.test_request_context():
            data = self.schema.dump(random_webshop)
            data['locations'] = random.sample([x.guid for x in locations_suite], random.randrange(len(locations_suite)))
            del data['logo']
            r = as_admin.post('/webshops/', json=data)
            assert 201 == r.status_code, r.json

    @pytest.mark.parametrize("as_non_admin, status_code",
                             [(pytest.lazy_fixture('as_shopkeeper'), 403),
                              (pytest.lazy_fixture('as_customer'), 403),
                              (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_webshop_rbac_delete(self, as_non_admin, status_code, webshops_suite):
        shop = choice(webshops_suite)
        r = as_non_admin.delete(f'/webshops/{shop.guid}/')
        assert status_code == r.status_code, r.json

    @pytest.mark.parametrize("as_non_admin, status_code",
                             [(pytest.lazy_fixture('as_shopkeeper'), 201),
                              (pytest.lazy_fixture('as_customer'), 201),
                              (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_webshop_rbac_post(self, app, as_non_admin, status_code, create_random_webshop, locations_suite):
        with app.application.test_request_context():
            webshop = create_random_webshop(enabled=False)
            r = as_non_admin.post(f'/webshops/', json=self.schema.dump(webshop))
            assert status_code == r.status_code, r.json

    @pytest.mark.parametrize("as_non_admin, status_code",
                             [(pytest.lazy_fixture('as_shopkeeper'), 403),
                              (pytest.lazy_fixture('as_customer'), 403),
                              (pytest.lazy_fixture('as_anonymous'), 401)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_webshop_rbac_put(self, app, as_non_admin, status_code, webshops_suite, random_webshop):
        with app.application.test_request_context():
            shop = choice(webshops_suite)
            r = as_non_admin.put(f'/webshops/{shop.guid}/', json=shop.Schemas.Put().dump(random_webshop))
            assert status_code == r.status_code, r.json

    def test_webshop_create_as_non_admin(self, app, as_customer, ato_patched, locations_suite):
        webshop = random_webshop_data()
        webshop['enabled'] = False
        with app.application.test_request_context():
            r = as_customer.post(f'/webshops/', json=self.schema.dump(webshop))
        assert r.status_code == 201, r.json
        data = json.loads(r.data)
        assert not data['enabled']

    @pytest.mark.parametrize('attr_name', (x for x in Webshop.Schemas.Put().fields.keys() if
                                           x not in ('_private_data', 'phone', 'email', 'locations', 'tags')))
    def test_put_webshop(self, app, as_admin, attr_name, webshops_suite, create_random_payment_method):
        shop = choice(webshops_suite)
        with app.application.test_request_context():

            while True:
                rand_data = random_webshop_data()

                if attr_name == 'payment_method_guids':
                    pm = create_random_payment_method()
                    pm.save()
                    rand_data[attr_name] = [str(pm.guid)]

                random_data = Webshop.Schemas.Get(exclude=['static_pages.guid']).dump(rand_data)
                data = {attr_name: random_data[attr_name]}
                if data[attr_name] != Webshop.Schemas.Put().dump(shop)[attr_name]:
                    break

            res = as_admin.put(f'/webshops/{shop.guid}/', json=data)
            assert 200 == res.status_code, res.json
            shop.refresh()

            try:
                assert {x: float(y) if isinstance(y, Decimal) else y for x, y in data[attr_name].items()} == res.json[attr_name]
            except AttributeError:
                if attr_name == 'static_pages':
                    for i, static_page in enumerate(data[attr_name]):
                        static_page['guid'] = res.json[attr_name][i]['guid']

                assert res.json[attr_name] == Webshop.Schemas.Get().dump(data)[attr_name]

            assert data[attr_name] == Webshop.Schemas.Get().dump(shop)[attr_name]

            res = as_admin.put(f'/webshops/{shop.guid}/', json=dict())
            assert 200 == res.status_code, res.json['message']
            assert res.json[attr_name] is not None
            shop.refresh()
            assert getattr(shop, attr_name) is not None

            data = {attr_name: None}
            res = as_admin.put(f'/webshops/{shop.guid}/', json=data)
            if Webshop.Schemas.Put._declared_fields[attr_name].allow_none:
                assert 200 == res.status_code, res.json['message']
                assert res.json[attr_name] is None or res.json[attr_name] == list()
            else:
                assert 409 == res.status_code

    def test_put_webshop_private_data_shopkeeper(self, as_shopkeeper, webshop):
        xpc = webshop._private_data.xpress_couriers
        webshop._private_data.xpress_couriers = None
        webshop.save()
        client_id = str(uuid.uuid4())
        data = dict(
            _private_data=dict(
                xpress_couriers=dict(
                    client_id=client_id
                )))
        res = as_shopkeeper.put(f'/webshops/{webshop.guid}/', json=data)
        assert 200 == res.status_code, res.json
        assert res.json.get('private_data') is None
        webshop.refresh()
        assert webshop._private_data.xpress_couriers is None
        webshop._private_data.xpress_couriers = xpc
        webshop.save()

    def test_put_webshop_private_data_admin(self, as_admin, webshop):
        client_id = str(uuid.uuid4())
        data = dict(
            _private_data=dict(
                xpress_couriers=dict(
                    client_id=client_id
                )))
        res = as_admin.put(f'/webshops/{webshop.guid}/', json=data)
        assert 200 == res.status_code, res.json
        assert res.json['private_data']['xpress_couriers']['client_id'] == client_id
        webshop.refresh()
        assert webshop._private_data.xpress_couriers.client_id == client_id

    @pytest.mark.parametrize('id, post_response, put_response', [
        ('valid_id', 201, 200),
        ('invalid_id', 409, 409),
    ])
    def test_business_id_post_put(self, app, as_admin, webshops_suite, id, post_response, put_response):
        with app.application.test_request_context():
            shop1, shop2 = webshops_suite[0], webshops_suite[1]
            post_business_id = fake.company_vat()
            put_business_id = fake.company_vat()
            if id == 'invalid_id':
                post_business_id = shop2.business_id
                put_business_id = shop2.business_id
            post_data = Webshop.Schemas.Post().dump(random_webshop_data(business_id=post_business_id))
            put_data = Webshop.Schemas.Put().dump(random_webshop_data(business_id=put_business_id))
            r_post = as_admin.post(f'/webshops', json=post_data)
            assert post_response == r_post.status_code
            r_put = as_admin.put(f'/webshops/{shop1.guid}/', json=put_data)
            assert put_response == r_put.status_code, r_put.json

    @pytest.mark.parametrize('l_guids, post_response, put_response', [
        ('valid_guids', 201, 200),
        ([uuid.uuid4() for _ in range(5)], 400, 400),
    ])
    def test_location_guid_post_put(self, app, as_admin, locations_suite, webshops_suite, l_guids, post_response, put_response):
        shop = choice(webshops_suite)
        if l_guids == 'valid_guids':
            l_guids = [l.guid for l in locations_suite]
        with app.application.test_request_context():
            post_data = Webshop.Schemas.Post().dump(random_webshop_data(locations=l_guids))
            put_data = Webshop.Schemas.Put().dump(random_webshop_data(locations=l_guids))
        r_post = as_admin.post(f'/webshops', json=post_data)
        assert post_response == r_post.status_code, r_post.json
        r_put = as_admin.put(f'/webshops/{shop.guid}/', json=put_data)
        assert put_response == r_put.status_code, r_put.json

    @pytest.mark.parametrize('tag_guids, post_response, put_response', [
        ('valid_guids', 201, 200),
        ([uuid.uuid4() for _ in range(5)], 400, 400)
    ])
    def test_tag_guids_post_put(self, app, as_admin, locations_suite, webshops_suite, tags_suite, tag_guids, post_response, put_response):
        shop = choice(webshops_suite)
        if tag_guids == 'valid_guids':
            tag_guids = random.sample([tag.guid for tag in tags_suite], random.randint(1, len(tags_suite) - 1))
        with app.application.test_request_context():
            post_data = Webshop.Schemas.Post().dump(random_webshop_data(tags=tag_guids))
            put_data = Webshop.Schemas.Put().dump(random_webshop_data(tags=tag_guids))
        r_post = as_admin.post(f'/webshops', json=post_data)
        assert post_response == r_post.status_code
        r_put = as_admin.put(f'/webshops/{shop.guid}/', json=put_data)
        assert put_response == r_put.status_code

    def test_local_promotions(self, as_anonymous, random_product, webshops_suite, faker, bootstrap):
        shop = choice(webshops_suite)
        random_product.save()
        webshop_product = WebshopProduct(product_guid=random_product.guid, webshop_guid=shop.guid)
        webshop_product.save()

        duration = {
            'start': datetime.datetime.now(datetime.timezone.utc),
            'end': (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(hours=2))
        }
        data = dict(desc=faker.name(), method=ModifierMethodType.PERCENTAGE, amount=Decimal('0.8'))
        type = Tag.Type.CAMPAIGN
        tag_attributes = dict(duration=duration, data=data, type=type)

        tag = Tag(owner=shop.guid, name='-'.join(faker.words(2)), **tag_attributes)
        tag.save()

        random_product.add_tag(tag)
        random_product.save()
        tag.save()

        r = as_anonymous.get(f'/bootstrap/{shop.guid}/')
        assert 200 == r.status_code
        # XXX: Re-enable once fixed.
        return
        promotions = r.json['shop']['promotions']['local']
        assert promotions
        r = as_anonymous.get(f'/product_tags/{promotions[0]["guid"]}/')
        assert 200 == r.status_code
        assert 1 == len(r.json)
        product_guid = r.json[0]
        assert str(random_product.guid) == product_guid

        r = as_anonymous.get(f'/webshops/{shop.guid}/products/{product_guid}/')
        assert 200 == r.status_code
        random_product.delete()
        webshop_product.delete()
        tag.delete()
        product_tag.delete()

    @pytest.mark.parametrize("forbidden_char",
                             ['<', '>', '\\', '\'', '"', '*'])
    def test_create_webshop_bad_shorthand_name(self, app, as_admin, create_random_webshop, locations_suite, forbidden_char):
        with app.application.test_request_context():
            location_guids = random.sample([x.guid for x in locations_suite], random.randrange(len(locations_suite)))
            webshop = create_random_webshop(locations=location_guids, enabled=False)
            body = self.schema.dump(webshop)
            short_name = list(body['shorthand_name'])[random.randint(0, len(body['shorthand_name']) - 1)] = forbidden_char
            body['shorthand_name'] = ''.join(short_name)
            r = as_admin.post(f'/webshops/', json=body)
            assert 400 == r.status_code
            assert '`shorthand_name` cannot include ' in r.data.decode('utf-8')

    def test_create_webshop_too_long_shorthand_name(self, app, as_admin, create_random_webshop, locations_suite, faker):
        with app.application.test_request_context():
            location_guids = random.sample([x.guid for x in locations_suite], random.randrange(len(locations_suite)))
            webshop = create_random_webshop(locations=location_guids, enabled=False)
            body = self.schema.dump(webshop)
            body['shorthand_name'] = faker.pystr(max_chars=13)
            r = as_admin.post(f'/webshops/', json=body)
            assert 400 == r.status_code
            assert '`shorthand_name` cannot be longer' in r.data.decode('utf-8')


@pytest.fixture(scope='class')
def test_webshop_suite(mock_aws, webshop):
    assert mock_aws
    webshop.save()


@pytest.mark.usefixtures('test_webshop_suite')
class TestOpeningHoursAPI:
    @pytest.mark.parametrize("as_client, status_code",
                             [(pytest.lazy_fixture('as_admin'), 200),
                              (pytest.lazy_fixture('as_customer'), 200),
                              (pytest.lazy_fixture('as_shopkeeper'), 200)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_get_opening_hours(self, webshop_guid, as_client, status_code):
        r = as_client.get(f'/webshops/{webshop_guid}/opening_hours/')
        assert status_code == r.status_code

    @pytest.mark.parametrize("as_client, status_code",
                             [(pytest.lazy_fixture('as_admin'), 200),
                              (pytest.lazy_fixture('as_customer'), 403),
                              (pytest.lazy_fixture('as_shopkeeper'), 200)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_set_opening_hours(self, faker, webshop_guid, as_client, status_code):
        hours = list()
        for day in range(5):
            hours.append(dict(day=day,
                              start_at=datetime.time(randint(0, 11), randint(0, 59)),
                              end_at=datetime.time(randint(13, 23), randint(0, 59))))

        now = datetime.datetime.now()
        current_date = datetime.date(now.year, now.month, now.day)
        exceptions = list()
        for i in range(8):
            delta = datetime.timedelta(days=(i + 1) * 2)
            past = current_date - delta
            future = current_date + delta
            exceptions.append(
                dict(date=past,
                     start_at=datetime.time(randint(6, 11), randint(0, 59)),
                     end_at=datetime.time(randint(18, 23), randint(0, 59)),
                     desc=str(get_random_uuid())))  # using UUID to make sure descriptions are unique because the test assumes they are
            exceptions.append(
                dict(date=future,
                     start_at=datetime.time(randint(0, 5), randint(0, 59)),
                     end_at=datetime.time(randint(12, 17), randint(0, 59)),
                     desc=str(get_random_uuid())))  # using UUID to make sure descriptions are unique because the test assumes they are

        data = Webshop.API.OpeningHours.update().dump(
            dict(opening_hours_normal=hours, opening_hours_exceptions=exceptions))
        r = as_client.put(f'/webshops/{webshop_guid}/opening_hours/', json=data)
        assert status_code == r.status_code

        # Do not continue the test if the put fails (rbac restriction hit)
        if 200 != r.status_code:
            return

        assert not list(filter(lambda day: day['day'] > 6 or day['day'] < 0, r.json['opening_hours_normal']))
        assert not list(filter(lambda day: day['day'] > 6 or day['day'] < 0, r.json['opening_hours']))

        # There should be three exceptional opening hours, those in the past or beyond 6 days from now should not be in the list
        assert 3 == len([x for x in r.json['opening_hours'] if x['exceptional'] is not None])

        r = as_client.get(f'/webshops/{webshop_guid}/')
        for hours_data in hours:
            res_hours_data = list(filter(lambda x: x['day'] == hours_data['day'], r.json['opening_hours']['opening_hours_normal']))[0]
            assert hours_data['start_at'].strftime('%H:%M') == res_hours_data['start_at']
            assert hours_data['end_at'].strftime('%H:%M') == res_hours_data['end_at']

        for exception_data in exceptions:
            r_e_data = list(filter(lambda x: x['date'] == exception_data['date'].strftime('%Y-%m-%d'), r.json['opening_hours']['opening_hours_exceptions']))[0]
            assert exception_data['start_at'].strftime('%H:%M') == r_e_data['start_at']
            assert exception_data['end_at'].strftime('%H:%M') == r_e_data['end_at']
            assert exception_data['desc'] == r_e_data['desc']

        for res_opening_hours in r.json['opening_hours']['opening_hours']:
            try:
                if res_opening_hours['exceptional']:
                    expected_data = list(filter(lambda x: res_opening_hours['exceptional'].startswith(x['desc']), exceptions))[0]
                else:
                    expected_data = list(filter(lambda x: x['day'] == res_opening_hours['day'], hours))[0]

                assert expected_data['start_at'].strftime('%H:%M') == res_opening_hours['start_at']
                assert expected_data['end_at'].strftime('%H:%M') == res_opening_hours['end_at']
            except IndexError:
                assert None == res_opening_hours['start_at']
                assert None == res_opening_hours['end_at']

        # clear opening hours
        data = Webshop.API.OpeningHours.update().dump(
            dict(opening_hours_normal=list(), opening_hours_exceptions=list()))
        r = as_client.put(f'/webshops/{webshop_guid}/opening_hours/', json=data)
        assert 200 == r.status_code
        assert not [x for x in r.json['opening_hours'] if x['start_at'] is not None]

    def test_exceptional_opening_hours_closed(self, faker, webshop_guid, as_shopkeeper):
        now = datetime.datetime.now()
        current_date = datetime.date(now.year, now.month, now.day)
        delta = datetime.timedelta(days=7)
        future = current_date + delta
        exceptions = [dict(date=future,
                           start_at=None,
                           end_at=None,
                           desc=str(get_random_uuid()))]
        data = Webshop.API.OpeningHours.update().dump(
            dict(opening_hours_normal=[], opening_hours_exceptions=exceptions))
        r = as_shopkeeper.put(f'/webshops/{webshop_guid}/opening_hours/', json=data)
        assert 200 == r.status_code

        r = as_shopkeeper.get(f'/webshops/{webshop_guid}/opening_hours/', json=data)
        assert 1 == len(r.json['opening_hours_exceptions'])

    def test_dashboard(self, as_shopkeeper, webshop_guid):
        hours = '''
        {
            "opening_hours_exceptions": [{
                "date": "2018-12-06",
                "desc": "Itsenäisyyspäivä",
                "end_at": null,
                "start_at": null
            }, {
                "date": "2018-12-07",
                "desc": "-",
                "end_at": "13:00",
                "start_at": "09:00"
            }, {
                "date": "2018-12-26",
                "desc": "jp",
                "start_at": null,
                "end_at": null
            }],
            "opening_hours_normal": [{
                "day": 0,
                "end_at": "20:00",
                "start_at": "08:00"
            }, {
                "day": 1,
                "end_at": "20:00",
                "start_at": "08:00"
            }, {
                "day": 2,
                "end_at": "20:00",
                "start_at": "08:00"
            }, {
                "day": 3,
                "end_at": "20:00",
                "start_at": "08:00"
            }, {
                "day": 4,
                "end_at": "20:00",
                "start_at": "08:00"
            }, {
                "day": 6,
                "end_at": "18:00",
                "start_at": "12:00"
            }, {
                "day": 5,
                "end_at": "18:00",
                "start_at": "09:00"
            }]
        }
        '''
        r = as_shopkeeper.put(f'/webshops/{webshop_guid}/opening_hours/', json=json.loads(hours))
        assert 200 == r.status_code


@pytest.fixture(scope='class')
def webshop_paymentmethods_suite(mock_aws, webshop, payment_methods_suite):
    assert mock_aws
    assert payment_methods_suite
    webshop.save()


@pytest.mark.usefixtures('webshop_paymentmethods_suite')
class TestWebshopPaymentMethods:
    @pytest.mark.parametrize("as_client, status_code",
                             [(pytest.lazy_fixture('as_admin'), 200),
                              (pytest.lazy_fixture('as_customer'), 200),
                              (pytest.lazy_fixture('as_shopkeeper'), 200)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_webshop_payment_methods_get_rbac(self, as_client, webshop_guid, status_code):
        r = as_client.get(f'/webshops/{webshop_guid}/payment_methods/')
        assert status_code == r.status_code

    @pytest.mark.usefixtures('mock_stripe')
    @pytest.mark.parametrize("as_client, status_code",
                             [(pytest.lazy_fixture('as_admin'), 200),
                              (pytest.lazy_fixture('as_customer'), 403),
                              (pytest.lazy_fixture('as_shopkeeper'), 200)],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_webshop_payment_methods(self, as_client, webshop_guid, payment_methods_suite, status_code):
        data = dict(payment_methods=[str(x.guid) for x in sample(payment_methods_suite, randint(1, len(payment_methods_suite)))])
        empty = dict(payment_methods=list())

        r = as_client.put(f'/webshops/{webshop_guid}/payment_methods/', json=data)
        assert status_code == r.status_code
        # No need to continue (just rbac check)
        if 200 != r.status_code:
            return
        assert set(data['payment_methods']) == set([x['guid'] for x in r.json])

        r = as_client.get(f'/webshops/{webshop_guid}/payment_methods/')
        assert 200 == r.status_code
        assert set(data['payment_methods']) == set([x['guid'] for x in r.json])

        r = as_client.get(f'/webshops/{webshop_guid}/')
        assert 200 == r.status_code
        assert set(data['payment_methods']) == set([x['guid'] for x in r.json['payment_methods']])

        r = as_client.put(f'/webshops/{webshop_guid}/payment_methods/', json=empty)
        assert 200 == r.status_code
        assert not r.json


@pytest.fixture(scope='class')
def webshop_static_pages_suite(mock_aws, webshop):
    assert mock_aws
    webshop.save()


@pytest.fixture(scope='class')
def static_pages(as_shopkeeper, webshop_guid, faker):
    data = {}
    data['pages'] = [random_static_pages_data() for _ in range(randint(2, 10))]
    r = as_shopkeeper.put(f'/webshops/{webshop_guid}/static_pages/', json=data)
    assert 200 == r.status_code, r.json
    actual_pages = copy.deepcopy(r.json)
    for page in actual_pages:
        uuid.UUID(page.pop('guid'))
    assert actual_pages == data['pages']
    yield r.json


@pytest.mark.usefixtures('webshop_static_pages_suite')
class TestWebshopStaticPages:
    @pytest.mark.parametrize(('client', 'expected_status_code'),
                             [(pytest.lazy_fixture('as_admin'), 200),
                              (pytest.lazy_fixture('as_customer'), 200),
                              (pytest.lazy_fixture('as_shopkeeper'), 200)])
    def test_get_webshop_static_pages(self, client, expected_status_code, webshop_guid):
        resp = client.get(f'/webshops/{webshop_guid}/static_pages/')
        assert expected_status_code == resp.status_code

    @pytest.mark.parametrize(('client', 'expected_status_code'),
                             [(pytest.lazy_fixture('as_admin'), 200),
                              (pytest.lazy_fixture('as_customer'), 403),
                              (pytest.lazy_fixture('as_shopkeeper'), 200)])
    def test_put_webshop_static_pages_new(self, client, expected_status_code, webshop_guid, faker):
        data = {}
        data['pages'] = [dict(title=faker.sentence(), link=faker.uri(), inline=faker.boolean()) for _ in
                         range(randint(2, 10))]
        resp = client.put(f'/webshops/{webshop_guid}/static_pages/', json=data)
        assert expected_status_code == resp.status_code
        if resp.status_code == 200:
            for page in resp.json:
                uuid.UUID(page.pop('guid'))
            assert resp.json == data['pages']

    def test_put_webshop_static_pages_update(self, as_shopkeeper, webshop_guid, static_pages, faker):
        actual_pages = copy.deepcopy(static_pages)
        random.choice(actual_pages)['link'] = faker.uri()
        random.choice(actual_pages)['title'] = faker.sentence()
        random.choice(actual_pages)['inline'] = faker.boolean()
        assert actual_pages != static_pages

        put_data = Webshop.API.StaticPage.Schemas.Put(many=True).dump(actual_pages)
        r = as_shopkeeper.put(f'/webshops/{webshop_guid}/static_pages/', json=put_data)
        assert 200 == r.status_code, r.json
        assert actual_pages == r.json

    def test_put_webshop_static_pages_add(self, as_shopkeeper, webshop_guid, static_pages, faker):
        actual_pages = copy.deepcopy(static_pages)
        actual_pages.append(dict(title=faker.sentence(), link=faker.uri(), inline=faker.boolean()))
        r = as_shopkeeper.put(f'/webshops/{webshop_guid}/static_pages/', json=actual_pages)
        assert 200 == r.status_code, r.json
        # Add the new guid to the static page info
        actual_pages[-1]['guid'] = next(filter(lambda x: x['title'] == actual_pages[-1]['title'], r.json))['guid']
        assert actual_pages == r.json

    def test_put_webshop_static_pages_update_bad_guid(self, as_shopkeeper, webshop_guid, static_pages, faker):
        actual_pages = copy.deepcopy(static_pages)
        actual_pages[-1]['guid'] = get_random_uuid()
        r = as_shopkeeper.put(f'/webshops/{webshop_guid}/static_pages/', json=actual_pages)
        assert 409 == r.status_code, r.json
        assert 'Unknown guid' in str(r.data)
