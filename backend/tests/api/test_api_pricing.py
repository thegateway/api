# -*- coding: utf-8 -*-
"""
Test the api for global pricing
"""
from random import (
    choice,
    randint,
)

import pytest

from gwio.ext import SYSTEM_GUID
from gwio.models.pricing import Pricing


@pytest.fixture(scope='class')
def pricing_api_testdb(mock_aws, create_random_pricings):
    assert mock_aws

    pricings = list()
    for pricing in create_random_pricings(randint(2, 8)):
        pricing.save()
        pricings.append(pricing)

    yield pricings


@pytest.mark.usefixtures('pricing_api_testdb')
class TestDefaultPricingApi:
    def test_add_delete_pricing(self, as_admin, create_random_pricing):
        pricing = create_random_pricing()
        r = as_admin.post(f'/pricings/', json=Pricing.Schemas.Post().dump(pricing))
        assert 201 == r.status_code, f"response: {r.json}\npricing: {pricing}"
        assert str(SYSTEM_GUID) == r.json['owner']
        guid = r.json['guid']
        r = as_admin.delete(f'/pricings/{guid}/')
        assert 204 == r.status_code

    def test_list_pricings(self, as_admin, pricing_api_testdb):
        r = as_admin.get('/pricings/')
        assert 200 == r.status_code

    def test_update_pricings(self, as_admin, pricing_api_testdb):
        pricing = choice(pricing_api_testdb)
        cost = pricing.methods.cost.Schema().dump(pricing.methods.cost)
        r = as_admin.put(f'/pricings/{pricing.guid}/', json=dict(methods=dict(cost=cost)))
        assert 200 == r.status_code
        assert r.json['methods']['retail'] is None
