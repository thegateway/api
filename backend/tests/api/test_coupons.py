import random
import uuid

import pytest

import gwio.user
from gwio.logic.orders import types
from gwio.logic.packages.handler import (
    PackageAction,
    PackageState,
)
from gwio.logic.payments.handler import PaymentState
from gwio.models import (
    payment_method,
)
from gwio.models.coupons.conditions import CouponCondition
from gwio.models.coupons.promotions import (
    Promotion,
)
from gwio.models.modifiers import (
    Modifier,
    ModifierMethodType,
)
from gwio.models.products import Product
from gwio.models.user import UserData
from gwio.utils.notify import Notification
from ._fakes.coupons import (
    random_coupon_data,
    random_promotion_data,
)

User = gwio.user.User
OrderState = types.OrderState
PaymentMethod = payment_method.PaymentMethod


@pytest.fixture(scope='class')
def coupons_suite(create_random_product, customer_identity_guid):
    for i in range(random.randint(10, 20)):
        product = create_random_product()
        product.save()


@pytest.mark.usefixtures('coupons_suite')
class TestPromotion:
    @pytest.mark.usefixtures('mock_stripe')
    def test_coupon_from_order(
            self,
            create_order_via_api,
            create_random_promotion,
            webshop,
            customer_identity_guid,
            as_customer,
            as_shopkeeper,
            as_anonymous,
            mock_email,
            mock_fcm,
            mock_webpush,
            random_notification_subscription_payload,
            stripe_webhook_payload,
    ):
        # Subscribe user to receive email notifications
        payload = random_notification_subscription_payload(Notification.Type.FCM_PUSH.value)
        r = as_customer.post(f'/notifications/types', json=payload)
        assert r.status_code == 204

        order = create_order_via_api(
            payment_method=dict(type=PaymentMethod.API.Type.STRIPE),
        )

        promotion = create_random_promotion(conditions=list())
        promotion.save()
        assert promotion.benefit
        assert not promotion.conditions

        user = User.by_identity_id(customer_identity_guid)
        user.data.stripe_id = str(uuid.uuid4())
        user.data.save()

        user_data = user.data

        def check_coupons(*, user_data, new_coupon):
            coupon_guids = [x.guid for x in user_data.coupons]
            user_data.refresh()
            new_coupons = [x for x in user_data.coupons if x.guid not in coupon_guids]
            assert len(new_coupons) == (1 if new_coupon else 0)
            try:
                return new_coupons[0]
            except IndexError:
                return

        mock_fcm.reset_mock()
        r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{order.guid}/_place')
        assert r.status_code == 200, r.json
        assert OrderState(r.json['state']) == OrderState.PROCESSING
        assert PaymentState(r.json['payment']['state']) == PaymentState.COMPLETED
        assert PackageState(r.json['packages'][0]['state']) == PackageState.WAITING_PACKING

        # Check the coupon is found for user
        coupon = check_coupons(
            user_data=user_data,
            new_coupon=True,
        )

        # Check the promotion triggered and coupon was sent
        assert mock_fcm.notified == True
        assert [x for x in mock_fcm.notifications if x['message_title'].startswith('New coupon')]
        assert coupon.used == False

        modifier_schema = Modifier.Schema()
        # Checking all the benefits from promotion have been copied to the coupon
        assert modifier_schema.dump(coupon.benefit) == modifier_schema.dump(promotion.benefit)

        condition_schema = CouponCondition.Schema()
        # Checking all the benefits from promotion have been copied to the coupon
        not_found = 0
        for promotion_condition in promotion.conditions:
            promotion_condition = condition_schema.dump(promotion_condition)
            try:
                next(x for x in coupon.conditions if condition_schema.dump(x) == promotion_condition)
            except StopIteration:
                not_found += 1
        assert not not_found, f'{not_found}/{len(promotion.benefits)} coupons not found in coupon'

        mock_fcm.reset_mock()
        order.refresh()
        package = order.packages[0]
        r = as_shopkeeper.put(f'/webshops/{webshop.guid}/orders/{order.guid}/packages/{package.guid}/_{PackageAction.PACK}')
        assert r.status_code == 200, r.json
        assert OrderState(r.json['state']) == OrderState.PROCESSING
        assert PackageState(r.json['packages'][0]['state']) == PackageState.WAITING_DELIVERY

        r = as_shopkeeper.put(f'/webshops/{webshop.guid}/orders/{order.guid}/packages/{package.guid}/_{PackageAction.CONFIRM}')

        assert PackageState(r.json['packages'][0]['state']) == PackageState.DELIVERED
        assert OrderState(r.json['state']) == OrderState.COMPLETED

        assert mock_fcm.notified == False
        _ = check_coupons(
            user_data=user_data,
            new_coupon=False,
        )

    @pytest.mark.parametrize('modifier_method', list(ModifierMethodType))
    def test_apply_coupon(self, faker, as_customer, webshop, customer_identity_guid, customer_guid, modifier_method):
        # Making a new order and using the coupon from earlier
        product = random.choice(list(Product.scan(owner_guid=str(webshop.guid))))
        post_r = as_customer.post(f'/customers/{customer_identity_guid}/orders/', json=dict(
            customer_name=faker.name(),
            products=[
                dict(
                    guid=product.guid,
                    qty=2,
                    shop_guid=webshop.guid,
                )
            ]
        ))
        assert post_r.status_code == 201, post_r.json['message']

        # Creating the coupon
        user = User.by_identity_id(customer_identity_guid)
        is_override = modifier_method.value.startswith('override')
        user.data.coupons.append(random_coupon_data(
            benefit=dict(
                amount=faker.pydecimal(
                    # Making sure benefit amount makes sense for all methods
                    min_value=0 if is_override else None,
                    max_value=int(product.base_price.amount / 2) if is_override else -1,
                ),
                method=modifier_method,
            ),
            conditions=list(),
            used=False,
        ))
        user.data.save()
        coupon = user.data.coupons[-1:][0]
        assert coupon.used == False

        # Apply coupon to the order
        put_data = dict(
            coupon=coupon.guid,
        )
        put_r = as_customer.put(f'/customers/{customer_identity_guid}/orders/{post_r.json["guid"]}', json=put_data)
        assert put_r.status_code == 200, put_r.json

        # Check the coupon is applied correctly to the order
        coupon_guid = uuid.UUID(put_r.json['coupon']['guid'])
        assert coupon_guid == coupon.guid

        user_data = UserData.get(guid=customer_guid)
        used_coupon = next(x for x in user_data.coupons if x.guid == coupon_guid)

        assert used_coupon.used == True

        # Check the order wide discount is applied on every line
        for line in put_r.json['lines']:
            applied = False
            for modifier in line['price']['original']['modifiers']:
                coupon_dict = put_r.json['coupon']['benefit'].copy()
                if modifier_method.value.startswith('override'):
                    # Can not compare `amount` and `method` for overrides, because they are converted to "amount change" on order line modifiers
                    del coupon_dict['amount']
                    del coupon_dict['method']

                if all(modifier[attr_key] == attr_value for attr_key, attr_value in coupon_dict.items()):
                    applied = True
            assert applied, 'Coupon benefit not found on line'


@pytest.fixture(scope='class')
def promotions_suite(create_random_promotion):
    for _ in range(random.randint(4, 8)):
        promotion = create_random_promotion()
        promotion.save()


@pytest.mark.usefixtures('promotions_suite')
class TestPromotionAPI:
    def test_crud(self, as_admin):
        def test_changes_saved(response, guid=None):
            try:
                r = as_admin.get(f'/admin/promotions/{response.json["guid"]}')
                assert r.status_code < 300, r.json['message']
                assert response.json == r.json
            except TypeError:
                r = as_admin.get(f'/admin/promotions/{guid}')
                assert r.status_code == 404

        post_data = Promotion.Schemas.Post().dump(random_promotion_data())
        r = as_admin.post(f'/admin/promotions', json=post_data)
        assert r.status_code < 300, r.json['message']
        get_schema = Promotion.Schemas.Get()
        post_res_data = get_schema.dump(get_schema.load(r.json))
        for attr_name, attr_value in post_data.items():
            assert post_res_data[attr_name] == attr_value

        test_changes_saved(r)

        guid = r.json['guid']

        put_data = Promotion.Schemas.Put().dump(random_promotion_data())
        r = as_admin.put(f'/admin/promotions/{guid}', json=put_data)
        assert r.status_code < 300, r.json['message']
        pup_res_data = get_schema.dump(get_schema.load(r.json))
        for attr_name, attr_value in put_data.items():
            assert pup_res_data[attr_name] == attr_value

        test_changes_saved(r)

        r = as_admin.delete(f'/admin/promotions/{guid}')
        assert r.status_code < 300, r.json['message']

        test_changes_saved(r, guid)

    def test_list(self, as_admin):
        r = as_admin.get(f'/admin/promotions')
        assert r.status_code < 300
        promotions = list(Promotion.scan())
        assert len(promotions) > 1
        assert len(r.json) == len(promotions)
        guids = [uuid.UUID(x['guid']) for x in r.json]
        assert all(x.guid in guids for x in promotions)
