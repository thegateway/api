# coding=utf-8
import random
import uuid
from decimal import Decimal

import pytest
import stringcase
from marshmallow import EXCLUDE
from pytest_lazyfixture import LazyFixture

from gwio.core.utils.testing.helpers import (
    assert_error_message,
    request_redirected_uri,
)
from gwio.ext import SYSTEM_GUID
from gwio.ext.vat import Currency
from gwio.models.pricing import (
    Pricing,
    PricingMethod,
    PricingOperation,
)
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DEFAULT
from gwio.models.tags import Tag
from gwio.models.webshop_product import WebshopProduct
from .test_tags import random_tag_name


@pytest.fixture(scope='class')
def webshop_products_suite(products_suite, webshop, create_random_webshop):
    random_webshop = create_random_webshop()
    random_webshop.save()
    webshop_products = list()
    for shop in (random_webshop, webshop):
        for product in random.sample(products_suite, random.randint(2, 6)):
            webshop_product = WebshopProduct(product_guid=product.guid, webshop_guid=shop.guid)
            webshop_product.save()
            webshop_products.append(webshop_product)
    return webshop_products


@pytest.fixture(scope='class')
def test_suite(mock_aws, products_suite, webshop_products_suite, webshop):
    assert mock_aws
    assert webshop_products_suite
    webshop.save()


@pytest.fixture(scope='function')
def pricing_test_suite(mock_aws, products_suite, webshop_products_suite, webshop):
    assert products_suite
    assert mock_aws
    assert webshop_products_suite
    product = next(x for x in products_suite if x.type == 'default' and ' ' in x.name)

    system_pricing_method = PricingMethod(op=PricingOperation.SET, value=Decimal(10.00))
    webshop_pricing_method = PricingMethod(op=PricingOperation.SET, value=Decimal(20.00))
    system_pricing = Pricing(
        guid=Pricing.API.generate_guid(name=product.type, owner=SYSTEM_GUID),
        name=product.type,
        owner=SYSTEM_GUID,
        methods=dict(cost=system_pricing_method, retail=system_pricing_method))
    webshop_pricing = Pricing(
        guid=Pricing.API.generate_guid(name=product.type, owner=webshop.guid),
        name=product.type,
        owner=webshop.guid,
        methods=dict(cost=webshop_pricing_method, retail=webshop_pricing_method))

    webshop_product = WebshopProduct(
        product_guid=product.guid,
        webshop_guid=webshop.guid,
        pricing_guid=webshop_pricing.guid
    )

    system_pricing.save()
    webshop_pricing.save()
    webshop_product.save()
    webshop.save()
    return dict(
        webshop=webshop,
        product=product,
        system_pricing_method=system_pricing_method,
        webshop_pricing_method=webshop_pricing_method)


@pytest.fixture(scope='class')
def shop_products(random_products, faker, webshop_guid):
    products = list()
    other_products = list()
    for product in random_products(6, type=PRODUCT_TYPE_DEFAULT, name=faker.sentence(nb_words=4, variable_nb_words=False), owner_guid=webshop_guid):
        product.save()
        products.append(product)
    for product in random_products(6, type=PRODUCT_TYPE_DEFAULT, name=faker.sentence(nb_words=4, variable_nb_words=False), owner_guid=uuid.uuid4()):
        product.save()
        other_products.append(product)
    yield dict(shop_products=products, other_shop_products=other_products)
    for p in products:
        p.delete()
    for p in other_products:
        p.delete()


@pytest.fixture(scope='function')
def test_product(products_suite, webshop_guid):
    for p in products_suite:
        try:
            WebshopProduct.get(product_guid=p.guid, webshop_guid=webshop_guid)
        except WebshopProduct.DoesNotExist:
            product = p
            break
    assert product
    return product


@pytest.mark.usefixtures('test_suite')
class TestWebshopProductAPI:
    product_schema = Product.Schemas.Get()
    schema = WebshopProduct.Schemas.Get()
    base_path = '/webshops'

    def test_get_product_in_shop(self, as_admin, webshop_guid, webshop_products_suite):
        shop_guid, product_guid = next((x.webshop_guid, x.product_guid) for x in webshop_products_suite if webshop_guid == x.webshop_guid)
        r = as_admin.get(f'/webshops/{shop_guid}/products/{product_guid}')
        assert 200 == r.status_code
        assert product_guid == uuid.UUID(r.json['guid'])

        product = Product.by_guid(product_guid)
        # evaluated_product = product.API.Assessor.EvaluatedProduct.API.customer_marshal().dump(product.API.Assessor.evaluate(product))
        # evaluated_product = WebshopProduct.API.Assessor.evaluate(product=product, webshop_guid=webshop_product.webshop_guid)

        evaluated_product = Product.API.Assessor.EvaluatedProduct.Schemas.CustomerGet().dump(WebshopProduct.API.Assessor.evaluate(product=product,
                                                                                                                                  webshop_guid=shop_guid))

        # Product response doesn't content 'total'
        for value in evaluated_product['price'].values():
            try:
                del value['total']
            except (TypeError, KeyError):
                pass

        def assert_dict(expected, dictionary):
            for key, expected_value in expected.items():
                value = dictionary[key]
                try:
                    assert_dict(expected_value, value)
                except AttributeError:
                    if expected_value is None:
                        assert value is expected_value
                        continue

                    evaluated = '{0:.2f}'.format(value) if isinstance(value, float) or isinstance(value, Decimal) or isinstance(value, int) else value
                    expect = expected_value
                    if isinstance(expected_value, float) or isinstance(expected_value, Decimal) or isinstance(expected_value, int):
                        expect = '{0:.2f}'.format(expected_value)
                    assert expect == evaluated, key

        assert_dict(evaluated_product, r.json)

    def test_get_product_not_in_shop(self, as_admin, products_suite, webshop_products_suite, webshop_guid):
        product_guids = {product.guid for product in products_suite}
        wsp_guids = {product.product_guid for product in webshop_products_suite}
        guid = random.choice(list(product_guids - wsp_guids))
        r = as_admin.get(f'/webshops/{webshop_guid}/products/{guid}')
        assert 404 == r.status_code

    @pytest.mark.parametrize('as_role',
                             [pytest.lazy_fixture('as_admin'),
                              pytest.lazy_fixture('as_shopkeeper')],
                             ids=lambda x: x.name if isinstance(x, LazyFixture) else repr(x))
    def test_add_archive_unarchive_product(self, as_role, test_product, webshop_guid):
        # Redirect testing, should be removed when old endpoint is removed
        r = request_redirected_uri(as_role.post, f'/webshops/{webshop_guid}/products/', json=dict(products=[dict(product_guid=test_product.guid)]))

        assert 200 == r.status_code
        assert str(test_product.guid) == r.json['results'][0]['product_guid']
        assert 201 == r.json['results'][0]['status_code']

        # Adding it again should result in an error
        # Redirect testing, should be removed when old endpoint is removed
        r = request_redirected_uri(as_role.post, f'/webshops/{webshop_guid}/products/', json=[dict(product_guid=test_product.guid, pricing_guid=uuid.uuid4())])

        assert 200 == r.status_code
        assert str(test_product.guid) == r.json['results'][0]['product_guid']
        assert 409 == r.json['results'][0]['status_code']

        # Check the webshop product is not archived
        r = as_role.get(f'/webshops/{webshop_guid}/products/{test_product.guid}/')
        assert not r.json['archived']

        # Delete the product we just added
        # Redirect testing, should be removed when old endpoint is removed
        r = request_redirected_uri(as_role.delete, f'/webshops/{webshop_guid}/products/{test_product.guid}/')
        assert 204 == r.status_code

        # Check the webshop product is archived
        r = as_role.get(f'/webshops/{webshop_guid}/products/{test_product.guid}/')
        assert 200 == r.status_code
        assert r.json['archived']

        # And deleting it again should result be fine
        # Redirect testing, should be removed when old endpoint is removed
        r = request_redirected_uri(as_role.delete, f'/webshops/{webshop_guid}/products/{test_product.guid}/')
        assert 204 == r.status_code

        # Updating archived to False should unarchive product
        # Redirect testing, should be removed when old endpoint is removed
        r = request_redirected_uri(as_role.put, f'/webshops/{webshop_guid}/products/{test_product.guid}/', json=dict(archived=False))
        assert 200 == r.status_code

        # Check the webshop product is not archived anymore
        r = as_role.get(f'/webshops/{webshop_guid}/products/{test_product.guid}/')
        assert 200 == r.status_code
        assert r.json['archived'] is None

    def test_add_archived_product(self, as_admin, webshop_guid, test_product):
        stock_level = random.randint(1, 5)
        r = as_admin.post(f'/dashboard/webshops/{webshop_guid}/products/',
                          json=dict(products=[dict(product_guid=test_product.guid, archived=True, stock_level=stock_level)]))
        assert 200 == r.status_code
        assert str(test_product.guid) == r.json['results'][0]['product_guid']
        assert 201 == r.json['results'][0]['status_code']

        r = as_admin.get(f'/webshops/{webshop_guid}/products/{test_product.guid}')
        assert 200 == r.status_code
        assert r.json['archived']
        assert stock_level == r.json['stock_level']

    def test_add_not_archived_product(self, as_admin, webshop_guid, test_product):
        stock_level = random.randint(1, 5)
        r = as_admin.post(f'/dashboard/webshops/{webshop_guid}/products/',
                          json=dict(products=[dict(product_guid=test_product.guid, archived=False, stock_level=stock_level)]))
        assert 200 == r.status_code
        assert str(test_product.guid) == r.json['results'][0]['product_guid']
        assert 201 == r.json['results'][0]['status_code']

        r = as_admin.get(f'/webshops/{webshop_guid}/products/{test_product.guid}')
        assert 200 == r.status_code
        assert r.json['archived'] in [False, None]
        assert stock_level == r.json['stock_level']

    def test_update_stock_level(self, as_admin, webshop_guid, test_product):
        stock_level = random.randint(1, 5)
        r = as_admin.post(f'/dashboard/webshops/{webshop_guid}/products/',
                          json=dict(products=[dict(product_guid=test_product.guid, stock_level=stock_level)]))
        assert 200 == r.status_code
        assert str(test_product.guid) == r.json['results'][0]['product_guid']
        assert 201 == r.json['results'][0]['status_code']

        r = as_admin.get(f'/webshops/{webshop_guid}/products/{test_product.guid}')
        assert 200 == r.status_code
        assert stock_level == r.json['stock_level']

        new_stock_level = random.randint(7, 20)
        r = as_admin.put(f'/dashboard/webshops/{webshop_guid}/products/{test_product.guid}', json=dict(stock_level=new_stock_level))
        assert 200 == r.status_code
        assert new_stock_level == r.json['stock_level']

    @pytest.mark.parametrize('shop_guid', [uuid.uuid4(), None],
                             ids=lambda x: 'real shop' if x is None else 'fake shop')
    @pytest.mark.parametrize('product_guid', [uuid.uuid4(), None],
                             ids=lambda x: 'real product' if x is None else 'fake product')
    def test_delete_invalid_webshop(self, as_shopkeeper, shop_guid, product_guid, webshop_guid, webshop_products_suite):
        if shop_guid is None:
            shop_guid = next(x.webshop_guid for x in webshop_products_suite if webshop_guid != x.webshop_guid)
        if product_guid is None:
            try:
                product_guid = next(x.product_guid for x in webshop_products_suite if shop_guid == x.webshop_guid)
            except StopIteration:
                product_guid = next(x.product_guid for x in webshop_products_suite)

        # Redirect testing, should be removed when old endpoint is removed
        r = request_redirected_uri(as_shopkeeper.delete, f'/webshops/{shop_guid}/products/{product_guid}/')

        assert 403 == r.status_code

    @pytest.mark.parametrize('sent_data, message', [
        (dict(), '{"products": ["Missing data for required field."]}'),
    ])
    def test_create_invalid_data(self, as_admin, webshop, sent_data, message):
        r = request_redirected_uri(as_admin.post, f'/webshops/{webshop.guid}/products/', json=sent_data)
        assert 409 == r.status_code
        assert message == r.json['message']

    def test_update_webshop_product(self, as_admin, webshop_guid, webshop_products_suite):
        product_guid = next(x.product_guid for x in webshop_products_suite if webshop_guid == x.webshop_guid)
        # Redirect testing, should be removed when old endpoint is removed
        put_data = dict(
            pricing_guid=uuid.uuid4(),
            stock_level=random.randint(1, 10000),
        )
        r = request_redirected_uri(as_admin.put, f'/webshops/{webshop_guid}/products/{product_guid}/', json=put_data)
        assert r.status_code == 200, r.json

        wp = WebshopProduct.get(product_guid=product_guid, webshop_guid=webshop_guid)
        for name, value in put_data.items():
            assert getattr(wp, name) == value

    def test_for_sale_post_put(self, as_admin, test_product, webshop_guid):
        post_data = dict(
            products=[
                dict(
                    product_guid=test_product.guid,
                    for_sale=True,
                )
            ]
        )
        r = as_admin.post(f'/dashboard/webshops/{webshop_guid}/products/', json=post_data)
        assert 200 == r.status_code
        assert 201 == r.json['results'][0]['status_code']
        r = as_admin.get(f'/webshops/{webshop_guid}/products/{test_product.guid}')
        assert 200 == r.status_code
        assert r.json['for_sale']
        r = request_redirected_uri(as_admin.put, f'/webshops/{webshop_guid}/products/{test_product.guid}/', json=dict(for_sale=False))
        assert r.status_code == 200
        assert r.json['for_sale'] is False

    def test_select(self, as_admin, webshop_guid):
        # This test will not work if all products are in the webshop
        r = request_redirected_uri(as_admin.post, f'/webshops/{webshop_guid}/products/_select',
                                   json=dict(dsl=dict(query=dict(match=dict(name=' ')))))
        assert 200 == r.status_code

    def test_search(self, as_admin, webshop_guid):
        # This test will not work if all products are in the webshop
        r = request_redirected_uri(as_admin.post, f'/webshops/{webshop_guid}/products/_query',
                                   json=dict(dsl=dict(query=dict(match=dict(name=' ')))))
        assert 200 == r.status_code

    def test_search_webshop_products(self, as_admin, webshop_guid):
        # This test will not work if all products are in the webshop
        r = as_admin.post(f'/dashboard/webshop_products/_query',
                          json=dict(dsl=dict(query=dict(match=dict(name=' ')))))
        assert 200 == r.status_code
        for item in r.json['products']:
            for price_type in ['base', 'cost', 'final', 'original']:
                for price, check in [('amount', lambda x: isinstance(x, (Decimal, int, float))),
                                     ('currency', lambda x: Currency(x)),
                                     ('vat', lambda x: isinstance(x, (Decimal, int, float))),
                                     ('vat0', lambda x: isinstance(x, (Decimal, int, float))),
                                     ('vat_percent', lambda x: isinstance(x, (Decimal, int, float)))]:
                    assert check(item['price'][price_type]['unit'][price])

    def test_product_tags(self, as_admin, webshop_guid, products_suite):
        product_guid = next(x for x in products_suite if x.type in Product.API.valid_campaign_types).guid
        r = as_admin.post(f'{self.base_path}/{webshop_guid}/tags',
                          json=dict(name=random_tag_name(), type=Tag.Type.CATEGORY.name.lower()))
        assert 201 == r.status_code, r.json['message']
        tag_guid = r.json['guid']
        r = as_admin.put(f'/product_tags/{tag_guid}', json=dict(add=product_guid))
        assert 204 == r.status_code
        r = as_admin.get(f'/product_tags/{tag_guid}')
        assert 200 == r.status_code
        assert 1 == len(r.json)

    @pytest.mark.parametrize('shop, expected_status_code', [
        ('shop_products', 204),
        ('other_shop_products', 404),
    ])
    def test_product_tags_as_shopkeeper(self, as_shopkeeper, webshop_guid, products_suite, shop_products, shop, expected_status_code):
        product_guid = next(x for x in shop_products[shop] if x.type in Product.API.valid_campaign_types).guid
        r = as_shopkeeper.post(f'{self.base_path}/{webshop_guid}/tags',
                               json=dict(name=random_tag_name(), type=Tag.Type.CATEGORY.name.lower()))
        assert 201 == r.status_code, r.json
        tag_guid = r.json['guid']
        r = as_shopkeeper.put(f'/product_tags/{tag_guid}', json=dict(add=product_guid))
        assert expected_status_code == r.status_code
        r = as_shopkeeper.get(f'/product_tags/{tag_guid}')
        assert 200 == r.status_code
        expected_num_products = 0
        if 200 <= expected_status_code < 300:
            expected_num_products = 1
        assert expected_num_products == len(r.json)

    def _find_product_in_response(cls, as_role, response_json, product):
        if 'guid' in response_json:
            return [response_json]
        try:
            product_list = response_json['products']
        except KeyError:
            product_list = [response_json]

        assert product_list, 'Product not found in the response or on possible following _fetch requests'
        try:
            wsps = [x for x in product_list if uuid.UUID(x['guid']) == product.guid]
            return wsps
            # return next(x for x in product_list if uuid.UUID(x['guid']) == product.guid)
        except StopIteration:
            # Product not on the page, fetch next
            return cls._find_product_in_response(as_role, as_role.get(response_json['next']).json, product)

    @pytest.mark.parametrize('method, url, sent_data, pricing', [
        ('post', '/admin/products', dict(), 'S'),
        ('put', '/admin/products/{product_guid}', dict(), 'S'),
        ('get', '/dashboard/products/{product_guid}', None, 'S'),
        ('put', '/dashboard/webshops/{webshop_guid}/products/{product_guid}', dict(), 'W'),
        ('post', '/dashboard/products/_select', dict(dsl=dict(query=dict(match=dict(name=' ')))), 'S'),
        ('post', '/dashboard/products/_query', dict(dsl=dict(query=dict(match=dict(name=' ')))), 'S'),
        ('post', '/products/_query', dict(dsl=dict(query=dict(match=dict(name=' ')))), 'W'),
        ('get', '/webshops/{webshop_guid}/products/{product_guid}', None, 'W'),

    ])
    def test_returns_are_webshop_products(self, as_admin, pricing_test_suite, method, url, sent_data, pricing, faker):
        """ This test is meant to make sure all the generic products are returning Products
        and that all the webshop product endpoints are returning WebshopProducts """
        webshop = pricing_test_suite['webshop']
        product = pricing_test_suite['product']

        url = url.format(webshop_guid=webshop.guid, product_guid=product.guid)

        pricings = dict(
            S=pricing_test_suite['system_pricing_method'].value,
            W=pricing_test_suite['webshop_pricing_method'].value,
        )

        expected_price = pricings[pricing]

        if sent_data is not None:
            if not sent_data:
                sent_data = getattr(product.Schemas, stringcase.capitalcase(method))(unknown=EXCLUDE).dump(product)
                sent_data['name'] = faker.sentence(nb_words=4)
            res = getattr(as_admin, method)(url, json=sent_data)
        else:
            res = getattr(as_admin, method)(url)
        assert res.status_code in (200, 201)

        res_products = self._find_product_in_response(as_admin, res.json, product)

        for res_product in res_products:
            try:
                if uuid.UUID(res_product['shop_guid']) == webshop.guid:
                    break
            except KeyError:
                if len(res_products) == 1:
                    break
                raise

        actual_original_price = Decimal(res_product['price']['original']['unit']['vat0'])
        assert expected_price == actual_original_price
        actual_final_price = Decimal(res_product['price']['final']['unit']['vat0'])
        assert expected_price == actual_final_price

    def test_create_new_product(self, random_product, as_shopkeeper, webshop, create_random_tag):
        data = Product.Schemas.Post().dump(random_product)
        tag = create_random_tag()
        tag.save()
        data['tag_guids'] = [tag.guid]
        r = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=dict(products=[dict(product=data)]))
        assert r.status_code == 200, r.json
        assert 1 == len(r.json['results'])
        product = Product.by_guid(uuid.UUID(r.json['results'][0]['product']['guid']))
        assert webshop.guid == product.owner_guid
        assert tag.guid in [y.guid for y in product.tags]
        tag.refresh()
        assert product.guid in tag.product_guids

    def test_create_product_and_product_guid(self, random_product, as_shopkeeper, webshop, test_product):
        data = Product.Schemas.Post().dump(random_product)
        r = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=dict(
            products=[dict(
                product_guid=test_product.guid,
                stock_level=1,
                product=data,
            )],
        ))
        assert r.status_code == 409, r.json
        error_msg = 'Can not provide both `product` and `product_guid`'
        assert_error_message(
            response=r,
            expected=dict(
                products={0: dict(
                    product_guid=error_msg,
                    product=error_msg,
                )},
            ),
        )

    def test_create_no_product_or_product_guid(self, random_product, as_shopkeeper, webshop, test_product):
        r = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=dict(products=[dict()]))
        assert r.status_code == 409, r.json
        error_msg = 'Must provide either `product` or `product_guid`'
        assert_error_message(
            response=r,
            expected=dict(
                products={0: dict(
                    product_guid=error_msg,
                    product=error_msg,
                )},
            ),
        )
