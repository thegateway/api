# -*- encoding: utf-8 -*-
# pylint: disable=E1101
"""
Test the order handling logic

The purpose of this is to make sure logic can handle all the use-cases.
"""
import itertools
import random

import pytest
from dynamorm import DynaModel
from flask import Response
from marshmallow import (
    fields,
)
from moto.dynamodb2 import mock_dynamodb2

from gwio.ext.dynamorm import (
    FsmModel,
    GwioListField,
    adapted_dynamodel,
)
from gwio.logic.orders.handler import (
    OrderHandler,
    _notifications,
    _transitions,
)
from gwio.logic.orders.types import (
    OrderAction,
    OrderAssessment as OrdA,
    OrderNotificationType,
    OrderNotificationType as N,
    OrderState,
    OrderStatus,
)
from gwio.logic.packages.handler import (
    PackageHandler,
    PackageStatus,
)
from gwio.logic.payments.handler import (
    PaymentAction,
    PaymentHandler,
    PaymentState,
    PaymentStatus,
)
from gwio.models.orders import (
    Order,
    OrderTransitionEvent,
    Package,
)
from gwio.models.payment_attribute import Payment
from gwio.utils.fsm import (
    FsmAction,
    FsmAssessmentContext,
    FsmNotificationContext,
    FsmProcessingError,
    FsmState,
    FsmStatus,
)


def listify(value):
    if isinstance(value, list):
        return value
    return [value]


def find_transition(*, start_state, action, end_state):
    for transition in _transitions:
        if transition.src != start_state and start_state != FsmState._ANY:
            continue
        if transition.dst != end_state and end_state != FsmState._ANY:
            continue
        if action not in listify(transition.on) and action != FsmAction._ANY:
            continue
        return transition
    assert False, f'Could not find transition {start_state} + {action} -> {end_state}'


@adapted_dynamodel
class _TransitionTestOrder(FsmModel, DynaModel):
    """Fake dynamo table (OrderHandler checks against FsmModel type)"""
    current_state = None
    status = None

    # These are only for the unittesting
    assessments_done = None
    notifications_done = None

    class Table:
        name = 'order-handling-test'
        hash_key = 'guid'

    class API:
        _notifiers = dict([(x, lambda: True) for x in OrderNotificationType])

        @staticmethod
        def assess(model: '_TransitionTestOrder', action: OrderAction, assessment: OrdA, ctx: FsmAssessmentContext):
            model.assessments_done.append(assessment)

        @staticmethod
        def notify(model: '_TransitionTestOrder', action: OrderAction, notification: N, ctx: FsmNotificationContext):
            model.notifications_done.append(notification)

    class ProcessLogic:
        @staticmethod
        def processing(*, model, action, source):
            """Process logic for the processing state"""
            model.processing_called = True

    class Schema:
        guid = fields.UUID()
        _transition_history = GwioListField(OrderTransitionEvent)  # FSM adds values to this

    _fsm_handler = OrderHandler()

    def __init__(self):
        super().__init__()
        self.processing_called = False
        self.current_state = None
        self.status = dict()
        self.assessments_done = list()
        self.notifications_done = list()

    # Few helper functions to set/reset the status of this fake order so that
    # that the needed condition checkers work
    def set_conditions(self, status=None):
        self.status = dict()
        if not status:
            return
        for condition in listify(status):
            if condition != FsmStatus.ANY:
                self.status[str(condition)] = True

    def set_status(self, status):
        self.status[str(status)] = True

    def reset_status(self, status):
        self.status[str(status)] = False

    #  The properties that need to be defined for OrderHandler()
    @property
    def status_paid(self) -> bool:
        return self.status.get('paid', False)

    @property
    def status_need_review(self) -> bool:
        return self.status.get('need_review', False)

    @property
    def status_prepayment(self) -> bool:
        return self.status.get('prepayment', False)

    @property
    def status_invalid(self) -> bool:
        return self.status.get('invalid', False)

    @property
    def status_refunded(self) -> bool:
        return self.status.get('refunded', False)

    @property
    def status_delivered(self) -> bool:
        return self.status.get('delivered', False)


@pytest.fixture(scope='class')
def mocked_dynamo():
    dynamo_mock = mock_dynamodb2()
    dynamo_mock.start()
    assert not Order.exists()  # To make sure we are not actually messing with real dynamodb
    assert not _TransitionTestOrder.exists()
    Order.create_table()
    _TransitionTestOrder.create_table()
    yield
    dynamo_mock.stop()


def _test_all_conditions(status, model):
    # NOTE! again a useless looking test, but the purpose is to verify that all the
    #       conditions used by the order logic are present in Order model
    try:
        checker = status.condition_checker
        assert isinstance(getattr(model, checker),
                          property), f'{model.__class__.__name__} is missing "{checker}"'
    except ValueError:
        pass
    except AttributeError:
        assert False, f'{model.__name__} is missing "{checker}"'


@pytest.mark.use_fixtures('mocked_dynamo')
class TestOrderHandling:
    fsm = None

    @classmethod
    def setup_class(cls):
        cls.fsm = OrderHandler()

    @pytest.mark.parametrize('status', list(OrderStatus))
    @pytest.mark.parametrize('model', [Order, _TransitionTestOrder])
    def test_all_order_statuses(self, status, model):
        _test_all_conditions(status, model)

    def test_impossible_event(self):
        # Sending wrong events wont change the order state but should raise
        # OrderProcessingError exception
        order = _TransitionTestOrder()
        with pytest.raises(FsmProcessingError):
            # OrderAction._ANY cannot be used to define Transitions, so there should be no rule for this
            order.process(FsmAction._ANY)
        assert order.current_state == order._fsm_handler._initial_state

    @pytest.mark.parametrize('start_state, action, end_state, statuses', [
        (OrderState.START, OrderAction.CREATE, OrderState.CREATED, []),
        (OrderState.CREATED, OrderAction.UPDATE, OrderState.CREATED, []),
        (OrderState.CREATED, OrderAction.PLACE, OrderState.CREATED, [OrderStatus.INVALID]),
        (OrderState.COMPLETED, OrderAction.ARCHIVE, OrderState.ARCHIVED, []),
        (OrderState.COMPLETED, OrderAction.CANCEL, OrderState.CANCELLED, []),
    ])
    def test_transitions(self, start_state, action, end_state, statuses):
        for testfn, msg, conditions in [
            (lambda x: end_state == x.current_state, 'valid transition', statuses),
            (lambda x: end_state != x.current_state or not statuses, 'undefined transition',
             [s for s in OrderStatus if s not in statuses])]:
            order = _TransitionTestOrder()
            order.set_conditions(conditions)
            order.current_state = start_state
            order.process(action)
            assert testfn(order), msg

    def test_process_logic(self):
        order = _TransitionTestOrder()
        order.current_state = OrderState.CREATED
        order.process(OrderAction.PLACE)
        assert order.processing_called

    def test_transition_history(self):
        order = _TransitionTestOrder()
        order.current_state = OrderState.CREATED
        test_value = OrderState.ARCHIVED  # Using a state that won't appear for sure otherwise in the history
        order._transition_history.append(dict(state=test_value))
        order.process(OrderAction.PLACE)
        placed = order.current_state
        triggers = list(filter(lambda x: not x.startswith('to_'), self.fsm.get_triggers(order.current_state.transitions_state)))
        order.process(self.fsm.deserialise_action(random.choice(triggers)))
        assert test_value == order._transition_history[0].state  # Should be first in the list
        assert order.current_state == order._transition_history[-1].state  # Should be last in the list
        # It is not known how many internal states the FSM went through so it is enough to that the state is in the list
        assert any(placed == x.state for x in order._transition_history if not isinstance(x, str))
        assert len(order._transition_history) == 3

    def test_use_state_from_model(self):
        order = _TransitionTestOrder()
        order.current_state = OrderState.COMPLETED
        assert OrderState.COMPLETED == order.current_state

    def test_initial_does_not_override_existing_state(self):
        order = _TransitionTestOrder()
        order.current_state = OrderState.COMPLETED
        assert OrderState.COMPLETED == order.current_state

    # TODO: Remove?
    @pytest.mark.parametrize('start_state, action, end_state',
                             [item for pl in [itertools.product([n.via], listify(n.by), listify(n.state)) for n in _notifications] for item in pl])
    def test_notify_failure(self, start_state, action, end_state):
        def _just_fail(*args, **kwargs):
            raise RuntimeError('We just failed')

        transition = find_transition(start_state=start_state, action=action, end_state=end_state)

        order = _TransitionTestOrder()
        order.current_state = transition.src
        order.set_conditions(transition.status)
        # This is required because we are indirectly modifying class, not instance
        orig_notify = order.API.notify
        order.API.notify = _just_fail
        try:
            order.process(random.choice(listify(transition.on)))
            assert FsmState._FAILED == order.current_state
        finally:
            order.API.notify = orig_notify


class TestPaymentFsm:
    @classmethod
    def setup_class(cls):
        cls.fsm = PaymentHandler()

    @pytest.mark.parametrize('status', list(PaymentStatus))
    def test_all_payment_statuses(self, status):
        _test_all_conditions(status, Payment)

    @pytest.mark.parametrize('payment_type', list(Payment.API.Type))
    def test_no_state_change(self, app, create_test_order, create_random_payment_method, test_customer, webshop, customer_identity_id, payment_type):
        assert test_customer
        pm = create_random_payment_method(type=payment_type)
        pm.save()
        order = create_test_order(
            buyer_identity_id=customer_identity_id,
            shop_guid=webshop.guid,
            payment_method=pm.to_dict(),
            _current_state=OrderState.PROCESSING,
        )
        payment = order.payment_method.payment_method.new_payment_from_order(order=order)
        payment.current_state = PaymentState.WAITING_PAYMENT
        payment.amount = 0
        order.payments.append(payment)
        with app.application.test_request_context():
            app.application.preprocess_request()  # For log events to not break
            payment.process(PaymentAction.PAY)
            app.application.process_response(Response())  # To make sure no issues with saving log events
        assert PaymentState.WAITING_PAYMENT == payment.current_state


class TestPackageFsm:
    @classmethod
    def setup_class(cls):
        cls.fsm = PackageHandler()

    @pytest.mark.parametrize('status', list(PackageStatus))
    def test_all_package_statuses(self, status):
        _test_all_conditions(status, Package)
