# -*- coding: utf-8 -*-
"""
Testing the vat
"""
from decimal import Decimal

import pytest

from gwio.environment import Env
from gwio.ext.vat import (
    Currency,
    Price,
    Vat,
    VatPrice,
)


def test_currency():
    assert Currency.EUR.abbr == 'EUR'
    assert str(Currency.EUR) == '€'
    assert Currency.GBP.abbr == 'GBP'
    assert str(Currency.GBP) == '£'


def test_price_parse():
    price0 = Price('10.00 EUR')
    price2 = Price(price0)
    price3 = Price(10, currency=Currency.EUR)
    price4 = Price('10', currency=Currency.EUR)
    assert '<Price(10.00 EUR)>' == repr(price0)
    assert price0 == price2 == price3 == price4


def test_price_currency_conversion():
    with pytest.raises(RuntimeError):
        _ = Price('10.00 USD', currency=Currency.EUR)


def test_price_sum():
    assert Price("22.00 GBP") == Price(10.0, currency=Currency.GBP) + Price("12.00 GBP")
    with pytest.raises(RuntimeError):
        Price("12.00 USD") + Price("10.00 EUR")

    with pytest.raises(RuntimeError):
        Price("12 EUR") + Price("12.00 GBP")

    assert Price(24) == Price(14, currency=Env.DEFAULT_CURRENCY) + 10


def test_price_mult():
    assert Price("22.00 GBP") == Price(11.0, currency=Currency.GBP) * 2
    assert Price("11.00 GBP") == Price(22.0, currency=Currency.GBP) / 2


def test_vat_error_category():
    assert '<Vat(ERR)>' == repr(Vat('ERROR'))
    assert '<Vat(ERR)>' == repr(Vat('kukkakaali'))
    assert '<Vat(0)>' == repr(Vat(None))


def test_vat_conversion():
    assert '<VatPrice(<Price(12.40 USD)>,<Vat(24)>)>' == repr(VatPrice.from_price_and_vat('10.00 USD', vat=Vat(0)) @ Vat('VAT24'))


def test_default_vat_price_currency():
    price = VatPrice.from_price_and_vat("3 PLN")
    assert price.vat_percent == Decimal(0)
    assert f"3.00 {Env.DEFAULT_CURRENCY} VAT0" == str(price)


def test_vatprice_sum():
    add_price = VatPrice.from_price_and_vat("10.00 USD", vat=Vat(12)) + VatPrice.from_price_and_vat("2 USD", vat=Vat(12))
    assert VatPrice.from_price_and_vat("12.00 USD", vat=Vat(12)) == add_price
    sub_price = VatPrice.from_price_and_vat("10.00 EUR", vat=Vat(12)) - VatPrice.from_price_and_vat("12 EUR", vat=Vat("12"))
    assert VatPrice.from_price_and_vat("-2.00 EUR", vat=Vat('vat12')) == sub_price

    with pytest.raises(TypeError, match="VAT categories don't match \\(VAT12/VAT0\\)"):
        _ = VatPrice.from_price_and_vat("10.00 USD", vat=Vat(12)) + VatPrice.from_price_and_vat("2 USD", vat=Vat(0))

    with pytest.raises(TypeError, match="VAT categories don't match \\(VAT12/VAT0\\)"):
        _ = VatPrice.from_price_and_vat("10.00 USD", vat=Vat(12)) - VatPrice.from_price_and_vat("2 USD", vat=Vat(0))


def test_vatprice_mult():
    assert VatPrice.from_price_and_vat("42.00 USD", vat=Vat(12)) == VatPrice.from_price_and_vat("21.00 USD", vat=Vat(12)) * 2
    assert VatPrice.from_price_and_vat("20.00 USD", vat=Vat(12)) == VatPrice.from_price_and_vat("120.00 USD", vat=Vat(12)) / 6

    with pytest.raises(TypeError, match='conversion from VatPrice to Decimal is not supported'):
        _ = VatPrice.from_price_and_vat("10.00 USD", vat=Vat(12)) * VatPrice.from_price_and_vat("2 USD", vat=Vat(0))


def test_vatprice_comparisons():
    assert VatPrice.from_price_and_vat("10 USD", vat=22) < VatPrice.from_price_and_vat("10 USD", vat=0)
    assert VatPrice.from_price_and_vat("10 USD", vat=0) > VatPrice.from_price_and_vat("10 USD", vat=8)
    assert VatPrice.from_price_and_vat("12.40 USD", vat=24) @ Vat(0) == VatPrice.from_price_and_vat("10 USD", vat=0)
    assert VatPrice.from_price_and_vat("12.40 USD", vat=12) != VatPrice.from_price_and_vat(Price(12.40, currency='USD'), vat=22)

    with pytest.raises(RuntimeError):
        _ = VatPrice.from_price_and_vat("12.00 USD", vat=Vat(12)) == VatPrice.from_price_and_vat("12.00 EUR", vat=Vat(12))


def test_vatprice_properties():
    assert Price("2.40 USD") == VatPrice.from_price_and_vat("12.40 USD", vat=Vat(24)).included_vat
    assert Price("10.00 GBP") == VatPrice.from_price_and_vat("10.80 GBP", vat=8).without_vat
