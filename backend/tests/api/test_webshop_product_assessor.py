# -*- coding: utf-8 -*-
from decimal import Decimal

import pytest

from gwio.ext import SYSTEM_GUID
from gwio.models.pricing import Pricing, PricingError, PricingMethod, PricingOperation
from gwio.models.webshop_product import WebshopProduct


@pytest.fixture(scope='class')
def webshop_product(create_random_product):
    product = create_random_product(type='testing')
    yield product


@pytest.fixture(scope='class')
def override_product(create_random_product):
    product = create_random_product(type='testing')
    yield product


@pytest.fixture(scope='class')
def webshop_records(webshop, random_product, webshop_product, override_product, faker):
    # We need the following set of pricings to see if that everything works
    # for one product there should by system owned "default" pricing (this is for the random product)
    # for testing the default override we need two pricings, one for system and for webshop
    # and for the final product specific override we need system default, webshop default and product
    # specific.
    # The identification of applied pricing is determined simply by the price e.g.
    # price 10 -> used system default
    # price 20 -> used webshop default
    # price 30 -> used product specific setting

    # Create the pricings
    set_10_method = PricingMethod(op=PricingOperation.SET, value=Decimal(10.00))
    set_20_method = PricingMethod(op=PricingOperation.SET, value=Decimal(20.00))
    set_30_method = PricingMethod(op=PricingOperation.SET, value=Decimal(30.00))
    system_pricing = Pricing(guid=Pricing.API.generate_guid(name=override_product.type, owner=SYSTEM_GUID),
                             name=override_product.type, owner=SYSTEM_GUID,
                             methods=dict(cost=set_10_method, retail=set_10_method))
    webshop_pricing = Pricing(
        name=override_product.type,
        owner=webshop.guid,
        methods=dict(
            cost=set_20_method,
            retail=set_20_method,
        ),
    )
    name = faker.name()
    single_pricing = Pricing(
        name=name,
        owner=webshop.guid,
        methods=dict(
            cost=set_30_method,
            retail=set_30_method,
        ),
    )

    # Add the products to have webshop specific pricing to be sold in the webshop
    wsp_default = WebshopProduct(product_guid=webshop_product.guid, webshop_guid=webshop.guid)
    wsp_override = WebshopProduct(product_guid=override_product.guid, webshop_guid=webshop.guid, pricing_guid=single_pricing.guid)
    return [system_pricing, webshop_pricing, single_pricing, wsp_default, wsp_override]


@pytest.fixture(scope='class')
def test_suite(mock_aws, webshop, random_product, webshop_product, override_product, webshop_records):
    assert mock_aws
    webshop.save()
    random_product.save()
    webshop_product.save()
    override_product.save()
    for record in webshop_records:
        record.save()
    return


@pytest.mark.usefixtures('test_suite')
class TestWebshopPricing:
    def test_random_product_pricing(self, webshop, random_product):
        with pytest.raises(PricingError):
            ep = WebshopProduct.API.Assessor.evaluate(webshop_guid=webshop.guid, product_guid=random_product.guid)

    def test_default_webshop_pricing(self, webshop, webshop_product):
        ep = WebshopProduct.API.Assessor.evaluate(webshop_guid=webshop.guid, product_guid=webshop_product.guid)
        assert Decimal(20) == ep.price.original.unit.without_vat.value

    def test_override_webshop_pricing(self, webshop, override_product):
        ep = WebshopProduct.API.Assessor.evaluate(webshop_guid=webshop.guid, product_guid=override_product.guid)
        assert Decimal(30) == ep.price.original.unit.without_vat.value
