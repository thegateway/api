# -*- coding: utf-8 -*-
import datetime
import random
from decimal import Decimal
from functools import reduce

import pytest

from gwio.core.utils.testing.helpers import request_redirected_uri
from gwio.logic.orders.types import (
    OrderAction,
    OrderAssessment,
    OrderState,
)
from gwio.models.modifiers import (
    ModifierLineSource,
    ModifierMethodType,
)
from gwio.models.products import Product
from gwio.models.tags import (
    Tag,
)
from gwio.models.webshop_product import WebshopProduct


def best_valid_tag(tags):
    def better_valid_tag(tag, tag2):
        try:
            if tag.data.method != tag2.data.method:
                raise ValueError('Tags with different method, could not compare which gives biggest discount')
        except AttributeError:
            pass
        tag = tag if tag.active else None
        if tag2.active:
            try:
                tag = tag if tag.data.amount < tag2.data.amount else tag2
            except AttributeError:
                tag = tag2
        return tag

    return reduce(better_valid_tag, tags)


@pytest.fixture(scope='function')
def products_suite(random_products):
    products = random_products(random.randint(5, 10))
    for item in products:
        item.save()
    return products


@pytest.fixture(scope='function')
def shops_selection(products_suite, webshop):
    wps = list()
    for product in random.sample(list(products_suite), k=random.randint(2, 5)):
        product.save()
        wp = WebshopProduct(product_guid=product.guid, webshop_guid=webshop.guid)
        wp.save()
        wps.append(wp)
    yield wps
    for wp in wps:
        wp.delete()
        Product.by_guid(wp.product_guid).delete()


@pytest.fixture(scope='function')
def ordered_products(shops_selection):
    return random.sample(list(shops_selection), k=2)


@pytest.fixture(scope='function')
def campaign_tags(mock_aws, faker, webshop):
    assert mock_aws
    old_data = dict(name=faker.name(),
                    owner=webshop.guid,
                    type=Tag.Type.CAMPAIGN,
                    duration=dict(start=datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=2),
                                  end=datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1)),
                    data=dict(desc=faker.name(), method=ModifierMethodType.PERCENTAGE, amount=Decimal('-50')))
    higher_discount_data = dict(name=faker.name(),
                                owner=webshop.guid,
                                type=Tag.Type.CAMPAIGN,
                                duration=dict(start=datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1),
                                              end=datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=1)),
                                data=dict(desc=faker.name(), method=ModifierMethodType.PERCENTAGE, amount=Decimal('-25')))
    lower_discount_data = dict(name=faker.name(),
                               owner=webshop.guid,
                               type=Tag.Type.CAMPAIGN,
                               duration=dict(start=datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1),
                                             end=datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=1)),
                               data=dict(desc=faker.name(), method=ModifierMethodType.PERCENTAGE, amount=Decimal('-15')))
    tags = [Tag(guid=Tag.API.generate_guid(name=x['name'], owner=x['owner']), **x) for
            x in (old_data, higher_discount_data, lower_discount_data)]
    for tag in tags:
        tag.save()
    yield tags
    for tag in tags:
        tag.delete()


@pytest.fixture(scope='function')
def campaign_products(ordered_products, campaign_tags):
    wps = random.choice(list(ordered_products))
    wps.save()
    for tag in campaign_tags:
        wps.product.add_tag(tag)
        tag.save()
    wps.product.save()
    yield [wps.product]
    wps.delete()


@pytest.fixture(scope='function')
def test_order(mock_aws, create_test_order,
               webshop, products_suite, shops_selection, ordered_products,
               campaign_tags, campaign_products, customer_identity_id):
    assert mock_aws
    webshop.save()

    mapped = Product.API.Assessor.EvaluatedProduct.ProductLine.mapped
    evaluate = WebshopProduct.API.Assessor.evaluate
    products = [mapped(evaluate(product_guid=product.product_guid, webshop_guid=webshop.guid)) for product in
                ordered_products]
    return create_test_order(
        _current_state=OrderState.CREATED,
        product_lines=products,
        shop_guid=webshop.guid,
        buyer_identity_id=customer_identity_id)


@pytest.fixture(scope='function')
def product_campaign_price_suite(mock_aws, campaign_tags, products_suite, campaign_products):
    assert mock_aws

    return dict(
        tags=campaign_tags,
        best_valid_tag=best_valid_tag(campaign_tags))


@pytest.mark.usefixtures('mock_aws')
class TestOrderedCampaignProduct:
    def test_product_campaign_modifier(self, campaign_tags, campaign_products, faker, webshop):
        # Two active campaigns for the product
        product = campaign_products[0]
        expected_tag = best_valid_tag(campaign_tags)
        assert expected_tag.product_guids
        for campaign_product_guid in expected_tag.product_guids:
            campaign_product = Product.by_guid(campaign_product_guid)
            assert expected_tag.data.method == campaign_product.campaign_modifier.method
            assert expected_tag.data.amount == campaign_product.campaign_modifier.amount

        # Three active campaigns for the product
        new_discount_data = dict(name=faker.name(),
                                 owner=webshop.guid,
                                 type=Tag.Type.CAMPAIGN,
                                 duration=dict(start=datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1),
                                               end=datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=1)),
                                 data=dict(desc=faker.name(), method=ModifierMethodType.PERCENTAGE, amount=Decimal('-35')))
        tag = Tag(guid=Tag.API.generate_guid(name=new_discount_data['name'], owner=new_discount_data['owner']), **new_discount_data)
        tag.add_product(product)
        tag.save()
        product.save()
        assert 2 < len(product.campaigns())
        assert new_discount_data['data']['method'] == product.campaign_modifier.method
        assert new_discount_data['data']['amount'] == product.campaign_modifier.amount

        # One active campaign for the product
        for pt in list(product.tags):
            if tag.guid != pt.guid:
                pt.tag.remove_product(product)

        assert 1 == len(product.campaigns())
        assert new_discount_data['data']['method'] == product.campaign_modifier.method
        assert new_discount_data['data']['amount'] == product.campaign_modifier.amount
        product.remove_tag(tag)
        product.save()
        tag.delete()

    def test_ordered_campaign_product(self, test_order, campaign_products):
        assert test_order
        # Making sure the order being tested does not already have campaign modifiers so that the test actually tests assess adding the campaign modifier
        assert not [y for x in test_order.product_lines for y in x.price.original.modifiers if y.source == ModifierLineSource.CAMPAIGN]
        test_order.API.assess(model=test_order, action=OrderAction.PLACE, assessment=OrderAssessment.PRICES)
        test_order.save()  # To make sure pynamodb validates the values
        guids = [x.guid for x in campaign_products if x.campaign_modifier is not None]

        matched_line_modifiers = []
        for line in test_order.product_lines:
            for modifier in line.price.original.modifiers:
                if line.guid in guids and modifier.source == ModifierLineSource.CAMPAIGN:
                    matched_line_modifiers.append(modifier)
        assert 1 == len(matched_line_modifiers)

    def test_product_api_campaign_price(self, as_admin, product_campaign_price_suite):
        expected_tag = product_campaign_price_suite['best_valid_tag']
        product = Product.by_guid(random.choice(expected_tag.product_guids))
        r = request_redirected_uri(as_admin.get, f"/products/{product.guid}")
        ep = Product.API.Assessor.evaluate(product)
        ep['price']['final'] = ep.price.original  # This is avoid calc_fixed_modifiers applying the discount on already dicounted price
        fixed_modifiers = expected_tag.data.calc_fixed_modifiers(ep)
        r_original = r.json['price']['original']['unit']
        r_modifier_amount = round(Decimal(r.json['price']['original']['campaign_modifier']['fixed_modifiers']['unit']['amount']), 2)
        assert fixed_modifiers.unit.amount == r_modifier_amount
        expected_final = round(Decimal(r_original['amount']) + fixed_modifiers.unit.amount, 2)

        # The calculations use the exact values, but the response is returning everything rounded to 2 decimals
        r_final_unit_amount = round(Decimal(r.json['price']['final']['unit']['amount']), 2)
        assert expected_final + Decimal('0.01') >= r_final_unit_amount
        assert expected_final - Decimal('0.01') <= r_final_unit_amount
