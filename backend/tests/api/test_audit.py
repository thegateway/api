import random
import uuid

import pytest
from gwio.core.cw_logging import Event
from marshmallow import (
    fields,
)

import gwio.user
from gwio.core import cw_logging
from gwio.core.utils.testing._fixtures.aws import MockedEventsStore
from gwio.ext.marshmallow import GwioSchema

User = gwio.user.User
LogEventType = cw_logging.LogEventType
AuditRequestsUtil = cw_logging.AuditRequestsUtil


@pytest.mark.usefixtures('mock_aws')
class TestCWEvent:

    @pytest.mark.parametrize('user_id', [uuid.uuid4()])
    def test_web_event(self, app, user_id, faker):
        with app.application.test_request_context():
            from flask import (
                g,
            )
            MockedEventsStore._events = []

            g.aws_request_id = uuid.uuid4()
            g.aws_event_source = 'arn:partition:service:region:account-id:resource-type/resource-id'

            g.user = User(guid=user_id)
            Event(
                subject=g.user,
                obj=g.user,
                event_type=random.choice(list(LogEventType)),
            ).emit()
            assert MockedEventsStore._events

    def test_event(self, app, mock_aws, faker):
        class User:
            guid = uuid.uuid4()

        class TestOrder:
            guid = uuid.UUID(int=42)

            class Schema(GwioSchema):
                guid = fields.UUID()

        user = User()
        order = TestOrder()
        with app.application.test_request_context():
            from flask import g

            g.aws_request_id = uuid.uuid4()
            g.aws_event_source = 'arn:partition:service:region:account-id:resource-type/resource-id'

            util = AuditRequestsUtil()
            util.audit_event = Event(
                obj=order,
                event_type=random.choice(list(LogEventType)),
                subject=user,
                data=dict(foo="bar"),
            )
            util.audit_response = app.application.response_class()
            MockedEventsStore._events = []
            util.save()
            new_events = MockedEventsStore._events
            assert len(new_events) == 1
            assert new_events[0]
