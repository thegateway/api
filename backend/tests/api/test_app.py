import pytest

from gwio.environment import Env


@pytest.mark.usefixtures('mock_aws')
class TestAPP:
    def test_root_redirect(self, as_anonymous):
        res = as_anonymous.get('/')
        assert res.status_code == 302

    def test_version_header(self, as_anonymous):
        res = as_anonymous.get('/')
        header = res.headers['X-Gateway-API-Version']
        assert header
        version, definition = header.split()
        assert version
        assert definition == f'{Env.VERTICAL}/{Env.APPLICATION_ENVIRONMENT}'

    def test_openapispec(self, as_anonymous):
        res = as_anonymous.get('/swagger.json')
        assert res.status_code == 200
        assert res.content_type == 'application/json'
        assert isinstance(res.json, dict)
        for key in ('openapi', 'info', 'paths'):
            assert res.json[key]
