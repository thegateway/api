# -*- coding: utf-8 -*-
"""Contains tests related to customers."""
import json

import pytest

from gwio.environment import Env
from ._mocks import MockedCoreCognitoIdpClient

VERTICAL = Env.VERTICAL


@pytest.fixture()
def mock_customer_pool(mocker):
    mocker.patch('gwio.api.admin.admin.app.customer_pool', wraps=MockedCoreCognitoIdpClient())
    yield


@pytest.mark.usefixtures('mock_customer_pool', 'mock_aws')
class TestCustomersAPI:
    @pytest.mark.parametrize(('client', 'expected_status_code'), [
        (pytest.lazy_fixture('as_admin'), 200),
        (pytest.lazy_fixture('as_shopkeeper'), 200 if VERTICAL == 'imeds' else 403),
        (pytest.lazy_fixture('as_customer'), 403)], ids=('admin', 'shopkeeper', 'customer'))
    def test_get_customers(self, client, expected_status_code):
        resp = client.get('/customers/')
        assert resp.status_code == expected_status_code

        if expected_status_code == 200:
            data = json.loads(resp.data)
            assert set({'next', 'customers'}) == set(data)
            next_url = data['next']
            while True:
                next_page_response = client.get(next_url)
                assert resp.status_code == 200
                next_page_data = json.loads(next_page_response.data)
                if next_page_data['next'] is None:
                    break
                next_url = next_page_data['next']
