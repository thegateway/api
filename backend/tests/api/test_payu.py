import datetime
import hashlib
import json
import random
import uuid
from decimal import Decimal

import pycountry
import pytest

from gwio.core.utils.payu.payu import (
    PayuOrder,
    Payu,
    PayuOrderStatus,
    PayuSubmerchant,
)
from gwio.core.utils.testing.helpers import request_redirected_uri
from gwio.environment import Env
from gwio.ext.vat import Currency
from gwio.logic.orders.types import OrderState
from gwio.logic.payments.handler import PaymentState
from gwio.models.orders import Order


@pytest.fixture(scope='class')
def random_submerchant(mock_payu):
    def f(**kwargs):
        mock_payu['payu_db'].add_submerchant({
            **dict(
                submerchant_id=uuid.uuid4(),
                customerVerificationStatus="Verified",
                name="Example Company",
                taxId="123123123",
                regon="123123123",
                balance=dict(availableAmount=random.randint(1000, 10000),
                             totalAmount=random.randint(10000, 100000))
            ), **kwargs})

    return f


@pytest.mark.usefixtures('mock_payu')
class TestPayu:
    def test_create_payu_order(self):
        order = PayuOrder._create_order(
            notify_url='https://payutest.dev.gwapi.eu/payu/notify',
            # card_token="TOK_XATB7DF8ACXYTVQIPLWTVPFRKQE",
            card_token=None,
            amount=random.randint(500, 2500),
            currency=Currency.PLN,
            customer_ip='127.0.0.1',
            continue_url='https://atestdomainthatdoesnotexist.com/',
            description='Test Order #1',
            pos_id='368189',
            order_guid=uuid.uuid4()
        )

        order2 = PayuOrder.get(order.order_id)
        response = order2.cancel()
        assert order2.order_id == response['orderId']

    def test_create_payu_order_multiple_shops(self, customer_identity_id):
        order = PayuOrder._create_submerchants_order(
            notify_url='https://payutest.dev.gwapi.eu/payu/notify',
            # card_token="TOK_XATB7DF8ACXYTVQIPLWTVPFRKQE",
            buyer_id=customer_identity_id,
            card_token=None,
            # "submerchant_{A,B,C} are the test merchants in PayU sandbox
            amounts={f'submerchant_{var}': dict(amount=random.randint(500, 2500), fee=random.randint(100, 500)) for var in ('A', 'B', 'C')},
            total_amount=random.randint(7500, 10000),
            currency=Currency.PLN,
            customer_ip='127.0.0.1',
            continue_url='https://atestdomainthatdoesnotexist.com/',
            description='Test Order #1',
            pos_id='341009',
            order_guid=uuid.uuid4()
        )

        order2 = PayuOrder.get(order.order_id)
        response = order2.cancel()
        assert order2.order_id == response['orderId']

    def test_get_merchant_registration_form(self, webshop):
        url = Payu.get_register_form_url(webshop.guid)
        assert url.startswith('http')

    def test_get_merchant_status(self, webshop, random_submerchant):
        status = PayuSubmerchant.get(webshop.guid).status()
        assert status['created'] is False

        random_submerchant(submerchant_id=webshop.guid)
        status = PayuSubmerchant.get(webshop.guid).status()
        assert status['created'] is True


@pytest.mark.usefixtures('mock_payu')
class TestPayuApi:
    def test_get_merchant_status(self, webshop, as_shopkeeper, random_submerchant):
        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/payu/merchant_status')
        assert 200 == res.status_code
        assert res.json['created'] == False
        assert res.json['verified'] == False

        random_submerchant(submerchant_id=webshop.guid)
        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/payu/merchant_status')
        assert 200 == res.status_code
        assert res.json['created'] == True
        assert res.json['verified'] == True

    def test_get_merchant_registration_form(self, webshop, as_shopkeeper):
        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/payu/registration_form_link')
        assert 200 == res.status_code
        assert res.json['link_url'].startswith('http')

    def test_get_merchant_balance(self, webshop, as_shopkeeper, random_submerchant):
        random_submerchant(submerchant_id=webshop.guid)
        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/payu/balance')
        assert 200 == res.status_code
        assert res.json['available_amount'] > 1
        assert res.json['total_amount'] >= res.json['available_amount']
        assert res.json['amount_available_for_payout'] > 1

    def test_submerchant_payuout(self, webshop, as_shopkeeper, random_submerchant):
        random_submerchant(submerchant_id=webshop.guid)

        res = as_shopkeeper.get(f'/webshops/{webshop.guid}/payment_methods/payu/balance')
        assert 200 == res.status_code
        pre_amount_available_for_payout = res.json['amount_available_for_payout']

        cents = Decimal('0.01')
        payload = dict(amount=Decimal(random.uniform(1.0, 5.0)).quantize(cents),
                       currency=pycountry.currencies.get(alpha_3='PLN').alpha_3)
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/payment_methods/payu/payout', json=payload)
        assert 200 == res.status_code, res.json
        assert res.json['payout']['amount'] > 1
        assert res.json['payout']['payout_id']
        assert not res.json['payout']['full_payout']
        assert Decimal(res.json['balance']['amount_available_for_payout']).quantize(cents) == \
               Decimal(pre_amount_available_for_payout).quantize(cents) - Decimal(payload['amount']).quantize(cents)


@pytest.mark.usefixtures('mock_payu')
class TestPayuPayment:
    def test_payu_payment(self, random_products, as_shopkeeper, as_customer, as_anonymous, webshop, payment_method_payu, customer_identity_guid,
                          create_random_delivery_method):
        # WTF? webshop fixture saves this?
        webshop.save()
        # Create order
        products = []
        for product in random_products(3, owner_guid=webshop.guid):
            product.save()
            products.append(dict(product_guid=product.guid))
        res = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=products)
        assert 200 == res.status_code

        res = as_customer.post(f'/customers/{customer_identity_guid}/orders',
                               json=dict(customer_name='Mr. Test Order Man #1',
                                         products=[dict(guid=p['product_guid'], shop_guid=webshop.guid, qty=random.randint(1, 3)) for p in products]))
        assert 201 == res.status_code, res.json
        order_guid = res.json['guid']

        delivery_method = create_random_delivery_method(owner_guid=uuid.UUID(int=0))
        delivery_method.save()
        update_data = dict(
            payment_method=dict(
                guid=payment_method_payu.guid,
                continue_url='http://notreallyadomainhereeither.com/test'),
            delivery_method=delivery_method.guid
        )

        res = as_customer.put(f"/customers/{customer_identity_guid}/orders/{order_guid}/", json=update_data)
        assert 200 == res.status_code, res.json

        # Place order
        res = as_customer.put(f"/customers/{customer_identity_guid}/orders/{res.json['guid']}/_place")
        assert res.status_code == 200, res.json

        order = Order.get(guid=uuid.UUID(res.json['guid']))
        assert order.current_state == OrderState.PROCESSING
        assert order.payment_data['state'] == PaymentState.NOT_STARTED
        assert order.payment_data['amount'] == order.summary.price

        # Send webhook
        payload = json.dumps(dict(order=dict(extOrderId=str(order.guid),
                                             status=PayuOrderStatus.COMPLETED.name),
                                  localReceiptDateTime=datetime.datetime.now(tz=datetime.timezone.utc).isoformat(),
                                  properties=list()))
        signature_input = payload + Env.PAYU_SECOND_KEY
        signature = hashlib.md5(signature_input.encode('utf-8')).hexdigest()
        openpayu_header = ';'.join([f'{k}={v}' for k, v in dict(sender='checkout', signature=signature, algorithm='MD5', content='DOCUMENT').items()])
        res = as_anonymous.post('/payu/webhook', data=payload,
                                headers={'OpenPayu-Signature': openpayu_header,
                                         'Content-Type': 'application/json'})
        assert 200 == res.status_code, res.json

        order.refresh()
        assert PaymentState(order.payment_data['state']) == PaymentState.COMPLETED
