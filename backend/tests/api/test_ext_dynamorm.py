import random

import pytest
from marshmallow import ValidationError

from gwio.ext.vat import Vat
from gwio.models.products import Product


def test_init_model_field_with_wrong_value_type():
    with pytest.raises(ValidationError, match="base_price': {'_schema': \['Invalid input type."):
        _ = Product(
            base_price_type=Product.API.PriceType.COST,
            base_price=Product.API.PriceType.COST,  # GwioModelField with wrong value
            vat=Vat(random.choice([10, 14, 24])),
            name='foo',
        )
