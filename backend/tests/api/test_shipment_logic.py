import pytest
from gwio.logic.packages.xpress_couriers import shipping_process, Shipment
from ._fakes.shipment_logic import random_product_line, random_order


def by_weight(p):
    return p['weight']


class TestShipmentLogic:
    @pytest.mark.parametrize('condition', [
        'over_price',
        'over_size',
        'over_weight',
        'cumulative_over_weight',
        'only_conditional_products',
        'conditional_products_with_normal_products',
        'single_product'
    ])
    def test_shipment_logic_base_on_size_weight_price(self, condition):
        base_product_lines = [random_product_line() for _ in range(3)]
        single_product = random_product_line()
        over_price_product = random_product_line(price=dict(final=dict(total=3000, unit=3000)))
        over_size_product = random_product_line(size=dict(length=150, height=100, width=90))
        over_weight_product = random_product_line(weight=60)
        cumulative_over_weight_product_1 = random_product_line(weight=48)
        cumulative_over_weight_product_2 = random_product_line(weight=48)

        result_dict = dict(
            over_price=dict(
                result=[
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_price_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, base_product_lines)
                ],
                product_lines=base_product_lines + [over_price_product]
            ),
            over_size=dict(
                result=[
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_size_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, base_product_lines)
                ],
                product_lines=base_product_lines + [over_size_product]
            ),
            over_weight=dict(
                result=[
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_weight_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, base_product_lines)
                ],
                product_lines=base_product_lines + [over_weight_product]
            ),
            cumulative_over_weight=dict(
                result=[
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [cumulative_over_weight_product_1]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [cumulative_over_weight_product_2]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, base_product_lines)
                ],
                product_lines=[cumulative_over_weight_product_1] + [cumulative_over_weight_product_2] + base_product_lines
            ),
            only_conditional_products=dict(
                result=[
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_weight_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_price_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_size_product])
                ],
                product_lines=[over_weight_product, over_price_product, over_size_product]
            ),
            conditional_products_with_normal_products=dict(
                result=[
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_weight_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_price_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_size_product]),
                    Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, base_product_lines)
                ],
                product_lines=[over_weight_product, over_price_product, over_size_product] + base_product_lines
            ),
            single_product=dict(
                result=[Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [single_product])],
                product_lines=[single_product]
            ),
        )

        order = random_order(shop_count=1, mall_count=1, product_lines=result_dict[condition]['product_lines'])
        res = shipping_process(order)
        assert res == result_dict[condition]['result']

    @pytest.mark.parametrize('shop_count, mall_count, condition', [
        (
                1,
                1,
                'single_shop_order'
        ),
        (
                2,
                1,
                'single_mall_order'
        ),
        (
                2,
                2,
                'multiple_malls_order'
        )
    ])
    def test_shipment_logic_base_on_shops_malls_count(self, shop_count, mall_count, condition):
        product_lines = [random_product_line() for _ in range(3)]
        result_dict = dict(
            single_shop_order=[Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, product_lines.copy())],
            single_mall_order=[Shipment(Shipment.ShippingMethod.VIA_LOCAL_HUB, product_lines.copy())],
            multiple_malls_order=[Shipment(Shipment.ShippingMethod.VIA_CENTRAL_HUB, product_lines.copy())],
        )
        order = random_order(shop_count=shop_count, mall_count=mall_count, product_lines=product_lines)
        res = shipping_process(order)
        assert res == result_dict[condition]

    @pytest.mark.parametrize('shop_count, mall_count, condition', [
        (
                2,
                1,
                'single_mall_order'
        ),
        (
                2,
                2,
                'multiple_malls_order'
        )
    ])
    def test_shipment_for_multiple_shops_and_malls_with_conditional_product(self, shop_count, mall_count, condition):
        base_product_lines = [random_product_line() for _ in range(3)]
        over_price_product = random_product_line(price=dict(final=dict(total=3000, unit=3000)))
        over_size_product = random_product_line(size=dict(length=150, height=100, width=90))
        over_weight_product = random_product_line(weight=60)
        result_dict = dict(
            single_mall_order=[
                Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_weight_product]),
                Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_price_product]),
                Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_size_product]),
                Shipment(Shipment.ShippingMethod.VIA_LOCAL_HUB, base_product_lines)
            ],
            multiple_malls_order=[
                Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_weight_product]),
                Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_price_product]),
                Shipment(Shipment.ShippingMethod.DIRECT_TO_CUSTOMER, [over_size_product]),
                Shipment(Shipment.ShippingMethod.VIA_CENTRAL_HUB, base_product_lines)
            ]
        )
        product_lines = [over_weight_product, over_price_product, over_size_product] + base_product_lines
        order = random_order(shop_count=shop_count, mall_count=mall_count, product_lines=product_lines)
        res = shipping_process(order)
        assert res == result_dict[condition]
