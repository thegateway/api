import hashlib
import json
import random
import uuid
from datetime import (
    datetime,
    timedelta,
    timezone,
)
from decimal import (
    Decimal,
)
from functools import reduce

import pytest
from flask import g
from werkzeug.exceptions import Conflict

from gwio.api.orders.base import basic_endpoint_logic
from gwio.core import rbac
from gwio.core.utils.payu.payu import PayuOrderStatus
from gwio.core.utils.testing.helpers import request_redirected_uri
from gwio.environment import Env
from gwio.logic.orders.types import (
    OrderAction,
    OrderState,
)
from gwio.logic.payments.handler import PaymentState
from gwio.models.orders import Order
from gwio.models.products import Product
from gwio.models.products.base import PRODUCT_TYPE_DELIVERY_METHOD
from gwio.models.tags import TagType
from gwio.user import User
from gwio.utils.fsm import FsmState
from gwio.utils.uuid import get_random_uuid
from ._mocks import mock_order_fsm_triggered_logic


@pytest.fixture(scope='function')
def customer(test_customer):
    test_customer.save()
    yield test_customer
    test_customer.delete()


@pytest.fixture(scope='function')
def multiple_shops_products(webshop, random_products, random_webshops):
    # Add products to webshop
    products = []
    for product in random_products(3, owner_guid=webshop.guid):
        product.save()
        products.append(product)
    webshops = []
    for ws in random_webshops(2):
        ws.save()
        webshops.append(ws)
        for product in random_products(3, owner_guid=ws.guid):
            product.save()
            products.append(product)
    webshops.append(webshop)
    yield products
    for product in products:
        product.delete()
    for webshop in webshops:
        webshop.delete()


@pytest.mark.usefixtures('mock_aws')
class TestApiShopsOrders():

    def test_order_endpoint_wrapper_failed_state(self, app, mocker, webshop_guid, create_test_order):
        mock_order_fsm_triggered_logic(mocker=mocker, force_exception=True)
        mocker.patch.object(rbac, 'admin', lambda: True)
        mocker.patch.object(Order, 'status_invalid', False)
        with app.application.test_request_context():
            g.user = User(guid=get_random_uuid())
            try:
                order = create_test_order(shop_guid=webshop_guid, _current_state=OrderState.CREATED)
                order.save()

                with pytest.raises(Conflict):
                    _ = basic_endpoint_logic(order=order, func=lambda order: order.process(OrderAction.PLACE))

                order.refresh()
                assert order.current_state == FsmState._FAILED
            finally:
                g.user = None

    def test_create_empty_order(self, customer, as_customer, webshop):
        res = as_customer.post(f'/customers/{customer.identity_id.split(":")[1]}/orders', json=dict(customer_name='Mr. Test #1', products=[]))
        assert 201 == res.status_code

    def ignore_test_modifiers_for_order_products(self, customer, as_customer, as_shopkeeper, webshop, customer_guid, random_products, faker):
        # Add products to webshop
        products = []
        for product in random_products(3):
            product.save()
            products.append(dict(product_guid=product.guid))
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/products/', json=products)
        assert 200 == res.status_code

        res = as_customer.post(f'/webshops/{webshop.guid}/orders',
                               json=dict(customer_name='Mr. Test #1', products=[dict(guid=p['product_guid'], qty=random.randint(1, 3)) for p in products]))
        order_guid = res.json['guid']

        product = random.choice(products)
        data = [dict(desc=faker.sentence(),
                     method=random.choice(['fixed', 'percentage']),
                     amount=random.randint(-4000, 4000) / 100) for _ in range(random.randint(1, 4))]
        res = as_shopkeeper.put(
            f'/webshops/{webshop.guid}/orders/{order_guid}/product_lines/{products[0]["product_guid"]}/modifiers/',
            json=data)
        assert 200 == res.status_code

        res = as_customer.get(f'/customers/{customer_guid}/orders/{order_guid}')
        modifiers = res.json['lines'][0]['price']['original']['modifiers']
        original_total = res.json['lines'][0]['price']['original']['total']['amount']
        actual_calculated_final = round(Decimal(original_total), 2)
        for modifier in modifiers:
            actual_calculated_final += Decimal(modifier['fixed_modifiers']['total']['amount'])

        actual_final = round(Decimal(res.json['lines'][0]['price']['final']['total']['amount']), 2)
        assert actual_final == round(actual_calculated_final, 2)

        for product in products:
            Product(product['product_guid']).delete()

    def test_order_with_campaign_product(self, as_admin, customer, as_customer, as_shopkeeper, webshop, random_products, faker):
        products = []
        for product in [product for product_type in Product.API.valid_campaign_types for
                        product in random_products(1 if product_type == PRODUCT_TYPE_DELIVERY_METHOD else 3, type=product_type, owner_guid=webshop.guid)]:
            product.save()
            products.append(dict(product_guid=product.guid, owner_guid=product.owner_guid))

        # Redirect testing, should be removed when old endpoint is removed
        res = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=[dict(product_guid=p['product_guid']) for p in products])

        assert 200 == res.status_code, res.json['message']

        duration = {
            'start': datetime.now(timezone.utc).isoformat(),
            'end': (datetime.now(timezone.utc) + timedelta(hours=2)).isoformat()
        }
        discount = dict(desc=faker.name(), method='percentage', amount=Decimal('0.8'))
        res = as_shopkeeper.post(f'/webshops/{webshop.guid}/tags', json=dict(name='discounts!', type=TagType.CAMPAIGN.value, duration=duration, data=discount))
        assert 201 == res.status_code, res.json['message']
        discount_guid = res.json['guid']

        res = as_admin.put(f'/product_tags/{discount_guid}', json=dict(add=[product['product_guid'] for product in products]))
        assert 204 == res.status_code, res.json['message']
        order_products = [dict(guid=p['product_guid'], qty=random.randint(1, 3), shop_guid=p['owner_guid']) for p in products]
        res = as_customer.post(f'/customers/{customer.identity_id.split(":")[1]}/orders',
                               json=dict(customer_name='Mr. Test #1',
                                         products=order_products))
        assert 201 == res.status_code, res.json['message']
        for line in res.json['lines']:
            assert line['price']['original']['modifiers']  # All products in the order should have modifiers via campaign

    def test_order_products_from_multiple_shops(self, multiple_shops_products, as_customer, webshop, as_shopkeeper, customer):
        res = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/',
                                     json=[dict(product_guid=p.guid) for p in multiple_shops_products])
        assert 200 == res.status_code

        order_products = [dict(guid=p.guid, qty=random.randint(1, 3), shop_guid=p.owner_guid) for p in multiple_shops_products]
        res = as_customer.post(f'/customers/{customer.identity_id.split(":")[1]}/orders',
                               json=dict(customer_name='Mr. Test #1', products=order_products))
        assert 201 == res.status_code
        summary = res.json['summary']

        def quantize(d):
            return Decimal(d).quantize(Decimal('0.01'))

        assert all(summary['currency'] == shop['currency'] for shop in summary['shops'])
        # Check the shop price and vat price sum to the whole order prices
        assert quantize(summary['price']) == reduce(lambda coll, el: coll + quantize(el['price']), summary['shops'], Decimal(0))
        assert quantize(summary['vat0_price']) == reduce(lambda coll, el: coll + quantize(el['vat0_price']), summary['shops'], Decimal(0))

        # Check the vat category amounts from shops sum to the whole order vat categories
        def get_vat(vats, vat):
            try:
                return next(filter(lambda v: v['vat'] == vat, vats))['amount']
            except StopIteration:
                return 0

        for vat in summary['vats']:
            assert quantize(vat['amount']) == reduce(lambda coll, el: coll + quantize(get_vat(el['vats'], vat['vat'])), summary['shops'], Decimal(0))


class TestFullOrderApi:
    def test_order_without_login(self, mock_payu, random_products, as_shopkeeper, as_anonymous, webshop, payment_method_payu, create_random_delivery_method,
                                 faker):
        # WTF? webshop fixture saves this?
        webshop.save()
        delivery_method = create_random_delivery_method(owner_guid=uuid.UUID(int=0))
        delivery_method.save()
        # Create order
        products = []
        for product in random_products(3, owner_guid=webshop.guid):
            product.save()
            products.append(dict(product_guid=product.guid))
        res = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=products)
        assert 200 == res.status_code

        customer_name = faker.name()
        res = as_anonymous.post(f'/customers/orders',
                                json=dict(customer_name=customer_name,
                                          payment_method=dict(guid=payment_method_payu.guid,
                                                              continue_url='http://localhost/test'),
                                          delivery_method=delivery_method.guid,
                                          customer=dict(name=customer_name,
                                                        phone=faker.phone_number()),
                                          shipping_address=dict(name=customer_name,
                                                                email=faker.email()),
                                          products=[dict(guid=p['product_guid'],
                                                         shop_guid=webshop.guid,
                                                         qty=random.randint(1, 3)) for p in products]))
        assert 201 == res.status_code, res.json
        order = Order.get(guid=uuid.UUID(res.json['guid']))
        assert order.current_state == OrderState.PROCESSING
        assert order.payment_data['state'] == PaymentState.NOT_STARTED

        # TODO: moto identity ids are not REGION:GUID so they don't match uuid route parameters
        # Unocomment lines when fixed: https://github.com/spulec/moto/issues/2800
        # res = as_anonymous.get(f'/customers/{order.buyer_identity_id.split(":")[-1]}/orders/{order.guid}/anonymous')
        # assert 200 == res.status_code
        # assert order.current_state == OrderState(res.json['state'])

        # Send webhook
        payload = json.dumps(dict(order=dict(extOrderId=str(order.guid),
                                             status=PayuOrderStatus.COMPLETED.name),
                                  localReceiptDateTime=datetime.now(tz=timezone.utc).isoformat(),
                                  properties=list()))
        signature_input = payload + Env.PAYU_SECOND_KEY
        signature = hashlib.md5(signature_input.encode('utf-8')).hexdigest()
        openpayu_header = ';'.join([f'{k}={v}' for k, v in dict(sender='checkout', signature=signature, algorithm='MD5', content='DOCUMENT').items()])
        res = as_anonymous.post('/payu/webhook', data=payload,
                                headers={'OpenPayu-Signature': openpayu_header,
                                         'Content-Type': 'application/json'})
        assert 200 == res.status_code, res.json

        order.refresh()
        assert PaymentState(order.payment_data['state']) == PaymentState.COMPLETED

        packages_route_root = f'/webshops/{webshop.guid}/orders/{order.guid}/packages/{order.packages[0].guid}'
        res = as_shopkeeper.put(f'{packages_route_root}/_pack')
        assert 200 == res.status_code
        res = as_shopkeeper.put(f'{packages_route_root}/_confirm')
        assert 200 == res.status_code

        order.refresh()
        assert order.current_state == OrderState.COMPLETED
        order.delete()

    def test_cod_order_without_login(self, mock_payu, mock_xpress_couriers, faker, random_products, as_shopkeeper, as_any_shopkeeper, as_anonymous, webshop,
                                     payment_method_cod, delivery_method_xpress_couriers, create_random_location, create_random_webshop):
        location = create_random_location(description='Zgrupowania AK "Kampinos", 00-001 Warszawa, Polska')
        location.save()
        webshop.locations.append(location.guid)
        webshop.save()
        products = []
        for product in random_products(3, owner_guid=webshop.guid):
            product.save()
            products.append(product)
        res = request_redirected_uri(as_shopkeeper.post, f'/webshops/{webshop.guid}/products/', json=[dict(product_guid=p.guid) for p in products])
        assert 200 == res.status_code

        other_webshop = create_random_webshop(locations=[location.guid])
        other_webshop.save()
        for product in random_products(3, owner_guid=other_webshop.guid):
            product.save()
            products.append(product)
        client = as_any_shopkeeper(str(uuid.uuid4()), str(uuid.uuid4()), str(other_webshop.guid))
        res = request_redirected_uri(client.post, f'/webshops/{other_webshop.guid}/products/', json=[dict(product_guid=p.guid) for p in products])
        assert 200 == res.status_code

        delivery_method_xpress_couriers.save()
        # Create order
        customer_name = faker.name()
        res = as_anonymous.post(f'/customers/orders',
                                json=dict(customer_name=customer_name,
                                          payment_method=dict(guid=payment_method_cod.guid),
                                          delivery_method=delivery_method_xpress_couriers.guid,
                                          customer=dict(name=customer_name,
                                                        phone=faker.phone_number()),
                                          shipping_address=dict(name=customer_name,
                                                                email=faker.email()),
                                          products=[dict(guid=p.guid,
                                                         shop_guid=p.owner_guid,
                                                         qty=random.randint(4, 6)) for p in products]))
        assert 201 == res.status_code, res.json
        order = Order.get(guid=uuid.UUID(res.json['guid']))
        assert order.current_state == OrderState.PROCESSING

        packages_route_root = f'/webshops/{webshop.guid}/orders/{order.guid}/packages'
        for package in order.packages:
            res = as_shopkeeper.put(f'{packages_route_root}/{package.guid}/_pack')
            assert 200 == res.status_code
            res = as_shopkeeper.put(f'{packages_route_root}/{package.guid}/_confirm')
            assert 200 == res.status_code

        order.refresh()
        for payment in order.payments:
            # TODO: WEBSHOP? NO WEBSHOP HERE ANYMORE
            payments_route_root = f'/webshops/{webshop.guid}/orders/{order.guid}/payments/{payment.guid}'
            res = as_shopkeeper.put(f'{payments_route_root}/_pay')
            assert 200 == res.status_code

        order.refresh()
        assert order.current_state == OrderState.COMPLETED
        order.delete()
