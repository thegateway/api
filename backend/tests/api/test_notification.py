from urllib.parse import urlparse

import boto3
import pytest
from envs import env
from moto import mock_s3

from gwio.utils.notify import Notification
from gwio.logic.orders.notifications import (
    OrderCancelledShopNotification,
    OrderCancelledUserNotification,
    OrderPaidShopNotification,
    OrderPaidUserNotification,
    OrderReceivedShopNotification,
    OrderReceivedUserNotification,
)
from gwio.logic.orders.types import OrderNotificationType
from ._mocks import (
    MockPayment,
    MockShop,
    MockUser,
)

_NOTIFICATIONS = (OrderReceivedUserNotification, OrderPaidUserNotification, OrderCancelledUserNotification,
                  OrderReceivedShopNotification, OrderPaidShopNotification, OrderCancelledShopNotification)


@pytest.fixture(scope='function')
def fake_email(faker):
    return faker.email()


@pytest.fixture(scope='class')
def mock_jinja_templates(faker):
    s3_mock = mock_s3()
    s3_mock.start()
    url = urlparse(env('EMAIL_TEMPLATE_LOCATION'))
    keys = []

    s3 = boto3.client('s3')
    s3.create_bucket(Bucket=url.netloc)
    for notification in _NOTIFICATIONS:
        key = f'{url.path.lstrip("/")}/{{template_path}}/{notification.template_base_name}'
        content = '\n'.join(faker.paragraphs(nb=10))
        s3.put_object(Bucket=url.netloc,
                      Key=key.format(template_path='email_html'),
                      Body=b'<html><body>#{{ order.guid }}' + content.encode('utf-8') + b'</body></html>')
        s3.put_object(Bucket=url.netloc,
                      Key=key.format(template_path='email_text'),
                      Body=('#{{ order.guid }} \n' + content).encode('utf-8'))
        keys.append(key.format(template_path='email_html'))
        keys.append(key.format(template_path='email_text'))
    yield
    s3.delete_objects(Bucket=url.netloc, Delete={'Objects': [{'Key': x} for x in keys]})
    s3.delete_bucket(Bucket=url.netloc)
    s3_mock.stop()


@pytest.mark.skip(reason='This fails because env and seems to test functionality that is going away.')
@pytest.mark.usefixtures('mock_aws', 'mock_jinja_templates')
class TestEmailNotifications:
    @pytest.mark.parametrize(('notification_type', 'ctx'), [
        (OrderNotificationType.ORDER_RECEIVED, dict(user=MockUser(), shop=MockShop(), system_mail=pytest.lazy_fixture('fake_email'))),
        (OrderNotificationType.ORDER_PAID, dict(user=MockUser(), shop=MockShop(), system_mail=pytest.lazy_fixture('fake_email'))),
        # (OrderNotificationType.ORDER_READY_FOR_PICKUP, dict(user=MockUser(), shop=MockShop(), system_mail=pytest.lazy_fixture('fake_email'))),
        (OrderNotificationType.ORDER_CANCELLED, dict(user=MockUser(), shop=MockShop(), payment=MockPayment(), system_mail=pytest.lazy_fixture('fake_email')))
    ])
    def test_email_notification(self, faker, notification_type, ctx, create_test_order):
        ctx['order'] = create_test_order()
        ctx['title'] = f'{faker.word()} {ctx["order"].guid} {faker.word()}'
        for handler in Notification.from_type(notification_type):
            handler.emit(ctx)
            for notification in [notification for x in ctx.values() if hasattr(x, 'received_notifications') for notification in x.received_notifications if
                                 isinstance(notification, _NOTIFICATIONS)]:
                assert all([str(ctx['order'].guid) in x for x in (notification.title, notification.email_text, notification.email_html)])
