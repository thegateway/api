import json
import random
import uuid
from datetime import (
    datetime,
    timezone,
)

import pytest
from dateutil.parser import parse

from gwio.models.residence import Residence
from ._fakes.residences import random_residence_data


@pytest.fixture(scope='class')
def residence_suite(create_random_residence, random_identity_id):
    residence = create_random_residence(owner_identity_id=random_identity_id())
    residence.save()
    yield residence


@pytest.fixture(scope='function')
def residences_suite(create_random_residence, random_identity_id):
    residences = []
    for _ in range(10):
        residence = create_random_residence(owner_identity_id=random_identity_id())
        residence.save()
        residences.append(residence)

    yield residences

    for r in residences:
        r.delete()


@pytest.fixture(scope='function')
def customer(mock_aws, test_customer, customer_identity_id):
    assert mock_aws
    test_customer.save()
    yield test_customer
    test_customer.delete()


@pytest.mark.usefixtures('mock_aws', 'customer')
class TestResidences:
    def test_list_residences(self, as_admin, residences_suite):
        res = as_admin.get(f'/residences/')
        assert 200 == res.status_code
        all_residences = list(x for x in Residence.scan())
        unarchived_count = len(list(x for x in all_residences if x.archived is None))
        assert unarchived_count == len(res.json)
        residence = random.choice(list(x for x in residences_suite if x.archived is None))
        residence.archived = datetime.now(tz=timezone.utc)
        residence.save()
        res = as_admin.get(f'/residences/?include_archived=true')
        assert 200 == res.status_code
        assert len(all_residences) == len(res.json)

        res = as_admin.get(f'/residences/')
        assert 200 == res.status_code
        assert unarchived_count - 1 == len(res.json)

    def test_create_get_residence(self, as_admin, as_customer, customer_identity_id):
        residence = random_residence_data()
        residence['owner_identity_id'] = customer_identity_id
        post_data = Residence.Schemas.Post().dump(residence)
        res = as_admin.post('/residences', json=post_data)
        assert 200 == res.status_code
        actual_residence = res.json
        residence_guid = actual_residence.pop('guid')
        archived = actual_residence.pop('archived')
        for attr_name, value in post_data.items():
            assert value == actual_residence[attr_name], attr_name

        res = as_admin.get(f'/residences/{residence_guid}')
        assert 200 == res.status_code
        actual_residence['archived'] = archived
        assert {**actual_residence, 'guid': residence_guid} == res.json

        res = as_customer.get(f'/customers/{customer_identity_id.split(":")[1]}/residences')
        assert 200 == res.status_code
        assert 1 == len(res.json)

    def test_update_residence(self, as_admin, residence_suite):
        residence = residence_suite
        data = Residence.Schemas.Put().dump(random_residence_data())
        res_put = as_admin.put(f'/residences/{residence.guid}', json=data)
        res_get = as_admin.get(f'/residences/{residence.guid}')
        assert res_put.json == res_get.json
        for attr_name, value in data.items():
            assert value == res_get.json[attr_name], attr_name

        for k, v in json.loads(Residence.Schemas.Put().dumps(random_residence_data())).items():
            res_put = as_admin.put(f'/residences/{residence.guid}', json={k: v})
            res_get = as_admin.get(f'/residences/{residence.guid}')
            assert res_put.json == res_get.json
            assert v == res_get.json[k]

    def test_delete_residence(self, as_admin, residence_suite):
        residence = random.choice(list(Residence.scan(archived__not_exists=True)))
        res = as_admin.get(f'/residences/{residence.guid}')
        assert 200 == res.status_code
        assert residence.guid == uuid.UUID(res.json['guid'])
        start_of_test = datetime.now(tz=timezone.utc)
        res = as_admin.delete(f'/residences/{residence.guid}')
        assert 204 == res.status_code
        res = as_admin.get(f'/residences/{residence.guid}')
        assert res.json['archived'] is not None
        archived_date = parse(res.json['archived'])
        assert type(archived_date) == datetime
        assert start_of_test <= archived_date <= datetime.now(tz=timezone.utc)
        # Deleting the same residence again should raise conflict
        res = as_admin.delete(f'/residences/{residence.guid}')
        assert 409 == res.status_code
