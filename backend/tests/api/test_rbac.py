import uuid

import pytest
from flask import g

import gwio.user
from gwio.core import rbac
from gwio.models import user
from .consts import DEMO_SHOP_ID

admin = rbac.admin
customer = rbac.customer
shopkeeper = rbac.shopkeeper
User = gwio.user.User


@pytest.mark.parametrize(
    ('test_msg', 'rbac_func', 'user_uuid', 'shop_uuid', 'is_admin', 'is_shopkeeper', 'is_customer', 'expected_result', 'args'), [
        ("Admin passes as admin", admin, uuid.uuid4(), None, True, False, True, True, dict()),
        ("Admin does not pass as shopkeeper", shopkeeper, uuid.uuid4(), None, True, False, True, False, dict()),
        ("Admin passes as customer", customer, uuid.uuid4(), None, True, False, True, True, dict()),
        ("Shopkeeper does not pass as admin", admin, uuid.uuid4(), uuid.uuid4(), False, True, True, False, dict()),
        ("Shopkeeper passes as shopkeeper", shopkeeper, uuid.uuid4(), uuid.uuid4(), False, True, True, True, dict()),
        ("Shopkeeper passes as shopkeeper of their own shop", shopkeeper, uuid.uuid4(), uuid.UUID(DEMO_SHOP_ID),
         False, True, True, True, {'shop_guid': uuid.UUID(DEMO_SHOP_ID)}),
        ("Shopkeeper does not pass as shopkeeper of someone else's shop", shopkeeper, uuid.uuid4(),
         uuid.uuid4(), False, True, True, False, {'shop_guid': uuid.uuid4()}),
        ("Shopkeeper passes as customer", customer, uuid.uuid4(), uuid.uuid4(), False, True, True, True, dict()),
        ("Customer does not pass as admin", admin, uuid.uuid4(), None, False, False, True, False, dict()),
        ("Customer does not pass as shopkeeper", shopkeeper, uuid.uuid4(), None, False, False, True, False, dict()),
        ("Customer passes as customer", customer, uuid.uuid4(), None, False, False, True, True, dict()),
    ])
def test_rules(simple_app, test_msg, rbac_func, user_uuid, shop_uuid, is_admin, is_shopkeeper, expected_result, is_customer, args):
    with simple_app.test_request_context():
        groups = list()
        if is_shopkeeper:
            groups.append('shopkeeper')
        if is_admin:
            groups.append('admin')
        if is_customer:
            groups.append('customer')
        g.user = User(
            guid=user_uuid,
            organization_guid=shop_uuid,
            groups=groups
        )
        assert rbac_func(**args) == expected_result, test_msg
