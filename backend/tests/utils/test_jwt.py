import simplejson

from gwio.environment import Env
from gwio.security.jwt import (
    sign,
    verify,
)


def test_sign_verify():
    # To avoid trying to connect to AWS
    Env._store = dict(SYSTEM_SECRET_KEY='secret')

    payload = dict(
        a='a',
        b=2,
    )
    gwio_signed_payload = sign(simplejson.dumps(payload))
    assert payload == verify(simplejson.loads(gwio_signed_payload))
