# -*- coding: utf-8 -*-

from gwio.pytest.suites import include_suites


def _unset_statics(f):
    """
    include_suites() uses static vars inside the function that are only set on the first call, so to
    do different tests with different set of environment variables we need to reset the function to
    its initial state.
    """
    f.matchers = None
    f.enabled = None
    f.disabled = None


def test_include_suites_local_run(monkeypatch):
    """Check that TGW_ENABLED_TESTSETS evironment variable is obeyed"""
    monkeypatch.setenv("TGW_ENABLED_TESTSETS", "suite1,suite2,suite3,aws.*,.*email")
    _unset_statics(include_suites)
    assert include_suites('suite1')
    assert include_suites('suite2')
    assert include_suites('suite3')
    assert not include_suites('suite4')
    assert include_suites('aws_email')
    assert include_suites('aws_xmail')
    assert include_suites('local_email')
    assert not include_suites('local_xmail')


def test_include_suites_ci_run(monkeypatch):
    """Make sure all tests are enabled when running in CI and TGW_ENABLED_TESTSETS is not explictly set"""
    monkeypatch.setenv("CI", "0.4")
    _unset_statics(include_suites)
    assert include_suites('suite1')
    assert include_suites('suite2')
    assert include_suites('suite3')
    assert include_suites('suite4')
    assert include_suites('aws_email')
    assert include_suites('aws_xmail')
    assert include_suites('local_email')
    assert include_suites('local_xmail')
