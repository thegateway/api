import base64
import uuid

from jose import jwt

shop_guid = uuid.UUID('89f09a2a-41f0-5e53-bcf7-52038c9ee17b')
_key = 'xgNWqve61U3HmKNFzHjNnMqc4gePMPp71vv0tAp8muY='

key = base64.b64decode(_key)


def main():
    token = jwt.encode(dict(iss=f"shop:{shop_guid}", organization_guid=str(shop_guid), groups=['shopkeeper']), key, algorithm="HS256")

    print(f'Bearer {token}')


if __name__ == '__main__':
    main()
