import json
import os

import jinja2
import pytest

from gwio.services.notification_to_email.app import (
    _handle,
    render,
)

j2 = jinja2.Environment(loader=jinja2.PackageLoader('gwio.services.notification_to_email', 'templates'))


def load_sample_event():
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data/sample_event.json")) as f:
        return json.load(f)


@pytest.mark.skipif(True, reason="Don't send emails in unit tests.")
def test_handler_fn():
    ev = load_sample_event()
    _handle(ev, None)


@pytest.mark.parametrize('fmt', ['html', 'txt'])
@pytest.mark.parametrize('notification_type', ['order-received'])
def test_render(notification_type, fmt):
    # This test saves the local result files so that it's easier to manually check if they look right
    # It *will* however catch any syntax errors or undefined variables in templates, so it should be run by default
    ev = load_sample_event()
    template = j2.get_template(f'{notification_type}.{fmt}.jinja2')
    rendered = render(template, ev['detail']['payload'])
    assert rendered
    with open(f"{notification_type}.{fmt}", 'w') as f:
        f.write(rendered)
