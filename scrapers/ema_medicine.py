import csv
import json
import logging
import re
from collections import defaultdict

import boto3
import requests
from googleapiclient.discovery import build

logger = logging.getLogger(__name__)
MISSING_FN = 'missing.json'
VNR_FN = 'VNR.json'
API_KEY = 'AIzaSyB_kInBgZk4iAQkpSwgWilMW0wzIKAqXBM'
CSE_ID = '001601067564909383309:wlfacuthcd0'
BUCKET_NAME = 'imeds-pakkaus'
URL_RE = re.compile(
    'http://www.ema.europa.eu/docs/(?P<lang>[a-z]{2}_[A-Z]{2})/document_library/EPAR_-_Product_Information/'
    '(?P<type>veterinary|human)/(?P<part1>[0-9]{6})/(?P<part2>[A-Z]{2}[0-9]{9}.pdf)')


def google_search(search_term, api_key=API_KEY, cse_id=CSE_ID, **kwargs):
    service = build("customsearch", "v1", developerKey=api_key)
    return service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()


def download_and_upload_pdfs():
    urls = dict()
    fn_to_vnr_map = defaultdict(list)
    try:
        with open(MISSING_FN) as f:
            missing = json.load(f)
    except Exception as e:
        logger.fatal(e)
        missing = dict()
    with open('urls.json') as f:
        to_get = json.load(f)
    for vnr, url in to_get.items():
        m = URL_RE.match(url)
        if m is not None:
            url = 'http://www.ema.europa.eu/docs/{dl_lang}/document_library/EPAR_-_Product_Information/{type}/{part1}/{part2}'.format(
                dl_lang='fi_FI', **m.groupdict())
            logger.info('{}: {}'.format(vnr, url))
            fn = '{0}_{1}_{part1}_{part2}'.format('FI', m.group('type')[:3], **m.groupdict())
            fn_to_vnr_map[fn].append(vnr)
            urls[vnr] = (url, fn)
        else:
            logger.warning("URL for {} does not looks like product information leaflet: {}".format(vnr, url))
            missing[vnr] = url
    s3 = boto3.client('s3')
    try:
        with open(VNR_FN) as f:
            vnr_to_fn_map = json.load(f)
    except Exception as e:
        logger.fatal(e)
        vnr_to_fn_map = dict()
    fetched = set(vnr_to_fn_map.values())
    try:
        for vnr, (url, fn) in urls.items():
            if vnr in vnr_to_fn_map:
                logger.debug('Skipping {} as it is already present'.format(vnr))
                continue
            logger.info('Saving {}: {} -> {}'.format(vnr, url, fn))
            vnr_to_fn_map[vnr] = fn
            if fn in fetched:
                logger.debug("Skipping {} as already fetched".format(url))
                continue
            fetched.add(fn)
            r = requests.get(url)
            if r.status_code > 299:
                logger.error(r.status_code)
                missing[vnr] = url
            s3.put_object(
                ACL='public-read',
                Bucket=BUCKET_NAME,
                ContentType='application/pdf',
                Metadata=dict(
                    VNRs=' '.join(fn_to_vnr_map[fn])
                ),
                Key='EU/{}'.format(fn),
                Body=r.content
            )
    finally:
        with open(MISSING_FN, 'w') as f:
            json.dump(missing, f)
        with open(VNR_FN, 'w') as f:
            json.dump(vnr_to_fn_map, f)
        s3.upload_file(VNR_FN, BUCKET_NAME, 'EU/VNR.json')


def main():
    # get_urls()
    download_and_upload_pdfs()


def get_urls():
    data = dict()
    misses = dict()
    fields = ["seloste", "tunnustyyppi", "yksilointitunnus", "lupanumero", "nimi", "markkinoija"]
    with open('seloste.list') as f:
        try:
            reader = csv.DictReader(f, fieldnames=fields, delimiter=';')
            for row in reader:
                if int(row['tunnustyyppi']) != 1:
                    continue
                lupanumero = row['lupanumero']
                if lupanumero.startswith('EU/'):
                    vnr = row['yksilointitunnus']
                    try:
                        pdf_url = get_url_for_pdf(lupanumero)
                    except Exception as e:
                        logger.error('No results for {}'.format(row))
                        misses[vnr] = lupanumero
                    print(pdf_url)
                    data[vnr] = pdf_url
        finally:
            with open('urls.json', 'w') as f:
                json.dump(data, f)
            with open('misses.json', 'w') as f:
                json.dump(misses, f)


def download(myyntilupanumero, vnr):
    r = requests.get(get_url_for_pdf(myyntilupanumero))
    with open('{}.pdf'.format(vnr), 'wb') as f:
        f.write(r.content)


def get_url_for_pdf(myyntilupanumero):
    res = google_search('{} valmisteyhteenveto'.format(myyntilupanumero), num=1)
    return res['items'][0]['link']


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
